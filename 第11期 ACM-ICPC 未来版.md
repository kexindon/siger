![输入图片说明](https://images.gitee.com/uploads/images/2021/1230/233007_24913fa6_5631341.jpeg "ICPC副本II.jpg")

> 这是一个写了9个多月的学习笔记，零零星星地，以《好想让时光倒流》为题是致敬恩师，也是忏悔自己的不够刻苦，愿发多了，力不够啦:pray: 这是在恩施面前 “耍赖” 啦，曾经多少个码字的深夜，才凝结出这厚厚的一本 SIGer 开源文化期刊。而学的多了，才知道自己的无知。笔记开篇的两个关键词 [ACM 志愿者](https://gitee.com/yuandj/siger/issues/I3B6OG#note_4561451) [SIG-acm](https://gitee.com/yuandj/siger/issues/I3B6OG#note_4561471) 看到了才知道 学生学的太少了，太迟了。:pray:

> 时间就是一个尺一个度，也曾经意气风发，也曾豪言壮志，也曾FLAGs立了又立，人生总要交出答卷，不只为了面对天地，而要无愧于心。“不忘初心，不负韶华” 是每个人都应有的气韵，好想时光倒流，更加珍惜时光，不虚度光阴。这也是 SIGer 希望青年一代拥有的精神，也是怀有少年心的我们的努力方向，度人度己。“用奉献让心灵宁静” “关爱他人就是关爱自己” 到 “福久绵长，祺福天下” 吾心成长啦 :pray:

> 也就是这份初心使然，才有了[石榴派](http://shiliupi.cn/) —— [SIGer](https://gitee.com/flame-ai/siger) 年度最佳作品，它应当由恩师颁奖，只是现在它的成就还太小太小，仅以此篇以表敬意。三月，春意盎然，2022更值得期待。看下面的缘起，接着的就是学习笔记啦。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/230106_4ae71628_5631341.png "在这里输入图片标题")   _推进计算机科学与专业的发展_ 

[ACM 志愿者](https://gitee.com/yuandj/siger/issues/I3B6OG#note_4561451)
-  **SIG**  和会议–与您的SIG的领导联系，并说明您希望更多地参与SIG或其会议的意愿。

[SIG-acm](https://gitee.com/yuandj/siger/issues/I3B6OG#note_4561471)
- 特别兴趣小组
  > ACM的特殊兴趣小组（SIG）代表了计算的主要领域，解决了推动创新的技术社区的利益。SIG提供了大量针对特定计算子学科的会议，出版物和活动。它们使成员可以共享专业知识，发现和最佳实践。

# 《 [ACM-ICPC 未来版](https://gitee.com/yuandj/siger/issues/I3CHVL) 》

> 未来版是我未完成的恩师之愿，也是在这份嘱托下才结缘了现在的机器博弈，想来因缘际会间就是这样的奇妙啊 :pray:

### 相关资料

![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/172406_3745329f_5631341.png "屏幕截图.png")

https://tools.icpc.global/

### 学习摘要

- SIG Conference Planning Guide
- 虚拟会议：最佳实践指南
- SIG会议-推动计算技术创新（我们要推动社区创新）

### 笔记

- ICPC 工具 https://tools.icpc.global/ 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4571871_link)】

  > 欢迎使用ICPC工具网页！此页面包含由国际大学编程竞赛（ICPC）工具组实施的各种工具，其中大多数最初是为在ICPC世界总决赛中使用而开发的，并已被改编为在其他编程竞赛中使用。这些工具已用于支持各种编程竞赛，包括世界各地大学的本地竞赛，世界各地的多个ICPC地区竞赛以及许多ICPC世界总决赛。

  > 所有ICPC工具均设计为可协同工作，并且在适用时，它们均基于已发布的ICPC标准。特别是，许多工具都来自CLI规范网站上发布的基于竞争性学习计划主持下发布的规范，并基于该规范。

    - 竞赛数据服务器（CDS） https://tools.icpc.global/cds 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575484_link)】
    - 解析器 https://tools.icpc.global/resolver 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575487_link)】
    - 简报管理员 https://tools.icpc.global/pres-admin 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575493_link)】
    - 演示客户 https://tools.icpc.global/pres-client 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575497_link)】
    - 气球工具 https://tools.icpc.global/balloon-util 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575498_link)】
    - 教练视图 https://tools.icpc.global/coach-view 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575501_link)】
    - 问题集编辑器 https://tools.icpc.global/problem-set-editor 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575505_link)】
    - 比赛实用程序 https://tools.icpc.global/contest-utils 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575507_link)】

  - 免责声明 | 贡献 | 执照

  - [变更日志](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4572243_link)
    > 您可以在下面找到ICPC工具的变更日志。创建新的版本时，它将自动更新。

  - [所有版本](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575437_link)
    > 您将在下面找到ICPC工具的所有可用版本。大多数版本也都有.sha256和。sha512文件，可用于验证下载。

    - https://github.com/icpctools
    - https://github.com/icpctools/icpctools/releases
    - icpctools/许可 (版权所有（C）2019 ICPC基金会)

- [如何建立一个在线的 ICPC 训练系统？](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575556_link)

  ICPC 训练赛 RANK

  - 可以给自己掐表
  - 收集一些赛题，并进行本地化，避免歧义
  - 能否获得一些志愿者资源，丰富这个训练系统
  - 这是一个高校 ICPC 团队的预备队。

  ACM-SIG 是我们的学习榜样

- [参加ICPC国际编程竞赛的大学生到底有多厉害，在校生该如何准备？](https://www.zhihu.com/question/389967724) 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575562_link)】

  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/023642_920668cf_5631341.png "在这里输入图片标题")

  代表清华出战 ACM-ICPC 2017 右一（策神）这位数独大侠与火焰棋实验室的交集 【图】[1](https://images.gitee.com/uploads/images/2021/0323/023642_920668cf_5631341.png)/[2](https://images.gitee.com/uploads/images/2021/0323/023927_4777f5e3_5631341.png)/[3](https://images.gitee.com/uploads/images/2021/0323/023945_b6dc016c_5631341.png)/[4](https://images.gitee.com/uploads/images/2021/0323/024016_6d22f59b_5631341.png)
  
- [Phase 2 of the ICPC Shanghai Training Camp powered by Huawei](https://mp.weixin.qq.com/s?__biz=MzI5MjU0NDAwMw==&mid=2247487858&idx=1&sn=b85430c69df2873aedcfbe7350cecb16)
  ICPCNews 公号推送 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575563_link)】

  > ICPC大学很高兴宣布由华为提供支持的ICPC上海培训营的第二阶段，其中包括在2021年3月21日和4月4日举行的两次在线培训课程。有关所有活动的更新，请访问 https://u.icpc.global/training/ 。以及培训平台在上线时的链接。欢迎所有个人和团队参加这些激动人心的培训活动！#ICPCU

- [以ACM/ICPC竞赛促进计算机专业实践教学研究](https://m.fx361.com/news/2018/0917/4234690.html) 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575569_link)】

  > 摘 要： ACM/ICPC程序设计竞赛是世界上最具影响力的计算机类学科竞赛之一，能够最直接体现学生对专业知识的掌握以及实践能力。ACM/ICPC集训在选手日常训练方面都有一套行之有效的机制，对学生的学习兴趣、分析解决问题能力、创新能力等方面的培养都起着推动作用，ACM竞赛是对学生实践能力最直接的考核。因此整合在线判题系统建设实践平台，将ACM/ICPC模式引入计算机专业实践教学，是提高计算机专业学生实践能力的途径之一。(计算机时代)

    1. ACM/ICPC竞赛训练机制
    2. 基于在线判题系统的实践平台建设
    3. 利用实践平台促进实践教学途径

- [字节跳动ICPC网络赛](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4575571_link)

- [ACM Turing Award Honors Innovators Who Shaped the Foundations of Programming Language Compilers and Algorithms](https://awards.acm.org/about/2020-turing) 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_4783700_link)】

  - Influential Textbooks 有影响力的教科书  
    合著了九本有影响的书（包括第一版和以后的版本）。他们最广为人知的两本书包括：

    1. The Design and Analysis of Computer Algorithms (1974) 计算机算法设计与分析
    2. Principles of Compiler Design (1977) 编译程序设计原理

  - About the ACM A.M. Turing Award

- [2021 ICPC Asia EC网络预选赛第一场报名公示](https://gitee.com/link?target=https%3A%2F%2Fmp.weixin.qq.com%2Fs%3F__biz%3DMzI5MjU0NDAwMw%3D%3D%26mid%3D2247488116%26idx%3D1%26sn%3D292a1d513aec75832a8319ba4b13f166)
  ICPC ICPCNews 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_6655735_link)】

  - 2021 ICPC 北京总部网站 上线啦 https://icpc.pku.edu.cn/ 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_6656227_link)】

    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0913/155024_db9e3046_5631341.png "屏幕截图.png")

    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0913/155057_a57d935b_5631341.png "屏幕截图.png")

    - 2021-09-01 [《2021-2022年度ICPC Asia EC 网络预选赛方案》会议纪要](https://icpc.pku.edu.cn/xwdt/130629.htm) 【[原文](https://icpcasia.wp.txstate.edu/2021/08/20/2021-2022%e5%b9%b4%e5%ba%a6icpc-ec-%e7%bd%91%e7%bb%9c%e9%a2%84%e9%80%89%e8%b5%9b%e6%96%b9%e6%a1%88/)】

      - 一、会议概况：

        1. 时间：2021.08.14 14:30-16:30
        2. 地点： ZOOM会议
        3. 主持人：黄金雄+李文新
        4. 会议记录：卢家兴
        5. 参会人员（16人）：

      - 三、各类咨询委员会相关情况：
        各类咨询委员会在ICPC Asia EC委员会领导之下开展工作。

        1. 科学暨命题委员会：召集人北大罗国杰（北大李文新），成员待定；
        2. 竞赛暨组织委员会：召集人北大李文新（浙大陈越，北大张勤健），成员由EC委员会成员（包括秘书）、各赛站RCD联系人组成；
        3. 监督暨辅导委员会：召集人上海大学沈俊，成员待定；
        4. 外联暨赞助委员会：联合召集人西北工大姜学锋，成员待定；

    - 通知

      - 09-13-2021 [2021 ICPC Asia EC网络预选赛第一场报名公示](https://icpc.pku.edu.cn/tzgg/130925.htm)
      - 09-03-2021 [2021 ICPC Asia EC网络预选赛通知（第二轮，含报名链接）](https://icpc.pku.edu.cn/tzgg/130706.htm)
      - 08-20-2021 [2021 ICPC Asia EC网络预选赛通知（第一轮）](https://icpc.pku.edu.cn/tzgg/130591.htm)

    - 相关链接

      - icpc官网 https://icpc.global 

      - 亚洲区 governor 黄金䧺教授博客 https://icpcasia.wp.txstate.edu

      - ICPCNews公众号 icpcnews
        An official WeChat account for ACM International Collegiate Programming Contest (ICPC), affiliated with ICPC Foundation.

  - ICPC ASIA COUNCIL CHAIR CJ HWANG BLOG 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_6656572_link)】

    ICPC Asia Governor 亚洲主席黄金雄 (西杰)博客
    > WEBPAGE TOTAL COUNT (BEGINNING SEP 5, 2021)

    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0913/160331_c4597253_5631341.png "屏幕截图.png")

    **Important Related Links:** 

    - [ICPC Foundation](http://icpc.foundation/)
    - [ICPC World Finals Information](https://icpc.baylor.edu/)
    - [Asia Blog 2013.1-2021.7](http://blog.sina.com/cjhwang)  **http://blog.sina.com/cjhwang** 
    - [Web Site Host CS Txstate](http://www.txstate.edu/)

    POSTS BY PUBLISHING DATE
    - [2021 ICPC Asia EC网络预选赛通知 Online Contest 2nd Announcement](https://icpcasia.wp.txstate.edu/2021/09/05/2021-icpc-asia-ec%e7%bd%91%e7%bb%9c%e9%a2%84%e9%80%89%e8%b5%9b%e9%80%9a%e7%9f%a5-online-contest-2nd-announcement/) September 5, 2021
    - [World Finals team selection rules from Asia Pacific (2021-2022 cycle)](https://icpcasia.wp.txstate.edu/2021/09/05/world-finals-team-selection-rules-from-asia-pacific-2021-2022-cycle/) September 5, 2021
    - [2021-2022年度ICPC Asia EC 网络预选赛](https://icpcasia.wp.txstate.edu/2021/08/20/2021-2022%e5%b9%b4%e5%ba%a6icpc-ec-%e7%bd%91%e7%bb%9c%e9%a2%84%e9%80%89%e8%b5%9b%e6%96%b9%e6%a1%88/) August 20, 2021
    - [Asia West Continent Region meeting Minutes](https://icpcasia.wp.txstate.edu/2021/07/27/asia-west-continent-region-meeting-minutes/) July 27, 2021
    - [Asia Pacific (PSP) 2021 WF teams from 2020-2021 Cycle](https://icpcasia.wp.txstate.edu/2021/07/23/asia-pacific-psp-2021-wf-teams-list/) July 23, 2021
    - [《2021年度 （2021.9-2022.8）ICPC EC 研讨会》会议纪要](https://icpcasia.wp.txstate.edu/2021/07/22/%e3%80%8a2021%e5%b9%b4%e5%ba%a6-%ef%bc%882021-9-2022-8%ef%bc%89icpc-ec-%e7%a0%94%e8%ae%a8%e4%bc%9a%e3%80%8b%e4%bc%9a%e8%ae%ae%e7%ba%aa%e8%a6%81/) July 22, 2021
    - [ICPC银川站2020-2021事件的处理结果](https://icpcasia.wp.txstate.edu/2021/07/22/icpc%e9%93%b6%e5%b7%9d%e7%ab%992020-2021%e4%ba%8b%e4%bb%b6%e7%9a%84%e5%a4%84%e7%90%86%e7%bb%93%e6%9e%9c/) July 22, 2021
    - [WF Teams 2021 from Asia EC 2020 Regionals](https://icpcasia.wp.txstate.edu/2021/07/21/hello-world/) July 21, 2021

  - [2020-2021年ICPC银川站打铁总结](https://blog.csdn.net/weixin_45755679/article/details/117001389)  【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_6657006_link)】
  - [ICPC银川站总结](https://www.cnblogs.com/KingZhang/p/14791571.html) 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_6657027_link)】
  - [如何评价 2019 ACM/ICPC 银川站网络赛？](https://www.zhihu.com/question/343590565/answer/809013747) 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_6657058_link)】

### [我要感谢 UnixCenter 的活动，让我结缘我的恩师](https://gitee.com/yuandj/siger/issues/I3CHVL#note_7537676_link) :pray:

- [原创 与大师Debian创始人对话 -- 北京，上海，南京，长春](https://blog.csdn.net/yuandj/article/details/1818804) 【[笔记](https://gitee.com/shiliupi/piserver/issues/I4JP3Q#note_7537618_link)】
  > [Unix-Center.Net] 与大师对话 -- 北京，上海，南京，长春Unix-Center.Net bbs@unix-center.net member@unix-center.netWed, 10 Oct 2007 08:22:40 +0000尊敬的Unix-Center.Net用户：Unix-Center.Net诚挚邀请您参加我们与Sun 中国技术社区和ChinaUnix.
2007-10-10 17:02:00  6508  0

- [原创 《为什么时光不能倒流》UnixCenter的奖品！](https://blog.csdn.net/yuandj/article/details/1771820) 【[笔记](https://gitee.com/yuandj/siger/issues/I3CHVL#note_7537694_link)】
  > 今天是个高兴的日子，收到了《为什么时光不能倒流》Sun, 26 Aug 2007 18:56:29 +0000尊敬的Unix-Center.Net用户：     由于软件的通知功能可能有误，我们可能将领奖通知信错误地发给了所有会员。所以，幸运网友的名单以 Unix-Center.net 网站公布的名单为准： Unix-Center.Net的第二批幸运网友名单及领奖办法 http://www.unix-
2007-09-04 15:17:00  4714  1

- [原创 UNIX-CENTER初体验——没有半个字节浪费！](https://blog.csdn.net/yuandj/article/details/1772559)
  > 以下是阅读《[从Windows登陆Unix体验中心服务器的几种方法](http://www.unix-center.net/?p=10)》时的感受, 图文并貌。黑色背景的终端，让偶想起大学的VT220终端了。当时用的是DEC系列，1M内存，FORTRAN-77[此部分内容由Jerry Tao最初发表于其个人Blog，经其本人同意后转贴至本站。]好细致的文风，让我感受到了UNIX的严谨。整个UNIX-CENTER
2007-09-05 01:59:00  8993  1

<img src="https://images.gitee.com/uploads/images/2021/1125/005108_385cf965_5631341.png" height="333px"> <img src="https://images.gitee.com/uploads/images/2021/1125/005438_3535f3d6_5631341.png" height="333px">