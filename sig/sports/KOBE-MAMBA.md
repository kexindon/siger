《曼巴精神科比自传》

- P25 [我恪守的训练哲学（摘要）](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13493036_link) 《[关于科比的训练哲学](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13493162_link)》
- P26 [我的午夜特训已成传奇](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13497909_link)
- P28 [研究比赛录像，重在细节 P29 我不只训练身体，也训练精神](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13532559_link)
- P31 [局势不同，精神准备也随之不同](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13563184_link)

[「曼巴精神」是什麼？看看Kobe Bryant如何成為當今最偉大的籃球員](https://mf.techbang.com/posts/7628-kobe-02)

<p><img width="706px" src="https://foruda.gitee.com/images/1665035200726929231/1eb7a3e8_5631341.jpeg" title="312KOBE.jpg"></p>

本期专题是关于体育精神的，以科比自传的曼巴精神为题再合适不过啦。这既是体育频道的首篇成刊，也是我送给新晋频道主编 @[赵乐陶](https://gitee.com/zhao-letao) 的礼物，也是一周来共同学习的纪念，笔记体是重要 SIGer 成刊的标志，不只有成果，更有共同学习的过程呈现，这才是 SIGer 推崇的注重过程的学习分享啦。专题笔记附在专题之后，更能体现学习过程的变化。以读书笔记的页码为索引，直观明了，更多读书笔记将成为频道读者期盼的一项内容。只在范例中做了原书的摘抄，但整篇摘抄是不鼓励的，推荐同学们阅读原书。

> 封面设计选择原版书也是一种继承，喜欢科比额头的汗水，这是曼巴精神的直观表现。不喧宾夺主了，现在交给主编：

# [事关篮球，我无所畏惧](https://gitee.com/zhao-letao/siger/issues/I5U84D)

### 事关篮球，我无所畏惧

我的意思是，如果自己想掌握什么新招数，就会把它弄懂，马上付诸实践。  
投丢、动作难看、出洋相，全都无所谓。我心里一直都明白，比赛的最终结果才最重要。  
我始终专注于这一事实：为了掌握新技能，必须大胆尝试，一旦能熟练应用，武器库就得以丰富。  
如果代价是大量工作和几次投丢，我没意见。孩提时代，为了掌握新招数，我刻苦训练，不知疲倦。  
如果有什么新动作引起我的注意，不管是亲眼看见，还是从录像中看到，我都马上投入练习，第二天继续加倍苦练，然后直接用到比赛中。  
刚进联盟时，我学什么都很快。看到什么新动作，马上“下载”下来，把它彻底弄明白。从一开始，我就渴望成为最伟大的球员之一。  
日益精进、成就伟大，这是我持之以恒的内在渴望，从不需要任何外部力量的刺激。新秀赛季，一开始有球探报告说我不够强硬。比赛中的第一次上篮，我就遭到强力冲撞，对手都以为已经将我摆平。  
然而，我会在紧接着的下一回合卷土重来，宁可领到进攻犯规，也要给他们点颜色瞧瞧。  
为了成就伟大，我并不需要额外的推动力。从第一天起，我就渴望统治一切。  
我的心态是：我要把对手琢磨透，不管你是艾弗森、麦蒂还是卡特—或者，用现在的球员来类比，不管你是詹姆斯、威斯布鲁克还是库里—我的目标都是吃透你。  
为了做到这一点，为了能破解难题，我愿意比其他人做得更多。这对我来说其乐无穷。

### 曼巴精神

一开始，“曼巴精神”只是我在推特上发起的一个话题标签，它激励人心，饱含智慧，令人过目难忘。  
但随后它从那里流行开来，开始有了更多象征意义。“曼巴精神”是一种思维模式，它不在于寻求结果，而在于如何做才能取得结果，在于从现实到目标的这个过程。  
它是一段旅程、一种方法、一种生活方式。我真心认为，在所有努力之中，拥有这种心态最为重要。每当我听到有人提起“曼巴精神”，无论他是一位精英大学球员、一位NBA球星，还是一位《财富》500 强公司的CEO，我都感到意义非凡。  
每当有人谈到他们从“曼巴精神”中获得了激励，我都会觉得所有的辛勤工作、所有的汗水、所有的凌晨3∶00早起没有白费。  
书中的每一页都饱含着我的经验教训，不仅仅关于篮球，更关于“曼巴精神”。

### 篮球带我无所不至

我曾梦寐以求的每一个机会，篮球都如数奉上。  
在这段旅程中，它让我受益良多。收益远不止在球场之内，球场之外的收获更是难以估量。  
如果没有篮球，我不会懂得该如何去创造，去书写，不可能理解人性，也不可能明白该如何去领导。  
本质上讲，比赛教会我讲故事的艺术。如果没有篮球，我既拿不到艾美奖，也得不到奥斯卡奖，更不可能像现在这样，拥有那些创造力十足的梦想和愿景，渴望能在未来展示给你们看。  
没错，篮球带我无所不至。如今，我也要把篮球带到世界各地。

### [“你见过凌晨四点半的洛杉矶吗？”](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13485333_link)

“你见过凌晨四点半的洛杉矶吗？”  
“即使投丢十九次，我也不会失去投第二十次的信心！”  
“第二名只能说明你是头号输家！”  
有一种信仰叫做科比。“科比”一词之于篮球界，乃至全世界各行各界都有举足轻重的影响。  
正所谓你的偶像可能不是曼巴，但你偶像的偶像一定是曼巴。一个人可能没有听过科比在篮球场上叱咤风云的事迹，但他一定听过“曼巴精神”，并将其视作训典。绝杀时刻面对三人包夹又怎样，照样顶着压力艰难出手；拼抢时手指关节错位又怎样，掰回来继续打；拼尽最后一丝力，跟腱断裂又怎样，忍痛罚完最后两颗球。曼巴精神蕴含了太多，有迅猛、有坚持、有无畏……我想，这样的精神不会只停留在球场上，也会出现在日常生活的方方面面，更是激励我们前进、助我们披荆斩棘的精神动力！

# 《曼巴精神科比自传》

### P25 [我恪守的训练哲学](https://gitee.com/zhao-letao/siger/issues/I5UAIC)（[摘要](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13493036_link)）

![输入图片说明](https://foruda.gitee.com/images/1665116315301905035/bc341d20_5631341.jpeg "manbap25.jpg")

> 初入联盟时我只有17岁，那时就开始举重训练。那是一点儿也不花哨的、 **最基本的** 、久经时间检验的举重训练，每一次上举，都专注调动一组肌肉同时发力，直到完成规定次数。纵观整个职业生涯，无论在赛季期还是休赛期，我都在周一、周二、周四和周五各做 **_90分钟_** 举重训练， **风雨无阻** 。我所说的举重，是那种很重、很难、练到手臂失去感觉的举重。练完之后，我才能去球馆做投蓝训练。

> 多年来，我的例行安排与时俱进，但训练哲学始终如一。 **如果有些事情已经被其他伟大球员证明有效，而且对你同样有效，为什么要舍近求远，另辟蹊径？** 如果有效，就坚持到底， **哪怕它不再流行** 。

- 概念计算
- 不盲从，不冒险
- 很多人为了标新立异，追求粉丝数量的增加而不择手段

### 《[关于科比的训练哲学](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13493162_link)》

即使科比成为了联盟顶级的球星，他也会坚持进行不花哨的、最基本的、久经时间检验的举重训练。这个训练，他从初入联盟就坚持着直至退役。我想，正是这样最基础的长时间的训练筑就科比职业生涯的辉煌。 “从基础练起，并且长时间坚持”是科比的训练哲学，我想，这个哲学道理是放之四海而皆准的。

升入高中以来，数学老师（其实各科老师均有提及）不止一次、不厌其烦地对我们给出“计算是命根子”、“在草稿纸上写计算，不跳步，训练计算的基本功，在答题纸上写逻辑过程”这样的谆谆告诫，再返过来看科比的训练哲学，简直让我有躯干为之一振的感觉！

是呀，无论做什么事情，基础都是至关重要的，基础计算、基本概念是一切学习之根基，只有把根扎实才能进行进一步的学习和尝试，也最终才能在学习上有所突破。学习的过程中更为重要的另一点就是思考，学而不思则罔。我相信，如果我能做好上述两点，一定会在学习上有更大的突破和进步。

### P26 [我的午夜特训已成传奇](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13497909_link)

午夜特训是有意为之，是平衡事业和家庭的折中选择。

我始终认为，如果提早开始工作，每天能练得更多。如果训练从上午11点开始，练几小时，休息4小时，下一次训练只能安排在下午5:00到7:00。但如果从早上5:00练到7:00，就能在上午11:00到下午2:00、下午6:00到8:00各练一次。早点开工，每天就能多一次完整训练。 **一个夏天下来，能累计多练无数个小时** 。

与此同时，提早工作有助于平衡篮球与生活。孩子们早上醒来的时候，看到我陪在他们身边，一点儿也不知道我刚从球馆练完回来。到了晚上，我能哄他们入睡，然后再回去训练。 **我用掉的是原本属于自己的时间，而不是他们的。** 

我不愿牺牲比赛水平，也不想牺牲家庭时间，所以决定 **牺牲睡眠时间** ，仅此而已。

![](https://foruda.gitee.com/images/1665191360004840969/4e807ce4_11711172.jpeg)

一个人如果有爱好，那是一定要在这个爱好上花费时间的。时间又是那么宝贵，如何权衡确实是一个问题。从科比的午夜特训传奇中，我知道了人应该学会取舍，懂得进退，开合有度，这是人生第一必修课。步入高一，学业压力确实不小，但我又不想放弃篮球时间，所以如果我想打篮球，那我首先要抓紧白天的时间尽量多地完成作业，假日就更要充分利用，然后挤出时间去打球。这是午夜特训给我的启示，也是篮球给我的启示。

![](https://foruda.gitee.com/images/1665191748322914321/8ad2b286_11711172.jpeg)

> @[袁德俊](https://gitee.com/yuandj) [人生是选择的结果，每一次的取舍都塑造了现在的自己。享受当下，奋进前行 :pray: 未来无限光明。一起加油。](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13504279_link)

### P28 [研究比赛录像，重在细节](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13532559_link)

在很小的年纪——非常小的年纪——我就开始研究能拿到手的各种比赛录像，如饥似渴，乐此不疲。说到底，有一些人喜欢看手表，另一些人却乐于研究手表的工作原理。

这些事总是其乐无穷： **观看、研究、提出** 最重要的问题——为什么?

然而，随着时间推移，我看比赛录像时最关心的部分悄然改变， **不再关注看到的事物，转而观察本该出现却没出现的事物 ； 不再关注发生了的事情，转而去思考本该发生却没能发生的事情** 。研究比赛录像最终变成对备选方案、临场变招和其他选项的想象，同时完善各种细节，深究为什么有些行为能奏效，另一些却是徒劳。

### P29 [我不只训练身体，也训练精神](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13532559_link)

识别比赛关键、洞悉细枝末节的唯一途径，是在场下刻意训练精神，专注日常生活中每一个细节。通过阅读、在训练中集中注意力和刻苦工作，我的专注力得以强化，磨炼出聚精会神、不开小差的能力。

与阅读同等重要的是，跟前几代伟大球员保持良好关系。看看我的退役仪式有哪些大人物出席，那是最好的证明。这能让你明白，为了取得球衣退役这样的成就，我付出了怎样的努力。你会看到比尔·拉塞尔、卡里姆·阿卜杜-贾巴尔、“魔术师”约翰逊、杰里·韦斯特、詹姆斯·沃西。他们对我不吝赐教，帮我在竞争中脱颖而出。这就是找到导师如此重要的原因，他们有如北极星，你随时都能抬头仰望，得到指引。

![](https://foruda.gitee.com/images/1665299643083945940/0ac456f6_11711172.jpeg)

[细节决定成败，态度决定一切，心态决定人生。](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13532718_link)

@[袁德俊](https://gitee.com/yuandj)

> 王者，或者说抵达顶峰的人的共同点，“训练身体，也训练精神”，是最恰当的比喻。凡事都求个精进，如同 @赵乐陶 所比喻的 “左手画圆，右手画方”，不只知其然，还要知其所以然，同样勉励自己，开源是方法，不是目标，开源的分享精神才是驱动社会变革的动力，也是开源文化科普的中心思想。

> 再分享一个学习方法，DIFFTEST，是SIGer RISCV频道，开源芯片 教育计划 一生一芯的 重要方法论，任何系统通过不同的维度和方式进行测试，可以加大发现BUG的速度和效率，是重要的敏捷开发的方法论。如同 @赵乐陶 的旁注笔记，通过自己的理解给予的注释，是不同于摘抄的学习笔记方式，是经过了自己的思考的。与 DIFFTEST 异曲同工。

> 有时候，我们可以换换脑子，以为做信息安全的 UP 主介绍自己的爱好 “古琴” 的时候，也是这样说明的。

> 伟大的载人航天的奠基人，三十年前，说出歪果仁能搞，中国人为什么不能搞，现在 SIGER 遥望比邻频道的 精神指引， 钱老，就以 科学和艺术的结合，是他取得如此成就的根基，追忆自己的父亲在儿时对自己的培养。这是一个人自我塑造的多维发展的必由之路。 @赵乐陶 就是 SIGer 少年的典范啦。加油！期待 你对曼巴精神的进一步解读。

### P31 [局势不同，精神准备也随之不同](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13563184_link)

它随机应变，取决于我认为该对特定比赛采用何种心态。例如，需要兴奋起来，我就去听重音乐。需要舒缓情绪，我会去听高中时期在大巴上百听不厌的老歌，找回那种感觉。

一切的目的，都是为了调整状态，为接下来的比赛做好准备。有些比赛需要更高的精神强度，需要把个性和精神都调整到兴奋状态。另一些比赛需要保持冷静，我不会听任何音乐，有时甚至会一人独坐，一语不发。

关键在于，你必须了解自身情绪的运行机制，以及希望让它怎样运行。一切都始于清醒的自我认知。

![](https://foruda.gitee.com/images/1665392258476839406/2b18937f_11711172.jpeg)

看到这篇文章的时候，我对“局势不同，精神准备也随之不同”真的是感同身受！2019年我上初一，2020年伊始，新冠疫情扑面而来，直到2022年中考，疫情伴随了我整整三年。在这三年中，我的学习状态一直在不停的调整当中，真就是局势不同，精神准备也随之不同！初一下学期居家整整一个学期，我竟然还度过了一段边上网课边玩游戏不知老师所云的日子！当我意识到问题严重性的时候，我马上调整！在父母的帮助下强制戒掉ipad游戏，强制退出游戏模式，跟妈妈一起去她的单位上网课，非常幸运，我调整过来了，初一下的期末考试成绩相当理想。自此我就知道了状态的重要性，也开始不停地在学习、懈怠、兴奋、浮躁等等各种状态中不停地调整！最重要的一次调整就是在中考前两个月，疫情再次反复，我们又居家了。这次我不怕了，虽然时有浮躁，但最终我还是踏踏实实地完成了网课、二模、直到中考！所以，我无比赞同科比的这篇文章，我相信在以后的生活和学习，甚至是将来的工作中，我还是会时不时地调整状态，努力以最好的状态迎接未知的一切！

> 非常榜的分享！自律即使对于成年人都很难真正形成。体育的自我激励，和个人成长中的体会是同样的。只有经历过，战胜过自己，才能懂得其中的道理，这一点在另一位体育编委的分享中，也同样看到。

> @赵乐陶 你非常圆满地完成了本周的值班，我想，你的这周曼巴精神的分享，已经是很好的素材。只要稍加串联，和 SIGer 科普做个结合，横向比对，一定会有一篇，非常好的值班心得的。你也正式成为 SIGer 体育频道的主编，没有之一，你的尽心尽责，一定可以为体育频道，带来崭新的风貌的。

> 这周，老师一直再支援天文频道，也是目前独立频道发展的最好的一个范例频道，其频道主编 @林书平 也是一个笔耕勤奋的编委，同时，他坚持的天文摄影，也产出颇丰。你可以参考。老师还将一如既往的做好后勤保障，编辑的工作老师会协助，必能事半功倍。你就尽情分享吧。

> 最后，祝学业有成，不负家人的期望，在此也感谢妈妈，给 SIGer 送来了这么好的小伙子！:pray:

---

【[笔记](https://gitee.com/zhao-letao/siger/issues/I5U84D)】事关篮球，我无所畏惧 （[封面](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13487228_link)）

- [敏感词](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13486539_link)，可能需要点儿耐心，慢慢排查，老师通常是分段儿回帖，找到段落后，再逐句排查。昨天晚上给 氢能 的学长排查了一段儿。  https://gitee.com/men-jianwu/menjianwu/issues/I5U6BL#note_13485066_link 这只能是个小插曲。[`note_13485326`](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13485326_link)

- @zhao-letao 修改了描述，这是新值，开始分享

  - **事关篮球，我无所畏惧** 
  - **曼巴精神** 
  - **篮球带我无所不至**

- [“你见过凌晨四点半的洛杉矶吗？”](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13485333_link)

- 封面建议：

  <p><img height="99px" src="https://foruda.gitee.com/images/1665014086408864370/32cb3f04_11711172.png"> <img height="99px" src="https://foruda.gitee.com/images/1665014268536197418/82bd0f36_11711172.png"></p>

  > [看着就热血沸腾！](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13486516_link)

  - [如果我这个体育频道能出刊，就是300 中的一份子，对吗？](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13486853_link)

    - Dorothy 23:01

      > 袁老师，300 to 500，300 ，是什么意思呢  
      > 如果我这个体育频道能出刊，就是300 中的一份子，对吗？

    - 袁德俊 23:01

      > 期刊数。SIGER 的成果  
      > 你的专题是 309  已经是啦 [玫瑰]  
      > <p><img height="69px" src="https://foruda.gitee.com/images/1665029740074163550/b3ed4cff_5631341.png"></p>

    - 袁德俊 23:04

      > 同学们的分享，就是老师的动力。  
      > 一起学习的赶脚

    - Dorothy 23:04

      > 我今天看了一篇文章，写了一段感想，我不知道明天放在哪里？是新建一个issue还是在昨天的issue下继续跟帖呢？
      > 《事关篮球，我无所畏惧》—— 这是科比的自传，这是我的感想

    - 袁德俊 23:04

      > 主笔的节奏。  
      > 新建一个 ISSUE  
      > 回头再聚合。[强]

    - Dorothy 23:05

      > 明白了，我明天在我的仓下新建一个issue
      > 我打算就这样看一篇写一段感想，既……又……[偷笑]

    - 袁德俊 23:05

      > ISSUES 是论坛，讨论的地方。  
      > 新的议题，就可以新一个 ISSUE 。然后其他同学和老师回帖支持。建议。  
      > （边看边写）非常好的习惯。  
      > 老师见证了 300期期刊的内容是怎么一点一滴成长起来的，很带劲。  
      > 这在你的文章中，我找到了能量来源，这就是自我激励，互相鼓励，这就是社群，和社区的力量。  
      > 同学们也是我的老师，也是我的精神支柱啦。相互鼓励。  
      > 我们是一个 TEAM

    - Dorothy 23:08

      > 明白[偷笑]  
      > 嗯，我继续努力

    - 袁德俊 23:11

      > **篮球带我无所不至，如今，我也要把篮球带到世界各地。**  
      > 老师用一个关键词描述它，“法门”，它是每个人与世界连接的钥匙。老师就有这样的感受，分享，就是老师的法门。以后老师再分享更多故事给你。  
      > 你的分享，让我回想起了最初的分享，第一次分享。

    - Dorothy 23:12

      > 太好了  
      > 加入这个团体让我有一种不一样的感觉

    - 袁德俊 23:13

      > SIGER 有一个SLOGAN 北向文化，文化就是精神的引领，就和你分享的文字中描述的，篮球早已经超越啦篮球。而所有同学的兴趣，在精神层面上都将统一。这就是 SIGER 的目标。从一二三三步，慢慢步入到开源的精神境界，每个其中的人又带入自己的精神，最后大家的精神世界统一在一起，将无往不利，所向披靡。这比任何物质的吸引力，都来得更有归属感，这就是热爱的力量。我从你的文字中，产生了共鸣感。有种久别重逢的感觉。SIGER 还有一个 SLOGAN 叫 SIGER 星云。两颗恒星实际上是很遥远的。包括巨大无比的星系团，一旦他们彼此靠近，将带着巨大的吸引力，创生出更加巨大的天体。这是 SIGER 的方法论的起点。我们彼此共鸣，就是彼此吸引和靠近的过程。这也是老师最期待的时刻。期待你的更多分享。也能激励起老师更多的共鸣和产出。我也是在和同学们的交流中，不断进步的。所以说，我们是共同学习啦。

    - 袁德俊 23:20

      > 假期也注意劳逸结合。早点休息。  
      > 老师觉少，再工作一会儿。

- [MAMBA MENTALITY KOBE BRYANT](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13486864_link)

  > The Mamba Mentality cannot be separated from its origins: Bryant gave himself the nickname "The Black Mamba" in an effort to rebrand himself in the aftermath of a sexual assault case. All of his sponsors except Nike had dropped him. "My vision was to build a brand and do all these things," Bryant told the New Yorker's Ben McGrath in 2014.
How Kobe Bryant's 'Mamba Mentality' changed the NBA

  - [科比·布莱恩特](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13486877_link)

    - 出生： 1978年 8月 23日 (44 岁) · 宾夕法尼亚州
    - 身高： 1.98 米
    - 数据来源: 豆瓣电影 · Baidu

  - [Mamba mentality 图库](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13486947_link)

    <p><img width="33.3%" src="https://foruda.gitee.com/images/1665030876820821067/19afa0af_5631341.png"> <img width="28%" src="https://foruda.gitee.com/images/1665030959825922070/cfba824e_5631341.jpeg"> <img width="30.6%" src="https://foruda.gitee.com/images/1665030990309927860/5d545b59_5631341.png"> <img width="19%" src="https://foruda.gitee.com/images/1665030715004223354/3f875fe6_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1665030835335305897/1a790318_5631341.jpeg"> <img width="18.6%" src="https://foruda.gitee.com/images/1665031064049496983/416a7b20_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1665031150664088370/9ce2de0c_5631341.jpeg"> <img width="18.6%" src="https://foruda.gitee.com/images/1665031267643404059/6cc3dc1e_5631341.png"> <img width="32.5%" src="https://foruda.gitee.com/images/1665031346319308302/39cb774a_5631341.jpeg"> <img width="26%" src="https://foruda.gitee.com/images/1665031405716753245/403b6c11_5631341.jpeg"> <img width="31%" src="https://foruda.gitee.com/images/1665031673416754269/0ae7b04f_5631341.jpeg"></p>

    - [Mamba mentality, el éxito de Kobe Bryant](https://www.josecarmelo.com/blog/mamba-mentality-el-exito-de-kobe-bryant/)  
josecarmelo.com|2015 × 2560 jpeg|图像可能受版权保护。
    - [The Mamba Mentality | Kobe Bryant | Macmillan](https://us.macmillan.com/books/9780374201234)  
Macmillan PublishersMacmillan Publishers|2537 × 3269 jpeg|图像可能受版权保护。
    - [Remembering Kobe Bryant: From 'Mamba mentality' to his final interview](https://www.freep.com/story/sports/2020/01/27/remembering-kobe-bryant-mamba-mentality-his-final-interview/4574192002/)  
Detroit Free PressDetroit Free Press|1423 × 800 jpeg|图像可能受版权保护。
    - [Discover Kobe Bryant Murals in Los Angeles | Discover Los Angeles](https://www.discoverlosangeles.com/things-to-do/discover-kobe-bryant-murals-in-los-angeles)  
Discover Los AngelesDiscover Los Angeles|2600 × 1733 jpeg|图像可能受版权保护。
    - [MAMBA MENTALITY - Globuss](https://www.gramatnicaglobuss.lv/gramatas/literatura-anglu-valoda/nonfiction/mamba-mentality)  
gramatnicaglobuss.lv|1400 × 856 jpeg|图像可能受版权保护。
    - [The Mamba Mentality | Kobe Bryant | Macmillan](https://us.macmillan.com/books/9780374201234)  
Macmillan PublishersMacmillan Publishers|2497 × 3289 jpeg|图像可能受版权保护。
    - [The Mamba Mentality](https://us.macmillan.com/books/9780374719159)  
Macmillan PublishersMacmillan Publishers|2560 × 3320 jpeg|图像可能受版权保护。
    - [The Mamba Mentality Book Summary by Kobe Bryant](https://www.shortform.com/summary/the-mamba-mentality-summary-kobe-bryant)  
shortform.com|1264 × 1690 png|图像可能受版权保护。
    - [「曼巴精神」是什麼？看看Kobe Bryant如何成為當今最偉大的籃球員 ...](https://mf.techbang.com/posts/7628-kobe-02)  
mf.techbang.commf.techbang.com|3900 × 2424 jpeg|图像可能受版权保护。
    - [Le monde entier rend hommage à Kobe Bryant | WAVE®](https://wave.fr/monde-entier-rend-hommage-a-kobe-bryant-273505)  
wave.fr|1300 × 1000 jpeg|图像可能受版权保护。
    - [Latest Kobe Bryant Black Mamba Wallpaper ~ Joanna-dee.com](https://joanna-dee.com/kobe-bryant-black-mamba-wallpaper/)  
joanna-dee.com|3840 × 2485 jpeg|图像可能受版权保护。

  - [「曼巴精神」是什麼？看看Kobe Bryant如何成為當今最偉大的籃球員](https://mf.techbang.com/posts/7628-kobe-02) （[全文转载](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13487006_link)）

  - [BILIBILI 《曼巴精神》](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13487079_link)

    - [“总有人要赢的，为什么不是我”](https://www.bilibili.com/video/BV1Hq4y1X7QK)

      > bgm Discovery-Ak 末尾电影是 当幸福来敲门。

    - [“每天叫醒你的不是闹钟，是梦想！”](https://www.bilibili.com/video/BV1s54y1Y7Pb)

      > bgm dusk 审核大大辛苦了

    - [科比讲述黑曼巴精神：如果你永不畏惧](https://www.bilibili.com/video/BV1r7411673K) 

      > 全站排行榜最高第61名 | 110.4万 | 1.1万 | 2020-01-27 13:56:13

      <p><img height="99px" src="https://foruda.gitee.com/images/1665033904250487647/dfff60c9_5631341.png"></p>

- [读书笔记摘录示范](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13493036_link)

  - [就是它啦。这会是一个非常好的专题。归并到一个 ISSUE 下更便于管理专题内容。](https://gitee.com/zhao-letao/siger/issues/I5UAIC#note_13493031_link)

    - [做了图片旋转，摘要加粗，加重，旁白提取，格式分段。这些编辑技巧都在输入框上的](https://gitee.com/zhao-letao/siger/issues/I5UAIC#note_13493053_link) 小问号里。![输入图片说明](https://foruda.gitee.com/images/1665116594335324583/6832652f_5631341.png "屏幕截图") `https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13493036_link`

      - [收到，袁老师，我再学学。我每天看几页，一点点结合自身的经历再写点儿感想](https://gitee.com/zhao-letao/siger/issues/I5UAIC#note_13493112_link) :blush:
      - [袁老师，我还是没找到怎么把图片旋转](https://gitee.com/zhao-letao/siger/issues/I5UAIC#note_13493144_link) :stuck_out_tongue_closed_eyes:
      - [做在一个issue下？也就是我今天其实不用再新建一个issue，是这个意思吗？那我明天接着这个issue继续写下去。](https://gitee.com/zhao-letao/siger/issues/I5UAIC#note_13493148_link)

    - [手机有编辑功能，可以简单处理，老师用的是专用的图片编辑软件 PS ，这个可以老师代劳，很方便。你尽情分享抒发即可。](https://gitee.com/zhao-letao/siger/issues/I5UAIC#note_13493158_link) 

    - [是的，一个立项后，就直接回帖即可。ISSUE 有两级目录，学会这个，可以很好的管理，同一个议题下的，不断更新。](https://gitee.com/zhao-letao/siger/issues/I5UAIC#note_13493161_link)

  - [即使科比成为了联盟顶级的球星](https://gitee.com/zhao-letao/siger/issues/I5UAIC#note_13493109_link)，...

    - [分段，要中间空一行，这是 MD 格式的标记，否则，正式 PR 的时候，会混到一起，这个编辑小问号，可以抽看，看看，提高自己的编辑能力。MD 文件的发明宗旨，就是不过多考虑版面问题，直接抒发。](https://gitee.com/zhao-letao/siger/issues/I5UAIC#note_13493171_link)

  - [哦哦，我看到了！谢谢您，袁老师！](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13493152_link)

    - [你的进步是显见的，也是最快的。俨然就是老编委了。和技能水平无关，只和专注投入的时间成正比，分享之心就是一切。](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13493355_link)

  - [读书笔记摘录示范](https://gitee.com/zhao-letao/siger/issues/I5U84D#note_13602411_link) [已经详细复制了以上沟通过程，笔记整理到一个 Issues 里。可以关闭，设置为已完成。](https://gitee.com/zhao-letao/siger/issues/I5UAIC#note_13603194_link)

---

起初，我以为【曼巴精神】只不过是我在推特上使用的标记，是某个可以朗朗上口的聪明词语而已。没想到它后来展翅起飞了，从此变成一种象征，代表着更伟大的事物。

曼巴精神的重点不在于结果——而是追求结果的过程。它的重点是你的做法，以及你亲身经历的旅程。它是一种生活的态度。我真心的认为，要尽力养成这种精神，这点非常重要。

每当我听闻某个优秀的大学选手、NBA球员或是名列《财富》伍佰强的总裁使用 #MambaMentality，我都觉得这个词语很有意义。当我听到人们说自己从曼巴精神中得到启发，我就觉得一切的努力、所有汗水、每一个凌晨三点起床练球的时刻都值得了。这也是我写作本书的原因。我透过书页教导，不只教篮球，也教曼巴精神。

![输入图片说明](https://foruda.gitee.com/images/1665031346319308302/39cb774a_5631341.jpeg "在这里输入图片标题")

# [「曼巴精神」是什麼？看看Kobe Bryant如何成為當今最偉大的籃球員](https://mf.techbang.com/posts/7628-kobe-02)

2019年4月09日 15:00

起初，我以為「曼巴精神」只不過是我在推特上使用的標記，是某個可以琅琅上口的聰明詞彙而已。沒想到它後來展翅起飛了，從此變成一種象徵，代表著更偉大的事物。
 

### 曼巴精神 The Mamba Mentality

曼巴精神的重點不在於結果，而是追求結果的過程。它的重點是你的做法，以及你親身經歷的旅程，它是一種生活的態度。我真心的認為，要盡力養成這種精神，這點非常重要。

每當我聽聞某個優秀的大學選手、NBA球員或是名列《財富》五百強的總裁使用#MambaMentality，我都覺得這個詞彙很有意義。當我聽到人們說自己從曼巴精神中得到啟發，我就覺得一切的努力、所有汗水、每一個凌晨三點起床練球的時刻都值得了。

這也是我寫作本書的原因。我透過書頁教導，不只教籃球，也教曼巴精神。

### 教學時刻 A TEACHING MOMENT

![输入图片说明](https://foruda.gitee.com/images/1665032433888848922/5c2031a4_5631341.jpeg "24577a9ef72fe6b683db3b4f56504694.jpg")

我還是一個年輕球員的時候，身體的平衡大有問題。

從站姿開始，看看我和麥可差在哪裡。麥可從腰部以上都是打直的，他的身體絕沒有朝著任何一個方向傾斜，正因為這樣，他擁有了平衡與重心。他完全掌控自己的身體，也掌控了這一球的結果。

接下來，請看我的防守姿態，做個對照。按照人家所教的，我用前臂抵著他的背部，不幸的是，這大概是我唯一做對的地方。我的身體太往前傾斜，對他施加了太大的壓力，這是個大錯，光是因為這一點，重力的作用就能讓我失去平衡。

![输入图片说明](https://foruda.gitee.com/images/1665032484406467606/b79098c5_5631341.jpeg "ca1af84995b913e33e1966c3904e0245.jpg")

結果就是，麥可只要隨便來一招，例如果斷往右旋轉，或是向左做一個假動作，就能把我晃開。然後，他就得到了空間，可以起身投籃或是轉身甩開我，我的這個防守真的很不妙。

謝天謝地，我在一九九八年就看到這張照片。仔細研究過後，我修正了我的姿勢與平衡，從此之後，要在低位對付我就困難多了。

### 投籃假動作 Fake Shot

![输入图片说明](https://foruda.gitee.com/images/1665032517687175656/28fafdbd_5631341.jpeg "9645b1d1ee4f757e3a9047a6498f557c.jpg")

西河很珍惜「防守我」這項挑戰，反過來，我也很愛對他展現十八般武藝。其中一招是心理層面的：熟知對手。我知道他很好強，當時的他跟現在沒兩樣，所以一抓到機會就會試圖搧我火鍋。所以我會猛然做一個投籃假動作，然後買到他的犯規，或是擺脫他的防守。

年輕人畢竟是年輕人。他總得繳學費，而我就是來教育他的。

隨著比賽進行，我會慢慢對他使出一招一式。我的目的很簡單，要不就是在他頭上出手，我比他略高幾吋；要不就是把他引到場上對我有利的位置。在他的防守之下，我不想做太多無謂的運球，我會溜到低位或是罰球線兩側，然後耐心等候機會。

### 警告：花時間讀這本書，就是踏上一趟高階籃球的冒險之旅

![输入图片说明](https://foruda.gitee.com/images/1665032553378609055/3c1db3ec_5631341.jpeg "4195570b91db44402de7d71dcc9c4b5d.jpg")

這本書絕對會讓你深入理解柯比對籃球運動的全心投入，以及他對細節的重視。一個人有天賦是一回事，但擁有「學習一切細節」的驅動力量，這又是另一回事。據說發明籃球運動的詹姆士．奈史密斯（James Naismith）曾經說過：「要打籃球很容易，要精通籃球就難了。」

這本書就是一扇窗，打開它，讓讀者窺見一位籃球大師的內心世界。如果你有心的話，透過本書傑出的照片（出自攝影家安德魯．伯恩斯坦），再加上書裡主人翁柯比的智慧，也許這本書能夠讓你的籃球打得更好。

![输入图片说明](https://foruda.gitee.com/images/1665032574745980597/de9e9800_5631341.jpeg "312606bd416766643ced47f10a2cc3c0.jpg")

柯比帶著「我要成為史上最偉大球員」的才華及慾望，進入了NBA。他以毅力及全心的投入，達成了這個目標。湖人這支歷史悠久的球隊給了他觀眾與平台，但他的高度成就，全然來自內心。

本書的照片說明了柯比是如何思索球賽的。事實上，柯比面對球賽的方式，也為他生命的下一個階段做好準備。在我看來，他未來生命的趣味與強度，絕不會輸給他在湖人隊打球的生涯。

—菲爾．傑克森（Phil Jackson），1999–2004，2005–2011，教練

### 本文節錄自《曼巴精神》

- 作者：柯比布萊恩 Kobe Bryant
- 出版社：遠流出版
- 出版日期：2019/02/24
- 更多書籍資訊請點此

▼《曼巴精神》首刷版開箱影片