- [走近业余卫星通信|你的第一次业余卫星通信：它比你想象中更简单(一)](https://www.hnra.org.cn/nd.jsp?id=242) 2021-02-01 18:00
- [走近业余卫星通信|你的第一次业余卫星通信：它比你想象中更简单(二)](https://www.hnra.org.cn/nd.jsp?id=241) 2021-02-01 17:59
- [走近业余卫星通信|你的第一次业余卫星通信：它比你想象中更简单(三)](https://www.hnra.org.cn/nd.jsp?id=240) 2021-02-02 17:59
- [走近业余卫星通信|你的第一次业余卫星通信：它比你想象中更简单(四)](https://www.hnra.org.cn/nd.jsp?id=239) 2021-02-03 17:58
- [《走近业余卫星通信》（五）| 业余卫星线性转发器](https://www.hnra.org.cn/nd.jsp?id=238) 2021-02-04 17:57
- [《走近业余卫星通信》（六）| 载人航天和业余无线电](https://www.hnra.org.cn/nd.jsp?id=237) 2021-02-05 17:57
- [《走近业余卫星通信》（七）| 聚焦日本业余卫星通信](https://www.hnra.org.cn/nd.jsp?id=236) 2021-02-06 17:53
- [《走近业余卫星通信》（八）| 聚焦OSCAR-7业余卫星](https://www.hnra.org.cn/nd.jsp?id=235) 2021-02-07 17:53
- [《走近业余卫星通信》（九）完结| 聚焦VUSAT OSCAR 52](https://www.hnra.org.cn/nd.jsp?id=234) 2021-02-08 17:52

AMSAT: www.amsat.org

- [Keith's Ham Radio](https://qsl.net/kb1sf/)
- [For Beginners](https://www.amsat.org/introduction-to-working-amateur-satellites/)
- [The AMSAT Journal](https://launch.amsat.org/The_AMSAT_Journal)
- [Catching Satellites on Ham Radio](https://makezine.com/article/science/catching-satellites-on-ham-radio/)

<p><img width="706px" src="https://foruda.gitee.com/images/1668868047348209453/fe934566_5631341.jpeg" title="454AMSATbeginners.jpg"></p>

我考古的运气还不错，找到了一个老电台。直接上笔记，就不转载啦。

# [走近业余卫星通信| 你的第一次业余卫星通信：它比你想象中更简单](https://www.hnra.org.cn/nd.jsp?id=242)

发表时间：2021-02-01 18:00

兴趣爱好的广泛涉及是业余无线电通信最显著的特点之一。如果你对其中某一类探索失去了兴趣，总会对其他新领域产生尝试的兴趣。六十年转瞬即逝，业余无线电新技术层出不穷，但业余卫星通信始终是最有趣的事。

刚开始接触业余卫星通信之前（爱好者们通常称业余卫星为“鸟”），了解如何发现和跟踪卫星是很重要的。作者发表系列文章旨在简要介绍跟踪和操作卫星的基本概念、业余无线电爱好者操作卫星通信的方式，并对如何开展业余卫星通信提供指导和帮助。对于初学者，本文将以AO-51业余卫星为例（爱好者们称相对容易跟踪的通信卫星为easier-to-operate卫星，缩写为EZ sats），带您走进最活跃、最精彩、最具挑战的业余卫星通信领域。

![输入图片说明](https://foruda.gitee.com/images/1668850985601209370/73dc9894_5631341.png "屏幕截图")

> 图1  被称为“Echo”的AO-51放置在运载火箭前级。地点：拜科努尔发射场。（AMSAT提供）

爱好者们既会对利用无线电设备通过卫星收听信号或交谈感到神秘和敬畏,也会因为错误操作或者无法通信等问题产生担忧。早期，仅有一两颗业余卫星（OSCARs, Orbiting Satellites Carrying Amateur Radio)在轨运行，爱好者们需要花费大量时间和精力发现、跟踪卫星的运行轨迹。

截至2010年，轨道上已有20多颗活跃的业余卫星，国际空间站（ISS）在轨实验室也搭载了业余无线电设备，许多新一代的业余卫星正在研发或制造、测试中。可以肯定地说，现在利用业余卫星实现通信的机率比以往更大。

### 跟踪业余卫星

想要收听业余卫星或利用它进行通信，首先需要知道跟踪的卫星何时在地面站可视范围出现。目前，业余卫星爱好者们都可以接入互联网，直接通过数字终端跟踪卫星。

各种操作系统都有共享或付费的卫星跟踪软件，许多业余无线电网站还提供在线跟踪，跟踪卫星变得轻而易举。但精确掌握卫星运行轨迹，则需要爱好者对源于卫星运行轨迹的观测数据，熟练使用开普勒轨道根数（简称“Keps”）。

北美防空司令部（NORAD）承担跟踪在轨卫星工作，并授权美国国家航空航天局（NASA）定期公布非涉密卫星轨道信息（信息按卫星编号排列，包含用于描述并确定卫星运动轨道的形状、大小、轨道面的空间位置以及卫星在其轨道上位置的参数）。

现在，爱好者不需要掌握轨道力学（开普勒定律）的复杂细节，通过在卫星跟踪软件中输入所在位置的经纬度、当前时间和开普勒轨道根数后,程序将解决复杂的轨道运算，实现实时轨道预报和卫星跟踪。

开普勒轨道根数会随时间变化而失效，因此可靠获取最新轨道根数是爱好者的首要任务。业余无线电网站大都会发布Keps。例如， AMSAT网站可以下载各种格式Keps文件，网址：www.amsat.org/amsat-new/tools。对于AO-51和ISS这种容易跟踪的“EZ sats”而言，一些网站甚至会提供嵌入式在线跟踪功能，即输入地球站经纬度或Maidenhead区块坐标（MaidenheadGrid Square），就能提供被关注卫星进入地球站可视范围的时间信息。 

![输入图片说明](https://foruda.gitee.com/images/1668851052498670832/ae3af169_5631341.png "屏幕截图")

> 图2 Chuck Green（N0ADI）正在做AO-51发射准备。地点：拜科努尔发射场（N0ADI提供）

### 信标  

掌握被关注卫星进入地球站可视范围时间信息后，下一步是接收该卫星信标。卫星信标大多包括一个或多个频率、幅度固定的信号，主要功能是显示卫星在空间中自身位置,以利地球站对卫星进行搜索、捕获和跟踪,以及轨道测量。

卫星信标有莫尔斯电码，各类数字信号等多种信号模式。一般情况下，信标频率大多以固定功率在卫星下行频段的低端或高端发射。设置和校准地球站天线及其他设备时可作为绝佳参考值。

大多数卫星通过信标向地球站既发送卫星状态信息，又发送遥测信号，有些卫星还能够发送转发器时间信息。大多数FM卫星（包括AO-51）的单通道下行链路就是信标本身。

### 转发器

通信卫星转发器是用于接收和转发卫星通信地球站发来的无线电信号，实现地球站之间或地球站与卫星之间通信的设备，可对接收的信号进行放大、变频，并再次发射回各卫星通信地球站或卫星，其功能类似于调频中继器。与地面调频中继器不同的是，大多数业余卫星转发器有严格对应的上下行频率，在完全不同的业余频段上接收和转发信号。简言之，业余卫星类似“挂在天上的变频中继器”。

卫星轨道运行使其转发的信号带有显著的多普勒效应。就像火车驶来时，鸣笛声的波长被压缩，频率变高，因而声音听起来纤细；火车远离时，声音波长被拉长，频率变低，声音听起来雄浑。通信期间，当卫星接近时，上、下行频率大于标称值；当卫星过顶后，上、下行频率逐渐降低。

例如AO-51，采用弯管转发器，在某个频率上接收信号并即时在另一个频率上通过下行信号转发回地面。AO-51搭载有不同类型的转发器，本文将以调频语音转发器为例，指导初学者进行业余卫星通信。

### 通信模式

业余卫星运行早期，常用一个或几个字母来表示卫星转发器的模式。例如，卫星的上行频率波长2米，下行频率波长为70厘米，则该卫星的工作模式被称为“模式J”；上行频率波长70厘米，下行频率波长为2米，则称为“模式B”。

目前，很多卫星都搭载了多模式的转发器，会出现类似用频段首字母的代号体系。例如，卫星的上行频率在UHF频段，下行频率在VHF频段，原来的“模式B”变为“模式U/V”。同理，原来的“模式J”现在变成了“模式V/U”。AO-51的转发器上行频率波长2米，下行频率波长70厘米，属于“模式V/U”或“模式J”。 

![输入图片说明](https://foruda.gitee.com/images/1668851106467959258/4069ccbd_5631341.png "屏幕截图")

> 图3 作者夫人Kate Baker（KB1OGF/VA3OGF）使用建伍 TH-78A双频段对讲机和MFJ 1717扩展型“rubber duck”天线，通过AO-51开展通信。在卫星过顶时段，对讲机输出功率5瓦，能完成通信。（地点：密歇根休伦湖畔。）

### 转发时间表

通常业余卫星管理者会提供转发时间表。例如，AO-51有多个转发器，通信前核对时间表尤为重要。可在AMSAT网站AO-51主页查询，点击“查询时间表”链接并查找“FM转发器”频率，网址：www.amsat.org /amsat-new / echo / index.php。转发器大多转发的是FM语音，慢扫描电视（SSTV），低功率（QRP）调频或数字通信信号。通过查看时间表，爱好者可以确定转发时间、发射频率、解调模式。此外，在时间表中还能发现其他安排。例如，“大学卫星之夜”或AO-51的管理者何时调整卫星角度更好覆盖南半球等。 

![输入图片说明](https://foruda.gitee.com/images/1668851160604844834/59ab17c6_5631341.png "屏幕截图")

> 表1 部分“EZ-sats” FM卫星上、下行频率

### 设备

开展业余卫星通信既不需要超级强大的FM收发信机，也不需要大尺寸天线。爱好者有时仅用双频段对讲机和“rubber duck”天线就能成功实现业余卫星通信。“EZ sats”这类卫星UHF频段转发器下行输出功率很低（通常小于1瓦），爱好者可以采用提高天线接收增益的方式，有效提升下行链路信号接收的成功率。

一些爱好者用金属衣架安装在木头上制作八木卫星天线。但大多数AO-51通信爱好者，都在使用“Arrow II”型手持式卫星天线。 

![输入图片说明](https://foruda.gitee.com/images/1668851188572915488/ace5715a_5631341.png "屏幕截图")

> 图4 作者正用一台建伍TH-78A双频段对讲机和一副轻量化天线通过AO-51进行通信。在空旷、无遮挡的地带，只输出功率5W也能在FM卫星刚出现在地平面以上进行通信。  **地点：** 密歇根州休伦湖畔 (KB1OGF 提供)

大多数业余卫星采用“全双工”传输通信方式，通信数据可以上行接收和下行转发同时传输，地球站采用这种方式通信同样便于通信传输。例如，全双工模式下，地球站发射的上行信号，可以立刻在下行链路接收。

即使没有全双工电台，爱好者仍然可以通过使用两部电台的方式开展卫星通信：一部电台向卫星发射上行信号，另一部电台接收卫星下行信号。如果天线接收增益高，甚至可以考虑使用对讲机或VHF / UHF频段扫描式接收机接收下行信号。

### 设置电台

为解决多普勒频移的影响，还需要考虑设置电台频率。如果电台有编辑存储功能，建议先存储一、两个略高于和稍低于卫星管理者发布的上下行信号频率，当卫星靠近和远离地球站时可以使用。

例如， AMSAT网站发布的AO-51转发器下行信号频率435.300 MHz，建议先编辑存储两个略高的频率435.320 MHz、435.310 MHz和两个稍低的频率435.290 MHz、435.280 MHz；同样如果时间表中上行信号频率为145.920MHz，建议先编辑存储上行信号频率        略高的145.925MHz、145.930 MHz和稍低的145.915 MHz、145.910 MHz。

类似地面中继器，部分FM卫星上行链路信号输入卫星转发器时，要求发送CTCSS系统信令。所以，在发射信号前，一定要明确通信卫星是否需要发送CTCSS系统信令。目前，AO-51需要67 Hz的 CTCSS系统信令。

随着频率的升高，多普勒频移效应更加明显。在运动的波源前面，波被压缩，波长变得较短，频率变得较高（蓝移blue shift）；在运动的波源后面时，波长变得较长，频率变得较低（红移red shift）。根据经验，波源速度越高，产生的多普勒效应越大。因此，在已设定频率上切换接收频率就能保证卫星靠近和远离地面站时持续接收下行信号。

### 功率：多大就够了？

功率大小是相对的，取决于使用同一转发器的用户数量、天线系统的上行增益，以及卫星过顶或刚出地平线时与地面站的距离。通常情况下，5瓦功率的对讲机和本文图片中的天线能够在“无竞争”的条件下有效和AO-51通信。

竞争”是因为卫星更像是安装在800公里高塔上的地面中继器，因为一个卫星转发器只有一个频率，通信可能异常繁忙。用户数量不多时，5瓦功率双频段对讲机和扩展型“rubber duck”天线足以在卫星接近过顶时实现快速通信，但周末可能50瓦发射功率电台配八木卫星天线都无法覆盖大功率上行信号。

### 卫星覆盖区

理论上卫星处于地平线时，可以实现通信，但需要更大功率的电台，更高增益的天线。“卫星覆盖区”是指卫星信号所覆盖地面的区域。对低轨卫星（LEO）而言，地面上两点能同时观察到同一颗卫星的区域范围可能是整个北美的地理面积。

例如，美国东海岸的一些业余通信爱好者可以利用AO-51和西欧进行通信，或者在美国西海岸和夏威夷通信。但这种情况只有在一方处于卫星覆盖区边缘，而另一方处于覆盖区的另一边缘时才能成功，并且因覆盖区移动速度过快，无法维持稳定通信。

极轨卫星通常有三次操作机会，可实现一天两次通信。过境时间（处于卫星覆盖区的时间）7-15分钟不等，具体通信时长取决于卫星过境时，地球站是在覆盖区还是部分在覆盖区范围。

很多卫星可以覆盖整个大西洋。一些卫星通信爱好者，使用厂家出售或自制的手持式八木天线，配合对讲机，在轮船甲板上实现通信。 

![输入图片说明](https://foruda.gitee.com/images/1668851284735798431/fe4b6b6a_5631341.png "屏幕截图")

> 图5 发射前夕，AO-51和其他卫星置于运载火箭前级。AO-51为图片下方的立方体卫星，两边是六棱柱形的意大利UniSat-3卫星和长方形SaudiSat-2卫星。（AMSAT提供）

### 过境预测

访问AMSAT网站，可以下载AO-51过境时间在线预测数据，网址：www.amsat.org/amsat-new/tools/predic。选择AO-51，输入经纬度或Maidenhead区块坐标，点击“预测”按钮，过境时间（UTC）和过境方向就会呈现出来。点击AO-51位置链接，可以查看其基于地图的运行轨迹。

表格中缩写AOS表示“Acquisition of Signal”，指卫星第一次出现在地平线；LOS表示“Loss of Signal”，指卫星消失在地平线。方位角和仰角都基于360度罗盘表示。

观察运行轨迹发现AO-51会重复、规律地经过地球站所在位置。当卫星在近极地轨道（卫星轨道通过地球南北极）运行时，随着地球缓慢旋转，一天内卫星多次覆盖到地球上每个点。观察AO-51和其他极轨卫星的运行轨迹能发现卫星连续三次，每间隔90分钟从北向南或从南向北过境，一次偏东，一次接近过顶，一次偏西。12小时后，相同的三次连续过境将从相反方向再次运行。

第一次尝试业余卫星通信，先在列表中选择超过45或50度的卫星通信成功概率较大。 

![输入图片说明](https://foruda.gitee.com/images/1668851359324243073/57d21d0b_5631341.png "屏幕截图")

> 图6  2004年6月29日，AO-51搭载的改造版俄罗斯ICBM火箭发射升空。地点：拜科努尔发射场（AMSAT提供）

### 收听什么？

根据AOS时间，在空旷地带打开电台，将其频率设置为稍高于所公布的卫星上、下行信号频率，然后等待卫星出现在地平面上。如果使用八木天线，在水平方向瞄准预测的AOS方向，前后水平移动天线，开启电台静噪，电台安静时，意味着爱好者能够收听到800公里外的AO-51了！

卫星不仅由于快速接近和远离导致多普勒频移，它在轨道上还会缓慢旋转导致交叉极化现象，信号会显著衰减。当卫星信号衰减或语音模糊时，尝试调高或调低下行频率、旋转或移动天线，获得更好的极化匹配。

卫星过境时，可能听到业余无线电爱好者说“你好”，或者交换他们的Maidenhead区块坐标。大部分AO-51的对话都是类似于高频远距离通信（HFDX）的“你好-再见”，长时间占用上行链路在FM卫星通信中是不受欢迎地。

### 表演时间！

使用全双工电台尝试业余卫星通信时，需要确保扬声器与麦克风分离。全双工模式下，当麦克风和扬声器距离很近会产生啸叫，并通过卫星传播！

准备好尝试卫星通信后，只需等待卫星语音暂停的瞬间，飞快按下呼号，注意避免发射CQ信号。等待在下行链路收听到自己发出的信号，表示爱好者已经建立起卫星通信连接。

### 附录：使用正确的天线

一部电台，功能与价格成正比，爱好者并不需要投入过多资金成本，有些关于天线的小知识，可以帮助你更高效地与“EZ sats”通信。

“EZ sats”输出功率一般仅有1瓦多。为了延长卫星电池的使用寿命，卫星管理者经常将其下行输出功率降至0.5瓦以内。为了更高增益，特别是卫星通信位于地平线附近时，可以使用VHF/UHF垂直天线。

另一种高可靠性的天线是FM卫星通信者使用的双频段可旋转3~4单元八木天线。当卫星从地平面出现时，将天线波束对准卫星并随之转动；当卫星过顶后，反转天线跟踪卫星，这种操作适用于卫星过境俯仰角在45度左右时的情况。

安装在方位俯仰旋转器上的高增益、3~4单元八木天线适用于卫星过境全程通信。但与“EZ sats”通信时，并不需要这种天线。

 **编译：李英华 纽莉荣 戴慧玲 张宁** 

### 关于作者

Keith Baker于1976年首次获得业余无线电操作认证，呼号为KB1SF/VA3KSF，持美国高级执照和加拿大高级执照，担任北美AMSAT组织前任主席和现任财务总管。1994-1998年任AMSAT执行副总裁，1994-2003年任AMSAT董事会成员。除《AMSAT期刊》，在《ARRL手册》《ARRL卫星选集》《QST杂志》《CQ杂志》《CQ VHF》《CQ-Ham电台（日本）》和《OSCAR新闻（英国）》上也发表文章和图像资料。

曾出版专著《How to Use the Amateur Radio Satellites》，该著作自1990年以来由北美AMAST组织出版数个版本，之后KeithBaker将此书的成果捐赠予世界其他AMAST组织。目前，该著作已被翻译成五种语言出版发行。

Keith Baker现为《监测时报》撰写“sky surfing”季度专栏，并为北美AMAST组织的出版物《AMSAT:The First Forty Years》撰写部分内容，同时担任ARRL新版《卫星手册》编辑。现与妻子（KB1OGF/VA3OGF）、女儿Emily居住于加拿大安大略省科伦纳小镇。 

业余卫星通信概述 by Keith Baker, KB1SF/VA3KSF, kb1sf@amsat.org

本文原题为《开启业余卫星通信之旅：它比你想象中更简单》发表于《监测时报》（Brasstown, NC 28902） 

# Keith's Ham Radio 

https://qsl.net/kb1sf/

I'm Keith Baker  
KB1SF  VA3KSF  VA3OB  
A US Citizen by birth, I was first licensed as an Amateur Radio Operator in Ohio (USA) in 1976 and was issued the call sign WD8CMU. From 1978 to 1988, I operated my station with a variety of subsequent call signs including KA5CCO (Texas), KC0YU (Nebraska), and IV3KBU (Italy).  

I was issued my present US call sign (KB1SF) in 1985 and currently hold an Extra Class license authorized by the US Federal Communications Commission.  

I was issued my first Canadian call sign (VA3KSF) in 2004. I was issued my second Canadian call sign (VA3OB) in 2013.  I currently hold the Basic, Morse Code and Advanced Amateur Radio qualifications authorized by the Canadian Government. 

I'm a retired Board of Directors Member, Treasurer, Executive Vice President and President of the Radio Amateur Satellite Corporation (AMSAT). 

![输入图片说明](https://foruda.gitee.com/images/1668853178275350901/5b26ee1b_5631341.png "屏幕截图")

# [For Beginners](https://www.amsat.org/introduction-to-working-amateur-satellites/)

			
<div class="entry-content" itemprop="text">
<p style="text-align: justify;">Here for you to freely download is a compendium of “getting started” articles written by Keith Baker, KB1SF/VA3KSF. These articles appeared over the course of several editions of <em>The AMSAT Journal</em> from 2010 to 2011.</p>
<p style="text-align: justify;">Unfortunately, because both satellites (and Web addresses!) have a finite lifetime, information such as this can quickly be overcome by events.&nbsp;For example, the AO-51, AO-27 and VO-52 satellites referred to in these documents are&nbsp;no longer operational and several of the Web links he mentions no longer point to active Web pages. However, despite these (minor) shortcomings, the tools and techniques outlined in Keith’s beginner series are still very much applicable to operating on current and future AMSAT satellites.</p>
<p><a title="Getting Started Part 1" href="http://www.amsat.org/wordpress/xtra/Getting%20Started%201.pdf" target="_blank" rel="noopener">Getting Started Part 1</a></p>
<p><a title="Getting Started Part 2" href="http://www.amsat.org/wordpress/xtra/Getting%20Started%202.pdf" target="_blank" rel="noopener">Getting Started Part 2</a></p>
<p><a title="Getting Started Part 3" href="http://www.amsat.org/wordpress/xtra/Getting%20Started%203.pdf" target="_blank" rel="noopener">Getting Started Part 3</a></p>
<p><a title="Getting Started Part 4" href="http://www.amsat.org/wordpress/xtra/Getting%20Started%204.pdf" target="_blank" rel="noopener">Getting Started Part 4</a></p>
<p><a title="Getting Started Part 5" href="http://www.amsat.org/wordpress/xtra/Getting%20Started%205.pdf" target="_blank" rel="noopener">Getting Started Part 5</a></p>
<p><a title="Getting Started Part 6" href="http://www.amsat.org/wordpress/xtra/Getting%20Started%206.pdf" target="_blank" rel="noopener">Getting Started Part 6</a></p>
<p><a title="Getting Started Part 7" href="http://www.amsat.org/wordpress/xtra/Getting%20Started%207.pdf" target="_blank" rel="noopener">Getting Started Part 7</a></p>
<p><a title="Getting Started Part 8" href="http://www.amsat.org/wordpress/xtra/Getting%20Started%208.pdf" target="_blank" rel="noopener">Getting Started Part 8</a></p>
<p><a title="Getting Started Part 9" href="http://www.amsat.org/wordpress/xtra/Getting%20Started%209.pdf" target="_blank" rel="noopener">Getting Started Part 9</a></p>
<p>More useful information for beginners is also available on the AMSAT <a href="http://www.amsat.org/?page_id=2144">Station and Operating Hints page</a>.</p>
<p>Frequency and operational information for current satellites can be found on the AMSAT <a href="https://www.amsat.org/two-way-satellites/">Communications Satellites page</a></p>
<p>More detailed information can be found in AMSAT’s&nbsp;<em>Getting Started with Amateur Satellites&nbsp;</em>book which can be purchased on the <a href="https://www.amsat.org/product-category/amsat-books-and-dvds/">AMSAT store</a></p>
<p>&nbsp;</p>
</div>

</div>

## Radio Amateur Satellite Corporation

<p>AMSAT<br>
712 H Street NE, Ste 1653<br>
Washington DC&nbsp; 20002<br>
<a href="mailto:info@amsat.org">info@amsat.org</a></p>

# [Catching Satellites on Ham Radio](https://makezine.com/article/science/catching-satellites-on-ham-radio/)

![输入图片说明](https://foruda.gitee.com/images/1668854493311975056/11324a86_5631341.png "屏幕截图")

My favorite ham activity is making contacts via satellites. Not only is there the romantic notion of sending messages into outer space, but you have to trace the orbit of the satellite with your antenna while tuning the radio, to compensate for the Doppler effect.

The satellites AO-51, SO-50, and AO-27 orbit the Earth acting as repeaters. Repeaters are automated relay stations that allow hams to send signals over a greater distance using low-power hand held transceivers. The satellites allow hams to relay messages from Earth to space and back to other hams somewhere on the planet. The International Space Station (ISS) also has a repeater, but occasionally, if you’re lucky, the astronauts turn on their radios to make contact directly with hams on the ground.

The following instructions will get you started listening to birds (satellites) on FM, which can be done with a simple VHF/UHF FM radio with a whip antenna, without the need of a ham license. For better coverage, you can use a Yagi antenna (like the one pictured above) connected to a mutli-mode radio and a license (if you want to transmit). A Yagi antenna can also be used to improve the signal of your hand held radio.

### Materials

All you need is a VHF/UHF FM receiver (like a police scanner) or a VHF/UHF transceiver (like a Yaesu VX-7) and an antenna.

1. Specifying your location

Start by visiting [Heavens-Above.com](http://www.heavens-above.com/) to check the orbit of the satellite you want to listen to and specify your location.

2. Specifying a satellite

Check the passes of your specific satellite or the ISS. AO-51, SO-50, AO-27, ISS. Make sure that the passes are shown for your correct location.

![输入图片说明](https://foruda.gitee.com/images/1668868252717228587/5d5b12b8_5631341.png "satelliteschart.jpg")

3. Reading the chart

This pass chart shows the Start (when/where the satellite enters on the horizon), the Max. Altitude (when/where the satellite is at its highest point in the sky), and the End (when/where the satellite finishes it’s pass). Alt. is the altitude, the angle of the satellite from the observer’s horizon. 0 degrees is exactly on the horizon, and 90 degrees is directly above the observer. Az. is the Azimuth, the cardinal direction of the satellite from the observer’s point of view.

4. Picking a good pass

Satellites orbit the Earth at all sorts of angles, some that are very close to the horizon and some that are directly overhead. It is much easier to hear a satellite that passes directly overhead. To find a good sat pass, check the Max. Altitude Alt. for a pass that is 45 or higher (the higher the better). In our example, the second pass at 7:28 looks like a good one since the Max. Altitude Alt. is 77. The first pass at 5:52 has a Max. Altitude Alt. of only 12 which is very close to the horizon and difficult to pick up.

5. Finding the frequency

Satellite repeaters work with two different frequencies, an uplink and a downlink. You will listen to signals received on the downlink. If you wish to transmit, you’ll need to program in the uplink frequency as well. Follow the corresponding links to find the FM repeater frequencies of the satellites. The frequencies often change, so be sure to check the websites for the latest updates. AO-51, SO-50, AO-27, ISS. Tune your radio to the downlink frequency and you’re ready to go outside and listen (example: 435.300 MHz FM).

![输入图片说明](https://foruda.gitee.com/images/1668868285945048687/758b2c38_5631341.png "satellitewhipantenna copy.jpg")

6. Aiming a whip antenna

If you’re using a whip antenna, you will not aim the antenna directly at the satellite. Instead, you’ll keep it perpendicular to the satellite. You can rotate the antenna by rotating your wrist to try and get a clearer signal.

7. Following the pass with the antenna

You will trace the path of the satellite orbit with the antenna using the Heavens-Above pass chart as a guide. At the Start Time, start with the antenna perpendicular to the Az. direction at the given Alt. For example, at 7:28, aim the antenna perpendicular to north at 10 degrees above the horizon. Trace the path of the satellite so that at the Max. Altitude Time the antenna is pointed in the corresponding location. For example, at 7:33, the antenna should be perpendicular to west northwest at 77 decrees above the horizon. Finish tracing the path of the satellite so that at the End Time the antenna is perpendicular to the corresponding location. For example at 7:39, the antenna will be perpendicular to south southwest at 10 degrees above the horizon. It can be very difficult trying to catch the satellites and you may spend a lot of time not hearing anything. As you trace the general path of the satellite with the antenna, move the antenna around in small side to side and up and down motions until you hear a bit of audio. Adjust the antenna to make the audio clearer.

8. Tuning the radio for the Doppler effect

The Doppler effect makes the frequency vary by .010 MHz. As you trace the path of the satellite with the antenna, you will also need to tune the radio back and forth plus or minus .010 MHz until you hear a good signal. Early in the pass, you will add .010 MHz, for example, if you’re listening on 435.300 MHz, you’ll need to tune the radio back and forth between 435.300 MHz and 435.310 MHz. Later in the pass, you will subtract .010 MHz, for example, you will tune the radio back and forth between 435.300 MHz and 435.290 MHz.

[Here is an audio clip](https://cdn.makezine.com/make/audio/satellites_ham_radio.mp3) from my first satellite contacts. The contacts seem to be going pretty slowly, but while I was making them, I remember everything happening very quickly. It was a lot to tune the radio and maneuver the antenna while trying to write down the call signs of the contacts.

---

【[笔记](https://gitee.com/yuandj/siger/issues/I4UC12#note_14488613_link)】

The AMSAT Journal

https://launch.amsat.org/The_AMSAT_Journal

![输入图片说明](https://foruda.gitee.com/images/1668854205263378931/e944ae46_5631341.png "屏幕截图")

https://docslib.org/doc/2564696/handbook-on-amateur-and-amateur-satellite-services

![输入图片说明](https://foruda.gitee.com/images/1668854424347679940/b4154ca3_5631341.png "屏幕截图")

[Getting Started With Amateur Satellites by G. Gould Smith](https://www.goodreads.com/book/show/14761609-getting-started-with-amateur-satellites)  
GoodreadsGoodreads|1200 × 630 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1668854550986081619/4cacbd70_5631341.png "屏幕截图")

srrc.org.cn · 3月 22, 2021

- 中心科普 | 《全球业余卫星介绍》（四）中国业余卫星（上 ...
  
  > 卫星业余无线电的信标首先被用于地面业余无线电爱好者跟踪卫星的运行。 其次，卫星信标信号通常包含有反映卫星工作状态的遥测数据，业余无线电爱好者通过解码遥测数据可了解卫星的工作情况。 2010年和2011年举办的前两届全国青少年业余无线电卫星通信锦标均使用了我国的第一颗业余卫星HO-68（XW-1）。 （2）青少年科普活动 2016年，11月10日早上7点42分，我国第一颗由中学生参与研制的科普卫星—“丰台少年一号”暨“少年梦想一号”卫星在酒泉卫星发射基地搭载长征11号运载火箭成功发射升空。 我国成为继美国、俄罗斯、法国、日本、印度之后第6个发射由青少年参与研制卫星并顺利升空的国家。

  www.srrc.org.cn/article24576.aspx

- 中国青少年科普卫星工程“西柏坡号”科普卫星(八一 02 星 ...  
  www.sastind.gov.cn/n112/n117/c6809769/content.html

  > 2020-7-3 · “中国青少年科普卫星工程”是在国家航天局、中国航天科技集团有限公司等单位的指导支持下，由中国航天科技教育联盟实施的一项科普工程。 该工程旨在国家基础教育 …

- 中心科普 | 《全球业余卫星介绍》（一）业余卫星概述 | 中国 ...  
  www.srrc.org.cn/article24519.aspx
  
  > 2021-3-22 · 1）业余无线电卫星组织（AMSAT，The Radio AmateurSatellite Corporation）：作为一个非盈利的科研教育组织，于1969年在美国华盛顿哥伦比亚特区 …

- 希望一号：中国首颗为青少年无线电爱好者发射的卫星  
  https://baijiahao.baidu.com/s?id=1685441301267551894

  > 2020-12-8 · 这是我国的首颗青少年科学实验卫星，同时也是我国的首颗业余通信卫星。 它的主要任务是： ①搭载青少年科学实验方案竞赛优秀作品； ②建立业余无线电频率的空间 …

- 中心科普 | 《全球业余卫星介绍》（一）业余卫星概述 ...  
  www.hljrm.cn/jscs/3169.jhtml

  > 2021-3-22 · 1）业余无线电卫星组织（AMSAT，The Radio AmateurSatellite Corporation）：作为一个非盈利的科研教育组织，于1969年在美国华盛顿哥伦比亚特区 …

- 第53次！我国又发射一枚火箭，连中小学都有自己的卫星了  
  https://new.qq.com/omn/20211226/20211226A093ED00.html

  > 2021-12-26 · 小编猜测就是就是就是“一零一中学”科普小卫星。 而在此前的2009年12月15日，我国第一颗业余无线电卫星“希望一号”，在太原卫星发射中心搭载长征二号丙运载火 …