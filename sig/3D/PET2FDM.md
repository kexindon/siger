- [你也可以用旧饮料瓶制作FDM耗材3D打印](https://www.bilibili.com/video/BV1iR4y1K7TD)
- [回收PET饮料瓶制作3D打印耗材还能染色](https://www.bilibili.com/video/BV1J3411u7aH)

<p><img width="706px" src="https://foruda.gitee.com/images/1661518304555304900/30d4232c_5631341.jpeg" title="251PET2FDM.jpg">

第一眼看到这个作品的时候，“大师也用3D打印笔创作吗？”，我和小袁都惊叹这个神奇的艺术品，颇有几分鸟巢的味道。这种新奇的造型通常都是数码作品的标志，如今成为手艺，颇有几分惊讶，是老艺术家赶新潮，还是新派小生的时代创作。不管答案如何？这期封面是有了。这个FDM增材的PET瓶线材，才是本期主角。之所以会看重 PET 线材，是火焰棋实验室与 PET 瓶的故事使然，此处省略1万字，等我亲手造一套 PET 线材机，并创作出经验的作品之时，再行分享吧。还是感谢南极人用心分享，视频搬运加整理，配文字幕生动有趣。果断转载，995篇作品的笔更，可以看出是真心的。

# [你也可以用旧饮料瓶制作FDM耗材3D打印](https://www.bilibili.com/video/BV1iR4y1K7TD)

自己使用旧饮料瓶制作3D打印耗材  
确实有很多玩家很感兴趣  
不过南极熊还是先强调一下  
这个耗材拉丝和打印  
PET饮料瓶的过程  
存在健康风险  
需要谨慎对待  
做好必要的防护  
南极熊获悉  
国外网友 Ond ej raitr  
免费分享了他设计的 Petamentor  
饮料瓶回收拉丝装置  
该方案相对简单易操作  
同时成本低廉  
据他介绍  
整套方案的成本在 50刀以下  
并且使用的均为容易获取到的通用配件  
其主要构成组件  
为用于拉丝温度控制的  
W1209温控器  
用于控制拉拔速度的 PWM 控制器  
廉价的 12V 5A 电源  
缠绕完成耗材拉丝的拉拔器  
以及组合以上所有组件的木质平台  
整套方案需要用的所有 STL 模型文件  
都可免费下载  
详细的制作教程  
Ond ej raitr  
也通过项目网页和视频的方式  
分享了出来  
如果你想亲手做一个这样的  
PET 回收拉丝装置  
可以来要相关资料了  
南极熊感谢你的观看

<p><img width="23.56%" src="https://foruda.gitee.com/images/1661517896196728562/e06d894e_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517909326828369/029c4ce0_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517923415355325/583f0b92_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517937631921797/84121e77_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517952823997223/da435378_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517966702824948/870685fa_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517981575573708/1105e3b2_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661518002209853742/9f98639e_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661518015814340644/29c35b0d_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661518029922772053/9aab0756_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661518042492179440/3a7d0449_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661518054844865802/fda049d4_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661518068113024920/3d09736c_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661518081813046946/cb0a9778_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661518094878711698/2827c637_5631341.jpeg"> <img width="23.56%" src="https://foruda.gitee.com/images/1661518107111522330/bda39e54_5631341.jpeg"></p>

| title | len |
|---|---|
| [你也可以自己使用旧饮料瓶制作FDM 3D打印耗材啦](https://www.bilibili.com/video/BV1iR4y1K7TD) | 01:15 |
| [超简单的实用方法解决回收饮料瓶切割成条的问题](https://www.bilibili.com/video/BV1N3411K73V) | 00:50 |
| [3D printing bottle filament machine - PETamentor2 instructions and suggestions](https://www.bilibili.com/video/BV1f34y1J7WB) | 07:53 |
| [Part 1 Petamentor - Platform平台](https://www.bilibili.com/video/BV1AS4y1a7RN) | 08:26 |
| [Part 2 Petamentor - Puller拉拔器](https://www.bilibili.com/video/BV1NA4y1X7FW) | 08:10 |
| [Part 3 Petamentor - Power Supply电源模块](https://www.bilibili.com/video/BV1m34y1Y7Xc) | 07:28 |
| [Part 4 Petamentor PWM controller with diagram控制器与图表](https://www.bilibili.com/video/BV1mF411M7sg) | 07:59 |
| [Part 5 Petamentor W1209 Thermocontroller温控器](https://www.bilibili.com/video/BV1TT4y1r7Kf) | 08:04 |
| [Part 6 Petamentor XH - B310 Thermometer温度计](https://www.bilibili.com/video/BV1T34y1e7M7) | 03:52 |
| [Part 7 Petamentor Hotend and W1209 Settings 热端和W1209设置](https://www.bilibili.com/video/BV13F411u7p6) | 03:20 |
| [Part 8 Petamentor The Nozzle喷嘴](https://www.bilibili.com/video/BV1UZ4y117Kk) | 06:09 |
| [Petamentor bottle cutter and first run 切瓶机和首次运行](https://www.bilibili.com/video/BV1NY411P7yd) | 08:16 |
| [Petamentor STL files important update 😁stl文件重要更新](https://www.bilibili.com/video/BV1rS4y1c7p8) | 02:41 |
| [3D Printing PET bottle filament machine PETamemtor2](https://www.bilibili.com/video/BV15v4y1M7t8) | 03:01 |

# [回收PET饮料瓶制作3D打印耗材还能染色](https://www.bilibili.com/video/BV1J3411u7aH)

回收饮料瓶拉丝制作的3D打印线材  
还能这样染色吗？

南极熊获悉

专门使用PET饮料瓶的  
3D打印博主 mr3dprint  
展示了他给PET耗材丝染色的方法  
这种方法像极了小时候  
水彩笔快没水了  
却还要拆开彩色笔使用笔芯涂色的感觉  
没错他就是使用PET制作的丝材  
穿过彩色笔笔芯的方法给耗材染色  
出乎意料的是  
这样的方法  
实际打印的色彩表现居然还不错  
这甚至让南极熊怀疑  
这到底是不是真的  
也许油性马克笔  
在PET材料上的附着力足够好  
才能达到视频中  
3D打印模型的色彩表现  
那么问题来了  
这个方法适用于PLA等其他材料耗材吗？  
有没有熊友尝试过？  
南极熊感谢你的观看

<p><img width="23.56%" src="https://foruda.gitee.com/images/1661517214971052515/7905bef7_5631341.png"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517233603334537/8a1790dd_5631341.png"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517604664068596/ca92729e_5631341.png"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517271218688992/f389f476_5631341.png"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517280146207129/a3322283_5631341.png"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517292109967823/04bbfb30_5631341.png"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517301298440877/3659499b_5631341.png"> <img width="23.56%" src="https://foruda.gitee.com/images/1661517311337023979/cb3f6625_5631341.png"></p>