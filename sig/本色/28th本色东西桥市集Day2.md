- 第二十八届【本色东西桥市集】[最新疫情防控公告](http://mp.weixin.qq.com/s?__biz=MzA5ODY5NTMyNA==&mid=2652856281&idx=2&sn=83f03e6e4202d87f837ac173cfbe566b)
- 第二十八届【本色东西桥市集】| [东方美学生活集群](http://mp.weixin.qq.com/s?__biz=MzA5ODY5NTMyNA==&mid=2652857186&idx=1&sn=721abd0cf07207106d112eb3ccc10e59)

<p><img width="49%" src="https://images.gitee.com/uploads/images/2022/0705/000735_0db2c49c_5631341.jpeg"> <img width="49%" src="https://images.gitee.com/uploads/images/2022/0705/000750_a8d51cca_5631341.jpeg"></p>

# 第二十八届【本色东西桥市集】| [东方美学生活集群](http://mp.weixin.qq.com/s?__biz=MzA5ODY5NTMyNA==&mid=2652857186&idx=1&sn=721abd0cf07207106d112eb3ccc10e59)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/001701_a0d8fcf5_5631341.png "屏幕截图.png")

### 市集篇章

- 东方美学生活集群
- 行走的故事
- 本色夜市
- 热西·才让旦本色音乐会【听见喜马拉雅】
- 回香煎茶道【荷风渡夏】茶会
- 音疗休息栈 | 安静的力量
- 小扇流萤 | 苏绣体验课
- 南山叙 | 修缮体验课
- 丹道养生
- 天生艺术家 | 遇见鸢尾花
- 央金 | 茶菓体验课
- 优沉香道 | 沉香品鉴课
- 普洱茶公益分享会
- 行云流水 | 普洱茶品鉴会
- 本色茶水铺
- 无聊猴&维度 · 首展
- 在细雨中呼喊 · 阿初摄影展
- 本色艺术收藏大展

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/001712_5ebecd32_5631341.png "屏幕截图.png")

### 1. 东方美学生活集群

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/001832_c08a4b41_5631341.png "屏幕截图.png")

本色美术馆用最自然的方式生长灌溉，不仅仅是对生态，更是对人文内心的滋养，衍生到生活方式，以这样的理念立足。卖家有好物，买主有看点，也有很多人趁机来看看老朋友，清风拂面，阳光温暖，人间喜乐。除了市集，更有音乐的体验、摊主的聚会、好玩的活动、喝喝茶、逛逛摊、学做美食、手作制品、以物易物。本色美术馆用20年的沉淀，在独特的人文环境中，成就了一个跨界的东方美学生活集群。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/001916_eefa1835_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/001929_064e0f57_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/001935_ee8635bf_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/001941_1ae19576_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/001947_4f743b0b_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/001952_ced67d5e_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/001958_31a48819_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002004_320f9f16_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002010_05f05546_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002015_89028570_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002022_12dacad2_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002029_cc489a88_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002036_83f253f0_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002043_2cf3ae71_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002050_a8d8d7dd_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002057_808e19c6_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002102_daaad4f9_5631341.png "屏幕截图.png")

### 2. 行走的故事

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002110_6c08d528_5631341.png "屏幕截图.png")

夜幕在这里停歇，将一切变得美好，谁曾记得古老的江南夜？没有什么比一瓢饮、一箪食，一曲吟、一隅景更能抚慰夜归人的心灵，月光缥缈的温柔，错落相映，此时的城市喧嚣无比，而本色的夜晚自在安心，醉人的酒，清香的茶，诱惑的美食，荡漾的天籁，络绎的人群，温馨的氛围融入在一片灯光璀璨中，仿佛回到古老的江南梦。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002129_cb2573a3_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002135_8e7bd9b0_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002140_7ff637cc_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002147_2f2bc902_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002153_cbde2701_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002157_9f8b774d_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002206_79be8bfe_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002213_795659a0_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002218_c76f9b71_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002223_887ae029_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002230_a174c9d5_5631341.png "屏幕截图.png")


### 3. 本色夜市

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002237_7029bda4_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002247_ca790d1c_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002254_e71f2459_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002303_4934ad8a_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002308_39da59a7_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002314_1bc4d742_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/002321_d264671d_5631341.png "屏幕截图.png")
