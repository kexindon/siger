- Senet 
- 20 Squares (Aseb) & Royal Game of Ur
- Hounds and Jackals

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0630/201539_266334e3_5631341.jpeg" title="民族棋世界史.jpg"></p>

> 这是第一份没有标准答案的游戏攻略，而作者我称为编委，就是你，正在阅读的同学。有的是精美的穿越世纪的精美棋盘，玩法虽然不能穿越，但留给了后来的学者无尽的想象。今天我邀请你一起来撰写你的玩法，并去介绍给你的小伙伴。我将努力为你们营造一个考古现场，让我们共同穿越到那个时代，带会我们想要的游戏吧。

我将这个过程称为：“从游戏开始学习”。通篇，是各种相关的文献和资料的 “堆砌”，它们尽可能呈现出学习的线索。这更像是一个多结局的剧情游戏，而导演就是你，演员暂且也由你扮演，坐在桌前，注视着桌面上的棋盘（可能是你手绘了一个基础），精美程度完全取决于你头脑中的想象（在你阅读过我给你提供的这些资料。）并最终一幅幅连贯的画面出现在你的眼前，你能够滔滔不绝地开始给同学们讲解它是怎么玩起来的。

> 请你用你擅长的方式，记录下来，你的研究（想象）成果，并分享给你的小伙伴，还有我，我们一起来完成终稿，并写下你的名字在海报上。这是我们共同的成果，就如同之前的成果同样的绚丽。让我们开始今天的探秘吧。

# Senet 

## SENET PPT

塞内特：金字塔海峡  
Strait from the pyramids

### What is SENET ?

- A board game. 棋盘游戏
- Category: race game 赛跑比赛类
- From: Egypt 在埃及被发现
- Age: 3000+ years 有 3000年以上的历史
- The game was found in the pyramids. 它是在金字塔里发现的

### What is it made of? 它的构成？

- The board is made of 3x10 square. 棋盘由 3x10 的正方形组成
- It comes with 10 pegs, 5 of each colour. 10个棋子（钉子）两种颜色各5枚
- Also, 4 sticks are used as dice. 还有4根棍筹，用来当骰子

### Where is SENET from? 源起

古埃及的法老（5000年前，埃及的国王喜欢玩这个游戏），  
这游戏的是埃及王后... 是埃及艳后吧？

- SENET comes from ancient Egypt. 
- Pharaos ( egyptian kings enjoyed playing this game, 5000 years ago. ) 
- Egyptian queen playing Senet... is it Cleopatra ?

### How do we play SENET ? 怎么玩？

当我们在古老的金字塔、坟墓和城市中发现这个游戏时，没有规则可循！！！
- When we found the game in the old pyramids, tombs and cities, no rules came with it !!!

### SO HOW DO WE PLAY??? 哪怎么办呢？

- Start on top left square and follow this path:

  | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 |
  |---|---|---|---|---|---|---|---|---|---|
  | 20 | 19 | 18 | 17 | 16 | 15 | 14 | 13 | 12 | 11 |
  | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 |

  从左上角的正方形开始，沿着数字描述的路径：

### How do you start the game ? 我们怎么开始比萨？

- At the beginning of the game the 5 pawns per player alternate along the 10 first squares.
- 在游戏开始时，每个玩家的5个棋子沿前10个方块交替。
- The pawns move according to the throw of four sticks.
- 棋子随着四根棍子的投掷而移动。

### How do you move ? 怎么个移动法？

Throw all 4 sticks (like a dice) and look at how many painted sides are visible:  

- One painted side : 1 point.
- With two : 2 points.
- With three : 3 points.
- With four : 4 points.
- With none : 5 points.

扔出4根棍筹（像骰子一样），看看有多少涂过的面儿可见：几个面在上算几点儿，没有算5点儿。

### Rules 规则

- When a pawn reached a square already occupied by an opponent pawn, they have to exchange their positions.
- 当棋子到达已经被对手棋子占据的方块时，他们必须交换位置。
- If you cannot move forward, you must move backward. If you cannot, you skip your turn.
- 如果你不能前进，你必须后退。如果你做不到，你就暂停一轮让对手继续。
- The winner is the first to move all of their pawns off the board.
- 获胜者是第一个将所有棋子移出棋盘的人。

### The special squares !!! 特殊的方块

- 15 : House of Rebirth, starting square and the return square for the pawns reaching square number 27 重生之家：落入此方块的棋子还有已经抵达27号方块的棋子，回到起点。
- 26 : House of Happiness, a mandatory square for all the pawns. Safe square. 安全方块：所有棋子都被强制。
- 27 : House of Water, a square that can be reached by the pawns located on squares 28 to 30 which moved back when their throws did not allow them to exit the board. They have to restart from square 15. 水房：如果扔到28或者30，就要被冲到15开始啦。
- 28 : House of the Three Truths, a pawn may only leave when a 3 is thrown. Safe square. 三真之家：一个棋子必须扔到3才能离开棋盘。安全方块
- 29 : House of the Re-Atoum, a pawn may only leave when a 2 is thrown. Safe square. 神殿，一个棋子必须扔到2才能离开棋盘。安全方块

### Sur Internet ? Mais oui ! 互联网？是的

<p><img width="16.6%" src="https://images.gitee.com/uploads/images/2022/0630/184151_372211ae_5631341.png"><img width="16.6%" src="https://images.gitee.com/uploads/images/2022/0630/184138_8a674b5e_5631341.png"><img width="16.6%" src="https://images.gitee.com/uploads/images/2022/0630/184124_f8821412_5631341.png"><img width="16.6%" src="https://images.gitee.com/uploads/images/2022/0630/184109_ec018f7d_5631341.png"><img width="16.6%" src="https://images.gitee.com/uploads/images/2022/0630/184052_b289757a_5631341.png"><img width="16.6%" src="https://images.gitee.com/uploads/images/2022/0630/184036_1094dd71_5631341.png"><img width="19.9%" src="https://images.gitee.com/uploads/images/2022/0630/184021_d0eadccc_5631341.png"><img width="19.9%" src="https://images.gitee.com/uploads/images/2022/0630/184007_19e852c3_5631341.png"><img width="19.9%" src="https://images.gitee.com/uploads/images/2022/0630/183951_0245f1d2_5631341.png"><img width="19.9%" src="https://images.gitee.com/uploads/images/2022/0630/183932_89fe679f_5631341.png"><img width="19.9%" src="https://images.gitee.com/uploads/images/2022/0630/183916_9414812e_5631341.png"></p>

## 塞内特 (Znt) Ludii.China

![输入图片说明](https://images.gitee.com/uploads/images/2022/0630/175309_87ddd744_5631341.png)

- **时期**  古代
- **地区**  北非 , 西亚
- **分类**  板 , 比赛 , 逃生

### 描述

Senet 是世界上已知的最古老的棋盘游戏之一，从大约公元前 3000 年到公元前一千年的埃及都有记载。大部分时间它也在塞浦路斯游戏，并在黎凡特的不同时间游戏。 尽管从未找到完整的规则集，但埃及文本和墓葬画的提示为我们提供了有关其可能玩法的线索。 游戏也充满了宗教意义，因为棋盘本身代表了来世的旅程。

### 规则

在 3x10 板上玩。棋子可以是五个或七个。两名玩家。 用作骰子的四根铸筹（棒）。 Boustrophedon 轨迹 从左上角到右下角。

### 所有规则集

 **学术规则集** 

- 肯德尔 Timothy Kendall 提出的标准规则。
- 肯德尔五件套 每个玩家五件。
- 肯德尔首发 需要投掷 1 才能开始。
- 肯德尔陷阱 广场 27 是一个陷阱。
- 肯德尔本垒打 直到所有棋子都超出本垒时才能承受。
- 肯德尔五段起跑 五件和起跑规则。
- 肯德尔五件套陷阱 五件套和陷阱规则。
- 肯德尔五件本垒打 五件和本垒规则。
- 肯德尔开始投掷陷阱 开始投掷和陷阱规则。
- 肯德尔首发本垒打 首发和本垒打规则。
- 肯德尔陷阱主行 陷阱和本垒规则。
- 肯德尔五件式开始投掷陷阱 五件，起跑和陷阱规则。
- 肯德尔开始投掷陷阱本垒打 开始投掷、陷阱和本垒打规则。
- 肯德尔五件套陷阱本垒打 五件，陷阱，本垒。
- 肯德尔五件首发本垒打 五件，起跑，本垒规则。
- 肯德尔所有选项 五件，起跑、圈套和本垒打规则。
- 鸽子 Peter Piccione 提出的规则。
- 热基尔 Gustave Jéquier 提出的规则。

 **建议的规则集** 

- 钟 RC Bell 的建议规则。
- 简单的塞内特 在没有标记的棋盘上玩。
- 简单标记的塞内特 在带有基本标记的棋盘上演奏。
- 中王国塞内特 在埃及中王国常见的棋盘上玩。
- 早期的新王国塞内特 在早期新王国埃及常见的棋盘上玩。
- 新王国晚期塞内特 在后来的新王国埃及常见的棋盘上玩。
- 垂直塞内特 玩垂直方向的棋盘和不同的标记。
- 双塞内特 在两个相邻的棋盘上玩。
- 塞浦路斯塞内特 在史前塞浦路斯用种子或石头在简单的棋盘上玩。

### 起源

埃及

### 身份标识

DLP.Games.3

BGG.2399

### [BGG 2399](https://boardgamegeek.com/boardgame/2399): & pdfs

- **[BGG.2399](https://boardgamegeek.com/boardgame/2399)**   

  > Senet is an ancient Egyptian board game similar to backgammon. Each player has 5 pawns which travel around a 3 by 10 board in an S shaped path. Movement is dictated by the throw of 4 sticks (black on one side and white on the other) which can produce a 1,2,3,4, or 6, (5 is not possible) depending on how they land.. As in backgammon, the objective is to bear all of one's pawns off first. A couple of interesting differences is that there are safe and trap spaces on the board. Also, players may block each others' progress as in Parchisi. Senet sets are usually constructed from various woods and are often quite beautiful. Senet is one of the oldest boardgames but unfortunately it fell out of use for a few thousand years and no ancient rules set has been discovered. Undoubtably another tragic loss resulting from the Library of Alexandria burning to the ground. As a result scholars has made several educated guesses as to the rules. The most popular versions are in the files section.

    - [Parchisi](https://game-solver.com/parchisi-star/) (Parchis) is a popular board game in Spain and known by different name in other countries. It is a board game of the Cross and Circle family. It is an adaptation of the Indian game Pachisi or Parchisi or Ludo

    - Senet是一种类似于双陆棋的古埃及棋类游戏。每个玩家有5个棋子，沿着S形路径绕着一个3×10的棋盘移动。动作由四根棍子（一边黑，一边白）的投掷决定，这可以产生1、2、3、4或6（5不可能），这取决于它们落地的方式。就像双陆棋一样，目标是先把所有的棋子都拿开。两个有趣的区别是，棋盘上有安全和陷阱空间。此外，玩家可能会像在 Parchisi 一样阻碍对方的进步。Senet棋具通常由木材制成，相当漂亮。Senet是最古老的棋类游戏之一，但不幸的是，它已经消失了几千年，没有发现任何古老的规则集。毫无疑问，亚历山大图书馆被烧毁，造成了另一个悲惨的损失。因此，学者们对规则进行了一些有根据的猜测。最流行的版本见附件。

      - sig/chess/[Senet.pdf](Senet.pdf)
      - sig/chess/[leaflet-04-senet.pdf](leaflet-04-senet.pdf)
      - sig/chess/[senet_rules_variant_en_v1.pdf](senet_rules_variant_en_v1.pdf)

### 证据链

共346件证据。 浏览 Senet 的所有证据 在此处 。

单击任何标记或突出显示的区域以查看与其相关的证据。  
要查看所有地区，请从下面的类别选项中选择它。

 **来源** 

1. Åström, P. 1984。“Hala Sultan Tekke 的有洞的石头。” 在 C. Nicole（编辑）中，希腊主义的起源：克里特岛和希腊：向 Henri van Effenterre 致敬。 巴黎：索邦大学，9-14。

1. Ayrton, E. 1904。Abydos。 伦敦：埃及勘探基金。

1. Bénédite, G. 1894。“Amenemanit 之子 Neferhotpu 之墓。” 开罗（巴黎）法国考古团成员出版的回忆录 5：489-540。

1. Blackman, A. 1920。“已故耶利米·詹姆斯·科尔曼 (Jeremiah James Coleman) 收藏的粮仓彩绘陶器模型。 卡罗楼，诺里奇。 埃及考古学杂志 6：206-208。

1. Bruyère, B. 1930。在 Deir el Medineh 的 Mert-Seger。 开罗：法国东方考古研究所印刷办公室。

1. Buchholz， H.-G。 1981. ''Cup Stones' 在希腊、安纳托利亚和塞浦路斯。 在 H. Lorenz (ed), Studies on the Bronze Age, Wilhelm Albert von Brunn 的 Festschrift。 莱茵河上的主干道：来自扎伯恩，63-94。

1. Capart, J. 1938。“新孟菲特浅浮雕”。 沃尔特斯美术馆杂志 1：13-18。

1. Carnarvon，伯爵和 H. 卡特。 1912. 在底比斯的五年探索。 伦敦：牛津大学出版社。

1. 卡特、H 和 AC 梅斯。 1933. Tut-Ankh-Amen 墓。 开罗：Cassell & Co.

1. 卡特，H 和 P. 纽伯里。 1904. 图特摩斯之墓 IV。 威斯敏斯特：A. Constable & Co.

1. Cherpion、N、JP Corteggiani 和 J.-F. Gout。 2017. Touna el-Gebel 的 Petosiris 墓：摄影调查。 开罗：法国东方考古研究所。

1. Cohen, R. 1986。根据考古和文学资料在公元前 4-1 千年期间内盖夫中部的定居点。 未发表的博士论文，耶路撒冷希伯来大学。

1. Coleman, J.、J. Barlow、M. Mogelonsky 和 ​​K. Sharr。 1996. Alambra：塞浦路斯的青铜时代中期遗址。 康奈尔大学的调查，1975-1978 年。 Jonsered: Paul Åströms Förlag。

1. Crist, W. 2016。《权力的游戏：塞浦路斯青铜时代的棋盘游戏和社会复杂性》。 未发表的博士 论文，亚利桑那州立大学。

1. 克里斯特，W. 2016a。 游戏石形态数据。 哈佛数据宇宙。

1. 克里斯特，W.，A.-E。 Dunn-Vaturi 和 A. de Voogt。 2016. 古埃及人在玩：跨境棋盘游戏。 伦敦：布卢姆斯伯里。

1. Daressy, G. 1902。1889-1899 年帝王谷发掘。 开罗：法国东方考古研究所印刷办公室。

1. 戴维斯，N 和 A. 加德纳。 1915. 阿梅内姆赫特墓（第 82 号）。 伦敦：埃及勘探协会。

1. de Buck, A. 1961。埃及棺材文本。 东方研究所出版物 87. 芝加哥：芝加哥大学出版社。

1. de Meulenaere, H. “古埃及塞尼特棋盘游戏回顾”。 埃及纪事 56：89-90。

1. Decker, W. 1987。古埃及的体育和游戏。 慕尼黑：Verlag CH Beck。

1. Deveria, T. 1897。埃及的跳棋。 在 G. Maspero (ed.), Memoirs and Fragments II。 埃及图书馆 5。巴黎，83-96。

1. Dunand, M. 1954。比布鲁斯第一卷的发掘。Parios：Maisonneuve。

1. Dunand, M. 1958。比布鲁斯发掘 1933-1938 卷 II。 巴黎：Maisonneuve。

1. 邓纳姆，D. 1978。雅利安人。 与后来的金字塔相邻的墓地。 波士顿：美术博物馆。

1. Emery, W. 1979。布恩堡垒：考古报告。 伦敦：埃及勘探基金。

1. Faulkner, R. 1972。《死者之书：咒语集》。 纽约：限量版俱乐部。

1. Firth, CM 1927。努比亚考古调查。 1910-1911 年的报告。 开罗：国家印刷部。

1. Frankel, D. 和 J. Webb。 1996. Marki-Alonia：塞浦路斯早期和中期青铜时代的小镇。 挖掘 1990-1994。 地中海考古学研究 123:1。 Jonsered: Paul Åströms Förlag。

1. Frankel, D. 和 J. Webb。 2006. 马基阿洛尼亚。 塞浦路斯早期和中期青铜时代的定居点。 挖掘 1995–2000。 Sävedalen: Paul Åströms Förlag。

1. Fugmann, E. 1958。哈马：挖掘和研究 1931-1938 II.1：前希腊化时期的建筑。 哥本哈根：嘉士伯基金会。

1. Gessler-Löhr, B. 等人。 1978. 古埃及陶瓷杰作：5000 年的粘土和彩陶工艺美术。 Höhr-Grenzhausen：韦斯特瓦尔德陶瓷博物馆。

1. Guillaume, P. 2013。“游戏”。 在 I. Finkelstein、D. Ussishkin 和 E. Cline（编辑）中。 Megiddo V：2004-2008 赛季。 特拉维夫：Emery 和 Claire Yass 在考古学中的出版物，1106-1114。

1. Hassan, S. 1975。Neb-Kaw-Her 的 Mastaba：1937-1938 年在 Saqqara 的发掘。 挖掘 Saqqara 24. 开罗：古物服务。

1. 海耶斯，WC 1959。埃及的权杖。 第 2 卷。纽约。

1. Hoffman, H. (ed.) 1964. 古代艺术之美：Norbert Schimmel 收藏。 美因茨：菲利普·冯·扎伯恩。

1. Iskander, JN 2010。“最近从赫利奥波利斯发现的一个 senet 板。” 德国考古研究所通讯，开罗部 66：122-129。

1. Jacquet, J. 1983。图特摩斯的宝藏 I。 开罗：法国东方考古研究所。

1. Jacquet-Gordon, H. 2003。卡纳克孔苏神庙屋顶上的涂鸦：个人虔诚的体现。 东方研究所出版物 123。芝加哥：芝加哥大学出版社。

1. Jéquier, G. 1930。“古人的更新游戏”。 埃及编年史 5：124-127。

1. Junker, H. 1940。Giza IV：Die Mastaba des Kai-em-ankh。 维也纳：Hölder-Pichler-Tempsky AG

1. Junker, H. 1953。在吉萨金字塔的旧王国公墓挖掘。 Giza 11：Cheops 金字塔以南的墓地。 维也纳：鲁道夫·罗勒。

1. Kamal, A. 1914。关于在北部 Deîrout 和南部 Deir-el-Ganadlah 之间地区进行的挖掘的报告。 埃及古物服务年鉴 15：45-87。

1. Kamal, A. 1915。梅尔的新墓。 埃及古物服务年鉴 14：209-258。

1. Karageorghis、V 和 M. Demas。 1988. Maa-Palaeocastro 1979-1986 的发掘​​。 尼科西亚：古物部，

1. Karageorghis, V. 和 M. Demas。 1984 年，Pyla-Kokkinokremos：公元前 13 世纪晚期在塞浦路斯的设防定居点。 尼科西亚：古物部。

1. Karageorghis, V. 和 M. Demas。 1985. Kition V 的发掘：前腓尼基水平。 尼科西亚：古物部。

1. Kendall, T. 1978。穿越阴间：Senet 的意义和游戏，古埃及的丧葬游戏。 马萨诸塞州贝尔蒙特：柯克游戏公司。

1. Kochavi, M. 1967。I Age 中青铜（迦南）中的内盖夫定居点。 未发表的博士论文，耶路撒冷希伯来大学。

1. 劳尔，J.-P。 1926. Saqqara，孟菲斯皇家公墓。 伦敦：泰晤士河和哈德逊。

1. Lauffray, J. 1971。西方接近卡纳克的第一个塔。 凯米 21：77-144。

1. Lee, J. 1982。“来自约旦 Bab edh-Dhra 的早期青铜时代游戏石。” 黎凡特 14：171-174。

1. Leemans, C. 1846–1850。 来自荷兰莱德古物博物馆的埃及古迹，第二卷。 莱顿：布里尔。

1. Lefebvre, G. 1923。Petosiris 墓。 开罗：法国东方古物研究所。

1. Lepsius, R. 1849。埃及和埃塞俄比亚的纪念碑。 第 1 卷。柏林：Nicolaische Buchhandlung。

1. MacGregor, W. 1922。MacGregor 埃及古物收藏目录。 苏富比拍卖目录 1922。伦敦：德莱顿出版社。

1. Mariette, A. 1889 年。Les mastabas de l'Ancien Empire。 巴黎：F. Vieweg。

1. 玛丽埃特，A. 1889a。 在埃及和努比亚收集的各种纪念碑。 巴黎：F. Vieweg。

1. Maspero, G. 1871。第 20 王朝时期底比斯的司法调查。 雅培纸莎草纸研究。 铭文学院回忆录和 Belles-Lettres 7：1-85。

1. Maspero, G. 1883。布拉克博物馆游客指南。 布拉克：布拉克博物馆。

1. Maspero, G. 1897。埃及的跳棋。 埃及图书馆 5：83-96。

1. 迈耶，J.-W。 1986. 游戏板 KL 78:534 和 KL:53d。 在 R. Hachmann (ed)，关于 1977 年至 1981 年 Kamid el-Loz 挖掘结果的报告。波恩：Rudolf Habelt，123-143。

1. Mond, R. 1904。底比斯 Gebel ash-sheikh Abd-el-Kurneh 所做工作的报告。 埃及古物服务年鉴 5：97-104。

1. 蒙特，P. 1938.“Abou Rouach 的第一和第四王朝陵墓”。 凯米 7：11-69。

1. Murray, HJR 1951。国际象棋以外的棋盘游戏的历史。 牛津：克拉伦登出版社。

1. Muscarella, O. 1974。古代艺术：Norbert Schimmel 收藏。 美因茨：冯·扎伯恩。

1. Nash, W. 1902。古埃及的绘图板和绘图员。 圣经考古学会会刊 24（十二月）：341-348。

1. Naville, E. 1900。埃及和埃塞俄比亚的纪念碑。 歌词。 莱比锡：JC Hinrich。

1. Needler, W. 1953 年。皇家安大略博物馆中的一个 30 平方的草稿板。 埃及考古学杂志 39：60-75。

1. Needler, W. 1983。“古埃及塞尼特棋盘游戏回顾”。 美国埃及研究中心杂志 20：115-118。

1. Petrie，WMF 1888。Tanis 第二部分：Nebesheh (Am) 和 Deffeneh (Tahpanhes)。 伦敦：埃及勘探基金。

1. Petrie，WMF 1890。Kahun、Gurob 和 Hawara。 伦敦：Paul, Trench, Trübner & Co.

1. Petrie, WMF 1927。日常用品。 伦敦：埃及的英国考古学院。

1. 皮特里，WMF 等人。 1892. 米底亚人。 伦敦：D. Nutt。

1. Piankoff, A. 和 H. Jacquet-Gordon。 1972. 灵魂的流浪。 普林斯顿：普林斯顿大学出版社。

1. Piccione, P. 1984。“古埃及塞尼特棋盘游戏回顾”。 埃及考古学杂志 70：172-180。

1. Piccione, P. 1990。Senet 游戏的历史发展及其对埃及宗教的意义。 芝加哥大学未发表的博士论文。

1. Pieper, M. 1909。古埃及人的棋盘游戏。 柏林：魏德曼书店。

1. Pieper, M. 1931。关于埃及棋盘游戏的文字。 埃及语言和考古学杂志，66：16-33。

1. Pierret, P. 1878。埃及卢浮宫博物馆未发表的铭文收藏。 埃及学研究 8. 巴黎：F. Vieweg。

1. 波特，B 和 R. 莫斯。 1973. 古埃及象形文字、浮雕和绘画的地形书目。 第 3 卷：孟菲斯。 第二版。 由 J. Malek 修订。 牛津：克拉伦登出版社。

1. 波特，B 和 R. 莫斯。 1980. 古埃及象形文字、浮雕和绘画的地形书目。 第 3 卷：孟菲斯。 第二版。 由 J. Malek 修订。 牛津：克拉伦登出版社。

1. Porter, B. 和 R. Moss。 1960/1964 年。 古埃及象形文字、浮雕和绘画的地形参考书目。第 1 卷。 1：底比斯墓地：私人陵墓。 铂。 2：底比斯墓地：皇家陵墓。 第 2 版，修订。 牛津：格里菲斯研究所。

1. Porter, B. 和 R. Moss。 1972. 古埃及象形文字、浮雕和绘画的地形书目。卷。 2：底比斯神庙。 第 2 版，修订。 牛津：克拉伦登出版社。

1. Prisse-d'Avennes，埃。 1847. 埃及纪念碑、浅浮雕、绘画、铭文等。 巴黎：狄多兄弟。

1. Pusch, E. 1979。古埃及的塞尼特棋盘游戏。 慕尼黑：德国艺术出版商。

1. Quibbell, J. 1909 年。萨卡拉的发掘（1907-1908 年）。 开罗：国际粮农组织。

1. Quibbell, J. 1913。1911-1912 年在 Saqqara 的发掘：Hesy 的 Mastaba。 开罗：国际粮农组织。

1. Randall-McIver, D. 和 AC Mace。 1902. El Amrah 和 Abydos 1899-1901。 EM 23. 伦敦：埃及勘探基金。

1. Rast, W. 和 T. Shaub。 2003. Bâb edh-Dhrâ'：城镇遗址发掘（1975-1981）。 印第安纳州威诺纳湖：艾森布劳恩斯。

1. Richard, S. 和 R. Boraas。 1984. “1981-82 赛季 Khirbet Iskander 远征及其附近地区的初步报告。” 美国东方研究学院公报 254：63-87。

1. Riefstahl, E. 布鲁克林博物馆的古埃及玻璃和釉料。 纽约：布鲁克林博物馆。

1. Romano I、Tait W、Bisulca C、Creasman P、Hodgins G 和 Wazny T。 2018. 亚利桑那州立博物馆的古埃及赛尼特棋盘。 埃及语言和考古学杂志，145（1）：71-85。

1. 萨卡拉探险队。 1938. Mereruka 的马斯塔巴。 芝加哥：芝加哥大学出版社。

1. Schiaparelli, E. 1927 年。关于意大利在埃及的考古任务的工作报告 II。 都灵。

1. Schott, G. 1986。耶鲁大学的古埃及艺术。 纽黑文：耶鲁大学艺术画廊。

1. Sebbane, M. 2001。“青铜时代早期和中期迦南的棋盘游戏和埃及塞内特游戏的起源。” 特拉维夫 28：213–230。

1. Sebbane, M. 2004。“附录：和切开的 Senet 棋盘游戏。” 在 D. Ussishkin（编辑）中，拉吉的新考古发掘（1973-1994）。 特拉维夫：Emery 和 Claire Yass 考古学出版物，690–694。

1. Seele, K. 底比斯的 Tjanefer 墓。 东方研究所出版物 86。芝加哥：芝加哥大学出版社。

1. Seyffarth, G. 1833. 对文学、艺术知识的贡献， 古埃及的神话和历史。 第三卷。 莱比锡：JA Barth。

1. Shai、 I、H. Greenfield、J. Regev、E. Boaretto、A. Eliyahu-Behar 和 A. Maeir。 2014. 'Tell exs-Safi/Gath 的早期青铜时代仍然存在：中期报告。 特拉维夫 41：20-49。

1. Simpson, WK 1976. Giza Mastabas 2：Qar 和 Idu 的 Mastabas。 波士顿：美术博物馆。

1. Smith, WS 1949。埃及雕塑和绘画史。 波士顿：美术博物馆。

1. South, A., P. Russell 和 P. Keswani。 1989. Vasilikos Valley Project 3: Kalavasos Ayios-Dhimitrios II（陶瓷、物品、墓葬、专业研究）。 地中海考古学研究 71.3。 哥德堡：Paul Åström 出版社。

1. Swiny、S、G. Rapp 和 E. Herscher。 2003. Sotira Kaminoudhia：塞浦路斯早期青铜时代遗址。 美国东方研究学院报告，塞浦路斯美国考古研究所专着 4。波士顿：美国东方研究学院。

1. Swiny, S. 1981。塞浦路斯西南部的青铜时代定居模式。 黎凡特 13：51-87。

1. Swiny, S. 1986。肯特州立大学远征 Episkopi Phaneromeni。 尼科西亚：Paul Åströms Förlag。

1. Tait, W. 1982。图坦卡蒙墓中的游戏盒和配件。 牛津：格里菲斯学院。

1. Todd, I. 1993。Kalavassos-Laroumena：青铜时代中期定居点的测试挖掘。 塞浦路斯古物部的报告，81-96。

1. Townshend, P. 1986。文化游戏：斯瓦希里语棋盘游戏的背景分析及其与非洲曼卡拉变化的相关性。 剑桥大学未发表的博士论文。

1. Wallis, H. 1898。埃及陶瓷艺术：麦格雷戈收藏。 伦敦：泰勒和弗朗西斯。

1. Wiedemann, A. 1897。古埃及人的棋盘游戏。 在 Actes du Dixième Congrès Internationale des Orientalistes，1894 年。莱顿：布里尔，35-61。

1. Wild, H. 1979。Deir el-Medina 的 Nefer-hotep (I) 和 Neb-nefer 墓（第 6 号）。 法国东方考古研究所成员出版的回忆录103。开罗：法国东方考古研究所。

1. Yadin, Y. 1960 年。Hazor II。 耶路撒冷：马格雷斯出版社。

## [Senet](https://thcgc.gitee.io/ludii.china/details/3.Senet.html) Ludii.games

<div id ="container">
<table width="100%" border="0" cellpadding="0" cellspacing="0" margin="0" padding="0" bgcolor="#FFFFFF">
<tbody>
<tr>
<br>
<td width="1054" valign="top" >
<br>

<span class="style11">(<i>Znt</i>)<span style='float:right;color:red;'>DLP Game</span>                   
</span>
                
                
<p class="style11">
</p>
                                                
                
<a target="_blank" href="https://ludii.games/lwp/GenericPlayerDemo.html?ludeme=/lud/board/race/escape/Senet.lud&username=&width=950&height=400"><img src="https://images.gitee.com/uploads/images/2022/0630/175309_87ddd744_5631341.png"></a>    

                                       
<p class="style1">
<strong>Period</strong>
<a href=library.php?period=1&region=0&category=0&gameName=">Ancient</a></p>
                    
                            
<p class="style1">
<strong>Region</strong>
<a href=library.php?region=4&category=0&period=0&gameName=">Northern Africa</a>,<a href=library.php?region=17&category=0&period=0&gameName=">Western Asia</a></p>
                                            
                                    
<p class="style1">
<strong>Category</strong>
<a href=library.php?category=1&region=0&period=0&gameName=">Board</a>,<a href=library.php?category=3&region=0&period=0&gameName=">Race</a>,<a href=library.php?category=4&region=0&period=0&gameName=">Escape</a></p>
                    
<p class="style11">
<strong>Description</strong>
</p>
<p class="style1">
                            Senet is one of the oldest board games known in the world, documented from about 3000 BCE until the first millennium BCE in Egypt. It was also played In Cyprus for most of that time, and played at different times in the Levant. Though a full ruleset has never been found, hints from texts and tomb paintings in Egypt give us clues about the manner in which it was likely played. The game was also heavily imbued with religious significance, as the board itself represented the journey through the afterlife.</p>
                        
<p class="style11">
<strong>Rules</strong>
</p>
<p class="style1">
                            Played on a 3x10 board. Pieces can be five or seven in number. Two players. Four casting sticks used as dice. Boustrophedon track from top left to bottom right.                             
                            
                                                    
                            
                            
                            
                                                    
                        
                            
                                                                                           
                                                                   
                                                                   
                    
                            

                            
</p>
                                        
                
                                        

<p class="style11">
<strong>All Rulesets</strong>
</p>






                                                    
                                                             
                                
                                
                                
                                
                                
                                
                            
                        
                        
                        
                        
                        
                            
                            
</p>
<p class="style11">
<u>Scholarly rulesets</u><br>

                                                            

<a href="variantDetails.php?keyword=Senet&variant=922">Kendall</a>
                                 Standard rules proposed by Timothy Kendall.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=923">Kendall Five Pieces</a>
                                 Five pieces per player.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=924">Kendall Starting Throw</a>
                                 A throw of 1 is required to begin.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=925">Kendall Trap</a>
                                 Square 27 is a trap.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=926">Kendall Home Row</a>
                                 Cannot bear off until all pieces are beyond the home row.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=927">Kendall Five Pieces Starting Throw</a>
                                 Five pieces and starting throw rules.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=928">Kendall Five Pieces Trap</a>
                                 Five pieces and trap rules.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=929">Kendall Five Pieces Home Row</a>
                                 Five pieces and home row rules.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=930">Kendall Starting Throw Trap</a>
                                 Starting throw and trap rules.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=931">Kendall Starting Throw Home Row</a>
                                 Starting throw and home row rules.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=932">Kendall Trap Home Row</a>
                                 Trap and home row rules.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=933">Kendall Five Pieces Starting Throw Trap</a>
                                 Five pieces, starting throw, and trap rules.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=934">Kendall Starting Throw Trap Home Row</a>
                                 Starting throw, trap, and home row rules.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=935">Kendall Five Pieces Trap Home Row</a>
                                 Five pieces, trap, home row.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=936">Kendall Five Pieces Starting Throw Home Row</a>
                                 Five pieces, starting throw, home row rules.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=937">Kendall All Options</a>
                                 Five pieces, starting throw, trap, and home row rules.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=938">Piccione</a>
                                 Rules proposed by Peter Piccione.<br>
                                                                

<a href="variantDetails.php?keyword=Senet&variant=939">Jéquier</a>
                                 Rules proposed by Gustave Jéquier.<br>
                                
                        
                        
                        

                        
                            
                            



                        
                        
                        
                        
                                                
                        
                                
</p>
<p class="style11">
<u>Suggested rulesets</u><br>

                                                            

<a href="variantDetails.php?keyword=Senet&variant=529">Bell</a>
                                 Suggested Rules from R. C. Bell.<br>
                                                        
                        
                        
                        
                        
                        
                        

                        
                        
                                                            

<a href="variantDetails.php?keyword=Senet&variant=1">Simple Senet</a>
                                     Played on a board with no markings.<br>
                                                                        

<a href="variantDetails.php?keyword=Senet&variant=2">Simple Marked Senet</a>
                                     Played on a board with basic markings.<br>
                                                                        

<a href="variantDetails.php?keyword=Senet&variant=3">Middle Kingdom Senet</a>
                                     Played on a board common in Middle Kingdom Egypt.<br>
                                                                        

<a href="variantDetails.php?keyword=Senet&variant=4">Early New Kingdom Senet</a>
                                     Played on a board common in earlier New Kingdom Egypt.<br>
                                                                        

<a href="variantDetails.php?keyword=Senet&variant=5">Late New Kingdom Senet</a>
                                     Played on a board common in later New Kingdom Egypt.<br>
                                                                        

<a href="variantDetails.php?keyword=Senet&variant=6">Vertical Senet</a>
                                     Played with a vertically-oriented board and different markings.<br>
                                                                        

<a href="variantDetails.php?keyword=Senet&variant=8">Double Senet</a>
                                     Played on two boards placed next to each other.<br>
                                                                        

<a href="variantDetails.php?keyword=Senet&variant=9">Cypriot Senet</a>
                                     Played on simple boards with seeds or stones in prehistoric Cyprus.<br>
                                                            
                        
                        
                        
                        
                        
                
<p class="style11">
<strong>Origin</strong>
</p>
<p class="style1">
                            Egypt</p>
                                        
                                        
                
                                        
                
                        

                        

<p class="style11">
<strong>Ludeme Description</strong>
</p>
<p>
<a href="../lud/3.Senet.lud"">Senet.lud</a></p>
                        
<!--
<p class="style11">
<strong>Credit lud File</strong>
</p>
<p class="style1">
<a href=search.php?id=DLP.Games.credit.Eric%20Piette">Eric Piette</a></p>
                                                -->
                        
                        

<p class="style11">
<strong>Concepts</strong>
<p class="style1">
                                                Browse all concepts for Senet<a href=concepts.php?gameId=3 target="_blank"">here</a>.
</p>
</p>

                        
                        
                       
                
                
                
                   
                                
                
                                

                
                
<p class="style11">
<strong>Evidence Map</strong>
<p class="style1">
                                                346 pieces of evidence in total. Browse all evidence for Senet<a href=data.php?gameId=3 target="_blank"">here</a>.
</p>
<p class="style1" id="ageRange"></p>
<p class="style1">
                        Click on any marker or highlighted region to view the evidence relating to it.
<br>To view all regions, please select it from the category options below.
</p>
</p>
        
                

<p class="style1">

Evidence category:

<select id="select_category">
<option value="0">All</option>
<option value="1">Artifact</option>
<option value="2">Art</option>
<option value="3">Text</option>
<option value="4">Ethnography</option>
<!--<option value="5">Game Center</option>-->
<!--<option value="6">Regions</option>-->
</select>

<br><br> Evidence coloured based on:

<select id="select_colour">
<option value="1">Age</option>
<option value="0">None</option>

</select>

<br><br> Map style:

<select id="select_map">

<option value="0">Streets</option>
<option value="1">OpenStreetMaps</option>
<option value="2">ArcGIS</option>
<option value="3">Topographique</option>
<option value="4">Voyager</option>

</select>

</p>
<br>
<div id="map" class="map"></div>

<div id="popup" class="ol-popup"></div>
<br>
<p class="style11">
<strong>Sources</strong>
</p>
<p class="style1">    
<a href="https://www.worldcat.org/title/aux-origines-de-lhellenisme-la-crete-et-la-grece-hommage-a-henri-van-effenterre/oclc/1069280012&referer=brief_results">Åström, P. 1984. 'Stones with cavities at Hala Sultan Tekke.' In C. Nicole (ed.), Aux origines de l’hellenisme: la Crète et la Grèce: hommage à Henri van Effenterre. Paris: Sorbonne, 9–14.
</a><br><br><a href="https://digi.ub.uni-heidelberg.de/diglit/petrie1904bd3/0001/image">Ayrton, E. 1904. Abydos. London: Egypt Exploration Fund.</a><br><br><a href="https://www.worldcat.org/title/memoires-publies-par-les-membres-de-la-mission-archeologique-francaise-au-caire-sous-la-direction-de-mu-bouriant-tome-sixieme/oclc/80505265&referer=brief_results">Bénédite, G. 1894. 'Tombeau de Neferhotpu, fils d’Amenemanit.' Mémoires publiées par les membres de la mission archéologique française au Caire (Paris) 5: 489–540.
</a><br><br><a href="https://www.worldcat.org/title/journal-of-egyptian-archaeology/oclc/880853719&referer=brief_results">Blackman, A. 1920. 'A painted pottery model of a granary in the collection of the late Jeremiah James Coleman, Esq. of Carrow House, Norwich.' Journal of Egyptian Archaeology 6: 206–208.
</a><br><br><a href="https://www.worldcat.org/title/mert-seger-a-deir-el-medineh/oclc/558657434&referer=brief_results">Bruyère, B. 1930. Mert-Seger à Deir el Medineh. Cairo: Imprimerie de l’Institut Français d’Archéologie Orientale. 
</a><br><br><a href="https://www.worldcat.org/title/studien-zur-bronzezeit-festschr-fur-wilhelm-albert-v-brunn/oclc/252025714&referer=brief_results">Buchholz, H.-G. 1981. ''Schalensteine' in Griechenland, Anatolien, und Zypern.' In H. Lorenz (ed), Studien zur Bronzezeit, Festschrift für Wilhelm Albert von Brunn. Main am Rhein: von Zabern, 63–94.</a><br><br><a href="https://www.worldcat.org/title/journal-of-the-walters-art-gallery/oclc/819313252&referer=brief_results">Capart, J. 1938. 'A Neo-Memphite bas-relief.' Journal of the Walters Art Gallery 1:13–18.
</a><br><br><a href="https://www.worldcat.org/title/five-years-explorations-at-thebes-a-record-of-work-done-1907-1911/oclc/959428570&referer=brief_results">Carnarvon, Earl of and H. Carter. 1912. Five Years’ Exploration at Thebes. London: Oxford University Press.
</a><br><br><a href="https://www.worldcat.org/title/tomb-of-tutankh-amen-discovered-by-the-late-earl-of-carnarvon-and-howard-carter/oclc/49206642&referer=brief_results">Carter, H and A.C. Mace. 1933. The Tomb of Tut-Ankh-Amen. Cairo: Cassell & Co. 
</a><br><br><a href="https://www.worldcat.org/title/tomb-of-thoutmosis-iv/oclc/612237088&referer=brief_results">Carter, H and P. Newberry. 1904. The Tomb of Thutmosis IV. Westminster: A. Constable & Co.
</a><br><br><a href="https://www.worldcat.org/title/tombeau-de-petosiris-a-touna-el-gebel-releve-photographique/oclc/254558265&referer=brief_results">Cherpion, N, J.P. Corteggiani, and J.-F. Gout. 2017. Le tombeau de Pétosiris à Touna el-Gebel: Relevé photographique. Cairo: Institut Français d'Archéologie Orientale.</a><br><br><a href="https://www.worldcat.org/title/settlement-of-the-central-negev-in-the-light-of-archaeological-and-literary-sources-during-the-4th-through-the-1st-millenium-bce-1/oclc/470685400&referer=brief_results">Cohen, R. 1986. The Settlement of the Central Negev in the Light of Archaeological and Literary Sources during the Fourth-First Millennia BCE. Unpublished PhD dissertation, Hebrew University Jerusalem.</a><br><br><a href="https://www.worldcat.org/title/alambra-a-middle-bronze-age-settlement-in-cyprus-archaeological-investigations-by-cornell-university-1974-1985/oclc/36282900&referer=brief_results">Coleman, J., J. Barlow, M. Mogelonsky, and K. Sharr. 1996. Alambra: A Middle Bronze Age Site in Cyprus. Investigations by Cornell University, 1975–1978. Jonsered: Paul Åströms Förlag.
</a><br><br><a href="https://www.academia.edu/30183336/Games_of_Thrones_Board_Games_and_Social_Complexity_in_Bronze_Age_Cyprus">Crist, W. 2016. Games of thrones: board games and social complexity in Bronze Age Cyprus. Unpublished Ph.D. dissertation, Arizona State University.
</a><br><br><a href="https://doi.org/10.7910/DVN/1COPOZ">Crist, W. 2016a. Gaming stones morphological data. Harvard Dataverse.</a><br><br><a href="https://www.worldcat.org/title/ancient-egyptians-at-play-board-games-across-borders/oclc/903510539&referer=brief_results">Crist, W., A.-E. Dunn-Vaturi and A. de Voogt. 2016. Ancient Egyptians at Play: Board Games Across Borders.  London: Bloomsbury.
</a><br><br><a href="https://www.worldcat.org/title/fouilles-de-la-vallee-des-rois-1889-1899/oclc/876381015&referer=brief_results">Daressy, G. 1902. Fouilles de la Vallée des Rois 1889–1899. Cairo: Imprimerie de l’Institut Français d’Archéologie Orientale.
</a><br><br><a href="https://archive.org/details/tombofamenemhtno00davi/page/n4/mode/2up">Davies, N and A. Gardiner. 1915. The Tomb of Amenemhet (No. 82). London: Egypt Exploration Society.
</a><br><br><a href="https://www.worldcat.org/title/egyptian-coffin-texts/oclc/225961998&referer=brief_results">de Buck, A. 1961. The Egyptian Coffin Texts. Oriental Institute Publications 87. Chicago: University of Chicago Press. 
</a><br><br><a href="https://www.worldcat.org/title/chronique-degypte/oclc/883295197&referer=brief_results">de Meulenaere, H. 'Review of Das Senet-Brettspiel im alten Ägypten.' Chronique d'Égypte 56: 89-90.</a><br><br><a href="https://www.worldcat.org/title/sport-und-spiel-im-alten-agypten/oclc/1105502433&referer=brief_results">Decker, W. 1987. Sport und Spiel im alten Ägypten. Munich: Verlag C.H. Beck.
</a><br><br><a href="https://gallica.bnf.fr/ark:/12148/bpt6k55530h">Deveria, T. 1897. Les jeux de dames en Égypte. In G. Maspero (ed.), Mémoires et Fragments II. Bibliothèque Égyptologique 5. Paris, 83–96.
</a><br><br><a href="https://www.worldcat.org/title/fouilles-de-byblos/oclc/788312393&referer=brief_results">Dunand, M. 1954. Fouilles de Byblos Tome I. Parios: Maisonneuve.</a><br><br><a href="https://www.worldcat.org/title/fouilles-de-byblos-tome-ii-1933-1938/oclc/492072674">Dunand, M. 1958. Fouilles de Byblos 1933–1938 Tome II. Paris: Maisonneuve.</a><br><br><a href="https://www.worldcat.org/title/zawiyet-el-aryan-the-cemeteries-adjacent-to-the-layer-pyramid/oclc/473870324&referer=brief_results">Dunham, D. 1978. Zawiyet el-Aryan. The Cemeteries Adjacent to the Later Pyramid. Boston: Museum of Fine Arts.
</a><br><br><a href="https://www.worldcat.org/title/fortress-of-buhen-the-archaeological-report/oclc/5975503&referer=brief_results">Emery, W. 1979. Fortress of Buhen: Archaeological Report. London: Egypt Exploration Fund. 
</a><br><br><a href="https://www.worldcat.org/title/book-of-the-dead-a-collection-of-spells/oclc/941040769&referer=brief_results">Faulkner, R. 1972. The Book of the Dead: A Collection of Spells. New York: Limited Editions Club.
</a><br><br><a href=http://sfdas.com/IMG/pdf/6_-_firth_c._m._the_archaeological_survey_of_nubia_1910-1911_.pdf">Firth, C.M. 1927. The Archaeological Survey of Nubia. Report for 1910–1911. Cairo: National Printing Department. 
</a><br><br><a href="https://www.worldcat.org/title/marki-alonia-an-early-and-middle-bronze-age-town-in-cyprus/oclc/877997639&referer=brief_results">Frankel, D. And J. Webb. 1996. Marki-Alonia: An Early and Middle Bronze Age Town in Cyprus. Excavations 1990–1994. Studies in Mediterranean Archaeology 123:1. Jonsered: Paul Åströms Förlag. 
</a><br><br><a href="https://www.worldcat.org/title/marki-alonia-an-early-and-middle-bronze-age-settlement-in-cyprus-excavations-1995-2000/oclc/1131571852&referer=brief_results">Frankel, D. And J. Webb. 2006. Marki Alonia. An Early and Middle Bronze Age Settlement in Cyprus. Excavations 1995–2000. Sävedalen: Paul Åströms Förlag. 
</a><br><br><a href="https://www.worldcat.org/title/hama-fouilles-et-recherches-1931-1938/oclc/23420521&referer=brief_results">Fugmann, E. 1958. Hama: fouilles et recherches 1931–1938 II.1: L’architecture des périodes préhellénistiques. Copenhagen: Fondation Carlsberg.
</a><br><br><a href="https://www.worldcat.org/title/meisterwerke-altagyptischer-keramik-5000-jahre-kunst-und-kunsthandwerk-aus-ton-und-fayence-hohr-grenzhausen-rastal-haus-16-september-bis-30-november-1978/oclc/474947180&referer=brief_results">Gessler-Löhr, B. Et al. 1978. Meisterwerke altägyptischer Keramik: 5000 Jahre Kunst und Kunsthandwerk aus Ton und Fayence. Höhr-Grenzhausen: Keramikmuseum Westerwald.
</a><br><br><a href="https://www.worldcat.org/title/megiddo-v-the-2004-2008-seasons-2/oclc/859648628&referer=brief_results">Guillaume, P. 2013. 'Games.' In I. Finkelstein, D. Ussishkin, and E. Cline (eds). Megiddo V: The 2004–2008 Seasons. Tel Aviv: Emery and Claire Yass Publications in Archaeology, 1106–1114.</a><br><br><a href="https://www.worldcat.org/title/excavations-at-saqqara-1937-1938/oclc/4090122&referer=brief_results">Hassan, S. 1975. The Mastaba of Neb-Kaw-Her: Excavations at Saqqara, 1937–1938. Excavations Saqqara 24. Cairo: Service des Antiquités.
</a><br><br><a href="https://www.metmuseum.org/art/metpublications/The_Scepter_of_Egypt_Vol_2_The_Hyksos_Period_and_the_New_Kingdom_1675_1080_BC?Tag=&title=&author=&pt=0&tc=%7bAD9356EC-5405-4B7F-8553-AE512ADC84F1%7d&dept=0&fmt=0">Hayes, W.C. 1959. The Scepter of Egypt. Volume 2. New York. 
</a><br><br><a href="https://www.worldcat.org/title/norbert-schimmel-collection-the-beauty-of-ancient-art-exhibition-of-the-norbert-schimmel-collection-november-15-1964-to-february-14-1964-harvard-university/oclc/257857200&referer=brief_results">Hoffman, H. (ed.) 1964. The Beauty of Ancient Art: The Norbert Schimmel Collection. Mainz: Philipp von Zabern.
</a><br><br><a href="https://www.worldcat.org/title/mitteilungen-des-deutschen-archaologischen-instituts-abteilung-kairo/oclc/224599900&referer=brief_results">Iskander, J.N. 2010. 'A recently discovered senet-board from Heliopolis.' Mitteilungen des Deutsches Archäologischen Instituts, Abteilung Kairo 66: 122–129.
</a><br><br><a href="https://www.worldcat.org/title/karnak-nord-vii-le-tresor-de-thoutmosis-ier-installations-anterieures-ou-posterieures-au-monument/oclc/470361429&referer=brief_results">Jacquet, J. 1983. Le trésor de Thoutmosis Ier. Cairo: Institut Français d'Archeeologie Orientale.</a><br><br><a href="https://oi.uchicago.edu/sites/oi.uchicago.edu/files/uploads/shared/docs/OIP123.pdf">Jacquet-Gordon, H. 2003. The Graffiti on the Khonsu Temple Roof at Karnak: A Manifestation of Personal Piety. Oriental Institute Publications 123. Chicago: University of Chicago Press. 
</a><br><br><a href="https://www.worldcat.org/title/chronique-degypte/oclc/263588843&referer=brief_results">Jéquier, G. 1930. "Un jeu renouvelé des anciens." Chronique d'Ègypte 5: 124-127.</a><br><br><a href=http://www.gizapyramids.org/static/pdf%20library/junker_giza_4.pdf">Junker, H. 1940. Giza IV: Die Mastaba des Kai-em-ankh. Vienna: Hölder-Pichler-Tempsky A.G.
</a><br><br><a href=http://www.gizapyramids.org/static/pdf%20library/junker_giza_11.pdf">Junker, H. 1953. Grabungen auf dem Friedhof des Alten Reiches bei den Pyramiden von Giza. Giza 11: Der Friedhof südlich der Cheopspyramide. Vienna: Rudolf Rohrer.</a><br><br><a href="https://gallica.bnf.fr/ark:/12148/bpt6k5725841g">Kamal, A. 1914. Rapport sur les fouilles executés dans la zone comprise entre Deîrout au nord et Deir-el-Ganadlah, au sud. Annales du Service des Antiquités de l’Égypte 15: 45–87.
</a><br><br><a href="https://gallica.bnf.fr/ark:/12148/bpt6k5614758k">Kamal, A. 1915. Le tombeau nouveau de Meir. Annales du Service des Antiquités de l’Égypte 14: 209–258.
</a><br><br><a href="https://www.worldcat.org/title/excavations-at-maa-palaeokastro-1979-1986-2-plates/oclc/164921770&referer=brief_results">Karageorghis, V and M. Demas. 1988. Excavations at Maa-Palaeokastro 1979–1986. Nicosia: Department of Antiquities,</a><br><br><a href="https://www.worldcat.org/title/pyla-kokkinokremos-a-late-13th-century-bc-fortified-settlement-in-cyprus/oclc/758275524&referer=brief_results">Karageorghis, V. And M. Demas. 1984. Pyla-Kokkinokremos:  A Late 13th Century B.C. Fortified Settlement in Cyprus. Nicosia: Department of Antiquities.
</a><br><br><a href="https://www.worldcat.org/title/excavations-at-kition-5-the-pre-phoenician-levels/oclc/181723182&referer=brief_results">Karageorghis, V. And M. Demas. 1985. Excavations at Kition V: The Pre-Phoenician Levels. Nicosia: Department of Antiquities. 
</a><br><br><a href="https://www.worldcat.org/title/passing-through-the-netherworld-the-meaning-and-play-of-senet-an-ancient-egyptian-funerary-game/oclc/43405502&referer=brief_results">Kendall, T. 1978. Passing through the Netherworld: The Meaning and Play of Senet, an Ancient Egyptian Funerary Game. Belmont, MA: Kirk Game Company.
</a><br><br><a href="https://www.worldcat.org/title/gal-ha-hityashvuti-shel-tekufat-ha-bronzah-ha-kenaanit-ha-tikhonah-i-ba-negev/oclc/81698673&referer=brief_results">Kochavi, M. 1967. The Settlement of the Negev in the Middle Bronze (Canaanite) I Age. Unpublished PhD Dissertation, Hebrew University of Jerusalem.</a><br><br><a href="https://www.worldcat.org/title/saqqara-the-royal-cemetery-of-memphis-excavations-and-discoveries-since-1850/oclc/2523300&referer=brief_results">Lauer, J.-P. 1926. Saqqara, Royal Cemetery of Memphis.  London: Thames and Hudson.
</a><br><br><a href="https://www.worldcat.org/title/kemi-revue-de-philologie-et-darcheologie-egyptiennes-et-coptes/oclc/71851621&referer=brief_results">Lauffray, J. 1971. Abords occidentaux du premier pylône de Karnak. Kemi 21: 77–144.
</a><br><br><a href="https://www.worldcat.org/title/levant/oclc/488081303&referer=brief_results">Lee, J. 1982. 'An Early Bronze Age Game Stone from Bab edh-Dhra, Jordan.' Levant 14: 171–174.</a><br><br><a href="https://reader.digitale-sammlungen.de/en/fs1/object/display/bsb10257907_00001.html">Leemans, C. 1846–1850. Monuments égyptiens du Musée d’antiquités des Pays-Bas à Leide, Tome II. Leiden: Brill.
</a><br><br><a href="https://archive.org/stream/letombeaudepetos00lefeuoft#mode/2up">Lefebvre, G. 1923. Le Tombeau de Petosiris.  Cairo: Institute Français des Antiquités Orientales.
</a><br><br><a href=http://dlib.nyu.edu/awdl/sites/dl-pa.home.nyu.edu.awdl/files/denkmaelerausaeg05leps/denkmaelerausaeg05leps.pdf">Lepsius, R. 1849. Denkmäler aus Ägypten und Äthiopien. Volume 1. Berlin: Nicolaische Buchhandlung. 
</a><br><br><a href="https://www.worldcat.org/title/catalogue-of-the-macgregor-collection-of-egyptian-antiquities-which-will-be-sold-by-auction-by-messrs-sotheby-wilkinson-and-hodge/oclc/56227780&referer=brief_results">MacGregor, W. 1922. Catalogue of the MacGregor Collection of Egyptian Antiquities. Sotheby Sale Catalog 1922. London: Dryden Press.</a><br><br><a href=http://www.gizapyramids.org/static/pdf%20library/mariette_mastabas.pdf">Mariette, A. 1889. Les mastabas de l’Ancien Empire. Paris: F. Vieweg. 
</a><br><br><a href="https://digi.ub.uni-heidelberg.de/diglit/mariette1872bd1/0019/image">Mariette, A. 1889a. Monuments divers recueillis en Égypte et en Nubie. Paris: F. Vieweg.</a><br><br><a href="https://www.worldcat.org/title/mmoires-de-lacadmie-des-inscriptions-et-belles-lettres/oclc/1073732925&referer=brief_results">Maspero, G. 1871. Une Enquête judiciaire à Thébes au temps de la XXe Dynastie. Étude sur le papyrus Abbott. Mémoires de l’Academie des Inscriptions et Belles-Lettres 7: 1–85.
</a><br><br><a href="https://archive.org/details/guideduvisiteur02maspgoog/page/n11/mode/2up">Maspero, G. 1883. Guide du visiteur au Musée de Boulaq. Boulaq: Musée de Boulaq.
</a><br><br><a href="https://gallica.bnf.fr/ark:/12148/bpt6k55530h/f1.image">Maspero, G. 1897. Les jeux de dames en Égypte. Bibliotèque Égyptologique 5: 83–96.
</a><br><br><a href=http://digital.library.stonybrook.edu/cdm/ref/collection/amar/id/84085">Meyer, J.-W. 1986. Die Spielbretter KL 78:534 und KL:53d. In R. Hachmann (ed), Bericht über die Ergebnisse der Ausgrabungen in Kamid el-Loz in den Jahren 1977 bis 1981. Bonn: Rudolf Habelt, 123–143.</a><br><br><a href="https://gallica.bnf.fr/ark:/12148/bpt6k5724440c">Mond, R. 1904. Report of the work done in the Gebel ash-sheikh Abd-el-Kurneh at Thebes. Annales du Service des antiquités de l’Égypte 5: 97–104.
</a><br><br><a href="https://www.worldcat.org/title/kemi-revue-de-philologie-et-darcheologie-egyptiennes-et-coptes/oclc/71851621&referer=brief_results">Montet,P. 1938. "Tombeaux de la Ire et de la IVe Dynasties à Abou Rouach." Kemi 7: 11-69.</a><br><br><a href="https://www.worldcat.org/title/history-of-board-games-other-than-chess/oclc/59184280&referer=brief_results">Murray, H.J.R. 1951. A History of Board-Games Other Than Chess. Oxford: Clarendon Press.</a><br><br><a href="https://www.worldcat.org/title/ancient-art-the-norbert-schimmel-collection/oclc/1078712717&referer=brief_results">Muscarella, O. 1974. Ancient Art: The Norbert Schimmel Collection. Mainz: von Zabern.</a><br><br><a href="https://archive.org/details/proceedings24soci/page/n4/mode/2up">Nash, W. 1902. Ancient Egyptian draughts-boards and draughts-men. Proceedings of the Society of Biblical Archaeology 24(December): 341–348.
</a><br><br><a href=http://dlib.nyu.edu/awdl/sites/dl-pa.home.nyu.edu.awdl/files/denkmaelerausaeg05leps/denkmaelerausaeg05leps.pdf">Naville, E.  1900. Denkmäler aus Ägypten und Äthiopien. Texte. Lepizig: J.C. Hinrich.
</a><br><br><a href="https://www.worldcat.org/title/journal-of-egyptian-archaeology/oclc/162466746&referer=brief_results">Needler, W. 1953. A thirty-square draught board in the Royal Ontario Museum. Journal of Egyptian Archaeology 39: 60–75.
</a><br><br><a href="https://www.worldcat.org/title/journal-of-the-american-research-center-in-egypt/oclc/308362098&referer=brief_results">Needler, W. 1983. 'Review of Das Senet-Brettspiel im alten Ägypten.' Journal of the American Research Center in Egypt 20: 115–118.</a><br><br><a href="https://digi.ub.uni-heidelberg.de/diglit/petrie1888bd2/0001/image">Petrie, W.M.F. 1888. Tanis Part II: Nebesheh (Am) and Deffeneh (Tahpanhes). London: Egypt Exploration Fund.
</a><br><br><a href="https://archive.org/details/cu31924028675399/mode/2up">Petrie, W.M.F. 1890. Kahun, Gurob, and Hawara. London: Paul, Trench, Trübner & Co.
</a><br><br><a href="https://archive.org/details/ERA42/mode/2up">Petrie, W.M.F. 1927. Objects of Daily Use. London: British School of Archaeology in Egypt.
</a><br><br><a href="https://archive.org/details/cu31924028670465/mode/2up">Petrie, W.M.F. et al. 1892. Medum.  London: D. Nutt.
</a><br><br><a href="https://www.worldcat.org/title/wandering-of-the-soul/oclc/875761925&referer=brief_results">Piankoff, A. And H. Jacquet-Gordon. 1972. The Wandering of the Soul. Princeton: Princeton 			University Press.
</a><br><br><a href="https://www.academia.edu/8170411/Review_of_Das_Senet-Brettspiel_im_Alten_%C3%84gypten_by_E._Pusch">Piccione, P. 1984. 'Review of Das Senet-Brettpiel im alten Ägypten.' Journal of Egyptian Archaeology 70: 172–180.</a><br><br><a href="https://www.worldcat.org/title/historical-development-of-the-game-of-senet-and-its-significance-for-egyptian-religion/oclc/868960143&referer=brief_results">Piccione, P. 1990. The Historical Development of the Game of Senet and its significance for Egyptian Religion. Unpublished Phd Thesis, University of Chicago.
</a><br><br><a href="https://www.worldcat.org/title/brettspiel-der-alten-agypter-und-seine-bedeutung-fur-den-agyptischen-totenkult/oclc/6892905&referer=brief_results">Pieper, M. 1909. Das Brettspiel der alten Ägypter. Berlin: Weidmännische Buchhandlung.
</a><br><br><a href="https://www.worldcat.org/title/zeitschrift-fur-agyptische-sprache-und-altertumskunde/oclc/1607529&referer=brief_results">Pieper, M. 1931. Ein text über das ägyptisches Brettspiel. Zeitschrift für Ägyptische Sprache und 		Altertumskunde 66: 16–33.
</a><br><br><a href="https://archive.org/details/recueildinscrip00gygoog/page/n7/mode/2up">Pierret, P. 1878. Recueil d’inscriptions inédites du Musée égyptien du Louvre. Études égyptologues 8. Paris: F. Vieweg.
</a><br><br><a href=http://www.griffith.ox.ac.uk/topbib/pdf/pm3-1.pdf">Porter, B and R. Moss. 1973. Topographical Bibliography of Ancient Egyptian Hieroglyphic Texts, Reliefs, and Paintings. Volume 3: Memphis. Second edition. Revised by J. Malek. Oxford: Clarendon Press.
</a><br><br><a href=http://www.griffith.ox.ac.uk/topbib/pdf/pm3-2.pdf">Porter, B and R. Moss. 1980. Topographical Bibliography of Ancient Egyptian Hieroglyphic Texts, 		Reliefs, and Paintings. Volume 3: Memphis. Second edition. Revised by J. Malek. Oxford: 		Clarendon Press.
</a><br><br><a href=http://www.griffith.ox.ac.uk/topbib/pdf/pm1-1.pdf">Porter, B. And R. Moss. 1960/1964. Topographical Bibliography of Ancient Egyptian Hieroglyphic Texts, Reliefs, and Paintings.Vol 1 pt. 1: The Theban Necropolis: Private Tombs. Pt. 2: The 		Theban Necropolis: Royal Tombs. 2nd edition, revised. Oxford: Griffith Institute.
</a><br><br><a href=http://www.griffith.ox.ac.uk/topbib/pdf/pm2.pdf">Porter, B. And R. Moss. 1972. Topographical Bibliography of Ancient Egyptian Hieroglyphic Texts, Reliefs, and Paintings.Vol. 2: Theban Temples. 2nd edition, revised. Oxford: Clarendon Press.
</a><br><br><a href="https://gallica.bnf.fr/ark:/12148/btv1b7300479c.image">Prisse-d’Avennes, É. 1847. Monument égyptiens, bas-reliefs, peintures, inscriptions, etc.  Paris: Didot frères.
</a><br><br><a href="https://www.worldcat.org/title/senet-brettspiel-im-alten-agypten/oclc/899059346&referer=brief_results">Pusch, E. 1979. Das Senet-Brettspiel im Alten Ägypten. Munich: Deutsche Kunstverlag.
</a><br><br><a href="https://archive.org/details/cu31924028671265/page/n8/mode/2up">Quibbell, J. 1909. Excavations at Saqqara (1907–1908). Cairo: IFAO.
</a><br><br><a href="https://archive.org/details/cu31924028671299/page/n10/mode/2up">Quibbell, J. 1913. Excavations at Saqqara 1911–1912: The Mastaba of Hesy. Cairo: IFAO.
</a><br><br><a href="https://archive.org/details/elamrahandabydo00macgoog/page/n5/mode/2up">Randall-McIver, D. And A.C. Mace. 1902. El Amrah and Abydos 1899–1901. EM 23. London: Egypt Exploration Fund.
</a><br><br><a href="https://www.worldcat.org/title/bab-edh-dhra-excavations-at-the-town-site-1975-1981/oclc/897048739&referer=brief_results">Rast, W. and T. Shaub. 2003. Bâb edh-Dhrâ': Excavations at the Town Site (1975–1981). Winona Lake, IN: Eisenbrauns.</a><br><br><a href="https://www.academia.edu/32707755/Preliminary_Report_of_the_1981-82_Seasons_of_the_Expedition_to_Khirbet_Iskander_and_Its">Richard, S. and R. Boraas. 1984. 'Preliminary Report of the 1981–82 Seasons of the Expedition to Khirbet Iskander and its Vicinity.' Bulletin of the American Schools of Oriental Research 254: 63–87.</a><br><br><a href="https://babel.hathitrust.org/cgi/pt?id=mdp.39015055583317&view=1up&seq=9">Riefstahl, E. Ancient Egyptian Glass and Glazes in the Brooklyn Museum. New York: The Brooklyn Museum.</a><br><br><a href="https://statemuseum.arizona.edu/sites/default/files/Ancient%20Egyptian%20Senet%20Board%20in%20Arizona%20State%20Museum%20ZAeS%202018.pdf">Romano, I, W. Tait, C. Bisulca, P. Creasman, G. Hodgins, and T. Wazny. 2018. An Ancient Egyptian senet board in the Arizona State Museum. Zeitschrift für Ägyptische Sprache und Altertumskunde 145(1): 71–85.
</a><br><br><a href="https://oi.uchicago.edu/sites/oi.uchicago.edu/files/uploads/shared/docs/oip39.pdf">Saqqara Expedition. 1938. The Mastaba of Mereruka. Chicago: University of Chicago Press.
</a><br><br><a href="https://www.worldcat.org/title/relazione-sui-lavori-della-missione-archeologica-italiana-in-egitto-anni-1903-1920/oclc/977651895&referer=brief_results">Schiaparelli, E. 1927. Relazione sui lavori della missione archeologica Italiana in Egitto II. Turin.
</a><br><br><a href="https://www.worldcat.org/title/ancient-egyptian-art-at-yale/oclc/1120518109&referer=brief_results">Schott, G. 1986. Ancient Egyptian Art at Yale. New Haven: Yale University Arts Gallery. 
</a><br><br><a href="https://www.academia.edu/30823974/Sebbane_M._2001.Board_Games_from_Canaan_in_the_Early_and_Intermediate_Bronze_Ages_and_the_Origin_of_the_Egyptian_Senet_Game._Tel_Aviv_28_213-230">Sebbane, M. 2001. 'Board Games from Canaan in the Early and Intermediate Bronze Ages and the Origin of the Egyptian Senet Game.' Tel Aviv 28:213–230.</a><br><br><a href="https://www.worldcat.org/title/renewed-archaeological-excavations-at-lachish-1973-1994/oclc/471004979&referer=brief_results">Sebbane, M. 2004. 'Appendix: And Incised Senet Board Game.' In D. Ussishkin (ed), The Renewed Arcahaeological Excavations at Lachish (1973–1994). Tel Aviv: Emery and Claire Yass Publications in Archaeology, 690–694.</a><br><br><a href="https://oi.uchicago.edu/sites/oi.uchicago.edu/files/uploads/shared/docs/oip86.pdf">Seele, K. The Tomb of Tjanefer at Thebes. Oriental Institute Publication 86. Chicago: University of 		Chicago Press.
</a><br><br><a href="https://www.worldcat.org/title/beitrge-zur-kenntniss-der-literatur-kunst-mythologie-und-geschichte-des-alten-gypten/oclc/1069790510&referer=brief_results">Seyffarth, G. 1833. Beiträge zur Kenntnis der Literatur, Kunst, 
Mythologie und Geschichte des Alten Ägypten. Volume III.  Laipzig: J.A. Barth.
</a><br><br><a href="https://www.worldcat.org/title/tel-aviv-journal-of-the-tel-aviv-university-institute-of-archeology-reprint-series/oclc/652880617&referer=brief_results">Shai, I, H. Greenfield, J. Regev, E. Boaretto, A. Eliyahu-Behar, and A. Maeir. 2014. 'The Early Bronze Age Remains  at Tell exs-Safi/Gath: An Interim Report. Tel Aviv 41: 20–49.</a><br><br><a href=http://www.gizapyramids.org/static/pdf%20library/giza_mastabas_2.pdf">Simpson, W.K. 1976. Giza Mastabas 2: The Mastabas of Qar and Idu. Boston: Museum of Fine Arts. 
</a><br><br><a href="https://archive.org/details/in.ernet.dli.2015.69805">Smith, W.S. 1949. A History of Egyptian Sculpture and Painting. Boston: Museum of Fine Arts.
</a><br><br><a href="https://www.worldcat.org/title/vasilikos-valley-project-3-kalavasos-ayios-dhimitrios-ii-ceramics-objects-tombs-specialist-studies-alison-south-et-al/oclc/715355856&referer=brief_results">South, A., P. Russell and P. Keswani. 1989. Vasilikos Valley Project 3: Kalavasos Ayios-Dhimitrios II(Ceramics, Objects, Tombs, Specialist Studies). Studies in Mediterranean Archaeology 71.3. Göteborg: Paul Åströms Förlag. 
</a><br><br><a href="https://www.worldcat.org/title/sotira-kaminoudhia-an-early-bronze-age-site-in-cyprus/oclc/62888427&referer=brief_results">Swiny, S, G. Rapp and E. Herscher. 2003. Sotira Kaminoudhia: An Early Bronze Age Site in Cyprus. American Schools of Oriental Research Reports, Cyprus American Archaeological Research Institute Monographs 4. Boston: American Schools of Oriental Research. 
</a><br><br><a href="https://www.worldcat.org/title/levant/oclc/234220848&referer=brief_results">Swiny, S. 1981. Bronze Age Settlement Patterns in Southwest Cyprus. Levant 13: 51–87.
</a><br><br><a href="https://www.worldcat.org/title/kent-state-university-expedition-to-episkopi-phaneromeni/oclc/185444867&referer=brief_results">Swiny, S. 1986. The Kent State University Expedition to Episkopi Phaneromeni. Nicosia: Paul Åströms Förlag.
</a><br><br><a href="https://www.worldcat.org/title/game-boxes-and-accessories-from-the-tomb-of-tutankhamun/oclc/230275170&referer=brief_results">Tait, W. 1982. Game-boxes and Accessories from the Tomb of Tutankhamun. Oxford: Griffiths Institute.
</a><br><br><a href="https://www.worldcat.org/title/annual-report-of-the-department-of-antiquities-for-the-year/oclc/1041375508&referer=brief_results">Todd, I. 1993. Kalavassos-Laroumena: Test Excavation of a Middle Bronze Age Settlement. Report of the Department of Antiquities, Cyprus, 81–96.
</a><br><br><a href="https://www.worldcat.org/title/games-in-culture-a-contextual-analysis-of-the-swahili-board-game-and-its-relevance-to-variation-in-african-mankala/oclc/499910921&referer=brief_results">Townshend, P. 1986. Games in culture: A contextual analysis of the Swahili board game and its relevance to vaariation in African mankala. Unpublished PhD dissertation, University of Cambridge.</a><br><br><a href="https://archive.org/details/egyptianceramica00wall/page/n6/mode/2up">Wallis, H. 1898. Egyptian Ceramic Art: The MacGregor Collection. London: Taylor & Francis.
</a><br><br><a href="https://archive.org/details/actesdudiximeco00unkngoog/page/n10/mode/2up">Wiedemann, A. 1897. Das Brettspiel bei den Alten Ägyptern. In Actes du Dixième Congrès Internationale des Orientalistes, 1894. Leiden: Brill, 35–61.  
</a><br><br><a href="https://www.worldcat.org/title/tombe-de-nefer-hotep-i-et-neb-nefer-a-deir-el-medina-no-6-et-autres-documents-les-concernant/oclc/695401314&referer=brief_results">Wild, H. 1979. La tombe de Nefer-hotep (I) et Neb-nefer à Deir el-Medina (No. 6). Mémoires publiées par les membres de l’Institut Français d’Archéologie Orientale 103. Cairo: Institue Français de l’Archéologie Orientale.
</a><br><br>

<a href="https://www.worldcat.org/title/hazor-ii-an-account-of-the-second-season-of-excavations-1956/oclc/1086301935&referer=brief_results">Yadin, Y. 1960. Hazor II. Jerusalem: Magres Press.</a>

<br><br></p>


                             
                 
                             
        


<p class="style11">
<strong>Identifiers</strong>
</p>
<p class="style1">
             DLP.Games.3  
</p>
<a href="https://boardgamegeek.com/boardgame/2399>
<p class="style1"">
             BGG.2399  
</p>
</a>
                
        
        
        
                
        


                
</tr>
</tbody>
</table>
</td>
</tr>


<tr>
<td height="77" bgcolor="#eeeeee"><table width="100%" border="0" cellpadding="2" cellspacing="0">
</td></tr>
</tbody>  
</table>
</div>

## History UnBox 手工课

![输入图片说明](https://images.gitee.com/uploads/images/2022/0630/181610_5363220b_5631341.jpeg "IMG_20210612_101821-scaled (1).jpg")

[Senet Kit (Ancient Egypt) - History Unboxed](https://www.historyunboxed.com/shop/museum-craft-kits/senet-kit-ancient-egypt/)
historyunboxed.com|2204 × 2560 jpeg|Image may be subject to copyright.

![输入图片说明](https://images.gitee.com/uploads/images/2022/0701/112621_a9a8c7c1_5631341.jpeg "History-Unboxed-10-28-16_10-scaled.jpg")

### [Senet Kit (Ancient Egypt)](https://www.historyunboxed.com/shop/museum-craft-kits/senet-kit-ancient-egypt/)

One of the wonderful things about studying Ancient Egypt is that the Egyptians left behind so much information. Between hieroglyphs, wall paintings, and tombs fully stocked for the afterlife, we are able to learn a great deal about daily life in Egypt, including how they spent their leisure time. Games were an important part of life in Egypt, and the oldest known game was called Senet. Now you can make and play this game, just like the ancient Egyptians.

SKU: HTG-SEN-1AZ Category: History to Go Kits Tags: ancient history, classroom, library, museum


- What's included?

  > Kit includes everything needed to make and play Senet:

  - Wooden Game Board
  - White & Black Game Pieces
  - Throwing Sticks
  - Paint Marker
  - Pencil
  - Ruler
  - Rules Booklet
  - Historic Information & Instructions Sheet

- Additional information

  > Weight: 8.5 oz


- Image note:

  > Please note that your kit may differ in slight cosmetic ways from the image.

### Senet Kit（古埃及）

研究古埃及的美妙之处之一是埃及人留下了如此多的信息。  在象形文字、壁画和为来世而储备充足的坟墓之间，我们能够了解很多关于埃及的日常生活，包括他们如何度过闲暇时光。  游戏是埃及生活的重要组成部分，已知最古老的游戏被称为 Senet。  现在您可以制作和玩这款游戏，就像古埃及人一样。

SKU：HTG-SEN-1AZ 类别：History to Go Kits 标签：古代历史、教室、图书馆、博物馆

# 20 Squares (Aseb)

## [乌尔皇家游戏](https://thcgc.gitee.io/ludii.china/details/50.Royal%20Game%20of%20Ur.html) Ludii.China

所 谓的乌尔皇家游戏——我们不知道它的古老名称——是已知最古老的棋盘游戏之一，可追溯到公元前三千年。 虽然没有专门为这款游戏传递任何规则，但基于后续游戏 20 Squares 的规则集已应用于这个略有不同的棋盘。 棋盘由 3x4 方格组成，通过 1x2 方格短桥连接到 3x2 方格，连接其他两个方格的中央行。 游戏使用四个四面体骰子和每个玩家 7 个小圆盘作为棋子。

### 规则

4x3 网格与 3x 2 网格由两个正方形的“颈部”连接。 每个播放器五张光盘。 四个四面体骰子。

### 起源

美索不达米亚

### 来源

- Frenez, D. 2018。青铜时代中亚亚洲象牙的制造和贸易。 来自 Gonur Depe (Margiana, Turkmenistan) 的证据。 亚洲考古研究 15：13-33。

- Piperno、M 和 S. Salvatori。 1983. 伊朗锡斯坦 Shahr-I-Sokhta 墓地研究的最新成果和新观点。 年鉴 43：172-191。

- Sarianidi, VI 2007。Gonur 墓地。 雅典：卡彭版。

- Woolley, CL 1934。Ur 挖掘。 第 2 卷，皇家公墓。 牛津：牛津大学出版社。

## [Royal Game Of Ur – One Of The Oldest Game Boards Discovered](https://www.ancientpages.com/2016/02/03/royal-game-of-ur-one-of-the-oldest-game-boards-discovered/)

 **AncientPages.com**    -  

### Question : What was the Royal Game Of Ur?

 **Answer** : In the ancient world, board games were as popular as they are today. Board games were very common in ancient Egypt, Mesopotamia and other parts of the world.

The Royal Game of Ur is a board game made between 2600 and 2400 BC. It means the game is at least 4,400 years old!

![Royal Game Of Ur](https://images.gitee.com/uploads/images/2022/0730/091511_a5afbfaf_5631341.png "屏幕截图.png")

It was discovered in Mesopotamia (modern Iraq) in the 1920s when the British archaeologist Leonard Woolley was digging into the ruins of a long-buried royal city. Among many amazing treasures, he found several games and the Royal Game Of Ur is one of them.

The board's wood has decayed, but the hard, decorative pattern of shell, red limestone and lapis lazuli (a blueish mineral) has survived. The counters were made from shale and shell.

See also: [Ancient Egyptian Toys And Games In Focus](https://www.ancientpages.com/2016/01/15/ancient-egyptian-toys-games-focus/)

The Ur-style Twenty Squares game board was also known in Egypt as Asseb( **Aseb** ), and has been found in Pharaoh Tutankhamen's tomb, among other places. Discovery of a tablet partially describing the game play has allowed the game to be played again after over 2000 years, although reconstructions of the detailed rules have differed widely.

The Royal Game of Ur was played with two sets, one black and one white, of seven markers and three tetrahedral dice. After around 1000 BC, the layout of the twenty squares was altered to make the end course for the markers a straight line.

The rules of the game as it was played in Mesopotamia are not completely known but there have been a number of reconstructions of game play, based on a cuneiform tablet of Babylonian origin dating from 177–176 BC by the scribe Itti-Marduk-Balāṭu. It is universally agreed that the Royal Game of Ur, like [Senet](https://www.ancientpages.com/2016/01/15/ancient-egyptian-toys-games-focus/), is a race game.

AncientPages.com 

Expand for references
Tags: [Ancient games](https://www.ancientpages.com/tag/ancient-games/), [Board Games](https://www.ancientpages.com/tag/board-games/), [Mesopotamia](https://www.ancientpages.com/tag/mesopotamia/), [Royal Game of Ur](https://www.ancientpages.com/tag/royal-game-of-ur/), [Ur](https://www.ancientpages.com/tag/ur/)

### 提问：乌尔的皇家游戏是什么？

答：在古代，棋类游戏和今天一样流行。棋类游戏在古埃及、美索不达米亚和世界其他地区非常常见。

乌尔的皇家游戏是公元前2600年至2400年之间制作的棋盘游戏。这意味着这个游戏至少有4400年的历史了！

### 乌尔的皇家游戏

它是20世纪20年代在美索不达米亚（现代伊拉克）发现的，当时英国考古学家伦纳德·伍利正在挖掘一座埋藏已久的皇城废墟。在众多令人惊叹的珍宝中，他发现了几种游戏，乌尔的皇家游戏就是其中之一。

木板的木材已经腐烂，但坚硬的贝壳、红色石灰石和青金石（一种蓝色矿物）装饰图案仍然存在。计数器由页岩和贝壳制成。

另见：聚焦古埃及玩具和游戏

乌尔风格的二十方棋盘在埃及也被称为阿塞布，在图坦卡门法老墓和其他地方都有发现。一款部分描述游戏玩法的平板电脑的发现，使游戏在2000多年后得以再次玩起来，尽管对详细规则的重构存在很大差异。

乌尔的皇家游戏是由七个标记和三个四面体骰子组成的两盘，一黑一白。大约公元前1000年后，二十个广场的布局被改变，使标志的终点线成为一条直线。

美索不达米亚的游戏规则尚不完全清楚，但已有一些游戏规则的重建，这些规则是基于抄写员伊蒂·马尔杜克·巴尔（Itti Marduk Balā）于公元前177-176年制作的起源于巴比伦的楔形文字板ṭu、 人们普遍认为，乌尔的皇家游戏和塞内特一样，是一种种族游戏。

AncientPages.com

展开以获取参考

标签：古代游戏、棋类游戏、美索不达米亚、乌尔皇家游戏、乌尔


## [Royal Game of Ur](https://thcgc.gitee.io/ludii.china/details/50.Royal%20Game%20of%20Ur.html) Ludii.games

<td width="1054" valign="top">
                        
<span class="style2">

Royal Game of Ur DLP Game
</span>
                
<a target="_blank" href="https://ludii.games/lwp/GenericPlayerDemo.html?ludeme=/lud/board/race/escape/Royal Game of Ur.lud&amp;username=&amp;width=950&amp;height=400"><img src="https://images.gitee.com/uploads/images/2022/0731/032716_4fb085a0_5631341.png"></a>    
                        
                       

                        
                                        
<p class="style1">
<strong>Period</strong>
<a href="library.php?period=1&amp;region=0&amp;category=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Ancient</a></p>
                    
                            
<p class="style1">
<strong>Region</strong>
<a href="library.php?region=12&amp;category=0&amp;period=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Central Asia</a>,<a href="library.php?region=16&amp;category=0&amp;period=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Southern Asia</a>,<a href="library.php?region=17&amp;category=0&amp;period=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Western Asia</a></p>
                                            
                                    
<p class="style1">
<strong>Category</strong>
<a href="library.php?category=1&amp;region=0&amp;period=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Board</a>,<a href="library.php?category=3&amp;region=0&amp;period=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Race</a>,<a href="library.php?category=4&amp;region=0&amp;period=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Escape</a></p>
                    
<p class="style11">
<strong>Description</strong>
</p>
<p class="style1">
                            The so-called Royal Game of Ur—we do not know its ancient name—is one of the oldest known board games, dating to the third millennium BCE. While no rules have been passed down for this game specifically, a rule set based on a successor game, 20 Squares, has been applied to this slightly different board. The board consists of a grid of 3x4 squares, connected to a grid of 3x2 squares by a short bridge of 1x2 squares connecting the central rows of the other two grids. The game was played with four tetrahedral dice and 7 small disks per player as playing pieces.</p>
                        
<p class="style11">
<strong>Rules</strong>
</p>
<p class="style1">
                             4x3 grid with a 3x 2 grid connected by a "neck" of two squares. Five discs per player. Four tetrahedral dice.                            
                            
<br><br>
                            DLP Evidence.                                                    
                            
                            
                            
                                                    
                        
                            
                                                                                           
                                                                   
                                                                   
                    
                            

                            
</p>
                                        
                
                                        

<p class="style11">
<strong>All Rulesets</strong>
</p>






                                                    
                                                             
                                
                                
                                
                                
                                
                                
                            
                        
                        
                        
                        
                        
                            
                            
<p></p>
<p class="style11">
<u>Scholarly rulesets</u><br>

                                                            

<a href="variantDetails.php?keyword=Royal%20Game%20of%20Ur&amp;variant=77" style="color: #0000EE">Finkel</a>
                                 Proposed by Irving Finkel based on rules from Mesopotamia.<br>
                                
                        
                        
                        

                        
                            
                            



                        
                        
                        
                        
                                                
                        
                                
</p>
<p class="style11">
<u>Suggested rulesets</u><br>

                                                            

<a href="variantDetails.php?keyword=Royal%20Game%20of%20Ur&amp;variant=655" style="color: #0000EE">Murray</a>
                                 Rules suggested by Murray.<br>
                                                        
                        
                        
                        
                        
                        
                        

                        
                        
                                                
                        
                        
                        
                        
                        
                
</p><p class="style11">
<strong>Origin</strong>
</p>
<p class="style1">
                            Mesopotamia</p>
                                        
                                        
                
                                        
                
                        

                                                
                        
                        
                        

                                                
                        
                        
                        
                        
                        

                        
                

                        
                        
                       
                
                
                
                   
                                
                
                                

                
                
                
                        
                    
                
        
                


    
    

    
            
<p class="style11">
<strong>Sources</strong>
</p>
<p class="style1">    
<a href="https://www.academia.edu/34596109/Manufacturing_and_trade_of_Asian_elephant_ivory_in_Bronze_Age_Middle_Asia._Evidence_from_Gonur_Depe_Margiana_Turkmenistan_" target="_blank" class="style1" style="color: #0000EE">Frenez, D. 2018. Manufacturing and trade of Asian elephant ivory in Bronze Age Middle Asia. Evidence from Gonur Depe (Margiana, Turkmenistan). Archaeological Research in Asia 15: 13–33.
</a><br><br><a href="https://www.academia.edu/2163324/Recent_results_and_new_perspectives_from_the_research_at_the_graveyard_of_Shahr-i_Sokhta_Sistan_Iran" target="_blank" class="style1" style="color: #0000EE">Piperno, M and S. Salvatori. 1983. Recent results and new perspectives from the research at the graveyard of Shahr-I-Sokhta, Sistan, Iran. Annali 43: 172–191.
</a><br><br><a href="https://archive.org/details/NecropolisOfGonur/page/n4/mode/2up" target="_blank" class="style1" style="color: #0000EE">Sarianidi, V. I. 2007. Necropolis of Gonur. Athens: Kapon Editions.
</a><br><br><a href="http://www.etana.org/sites/default/files/coretexts/20263.pdf" target="_blank" class="style1" style="color: #0000EE">Woolley, C.L. 1934. Ur Excavations. Volume 2, The Royal Cemetery. Oxford: Oxford University Press.
</a><br><br></p>


                             
                 
                             
        


<p class="style11">
<strong>Identifiers</strong>
</p>
<p class="style1">
             DLP.Games.50  
</p>
                
        
        
        
                
        


                
</td>

## [20 Squares](https://thcgc.gitee.io/ludii.china/details/7.20%20Squares.html) (Aseb) Ludii.games

<td width="1054" valign="top">
<br>
20 Squares (<i>É Er-bé-et-ta, Iseb, Aseb, Game of Twenty, Twenty Squares, Room Four</i>)<span style="float:right;color:red;">DLP Game &nbsp;&nbsp;                
                
<p class="style11">
</p>
                                                
                
<a target="_blank" href="https://ludii.games/lwp/GenericPlayerDemo.html?ludeme=/lud/board/race/escape/20 Squares.lud&amp;username=&amp;width=950&amp;height=400"><img src="https://images.gitee.com/uploads/images/2022/0731/032042_d216f3b9_5631341.png"></a>    
                        
                                        
<p class="style1">
<strong>Period</strong>
<a href="library.php?period=1&amp;region=0&amp;category=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Ancient</a></p>
                    
                            
<p class="style1">
<strong>Region</strong>
<a href="library.php?region=4&amp;category=0&amp;period=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Northern Africa</a>,<a href="library.php?region=16&amp;category=0&amp;period=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Southern Asia</a>,<a href="library.php?region=17&amp;category=0&amp;period=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Western Asia</a></p>
                                            
                                    
<p class="style1">
<strong>Category</strong>
<a href="library.php?category=1&amp;region=0&amp;period=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Board</a>,<a href="library.php?category=3&amp;region=0&amp;period=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Race</a>,<a href="library.php?category=4&amp;region=0&amp;period=0&amp;gameName=" target="_blank" class="style1" style="color: #0000EE">Escape</a></p>
                    
<p class="style11">
<strong>Description</strong>
</p>
<p class="style1">
                            20 Squares appears for the first time at the beginning of the second millennium BCE. It seems to be derived originally from the the Royal Game of Ur, becoming particularly popular in Iran, the Levant, Egypt, and Cyprus in addition to Mesopotamia during the Late Bronze Age (1700–1050 BCE), and continued being played in Mesopotamia into the Seleucid period, at least until the second century BCE. Two cuneiform tablets which attest to the rules of the game as played in Seleucid times have recently been translated. There has been some discussion over the ancient name of the game. The best argument is made by Wee (2018: 870) who identified the name as É Er-bé-et-ta, "Room Four," which he also finds in other cuneiform tablets. Others have pointed to a Middle Kingdom Egyptian tomb painting at Beni Hasan which may show two men playing the game, accompanied by the caption "iseb" or "aseb" (Crist et al 2016: 85–87). Earlier scholars assumed that a game named "Tjau" mentioned in some Late Period Egyptian texts was this game (e.g., Peterson 1974: 853), but that has been rejected by more recent scholars (Pusch 2007: 84).</p>
                        
<p class="style11">
<strong>Rules</strong>
</p>
<p class="style1">
                            3x4 grid with an extension of eight spaces along the central row. Played with two knucklebones, one small and one large. Five or seven pieces per player.                             
                            
                                                    
                            
                            
                            
                                                    
                        
                            
                                                                                           
                                                                   
                                                                   
                    
                            

                            
</p>
                                        
                
                                        

<p class="style11">
<strong>All Rulesets</strong>
</p>






                                                    
                                                             
                                
                                
                                
                                
                                
                                
                            
                        
                        
                        
                        
                        
                            
                            
<p></p>
<p class="style11">
<u>Scholarly rulesets</u><br>

                                                            

<a href="variantDetails.php?keyword=20%20Squares&amp;variant=35" style="color: #0000EE">Seleucid</a>
                                 Proposed by Irving Finkel based on rules from Mesopotamia.<br>
                                
                        
                        
                        

                        
                            
                            



                        
                        
                        
                        
                                                
                        
                                                        
                        
                        
                        
                        
                        
                        

                        
                        
                        
</p>
<p class="style11">
<u>Incomplete rulesets</u><br>

                                                                    

<a href="variantDetails.php?keyword=20%20Squares&amp;variant=11" style="color: #0000EE">Simple 20 Squares</a>
                                     Played on a board with no markings.<br>
                                                                        

<a href="variantDetails.php?keyword=20%20Squares&amp;variant=12" style="color: #0000EE">Marked 20 Squares</a>
                                     Played on a board with certain marked squares.<br>
                                                                        

<a href="variantDetails.php?keyword=20%20Squares&amp;variant=13" style="color: #0000EE">20 Squares Liver Model</a>
                                     Played on a board shaped to resemble a liver.<br>
                                                                        

<a href="variantDetails.php?keyword=20%20Squares&amp;variant=14" style="color: #0000EE">Double 20 Squares</a>
                                     Played on a doubled 20 Squares board.<br>
                                                            
                        
                        
                        
                        
                        
                
</p><p class="style11">
<strong>Origin</strong>
</p>
<p class="style1">
                            Mesopotamia</p>
                                        
                                        
                
                                        
                
<p class="style11">
<strong>Ludeme Description</strong>
</p>
<p>
<a href="../lud/7.20 Squares.lud" target="_blank" class="style1" style="color: #0000EE">20 Squares.lud</a></p>
                        
<!--
<p class="style11">
<strong>Credit lud File</strong>
</p>
<p class="style1">
<a href=search.php?id=DLP.Games.credit.Eric%20Piette target="_blank" class="style1" style="color: #0000EE" />Eric Piette</a></p>
                                                -->
                        
                        

                        
                

                        
                        
                       
                
                
                
                   
                                
                
                                

                
                
                
                        
                    
                
        
                


    
    

    
            
            
            


                             
                 
                             
        


<p class="style11">
<strong>Identifiers</strong>
</p>
<p class="style1">
             DLP.Games.7  
</p>
<a href="https://boardgamegeek.com/boardgame/23211">
<p class="style1" style="color: #0000EE">
             BGG.23211  
</p>
</a>
                
        
        
        
                
        


                
</td>

# [Hounds and Jackals](https://thcgc.gitee.io/ludii.china/details/8.58%20Holes.html) 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0630/191734_243d4779_5631341.png "屏幕截图.png")

## ancient board game 

Beautifully carved ivory game pieces gave the commonly called game of Hounds and Jackals its name.

### [Hounds and Jackals game](https://allaboutfunandgames.com/the-ancient-board-game-of-hounds-and-jackals)
Ten carved ivory pegs, with heads resembling five hounds and five jackals, were excavated from a tomb at Thebes by Howard Carter in the 1900’s.  The original ivory board with its game pieces was discovered as a complete set.  The rectangular playing surface, with rounded top, measured six inches long and three inches wide.  The animal headed pegs were contained in a drawer underneath; locked by an ivory bolt.  Dating back to around 2000BC, the game was a splendid find, and now rests in the Metropolitan Museum of New York.

Although rules for the game have never been found, Hounds and Jackals (also called The Palm Tree game or 58 holes,) is believed to be have been a popular race game of the time.  Many game boards, either identical or similar in nature, have been discovered across the region.  The boards consist of a path formed by the holes in which the stick-like pieces are placed.  Players navigate the track by rolls of chance (either by dice, sticks, or knucklebones).

The tracks contains a few spaces which are specially marked.   Lines are drawn, connecting two holes, which seem to enable players to move several spaces forward if landed upon.  Perhaps backwards even.  No one knows for sure how the game was exactly played.  However, ways for the game to be played today have been constructed based from experimentation and study.


### ancient game board
Like mentioned, the game is believed to be a simple race game.  Two players take turns moving their pieces according to chance rolls on one side of the board. The winner of the game is the first person to move his pieces to a designated spot.  In some rules, this is at the golden central space of the tree.  A player would launch his pieces at the top of the board, move down the side of the board and back up one side of the trunk of the tree.  Other rules were as follows and went the reverse:

Each player begins moving a piece from the first hole beside the trunk of the tree.  Travelling down the side of the tree and then to the outer side of the board, a player strives to reach the top as quickly as possible.  Landing on space 6 would allow a player to move to space 20 and landing on space 8 leaps to space 10.  It is believed, landing on 20 or 10 would then require the player to move backwards.

Players are not allowed to launch their pieces onto the board until he throws a one.  When a one is thrown, his piece is introduced to the game at the center gold space at the top of the tree (top of board, playing  the other way).  A player immediately rolls again and it is this roll which determines the initial movement onto the holes of the playing surface.

Obviously, only once piece per hole is allowed.  Since it is believed each player traverses their own side of the board, there isn’t need for a rule which decides what to do if a player lands upon an opponent’s space.

The game ends once a player achieves the goal of navigating all five of his pieces off the board first.  The thirtieth space was the winning position.  The previous 29, on each side, gave rise to the name of 58 holes.  Even though Hounds and Jackals is an ancient game, playing and exploring the game is still enjoyable today.


## 古代棋类游戏

雕刻精美的象牙游戏件使人们通常称之为猎犬和豺狼游戏。

### 猎犬和豺狼游戏

20世纪，霍华德·卡特从底比斯的一座坟墓中挖掘出了十个雕刻的象牙钉，其头部类似于五只猎犬和五只豺狼。原来的象牙板及其游戏件被发现作为一套完整的。长方形的比赛场地，顶部为圆形，长6英寸，宽3英寸。动物头饰钉被放在下面的一个抽屉里；被象牙色的螺栓锁住。可以追溯到公元前2000年左右，这项运动是一项了不起的发现，现在保存在纽约大都会博物馆。



虽然游戏规则从未被发现，但猎犬和豺狼（也称为棕榈树游戏或58洞）被认为是当时流行的种族游戏。在该地区发现了许多性质相同或相似的游戏板。这些板由一条由孔形成的路径组成，孔中放置着棒状的零件。玩家通过掷骰（骰子、棍子或指节骨）在轨道上导航。



轨道包含一些特别标记的空间。画出了连接两个洞的线，这似乎可以让玩家在着陆时向前移动几个空间。甚至可能是倒退。没有人确切知道这场比赛到底是如何进行的。然而，今天玩这个游戏的方式是基于实验和研究构建的。



### 古代游戏板

如前所述，该游戏被认为是一个简单的比赛游戏。两名球员轮流根据棋盘一侧的机会掷骰移动他们的棋子。游戏的获胜者是第一个将棋子移动到指定位置的人。在某些规则中，这是在树的金色中心空间。玩家将在棋盘的顶部开始他的棋子，沿着棋盘的一侧向下移动，然后向树干的一侧后退。其他规则如下，反之亦然：



每个玩家从树干旁的第一个洞开始移动一块。沿着树的一侧向下移动，然后到达板的外侧，玩家努力尽快到达顶部。在6号空间着陆将允许玩家移动到20号空间，在8号空间着陆会跳到10号空间。人们认为，在20号或10号空间着陆后，玩家需要向后移动。



球员在投出一个棋子之前不得将其棋子掷到棋盘上。当投掷一个棋子时，他的棋子将被引入到树顶部的中心黄金区域（棋盘顶部，以另一种方式玩）。玩家立即再次翻滚，正是这一翻滚决定了玩家在游戏表面的洞上的初始移动。



显然，每个孔只允许一次。因为大家都认为每个玩家都会穿过自己的棋盘，所以不需要一条规则来决定当一个玩家降落在对手的棋盘上时该怎么做。



一旦玩家实现了先从棋盘上导航所有五个棋子的目标，游戏就结束了。第30格是获胜的位置。之前的29个洞，每边都有58个洞。尽管猎犬和豺狼是一种古老的游戏，但在今天玩和探索这项游戏仍然是一种享受。

- [Ludii.games](https://thcgc.gitee.io/ludii.china/details/8.58%20Holes.html)

---

<p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/173616_c38b9a98_5631341.png"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/173624_37173cae_5631341.png"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/173705_38226326_5631341.png"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/174210_279ba1df_5631341.jpeg" title="Faience-Senet-Gaming-Board-Inscribed-for-Amenhotep-III-from-Thebes-Brooklyn-Museum-49.56a-b.jpg"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/174238_a693ba13_5631341.jpeg"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/181913_24a4be33_5631341.jpeg"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/191238_3a407c7a_5631341.png"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/191659_a4468e93_5631341.png"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/173640_cd7e6515_5631341.png"></p>

【笔记】[Senet & 20 Squares & Hounds and Jackals](https://gitee.com/luckyludii/LuckyLudii/issues/I5EXDJ)

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/180752_2d8bad62_5631341.jpeg" title="hb_30.4.145.jpg">
</td>
<td>
<a href="https://www.metmuseum.org/toah/works-of-art/30.4.145/" target="_blank">Queen Nefertari Playing Senet | Nina deGaris Davies | 30.4.145 | Work ...</a><br>
Metropolitan Museum of ArtMetropolitan Museum of Art|1612 × 1500 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/180947_1782d8c1_5631341.jpeg" title="tempxwfbrettsenetgjpg100__v-gseagaleriexl.jpg">
</td>
<td>
<a href="https://www.planet-wissen.de/gesellschaft/spiele_und_spielzeug/brettspiele_spass_seit_jahrtausenden/pwiesenetdasaeltestebrettspiel100.html" target="_blank">Brettspiele: Senet - Spiele und Spielzeug - Gesellschaft - Planet Wissen</a><br>
planet-wissen.deplanet-wissen.de|1600 × 900 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/181047_cf1941e6_5631341.png">
</td>
<td>
<a href="https://www.thegreatapps.com/apps/egyptian-senet" target="_blank">Egyptian Senet</a><br>
thegreatapps.com|1280 × 800 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/181136_5caf8637_5631341.png">
</td>
<td>
<a href="https://www.elo7.com.br/senet-jogo-egipcio-da-morte-e-o-mais-antigo-do-mundo/dp/165D0E5" target="_blank">Senet jogo Egipcio da morte e o mais antigo do mundo no Elo7 | Mercado ...</a><br>
elo7.com.br|1200 × 900 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/181230_9421c15a_5631341.jpeg" title="tempxwfbrettnefertarigjpg100__v-gseagaleriexl.jpg">
</td>
<td>
<a href="https://www.planet-wissen.de/gesellschaft/spiele_und_spielzeug/brettspiele_spass_seit_jahrtausenden/pwiesenetdasaeltestebrettspiel100.html" target="_blank">Brettspiele: Senet - Spiele und Spielzeug - Gesellschaft - Planet Wissen</a><br>
planet-wissen.deplanet-wissen.de|1600 × 900 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>


<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/181610_5363220b_5631341.jpeg" title="IMG_20210612_101821-scaled (1).jpg">
</td>
<td>
<a href="../../STEM/HistoryUnboxed.md" target="_blank"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0704/012306_7999e710_5631341.jpeg" title="161.HistoryUnboxed"></a>
</td>
<td>
<a href="https://www.historyunboxed.com/shop/museum-craft-kits/senet-kit-ancient-egypt/" target="_blank">Senet Kit (Ancient Egypt) - History Unboxed</a><br>
historyunboxed.com|2204 × 2560 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>


<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/182124_c03fd8d4_5631341.png">
</td>
<td>
<a href="https://www.dreamstime.com/editorial-stock-photo-egyptian-game-senet-one-oldest-known-board-games-b-c-father-chess-image40948313" target="_blank">Egyptian Game of Senet editorial stock photo. Image of knight - 40948313</a><br>
DreamstimeDreamstime|1300 × 957 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>


<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/182244_08b1094f_5631341.jpeg" title="wall-painting-from-the-tomb-of-nefertari-the-queen-playing-senet-DE4180.jpg">
</td>
<td>
<a href="https://www.alamy.com/stock-photo-wall-painting-from-the-tomb-of-nefertari-the-queen-playing-senet-60325104.html" target="_blank">Wall painting from the tomb of Nefertari: the Queen playing senet Stock ...</a><br>
AlamyAlamy|1296 × 1390 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>


<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/182353_eada6a7d_5631341.jpeg" title="Senet-Imenmes-3BM.jpg">
</td>
<td>
<a href="http://projetrosette.info/page.php?Id=799&TextId=194" target="_blank">Corpus de textes</a><br>
projetrosette.info|3230 × 2068 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/182641_91460b10_5631341.png" title="Game Piece - Walters 199722.jpg - Wikimedia Commons">
</td>
<td>
<a href="File:Egyptian - "Senet" Game Piece - Walters 199722.jpg - Wikimedia Commons" target="_blank">File:Egyptian - "Senet" Game Piece - Walters 199722.jpg - Wikimedia Commons</a><br>
WikimediaWikimedia|1024 × 1024 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/182858_1b812578_5631341.jpeg" title="1451719382-f701635bbc26d98c07878f2a0f98c0cf2ca9e677029f00b5a112c9e956bdc1d1-d.jpg">
</td>
<td>
<a href="https://vimeo.com/720744758" target="_blank">Egyptian Senet History & Tutorial (Arabic) on Vimeo</a><br>
vimeo.com|1920 × 1080 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/183025_099eb3d9_5631341.jpeg" title="egyptian-senet-noble-expansion.jpg">
</td>
<td>
<a href="https://3dcg.org/modules/database/detail.php?id=17095" target="_blank">Olympia 8 - DAZ 3D Models - 3D CG</a><br>
3dcg.org|3000 × 3200 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/183359_b455b278_5631341.png">
</td>
<td>
<a href="https://www.slideserve.com/saxton/senet" target="_blank">PPT - SENET PowerPoint Presentation, free download - ID:5415367|Watch now</a><br>
SlideServeSlideServe|1024 × 768 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/184313_2a71ff5f_5631341.png" title="https://www.pivotalgamers.com/2018/12/05/free-egyptian-senet/">
</td>
<td>
<a href="https://www.pivotalgamers.com/2018/12/05/free-egyptian-senet/" target="_blank">Free Egyptian Senet! - Pivotal Gamers</a><br>
pivotalgamers.com|1620 × 800 jpeg|Image may be subject to copyright.<br>
<br>
The # 1 SENET Game Is Now Available On Steam. Don't Miss The Opportunity To Play The World's Oldest Board Game From Ancient Egypt. Start Your Mysterious Afterlife Journey To Pass Through The Underworld Realm Of Osiris.
</td>
</tr>
<tr>
<td colspan=2>
Now, thanks to Orlygift.com, you can play Egyptian Senet, one of the world’s oldest board games for free! Just click the red button, sign in with your Steam account and get your copy now! This is a limited-time offer so you’d better hurry! <a href="https://www.orlygift.com/promotion/egyptian-senet-giveaway" target="_blank">Get it Now!</a>
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/192135_ce2b8145_5631341.png">
</td>
<td>
<a href="https://www.kickstarter.com/projects/1068290755/senet-and-aseb-ancient-egypt-board-games" target="_blank">SENET & ASEB: Ancient Egypt board games. by Joan Antoni Salort ...</a><br>
KickstarterKickstarter|680 × 683 png|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/200931_b3a538ca_5631341.jpeg" title="Royal-Game-of-Ur-British-Museum-19281009.379.a.jpg">
</td>
<td>
<a href="http://www.ancientgames.org/category/all-games/mesopotamian-games/" target="_blank">Mesopotamian Games Archives - Ancient Games - Playing the Board Games ...</a><br>
ancientgames.org|4032 × 3024 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/201046_e7cc2197_5631341.png">
</td>
<td>
<a href="https://www.historygames.it/en/great-ancient-civilizations-boardgames/" target="_blank">Great Ancient Civilizations Board Games</a><br>
historygames.it|1350 × 800 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/201050_7f198a7a_5631341.png">
</td>
<td>
<a href="https://www.dailysabah.com/life/2018/11/27/in-iraq-an-ancient-board-game-is-making-a-comeback" target="_blank">In Iraq, an ancient board game is making a comeback - Daily Sabah</a><br>
Daily SabahDaily Sabah|645 × 550 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>

<table>
<tr>
<td>
<img height="99px" src="https://images.gitee.com/uploads/images/2022/0630/201434_31264b7e_5631341.jpeg" title="il_fullxfull.jpg">
</td>
<td>
<a href="https://www.ignitedartsdesign.com/listing/807941522/royal-game-of-ur-egyptian-senet-two" target="_blank">Royal Game of Ur / Egyptian Senet: Two Ancient Middle Eastern Board ...</a><br>
ignitedartsdesign.com|3000 × 3000 jpeg|Image may be subject to copyright.
</td>
</tr>
</table>
 
（认真看，它们都是我找来的宝藏，能否给你灵感，创造出属于你的 Senet 套装呢？记得署上你名字，赋予它新生！:pray: [看我的](../../STEM/HistoryUnboxed.md)！）
