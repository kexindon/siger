# 石头爱学习

[<img width="212px" src="https://images.gitee.com/uploads/images/2022/0410/015932_a98141f1_5631341.jpeg" title="科幻小说 786P/3027 T1彗星 (1-10)">](石头爱学习.md)

[SIGer 第92期 专题 石头爱学习](石头爱学习.md)

- [石头爱学习](https://gitee.com/rockrockyan/siger/issues/I51EJQ#note_9635269_link)
- [何为克莱因瓶](https://mp.weixin.qq.com/s/wrWrxxRo8rfHG3Ks_YKuAA)
- [石头爱分享](https://gitee.com/rockrockyan/siger/issues/I51EJQ#note_9681103_link) | [小志愿者](https://mp.weixin.qq.com/mp/homepage?__biz=MzU4Njc2OTYxNg==&hid=3&sn=4857709c0c02941dd91f586a25e67146) | [爱作文](https://mp.weixin.qq.com/mp/homepage?__biz=MzU4Njc2OTYxNg==&hid=6&sn=49a940801998a205a9f96d6dc8662af2)
- 科幻小说 [786P/3027 T1彗星](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzU4Njc2OTYxNg==&action=getalbum&album_id=1777111329531592706) (1-10)

## 关于公众号

### 公众号简介：

小石头，十三岁，喜欢编程，喜爱天文，热爱公益，参加了多项志愿者讲解活动；在妈妈帮助下开了公众号，感谢妈妈协助我进行管理和维护。辛苦妈妈。

### 基础信息

- 微信号：Rock_Study
- 帐号主体：个人

### 名称记录

- 2020年12月16日 “石头爱天文” 改名 “石头爱学习”

### 石头爱学习提供的服务

- 爱学习

  - [石头开讲](https://mp.weixin.qq.com/mp/video?__biz=MzU4Njc2OTYxNg==&mid=100000126&sn=9827e2d3693196ddf677aa841ddd7a87&vid=wxv_1100521791870681089&idx=1&vidsn=4d7cd7cf49a2774ec61ae7e24fbbd3da) - [小说创作](https://mp.weixin.qq.com/mp/homepage?__biz=MzU4Njc2OTYxNg==&hid=9&sn=682743a41c592d46d0edbe25f3bce493) - [我爱编程](https://mp.weixin.qq.com/mp/homepage?__biz=MzU4Njc2OTYxNg==&hid=8&sn=f7db14ac85a94845142d067922344bee) - [天文习作](https://mp.weixin.qq.com/mp/homepage?__biz=MzU4Njc2OTYxNg==&hid=4&sn=c8d744da709157861864e4f9a68df8d7) - 天文知识
  
- 爱分享

  - [小志愿者](https://mp.weixin.qq.com/mp/homepage?__biz=MzU4Njc2OTYxNg==&hid=3&sn=4857709c0c02941dd91f586a25e67146) - 快乐游玩 - [兴趣爱好](https://mp.weixin.qq.com/s/bs43OhaTcPx5lbS1dSjnAg) - [我爱作文](https://mp.weixin.qq.com/mp/homepage?__biz=MzU4Njc2OTYxNg==&hid=6&sn=49a940801998a205a9f96d6dc8662af2) - 我爱阅读
  
- 爱父母

  - [妈妈分享](https://mp.weixin.qq.com/mp/homepage?__biz=MzU4Njc2OTYxNg==&hid=7&sn=ef9db42101701e47585cd34b3cd62ef9) - 爸爸分享 - [家有美食](https://mp.weixin.qq.com/mp/homepage?__biz=MzU4Njc2OTYxNg==&hid=5&sn=84fff2715c159d72b263bd795628cfd1) - 成长点滴 - 共同学习