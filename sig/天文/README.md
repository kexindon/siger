- [sig/天文 征稿启事！](https://gitee.com/MadNug/siger/issues/I5U3MB) 2022/10/1 主编 @[林书平](https://gitee.com/MadNug/siger/issues) 邮箱：SIGerAstrophysics@snapmail.cc
- [天文摄影——十月星闻](天文摄影——十月星闻.md) 2022/10/31 @[林书平](https://gitee.com/MadNug/siger/issues)
- [天文摄影——九月星闻](天文摄影——九月星闻.md) 2022/10/8 @[林书平](https://gitee.com/MadNug/siger/issues)
- [sig/天文 频道敬贺 天爱500期](天爱500期.md) 2022/10/1

<table>
<tr>
<td width="59%">
<p align="center">
<a target="_blank" href="https://gitee.com/MadNug/siger/issues/I5U3MB">
<img width="95%" src="https://foruda.gitee.com/images/1665160565115044416/2bfa332e_5631341.jpeg" title="天文 征稿启事！.jpg"></a></p>
</td>
<td width="29%">
<p align="center">
<a target="_blank" href="天文摄影——九月星闻.md">
<img width="66%" src="https://foruda.gitee.com/images/1665254909918858663/67b8164a_5631341.jpeg" title="天文摄影——九月星闻.jpg"></a><br>
<a target="_blank" href="天文摄影——十月星闻.md">
<img width="66%" src="https://foruda.gitee.com/images/1670052300408408580/871078b6_5631341.jpeg" title="天文摄影——十月星闻.jpg"></a><br>
<a target="_blank" href="https://gitee.com/MadNug/siger/issues/I648UE">
<img width="66%" src="https://foruda.gitee.com/images/1670056943158676807/6a0c4fa1_5631341.jpeg" title="天文摄影——11月星闻.jpg"></a>
</p>
</td>
</tr>
</table>

[<img width="9.99%" src="https://foruda.gitee.com/images/1664611200150742727/3a1fee5b_5631341.jpeg" title="300天文爱好者300A4.jpg">](天爱500期.md) [<img title="318." width="9.99%" src="https://foruda.gitee.com/images/1665154354558063218/ac133bb3_5631341.jpeg">](https://gitee.com/huang-zixin001/siger/issues/I5T43P) [<img title="173." width="9.99%" src="https://images.gitee.com/uploads/images/2022/0713/005017_d4e1fa97_5631341.jpeg">](这个宇宙和我有什么关系？.md) [<img title="178." width="9.99%" src="https://images.gitee.com/uploads/images/2022/0716/030252_1fdd2d7d_5631341.jpeg">](暗能量宇宙.md) [<img title="185." width="9.99%" src="https://images.gitee.com/uploads/images/2022/0720/093837_6adf7773_5631341.jpeg">](宇宙级浪漫.md) [<img title="191." width="9.99%" src="https://images.gitee.com/uploads/images/2022/0726/152423_77723ab6_5631341.jpeg">](新牛反开光篇.md) [<img width="9.99%" src="https://images.gitee.com/uploads/images/2022/0701/003617_b4e0dcbf_5631341.jpeg" title="深空摄影副本.jpg">](遥望星辰大海.md) <img title="8." width="9.99%" src="https://images.gitee.com/uploads/images/2022/0105/151259_c750761b_5631341.jpeg"> <img width="9.99%" src="https://images.gitee.com/uploads/images/2022/0608/004321_0f2ddbb4_5631341.jpeg">
