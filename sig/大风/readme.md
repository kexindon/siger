# [mo言：致所有青年朋友的一封信](https://mp.weixin.qq.com/s/tgrNz17Zd0iNngzUu7iMKQ)

mo言 mo言 2022-05-04 13:11

- mo言 微信号: moyan_gaomi
- 功能介绍: 我想和年轻人聊聊天。

大家好，我是mo言。

后台有年轻朋友问我，“ **如果人生中遇到艰难时刻，该怎么办？** ”

趁着五四青年节，

我以此为主题，录制了一封写给大家的信——

 **点击下方视频，即可观看** 

<img width="29px" src="https://images.gitee.com/uploads/images/2022/0504/204851_44245d51_5631341.png">

[<img width="59.3%" src="https://images.gitee.com/uploads/images/2022/0504/204904_6455bd53_5631341.png">](https://www.bilibili.com/video/BV1A44y1u7PF) [<img width="36%" src="https://images.gitee.com/uploads/images/2022/0504/205102_4761f406_5631341.jpeg" title=".jpg">](https://images.gitee.com/uploads/images/2022/0504/213612_2d886b99_5631341.jpeg)

最后，送大家一幅我写的书法—— **不被大风吹倒** 。

祝每一位朋友，五四青年节快乐。

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0505/005201_38cb34ca_5631341.jpeg"></p>

 **点击关注我的公众号**   
 **一起交流困惑，探讨文学和人生**   
👇👇

[![输入图片说明](https://images.gitee.com/uploads/images/2022/0504/204946_40090488_5631341.png "屏幕截图.png")](#mo言和你聊聊天)

### 《不被大风吹倒》全信

亲爱的年轻朋友：

节日好！

想起几天前你们在我的公众号留言问我：“如果人生中遇到艰难时刻，该怎么办？” 这确实是一个必须面对的重要问题。谁都不敢保证自己一生中不会遇到困难，甚至是艰难时刻。我无法告诉你一个适合所有人标准答案，但可以与你们分享两个小故事。当我遇到艰难时刻时，给我带来知识和力量的是一本书和一个人。

一本书是《新华字典》。我一生中遇到的第一个艰难时刻是童年辍学。当时，与我同龄的孩子都在学校里，他们在一起学习玩耍，而我孤零零的一个人放牛、割草，十分孤独。幸好在这个时候，我得到了一本《新华字典》。我当然也希望能阅读很多的经典作品，但当时的农村书很少，谁家有本书都视若珍宝，轻易不外借。只有这本《新华字典》是属于我的，我认识的大部分汉字，实际上都不是在学校里学的，而是在辍学之后，通过阅读这本《新华字典》学的。总之，在当年那种孤独穷困的环境里，就是这本工具书，陪着我度过了艰难时刻。而且，也为我以后，能拿起笔来写小说奠定了基础。

一个人是我爷爷。小的时候，我跟着爷爷去荒草甸子里割草。归程时天象诡异，一根飞速旋转着的黑色的圆柱向我们逼过来，并且伴随着沉闷如雷鸣的呼隆声。我惊问爷爷，那是什么？爷爷淡淡地说：“风，使劲拉车吧，孩子” 风越来越大，我们车上的草被刮扬到天上去。我被风刮倒在地，双手死死地抓住了两丛根系很深的牛筋草，才没有被风刮走。我看到爷爷双手攥着车把，脊背绷得像一张弓。他的双腿在颤抖，小褂子被风撕破，只剩下两个袖子挂在肩上。爷爷与大风对抗着，车子未能前进，但也没有后退半步。大风过去了，爷爷还保持着这个姿势，仿佛一尊雕塑。许久之后，他才慢慢地直起腰。他的手指卷曲着，都伸不开了。爷爷与狂风对峙的模样，永远印刻在我的脑海里。

那么我们是胜利者？还是失败者？风来时爷爷没有躲避，尽管风把我们车上的草，刮得只剩下一棵，我们的车还在。我们就像钉在大坝上一样，没有前进，但是也没有倒退。我觉得从这个意义上来讲，我们胜利了。

我的故事是老生常谈，不一定能让你们感兴趣，但因为这是我的亲身经历，所以还是讲给你们听。但愿能给你们带来一些启发。古人云，“道祖且长，行则将至” 年轻朋友们，当我们遇到艰难时刻，不要灰心，不要沮丧，只要努力总是会有收获。希望，总是在失望，甚至是绝望时产生的，并召唤着我们重整旗鼓，奋勇前进。一个人可以被生活打败，但是不能被它打倒。

总之我想，越是在困难的时刻，越是文学作品能够发挥它的直达人的心灵的作用的时候。

2022年 五四前夕  
mo言

### [幕后短片（一）](https://www.bilibili.com/video/BV1rB4y117tr)

 **文学又用没用呢？** 

我忽然想起来这个 2012 年，我在瑞典斯德哥尔摩领取诺贝尔文学奖的（时候）有一个发言。这个发言是，文学与科学相比好像是没有用处的，数学可以帮助计算，物理学可以帮助我们改变很多现实，化学可以帮我们生产很多产品，等等。科学是有直接的作用的，但是文学好像，不具备这样一种，直接转化成物质和财富的这样一种作用。但是文学我觉得它对人的灵魂，对人的心灵，是有力量的。

> 我想大家肯定都读过，这个海明威的《老人与海》是吧。那样一个老人面对着茫茫大海，面对着一群一群的鲨鱼，他跟大海搏斗，跟鲨鱼搏斗。尽管他捕捞的那一条最大的金枪鱼，被一轮又一轮的鲨鱼们啃得只剩下了一根鱼刺，但是他疲倦地回到了这个港口。

这个故事给我们启发了很多。我想我的这样一封信，为了就是能够唤起你的记忆，让你在你的人生走过的这种道路上回忆一下，看看有没有这样类似的故事。你也写出来可以跟大家分享，不但让年轻的朋友们，甚至像我这样的老朋友们，也能够得到启发，我想这也会让你感到满足。互相把自己的人生感悟最深的故事，讲出来给大家听。

总之我想，越是在困难的时刻，越是文学作品能够发挥它的直达人的心灵的作用的时候。希望大家无论在什么样的情况下，都不要忘记读书，都不要忘记读经典。

### [幕后短片（二）](https://www.bilibili.com/video/BV12S4y187j5)

那么我想，大家就可以从我刚才讲的故事里面，应该给大家带来一点启发吧。面对着自然界的这种暴风、狂风，你当然不可能把风战胜。但是你也无法逃跑，风来了你往那里跑，是吧。你如果跟着风跑话，它会把你刮得更远。那我爷爷，靠他一个人的力量，与迎面来的这种暴风对抗。最后了，我在车梁这个缝隙里看到了唯一的剩下的一棵草。那么我们是胜利者还是失败者？我们割的草全被刮到天上去了，我们失败了。但是我们作为两个微小的人，与大自然的这样一种，似乎是不可对抗的力量，在对峙的时候。它没有把我们刮走，我就是从这个意义上来讲，我们胜利了。

 **你对青年人有什么鼓励和祝福吗？** 

我们每个人尽管境遇不同，大家都很难保证在我们的生活当中，不会遇到困难。遇到了这种艰难时刻怎么办？我觉得我们还是应该鼓起勇气。一个人可以被生活打败，但是不能被它打倒，还是要奋斗，还是要努力。新的希望，总是在失望的时候，甚至是绝望的时候产生的。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0504/231314_4ab463b6_5631341.png "屏幕截图.png")

### mo言和你聊聊天

> 大家好，欢迎关注我的公众号。  
> 我是mo言，我想在这里和年轻人聊聊天。  
> 每周一晚9：00，我会在头条发布一篇我的文章。  
> 在二条推文中，我会发布一条语音，每期一个话题，分享我对文学和人生的看法。  
> 大家有什么想法，欢迎随时给我评论、发消息，我们一起交流困惑、探讨文学与人生。

| 2022 | mo言： | 【mo言和你聊聊天】 |
|---|---|---|
| 05-04 | [致所有青年朋友的一封信](https://mp.weixin.qq.com/s/tgrNz17Zd0iNngzUu7iMKQ) | [我最近在看哪本书？](https://mp.weixin.qq.com/s/ohlVPyHYRs5pK4N_p1eiLg) |
| 05-02 | [夜深了，给大家讲个奇人奇事](https://mp.weixin.qq.com/s/T34Ad07P9YHDXSb78hgCvw) | [平凡人需要远大理想吗？](https://mp.weixin.qq.com/s/7VCLSlYLPgh-W7mQykJzwA) |
| 04-25 | [一份网友们送我的书单](https://mp.weixin.qq.com/s/yyDGoo6VjQZVdCDXnr6x4Q) | [都是自己人，我给大家唱一段儿](https://mp.weixin.qq.com/s/gk9q8IDREHeXJFm3AyaV4A) |
| 04-18 | [423读书日，聊聊对我意义重大的书](https://mp.weixin.qq.com/s/e5JstUwt9aHxKacVHSHmHA) | [平凡人注定要平庸一生吗？](https://mp.weixin.qq.com/s?__biz=Mzg3MTY2ODQxNA==&mid=2247487345&idx=2&sn=be3a0d91a27b63f032d96a82c9ec753e) |
| 04-11 | [作家朋友系列：高人阿城](https://mp.weixin.qq.com/s/-yOhEe19-JmEBqScLMWPmg) | [分享我拍的美景](https://mp.weixin.qq.com/s/heOIcoieOGtHP9pIOWIkcw) |
| 04-04 | [写给母亲的诗](https://mp.weixin.qq.com/s/Rj55vcne1r-96KDmTZYFBA) | [和居家隔离的朋友说说话](https://mp.weixin.qq.com/s/hF0PsqCIL7Tg0YwP0Oq2IA) |
| 03-28 | [我今年做的最难忘的一件事](https://mp.weixin.qq.com/s/_VeZ7vCMP_jXxeJWDuHcrQ) | [我为什么总穿格子衬衣？](https://mp.weixin.qq.com/s/eAlexLAbGW-Vxqui-d9NKg) |
| 03-21 | [人生的意义到底是什么？](https://mp.weixin.qq.com/s/WgGuetCfSOnWngRlBAMX3w) | [分享我最近的床头书](https://mp.weixin.qq.com/s/adWvHRNOfjGW6xY0wEFVtQ) |
| 03-14 | [我的头发是怎么一点一点凋零的……](https://mp.weixin.qq.com/s/b82vJ93TU2T-q_L9F3CJPw) | [《红楼梦》中，我最喜欢哪个人物？](https://mp.weixin.qq.com/s/53-4_77nYs2GUaYORCIxLw) |
| 03-07 | [除了写作，我还有一个“隐藏技能”](https://mp.weixin.qq.com/s/011VdV4IrL2x6G8l--y8YA) | [我最想穿越到唐朝](https://mp.weixin.qq.com/s/qd9qTdwfkdNnJN0ppmQafg) |
| 02-28 | [为什么我说人有局限性不一定是坏事](https://mp.weixin.qq.com/s/RveWW3aJ-ZDfdN1MI6yoIQ) | [听说你们好奇我的精力为什么这么好？](https://mp.weixin.qq.com/s/MhMxJn4ABgGH2qMGfxTfNQ) |
| 02-21 | [我心目中的yyds](https://mp.weixin.qq.com/s/2-88hfOmj7FZmYTvm3GDKA) | [哪本书曾陪我走过人生中的艰难时刻？](https://mp.weixin.qq.com/s/FOP8a0abpvUuWDlXKpopBQ) |
| 02-14 | [谈冬奥：我在谷爱凌、苏翊鸣身上看到了什么](https://mp.weixin.qq.com/s/ZVBq9gaRl05t8w66I2b2bw) | [谈开幕式：张艺谋又“超越了”张艺谋](https://mp.weixin.qq.com/s/ouG4EghMb7Bmbebzz0jbkQ) |
| 02-07 | [我是怎么读书的？](https://mp.weixin.qq.com/s/hBAsCAJG05fUfvTGvo6Few) | [如何度过一生才不算白费？](https://mp.weixin.qq.com/s/UXsmCJdrNtLgGsb6rM4BjA) |
| 01-31 | [你有一份来自mo言的新年祝福，请查收！](https://mp.weixin.qq.com/s/Mghcy2LmKR_WMW0N4JQxqQ) | |
| 01-17 | [过去的年](https://mp.weixin.qq.com/s/YyIZi9H8xAe-WzBwvn4OOQ) | [故乡对一个人的意义是什么？](https://mp.weixin.qq.com/s/9V7SDrDc09IpRgP5EXQ2uQ) |
| 01-10 | [想了解我，可以从这本书开始](https://mp.weixin.qq.com/s/6fRg49RBssYo6_xVBnR5QQ) | [经历大风大浪后，如何保持内心平静？](https://mp.weixin.qq.com/s/g8m3BVRWyi1sdCGP8KSZGw) |
| 01-03 | [作家朋友系列：我的室友余华](https://mp.weixin.qq.com/s/JyqO_3WYTHhE5e_O1_3vdg) | [我最喜欢的书中人物，可能出乎你们的意料](https://mp.weixin.qq.com/s/202CSYDOqVRF_bmfsOezKg) |