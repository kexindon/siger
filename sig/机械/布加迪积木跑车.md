- [布加迪积木跑车结构简介](https://gitee.com/zhangshuokai/siger/issues/I5M69S)
- [Lockdown Lego project: we build a £330 Bugatti Chiron](https://www.motoringresearch.com/car-news/video-lego-bugatti-chiron-build/)
- [Video: Lego releases James Bond Aston Martin DB5 – with standard ejector seat](https://www.motoringresearch.com/car-news/lego-release-james-bond-aston-martin-db5/)

<p><img width="706px" src="https://foruda.gitee.com/images/1660408689516130680/235lego_bugatti_chiron.jpeg" title="235LEGO_Bugatti_Chiron.jpg"></p>

封面是一个模型与真车的比对图，布加迪能满足人们所有对车神的向往，无论老幼。这是在输入 “Lego Bugatti Chiron ” 后的斩获，先前是搜索真车的照片，也有不少斩获附在文末。这些都不能掩盖一篇饱含激情的即说稿，它就出自我们的编委，机械频道的主编 @zhangshuokai 同学啦。连续两篇精彩车文，连同其他同学的同类分享，这个频道的内容一定会是丰满的。兵马未到，粮草先行，这之前更需要主将的任命。让我们期待机械频道的快速成长吧！

> 两篇英文推介，是封面图的出处，原来 LEGO 也是有套路的，看着影视红人 007 的座驾搬到 LEGO 旗舰店就知道以后我们的机械频道要怎样地和我们的生活紧密相连啦吧。今天的内容整理，有种制作邮集的感觉。原来爱好都是相通的，最后都是要有“秀”的环节，我们交分享。这是 SIGer 的源动力！

# [布加迪积木跑车结构简介](https://gitee.com/zhangshuokai/siger/issues/I5M69S)

源于对汽车的爱好，暑假当然少不了拼装跑车之作，布加迪来啦！让我们一起来揭秘经典跑车之谜，挖掘由内到外的精致细节。

![输入图片说明](https://foruda.gitee.com/images/1660377539506326747/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220813155142.jpeg "在这里输入图片标题")

带有活动式尾翼的流线型车身，浑然一体，造型令人热血沸腾。

![输入图片说明](https://foruda.gitee.com/images/1660377673902253853/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220813155034.jpeg "在这里输入图片标题")

![输入图片说明](https://foruda.gitee.com/images/1660378456648231449/%E6%8D%95%E8%8E%B71.png "在这里输入图片标题")

低扁平轮胎可随方向盘实现多方位自由转动，位于方向盘下方的换挡拨片与座侧的8级变速操作杆，在指尖操作间即可释放驰骋的速度与激情。

![输入图片说明](https://foruda.gitee.com/images/1660377854889284211/%E6%8D%95%E8%8E%B7.png "在这里输入图片标题")

带有移动活塞的W16发动机，真实模拟引擎的咆哮和轰鸣。

![输入图片说明](https://foruda.gitee.com/images/1660377964615707956/%E6%8D%95%E8%8E%B72.png "在这里输入图片标题")

逼真复刻，机械传动，拼搭探索。

![输入图片说明](https://foruda.gitee.com/images/1660378330919929620/%E6%8D%95%E8%8E%B74.png "在这里输入图片标题")

![输入图片说明](https://foruda.gitee.com/images/1660378347925901522/%E6%8D%95%E8%8E%B75.png "在这里输入图片标题")

# [Lockdown Lego project: we build a £330 Bugatti Chiron](https://www.motoringresearch.com/car-news/video-lego-bugatti-chiron-build/)

> Bored during the Covid-19 lockdown? The Lego Technic Bugatti Chiron consists of 3,599 pieces and took us 15 hours to complete. Watch our build video here.

Ethan Jupp APRIL 21, 2020

![Lego Bugatti Chiron](https://foruda.gitee.com/images/1660405019923492684/03_lego_bugatti_chiron.jpeg "在这里输入图片标题")

We’ve only gone and got ourselves a Bugatti Chiron to test. OK, it’s not the actual, 1,500hp, £2 million hypercar. Nor when you buy a real Chiron, does Bugatti ask you to build it yourself.

We are, of course, talking about the Lego Technic Bugatti Chiron – and it’s very nearly as cool as the real thing. It’s a great project to fill your time during lockdown.

### More LEGO cars on Motoring Research

Motoring Research’s photo and video guru, Bradley, fancied himself as a junior Bugatti technician, so we handed the Bug-build over to him.

The car is comprised of 3,599 individual pieces, contained within six boxes. The process is spread over 970 steps and took Bradley more than 15 hours to complete – handily caught on video and condensed into 16 minutes here.

[VIDEO]

This project might seem nightmarish to some, but Bradley really enjoyed it. And seeing the finished Lego car – a sizeable 560mm long, 250mm wide and 140mm tall – makes all his efforts seem worthwhile. It looks fantastic.

The attention to detail is stunning, from the LED light clusters at the front to the tan interior and engine with working pistons. Drive, reverse or neutral gear options dictate how those pistons leap around in the cylinders as you roll along.

There’s even a Bugatti-branded overnight bag under the bonnet.

At the back, the real magic happens. A ‘key’, modelled on an actual Chiron key, can activate the rear wing and air brake.

![Lego Bugatti Chiron](https://foruda.gitee.com/images/1660405519696942309/屏幕截图.png "屏幕截图.png")

![Lego Bugatti Chiron](https://foruda.gitee.com/images/1660405552063639940/屏幕截图.png "屏幕截图.png")

![Lego Bugatti Chiron](https://foruda.gitee.com/images/1660405594399102987/屏幕截图.png "屏幕截图.png")

![Lego Bugatti Chiron](https://foruda.gitee.com/images/1660405620417156888/屏幕截图.png "屏幕截图.png")

![Lego Bugatti Chiron](https://foruda.gitee.com/images/1660405648932006371/屏幕截图.png "屏幕截图.png")

Our Chiron comes with its very own serial number, as they all do, which can be used on the Lego Technic website to unlock special content.

Next up, we’re hoping to tackle James Bond’s [Aston Martin DB5](https://www.motoringresearch.com/car-news/lego-release-james-bond-aston-martin-db5/). That should pass another day or two in isolation…

# [Video: Lego releases James Bond Aston Martin DB5 – with standard ejector seat](https://www.motoringresearch.com/car-news/lego-release-james-bond-aston-martin-db5/)

> What might be the most famous car of all time – Bond’s Aston Martin DB5 – has been immortalised in Lego. “Listen carefully, 007” – it’s got all the toys…

![输入图片说明](https://foruda.gitee.com/images/1660405808854052356/屏幕截图.png "屏幕截图.png")

<p><img width="32.1%" src="https://foruda.gitee.com/images/1660406026960748801/img_5639.jpeg"> <img width="32.1%" src="https://foruda.gitee.com/images/1660406045945266508/img_5538.jpeg"> <img width="32.1%" src="https://foruda.gitee.com/images/1660406058902761762/img_5559.jpeg"> <img width="32.1%" src="https://foruda.gitee.com/images/1660406072493382626/img_5534.jpeg"> <img width="32.1%" src="https://foruda.gitee.com/images/1660406084108665292/img_5523.jpeg"> <img width="32.1%" src="https://foruda.gitee.com/images/1660406099588002543/img_5527.jpeg"> <img width="32.1%" src="https://foruda.gitee.com/images/1660406114446543484/img_5569.jpeg"> <img width="32.1%" src="https://foruda.gitee.com/images/1660406129725639652/img_5562.jpeg"> <img width="32.1%" src="https://foruda.gitee.com/images/1660406141934762220/img_5553.jpeg"> <img width="32.1%" src="https://foruda.gitee.com/images/1660406157339633734/img_5548.jpeg"> <img width="32.1%" src="https://foruda.gitee.com/images/1660406175819863611/img_5572.jpeg"> <img width="32.1%" src="https://foruda.gitee.com/images/1660406189334693296/img_5613.jpeg"></p>

![输入图片说明](https://foruda.gitee.com/images/1660405755562756468/屏幕截图.png "屏幕截图.png")

---

[Bugatti Chiron Is Not Going To Get A Roadster Version](https://www.nashvillechatterclass.com/bugatti-chiron-not-going-get-roadster-version/10551/)
nashvillechatterclass.comnashvillechatterclass.com|2048 × 1360 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660403893465474952/2017-bugatti-chiron-front.jpeg "2017-Bugatti-Chiron-front.jpg")

[New Model Announcement: Bugatti Chiron Pur Sport - AUTOMOTIVE RHYTHMS](https://www.automotiverhythms.com/new-model-announcement-bugatti-chiron-pur-sport/)
automotiverhythms.com|1920 × 1080 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660404034753901345/bugatti_chiron_pur_spor001.jpeg "Bugatti_Chiron_Pur_Spor001.jpg")

[A Brief History of Bugatti’s “Macaron” Emblem and How Its Currently ...](https://www.autoevolution.com/news/a-brief-history-of-bugattis-macaron-emblem-and-how-its-currently-made-150413.html)
autoevolution.comautoevolution.com|1680 × 1050 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660404085361885350/a-brief-history-of-bugattis-macaron-emblem-and-how-its-currently-made_6.jpeg "a-brief-history-of-bugattis-macaron-emblem-and-how-its-currently-made_6.jpg")

[First Bugatti Chiron Delivered to Southeast Asia, in Singapore Customer ...](http://senatus.net/article/first-bugatti-chiron-delivered-southeast-asia-singapore-customer/)
senatus.net|1900 × 2850 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660404147602373221/senatus_ldethk.jpeg "senatus_LDEthk.jpg")

[BUGATTI Leusden, Netherlands](https://www.partner.bugatti/netherlands/)
partner.bugatti|1280 × 854 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660404257561308297/se-image-399e1fdfea641e2de95617940f7c5321.jpeg "se-image-399e1fdfea641e2de95617940f7c5321.jpg")

[Bugatti Chiron makes its South-East Asia debut in Singapore | Torque](https://www.torque.com.sg/news/bugatti-chiron-makes-south-east-asia-debut-singapore/)
torque.com.sg|4000 × 2666 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660404314154444058/bugatti-chiron-bugatti-chiron-wearnes-automotive-formula-1-singapore-grand-prix-f1-singapore-grand-prix-pic1.jpeg "bugatti-chiron-bugatti-chiron-wearnes-automotive-formula-1-singapore-grand-prix-f1-singapore-grand-prix-pic1.jpg")

[Bugatti Chiron Pur Sport continues its tour through Europ](https://www.bugatti.com/media/news/2020/bugatti-chiron-pur-sport-on-tour-through-the-cities-of-europe/)
bugatti.com|1920 × 800 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660404471270129371/se-image-b52ad9ff452db8d52e09710db4976ec2.jpeg "se-image-b52ad9ff452db8d52e09710db4976ec2.jpg")
