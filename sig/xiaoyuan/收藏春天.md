- [最美校园，做最有价值的教育！](http://www.yuying.org.cn)
- [收藏春天！期待重返校园里热闹的夏天！](https://gitee.com/yuandj/siger/issues/I58MOZ)
- [北京市育英学校老师制作近百件校园植物标本](https://new.qq.com/rain/a/20220518A089JH00)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0522/191914_4a6b2ad0_5631341.jpeg" title="收藏春天.jpg"></p>

> 第一次去育英学校，接待我的李老师就给我推荐 “最美校园” ，当时只想着我的《棋》课，而且去的分校，高大敞亮的教学楼和很多现代校舍一般，就没有特别在意。因为一直关注李老师的朋友圈，也都一直为她对学生的热情点赞，而只有见到过她的学生们才能真正理解爱孩子和被孩子爱是对等的。《棋》课全程高能，孩子们认真聆听和我互动，共同体验了一次民族棋文化的魅力。说棋是缘，今天的主题才是紧要，“最美校园” 被李老师 POST 给我的密集的花海视频，震撼到了。这个约是必须赴啦，“一定去本校参观。”

> 本期专题引用了育英学校公众号的文章，介绍了当下在线学习的同学们的精神风貌，详细介绍了《[收藏春天](#看自然朋友在海淀校园)》作品的创作背景，特别推荐了该校专门为学生展示风采设立的专门公号，选取了与 SIGER 有交集的树莓派做推荐，虽然学生作品多少有些稚嫩，但公号引用的 孙云晓老师的话 “积极思考后的质疑更有价值” 凸显了学校的教育理念 “智慧发现，创新探索，实践挖掘”，篇尾是育英学校于会祥校长的题词《[思与索](https://images.gitee.com/uploads/images/2022/0523/224028_2a37cb78_5631341.png)》。对育英学校的更多了解，同学们可以移步其[官网](https://images.gitee.com/uploads/images/2022/0523/221249_d4f40781_5631341.png)，[公号](https://images.gitee.com/uploads/images/2022/0523/223318_def9d06a_5631341.png)。

收藏春天，则是一个小视频的标题，由育英学校的小学部的科学老师制作，主体就是用压花工艺，收藏了校园的200多种植物，并裱了镜框。“期待重返校园里热闹的夏天！” 的暖语打动了所有人，疫情的当下，老师们希望能留住校园的春色，等待孩子们盼归的心情，在阳光下是那样的晶莹剔透，就和我第一次见李老师和她的孩子们的感觉一样。“最美校园，一定有最美的孩子，最美的老师！” 这是我发出本期专题邀约后的敬意。“最美校园” 已经不是个标题，而是我的满心期许了。呈现给同学们的迫切心情，化作行动，于今日出刊了。而且感谢这份美好的心情，我为 SIGer 设立了新频道 xiaoyuan 频道，将会用来发现校园各种的美丽。 :pray:


# [收藏春天！期待重返校园里热闹的夏天！](https://gitee.com/yuandj/siger/issues/I58MOZ)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171446_bf31c809_5631341.jpeg "Screenshot_20220522_160740_com.tencent.mm.jpg")

- 01:40 月季花
- 01:38 普罗菲
- 01:35 鸡树条
- 01:32 诸葛菜
- 01:29 夏至草
- 01:26 红王子锦带花
- 01:23 苦荬菜
- 01:20 叶子花
- 01:17 蓝花鼠尾草
- 01:14 苦麻菜
- 01:11 紫苑
- 01:07 铁线莲
- 01:05 红枫
- 01:02 诸葛菜
- 00:58 银杏
- 00:56 箬竹
- 00:53 七叶菩提树叶
- 00:50 鸡爪槭
- 00:47 水杉
- 00:44 紫藤
- 00:41 山楂
- 00:38 木茼蒿
- 00:35 鸡爪槭
- 00:32 倒地铃
- 00:29 蓝花鼠尾草
- 00:26 猬实
- 00:22 桔梗
- 00:20 叶子花
- 00:17 大滨菊花朵
- 00:14 康乃馨
- 00:11 月季花
- 00:08 金银忍冬

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171535_a0145a97_5631341.jpeg "Screenshot_20220522_160742_com.tencent.mm.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171601_64ce8e50_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171617_b8c395b0_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171642_dea117a2_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171649_e642f812_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171656_d3f47b8f_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171702_efddddc2_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171710_697c7b3e_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171717_2260893d_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171728_949dcb50_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171737_8c5e3574_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171745_6e907284_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171756_8df3d8e2_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171804_1a496f17_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171814_4da59bf0_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171822_4a5baf30_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/171829_10f3c97b_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174648_8642b789_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174701_02e2959c_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174717_edc06ce8_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174727_ced573fb_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174735_86ec4799_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174748_eb854f05_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174758_400a91ed_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174809_1e958bbf_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174819_2a8cd25b_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174831_61e7c5aa_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174840_e784f339_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174852_abf44fd1_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/174902_ac17ae69_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/175106_7464d0d0_5631341.png "屏幕截图.png")

# [为学生“收藏春天”，北京市育英学校老师制作近百件校园植物标本](https://www.163.com/dy/article/H7LRERL40512D3VJ.html)
2022-05-18 17:13:06　来源: 新京报 北京  

分享至

新京报讯（记者杨菲菲）“夏天来了，校园的春天已经收藏起来”，配图是一张张精心制作干花作品。近日，北京市育英学校小学科学教研组组长徐娟发的一条微信朋友圈引来了众多点赞。5月18日，徐娟告诉记者，受疫情影响，学生目前都居家学习，为了让孩子们返校后能第一时间欣赏到校园里不同植物最美的样子，该校三名小学科学老师制作了近百件植物标本。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/180642_83ff74eb_5631341.png "屏幕截图.png")

大滨菊花朵标本。受访者供图

“我们学校的动植物资源都很丰富，统计在册的植物资源就有210多种，昆虫也有200多种。”徐娟说，学校一直坚持环境育人的教育理念，科学课程也在这一理念的指引下，进行教学设计，同时还设计了各种项目和活动，比如小初高联合参与的生物多样性课程，根据不同年级设计的采摘山楂、柿子等活动，我为学校动植物代言，等等。

四月中旬，学校的科学课程正好讲述《植物的生长变化》这一单元，徐娟告诉记者，此次制作植物标本也源于课程单元学习的需要。“今年4月中下旬只是几个学生在做标本，然后认识植物的茎、叶、花等结构。”徐娟表示，没过多久，学生全部居家学习，科学组的老师看到这种情况，就搜集了很多植物，想做成干花给孩子们，用这样的方式把春天“收藏”起来，等他们返校的时候就可以看到学校漂亮的花花草草了。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/180653_b1ecd79d_5631341.png "屏幕截图.png")

康乃馨标本。受访者供图

搜集不同的植物、购买押花器、制作干花再放到相框里……三位科学老师忙碌了起来。“不同植物的花期不同，我们就经常在校园里转转，有合适的就做成干花。”徐娟告诉记者，有的标本有花有叶，有的标本带着果实，有的标本则是叶子刚刚露出，截至目前，已经制作了近百个植物标本。

在徐娟看来，日常学习中，学生们其实已经储备了不少相关知识，但可能整体的科学体系尚未搭建起来，甚至对有些知识的理解有偏差，“所以学习科学课一定要多动手操作，在实际操作中学生很容易纠正自己的错误，在有趣的探索中获得新的知识。”

居家学习期间，孩子们要如何进行科学教育？徐娟表示，科学知识不仅在书本中，也在生活环境中，“无论是在学校还是在家，大家都可以多观察身边的各种植物。作为老师和家长，我们也要多鼓励学生提问，别管问题有多发散、多天马行空，只要引导他们提出问题再想办法解决问题，那他们就会在这个过程中获取新的知识，而且很有成就感。”

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/180705_d97b676c_5631341.png "屏幕截图.png")

猬实标本。受访者供图

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/180716_bed942aa_5631341.png "屏幕截图.png")

桔梗标本。受访者供图

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/180725_e8e1f17c_5631341.png "屏幕截图.png")

叶子花标本。受访者供图

编辑 缪晨霞

校对 卢茜

- QQ: [为学生“收藏春天”，北京市育英学校老师制作近百件校园植物标本](https://new.qq.com/rain/a/20220518A089JH00)

  > 新京报讯（记者杨菲菲）“夏天来了，校园的春天已经收藏起来”，配图是一张张精心制作干花作品。近日，北京市育英学校小学科学教研组组长徐娟发的一条微信朋友圈引来了众多点赞。5月18日，徐娟告诉记者，受疫情影响，学生目前都居家学习，为了让孩子们返校后能第一时间欣赏到校园里不同植物最美的样子，该校三名小学科学老师制作了近百件植物标本。

- BAIDU: [为学生“收藏春天”，北京市育英学校老师制作近百件校园植物标本](https://baijiahao.baidu.com/s?id=1733152706042793087)

  > 新京报讯（记者杨菲菲）“夏天来了，校园的春天已经收藏起来”，配图是一张张精心制作干花作品。近日，北京市育英学校小学科学教研组组长徐娟发的一条微信朋友圈引来了众多点赞。5月18日，徐娟告诉记者，受疫情影响，学生目前都居家学习，为了让孩子们返校后能第一时间欣赏到校园里不同植物最美的样子，该校三名小学科学老师制作了近百件植物标本。

### [把春天留下来，等待你们来看！](https://m.chinanews.com/wap/detail/chs/sp/9758644.shtml)

[教师制作近百件校园植物标本：为居家学生“收藏”校园的春天](https://m.chinanews.com/wap/detail/chs/sp/9758644.shtml)
中国新闻网

2022-05-19 17:03:08 34.1万

[![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/161551_5eb16d40_5631341.png "屏幕截图.png")](http://poss-videocloud.cns.com.cn/oss/2022/05/19/chinanews/MEIZI_YUNSHI/onair/AFCEDEAE9B8447DA9B0D0266B85A5A1A.mp4)

> 我们学校有非常丰富的植物和动物资源，就是从五一之后孩子们就居家了。就是想着怎么让孩子们还能继续看到校园的美，那后来我们就想了一种压花的技术。然后就做成了植物的标本。我们大概做了有近一百个了吧。这样既了解了这个植物的这些器官，又能保留住这个植物最原始的状态，最美好的状态。然后给孩子们把这个春天留下来。等待孩子们重回校园那一天。还依然能够看到，曾经校园的美以及现在校园的美。

　　淡雅的康乃馨、湛蓝的桔梗……近日，北京市育英学校小学科学教研组组长徐娟的一幅幅干花作品在朋友圈收获了众多点赞。

　　出于疫情防控考虑，目前北京市中小学暂停返校，学生居家学习，无法一睹校园的绝美春色。为此，学校三名教师制作了近百件植物标本，为孩子们“收藏”校园的春天。

　　记者 程宇

版权声明：中新视频版权属中新社所有，未经书面许可的使用行为，本社将依法追究其法律责任。

责任编辑:李霈韵

# 最美校园，做最有价值的教育！

### [传说中跻身海淀“新七小强”的这所中学，最近又有大动作](https://mp.weixin.qq.com/s?__biz=MzA3OTIxOTYwNw==&mid=2651483240&idx=2&sn=57bf4984758123ae9bdc11c449de25d9)
京城教育圈 2022-05-19 23:07 发表于北京

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/164401_9528b738_5631341.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/164418_6abe0b02_5631341.png "屏幕截图.png")


海淀区中学名校素有“六小强”一说，随着今年海淀一模考试结束，圈内不少人就开始流传“新七小强”的说法。


据了解，海淀区此次一模700分以上的28个学生，分别来自这七所中学，包括人大附中、清华附中、十一学校、101中学、首师大附中、育英学校、57中。其中，作为新锐校，育英学校和57中在高三一模中杀进“700分”大关，备受关注，这是最新圈内“新七小强”的由来。


业内人士分析，相比往年，北大附中今年一模表现不如人意，但综合实力仍很强劲。因此，即使有“新七小强”一说，北大附中肯定占有其中一席，同时需关注的是，育英学校和57中学进步都不小，尤其是育英学校，应是更有可能成为“新七小强”的新成员。


今天要和大伙聊聊育英学校最近推出的一个大动作：正式启动科学特色高中的建设。


![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/164430_4b011e84_5631341.png "屏幕截图.png")


这很可能是北京市现有高中校率先官宣建设“科学特色”高中、且提前在师资、课程、科研设置进行系统规划准备的学校。


育英学校的大动作，和目前普通高中最受关注的“多样化有特色发展”直接对标。2019年，印发《关于新时代推进普通高中育人方式改革的指导意见》。意见指出，到2022年，普通高中多样化有特色发展的格局基本形成。这就意味着高中教育既要强调共同扎实基础，又要满足学生多样性、个性化发展。而年初北京市印发的《2022年市工作报告重点任务清单》明确指出，推进普通高中多样化发展。


![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/164437_841e921f_5631341.png "屏幕截图.png")


作为一所具有悠久红色历史的普通学校，北京市育英学校培养目标是“培养行为规范、热爱学习、阳光大气、关心社稷、勇于担当的国家栋梁”。目前，育英学校已发展成为一所涵盖小学、初中、高中学段的集团化学校。近十年来，学校不断进行校园物质环境改造，不断开发、建设优质课程资源，图书馆、阅读馆、艺体馆、科学研究实验室等为学生的发展提供了硬件保障；在生态校园、书香校园、艺术校园建设的过程中，学校的科学课程特色基本形成。


“学校始终以国家发展需要作为学校人才培养的方向。”育英学校相关负责人表示，为落实教育部、市区教委提出的高中多样化特色办学的要求，学校结合培养目标和办学实际，将高中聚焦为“科学特色”发展，以进一步探索创新人才培养的新路径，践行育人为国育才的初心和使命。


![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/164446_994eb97f_5631341.png "屏幕截图.png")


据介绍， 为保证“科学特色”高中的建设，近年来，育英学校在物理、化学、生物、地理等学科的师资方面进行系统规划和准备。教师团队由资深教学名师和科学创新型教师组成。其中博士研究生18人，占比60%，专业研究涉及机械设计及理论、凝聚态物理、粒子物理和原子核物理、量子纠缠、原子核分子物理、纳米化学、材料物理与化学、天然产物与高分子化学、生物学、微生物学、生态学、矿物学-岩石学-矿床学、固体地球物理、地图学和地理信息系统等，涵盖了大部分科学研究领域。团队资深教师12人，占比40%，主要由特级教师、市区学科带头人和学科骨干组成，重点研究高中教学和为高考把关。


&#x2028;围绕科学特色高中发展，育英学校建设了一流的科学实验室，涉及物理、化学、生物、地理、跨学科等共计15个高端实验室；实验室除了可以实现全体学生志趣发展的科学实验外，还有包含3D打印机、原子吸收质谱仪、气质联用色谱仪、荧光倒置显微镜、紫外分光光度计、地理虚拟环境、电子沙盘、高铁运行、人工智能等高端实验设备，可以实现面对部分学生科学创新实践的高端实验探索。科学实验室的建设为高中科学特色发展提供了强大的硬件支持。


![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/164453_1e692458_5631341.png "屏幕截图.png")


近三年，学校科学研究团队带领学生完成并获奖的项目共42项，涉及学生130人次，包括北京市市长奖、科技创新全国一等奖、市级一等奖等，部分学生也通过在高中的科学研究实践确定了科学研究的志向，并进入北大、清华、中科大等顶尖高校继续自己的科学研究之路。


该校有关负责人介绍，作为小初高一体的学校，“科学特色”不仅在高中稳步推进，更为重要的是营造了全校师生进行科学研究的氛围。学校在各项科技活动中取得了优异成绩。比如，2021年在北京市中小学生科学建议活动中，我校学生获奖总数为283项，获奖总数占海淀区总数的40%，占北京市总数的16%；2022年晋级北京市青少年科技创新大赛中，学校共14项作品晋级北京市青少年科技创新大赛成果竞赛项目，成绩突出。


![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/164501_7dc6b8b4_5631341.png "屏幕截图.png")


 “作为一所有历史担当的学校，育英学校将继续探索并优化适合学生发展的教育教学途径，聚焦学生科学素养提升，为基础教育阶段创新人才的培养提供育校方案。”

- “京城教育圈”

  > 政策解析，升学指南，发现价值，分享新知

  > 关注更多教育信息，欢迎大家扫码加入圈友群：




编辑｜京教君

部分内容素材来源｜北京市育英学校；“京城教育圈”进行内容补充和编辑整理


### [看！自然朋友在海淀校园](https://www.bjhdedu.cn/zt/2022gjdyxrxy/2022gjdyxrxydt/202205/t20220520_51244.html)

时间:2022-05-18 08:15 育英学校 517次浏览
【字体：大 中 小】 打印本页

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/212608_166c8143_5631341.jpeg "W020220520373245617411_ORIGIN.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/212628_e4b210c3_5631341.jpeg "W020220520373245629917_ORIGIN.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/212648_2d40dd12_5631341.jpeg "W020220520373245644196_ORIGIN.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/212712_4e243e04_5631341.jpeg "W020220520373245658767_ORIGIN.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/212735_eb4a1623_5631341.jpeg "W020220520373245661469_ORIGIN.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/212754_8e7f6b91_5631341.jpeg "W020220520373245674722_ORIGIN.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/212813_83d7b573_5631341.jpeg "W020220520373245681962_ORIGIN.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/212842_13a1b98a_5631341.jpeg "W020220520373245691131_ORIGIN.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0522/212911_e8932649_5631341.jpeg "W020220520373245708747_ORIGIN.jpg")

转载请注明来源

### [隔空不隔研，交流增信心](https://mp.weixin.qq.com/s?__biz=MjM5NTY2OTMzNQ==&mid=2666154386&idx=1&sn=10714b81448171df22db02854830a070)

北京市育英学校 2022-05-22 19:27 发表于北京

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221249_d4f40781_5631341.png "屏幕截图.png")

-  **微信号** : 北京市育英学校 mini-yyxx
-  **功能介绍** : 最美校园 做最有价值的教育 
-  **官网** : http://www.yuying.org.cn 
-  **微博** : http://weibo.com/yuyingschool


 **隔空不隔研，交流增信心** 


5月20日这一天，小学的教研组长、年级主任和学部干部在腾讯会议的虚拟会议室里召开每月教研交流例会，不同的是，这个月的例会内容是“线上教学的实践与思索”。

上午的会议聚焦音体美等学科，五位教研组长分别介绍了各学科老师们如何运用学习单、小视频、微课等方式指导学生进行居家学习。


#### **音 乐** 


原本定于五月的丰富多彩的音乐活动因疫情而暂停了，为了弥补这个遗憾，音乐组的老师们根据“低年级重在发展兴趣，中高年级重在培养自主学习能力”的原则，精心设计有趣的学习单，学生们饶有兴趣地参与到音乐学习实践当中，用音乐旋律抚慰了居家的焦虑。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221538_0a61280e_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221605_80d28d6d_5631341.png "屏幕截图.png")

#### **美 术**

同为艺术学科的美术组老师秉持“让美术为居家减压”的美好愿望，一边带领低年级小同学关爱自己，描绘自己，一边关注教育热点，将美术课和劳动教育结合在了一起，指导中高年级同学们一起制作“我的美食图鉴”。有趣又有意义的美术实践令同学们快乐不已。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221626_83d1a570_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221634_740d2ff6_5631341.png "屏幕截图.png")

#### **科 学** 

科学老师们巧手巧思，除了在学习单里为学生设计了适合居家的学科活动，还制作了近百件校园春季植物的标本，这次的“留住春天”还得到了新京报的报道呢！

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221654_0e58a99d_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221700_b8240995_5631341.png "屏幕截图.png")

#### **道德与法治** 

道德与法治是实践性很强的学科，道法组的老师们结合疫情期间学生居家学习的实际，根据学部和年级的要求，设计独具特色的道法学科居家学习任务单，学生们纷纷参与，让居家的学习内容也变得有意思。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221718_b5f998eb_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221726_98866064_5631341.png "屏幕截图.png")

#### **体 育** 

疫情居家，学生的身体素质怎么保障？活泼好动的孩子除了玩耍还能怎么锻炼身体？体育这一重磅学科是老师和家长共同关注的焦点。这一点上，体育组先展开了“大练兵”，体育老师们各显其能，施展自己的十八般武艺，用短小又充满趣味的视频带领学生居家锻炼，体育学科的线下挑战赛如火如荼。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221742_cead7163_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221751_d575970a_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221802_68727b35_5631341.png "屏幕截图.png")

下午，结束了线上授课的语数英学科参与到了教研组长交流之中。低年级学生没有线上语数英网课，但老师们想尽办法，运用学习单、资料袋等方式带动同学们开展起居家学习。中高年级直播课安排得有条不紊，为学生尽量模拟出在校的生活画面。

#### **语  文** 

低年级的语文老师特别重视学习单的设计，力求通过直观、明白的学习单指导低年级小学生在阅读和实践中完成各项学习内容，多种形式展示促进学生的参与。二年级紧紧围绕四季课程开展阅读，一年级多学科融合的古诗学习还得到“海淀融媒”的报道。

中高年级的直播课实现了面对面交流，老师们为学生营造满满仪式感，根据每节课的实际情况，随时随地开展线上教研，利用多方资源，反复磨课。老师们还利用课上、课下、小组中的讨论机会，帮助学生克服困难，建立学习的信心，努力把线上课堂打造成新的“身边课堂”。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221900_a883fc09_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221907_8b9ab437_5631341.png "屏幕截图.png")

#### **数 学** 

低年级的数学学科同样采用了学习单和数学阅读的形式，老师们虽然没有上直播课，但却和学生保持着非常密切的“飞信沟通”，低年级小娃娃们把自己学习中的困惑或者有趣的发现都录成视频发给老师，老师再进行答疑、展示，低年级小课堂热闹非凡。

中高年级的数学学习内容的深度逐渐增加，数学组同样采取随时随地教研的方式，不仅合力解决学生共性的问题，还专门钻研了适用于学生居家学习的分层作业，有针对性地辅导，创设更多的实践性作业，想尽办法把数学这一门抽象的学科具体化、生活化，让学生学中有乐趣，趣中有收获！

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221926_7dbafd69_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221932_77332e4e_5631341.png "屏幕截图.png")

#### **英 语** 

英语学科秉承“研究型学科组”的传统，老师们在忙碌的教学和批阅作业之中，不断学习充电，为学生打造“花式学英语”的学习氛围，一边根据学生的年龄特点制定详细、指导性强的学习单，一边开展丰富多样的英语实践活动，调动着学生学习和参与的热情。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/221958_6422160a_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/222008_cb689978_5631341.png "屏幕截图.png")

在两次分学科的交流中，分享了线上教学所有的实践经验后，各教研组也不约而同地提出了一些困惑，比如没有直播课的学科如何更充分地关注学生的学习过程和学习效果？如何更广泛地展示学习成果？直播课的学科如何保证上课听讲和作业的完成质量？老师们如何高质量的作业设计和辅导答疑，实现因材施教可持续性发展？各学科组讨论得热烈非常，老师们隔空不隔研，工作热情不减，怀抱着期待开学见到学生的美好愿望，也为接下来可能还要持续一段时间的居家线上教学奠定了信心，相信在群策群力下，育英小桃桃们也可以居家不松劲，在未来这段时间有所收获！


END



- 文字：朱盈梅
- 视频：小学体育组、小学数学组提供
- 编辑：朱书美
- 统筹：王国新

### 【智能搭建】[树莓派爱心伞智能系统探究](https://mp.weixin.qq.com/s?__biz=MzU2NTU0ODAxMw==&mid=2247598983&idx=1&sn=cf9360f807f9b2b7aee53c07c7e28830)

原创 SPITT 思与索实践展示空间 2022-05-11 17:36 发表于北京

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/223318_def9d06a_5631341.png "屏幕截图.png")

-  **微信号** : 思与索实践展示空间 SPITT-3
-  **功能介绍** : 智慧发现，创新探索，实践挖掘

> 积极思考后的质疑更有价值  
> ——孙云晓

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/223517_adec8569_5631341.png "屏幕截图.png")

> 思维训练营  慧编程 1级  张清瑜


#### 1：需求背景

 _这个寒假老师在征集爱心伞管理方案，于是，我就想到：既然现在是数字化的时代，那么学校的借伞系统也要更加智能，便捷，那么我就给学校写一个爱心伞管理的程序吧！_ 

可是应为这个爱心伞管理程序可能会放到各种的场所，如：东南门，北门，教学楼等，所以这个系统不是单一的程序，而是需要服务与界面分离的多客户端程序。综合考虑成本、方便连接刷卡机、摄像头等多种外设因素，选择用树莓派来制作智能终端最为合适。 _可是，怎么制作界面呢？怎么把程序导入树莓派？_ 

#### 2：制作界面

首先梳理程序功能，手绘界面逻辑图

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/223617_6d071176_5631341.png "屏幕截图.png")

（图1:手绘界面逻辑图）

由于直接使用python TK库来编辑界面比较麻烦，需要撰写代码，并且无法直观看见成果，导致需要一点一点的调整具体位置，所以我就像选择使用所见即所得的TKinter Designer开源开发工具网址为https://github.com/honghaier-game/TKinterDesigner.git，而其它软件则都需要用代码来编辑。并且TKinter Designer编辑器软件绘制界面，绘制后会自动生成TKinter代码，开发功能时只需要对自动生成的代码进行适当修改，包括：添加连接功能，调节按钮效果，改变界面内部文字、输入框、图片、调整按钮位置等。

Tkinter Designer TkinterDesigner是一个基于python的开发工具，用于开发基于Tkinter界面的python应用程序项目。但它与其它类型的开发工具的最大不同之处在于，它提供了一套完整的开发流程。Tkinter Designer是一套Python进行应用软件开发的解决方案，包括了从项目创建到界面设计，再到事件逻辑编辑与调试运行到打包发布的整体开发流程。具体界面如下：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/223647_c859191f_5631341.png "屏幕截图.png")

（图2：TKinterDesigner软件页面） 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/223658_d1ca4db6_5631341.png "屏幕截图.png")

（图3：TKinter Designer软件页面）

不过从图3可以清晰地看出，TKinter Designer有些Bug,直接绘制界面面存在一些显示问题，于是就需要调整系统代码。


```
import tkinter.font
#Add your Varial Here: (keep this ling of comments)
#Define UI Class
class SanUI:
    def __init__(self,root,isTKroot = True):
        uiName = self.__class__.__name__
        Fun.Register(uiName,'UIClass',self)
        self.root = root
        Fun.Register(uiName,'root',root)
        style = SanUI_sty.SetupStyle()
        if isTKroot == True:
```

（[图4](https://images.gitee.com/uploads/images/2022/0523/223718_1f759b3b_5631341.png)：TKinter界面代码）

程序界面最终效果如下：

（图5：程序界面）

#### 3:将程序导入树莓派

先确定在树莓派上是否能成功访问电脑上的借伞后台服务

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/223838_c5af6370_5631341.png "屏幕截图.png")

(图6：连接后台服务接口)

访问成功后，将客户端的程序界面代码拷贝到树莓派上,不过由于界面代码中使用的是本地地址，连接失败，需要将地址改为后台电脑的地址，修改后成功运行。
运行效果如下：

#### 改进、收获与展望：

本程序已经具有了基本功能，后期还可以给给树莓派添加扫码，连接外设等功能，实现智能全自动借伞终端。而我在绘制界面和将程序导入树莓派的时候了解与学习了界面绘制的方法与流程，如何调整Tkinter Designer自动生成的代码，本地地址与远端地址的区别及应用环境，多种外设及功能和树莓派的功能与作用。

 **教师说** ： 

> 积极探索是一个小小工程师的优秀品质，张清瑜同学积极响应学校的活动——征集爱心伞管理方案。可以看到他 _先进行了思维导图的设计，梳理出方案的框架，然后分析问题，寻找合适的功能实现环境，再通过设计、修改、完善最终形成了自己的解决方案。这是一个很标准的项目实践流程，值得同学们学习与借鉴。_ 


![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/224006_0db704e4_5631341.png "屏幕截图.png")

世纪之林


- 作者/五年级 张清瑜
- 指导/薛晖老师
- 编辑/薛晖老师
- 统筹/梁秋颖老师
- 联系我们/lqy19700114（微信）


![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/224028_2a37cb78_5631341.png "屏幕截图.png")

 **欢迎关注北京市育英学校“思与索实践展示空间”** 
