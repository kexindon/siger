![输入图片说明](https://foruda.gitee.com/images/1672471189856283152/66e54486_5631341.jpeg "第一期SMART时报 P2副本2m.jpg")

> A2 pages 2P，A3 双面；页边距 3cm x 1.8cm；正文字号 7.2pt，标题间距 19.6pt，段落间距 17.1pt，行间距 12.32pt；

# SMART-TIMES-01

## CAR-T疗法全球上市
回望2021年，这一年可谓国内CAR-T细胞疗法的元年，复星凯特的阿基仑赛注射液和药明巨诺的瑞基奥仑赛注射液相继获批上市，为晚期血液肿瘤患者提供了全新的治疗手段。

放眼全球，FDA于2021年亦批准了两款CAR-T细胞疗法的上市申请，包括首款靶向BCMA的CAR-T疗法Abecma。对于CAR-T疗法，我们一方面惊叹于细胞疗法取得的卓越技术进步；另一方面，面对如此高昂的售价，难免不对其未来的商业化预期存在担忧。归根结底，技术的进步是否能转化为商业化的成功。

据药智数据统计，截至2021年12月29日，全球已批准6款CAR-T细胞疗法上市，其中FDA批准了5款，有2款获NMPA批准；包括四款靶向CD19和一款靶向BCMA的CAR-T细胞疗法。2021年，FDA先后批准了Breyanzi和Abecma的上市申请，前者是一款CD19 CAR-T，后者为一款BCMA CAR-T。

| [靶点](https://images.gitee.com/uploads/images/2022/0707/215504_34cd1f63_5631341.png) | 药物 | 企业 | 适应症 | 人数 | ORR | CR | CRS | NT | 上市时间 |
|---|---|---|---|---|---|---|---|---|---|
| CD19 | Kymriah | 诺华 | 急性淋巴细胞<br>白血病(ALL) | 75 | 81% | 60% | 47% | 21% | 2017.8 |
| | | | 大B细胞淋巴瘤<br>(DLBCL) | 93 | 52% | 40% | 22% | 18% | 2018.5 |
| | Yescarta<br>阿基仑赛<br>注射液 | 吉利德<br>/Kite/<br>复星凯特 | 大B细胞淋巴瘤 | 101 | 72% | 51% | 13% | 31% | 2017.1(FDA)<br>2021.6(NMPA) |
| | | | 滤泡性淋巴瘤 (FL) | 146 | 91% | 60% | 8% | 21% | 2021.3 |
| | Tecartus | 吉利德<br>/Kite | 套细胞淋巴瘤<br>(MCL) | 74 | 87% | 62% | 18% | 37% | 2020.7 |
| | Breyanzi | BMS/Juno | 大B细胞淋巴瘤<br>(DLBCL) | 192 | 73% | 54% | 2% | 12% | 2021.2 |
| | 瑞基奥仑<br>赛注射液 | 药明巨诺 | 大B细胞淋巴瘤<br>(DLBCL) | 58 | 75.90% | 51.70% | 5.10% | 5.10% | 2021.9(NMPA) |
| BCMA | Abecma | BMS/Blue<br>bird | 多发性骨髓瘤<br>(MM) | 127 | 72% | 28% | 9% | 4% | 2021.3 |

## 不忘初心，共执胜念
就在生命进入“倒计时”时，他有机会接受CAR-T细胞疗法。癌细胞消失了，他“捡”回了一条命......

<img src="https://images.gitee.com/uploads/images/2022/0625/174235_6e35ff8f_5631341.jpeg">

### 噩梦：二胎女儿两个多月，24岁的他确诊癌症

2013年10月份的一天，24岁的赵帅杰从修车厂下班回宿舍。他一边走一边扇着衣服，指甲缝里还残留着工作痕迹。

宿舍在二楼，同事毫不费力地就走上去了，可他得吃力地“爬楼梯”。回到宿舍后更是气喘吁吁，好久都喘不上来气儿，浑身没劲。坐在凳子上歇息时，他想起同事问他“脸怎么那么黄”，再加上自己的状态实在太差，便独自一人去医院做检查。

“你以前得过白血病吗?怀疑是白血病，给你转到大医院再检查一下吧?”

听到这话，赵帅杰愣了，脑子一片空白，只能尽量平复心情。当然，心理也抱有一丝侥幸地安慰自己：医生说了，只是怀疑。从医院出来后，他先给老板打了电话，结了7000元左右的工资。

之后又拨通了父亲的电话，父亲沉默一会说：“先回来吧......”赵帅杰当时还算平静，直到听见老婆的哭声，他绷不住了。

“儿子一岁多，女儿刚办完满月酒，想着来温州打工好好挣钱，谁知刚来一个多月就碰上这事儿。”他告诉顶端新闻·河南商报记者，当晚就坐车回到老家商丘，没回家，先去医院做了检查。等待检查结果的日子是难熬的，那两天，赵帅杰无心做事，只是祈祷着能等来一个好消息，可最终等来的，是确诊。
“怎么会呢?家里祖祖辈辈没有人得过癌症，我平日里身体好得很，感冒都很少有，而且我还是最壮的......”赵帅杰的脑子转啊转，怎么想都想不通，为啥自己会得这个病?加之经济条件并不好，他说：“当时想死的心都有!”
<img src="https://images.gitee.com/uploads/images/2022/0625/174527_30d3afbf_5631341.jpeg">
### 不幸：两次移植都没能杀死癌细胞
得病了，那就借钱治。

赵帅杰回忆说，亲戚朋友都借了，一家2万、3万的，没钱的时候就先出院，等有钱了再住院。

2014年6月份，他与刚大学毕业的弟弟配型成功，进行了第一次造血干细胞移植。可不幸的是，两年后，癌症复发了，又做了第二次造血干细胞移植，还是以失败告终。

“一开始是害怕，后来就麻木了，想着算了吧，过一天是一天吧，只是苦了自己的老婆。”赵帅杰说，整个住院期间，老婆一边照顾自己，一边带着两个孩子。印象最深的就是，老婆怀里抱一个，手里牵一个，抱孩子的那只手还得提着饭盒，打饭、送饭。“护士说，老婆每天都躲在门后面哭。”第二次复发后，赵帅杰深知自己的生命要进入倒计时了，还曾苦笑着说：“你看，这化疗对我都没用，人家化疗又吐又拉，瘦得不行，我都没瘦几斤，还能吃饭。就连两次移植都没能杀死癌细胞。”

“说实话，那时候就在等死了。”赵帅杰万念俱灰，殊不知，一次最终的机会正等待着他。

### 转机：最后的机会
据了解，早在2012年，河南省肿瘤医院整合血液科、免疫治疗科、中心实验室、检验科等部室的资源，建立了包括CAR-T细胞制备、生物活性检测和临床安全性评估平台，开展CAR-T细胞治疗的基础和临床应用研究。2016年，河南省肿瘤医院血液科等相关科室正在开展CAR-T细胞治疗血液肿瘤的创新技术的探索和应用。

得知赵帅杰的情况后，经与家属沟通，2016年8月5日，在河南省肿瘤医院副院长宋永平和高全立的组织下，血液科与生物免疫治疗科密切合作，为他实行了CAR-T细胞治疗。

“ 当时我啥也不知道，只记得那次治疗后，高烧40多℃，连着烧了100多个小时，家属说，我的眼睛里当时全是血块，身上置了很多管子，每天靠输营养液维持着。”赵帅杰告诉顶端新闻·河南商报记者，后来才知道那是临床研究，不收费，如果现在做的话，得花120万元呢。“谁也不想当‘小白鼠’，但当时没有别的办法了，总要试一试，很理解当时家属的心情。”
好在，结果是好的。

据高全立介绍，治疗半个月后复查，他体内已检测不到白血病细胞了。如今是第6年了，“医学上有一句话，如果连续5年都检测不到肿瘤细胞，那就可以说是完全治愈了。”

<img src="https://images.gitee.com/uploads/images/2022/0707/215304_6669763e_5631341.png">

### 新生：以后一定要让老婆孩子过得好一点

6月22日，赵帅杰再次来到医院复查，不用医护人员指导，他熟练又标准地蜷缩身体，等待着做骨髓穿刺。

“家人提前一个月都催着我来复查，来一次就要花费5000左右，内心实在不想来，但也想让家人安心。”此外，他也告诉顶端新闻·河南商报记者，不想来的另一个原因是，对医院有心理阴影了。“住院30多次，骨髓穿刺60多次，造血干细胞移植2次，进仓3次......这段经历无人能感同身受，现在来到医院就会觉得烦闷、恶心，闻见消毒水的味道就想吐。”
也会想起来当时隔壁床的那位患者。

“他比我大一岁，我最后一次进仓时，他进了ICU，不久后人就走了......”

赵帅杰说，自己比很多人都幸运，未来也一定会好好挣钱，弥补家人。

“24岁，正是大好时光。可我生病了，错过了陪伴孩子的黄金时期，也花费了一大笔钱，欠了外债。跟我同龄的，现在都已经有车了，还在县城买了房......”他感慨道，之前也会羡慕，但现在想开了，觉得只要有个好身体，家人平安健康就够了。如今，赵帅杰工作两年了，做铝合金，一个月能挣个5000多块钱;他老婆干小时工，能稍微挣点钱，也不耽误接送孩子。
他说，先给自己定个小目标，把外债还完后，在县城买个房子。

“就像老婆经常说的那样，以后一定都会慢慢好起来的。”他坚信，一切曾在生命中出现的阴霾终将会被行走的岁月赋予意义，然后重获新生。

## 路在何方
添CAR-T免疫细胞疗法上市后，以免疫细胞疗法及干细胞疗法为代表的细胞治疗时代已经悄然来到我们身边，不仅仅是政策的逐步支持与开放，“嗅觉灵敏”的商业保险也立即纳入，以增加对疾病的保障，这也展现出大型保险机构对于细胞科技深入调研后的信心。
一方面，从技术的角度，CAR-T疗法代表着技术进步的最前沿阵地，吸引了一批制药巨头布局。而前沿的技术尽管意味着较高的壁垒，但同样代表着高企的研发费用。跨国巨头吉利德科学对CAR-T疗法寄予厚望，2017年不惜以119亿美元的天价收购Kite Pharma，并获得旗下Yescarta的权益。

然而，另一方面，商业现实却给新药企业当头一棒，Yescarta在2020年实现了5.63亿美元的全球销售额，尽管增速明显，但若将119亿美元的收购成本纳入考虑，这一销售数据便可谓“惨不忍睹”，甚至距离成本回收亦遥遥无期。

而聚焦国内市场，阿基仑赛注射液和瑞基奥仑赛注射液均定出超过120万元的天价，如何快速衔接研发与商业化是药物上市后企业需要思考第一要素。且阿基仑赛注射液失利于2021年度医保谈判，不禁令人惋惜于企业的商业化经验，错失了与后来者拉开距离的绝佳条件。 因此，不得不说，具有中国本土特色的商业化能力是创新药企业下半场必须面对的现实问题。

当然，竞争日趋激烈的研发管线是悬在每一个细胞疗法公司头上的“达摩克利斯之剑”。据统计， 2020年国内开展CAR-T细胞疗法的临床试验数量已达到335个，而以CD19为靶点的CAR-T临床试验占比超过40%，研发的扎堆势必造成商业化阶段的激烈竞争。 CD19 CAR-T是否会成为下一个PD-1？目前还没有标准答案，但答案或许在每一个人心中。

## CAR-T疗法
CAR-T疗法就是嵌合抗原受体T细胞免疫疗法，英文全称Chimeric Antigen Receptor T-Cell Immunotherapy。                         

CAR是一种蛋白质受体，可使T细胞识别肿瘤细胞表面的特定蛋白质（抗原），表达CAR的T细胞可识别并结合肿瘤抗原，进而攻击肿瘤细胞。这种表达CAR的T细胞被称为CAR-T。CAR-T细胞直接与肿瘤细胞表面的特异性抗原相结合而被激活，通过释放穿孔素、颗粒酶素B等直接杀伤肿瘤细胞，同时还通过释放细胞因子募集人体内源性免疫细胞杀伤肿瘤细胞，从而达到治疗肿瘤的目的，而且还可形成免疫记忆T细胞，从而获得特异性的抗肿瘤长效机制。

经过设计的CAR-T细胞可在实验室培养生长达到数十亿之多，再将扩增后的CAR-T细胞注入到患者体内，注入之后的T细胞便会在患者体内增殖，进而杀死具有相应特异性抗原的肿瘤细胞。

## CAR-T技术

自2017年以来，CAR-T技术已被美国FDA批准用于治疗某些淋巴瘤和白血病。令人兴奋的是，在许多情况下， CAR-T细胞在这些癌症处于晚期时也有良好的治疗效果，甚至能完全治愈癌症患者。（源自FDA官网）

<img align="left" src="https://images.gitee.com/uploads/images/2022/0707/222414_48ecbb61_5631341.jpeg" height="300" width="200">

值得一提的是，今年6月，全球第6款、中国首款细胞治疗产品，抗人CD19自体CAR-T（FKC876）阿基仑赛注射液获国家药监局上市批准，用于治疗成人复发难治性大B细胞淋巴瘤。

但CAR-T在实体瘤的治疗方面举步维艰，缺乏在正常组织中不表达而在实体瘤中高表达的抗原、免疫抑制性的肿瘤微环境，T细胞内源性的抑制信号等方面挑战，导致其在实体瘤上不能像血液瘤领域一样大显神威。这也促使它与其他免疫疗法联合治疗实体瘤成为新的研究趋势（如CAR-T与CPB的联用）。

## 总结
免疫细胞疗法是个性化的疗法，每个患者使用的都是自己的免疫细胞，而对于这种活细胞疗法，免疫细胞的健康程度及活性质量与最终的治疗效果紧密关联，这就意味着，只有在健康的时候，通过临床级高标准的细胞库，保存好自己健康、有活力的免疫细胞，面临健康风险的时候，才能为自己准备万全的机会。商业保险是习惯于对经济未雨绸缪的具有前瞻目光的人对自己与家庭做出的财务风险保障，而细胞存储则是另一种健康保险，只有守住自己的健康，个人与家庭的财产才有意义。


## 病毒进化的秘密
理论论证和经验证据一致表明，所有细胞生命形式都可以作为多种病毒的宿主。病毒表现出巨大的多样性，事实上，单个病毒家族的序列多样性可能超过整个细胞生命领域的多样性，因此，对病毒基因的深层进化关系和祖先的识别极其具有挑战性。
2022年7月13日，美国国家生物技术信息中心在Cell Host＆Microbe杂志发表了名为“The logic of virus evolution”的文章，该研究用一个简单的功能逻辑解释了病毒的宏观进化，定义了病毒的进化过程。

本篇论文揭示了以下几点：
产生病毒基因组惊人多样性的基因获得是由细胞蛋白质适应对病毒繁殖至关重要或有益的功能的简单逻辑控制的。

这种适应包括直接获得一种细胞功能或外延，即获得的蛋白质的功能发生变化，通常伴有原有活性的消除。

这种重新利用细胞蛋白质的逻辑决定了病毒宏观进化的轨迹，包括在与军备竞赛的相互作用中出现新的病毒群。

病毒被认为是地球上数量最多、种类最多的生物实体，研究人员对它们的了解有限。元转录组学的出现——即对来自特定环境的样本中的总RNA进行测序——正在帮助研究人员发现地球RNA病毒体的重要特征。在这项研究中，研究人员发现了多种新型病毒，为RNA病毒的多样性、宿主范围和进化提供了见解。
        
研究人员从现有知识库中挖掘了5150多个不同的变转录组，发现了250万个RNA病毒衍生序列。这种扩展对应于已知RNA病毒多样性的五倍增长。根据研究作者的说法，许多发现的病毒都是真正的新病毒，与之前已知的病毒只有很遥远的关系。两组最不寻常的病毒形成了潜在的新门，而其他病毒则被划分为许多新的类和目。
        
作者说:“最值得注意的发现是感染细菌的病毒数量和多样性的急剧增加，这些病毒在RNA病毒中所占的比例比我们之前认为的要大得多。”根据研究作者的说法，大量新的RNA病毒基因组的收集为RNA病毒的进化提供了洞察，应该成为RNA病毒学的主要资源。

### 01 病毒与宿主细胞的军备竞赛
病毒进化的标准概念是，在一场常年的军备竞赛中，病毒与细胞宿主锁在一起。在这场竞赛中， 宿主进化出多样化的反病毒防御机制，而

病毒则以进化的反防御系统来回应，抑制宿主的防御。与军备竞赛相辅相成的是病毒与其宿主之间广泛的基因流动。病毒经常获得宿主基因并利用它们在病毒繁殖和病毒-宿主相互作用中发挥作用，相反，宿主捕获病毒基因并将其重新用于防御和其他细胞功能。

与生物体之间的水平基因转移相比， 病毒与宿主之间的基因流动是病毒繁殖的关键特征 ，这个过程严格在细胞内发生，使复制的病毒基因组与宿主的基因组和mRNA密切接触。

从上述角度出发，研究人员研究了病毒是如何捕获宿主基因的，以及近40亿年的病毒-宿主共同进化过程是如何塑造病毒基因组的。

### 02 病毒标志蛋白VHPs
研究人员首先通过对病毒蛋白质组的比较分析，确定了一小组病毒标志蛋白(VHPs)。这些蛋白质广泛分布在不同的病毒中， 复制相关VHPs在病毒基因组复制过程中发挥作用，而结构性VHPs在病毒粒子包括基因组包装到衣壳形成中发挥关键作用。

由于病毒衣壳蛋白CPs的氨基酸序列通常比与复制相关的VHPs进化快得多，研究人员由此通过建立不同病毒组CPs的进化关系来推断其起源。最终发现 VHPs极有可能是在生命进化早期，现代类型细胞出现之后，从细胞祖先进化而来的，可能早于LUCA。

LUCA是一群生物，复杂程度类似现存的原核生物。此外，RdRP和RT很可能是在推测的RNA世界向RNA-蛋白世界转变过程中出现的， DNA-RNA-蛋白样生物随后出现并演化出LUCA，最后进化出细胞生物。

在病毒复制酶多样化的表象下，是惊人的一致性：所有上述蛋白的结构核心都是RNA识别基序（RRM）结构域，用于结合核酸。在病毒中，该结构域还包含核苷酸多聚化的催化位点及核酸酶活性。这种相对简单、普遍存在又差异巨大的RRM结构域可能是最早出现的蛋白结构域之一，可能在RNA世界晚期就已作为核酶聚合酶的辅因子。RRM的多样性进化产生RdRP、RT及其他病毒或细胞复制相关蛋白，可能发生在原细胞（protocells）中的一系列原始复制子（replicators）中，在DNA基因组的现代复制类型建立之前，当然也在原核样细胞出现之前。

### 03 病毒进化框架

鉴于病毒及其基因的巨大多样性，研究人员最终将许多进化路线容纳于一个简单而有逻辑的概念框架：

病毒的进化是由一组参与基因组复制的核心必需蛋白质所形成的，这些蛋白质可以追溯到进化的前细胞阶段。

除了这些明显的原始蛋白质外，病毒的进化可以描述为从宿主捕获基因以及基因在病毒之间的水平传播和病毒繁殖中发挥各种作用的其他可移动遗传元素。
<img src="https://images.gitee.com/uploads/images/2022/0731/174437_82d32519_11385076.png">
## Cell 子刊：病毒进化的逻辑
2022年7月13日，美国国家生物信息中心的Eugene V. Koonin、Valerian V. Dolja和法国巴斯德研究所的Mart Krupovic联名在Cell Host & Microbe发表观点文章The logic of virus evolution，讨论一种解释病毒宏观进化的简单功能逻辑。作者认为一小部分负责基因组复制的核心基因传承自原始复制子，但从编码衣壳蛋白的基因起，大部分病毒基因陆续来自于自宿主。上述基因中最古老的可以追溯至所谓的最后一个共有细胞祖先（LUCA）。病毒基因的获取主要来自两种途径：直接有助于病毒复制的基因可直接招募，当宿主基因可被重利用做某些病毒功能时可扩展利用。

现有理论和经验证据都支持一种假设：除某些胞内病原体，所有细胞生物都可作为多种病毒的宿主。病毒多样性之丰富远超其宿主，至少表现在两个维度上：其一，细胞生物使用统一的遗传信息储存和表达模式，即由双链DNA基因组转录成mRNA，再翻译成蛋白质，但病毒可高效利用上述两类核酸分子进行任何形式的信息传递。其二，病毒基因的进化速度通常远快于其宿主，且病毒基因在其序列中占比更高。事实上，同一科（family）中的病毒序列多样性甚至超过细胞生物一个域（domain）的多样性，这使得分析病毒基因的深度进化关系和祖先更具挑战性。

当我们谈论病毒进化时，标准概念是指病毒被限定于其细胞宿主中持续进行对抗的过程。在这场军备竞赛中，宿主演化并创造出多种抗病毒防御机制，病毒也相应地产生对抗系统用于压制宿主防御。而病毒和宿主间大量的基因交换使这种竞争更加复杂纠缠：病毒频繁地获取宿主基因，并使其服务于病毒复制和病毒-宿主互作；相对应地，宿主也获取病毒基因，并将其改造为防御手段或其他功能。与物种间的水平基因转移不同，病毒和宿主间的基因流得益于病毒复制的关键特征，即严格限定于胞内，这允许复制中的病毒基因组与宿主的基因组和mRNA近距离接触。

### 结论
进化的许多路径都适用于一个概念框架：病毒的进化似乎萌芽于一小组涉及基因组复制的核心基础蛋白，它们可以追溯至细胞出现前阶段。

除上述原始蛋白，病毒的进化过程就是一个不断从宿主获取基因的过程，及随后出现的病毒间水平扩散基因的扩展适应。

通常来说，病毒的基因组越大，带有的辅助功能越多。病毒基因组的扩展过程伴随着多种辅助基因的获得，这增加了病毒的适应性或自主性，尽管伴随着与宿主功能的大量冗余。但这种冗余背后的逻辑在于，可以克服宿主设置的功能使用限制。此外，冗余的功能还可促进宿主代谢以维持活跃的病毒复制。

DNA和RNA病毒基因组扩展的机会是不同的。

总之，病毒进化的逻辑决定于病毒的关键生物学特征，即严格胞内寄生。这使得病毒有充足的机会直接获取或扩展进化宿主功能基因，但也需要克服宿主的防御系统。相对应地，病毒的基因也多次被宿主获取，病毒的某些基因也被宿主扩展功能，从而使得基因和功能在病毒和宿主间实现双向传递。深入理解病毒进化的逻辑将有助于探究病毒-宿主共进化及抗病毒疗法的开发。
病毒基因组的大小横跨三个数量级——最小不过2kb（单链RNA），最大甚至超过2Mb（某些DNA病毒，如巨型潘多拉病毒）。因此病毒基因组既包括已知最简单的蛋白编码复制子，也包括巨型病毒，后者甚至超过某些原核和单细胞真核生物基因组的尺度和复杂性。
<img src="https://pic.rmb.bdstatic.com/bjh/down/057bf925f84e971a92fdd901bdb3b074.png@wm_2,t_55m+5a625Y+3L0Jpb0FydOakjeeJqQ==,fc_ffffff,ff_U2ltSGVp,sz_27,x_17,y_17">
## 致敬颜宁：深圳医学科学院
“搞研究就是一山望着一山高，哪怕登顶珠穆朗玛峰，又会想去月球。”
                                                                                                              --颜宁

2022年11月1日召开的“2022深圳全球创新人才论坛”上，知名结构生物学家颜宁宣布，她将辞去普林斯顿大学教授一职回国，创建深圳医院科学院(以下简称“深圳医科院”)。

伴随着这一官宣，颜宁提到的医学科学院随之被推至大众视野。深圳医科院是一所集科研、转化、学生培养、经费资助等若干功能于一体的新型研发机构，英文简称“SMART”。

深圳市委宣传部副部长吴筠向《中国新闻周刊》表示，“深圳医科院的建设背后涉及很多决策，颜宁后续会担任医科院院长，目前相关手续仍在办理中，还未正式任命。”
<img src="https://foruda.gitee.com/images/1667735637137579580/7ea27bf1_11563376.png">