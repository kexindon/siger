### [CAR-T](CAR-T.md)

- [120万一针抗癌？河南首例接受CAR-T细胞疗法的癌症患者，如今咋样了](https://www.sohu.com/a/560545543_120094090)
- [120万一针抗癌治疗成功，一个月复查癌细胞消失五分之四，患者：感觉良好](https://view.inews.qq.com/wxn/20220623V03H6V00)
- [CAR-T细胞疗法纳入保障，覆盖近40省市惠民保、50余商业险](https://caifuhao.eastmoney.com/news/20220613101423258718910)
- [6款CAR-T疗法已全球上市！会是下一个PD-1?](http://med.china.com.cn/content/pid/313802/tid/1026/iswap/1)
- [一文读懂，CAR-T疗法与TCR-T疗法的区别](https://new.qq.com/omn/20211209/20211209A01F1N00.html)

<p><img width="32.1%" src="https://images.gitee.com/uploads/images/2022/0707/222414_48ecbb61_5631341.jpeg" title="CAR-T副本.jpg"></p>

### [进化与进化论](进化与进化论.md)

- [地球上的生物进化史，你知道多少？](https://page.om.qq.com/page/OGCMLp7FryibERXoY8Gzo29Q0)
- [进化计算前沿综述：值得算法学习的6个生物进化特征](https://mp.weixin.qq.com/s/p8KS81X_w9zaUb6iiCMJsQ)
- [鸡生蛋还是蛋生鸡？关于生命起源的猜想](https://zhuanlan.zhihu.com/p/115861485)
- [从原始细胞到神经系统——大脑的演化](https://zhuanlan.zhihu.com/p/115864894)
- [“无恶不作”，或“无所不能”：被误解百年的进化论](https://www.163.com/dy/article/HDADI30J05318Y5M.html)
- [细思极恐！Cell 子刊 揭开病毒进化的秘密](https://mp.weixin.qq.com/s/JFzHNKgFYmwl56I6R9k4vQ)
- [潮人新知｜人工智能真的会进化成一种新物种？](https://baijiahao.baidu.com/s?id=1724103175895049550)

<p><img width="32.1%" src="https://images.gitee.com/uploads/images/2022/0801/005719_685568af_5631341.jpeg" title="进化论副本.jpg"></p>

### [为什么我们要达观？](为什么我们要达观？.md)

- [关于饶毅教授评点本人科普工作的回应](https://mp.weixin.qq.com/s/ibDnaEANSmI6S9tHfiZldA)

<p><img width="32.1%" src="https://foruda.gitee.com/images/1660377229202362063/234达观副本.jpeg" title="234达观副本.jpg"></p>