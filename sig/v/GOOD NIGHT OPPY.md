《[晚安机遇号](https://www.bilibili.com/video/BV1k44y1D779)》

- [机遇号为何比其它火星车、月球车走得更长更久？](https://zhuanlan.zhihu.com/p/57055686)
- [机遇号的设计寿命只有3个月，她是如何创造纪录在火星上孤独的运行15年的](https://www.bilibili.com/video/BV16D4y1U7U6)
- I’ll Be Seeing You [再见机遇号：失联8个月 一千多次呼叫后被放弃](https://www.dogstar.net/post/420/)

<p><img width="706px" src="https://foruda.gitee.com/images/1669835171686059765/fef0084d_5631341.jpeg" title="481OPPY.jpg"></p>

> 谁特么能想到看一个在火星上执行科考任务的小小火星车的纪录片能看得人热泪盈眶？《[晚安机遇号](https://www.bilibili.com/video/BV1k44y1D779)》，一个在火星上孤独行走了15年的火星车（任务原计划9个月），最终“死”于一场火星风暴。

米洛 你看！那是什么？
- 火箭
- 太对了！
- 还有一辆漫游车！
- 漫游车 对 漫游车在里面！

感谢字幕小组，为同学们提供了众多的学习线索，但这还不够，SIGer 拉片儿有自己独特的科普视角，挖掘更多的线索，尽显编委的核心超能力。

# [GOOD NIGHT OPPY.srt](https://gitee.com/yuandj/siger/issues/I640LK#note_14827102_link)

1
00:00:23,232 --> 00:00:25,234
(wind whistling softly)

2
00:00:29,697 --> 00:00:31,699
 

3
00:00:36,036 --> 00:00:38,038
(wind howling)

4
00:00:50,259 --> 00:00:53,512
STEVE SQUYRES:
At the beginning, there's nothing.

5
00:00:55,973 --> 00:01:01,145
There's no concept of a robot explorer

6
00:01:01,228 --> 00:01:03,898
crawling across the surface
of another world.

7
00:01:08,611 --> 00:01:13,616
And then, gradually, you start to think.

8
00:01:13,699 --> 00:01:15,785
You start to act.

9
00:01:15,868 --> 00:01:17,578
We just start to build.

10
00:01:20,372 --> 00:01:23,083
And those machines come to life.

11
00:01:23,167 --> 00:01:25,169
 

12
00:01:48,275 --> 00:01:50,277
 

13
00:02:04,166 --> 00:02:06,168
 

14
00:02:13,592 --> 00:02:16,011
BEKAH SOSLAND SIEGFRIEDT:
A lot of people out there would say,

15
00:02:16,095 --> 00:02:18,097
"Oh, they're just robots."

16
00:02:18,180 --> 00:02:19,723
(beeping, whirring)

17
00:02:19,807 --> 00:02:22,184
But once we turned them on
for the first time,

18
00:02:22,268 --> 00:02:26,480
they became so much more
than just robots on another planet.

19
00:02:41,787 --> 00:02:43,789
("Roam" by The B-52's playing)

20
00:02:45,833 --> 00:02:47,459
(electronic trilling)

21
00:02:49,128 --> 00:02:51,714
 Boy, mercury  

22
00:02:51,797 --> 00:02:55,801
 Shooting through every degree  

23
00:02:55,885 --> 00:02:59,179
 Oh, girl dancing down those dirty  

24
00:02:59,263 --> 00:03:03,350
 And dusty trails  

25
00:03:03,434 --> 00:03:06,687
(over speaker):
 Roam if you want to  

26
00:03:06,770 --> 00:03:10,149
 Roam around the world  

27
00:03:10,232 --> 00:03:13,736
 Roam if you want to  

28
00:03:13,819 --> 00:03:17,323
 Without wings, without wheels  

29
00:03:17,406 --> 00:03:20,159
 Roam if you want to...  

30
00:03:20,242 --> 00:03:22,453
KOBIE BOYKINS:
Once the rover's on Mars,

31
00:03:22,536 --> 00:03:25,080
it has its own life.

32
00:03:25,164 --> 00:03:27,708
There's energy pulsing through its veins.

33
00:03:28,918 --> 00:03:31,879
And it needs to be given love.

34
00:03:32,880 --> 00:03:34,924
("Roam" continues)

35
00:03:35,007 --> 00:03:38,010
ASHLEY STROUPE: And so we try
and keep her as safe as possible...

36
00:03:40,220 --> 00:03:43,432
...but sometimes she has
a mind of her own.

37
00:03:59,740 --> 00:04:01,659
 Take it hip to hip  

38
00:04:01,742 --> 00:04:07,247
 Rocket through the wilderness  

39
00:04:07,331 --> 00:04:09,124
 Around the world...  

40
00:04:09,208 --> 00:04:11,710
DOUG ELLISON:
And so, yeah, it's only a robot.

41
00:04:12,920 --> 00:04:16,757
But through this robot, we are on
this incredible adventure together.

42
00:04:18,217 --> 00:04:21,512
And she becomes a family member.

43
00:04:21,595 --> 00:04:24,848
 Roam if you want to  

44
00:04:24,932 --> 00:04:30,104
 Without anything but the love we feel.  

45
00:04:30,187 --> 00:04:33,399
-(clicking, whirring)
-(rumbling)

46
00:04:33,482 --> 00:04:35,484
(buzzing)

47
00:04:35,567 --> 00:04:37,569
(song fades)

48
00:04:38,821 --> 00:04:40,823
(wind howling)

49
00:04:44,201 --> 00:04:46,578
 

50
00:05:12,813 --> 00:05:14,815
 

51
00:05:44,094 --> 00:05:47,806
JENNIFER TROSPER:
Something I think we all wonder about

52
00:05:47,890 --> 00:05:50,017
as we look up into the night sky...

53
00:05:52,019 --> 00:05:55,647
...is if we're really alone
in this universe.

54
00:05:57,483 --> 00:05:59,651
And trying to understand that

55
00:05:59,735 --> 00:06:02,196
is one of the great mysteries
that we have.

56
00:06:05,115 --> 00:06:07,409
ROB MANNING:
Over the centuries,

57
00:06:07,493 --> 00:06:10,662
Mars has been this enigmatic

58
00:06:10,746 --> 00:06:13,165
little red dot in the sky.

59
00:06:15,334 --> 00:06:18,212
It invigorated imaginations
of millions of people.

60
00:06:21,465 --> 00:06:24,468
What could be going on
in that distant land?

61
00:06:28,097 --> 00:06:31,600
STROUPE:
The overall goal of the whole Mars program

62
00:06:31,683 --> 00:06:36,271
has been the question of:
Did Mars ever actually have life?

63
00:06:36,355 --> 00:06:39,274
So, especially early on
in the Mars missions,

64
00:06:39,358 --> 00:06:40,776
we were following the water.

65
00:06:42,986 --> 00:06:46,949
That's because, at least on Earth,
everywhere that we find water...

66
00:06:49,243 --> 00:06:50,911
...there's life.

67
00:06:52,830 --> 00:06:54,832
(screeching)

68
00:06:57,251 --> 00:06:59,795
And so the question is:

69
00:06:59,878 --> 00:07:02,172
Was there water on Mars?

70
00:07:02,256 --> 00:07:04,883
And what kind of water?

71
00:07:04,967 --> 00:07:08,137
And could that have helped sustain life?

72
00:07:12,391 --> 00:07:15,144
MANNING: So, in the mid '70s,
the two Viking missions

73
00:07:15,227 --> 00:07:18,147
were kind of the epitome
of exploration at the time.

74
00:07:19,231 --> 00:07:21,984
NASA sent two orbiters and two landers,

75
00:07:22,067 --> 00:07:25,863
which would give us
a whole new perspective on Mars.

76
00:07:29,533 --> 00:07:31,118
SQUYRES:
Uh-huh.

77
00:07:31,201 --> 00:07:33,245
Ah...

78
00:07:35,706 --> 00:07:37,166
Wow.

79
00:07:37,249 --> 00:07:38,917
Yeah, there's the good one.

80
00:07:40,752 --> 00:07:42,296
Wow.

81
00:07:42,379 --> 00:07:44,548
(exhales, chuckles)

82
00:07:48,844 --> 00:07:50,846
It's funny to have such intense memories

83
00:07:50,929 --> 00:07:53,140
associated with a bunch
of 40-year-old pixels.

84
00:07:53,223 --> 00:07:55,225
(laughs)

85
00:07:55,309 --> 00:07:57,186
But I do, man.

86
00:07:57,269 --> 00:07:59,605
I remember the very first time I saw it.

87
00:07:59,688 --> 00:08:01,690
 

88
00:08:04,067 --> 00:08:09,198
At the time of the Viking mission,
I was a bang-it-with-a-hammer geologist.

89
00:08:09,281 --> 00:08:12,784
I would go out in the field,
and I would do geologic fieldwork.

90
00:08:14,286 --> 00:08:16,705
Fascinating science,

91
00:08:16,788 --> 00:08:20,083
but what I found disappointing about it

92
00:08:20,167 --> 00:08:22,961
was that there weren't
new places to discover.

93
00:08:25,714 --> 00:08:28,550
But then I started working with the images

94
00:08:28,634 --> 00:08:31,803
from the Viking orbiters,
and I would look down

95
00:08:31,887 --> 00:08:34,306
on Mars using these pictures...

96
00:08:35,933 --> 00:08:38,936
...and I had no idea
what I was looking at,

97
00:08:39,019 --> 00:08:41,355
but the beauty of it was nobody did.

98
00:08:44,274 --> 00:08:47,069
This was seeing stuff
nobody had ever seen before.

99
00:08:49,071 --> 00:08:51,907
And I knew that I was gonna do
space exploration.

100
00:08:54,201 --> 00:08:57,579
MANNING: The two Viking orbiters,
as they looked down on Mars, they saw...

101
00:08:57,663 --> 00:08:59,665
You know, that's strange.

102
00:08:59,748 --> 00:09:03,043
There could be signs
of past water flowing.

103
00:09:04,503 --> 00:09:07,089
Was Mars once a green world

104
00:09:07,172 --> 00:09:10,050
with living things and-and blue oceans?

105
00:09:12,594 --> 00:09:14,763
SQUYRES:
We'd go there ourselves if we could.

106
00:09:15,806 --> 00:09:17,808
But we can't.

107
00:09:17,891 --> 00:09:21,436
And I just knew
from my training as a geologist

108
00:09:21,520 --> 00:09:25,732
that if we could get a rover
down on the Martian surface

109
00:09:25,816 --> 00:09:28,110
and it could move around and travel

110
00:09:28,193 --> 00:09:30,696
and actually look close up at the rocks,

111
00:09:30,779 --> 00:09:34,741
we might find out the truth
about Martian history.

112
00:09:38,620 --> 00:09:41,707
And so, starting in the mid '80s,

113
00:09:41,790 --> 00:09:45,377
I'd spent ten years
writing proposals to NASA,

114
00:09:45,460 --> 00:09:47,754
but the proposals all failed. (laughs)

115
00:09:49,172 --> 00:09:51,591
And I was facing
the unpleasant possibility

116
00:09:51,675 --> 00:09:54,511
that I had just wasted
an entire decade of my career

117
00:09:54,594 --> 00:09:56,179
with nothing to show for it.

118
00:09:56,263 --> 00:09:58,098
 

119
00:09:58,181 --> 00:10:01,852
MANNING:
But then we pulled a team together at JPL.

120
00:10:01,935 --> 00:10:06,315
Could we actually put the rover
that Steve Squyres imagined

121
00:10:06,398 --> 00:10:10,652
and use this landing system
that we already designed?

122
00:10:12,112 --> 00:10:14,364
So we produced a proposal

123
00:10:14,448 --> 00:10:17,326
and presented that to NASA.

124
00:10:18,493 --> 00:10:21,663
SQUYRES:
And we finally get the phone call...

125
00:10:22,956 --> 00:10:25,500
...that made our dream come true.

126
00:10:25,584 --> 00:10:28,754
Well, I am indeed very, very happy
that we're able to announce

127
00:10:28,837 --> 00:10:33,925
that we are returning to Mars--
this time in force-- with twins.

128
00:10:34,009 --> 00:10:36,136
The Mars twin rovers.

129
00:10:36,219 --> 00:10:39,723
SQUYRES:
We named 'em Spirit and Opportunity.

130
00:10:39,806 --> 00:10:43,101
This was ten years of writing proposals

131
00:10:43,185 --> 00:10:46,855
that finally produced the result
that I'd been dreaming of.

132
00:10:47,898 --> 00:10:52,361
But I think, if I had known at that time

133
00:10:52,444 --> 00:10:56,073
what an arduous path
it was going to be from that point

134
00:10:56,156 --> 00:10:57,949
to actually get onto the surface of Mars,

135
00:10:58,033 --> 00:11:00,744
I wouldn't have felt
quite as elated as I did.

136
00:11:00,827 --> 00:11:03,372
(indistinct chatter)

137
00:11:06,375 --> 00:11:09,086
MAN: If we could take our seat,
I'd like to get started.

138
00:11:09,169 --> 00:11:12,672
TROSPER:
Okay, I'm up here as the project engineer

139
00:11:12,756 --> 00:11:15,509
in order to make sure
that the big picture fits together

140
00:11:15,592 --> 00:11:17,844
between the flight system
and mission system.

141
00:11:17,928 --> 00:11:20,555
I'll briefly go over launch,
cruise and EDL.

142
00:11:20,639 --> 00:11:26,061
Our whole objective was to build
two autonomous

143
00:11:26,144 --> 00:11:30,148
solar-powered rovers
that could survive 90 sols,

144
00:11:30,232 --> 00:11:32,109
three months on Mars.

145
00:11:33,360 --> 00:11:36,947
And we were really hoping
that at least one of them would work.

146
00:11:38,198 --> 00:11:43,412
But we knew that if we don't get it right
we're gonna miss our launch date.

147
00:11:44,913 --> 00:11:47,207
SQUYRES:
Schedule for mission to Mars

148
00:11:47,290 --> 00:11:50,293
is literally driven by
the alignment of the planets,

149
00:11:50,377 --> 00:11:54,214
and if you miss that launch window,
the next one comes around...

150
00:11:55,298 --> 00:11:57,092
...26 months later.

151
00:11:58,760 --> 00:12:02,848
MANNING: That's no time to design,
develop and test two rovers

152
00:12:02,931 --> 00:12:04,933
and put them on two rockets.

153
00:12:06,184 --> 00:12:09,396
And the pressure on the team
is really phenomenal.

154
00:12:09,479 --> 00:12:12,899
So we had to come up with an amazing team

155
00:12:12,983 --> 00:12:15,819
working around the clock to pull it off.

156
00:12:19,448 --> 00:12:22,075
BOYKINS:
From a young age, I was into Star Trek.

157
00:12:23,285 --> 00:12:25,287
I wanted to be Geordi La Forge.

158
00:12:25,370 --> 00:12:26,663
Engineering, this is La Forge.

159
00:12:26,746 --> 00:12:28,748
Shut down power to all transporters.
I'm on my way.

160
00:12:28,832 --> 00:12:30,542
BOYKINS: But I didn't really know
what that job was.

161
00:12:30,625 --> 00:12:33,545
You know, I knew they were the "engineer,"
but I didn't know what that was.

162
00:12:34,629 --> 00:12:38,425
I just knew that I wanted to be
the person that always fixed things.

163
00:12:41,219 --> 00:12:45,891
Building Spirit and Opportunity
really started on just a whiteboard.

164
00:12:47,726 --> 00:12:50,687
Okay, we want to have a 90-day mission,

165
00:12:50,770 --> 00:12:53,857
and we want to find evidence
of past water.

166
00:12:53,940 --> 00:12:56,067
Okay, what do we need to do that?

167
00:12:56,151 --> 00:12:59,029
And then this team of different engineers

168
00:12:59,112 --> 00:13:02,657
has to bring that rover to life.

169
00:13:06,244 --> 00:13:08,872
ASHITEY TREBI-OLLENNU:
This was my first mission.

170
00:13:08,955 --> 00:13:11,458
And it was very exciting, you know,

171
00:13:11,541 --> 00:13:14,419
doing something
that no one has done before.

172
00:13:14,503 --> 00:13:19,925
I grew up in Ghana, and when I was a kid,
I was very fascinated by radio.

173
00:13:20,008 --> 00:13:24,513
And I also was curious--
are there people inside the radio?

174
00:13:24,596 --> 00:13:26,723
So, one day, I opened a radio,
and I was disappointed

175
00:13:26,806 --> 00:13:28,808
to find there were no people in the radio.

176
00:13:29,100 --> 00:13:31,144
So that's my fascination with engineering.

177
00:13:32,979 --> 00:13:35,482
BOYKINS:
For the rover design...

178
00:13:36,566 --> 00:13:40,737
...it was a deliberate decision
to make the characteristics humanlike.

179
00:13:44,574 --> 00:13:46,284
TREBI-OLLENNU:
When you're a geologist

180
00:13:46,368 --> 00:13:48,662
and you're working in the field,
you typically take the rock

181
00:13:48,745 --> 00:13:50,789
and then break it up to look inside of it.

182
00:13:52,374 --> 00:13:55,001
So the robot needs the robotic arm

183
00:13:55,085 --> 00:14:00,423
that has multiple instruments to take
measurements and microscopic images.

184
00:14:01,424 --> 00:14:03,260
Like a Swiss Army knife.

185
00:14:07,973 --> 00:14:10,517
SQUYRES: Now, the resolution
of the rovers' cameras is

186
00:14:10,600 --> 00:14:12,811
the exact equivalent
of human 20/20 vision.

187
00:14:15,146 --> 00:14:18,775
So, all of a sudden, they start to look
an awful lot like eyeballs.

188
00:14:21,695 --> 00:14:24,781
BOYKINS: And then the height
of the rover was five foot two.

189
00:14:24,864 --> 00:14:27,075
That's the average height
of a human being.

190
00:14:30,287 --> 00:14:33,707
So it would feel like, as the rover
was driving, taking these images,

191
00:14:33,790 --> 00:14:36,001
that a human being
was walking along the surface.

192
00:14:39,963 --> 00:14:42,090
ELLISON:
It's just a box of wires, right?

193
00:14:44,050 --> 00:14:46,052
But you end up with this

194
00:14:46,136 --> 00:14:50,140
cute-ish-looking robot that has a face.

195
00:14:52,601 --> 00:14:56,646
TROSPER: So we had these
amazing science instruments,

196
00:14:56,730 --> 00:14:59,649
but once you put all that stuff
on the rover,

197
00:14:59,733 --> 00:15:01,109
the mass gets bigger.

198
00:15:03,111 --> 00:15:07,032
Then this is gonna be a big problem
for landing on Mars.

199
00:15:07,115 --> 00:15:09,451
But then what I'm trying to look at
is actually literally using

200
00:15:09,534 --> 00:15:13,079
six little bungee cords
attached here to airbags.

201
00:15:13,163 --> 00:15:16,082
And the challenge here is that there's
a lot of different ways to do this.

202
00:15:16,166 --> 00:15:18,043
A lot of different ways to do this.

203
00:15:18,126 --> 00:15:21,546
We don't know which one is the best,
and we really only get one shot at it.

204
00:15:21,630 --> 00:15:23,798
TROSPER:
So our landing system

205
00:15:23,882 --> 00:15:27,385
had these big airbags that inflated.

206
00:15:27,469 --> 00:15:29,846
And they would bounce it
across the surface.

207
00:15:31,014 --> 00:15:33,099
MANNING:
The biggest problem right off the bat--

208
00:15:33,183 --> 00:15:35,602
we started doing the math for how much
the Spirit and Opportunity

209
00:15:35,685 --> 00:15:37,812
were going to weigh,

210
00:15:37,896 --> 00:15:40,148
and will those airbags
be able to handle that weight?

211
00:15:43,109 --> 00:15:45,111
So we started doing tests.

212
00:15:45,195 --> 00:15:47,489
-What the...?
-This is just dandy.

213
00:15:47,572 --> 00:15:49,616
-This is not a problem. Yeah.
-No, this is a... this is a good rock.

214
00:15:49,699 --> 00:15:51,910
-I like this rock.
-Yes.

215
00:15:51,993 --> 00:15:54,037
TROSPER:
And so we were trying out the airbags

216
00:15:54,120 --> 00:15:57,248
with the types of rocks
we could encounter on Mars.

217
00:15:57,332 --> 00:15:59,042
We do the first big drop.

218
00:16:01,294 --> 00:16:04,005
(laughing):
Huge gaping holes in these airbags

219
00:16:04,089 --> 00:16:08,301
get ripped by the rocks, and we're like,
"Oh, this is not good.

220
00:16:08,385 --> 00:16:09,928
Not good at all."

221
00:16:10,929 --> 00:16:13,556
MANNING: The parachutes were
another story altogether.

222
00:16:13,640 --> 00:16:16,059
MAN (over speaker):
Three, two, one.

223
00:16:16,142 --> 00:16:20,063
MANNING: But we did those tests
with this big rocket-shaped payload

224
00:16:20,146 --> 00:16:22,732
and dropped it out of the sky
from a helicopter.

225
00:16:22,816 --> 00:16:26,736
First one, the parachute tore to shreds.

226
00:16:26,820 --> 00:16:28,905
(helicopter blades whirring)

227
00:16:29,989 --> 00:16:31,700
The second one...

228
00:16:31,783 --> 00:16:33,243
MAN:
Oh!

229
00:16:33,326 --> 00:16:34,744
MANNING:
...was torn to shreds.

230
00:16:36,454 --> 00:16:39,457
And so we realized
we didn't have a working parachute.

231
00:16:39,541 --> 00:16:43,336
Unfortunately, that chute
that just exploded was the chute

232
00:16:43,420 --> 00:16:45,880
that we were planning on taking to Mars.

233
00:16:45,964 --> 00:16:49,384
Cut to the quick.
You're in very, very serious trouble.

234
00:16:49,467 --> 00:16:52,095
What part of this gives you gas?

235
00:16:52,178 --> 00:16:53,596
Where are you concerned?

236
00:16:53,680 --> 00:16:56,599
PETE THEISINGER: There's a list of threats
that these guys have come to me with,

237
00:16:56,683 --> 00:16:59,018
and I've added all those threats up.

238
00:16:59,102 --> 00:17:02,939
They go in the category of everything
we can think of that can go wrong.

239
00:17:03,022 --> 00:17:04,441
I understand your concern...

240
00:17:04,524 --> 00:17:06,359
TROSPER:
In the back of your mind, you're like,

241
00:17:06,443 --> 00:17:09,612
"This is a billion-dollar national asset.

242
00:17:09,696 --> 00:17:11,990
This could be a complete disaster."

243
00:17:17,203 --> 00:17:18,997
(machine whirring)

244
00:17:20,373 --> 00:17:22,500
MAN (over speaker):
Okay, we're ready.

245
00:17:22,584 --> 00:17:24,085
Here we go.

246
00:17:24,169 --> 00:17:26,171
(vibrating, rattling)

247
00:17:33,178 --> 00:17:35,972
SQUYRES:
So we built Spirit and Opportunity

248
00:17:36,055 --> 00:17:39,309
with the intention
of them being identical twins.

249
00:17:40,810 --> 00:17:42,771
(laughing):
And they kind of started out that way,

250
00:17:42,854 --> 00:17:45,273
but things diverged quickly.

251
00:17:50,779 --> 00:17:52,697
-(vibrating and rattling stop)
-MAN: Okay, we're all clear.

252
00:17:52,781 --> 00:17:55,158
ELLISON:
All the way through assembly and testing,

253
00:17:55,241 --> 00:17:56,826
it was always Spirit

254
00:17:56,910 --> 00:17:59,412
hitting some sort of test first
and she would fail.

255
00:17:59,496 --> 00:18:00,663
(indistinct chatter)

256
00:18:00,747 --> 00:18:03,875
-We lost a bushing.
-We lost a bushing?

257
00:18:03,958 --> 00:18:06,503
Look on鈥?look on the deck.

258
00:18:06,586 --> 00:18:09,714
ELLISON:
And along comes Opportunity.

259
00:18:09,798 --> 00:18:11,424
(laughing):
Everybody... Okay.

260
00:18:12,509 --> 00:18:15,053
Three, two, one.

261
00:18:15,136 --> 00:18:16,471
-(whirring)
-MAN: Oh.

262
00:18:16,554 --> 00:18:18,473
MAN 2:
Ah!

263
00:18:18,556 --> 00:18:20,517
-Thank you.
-(man whistles)

264
00:18:20,600 --> 00:18:23,478
ELLISON: And on every test, Opportunity
came through with flying colors.

265
00:18:25,104 --> 00:18:29,192
So, even before they left this planet,
Spirit was troublesome,

266
00:18:29,275 --> 00:18:31,277
Opportunity was Little Miss Perfect.

267
00:18:35,156 --> 00:18:39,410
BOYKINS: So, after so much time
testing and building our rovers...

268
00:18:40,954 --> 00:18:44,082
...now it's time to put Oppy
on the ground.

269
00:18:44,165 --> 00:18:46,167
 

270
00:18:47,585 --> 00:18:50,088
This is the very first time

271
00:18:50,171 --> 00:18:52,632
we breathe life into the rover.

272
00:18:53,716 --> 00:18:55,260
Move.

273
00:19:01,808 --> 00:19:03,434
Her first steps.

274
00:19:05,436 --> 00:19:07,438
I'm getting tingly.

275
00:19:07,522 --> 00:19:10,233
'Cause it was like...
(gasps) "It's alive!"

276
00:19:10,316 --> 00:19:12,318
(applause)

277
00:19:15,488 --> 00:19:19,033
SQUYRES: She becomes
almost like a living thing to you.

278
00:19:21,035 --> 00:19:25,373
A real living robot
that you can imagine going to Mars

279
00:19:25,456 --> 00:19:28,293
and doing the things
that you've dreamed of doing there.

280
00:19:30,295 --> 00:19:33,172
To say it's like a child being born
would be to trivialize parenthood,

281
00:19:33,256 --> 00:19:35,174
but it feels sort of like that.

282
00:19:38,469 --> 00:19:41,723
MANNING:
But you feel like it's not clear

283
00:19:41,806 --> 00:19:45,894
your child is really ready
for this wild and woolly world.

284
00:19:47,645 --> 00:19:50,481
SQUYRES: Had we done
all the testing we wanted to do?

285
00:19:51,941 --> 00:19:53,610
Absolutely not.

286
00:19:56,362 --> 00:19:58,531
But eventually, you just run out of time.

287
00:19:59,616 --> 00:20:01,492
And it was time to fly.

288
00:20:05,246 --> 00:20:07,248
(birds chirping, insects trilling)

289
00:20:08,583 --> 00:20:10,627
BOYKINS:
We're out here at 5:30 in the morning,

290
00:20:10,710 --> 00:20:14,130
but, you know, for us,
this is a lot of time,

291
00:20:14,213 --> 00:20:17,216
lot of hours, lot of sleepless nights
coming together, so...

292
00:20:18,509 --> 00:20:21,179
It's sort of surreal. I don't know
if it's really gonna happen yet.

293
00:20:21,262 --> 00:20:24,557
You know, it's like the whole
butterfly thing going on at this point.

294
00:20:24,641 --> 00:20:26,643
 

295
00:20:35,360 --> 00:20:36,986
Lucky peanuts.

296
00:20:38,363 --> 00:20:40,573
TROSPER:
So, Spirit would launch first.

297
00:20:41,741 --> 00:20:44,452
And Opportunity three weeks later.

298
00:20:44,535 --> 00:20:46,579
MARK ADLER:
This is Delta Launch Control at

299
00:20:46,663 --> 00:20:48,289
T-minus eight minutes, 40 seconds,
and counting.

300
00:20:48,373 --> 00:20:52,710
TROSPER: And I was in
the control room for Spirit at JPL.

301
00:20:53,795 --> 00:20:57,256
And I actually like it
when I have a job to do

302
00:20:57,340 --> 00:20:59,342
because then I'm focused

303
00:20:59,425 --> 00:21:02,220
and-and it's a little harder
to get emotional,

304
00:21:02,303 --> 00:21:04,514
because you have something
you have to focus on.

305
00:21:04,597 --> 00:21:09,352
ADLER: This is the final checks
of the Spirit MER-A spacecraft.

306
00:21:09,435 --> 00:21:11,396
TROSPER:
I'm a farm girl from Ohio.

307
00:21:11,479 --> 00:21:14,482
I grew up raising sheep, pigs, cows.

308
00:21:14,565 --> 00:21:17,443
And my dad had worked
in the Army Corps of Engineers...

309
00:21:18,903 --> 00:21:23,741
...on the very first rockets, and he would
just tell these amazing stories.

310
00:21:25,118 --> 00:21:30,581
But aerospace engineering wasn't
something that girls around me did.

311
00:21:32,667 --> 00:21:35,753
So I just couldn't imagine
that I would ever have

312
00:21:35,837 --> 00:21:39,048
the opportunity to send a rover to Mars.

313
00:21:39,132 --> 00:21:41,050
TROSPER (over radio):
MER-2 is go for launch.

314
00:21:41,134 --> 00:21:42,343
MAN (over radio):
Roger.

315
00:21:42,427 --> 00:21:46,347
BOYKINS:
T-minus ten... you start freaking out.

316
00:21:46,431 --> 00:21:49,183
ADLER:
Nine, eight, seven,

317
00:21:49,267 --> 00:21:52,145
six, five, four,

318
00:21:52,228 --> 00:21:54,480
(echoing):
three, two, one.

319
00:21:54,564 --> 00:21:57,358
-BOYKINS: The engines start.
-ADLER: And liftoff!

320
00:21:57,442 --> 00:21:59,235
(rumbling, whooshing)

321
00:21:59,318 --> 00:22:02,071
BOYKINS:
You hear that rocket.

322
00:22:04,699 --> 00:22:06,034
No firecracker, no firecracker,

323
00:22:06,117 --> 00:22:08,036
no firecracker, no firecracker,
no firecracker, no firecracker,

324
00:22:08,119 --> 00:22:10,038
-no firecracker, no firecracker.
-(cheering)

325
00:22:10,121 --> 00:22:12,123
 

326
00:22:15,752 --> 00:22:18,629
MAN: Load relief integrated in.
Vehicle's responding.

327
00:22:18,713 --> 00:22:21,674
Vehicle's recovering very nicely
from the liftoff transition.

328
00:22:22,925 --> 00:22:26,345
I don't know whether I shed a tear,
but I think, you know,

329
00:22:26,429 --> 00:22:29,640
this rocket is carrying
my hopes and dreams.

330
00:22:29,724 --> 00:22:31,726
(chuckles):
And, um...

331
00:22:31,809 --> 00:22:34,312
you know, it's very...

332
00:22:34,395 --> 00:22:37,065
it's very difficult to describe. (laughs)

333
00:22:37,148 --> 00:22:40,818
But you-you feel
your life's work in the rocket.

334
00:22:40,902 --> 00:22:44,030
(whooping, whistling)

335
00:22:44,113 --> 00:22:46,115
(cheering, whistling)

336
00:22:47,867 --> 00:22:49,744
BOYKINS:
I have raised this child.

337
00:22:49,827 --> 00:22:51,454
Yeah!

338
00:22:51,537 --> 00:22:53,456
That's sort of what it feels like.

339
00:22:55,249 --> 00:22:57,502
And now it's that child's moment to shine.

340
00:22:59,962 --> 00:23:02,924
SQUYRES:
But it was hard to say goodbye.

341
00:23:04,217 --> 00:23:08,221
I devoted 16 years of my life
to these rovers.

342
00:23:10,014 --> 00:23:12,558
And then you put 'em on top of a rocket

343
00:23:12,642 --> 00:23:15,019
and you shoot 'em into space

344
00:23:15,103 --> 00:23:17,396
and you're never going to see 'em again.

345
00:23:21,901 --> 00:23:23,736
TROSPER:
For Opportunity,

346
00:23:23,820 --> 00:23:26,572
I was out with my, uh, family,
and we were watching

347
00:23:26,656 --> 00:23:31,202
from the same launchpad that my dad
had launched his missions from.

348
00:23:33,538 --> 00:23:35,790
And he had since passed away.

349
00:23:37,083 --> 00:23:42,004
And he was the proudest dad anybody...
anybody could ever have.

350
00:23:42,088 --> 00:23:46,676
It was just very emotional--
for me, for my mom, for my family--

351
00:23:46,759 --> 00:23:51,055
to just see how he had encouraged
and inspired me

352
00:23:51,139 --> 00:23:53,391
to do space exploration.

353
00:24:06,279 --> 00:24:08,281
(whooshing)

354
00:24:10,575 --> 00:24:14,996
BOYKINS: The travel time to Mars
for both rovers was six and a half months.

355
00:24:18,541 --> 00:24:21,252
Spirit and Opportunity were only
three weeks behind each other,

356
00:24:21,335 --> 00:24:24,088
so they're not super far apart
in celestial terms.

357
00:24:25,423 --> 00:24:28,676
So we have a trajectory to Mars,
and we want to make sure

358
00:24:28,759 --> 00:24:31,596
that we are following that road to Mars
as we move along.

359
00:24:36,309 --> 00:24:40,396
It's like you're in Los Angeles
and you want to hit a golf ball

360
00:24:40,479 --> 00:24:41,981
to hit a door handle
at Buckingham Palace,

361
00:24:42,064 --> 00:24:43,399
and that's what we're trying to do.

362
00:24:46,027 --> 00:24:48,446
BOYKINS:
We call it the quiescent period.

363
00:24:48,529 --> 00:24:51,741
Six and a half months of quiescent time,
nothing going on.

364
00:24:51,824 --> 00:24:54,702
Well, that's not exactly true.

365
00:24:54,785 --> 00:24:57,955
(explosive booming)

366
00:24:58,039 --> 00:25:01,500
MANNING:
We got hit by the largest series

367
00:25:01,584 --> 00:25:04,295
of solar flares
that had ever been seen before.

368
00:25:05,379 --> 00:25:09,884
And we saw this big ejection
of the sun's energies and particles

369
00:25:09,967 --> 00:25:11,969
racing toward our spacecraft.

370
00:25:12,053 --> 00:25:14,055
 

371
00:25:19,894 --> 00:25:22,104
TREBI-OLLENNU:
Throughout solar flares,

372
00:25:22,188 --> 00:25:26,400
the sun releases bursts of plasma.

373
00:25:28,861 --> 00:25:32,990
Plasma is a highly charged
cloud of electrons.

374
00:25:34,492 --> 00:25:38,829
MANNING: And the energetic particles,
which could actually kill a human,

375
00:25:38,913 --> 00:25:41,457
they go slamming right into our rovers.

376
00:25:42,541 --> 00:25:45,378
All the way in to the computer.

377
00:25:50,091 --> 00:25:51,884
BOYKINS:
Really bad for spacecraft.

378
00:25:51,968 --> 00:25:53,970
(rattling)

379
00:25:55,721 --> 00:25:58,724
MANNING: Now software we put on board
had been corrupted.

380
00:26:01,143 --> 00:26:03,271
So we had to reboot both rovers.

381
00:26:07,316 --> 00:26:10,569
BOYKINS: So we told our Johnny Fives
to go to sleep.

382
00:26:11,779 --> 00:26:13,489
This is really scary.

383
00:26:15,741 --> 00:26:20,413
TROSPER: So you're loading this
new version of software up on the vehicles

384
00:26:20,496 --> 00:26:22,164
and transitioning to it.

385
00:26:22,248 --> 00:26:24,500
You know, control, alt, delete,
hoping it all works.

386
00:26:24,583 --> 00:26:26,502
 

387
00:26:26,585 --> 00:26:27,962
(whirring)

388
00:26:28,045 --> 00:26:29,922
(beeping)

389
00:26:30,006 --> 00:26:31,799
(powering up)

390
00:26:32,883 --> 00:26:34,885
(clicking, whirring)

391
00:26:36,929 --> 00:26:39,724
MANNING:
It worked. They rebooted.

392
00:26:42,310 --> 00:26:45,563
And it took us a couple of weeks
to clean up our computers.

393
00:26:48,149 --> 00:26:50,568
By then, the sun had calmed down,

394
00:26:50,651 --> 00:26:54,530
the software was loaded,
and we were ready to land on Mars.

395
00:27:00,536 --> 00:27:02,955
SQUYRES:
But at that time,

396
00:27:03,039 --> 00:27:06,500
two thirds of missions to Mars had failed.

397
00:27:08,294 --> 00:27:11,797
Mars was a spacecraft graveyard...
(chuckles) when we flew.

398
00:27:11,881 --> 00:27:13,883
(whooshing)

399
00:27:15,593 --> 00:27:19,680
A few years before,
NASA launched two missions to Mars,

400
00:27:19,764 --> 00:27:22,350
Mars Polar Lander
and Mars Climate Orbiter.

401
00:27:22,433 --> 00:27:24,769
(explosion)

402
00:27:24,852 --> 00:27:26,270
Both failed.

403
00:27:27,813 --> 00:27:29,857
One burned up in the atmosphere.

404
00:27:29,940 --> 00:27:31,817
The other one crashed on the surface.

405
00:27:33,152 --> 00:27:34,779
BOYKINS:
Mars Climate Orbiter--

406
00:27:34,862 --> 00:27:36,364
it was a communication error.

407
00:27:36,447 --> 00:27:40,743
We were converting
what was given to us in English--

408
00:27:40,826 --> 00:27:42,995
we thought it was given to us in metric.

409
00:27:43,079 --> 00:27:45,831
And that's ridiculously embarrassing.

410
00:27:45,915 --> 00:27:48,709
Big news from outer space,
ladies and gentlemen.

411
00:27:48,793 --> 00:27:53,464
Apparently now, scientists claim
there is no intelligent life...

412
00:27:53,547 --> 00:27:55,800
-at NASA. Yeah.
-(laughter)

413
00:27:55,883 --> 00:27:57,051
Couldn't find it.

414
00:27:57,134 --> 00:28:00,930
SQUYRES:
And so all eyes were kind of on us.

415
00:28:03,265 --> 00:28:08,062
Our team felt that Spirit and Opportunity

416
00:28:08,145 --> 00:28:10,689
needed to be a mission of redemption.

417
00:28:13,275 --> 00:28:17,279
BOYKINS:
As part of the team, we felt as though,

418
00:28:17,363 --> 00:28:19,824
if this landing didn't succeed...

419
00:28:20,991 --> 00:28:23,160
...this might be the end of NASA.

420
00:28:26,080 --> 00:28:29,583
Good evening, everyone,
and welcome to what promises to be

421
00:28:29,667 --> 00:28:33,546
both an exciting and eventful night
here at JPL.

422
00:28:33,629 --> 00:28:36,799
This is live coverage
of Spirit's landing on Mars...

423
00:28:36,882 --> 00:28:39,510
Live coverage
of Opportunity's landing on Mars.

424
00:28:39,593 --> 00:28:43,806
And tonight, the navigation team says
all systems are go.

425
00:28:44,098 --> 00:28:45,891
 

426
00:28:48,310 --> 00:28:51,605
TROSPER: Spirit and Opportunity were
going to land on opposite sides of Mars

427
00:28:51,689 --> 00:28:53,274
three weeks apart.

428
00:28:55,734 --> 00:28:58,028
The anxiety is very high.

429
00:28:58,112 --> 00:29:02,324
I don't know at what point I, uh,
went on blood pressure medication.

430
00:29:03,409 --> 00:29:06,203
WAYNE LEE (over radio): And a pleasant
good evening to the flight deck.

431
00:29:06,287 --> 00:29:09,623
Our current speed is
11,320 miles per hour,

432
00:29:09,707 --> 00:29:12,376
which is fast enough to traverse
a distance equal to the United States

433
00:29:12,460 --> 00:29:13,878
in 12 minutes.

434
00:29:13,961 --> 00:29:17,381
At this time, we'd like to invite you
to sit back and enjoy the landing.

435
00:29:20,634 --> 00:29:22,761
BOYKINS:
So, entry, descent, landing.

436
00:29:24,013 --> 00:29:27,892
It's approximately 86 events where,
if one thing goes wrong...

437
00:29:27,975 --> 00:29:29,977
(whooshing)

438
00:29:30,060 --> 00:29:31,687
...we will lose the rovers.

439
00:29:33,022 --> 00:29:35,733
It's the scariest thing
you can ever think of,

440
00:29:35,816 --> 00:29:38,110
because the communication time

441
00:29:38,194 --> 00:29:41,572
from the rover saying, "Hey,
I'm doing this," to Earth is ten minutes.

442
00:29:44,783 --> 00:29:47,786
There is nothing you can do

443
00:29:47,870 --> 00:29:49,663
other than hope they'd survive.

444
00:29:49,747 --> 00:29:52,458
TROSPER:
We call it the six minutes of terror...

445
00:29:52,541 --> 00:29:56,045
is the time from when the spacecraft

446
00:29:56,128 --> 00:29:58,506
enters the top of the Martian atmosphere

447
00:29:58,589 --> 00:30:03,677
until it does all the autonomous--
all on its own-- activities it needs to do

448
00:30:03,761 --> 00:30:06,013
to get safely landed on the ground.

449
00:30:06,096 --> 00:30:07,973
LEE (over radio):
Atmospheric entry in...

450
00:30:08,057 --> 00:30:10,351
three, two, one.

451
00:30:10,434 --> 00:30:12,978
(whooshing)

452
00:30:13,062 --> 00:30:16,607
TROSPER: Everything is on the line
in the six minutes of terror.

453
00:30:16,690 --> 00:30:18,984
LEE: The vehicle is now
at the top of the Martian atmosphere.

454
00:30:19,068 --> 00:30:21,946
At the time of peak heating,
the heat shield will be heated

455
00:30:22,029 --> 00:30:24,615
to temperatures upwards
of 1600 degrees Celsius.

456
00:30:24,698 --> 00:30:26,909
(wind whistling)

457
00:30:26,992 --> 00:30:28,744
BOYKINS:
Parachute opens.

458
00:30:29,745 --> 00:30:30,996
Slows you down more.

459
00:30:31,080 --> 00:30:33,332
LEE (over radio):
Current velocity is 446 miles per hour.

460
00:30:33,415 --> 00:30:36,085
At this time, we expect
the vehicle has gone subsonic.

461
00:30:38,921 --> 00:30:41,549
BOYKINS: You have this thing
called the heat shield that's super hot,

462
00:30:41,632 --> 00:30:43,467
and you have to get rid of it.

463
00:30:45,469 --> 00:30:48,472
MANNING:
But now the hard part begins.

464
00:30:48,556 --> 00:30:53,227
The lander has to rappel down
on a 20-meter rope.

465
00:30:54,520 --> 00:30:56,855
BOYKINS: So now you have a parachute,
you have this back shell,

466
00:30:56,939 --> 00:30:58,607
you have this lander.

467
00:31:01,860 --> 00:31:03,779
The airbags inflate.

468
00:31:08,033 --> 00:31:11,453
At 40 feet,
the back shell fires retro-rockets,

469
00:31:11,537 --> 00:31:14,248
slows the rover down
to zero miles per hour

470
00:31:14,331 --> 00:31:16,250
and then cuts the last cord.

471
00:31:23,799 --> 00:31:25,134
(quiet chatter)

472
00:31:25,217 --> 00:31:27,094
LEE (over radio):
We won't see a signal at the moment.

473
00:31:27,177 --> 00:31:30,389
MAN: We saw an intermittent signal
that indicated we were bouncing.

474
00:31:30,472 --> 00:31:34,602
However... however, we currently
do not have signal from the spacecraft.

475
00:31:34,685 --> 00:31:36,353
LEE:
Please stand by.

476
00:31:42,610 --> 00:31:44,903
MANNING:
Spirit vanishes.

477
00:31:46,572 --> 00:31:48,949
The signal goes away.

478
00:31:50,034 --> 00:31:51,910
Completely gone.

479
00:31:53,245 --> 00:31:55,247
In other words, she may have crashed.

480
00:32:02,463 --> 00:32:04,131
BOYKINS:
Silence.

481
00:32:05,299 --> 00:32:07,301
Everybody waiting for a signal.

482
00:32:08,385 --> 00:32:10,220
Everybody waiting for something.

483
00:32:12,431 --> 00:32:14,433
(takes deep breath)

484
00:32:15,934 --> 00:32:18,979
MANNING: I was thinking
that we did all of this in vain.

485
00:32:20,397 --> 00:32:22,524
That maybe we lost this mission.

486
00:32:22,608 --> 00:32:24,777
 

487
00:32:30,324 --> 00:32:32,159
POLLY ESTABROOK:
Do you see it? Do you see it?

488
00:32:32,242 --> 00:32:34,203
-Do you see it?
-What do we see? -W-W-Wait, wait.

489
00:32:34,286 --> 00:32:35,871
(cheering, excited chatter)

490
00:32:35,954 --> 00:32:37,414
It's there, Rob!

491
00:32:37,498 --> 00:32:39,958
(excited chattering)

492
00:32:40,042 --> 00:32:42,002
LEE (over radio):
We have a very strong signal

493
00:32:42,086 --> 00:32:43,879
in the left-hand polarization channel,

494
00:32:43,962 --> 00:32:46,423
indicating that鈥?(continues indistinctly)

495
00:32:46,507 --> 00:32:48,759
(excited chattering)

496
00:32:48,842 --> 00:32:50,844
MANNING (over radio):
We're on Mars, everybody.

497
00:33:00,604 --> 00:33:02,856
You see us jumping up and down.

498
00:33:02,940 --> 00:33:04,942
We're not jumping for joy.

499
00:33:05,025 --> 00:33:06,819
We're jumping for relief.

500
00:33:09,822 --> 00:33:12,825
Both rovers landed safely
on the surface of Mars.

501
00:33:15,077 --> 00:33:17,079
(excited chatter)

502
00:33:18,414 --> 00:33:20,541
The signal's going up and down.
It means that...

503
00:33:20,624 --> 00:33:22,334
(chatter continues indistinctly)

504
00:33:27,464 --> 00:33:29,591
NARRATOR:
Spirit rover diary.

505
00:33:29,675 --> 00:33:33,887
January 4, 2004. Sol one.

506
00:33:45,399 --> 00:33:47,609

507
00:33:47,693 --> 00:33:50,112
CHRIS LEWICKI (over radio): Ladies and
gentlemen, you are privileged to be

508
00:33:50,195 --> 00:33:52,906
in one of the most exciting rooms on Earth
at the moment.

509
00:33:53,991 --> 00:33:55,951
ABIGAIL FRAEMAN:
I was actually a high school student

510
00:33:56,034 --> 00:33:57,911
when Opportunity landed.

511
00:33:58,162 --> 00:33:59,997
I was selected, um,

512
00:34:00,080 --> 00:34:02,207
as one of 16 students
from around the world...

513
00:34:03,542 --> 00:34:07,546
...to be in the mission control room
with the science team

514
00:34:07,629 --> 00:34:10,007
when Oppy sent down her first images.

515
00:34:10,090 --> 00:34:12,134
MAN (over radio):
Full Navcams are coming down now.

516
00:34:12,217 --> 00:34:13,844
Full Navcams.

517
00:34:13,927 --> 00:34:15,929
(applause)

518
00:34:18,265 --> 00:34:19,391
LEWICKI (over radio):
Wow!

519
00:34:19,475 --> 00:34:21,393
We are on Mars.

520
00:34:21,477 --> 00:34:23,437
(cheering)

521
00:34:29,693 --> 00:34:34,072
TROSPER: When those first
images come, the relief...

522
00:34:34,156 --> 00:34:38,160
the relief, you know, the level
of my bl-blood pressure going back down.

523
00:34:42,539 --> 00:34:44,374
Then we're all on cloud nine.

524
00:34:46,210 --> 00:34:48,796
(squealing excitedly, laughing)

525
00:34:52,841 --> 00:34:54,343
(sighs)

526
00:34:54,426 --> 00:34:56,220
My child has arrived.

527
00:34:56,303 --> 00:34:57,471
(laughs) It's...

528
00:34:57,554 --> 00:34:58,889
 Ah...  

529
00:35:02,142 --> 00:35:03,727
Welcome to Mars.

530
00:35:08,565 --> 00:35:12,194
NARRATOR:
Opportunity rover diary. Sol one.

531
00:35:13,987 --> 00:35:16,949
The signal from the vehicle
is solid and strong.

532
00:35:17,991 --> 00:35:20,953
Opportunity is on Mars.

533
00:35:21,036 --> 00:35:22,955
 

534
00:35:23,038 --> 00:35:25,040
(whirring)

535
00:35:44,142 --> 00:35:46,854
(buzzes, beeps)

536
00:35:46,937 --> 00:35:51,525
BOYKINS: Opportunity landed in a small
little crater in the Meridiani Planes.

537
00:35:52,985 --> 00:35:56,405
And it was a 300-million-mile hole in one.

538
00:35:58,407 --> 00:36:01,326
NARRATOR:
Pancam, Navcam and Hazcams

539
00:36:01,410 --> 00:36:04,788
are all returning spectacular images.

540
00:36:08,500 --> 00:36:10,586
What in God's name are we looking at?

541
00:36:10,669 --> 00:36:12,713
(rover beeps, buzzes)

542
00:36:14,673 --> 00:36:16,967
SQUYRES:
I will attempt no science analysis,

543
00:36:17,050 --> 00:36:19,261
'cause it looks like nothing
I've ever seen before in my life.

544
00:36:19,344 --> 00:36:21,263
(laughter)

545
00:36:21,346 --> 00:36:24,474
As we had expected, holy smokes.

546
00:36:24,558 --> 00:36:27,853
I'm sorry, I'm just... (stammers, sighs)

547
00:36:27,936 --> 00:36:30,397
(laughter)

548
00:36:30,480 --> 00:36:32,482
I got no words for this.

549
00:36:35,819 --> 00:36:39,823
There was this dark sand everywhere.

550
00:36:42,117 --> 00:36:47,372
And then poking out in the distance
were these light-colored rocks.

551
00:36:48,290 --> 00:36:50,459
They were jumping up and down and saying,

552
00:36:50,542 --> 00:36:53,754
"Oh, my gosh, that's bedrock, you guys.
I see bedrock."

553
00:36:53,837 --> 00:36:56,048
And, you know, of course,
I didn't know what that meant.

554
00:36:56,131 --> 00:36:57,674
I didn't know why that was important.

555
00:36:57,758 --> 00:37:00,010
But I don't think
I slept a wink that night.

556
00:37:00,093 --> 00:37:01,511
It was so exciting.

557
00:37:06,183 --> 00:37:09,144
NARRATOR: It's the stuff that
can tell you what happened right here

558
00:37:09,227 --> 00:37:12,356
in this exact place long ago.

559
00:37:15,067 --> 00:37:17,069
(beeps, whirs)

560
00:37:18,487 --> 00:37:20,489
(beeping)

561
00:37:22,824 --> 00:37:25,452
NARRATOR: Hundreds upon hundreds
of people around the world

562
00:37:25,535 --> 00:37:27,371
have worked on this project.

563
00:37:28,914 --> 00:37:33,210
And it all had to go perfectly
to make this moment happen.

564
00:37:35,253 --> 00:37:37,255
 

565
00:37:43,887 --> 00:37:46,556
(beeping)

566
00:37:48,266 --> 00:37:52,312
Spirit's alive, Opportunity landed safely,

567
00:37:52,396 --> 00:37:56,108
and we've got real bedrock
in front of us at Meridiani.

568
00:37:57,401 --> 00:38:00,278
Now it's time for sleep.

569
00:38:04,241 --> 00:38:05,409
(birds chirping)

570
00:38:05,492 --> 00:38:06,618
REPORTER:
Hello, everyone.

571
00:38:06,702 --> 00:38:10,205
This is a big day for the rover on Mars.

572
00:38:10,288 --> 00:38:14,459
And it's ready to do exactly
what it was designed to do

573
00:38:14,543 --> 00:38:16,670
and be a robotic geologist.

574
00:38:18,171 --> 00:38:22,009
Per custom, our morning
wake-up song is coming right up.

575
00:38:23,719 --> 00:38:26,221
("Born to Be Wild" by Steppenwolf playing)

576
00:38:30,934 --> 00:38:32,894
 Get your motor runnin'  

577
00:38:34,229 --> 00:38:36,356
 Head out on the highway...  

578
00:38:36,440 --> 00:38:39,151
SQUYRES:
A tradition in human spaceflight...

579
00:38:40,235 --> 00:38:42,487
...has been to wake the crew up.

580
00:38:42,571 --> 00:38:45,407
The crew wake-up song,
which they would play music.

581
00:38:45,490 --> 00:38:47,325
You know, "Wake up, guys.
It's time to get to work."

582
00:38:47,409 --> 00:38:49,411
("Born to Be Wild" guitar riff playing)

583
00:38:51,246 --> 00:38:54,791
 Born to be wild...  

584
00:38:54,875 --> 00:38:58,045
STROUPE:
The Martian day-- we call it a sol--

585
00:38:58,128 --> 00:39:01,339
is about 40 minutes longer
than an Earth day.

586
00:39:01,423 --> 00:39:05,469
So your schedule is shifting
by about an hour every single day.

587
00:39:06,553 --> 00:39:08,263
SQUYRES:
We were all living on Mars time.

588
00:39:09,347 --> 00:39:11,349
And it was a tough way to live

589
00:39:11,433 --> 00:39:15,353
'cause, you know, the daily planning
meeting today is gonna start at noon,

590
00:39:15,437 --> 00:39:19,483
and two and a half weeks later,
we started a new day at midnight.

591
00:39:19,566 --> 00:39:21,193
 We were born, born to be wild...  

592
00:39:21,276 --> 00:39:24,404
TROSPER:
And so we were tired, we were jet-lagged,

593
00:39:24,488 --> 00:39:26,156
and we needed to wake up, too.

594
00:39:26,239 --> 00:39:29,117
 I never wanna die  

595
00:39:30,285 --> 00:39:34,289
(playing faintly over speaker):
 Born to be wild...  

596
00:39:34,372 --> 00:39:36,875
(clicking, whirring)

597
00:39:36,958 --> 00:39:39,294
TROSPER:
And now we were on this 90-sol race

598
00:39:39,377 --> 00:39:43,465
to find out as much
as we could about Mars.

599
00:39:43,548 --> 00:39:45,801
 

600
00:39:45,884 --> 00:39:49,054
SQUYRES: We picked the Spirit
landing site, Gusev Crater,

601
00:39:49,137 --> 00:39:53,809
that looked like it had a huge,
dried-up riverbed flowing into it,

602
00:39:53,892 --> 00:39:58,396
and we went there hoping to find
evidence of past water

603
00:39:58,480 --> 00:40:00,690
and past habitability.

604
00:40:01,858 --> 00:40:04,903
I mean, there has to have been
a lake in Gusev Crater

605
00:40:04,986 --> 00:40:06,613
at one time.

606
00:40:12,285 --> 00:40:16,748
FRAEMAN: But all Spirit found
was this prison of lava rocks.

607
00:40:16,832 --> 00:40:18,834
(beeping, whirring)

608
00:40:25,298 --> 00:40:28,051
No evidence at all for any interaction
with water on these rocks.

609
00:40:29,845 --> 00:40:31,847
(clicking, beeping)

610
00:40:37,477 --> 00:40:40,480
ELLISON:
On the other side of Mars,

611
00:40:40,564 --> 00:40:44,526
Opportunity's landing site was unlike
anything we'd ever seen before.

612
00:40:48,530 --> 00:40:51,199
NARRATOR:
Opportunity. Sol eight.

613
00:40:51,283 --> 00:40:53,994
We've gotten down
the first images of the soil

614
00:40:54,077 --> 00:40:56,246
right in front of the rover.

615
00:40:56,329 --> 00:41:00,041
It's the strangest-looking thing
we've ever seen on Mars.

616
00:41:02,419 --> 00:41:06,256
SQUYRES: So it turns out
the surface of Mars at this location

617
00:41:06,339 --> 00:41:08,049
is covered by

618
00:41:08,133 --> 00:41:12,345
an uncountable number
of little round... things.

619
00:41:12,429 --> 00:41:13,555
(chuckles)

620
00:41:13,638 --> 00:41:15,640
(whirring)

621
00:41:18,935 --> 00:41:21,146
And when she got to the outcrop...

622
00:41:22,314 --> 00:41:25,442
...these little round things
were embedded in the rock

623
00:41:25,525 --> 00:41:27,527
like blueberries in a muffin.

624
00:41:35,869 --> 00:41:39,206
FRAEMAN: And it turns out the composition
of these little blueberries

625
00:41:39,289 --> 00:41:42,584
was a mineral called hematite,

626
00:41:42,667 --> 00:41:45,128
which is a mineral that often forms
in the presence of water.

627
00:41:45,212 --> 00:41:47,214
(applause, cheering)

628
00:41:52,052 --> 00:41:55,722
SQUYRES: From the mineralogy,
from the geochemistry,

629
00:41:55,805 --> 00:41:58,892
everything that we needed

630
00:41:58,975 --> 00:42:03,897
to come to a reasonable conclusion
that there was once water on Mars

631
00:42:03,980 --> 00:42:07,567
was right there
in the walls of Eagle Crater.

632
00:42:08,944 --> 00:42:10,862
But...

633
00:42:10,946 --> 00:42:13,240
this is a very acidic environment.

634
00:42:14,783 --> 00:42:17,619
Not a place where life
could have developed.

635
00:42:18,620 --> 00:42:21,581
So, yes, there had been liquid water,

636
00:42:21,665 --> 00:42:24,376
but this wasn't water
that you or I would want to drink.

637
00:42:24,459 --> 00:42:26,461
(beeping, whirring)

638
00:42:30,382 --> 00:42:32,801
It was basically like battery acid.

639
00:42:35,553 --> 00:42:37,555
You would not want
to put your toes in there.

640
00:42:37,639 --> 00:42:39,683
You probably wouldn't have
any toes left if you did.

641
00:42:46,314 --> 00:42:51,569
What you really want is nice, flowing,
neutral-pH groundwater.

642
00:42:54,197 --> 00:42:57,450
And so to go and find
a story of habitability...

643
00:42:59,411 --> 00:43:01,621
...you've got to go
on a bit of a road trip.

644
00:43:04,958 --> 00:43:07,585
But the problem is

645
00:43:07,669 --> 00:43:10,964
these rovers only have 90 days to live.

646
00:43:24,769 --> 00:43:29,274
VANDI VERMA: Rover drivers are those of us
who operate the rover on Mars.

647
00:43:30,567 --> 00:43:32,944
(quiet chatter)

648
00:43:33,028 --> 00:43:35,322
It's such a fun job, but you can't

649
00:43:35,405 --> 00:43:37,365
just use a steering wheel to drive it.

650
00:43:38,450 --> 00:43:41,745
Because it takes anywhere
from four minutes to 20 minutes

651
00:43:41,828 --> 00:43:44,664
for a signal to reach Mars.

652
00:43:44,748 --> 00:43:48,460
So we'd send the commands,
we then go off and sleep...

653
00:43:50,170 --> 00:43:53,256
...and then the rover will execute
the drive that day.

654
00:43:53,340 --> 00:43:55,633
And by the time the drive is done,

655
00:43:55,717 --> 00:43:57,677
we come back
and we get the results of that

656
00:43:57,761 --> 00:43:59,596
and start planning again.

657
00:44:00,972 --> 00:44:03,099
I grew up in India.

658
00:44:03,183 --> 00:44:05,685
And when I was about seven years old,

659
00:44:05,769 --> 00:44:08,813
somebody gave me this book
which was about space exploration,

660
00:44:08,897 --> 00:44:10,648
and I was just blown away.

661
00:44:12,525 --> 00:44:15,195
Did you see how close we get
to that rock in the beginning?

662
00:44:15,278 --> 00:44:19,324
During the mission,
I was pregnant with the twins.

663
00:44:19,407 --> 00:44:23,828
And so it was a different way
for me to relate to the twin rovers.

664
00:44:23,912 --> 00:44:27,207
I thought about these two beings that are

665
00:44:27,290 --> 00:44:29,626
so connected and so similar

666
00:44:29,709 --> 00:44:33,129
and yet are going to have
completely independent lives.

667
00:44:35,924 --> 00:44:38,051
The rovers have their own personality,

668
00:44:38,134 --> 00:44:42,806
and it's hard for me to pick
which one of them is my favorite.

669
00:44:42,889 --> 00:44:44,516
Can't really pick one, you know.

670
00:44:44,599 --> 00:44:46,601
It's sort of like this twin thing.
(laughs)

671
00:44:50,188 --> 00:44:52,482
(indistinct chatter)

672
00:44:54,859 --> 00:44:59,406
TROSPER: In Gusev Crater,
Spirit was in a much colder site.

673
00:44:59,489 --> 00:45:03,159
Opportunity was at the equator,
kind of like the vacation spot on Mars.

674
00:45:04,536 --> 00:45:08,373
And so Spirit just had
a tougher mission ahead of her.

675
00:45:12,127 --> 00:45:16,923
BOYKINS: And so Spirit,
she finds this rock we dubbed Adirondack.

676
00:45:21,386 --> 00:45:23,513
She touches the rock.

677
00:45:23,596 --> 00:45:25,890
(beeping)

678
00:45:28,518 --> 00:45:30,437
And she doesn't call home.

679
00:45:31,521 --> 00:45:35,108
(indistinct radio chatter)

680
00:45:35,191 --> 00:45:37,777
Uh, yes, sir, I'm not seeing
anything from our displays.

681
00:45:37,861 --> 00:45:39,988
Um, you're not seeing
any signal at this time?

682
00:45:44,993 --> 00:45:47,370
-MAN (over radio): Uh, that's a negative.
-Copy.

683
00:45:51,875 --> 00:45:54,878
TROSPER:
I was one of Spirit's mission managers.

684
00:45:54,961 --> 00:45:58,798
And so I didn't go home for several days.

685
00:46:00,800 --> 00:46:04,345
We're all kind of somber
in the mission support area

686
00:46:04,429 --> 00:46:09,350
where we're commanding Spirit and trying
to get any information from her.

687
00:46:09,434 --> 00:46:11,728
(indistinct radio chatter)

688
00:46:11,811 --> 00:46:17,108
And Mark Adler was picking
the wake-up song for the day.

689
00:46:18,193 --> 00:46:20,445
And I was just like,
"Oh, do we have to play a wake-up song?"

690
00:46:20,528 --> 00:46:23,198
You know, I was just worried about Spirit.

691
00:46:24,240 --> 00:46:27,869
You know, the fun part of the wake-up song
was lost on me at that point.

692
00:46:30,371 --> 00:46:32,207
ADLER (over speaker):
And all stations, this is mission.

693
00:46:32,290 --> 00:46:35,585
Uh, today is not the day
to buck a tradition, I think,

694
00:46:35,668 --> 00:46:37,712
so we're gonna play a song.

695
00:46:39,172 --> 00:46:41,799
("S.O.S." by ABBA playing)

696
00:46:47,013 --> 00:46:49,015
Where are those happy days? 

697
00:46:49,098 --> 00:46:51,935
They seem so hard to find 

698
00:46:53,102 --> 00:46:55,188
I tried to reach for you 

699
00:46:55,271 --> 00:46:58,983
But you have closed your mind 

700
00:46:59,067 --> 00:47:03,196
Whatever happened to our love? 

701
00:47:03,279 --> 00:47:05,907
I wish I understood 

702
00:47:07,325 --> 00:47:09,410
It used to be so nice 

703
00:47:09,494 --> 00:47:11,496
It used to be so good 

704
00:47:15,041 --> 00:47:18,503
So when you're near me,
darling, can't you hear me? 

705
00:47:18,586 --> 00:47:20,421
S.O.S. 

706
00:47:21,422 --> 00:47:23,091
(chuckling)

707
00:47:23,174 --> 00:47:26,302
The love you gave me,
nothing else can save me 

708
00:47:26,386 --> 00:47:28,346
S.O.S. 

709
00:47:29,597 --> 00:47:31,724
When you're gone 

710
00:47:31,808 --> 00:47:35,478
Though I try, how can I carry on? 

711
00:47:41,818 --> 00:47:43,820
TROSPER:
I thought, "What a perfect song."

712
00:47:45,029 --> 00:47:47,323
ABBA, "S.O.S."

713
00:47:48,700 --> 00:47:50,618
(whirring, beeping)

714
00:47:50,702 --> 00:47:52,704
("S.O.S." continues faintly over speaker)

715
00:47:52,787 --> 00:47:56,708
NARRATOR: We got back a beep,
but Spirit's a very sick rover.

716
00:47:58,293 --> 00:48:02,463
Her flash memory on board the vehicle
has somehow become corrupted,

717
00:48:02,547 --> 00:48:05,466
so she's been awake
through the last two nights,

718
00:48:05,550 --> 00:48:08,678
crashing and rebooting over and over.

719
00:48:08,761 --> 00:48:11,097
She's up all night.
She's like the teenage kid

720
00:48:11,180 --> 00:48:13,474
who just can't stop,
can't stop playing their video game.

721
00:48:13,558 --> 00:48:15,602
I mean, she was just going
and going and going.

722
00:48:15,685 --> 00:48:18,771
Until her batteries were almost drained.

723
00:48:21,357 --> 00:48:25,111
So we said,
"Let's try to get her shut down."

724
00:48:26,112 --> 00:48:29,532
But we gave her
the gentle shutdown command,

725
00:48:29,616 --> 00:48:31,242
and she wouldn't shut down.

726
00:48:32,327 --> 00:48:34,787
And so we started to get
a little panicked,

727
00:48:34,871 --> 00:48:38,374
'cause now we have to send Spirit
a "shut down, damn it."

728
00:48:38,458 --> 00:48:40,418
It's a command that,
no matter what else happens,

729
00:48:40,501 --> 00:48:42,712
it makes the rover shut down.

730
00:48:42,795 --> 00:48:47,133
MAN (over speaker): 1-4-2 decimal alpha,
decimal shut-down-damn-it until 24 hours.

731
00:48:48,051 --> 00:48:50,053
("S.O.S." continues)

732
00:48:54,098 --> 00:48:57,560
MANNING: We were about ready
to tell the world that we had lost Spirit.

733
00:48:57,644 --> 00:48:58,645
(indistinct radio chatter)

734
00:48:58,728 --> 00:49:00,688
But then, suddenly...

735
00:49:00,772 --> 00:49:03,274
-WOMAN (over radio): Go ahead, Telecom.
-Can confirm data is flowing.

736
00:49:03,358 --> 00:49:06,152
(cheering, applause)

737
00:49:06,235 --> 00:49:09,322
NARRATOR:
After a few nights of severe insomnia,

738
00:49:09,405 --> 00:49:11,157
the rover is now sleeping peacefully.

739
00:49:11,240 --> 00:49:12,992
Go power! (laughs)

740
00:49:13,076 --> 00:49:15,787
NARRATOR:
Spirit is back.

741
00:49:15,870 --> 00:49:17,914
Like a well-oiled machine, isn't it?

742
00:49:17,997 --> 00:49:20,333
-(laughter)
-(song ends)

743
00:49:22,126 --> 00:49:24,128
SQUYRES (over TV):
...is kind of an estimate.

744
00:49:24,420 --> 00:49:27,715
The thing that's ultimately gonna limit
the lifetime of these vehicles

745
00:49:27,799 --> 00:49:30,426
is buildup of dust on the solar arrays.

746
00:49:30,510 --> 00:49:33,721
You can think of 90 sols
as being when the warranty expires.

747
00:49:33,805 --> 00:49:36,140
Okay, that's how long
the mission is intended to last.

748
00:49:36,224 --> 00:49:38,768
We expect to get
at least 90 sols out of it.

749
00:49:38,851 --> 00:49:41,979
How much more than that we get
depends on what Mars gives us.

750
00:49:42,063 --> 00:49:44,065
(whirring)

751
00:49:45,149 --> 00:49:48,820
TROSPER: We were concerned
that after 90 sols on Mars

752
00:49:48,903 --> 00:49:53,491
Spirit and Opportunity would not
have enough power

753
00:49:53,574 --> 00:49:56,160
and that would be the way
that the rovers died.

754
00:49:56,244 --> 00:49:58,246


755
00:50:00,081 --> 00:50:02,083
(beeping)

756
00:50:03,126 --> 00:50:05,920
(whirring)

757
00:50:06,003 --> 00:50:08,005
(beeping)

758
00:50:12,135 --> 00:50:13,594
(beeping)

759
00:50:16,597 --> 00:50:18,599
(wind howling)

760
00:50:24,313 --> 00:50:26,691
TROSPER:
And then we see these dust devils,

761
00:50:26,774 --> 00:50:31,112
and we were concerned about what
they could do to Spirit and Opportunity.

762
00:50:36,159 --> 00:50:37,785
MANNING:
We'd taken this picture

763
00:50:37,869 --> 00:50:40,663
some weeks before, and it was getting
really, really red and dusty.

764
00:50:40,747 --> 00:50:42,915
You could barely see
the solar panels anymore.

765
00:50:44,167 --> 00:50:46,169
But the morning after the dust devil,

766
00:50:46,252 --> 00:50:48,254
it's like somebody came along with Windex.

767
00:50:48,337 --> 00:50:50,381
(imitates bottle spraying)

768
00:50:50,465 --> 00:50:54,260
And the solar panels were as clean
as the day that we landed.

769
00:51:01,058 --> 00:51:05,021
TROSPER: Turns out these dust devils were
the best friends these rovers had.

770
00:51:09,859 --> 00:51:12,862
TREBI-OLLENNU: They were literally
our life support machines.

771
00:51:12,945 --> 00:51:15,490
They come in at the right time

772
00:51:15,573 --> 00:51:18,034
to breathe鈥?They let off...
let off oxygen into us,

773
00:51:18,117 --> 00:51:20,453
and then we get our energies back.
(laughs)

774
00:51:20,536 --> 00:51:22,288
-(cork pops)
-(jovial chatter)

775
00:51:22,371 --> 00:51:24,248
MAN:
Here's to us.

776
00:51:24,332 --> 00:51:26,584
(cheering, applause)

777
00:51:28,336 --> 00:51:32,548
TROSPER: So, we had met our main
mission success requirements: 90 sols.

778
00:51:34,967 --> 00:51:40,181
And we start thinking we have
maybe unlimited life on these rovers

779
00:51:40,264 --> 00:51:42,475
because the dust devils
have really helped us out here.

780
00:51:42,558 --> 00:51:45,102
So let's go, let's hit the road,
pedal to the metal,

781
00:51:45,353 --> 00:51:47,271
and go see Mars.

782
00:51:51,901 --> 00:51:55,571
SQUYRES: We got to sol 90
for both rovers, and we had fun.

783
00:51:57,323 --> 00:51:58,825
So we were doing rover drag races.

784
00:51:58,908 --> 00:52:01,619
I mean, the two rovers were
competing with each other

785
00:52:01,702 --> 00:52:04,247
to see who could do
the most meters on a given sol.

786
00:52:10,253 --> 00:52:12,880
With Spirit, we had this disappointment.

787
00:52:12,964 --> 00:52:15,383
Like, this landing site is not
what we thought it would be.

788
00:52:15,466 --> 00:52:16,843


789
00:52:16,926 --> 00:52:19,136
FRAEMAN:
But Spirit looked off,

790
00:52:19,220 --> 00:52:21,389
and there were
these hills rising in the distance

791
00:52:21,472 --> 00:52:23,432
which were named the Columbia Hills.

792
00:52:23,516 --> 00:52:26,310
And so, if there's any potential evidence
of drinkable water,

793
00:52:26,394 --> 00:52:28,437
maybe we'll find it in them hills.

794
00:52:30,523 --> 00:52:33,109
And on the other side of the planet,

795
00:52:33,192 --> 00:52:37,572
our lucky rover Opportunity
was on a whole nother adventure.

796
00:52:39,031 --> 00:52:41,158
NARRATOR:
Opportunity rover diary.

797
00:52:41,242 --> 00:52:45,872
What we really need is more bedrock
deeper down in the ground.

798
00:52:45,955 --> 00:52:51,294
The closest thing is that big crater
off to the east, named Endurance.

799
00:52:51,377 --> 00:52:54,422
SQUYRES:
The beautiful thing about a crater is that

800
00:52:54,505 --> 00:52:58,801
it's a time-ordered sequence of events
with the old rocks at the bottom

801
00:52:58,885 --> 00:53:02,430
and younger and younger and younger
and younger rocks piled on top.

802
00:53:05,308 --> 00:53:08,269
There's scientific gold down there.

803
00:53:08,352 --> 00:53:13,691
But we had never intended
to drive a rover down such a steep slope.

804
00:53:16,027 --> 00:53:17,820
(whirs, beeps)

805
00:53:17,904 --> 00:53:20,531
ELLISON: It's very easy
to kill a robot on another planet

806
00:53:20,615 --> 00:53:23,034
when you're in a place like Endurance.

807
00:53:23,117 --> 00:53:27,371
I would plan to drive
as far down as we need to drive.

808
00:53:27,455 --> 00:53:29,582
FRAEMAN: The tension between
scientists and engineers is

809
00:53:29,665 --> 00:53:32,084
the scientists are the ones
who want to do the crazy thing.

810
00:53:32,168 --> 00:53:33,878
You know, "I want to drive at this

811
00:53:33,961 --> 00:53:36,464
35-degree slope
because that rock is so interesting,"

812
00:53:36,547 --> 00:53:38,591
and the engineers are the ones who say,

813
00:53:38,674 --> 00:53:40,509
"No, no, that's not safe.
You can't do that.

814
00:53:40,593 --> 00:53:42,553
This thing you want to do
is completely bananas."

815
00:53:42,637 --> 00:53:47,183
Frankly, if we can't climb
pretty reliably up these rocks,

816
00:53:47,266 --> 00:53:50,102
we're not going into this crater.

817
00:53:52,813 --> 00:53:55,066
STROUPE:
So we built a big test bed

818
00:53:55,149 --> 00:53:58,319
with basically a full-scale model
of the rover.

819
00:53:59,403 --> 00:54:01,530
MAN:
Oh, little slippage here.

820
00:54:01,614 --> 00:54:03,741
STROUPE:
We tried to simulate the geometry.

821
00:54:03,824 --> 00:54:05,826
We tried to simulate the soil.

822
00:54:05,910 --> 00:54:07,912
(indistinct chatter)

823
00:54:07,995 --> 00:54:10,206
TREBI-OLLENNU: You know,
the first time you go to test bed

824
00:54:10,289 --> 00:54:12,333
and you just drive straight up,

825
00:54:12,416 --> 00:54:13,542
it comes straight down. (laughs)

826
00:54:13,626 --> 00:54:16,003
(groaning)

827
00:54:17,088 --> 00:54:18,422
STROUPE:
So we inched our way down.

828
00:54:18,506 --> 00:54:20,633
Or I guess, you know,
we should use metric.

829
00:54:20,716 --> 00:54:22,176
We centimetered our way down.

830
00:54:22,259 --> 00:54:24,845
(whirring, beeping)

831
00:54:24,929 --> 00:54:27,598
Very carefully planning the drive

832
00:54:27,682 --> 00:54:30,518
to keep Opportunity
from getting into too much trouble.

833
00:54:38,192 --> 00:54:40,569
(quiet chatter)

834
00:54:40,653 --> 00:54:42,863
MAN:
Did we clear it?

835
00:54:42,947 --> 00:54:46,450
STROUPE: So we came back in
the next morning and looked at the images,

836
00:54:46,534 --> 00:54:50,371
and you could hear the gasp
from different parts of the room.

837
00:54:51,455 --> 00:54:54,250
The surface around the side of the crater

838
00:54:54,333 --> 00:54:58,004
wasn't as grippy as we'd hoped it was,
and she apparently started

839
00:54:58,087 --> 00:54:59,880
sliding down the hill...

840
00:54:59,964 --> 00:55:01,340
(buzzing)

841
00:55:01,424 --> 00:55:04,176
...towards this giant boulder.

842
00:55:04,260 --> 00:55:06,178
(alarm beeping)

843
00:55:06,262 --> 00:55:09,223


844
00:55:12,476 --> 00:55:16,355
STROUPE: But we have something
called autonomy built into the rovers.

845
00:55:19,108 --> 00:55:21,402
We allow the rover to think for itself.

846
00:55:22,486 --> 00:55:28,701
Because the rover knows more
about the situation on Mars than we do.

847
00:55:29,869 --> 00:55:32,121
STROUPE: So, when Opportunity
went down into the crater...

848
00:55:35,541 --> 00:55:38,419
...she noticed that she was
sliding too much downhill

849
00:55:38,502 --> 00:55:41,797
and stopped just centimeters

850
00:55:41,881 --> 00:55:44,925
from the tip of her solar panel.

851
00:55:46,052 --> 00:55:49,055
That short of crashing
into this giant rock.

852
00:55:50,139 --> 00:55:53,225
Which could've been
mission-ending for Opportunity.

853
00:55:55,394 --> 00:55:59,523
We all had heart attacks,
but her autonomy saved us.

854
00:56:00,900 --> 00:56:03,569
And we were so proud
of our lucky rover.

855
00:56:09,408 --> 00:56:11,410
SQUYRES:
With Opportunity,

856
00:56:11,494 --> 00:56:15,372
we had a big, big, big photo printed.

857
00:56:15,456 --> 00:56:19,543
It was a north-to-south strip
that was acquired from orbit.

858
00:56:19,627 --> 00:56:22,296
And it showed Eagle Crater
where we landed,

859
00:56:22,379 --> 00:56:25,841
Endurance Crater where Opportunity was,

860
00:56:25,925 --> 00:56:28,219
and we rolled it out on a table.

861
00:56:30,137 --> 00:56:33,099
Way, way down at the end,
there was this big crater,

862
00:56:33,182 --> 00:56:36,060
you know, kilometers to the south,
that we named Victoria Crater.

863
00:56:42,149 --> 00:56:44,151
ELLISON:
And I know it's ridiculous

864
00:56:44,235 --> 00:56:46,904
to take a mission that was
supposed to be three months and go,

865
00:56:46,987 --> 00:56:48,823
"So, we've got this crater.

866
00:56:48,906 --> 00:56:51,117
We think it'll maybe take
two years to get there."

867
00:56:52,201 --> 00:56:53,911
But we did it anyway.

868
00:56:53,994 --> 00:56:56,122
So, first things first.

869
00:56:58,582 --> 00:57:00,334
Ow! 

870
00:57:00,417 --> 00:57:02,837
("Walking on Sunshine"
by Katrina and The Waves playing)

871
00:57:02,920 --> 00:57:04,922
(laughter)

872
00:57:05,005 --> 00:57:07,174
Hey, yeah 

873
00:57:08,342 --> 00:57:11,178
(over speaker):
I used to think maybe you loved me 

874
00:57:11,262 --> 00:57:14,265
Now, baby, I'm sure 

875
00:57:16,684 --> 00:57:18,561
And I just can't wait... 

876
00:57:18,644 --> 00:57:23,107
MANNING:
So, Victoria Crater was miles away.

877
00:57:23,190 --> 00:57:24,400
Oh, yeah 

878
00:57:24,483 --> 00:57:28,696
I'm walking on sunshine, whoa-oh 

879
00:57:28,779 --> 00:57:30,906
I'm walking on sunshine... 

880
00:57:30,990 --> 00:57:32,616
MANNING:
But it was a pretty clear shot.

881
00:57:32,700 --> 00:57:34,410
There's no hills or mountains in the way.

882
00:57:35,494 --> 00:57:37,538
Just these ripples of dust.

883
00:57:37,621 --> 00:57:39,165
And don't it feel good 

884
00:57:39,248 --> 00:57:40,583
Hey... 

885
00:57:40,666 --> 00:57:43,460
TREBI-OLLENNU:
So we do what we call blind driving.

886
00:57:44,545 --> 00:57:46,172
Oh, yeah 

887
00:57:46,255 --> 00:57:48,716
And don't it feel good... 

888
00:57:48,799 --> 00:57:50,509
So we told Opportunity,

889
00:57:50,593 --> 00:57:52,928
"You're blindfolded.
Trust me. Keep going."

890
00:57:53,012 --> 00:57:55,431
I feel the love, I feel the love
that's really real 

891
00:57:55,514 --> 00:57:58,726
I feel alive, I feel the love,
I feel the love that's really real... 

892
00:57:58,809 --> 00:58:01,854
TREBI-OLLENNU: And with blind drive,
the way we count progress

893
00:58:01,937 --> 00:58:04,356
is the number of wheel turns.

894
00:58:04,440 --> 00:58:07,735
I'm walking on sunshine, whoa-oh 

895
00:58:07,818 --> 00:58:09,737
I'm walking on sunshine... 

896
00:58:09,820 --> 00:58:12,323
MANNING:
So, the wheels had turned on the drive.

897
00:58:12,406 --> 00:58:14,950
I'm walking on sunshine... 

898
00:58:15,034 --> 00:58:16,785
But to our shock...

899
00:58:16,869 --> 00:58:19,747
-And don't it feel good. 
-(song ends)

900
00:58:19,830 --> 00:58:21,957
...Oppy hadn't moved at all.

901
00:58:22,041 --> 00:58:23,667
(whirring)

902
00:58:23,751 --> 00:58:25,002
MAN:
This is the previous day.

903
00:58:25,085 --> 00:58:26,921
MAN 2:
Went to exactly where it got stuck.

904
00:58:27,004 --> 00:58:30,633
It literally didn't make
much more progress after about this point.

905
00:58:30,716 --> 00:58:33,302
But the rover thought
it was executing its plan

906
00:58:33,385 --> 00:58:35,512
as though it were all the way down here.

907
00:58:35,596 --> 00:58:40,893
VERMA: So, the entire day, Opportunity
was just spinning its wheels in place

908
00:58:40,976 --> 00:58:43,938
and digging itself deeper
and deeper and deeper.

909
00:58:44,021 --> 00:58:46,023
(beeping, whirring)

910
00:58:56,200 --> 00:58:58,327
ELLISON: There's no book that's,
you know, "Chapter Four:

911
00:58:58,410 --> 00:59:00,621
Extracting Mars Rovers from Sand Dunes."

912
00:59:00,704 --> 00:59:05,084
So we built a copy of the sand dune
at JPL, stuck her over it.

913
00:59:06,877 --> 00:59:09,588
TREBI-OLLENNU: From an engineering
perspective, it was exciting...

914
00:59:09,672 --> 00:59:10,923
(chuckles)

915
00:59:11,006 --> 00:59:12,841
...because we like...
we-we like a challenge.

916
00:59:14,510 --> 00:59:16,762
So it's almost like quicksand.

917
00:59:16,845 --> 00:59:21,517
And we spent six weeks trying
to learn how to extricate Opportunity.

918
00:59:22,851 --> 00:59:27,564
STROUPE:
But the soil had no friction on it at all.

919
00:59:27,648 --> 00:59:30,150
It was almost like trying
to drive through cake flour.

920
00:59:32,736 --> 00:59:37,032
ELLISON: The engineers decided the only
chance is stick it in reverse and gun it.

921
00:59:47,418 --> 00:59:49,420
(whirring)

922
00:59:56,427 --> 00:59:59,013
But on Mars, it was getting worse.

923
00:59:59,096 --> 01:00:00,806
It looked like we were getting
even deeper.

924
01:00:01,890 --> 01:00:03,600
This could be fatal.

925
01:00:05,728 --> 01:00:08,439
NARRATOR:
Sol 483.

926
01:00:08,522 --> 01:00:11,150
Power has dropped substantially.

927
01:00:11,233 --> 01:00:14,611
At the moment, we're just keeping
our nose above water.

928
01:00:24,455 --> 01:00:26,457
(whirring)

929
01:00:33,005 --> 01:00:35,007


930
01:00:41,013 --> 01:00:43,015
NARRATOR:
Sol 484.

931
01:00:44,516 --> 01:00:47,102
Long-term drive options are now back.

932
01:00:47,186 --> 01:00:49,063
(cheering, applause)

933
01:00:55,069 --> 01:00:57,488
MANNING: We said, "Okay,
let's be a little bit more conservative

934
01:00:57,571 --> 01:01:00,115
about our driving from here on out."
(chuckles)

935
01:01:00,199 --> 01:01:02,493
And so we carefully drove south,

936
01:01:02,576 --> 01:01:05,371
and we finally eventually made it
to Victoria Crater.

937
01:01:12,294 --> 01:01:14,254
SQUYRES:
There was a group of us

938
01:01:14,338 --> 01:01:17,299
sitting around a table
drinking margaritas at a party.

939
01:01:19,093 --> 01:01:22,679
And somebody came up with the idea,
"Hey, let's have a bet."

940
01:01:22,763 --> 01:01:24,431
Picked up a cocktail napkin,

941
01:01:24,515 --> 01:01:26,517
and we all wrote down our names.

942
01:01:26,600 --> 01:01:29,561
And everybody had to put in 20 bucks.

943
01:01:29,645 --> 01:01:32,231
MANNING:
We said, "Okay. Who believes that

944
01:01:32,314 --> 01:01:34,983
zero, one or two rovers
are alive next year?"

945
01:01:36,193 --> 01:01:39,613
SQUYRES:
We kept that same cocktail napkin,

946
01:01:39,696 --> 01:01:43,867
and we did it year after year
after year after year.

947
01:01:46,120 --> 01:01:51,542
MANNING: And every year, Steve Squyres,
the project's principal scientist,

948
01:01:51,625 --> 01:01:55,003
voted that both rovers
would be dead in the next year.

949
01:01:55,087 --> 01:01:59,591
My logic being
that someday I would eventually win,

950
01:01:59,675 --> 01:02:03,220
and when I did win, it would cheer me up
at a time when I'd be feeling sad.

951
01:02:03,303 --> 01:02:05,305
(indistinct chatter)

952
01:02:05,389 --> 01:02:09,560
MANNING: I voted just the opposite--
that both rovers would still be alive.

953
01:02:11,145 --> 01:02:15,023
So, actually, I did pretty well
through the years in these bets.

954
01:02:23,031 --> 01:02:24,825
(whirring)

955
01:02:24,908 --> 01:02:28,829
SQUYRES: At this point, Spirit,
our kind of hardworking blue-collar rover,

956
01:02:28,912 --> 01:02:31,206
was exploring the Columbia Hills.

957
01:02:33,250 --> 01:02:36,295
But she had been having
mechanical problems.

958
01:02:40,716 --> 01:02:43,886
And then the right front wheel failed.

959
01:02:46,805 --> 01:02:49,892
-The five-wheel, five-wheel drive.
-MAN: Okay.

960
01:02:52,853 --> 01:02:56,398
STROUPE: Somebody said,
"This is a lot like that grocery cart

961
01:02:56,482 --> 01:02:59,985
with the stuck wheel
that's easier to pull than push."

962
01:03:01,403 --> 01:03:03,864
We're like, "Pull!
Yes, let's go backwards."

963
01:03:08,911 --> 01:03:12,956
ELLISON: So, Spirit slowly drove
backwards through Columbia Hills,

964
01:03:13,040 --> 01:03:16,210
dragging this broken wheel as it went.

965
01:03:17,252 --> 01:03:20,964
And it was awful
because winter was coming.

966
01:03:21,048 --> 01:03:23,383


967
01:03:26,970 --> 01:03:32,017
BOYKINS: So, a Martian winter
is twice as long as it is on Earth.

968
01:03:33,268 --> 01:03:35,687
(shuddering):
So it gets really cold.

969
01:03:35,771 --> 01:03:40,609
It gets so cold that you have to use
much of your energy

970
01:03:40,692 --> 01:03:43,946
to keep all of your hardware
above a certain temperature,

971
01:03:44,029 --> 01:03:45,864
or it's likely to break.

972
01:03:47,157 --> 01:03:50,285
SQUYRES: At the Spirit site,
we desperately needed a way

973
01:03:50,369 --> 01:03:53,205
to tilt the solar arrays towards the sun.

974
01:03:54,373 --> 01:03:58,710
But the only way to do that
was to tilt the entire vehicle.

975
01:03:59,795 --> 01:04:01,797


976
01:04:10,264 --> 01:04:12,849
Spirit would have to climb backwards

977
01:04:12,933 --> 01:04:16,687
up this rocky, rugged terrain...

978
01:04:19,231 --> 01:04:22,109
...to stay alive all through the winter.

979
01:04:23,151 --> 01:04:25,153
(beeping)

980
01:04:30,576 --> 01:04:32,578
And it's not just the seasons.

981
01:04:32,661 --> 01:04:34,913
You've also got things like dust storms.

982
01:04:36,707 --> 01:04:39,793
SQUYRES: Sometimes dust storms
will blow up into a global storm.

983
01:04:41,003 --> 01:04:44,673
And this one hit Opportunity the hardest.

984
01:04:47,092 --> 01:04:49,928
-NARRATOR: Sol 1,226.
-(thunder crashes)

985
01:04:50,971 --> 01:04:53,807
Opportunity has been
fighting for her life.

986
01:04:55,183 --> 01:04:58,937
Mars took off its gloves
and pounded the Opportunity site

987
01:04:59,021 --> 01:05:01,648
with record high levels
of dust in the sky.

988
01:05:01,732 --> 01:05:03,150


989
01:05:03,233 --> 01:05:05,569
(thunder crashes)

990
01:05:08,447 --> 01:05:12,618
STROUPE: So we had to tweak the rover's
onboard decision-making process

991
01:05:12,701 --> 01:05:15,495
so that when the power
started getting too low...

992
01:05:17,873 --> 01:05:22,044
...Opportunity could shut herself down
to preserve her batteries.

993
01:05:29,009 --> 01:05:30,844
(indistinct radio chatter)

994
01:05:30,927 --> 01:05:32,554
STROUPE:
Nobody wanted to say out loud

995
01:05:32,638 --> 01:05:35,015
that we thought the mission
could end at any moment.

996
01:05:37,184 --> 01:05:39,978
We could see the dust storm.
We could track it from orbit.

997
01:05:42,731 --> 01:05:46,234
But it took weeks
before it started to clear.

998
01:05:48,028 --> 01:05:52,324
The original time that was, uh,
predicted for the data to come down

999
01:05:52,407 --> 01:05:57,037
was 12... 20:40 UTC,
but we are a bit tight.

1000
01:05:57,120 --> 01:06:01,249
So we actually had a lot of suggestions
on morning wake-up songs today,

1001
01:06:01,333 --> 01:06:05,253
so I thought I would just continue
to play them as we get the data.

1002
01:06:05,337 --> 01:06:08,131
(indistinct chatter)

1003
01:06:10,008 --> 01:06:13,053
("Here Comes the Sun" by The Beatles
playing)

1004
01:06:18,350 --> 01:06:20,977
STROUPE:
And then we just had to wait and see

1005
01:06:21,061 --> 01:06:22,979
if we were gonna survive.

1006
01:06:24,481 --> 01:06:26,316
Here comes the sun 

1007
01:06:26,400 --> 01:06:28,443
Doo-da-doo-doo 

1008
01:06:28,527 --> 01:06:30,487
Here comes the sun 

1009
01:06:30,570 --> 01:06:33,448
And I say it's all right 

1010
01:06:37,327 --> 01:06:39,162
Little darling 

1011
01:06:39,246 --> 01:06:43,500
It's been a long cold lonely winter 

1012
01:06:44,751 --> 01:06:46,628
Little darling 

1013
01:06:46,712 --> 01:06:51,133
It feels like years
since it's been here 

1014
01:06:52,467 --> 01:06:54,261
Here comes the sun 

1015
01:06:54,344 --> 01:06:56,096
Doo-da-doo-doo 

1016
01:06:56,179 --> 01:06:58,306
-Here comes the sun 
-(beeping)

1017
01:06:58,390 --> 01:07:01,560
And I say it's all right 

1018
01:07:01,643 --> 01:07:03,645
(whirring)

1019
01:07:05,313 --> 01:07:07,149
Little darling 

1020
01:07:07,232 --> 01:07:10,444
The smile's returning to the faces 

1021
01:07:10,527 --> 01:07:12,612
(cheering)

1022
01:07:12,696 --> 01:07:15,782
Little darling, it seems like years... 

1023
01:07:15,866 --> 01:07:18,326
ELLISON:
I don't think anyone expected the rovers

1024
01:07:18,410 --> 01:07:20,620
to survive all these disasters.

1025
01:07:20,704 --> 01:07:22,706
Here comes the sun 

1026
01:07:24,332 --> 01:07:26,251
Here comes the sun... 

1027
01:07:26,334 --> 01:07:28,336
You get this feeling of--

1028
01:07:28,420 --> 01:07:30,964
there's nothing Mars
can do to us at this point.

1029
01:07:31,047 --> 01:07:33,383
Like, we've survived everything.
We're basically invincible.

1030
01:07:33,467 --> 01:07:37,012
Here comes the sun, doo-da-doo-doo... 

1031
01:07:37,095 --> 01:07:39,222
But the mission wasn't done.

1032
01:07:40,307 --> 01:07:44,644
We were still hoping we would find
a place where life could have arisen,

1033
01:07:44,728 --> 01:07:48,732
with neutral-pH water, with water
you maybe could even have drunk.

1034
01:07:48,815 --> 01:07:50,817
(song ends)

1035
01:07:50,901 --> 01:07:52,903
(wind whistling softly)

1036
01:07:55,781 --> 01:07:57,699
(cheering, applause)

1037
01:07:57,783 --> 01:07:59,034
Thank you.

1038
01:07:59,117 --> 01:08:00,535
-Dr. Squyres.
-Nice to be here.

1039
01:08:00,619 --> 01:08:02,621
-Thank you so much for joining us.
-Yeah.

1040
01:08:02,704 --> 01:08:07,000
Now, um, this is a model of one
of the rovers that's on Mars right now.

1041
01:08:07,083 --> 01:08:08,460
-That's right.
-Which... Is this...

1042
01:08:08,543 --> 01:08:09,878
Is this, uh, Spirit or Opportunity?

1043
01:08:09,961 --> 01:08:11,171
SQUYRES:
Uh, they're basically identical twins,

1044
01:08:11,254 --> 01:08:12,881
-so they look the same.
-So you can't tell the difference

1045
01:08:12,964 --> 01:08:14,174
-between your two children.
-Uh...

1046
01:08:14,257 --> 01:08:15,717
You're a terrible father
is what you're saying.

1047
01:08:15,801 --> 01:08:17,093
-(laughs)
-(audience laughing)

1048
01:08:17,177 --> 01:08:18,303
Um...

1049
01:08:18,386 --> 01:08:20,972
SQUYRES:
So, Spirit and Opportunity's mission

1050
01:08:21,056 --> 01:08:26,102
had kind of taken on a life of its own
with the public.

1051
01:08:27,562 --> 01:08:30,315
The traffic once again
is heavy today on Mars.

1052
01:08:30,398 --> 01:08:32,609
Rovers Spirit and Opportunity still at it.

1053
01:08:32,692 --> 01:08:34,820
So far, they have traveled
nine and a quarter miles

1054
01:08:34,903 --> 01:08:37,572
and captured more than 156,000 images.

1055
01:08:38,657 --> 01:08:40,450
SQUYRES:
NASA does a lot of

1056
01:08:40,534 --> 01:08:42,327
wonderful things in space science.

1057
01:08:42,410 --> 01:08:45,330
But try to explain gamma-ray spectroscopy

1058
01:08:45,413 --> 01:08:47,290
to an eight-year-old-- it's hard.

1059
01:08:49,709 --> 01:08:52,254
But a robot geologist--

1060
01:08:52,337 --> 01:08:55,048
anybody could sort of understand
what it was about.

1061
01:08:56,842 --> 01:08:59,302
And now exploration and adventure

1062
01:08:59,386 --> 01:09:02,931
can become a very large
shared human experience.

1063
01:09:03,014 --> 01:09:04,182
(whirring)

1064
01:09:04,266 --> 01:09:06,393
MAN:
What is he doing?

1065
01:09:06,476 --> 01:09:08,979
MANNING:
The rovers became a phenomenon.

1066
01:09:11,106 --> 01:09:14,276
They represented exploration
and curiosity and interest in the world.

1067
01:09:14,359 --> 01:09:17,070
Godspeed to the Mars rover,
wherever you are tonight.

1068
01:09:17,153 --> 01:09:19,072
MANNING:
And the more these rovers lasted

1069
01:09:19,155 --> 01:09:21,867
and the more promise
of future discovery...

1070
01:09:21,950 --> 01:09:23,535
Elbow. Has a wrist.

1071
01:09:23,618 --> 01:09:27,914
MANNING: ...people around the world were
becoming really attached to these rovers.

1072
01:09:30,083 --> 01:09:32,711
STROUPE:
But I don't think any of us fully realized

1073
01:09:32,794 --> 01:09:37,549
the impact that we were having
on the public until Spirit got stuck.

1074
01:09:42,512 --> 01:09:45,140
TROSPER:
My alter ego, Spirit, had a problem.

1075
01:09:48,810 --> 01:09:51,479
She already had a broken wheel,

1076
01:09:51,563 --> 01:09:54,482
and she had gotten a little bit embedded.

1077
01:09:56,359 --> 01:10:01,072
And then another wheel broke,
and it was getting close to winter.

1078
01:10:01,156 --> 01:10:03,992
(beeps, whirs)

1079
01:10:04,075 --> 01:10:08,997
But I figured, knowing Spirit,
that she'd figure it out.

1080
01:10:09,080 --> 01:10:11,541


1081
01:10:11,625 --> 01:10:14,294
STROUPE:
This mound of rocks,

1082
01:10:14,377 --> 01:10:17,964
this may be
what we're hung on right there.

1083
01:10:18,965 --> 01:10:22,761
And both on Mars and in the test bed,
as we drive, it does sink.

1084
01:10:24,721 --> 01:10:29,726
This first slide here just is an overview
of the energy requirements for Spirit.

1085
01:10:29,809 --> 01:10:34,522
Um, the numbers in red are the ones
where we won't have enough energy

1086
01:10:34,606 --> 01:10:37,609
to survive for an extended period of time.

1087
01:10:38,902 --> 01:10:41,363
STROUPE: So now it was really
a race against the clock.

1088
01:10:41,446 --> 01:10:46,117
We were making very slow progress,
but we had to try and try to beat winter.

1089
01:10:46,201 --> 01:10:51,623
And we started getting letters
and phone calls from the public.

1090
01:10:52,916 --> 01:10:55,001
This real sense of, you know,

1091
01:10:55,085 --> 01:10:58,588
we have to do
whatever it takes to save Spirit.

1092
01:11:02,175 --> 01:11:05,220
VERMA: And the public
called this campaign "Free Spirit."

1093
01:11:06,972 --> 01:11:08,598
And it showed us that

1094
01:11:08,682 --> 01:11:14,396
humans are capable of forming
a connection and a bond to a robot.

1095
01:11:15,730 --> 01:11:19,025
NARRATOR:
Sol 2,196.

1096
01:11:19,109 --> 01:11:22,028
Spirit has been prepared
for her winter sleep.

1097
01:11:23,113 --> 01:11:25,156
She is tucked into bed,

1098
01:11:25,240 --> 01:11:30,203
and now we all watch carefully
for the signal or lack thereof.

1099
01:11:32,914 --> 01:11:35,583
BOYKINS:
As the rover starts to get hypothermia,

1100
01:11:35,667 --> 01:11:37,711
she can't communicate anymore.

1101
01:11:41,297 --> 01:11:44,092
And then she either wakes up
the next morning

1102
01:11:44,175 --> 01:11:46,261
or she doesn't wake up at all.

1103
01:11:51,599 --> 01:11:54,769
When the sun came back up,

1104
01:11:54,853 --> 01:11:58,565
we'd listen, we try to hear

1105
01:11:58,648 --> 01:12:01,609
a whisper, a tone.

1106
01:12:01,693 --> 01:12:03,194
Anything.

1107
01:12:04,946 --> 01:12:06,364
And we don't.

1108
01:12:11,244 --> 01:12:14,414
It did feel like we were,

1109
01:12:14,497 --> 01:12:17,459
you know, watching a friend go,
in a lot of ways.

1110
01:12:19,377 --> 01:12:23,048
I know people think it's weird 'cause
I sound like I'm talking about a person,

1111
01:12:23,131 --> 01:12:25,800
but even though she wasn't a person,

1112
01:12:25,884 --> 01:12:28,762
it was still a huge part
of all of our lives.

1113
01:12:31,681 --> 01:12:34,392
TROSPER: Spirit was our rugged
and adventurous rover,

1114
01:12:34,476 --> 01:12:37,979
and her environment required more of her.

1115
01:12:38,063 --> 01:12:40,482
And so maybe it's just because
I was Spirit's mission manager

1116
01:12:40,565 --> 01:12:42,484
and I wanted her to be like me, but...

1117
01:12:43,777 --> 01:12:47,238
...I feel like I connected
with Spirit in that way.

1118
01:12:52,994 --> 01:12:55,413
You know, maybe she was just
a little tired, too,

1119
01:12:55,497 --> 01:12:57,582
after all her hard work.

1120
01:12:57,665 --> 01:12:59,667


1121
01:13:16,601 --> 01:13:19,479
SQUYRES: At any rate,
if we can get something like

1122
01:13:19,562 --> 01:13:22,482
a hundred meters out of today's drive,

1123
01:13:22,565 --> 01:13:24,442
just project that line that you see...

1124
01:13:24,526 --> 01:13:27,403
MANNING: So, by this time,
only a handful of people

1125
01:13:27,487 --> 01:13:30,365
who were on the design originally
are still left on the team.

1126
01:13:31,574 --> 01:13:36,162
So we're in another generation of
engineers who are operating Opportunity.

1127
01:13:37,163 --> 01:13:39,165


1128
01:13:40,708 --> 01:13:43,044
SIEGFRIEDT:
Never in a million years did I think

1129
01:13:43,128 --> 01:13:44,921
I would be able to work on Opportunity.

1130
01:13:47,757 --> 01:13:52,178
When I was in eighth grade,
I saw this news story

1131
01:13:52,262 --> 01:13:54,597
of Spirit and Opportunity landing.

1132
01:13:56,599 --> 01:13:58,143
I was just some small-town girl

1133
01:13:58,226 --> 01:14:00,019
in the middle of nowhere in Texas.

1134
01:14:01,104 --> 01:14:03,648
But I knew that's what I wanted to do.

1135
01:14:03,731 --> 01:14:06,192
I wanted to help find life
on other planets.

1136
01:14:09,571 --> 01:14:11,406
MOOGEGA COOPER:
When I was about 17,

1137
01:14:11,489 --> 01:14:14,742
there was a naming contest
for Spirit and Opportunity.

1138
01:14:14,826 --> 01:14:17,078
(laughs)

1139
01:14:17,162 --> 01:14:19,372
I ended up submitting the names

1140
01:14:19,455 --> 01:14:21,124
Romulus and Remus.

1141
01:14:22,417 --> 01:14:25,753
Their father was Mars, the god of war.

1142
01:14:27,005 --> 01:14:31,092
I don't know what I was thinking, but...

1143
01:14:31,176 --> 01:14:36,097
that is when my brain was completely
turned on to Mars and space exploration...

1144
01:14:38,558 --> 01:14:42,478
...and eventually led me
to NASA's Mars program.

1145
01:14:44,480 --> 01:14:47,108
SIEGFRIEDT:
When I first started at JPL,

1146
01:14:47,192 --> 01:14:49,986
Opportunity was this older rover

1147
01:14:50,069 --> 01:14:52,197
that was in her extended, extended

1148
01:14:52,280 --> 01:14:53,823
times a thousand mission. (chuckles)

1149
01:14:54,908 --> 01:14:58,953
But she is the reason
I started aerospace engineering.

1150
01:14:59,037 --> 01:15:03,291
I knew Opportunity was the place
I wanted to start my career.

1151
01:15:03,374 --> 01:15:06,377
All stations, this is your TDL.

1152
01:15:06,461 --> 01:15:09,547
We will begin the downlink briefing
in about five minutes.

1153
01:15:12,675 --> 01:15:14,677
(beeping, whirring)

1154
01:15:20,350 --> 01:15:23,561
SQUYRES:
Now that Spirit's gone,

1155
01:15:23,645 --> 01:15:26,606
there's this--
what do we do next with Opportunity?

1156
01:15:28,358 --> 01:15:31,486
Do we just kind of noodle around
till the wheels fall off?

1157
01:15:31,569 --> 01:15:35,073
Or do we put our foot on the gas
and just go as fast as we can

1158
01:15:35,156 --> 01:15:38,493
and try to reach that big crater next?

1159
01:15:40,870 --> 01:15:43,122
STROUPE:
Miles and miles away,

1160
01:15:43,206 --> 01:15:46,918
this huge crater called Endeavour.

1161
01:15:47,001 --> 01:15:48,795
It would have the oldest rocks

1162
01:15:48,878 --> 01:15:52,131
that Opportunity would've
been able to look at so far.

1163
01:15:53,383 --> 01:15:55,718
But it was many years away.

1164
01:15:56,719 --> 01:15:59,722
And we might not make it,
but it was where the next good stuff was,

1165
01:15:59,806 --> 01:16:01,557
so we might as well try.

1166
01:16:03,309 --> 01:16:06,271
NARRATOR:
Sol 1,784.

1167
01:16:09,357 --> 01:16:12,277
Opportunity has been trekking
toward Endeavour Crater,

1168
01:16:12,360 --> 01:16:16,781
driving as frequently
and for as long as possible.

1169
01:16:17,865 --> 01:16:20,618
This week, she won
the reverse galactic lottery

1170
01:16:20,702 --> 01:16:22,870
-and was struck by lightning.
-(thunder cracks)

1171
01:16:22,954 --> 01:16:24,372
Sort of.

1172
01:16:24,455 --> 01:16:29,585
She got hit by a cosmic ray
that stalled her for a few days.

1173
01:16:29,669 --> 01:16:34,382
But she is okay and back to driving.

1174
01:16:34,465 --> 01:16:36,759
Sol 2,042.

1175
01:16:41,097 --> 01:16:44,017
Opportunity seems to have
become a meteorite hunter.

1176
01:16:44,100 --> 01:16:47,103
She has discovered
three meteorites so far

1177
01:16:47,186 --> 01:16:49,188
on her journey to Endeavour.

1178
01:16:51,983 --> 01:16:54,360
Sol 2,213.

1179
01:16:55,570 --> 01:16:58,197
-Oppy is in her fourth winter on Mars
-(wind whistling)

1180
01:16:58,281 --> 01:17:00,867
and the coldest yet.

1181
01:17:00,950 --> 01:17:04,996
So, in order to save energy,
the rover is sleeping more

1182
01:17:05,079 --> 01:17:07,373
to keep her electronics warm.

1183
01:17:09,250 --> 01:17:11,919
ELLISON: So we're sprinting
and we're sprinting and we're sprinting.

1184
01:17:12,003 --> 01:17:15,048
Some days getting great distance,
some days not going very far at all,

1185
01:17:15,131 --> 01:17:17,008
but we keep going.

1186
01:17:21,137 --> 01:17:26,267
NARRATOR: Opportunity is only about
two kilometers away from Endeavour Crater.

1187
01:17:26,351 --> 01:17:29,312
She'll make landfall at Spirit Point,

1188
01:17:29,395 --> 01:17:32,940
named in honor of Oppy's silent sister.

1189
01:17:36,944 --> 01:17:40,656
ANNOUNCER:
Welcome to the very first Mars Marathon

1190
01:17:40,740 --> 01:17:42,909
here at the Jet Propulsion Laboratory.

1191
01:17:42,992 --> 01:17:45,578
(cheering, applause)

1192
01:17:45,661 --> 01:17:50,625
The Opportunity rover has achieved
a marathon's distance on Mars

1193
01:17:50,708 --> 01:17:52,585
just a week and a half ago.

1194
01:17:52,668 --> 01:17:54,670
(cheering)

1195
01:17:56,130 --> 01:17:58,758
ANNOUNCER:
Congratulations, everyone!

1196
01:17:58,841 --> 01:18:01,761
BOYKINS:
So, at this point, we have

1197
01:18:01,844 --> 01:18:06,015
well exceeded our warranty
and then the extended warranty

1198
01:18:06,099 --> 01:18:07,517
and then the phone call
on your phone that says,

1199
01:18:07,600 --> 01:18:09,477
"Hey, we'll give you more warranty."
We've run past that, too.

1200
01:18:10,561 --> 01:18:12,563


1201
01:18:13,564 --> 01:18:15,566
(beeping)

1202
01:18:16,943 --> 01:18:20,238
And Oppy started to show signs of age.

1203
01:18:21,989 --> 01:18:25,535
Her gray hair was the dust accumulation

1204
01:18:25,618 --> 01:18:28,663
in the crevices between the cables.

1205
01:18:33,084 --> 01:18:35,420
STROUPE:
One of the shoulder joints

1206
01:18:35,503 --> 01:18:38,506
in Opportunity's arm
started getting arthritis.

1207
01:18:48,057 --> 01:18:51,436
We ended up realizing that
if we keep trying to move it,

1208
01:18:51,519 --> 01:18:54,564
it's gonna quit somewhere
that we don't want it to be.

1209
01:18:58,693 --> 01:19:00,862
VERMA:
So we just kept the arm out

1210
01:19:00,945 --> 01:19:04,157
in front of the rover
for the rest of the mission.

1211
01:19:07,410 --> 01:19:10,538
And with arthritis setting in,

1212
01:19:10,621 --> 01:19:14,375
Opportunity also started to have problems
with the right front wheel.

1213
01:19:18,713 --> 01:19:20,840
So, when you were driving it,
you had to think

1214
01:19:20,923 --> 01:19:24,677
in terms of it always veering off
and how you were gonna correct for that.

1215
01:19:30,766 --> 01:19:33,269
SIEGFRIEDT:
Once she started getting older and older,

1216
01:19:33,352 --> 01:19:37,148
Oppy started losing her memory.

1217
01:19:44,363 --> 01:19:46,365
She would go to sleep.

1218
01:19:46,449 --> 01:19:49,160
(beeping)

1219
01:19:49,243 --> 01:19:52,788
And she would essentially forget
all of the science information

1220
01:19:52,872 --> 01:19:55,791
and all of what she had done
before she'd wake up.

1221
01:19:59,670 --> 01:20:04,217
And around the same time that Opportunity
started losing her memory...

1222
01:20:06,260 --> 01:20:09,722
...my grandmother was diagnosed
with Alzheimer's.

1223
01:20:09,805 --> 01:20:15,645
And to see my own grandmother
become not herself anymore...

1224
01:20:16,729 --> 01:20:19,398
-A?
-Bicycle.

1225
01:20:19,482 --> 01:20:24,695
...it was one of the hardest things
to go through.

1226
01:20:24,779 --> 01:20:26,781


1227
01:20:28,115 --> 01:20:32,203
And so when I started seeing Opportunity
start slipping away, too...

1228
01:20:34,121 --> 01:20:37,166
...we had to figure out a way to operate

1229
01:20:37,250 --> 01:20:40,419
in this new paradigm
of her having amnesia.

1230
01:20:43,756 --> 01:20:46,092
(whirring)

1231
01:20:46,175 --> 01:20:49,095
And we did so successfully

1232
01:20:49,178 --> 01:20:51,764
just by forcing her to stay awake.

1233
01:20:53,599 --> 01:20:55,768
So she could send us earthlings
all the data

1234
01:20:55,851 --> 01:20:58,729
before she went to sleep
and forgot everything she did.

1235
01:21:06,445 --> 01:21:09,615
I think Opportunity helped me to really

1236
01:21:09,699 --> 01:21:13,703
better deal with
my grandmother's situation.

1237
01:21:15,955 --> 01:21:20,376
And to... to understand that part of life.

1238
01:21:25,631 --> 01:21:28,050
But she was still the perfect child.

1239
01:21:29,343 --> 01:21:33,139
And she kept trying her damnedest

1240
01:21:33,222 --> 01:21:35,433
to complete her mission,

1241
01:21:35,516 --> 01:21:39,604
to find neutral water
that can support life on Mars.

1242
01:21:43,065 --> 01:21:45,067


1243
01:21:46,193 --> 01:21:48,696
FRAEMAN:
After several years of travel,

1244
01:21:48,779 --> 01:21:53,659
we finally started to see the rim
of Endeavour Crater rise in the distance.

1245
01:21:53,743 --> 01:21:55,745


1246
01:21:59,206 --> 01:22:02,001
ELLISON: And even though this thing
is ten-plus miles wide...

1247
01:22:03,586 --> 01:22:06,339
...it wasn't till we pulled up
right to the edge, and suddenly...

1248
01:22:09,216 --> 01:22:10,843
...whack!

1249
01:22:17,516 --> 01:22:20,811
FRAEMAN: When Oppy reached the rim
of Endeavour, everything changed.

1250
01:22:23,397 --> 01:22:26,484
It almost felt like
the start of a whole new mission.

1251
01:22:28,861 --> 01:22:31,947
It was a whole new environment to explore,

1252
01:22:32,031 --> 01:22:35,618
stepping back tens or hundreds
of millions of years in time.

1253
01:22:38,913 --> 01:22:40,581
(laughing):
So I loved this part of the mission.

1254
01:22:42,458 --> 01:22:44,627
NARRATOR:
Sol 3,300.

1255
01:22:47,505 --> 01:22:52,968
Opportunity is feverishly working to
complete analysis of the rock Esperance,

1256
01:22:53,052 --> 01:22:57,640
which may hold the clues
to an ancient habitable environment.

1257
01:22:57,723 --> 01:23:00,101
(whirring)

1258
01:23:00,184 --> 01:23:01,686
(beeps)

1259
01:23:09,777 --> 01:23:12,905
This is a clay that has been
intensely altered

1260
01:23:12,988 --> 01:23:15,616
by relatively neutral-pH water,

1261
01:23:15,700 --> 01:23:19,453
representing the most favorable
conditions for biology

1262
01:23:19,537 --> 01:23:21,914
that Opportunity has encountered.

1263
01:23:25,292 --> 01:23:27,461
This was a huge discovery.

1264
01:23:29,088 --> 01:23:30,589
Water.

1265
01:23:30,673 --> 01:23:35,386
Drinkable, neutral water
once existed on the surface of Mars.

1266
01:23:37,054 --> 01:23:39,056


1267
01:23:42,643 --> 01:23:47,898
And not only was there water, but it could
possibly sustain ancient microbial life.

1268
01:23:47,982 --> 01:23:50,359
So that is just revolutionary.

1269
01:23:52,862 --> 01:23:56,657
SQUYRES:
It showed us that the really ancient Mars

1270
01:23:56,741 --> 01:24:00,035
was much more suitable for...

1271
01:24:00,119 --> 01:24:02,663
the origin of life.

1272
01:24:04,874 --> 01:24:07,334
MANNING:
This was the Holy Grail.

1273
01:24:07,418 --> 01:24:09,962
This is the reason we had gone to Mars.

1274
01:24:10,963 --> 01:24:14,383
Oppy discovered Mars was

1275
01:24:14,467 --> 01:24:17,845
a wet world very much like Earth.

1276
01:24:19,180 --> 01:24:20,389
There were oceans.

1277
01:24:20,473 --> 01:24:22,808
Water played a huge role
in its early history.

1278
01:24:22,892 --> 01:24:24,643
It completely altered the planet.

1279
01:24:31,400 --> 01:24:36,322
SIEGFRIEDT: And Opportunity spent years
exploring Endeavour Crater,

1280
01:24:36,405 --> 01:24:40,075
making incredible discoveries
that tell that story of water.

1281
01:24:42,119 --> 01:24:47,625
So we could go back in time to a planet
that might actually have had life.

1282
01:24:49,210 --> 01:24:51,212


1283
01:24:52,296 --> 01:24:56,383
BOYKINS: A lot of people ask why
I think it's important to explore Mars.

1284
01:24:57,718 --> 01:25:00,763
And I think one of the things
that will come out of

1285
01:25:00,846 --> 01:25:05,643
Spirit and Opportunity's legacy
is some of the answers to why.

1286
01:25:07,520 --> 01:25:09,772
Mars had water.

1287
01:25:09,855 --> 01:25:11,524
What happened to that water?

1288
01:25:11,607 --> 01:25:14,401
And can we take the information
and understand

1289
01:25:14,485 --> 01:25:16,487
how that could happen here on Earth?

1290
01:25:18,489 --> 01:25:21,200
And can we understand our part in that?

1291
01:25:21,283 --> 01:25:25,704
Are we doing something
that can accelerate...

1292
01:25:26,789 --> 01:25:29,208
...that change here on Earth?

1293
01:25:30,793 --> 01:25:32,878
Because that's not something
you recover from.

1294
01:25:32,962 --> 01:25:34,964


1295
01:25:41,720 --> 01:25:43,722
(whirring)

1296
01:25:55,150 --> 01:25:57,987
ELLISON:
So we're 14 years into the mission,

1297
01:25:58,070 --> 01:26:00,990
and sol 5,000 only comes along once.

1298
01:26:01,073 --> 01:26:03,075
Like, it was a big landmark.

1299
01:26:04,410 --> 01:26:06,245
We've got an aging rover.

1300
01:26:06,328 --> 01:26:08,163
She's forgetful. She's arthritic.

1301
01:26:08,247 --> 01:26:10,082
Cameras are still working.

1302
01:26:10,165 --> 01:26:11,750
What can we do?

1303
01:26:12,751 --> 01:26:16,005
I jokingly said
a few days before sol 5,000,

1304
01:26:16,088 --> 01:26:17,715
"We need to take a selfie."

1305
01:26:20,676 --> 01:26:24,013
FRAEMAN: So, we'd been seeing Mars
through Oppy's eyes...

1306
01:26:25,431 --> 01:26:27,808
...but we hadn't seen all of Oppy herself.

1307
01:26:28,809 --> 01:26:31,687
Not since she left the planet in 2003.

1308
01:26:31,770 --> 01:26:34,565
We've got a bit of data mining
ahead of us.

1309
01:26:34,648 --> 01:26:37,192
ELLISON:
So, sol 5,000 planning comes along,

1310
01:26:37,276 --> 01:26:41,572
and the science lead pipes up and says,
"So, the engineering team

1311
01:26:41,655 --> 01:26:42,907
"have this request.

1312
01:26:42,990 --> 01:26:44,825
They'd like to take a selfie."

1313
01:26:44,909 --> 01:26:47,912
And you could hear a pin drop,
'cause the entire science team

1314
01:26:47,995 --> 01:26:51,415
is like, "Come again."

1315
01:26:51,498 --> 01:26:56,170
We could be using up
the remaining life of the robotic arm

1316
01:26:56,253 --> 01:27:00,466
on this act of pure robotic vanity.

1317
01:27:01,884 --> 01:27:05,262
STROUPE: We tried to sell
this idea to the science team.

1318
01:27:08,307 --> 01:27:12,603
But it's tricky because
her shoulder was broken.

1319
01:27:12,686 --> 01:27:18,359
So we had to figure out a way to get
all of the different views of the rover

1320
01:27:18,442 --> 01:27:20,653
without moving the shoulder.

1321
01:27:22,696 --> 01:27:25,824
ELLISON: It wasn't great,
but it was the best we could do.

1322
01:27:27,326 --> 01:27:30,245
And I think it was almost
the science team way of saying

1323
01:27:30,329 --> 01:27:32,081
thank you to the engineering team.

1324
01:27:32,164 --> 01:27:33,666
"This one's on us.

1325
01:27:33,749 --> 01:27:35,751
Take the time to take a selfie.
You deserve it."

1326
01:27:35,834 --> 01:27:38,796
Like, "Let's have a look at this robot
you've made dance for us."

1327
01:27:41,966 --> 01:27:43,968
(whirring)

1328
01:27:46,178 --> 01:27:48,931
(beeps, clicks)

1329
01:27:51,892 --> 01:27:55,813
And so the engineers are taking pictures
from 17 different angles...

1330
01:27:55,896 --> 01:27:58,148
(beeps, clicks)

1331
01:27:58,232 --> 01:28:00,150
...based on the little preview picture

1332
01:28:00,234 --> 01:28:02,111
of what they thought
the microscope would be seeing.

1333
01:28:04,655 --> 01:28:07,116
And with Opportunity's slow,
old computer...

1334
01:28:11,078 --> 01:28:13,372
...it takes about a minute

1335
01:28:13,455 --> 01:28:15,582
just to take a picture.

1336
01:28:21,088 --> 01:28:23,090
We're like, "Refresh.
There's nothing here yet.

1337
01:28:23,173 --> 01:28:24,633
"Refresh. There's nothing here yet.

1338
01:28:24,717 --> 01:28:27,302
Refresh."
Bang, all the thumbnails showed up.

1339
01:28:27,386 --> 01:28:30,097
Little, tiny, 64-pixel thumbnails.

1340
01:28:32,933 --> 01:28:35,477
The images were kind of fuzzy
and upside down.

1341
01:28:37,563 --> 01:28:40,024
But then we run through it...

1342
01:28:41,734 --> 01:28:44,194
...and there's a picture of Opportunity.

1343
01:28:45,446 --> 01:28:49,116
Yes, it was little and black and white
and out of focus,

1344
01:28:49,199 --> 01:28:54,371
but for the first time in,
at that point, 14-plus years,

1345
01:28:54,455 --> 01:28:56,498
we saw our rover.

1346
01:28:56,582 --> 01:28:58,584


1347
01:29:12,931 --> 01:29:15,517
(quiet chatter)

1348
01:29:17,352 --> 01:29:20,481
SIEGFRIEDT:
Everybody who worked on Opportunity,

1349
01:29:20,564 --> 01:29:24,193
we'd get these emails
with our Mars weather data for the day.

1350
01:29:25,486 --> 01:29:29,281
So I looked one day,
and it's starting to get really dusty

1351
01:29:29,364 --> 01:29:32,034
and cloudy in the Opportunity site.

1352
01:29:32,117 --> 01:29:34,578
(typing)

1353
01:29:34,661 --> 01:29:40,125
So, this image was taken on sol 5,106,

1354
01:29:40,209 --> 01:29:43,504
and you can see
the sun is a big bright spot.

1355
01:29:44,588 --> 01:29:48,550
But you can see only three sols later
the sun has completely disappeared.

1356
01:29:50,219 --> 01:29:51,553
Yeah.

1357
01:29:52,888 --> 01:29:54,640
This is really scary.

1358
01:29:54,723 --> 01:29:56,725


1359
01:29:58,185 --> 01:30:01,647
BOYKINS:
There's this dust storm coming for Oppy.

1360
01:30:02,898 --> 01:30:05,734
Now, we've survived
other dust storms on Mars.

1361
01:30:05,818 --> 01:30:07,444
Opportunity has survived.

1362
01:30:08,445 --> 01:30:12,407
But a few days into it, I think, uh,
people began to realize

1363
01:30:12,491 --> 01:30:16,286
that it was different
than anything we've ever experienced.

1364
01:30:16,370 --> 01:30:18,372
(wind howling)

1365
01:30:22,793 --> 01:30:27,005
NARRATOR: The dust storm that is affecting
Opportunity has greatly intensified.

1366
01:30:31,718 --> 01:30:36,431
A spacecraft emergency was declared,
anticipating a low-power fault.

1367
01:30:36,515 --> 01:30:38,517
(beeping, buzzing)

1368
01:30:56,785 --> 01:30:59,329
FRAEMAN:
And then she went dark.

1369
01:31:02,666 --> 01:31:04,334
But we all said, "We know what to do.

1370
01:31:04,418 --> 01:31:06,044
"We have our little dust storm playbook,

1371
01:31:06,128 --> 01:31:08,589
"and we're gonna try everything
we possibly can

1372
01:31:08,672 --> 01:31:11,049
to reestablish communication
with Opportunity."

1373
01:31:12,426 --> 01:31:15,512
ELLISON: At this point,
wake-up songs had kind of went away.

1374
01:31:15,596 --> 01:31:20,976
But we then brought the tradition back
in the hope that maybe singing will help.

1375
01:31:21,059 --> 01:31:23,061
Jitterbug... 

1376
01:31:23,145 --> 01:31:25,981
And we would play them every time
we were trying to wake the rover up.

1377
01:31:26,064 --> 01:31:29,067
Wake me up before you go-go 

1378
01:31:29,151 --> 01:31:32,154
Don't leave me hanging on like a yo-yo 

1379
01:31:32,237 --> 01:31:35,073
Wake me up before you go-go 

1380
01:31:35,157 --> 01:31:37,993
I don't wanna miss it
when you hit that high 

1381
01:31:38,076 --> 01:31:39,828
(faintly over speaker):
You put the boom-boom into my heart... 

1382
01:31:39,912 --> 01:31:41,038
(wind howling)

1383
01:31:41,121 --> 01:31:44,458
NARRATOR:
Sol 5,176.

1384
01:31:44,541 --> 01:31:48,921
It has been over 60 sols
since we lost contact with Opportunity.

1385
01:31:50,088 --> 01:31:52,633
It may be weeks before the sky is clear.

1386
01:31:52,716 --> 01:31:55,594
Don't leave me hanging on like a yo-yo 

1387
01:31:55,677 --> 01:31:58,472
Wake me up before you go-go 

1388
01:31:58,555 --> 01:32:01,850
I don't wanna miss it
when you hit that high... 

1389
01:32:01,934 --> 01:32:04,686
NARRATOR:
Sol 5,210.

1390
01:32:04,770 --> 01:32:07,773
After almost 100 sols
without contact,

1391
01:32:07,856 --> 01:32:12,361
the team is waiting with anticipation
to hear from Opportunity.

1392
01:32:12,444 --> 01:32:16,323
I wanna hit that high 

1393
01:32:16,406 --> 01:32:18,200
Yeah, yeah 

1394
01:32:18,283 --> 01:32:20,661
You take the gray skies out of my way 

1395
01:32:20,744 --> 01:32:23,705
You make the sun shine brighter
than Doris Day... 

1396
01:32:27,793 --> 01:32:29,795
NARRATOR:
It has now been more than six months

1397
01:32:29,878 --> 01:32:32,464
since last contact with Opportunity.

1398
01:32:32,547 --> 01:32:35,759
The dust storm is finally over.

1399
01:32:35,842 --> 01:32:37,970
MANNING:
So, I had hopes that she would just...

1400
01:32:38,053 --> 01:32:40,806
(makes whooshing sound) ...wake up
and say, "Oh, we're-we're alive."

1401
01:32:40,889 --> 01:32:43,642
Everything will be all right. 

1402
01:32:43,725 --> 01:32:45,936
(song ends)

1403
01:32:46,019 --> 01:32:47,938
But it just didn't happen.

1404
01:32:49,856 --> 01:32:52,985
SIEGFRIEDT:
She has autonomy on board

1405
01:32:53,068 --> 01:32:55,112
to wake up at certain times,

1406
01:32:55,195 --> 01:32:57,322
and we know when that alarm will go off,

1407
01:32:57,406 --> 01:32:59,908
so we earthlings can try
and communicate with her.

1408
01:32:59,992 --> 01:33:03,954
So, every day at that time,
we would try and try.

1409
01:33:09,167 --> 01:33:12,713
NARRATOR: ...we have begun
commanding more aggressively.

1410
01:33:14,172 --> 01:33:18,719
We are listening every day
for Opportunity to talk to us.

1411
01:33:18,802 --> 01:33:20,595
(static droning)

1412
01:33:20,679 --> 01:33:22,681


1413
01:33:27,728 --> 01:33:32,649
SIEGFRIEDT: So, NASA declared
we were gonna try one last time

1414
01:33:32,733 --> 01:33:35,569
to try and communicate with Opportunity
and wake her up.

1415
01:33:44,911 --> 01:33:48,790
ELLISON: We were just staring out onto
the floor of what's called the Dark Room.

1416
01:33:48,874 --> 01:33:51,376
That's where we...
you know, for a decade and a half,

1417
01:33:51,460 --> 01:33:54,379
all the commands
had been sent to both rovers.

1418
01:33:54,463 --> 01:33:56,465


1419
01:34:02,012 --> 01:34:04,139
It's like, "Just wake up.

1420
01:34:04,222 --> 01:34:06,016
"We'll make it all better.

1421
01:34:08,185 --> 01:34:10,270
And we'll get back to exploring."

1422
01:34:22,449 --> 01:34:24,451
(static droning)

1423
01:34:28,205 --> 01:34:29,873
SIEGFRIEDT:
Seconds go by,

1424
01:34:29,956 --> 01:34:32,959
a minute goes by,
and at that point, we know.

1425
01:34:35,796 --> 01:34:40,592
FRAEMAN:
And I just had the most vivid flashback

1426
01:34:40,675 --> 01:34:42,844
to landing night,

1427
01:34:42,928 --> 01:34:47,391
standing there
as a 16-year-old in that same room

1428
01:34:47,474 --> 01:34:50,394
and just realizing
what I wanted to do with my life.

1429
01:34:50,477 --> 01:34:52,896
-(Fraeman chuckles)
-(cheering)

1430
01:35:01,988 --> 01:35:04,991
But the journey was over.

1431
01:35:07,327 --> 01:35:09,538
And it all just kind of hit me at once.

1432
01:35:16,044 --> 01:35:19,840
SQUYRES: The operations team said, "Hey,
we wanted to give you the opportunity

1433
01:35:19,923 --> 01:35:22,342
to pick the final rover wake-up song."

1434
01:35:24,219 --> 01:35:26,638
I had never picked a rover wake-up song,

1435
01:35:26,721 --> 01:35:29,641
and I really wanted to pick
something that felt right.

1436
01:35:31,101 --> 01:35:34,479
And in the end, the song that I picked was

1437
01:35:34,563 --> 01:35:39,151
about the ending of a relationship.

1438
01:35:39,234 --> 01:35:41,945
And it's... (stifled sobs)

1439
01:35:42,028 --> 01:35:45,240
("I'll Be Seeing You" by Billie Holiday
playing)

1440
01:35:45,323 --> 01:35:49,870
It's this feeling of gratitude
for the relationship that we had.

1441
01:35:52,539 --> 01:35:55,125
(indistinct chatter over phone)

1442
01:35:55,208 --> 01:35:57,377
MER project off the net.

1443
01:35:57,461 --> 01:35:59,963


1444
01:36:00,046 --> 01:36:07,012
I'll be seeing you 

1445
01:36:07,095 --> 01:36:12,893
In all the old familiar places... 

1446
01:36:13,977 --> 01:36:16,938
SQUYRES:
I don't have to tell you guys we get...

1447
01:36:17,022 --> 01:36:19,232
emotionally attached
to these vehicles, right?

1448
01:36:19,316 --> 01:36:24,029
I... You know, you use a word
like "love" advisedly, but... (sighs)

1449
01:36:24,112 --> 01:36:25,363
we love these rovers.

1450
01:36:27,240 --> 01:36:29,576
BOYKINS:
As a parent, I'm proud.

1451
01:36:30,619 --> 01:36:32,496
We rewrote the history books.

1452
01:36:33,663 --> 01:36:35,957
But as a human being, I'm really sad.

1453
01:36:36,041 --> 01:36:37,792
Because she was a friend.

1454
01:36:37,876 --> 01:36:41,880
I'll find you 

1455
01:36:41,963 --> 01:36:44,382
In the morning sun 

1456
01:36:44,466 --> 01:36:48,136
SQUYRES: The whole project was
bound together by that feeling of love.

1457
01:36:48,220 --> 01:36:50,180
And when the night is new... 

1458
01:36:50,263 --> 01:36:52,599
You're loving the rover,

1459
01:36:52,682 --> 01:36:55,101
and you're loving the people
who you built it with.

1460
01:36:56,102 --> 01:36:58,813
You're loving the people
who you operated it with

1461
01:36:58,897 --> 01:37:02,901
and tended it with you so lovingly
for so many years.

1462
01:37:04,069 --> 01:37:06,029
ELLISON:
To each and every one of us,

1463
01:37:06,112 --> 01:37:09,032
it has been the privilege of a lifetime.

1464
01:37:09,115 --> 01:37:11,284
And, uh...

1465
01:37:11,368 --> 01:37:14,329
you-you don't... you don't get
an adventure like that twice.

1466
01:37:14,412 --> 01:37:21,294
I'll be seeing you... 

1467
01:37:21,378 --> 01:37:24,798
NARRATOR:
Sol 5,352.

1468
01:37:24,881 --> 01:37:27,634
15 years into the mission.

1469
01:37:27,717 --> 01:37:29,719
Since the very first day,

1470
01:37:29,803 --> 01:37:33,723
when she rolled herself
into a hole in one at Eagle Crater,

1471
01:37:33,807 --> 01:37:39,020
Opportunity has affectionately
been called the lucky rover.

1472
01:37:39,104 --> 01:37:44,943
And now, after receiving
13,744 command files

1473
01:37:45,026 --> 01:37:48,738
and lasting 5,262 sols

1474
01:37:48,822 --> 01:37:53,034
past her original retirement age
of 90 sols,

1475
01:37:53,118 --> 01:37:58,498
Opportunity's incredible journey
has come to its end.

1476
01:37:58,582 --> 01:38:01,001
Good night, Opportunity.

1477
01:38:01,084 --> 01:38:02,544
Well done.

1478
01:38:02,627 --> 01:38:08,883
I'll be looking at the moon 

1479
01:38:08,967 --> 01:38:13,638
But I'll be 

1480
01:38:13,722 --> 01:38:21,521
Seeing you. 

1481
01:38:24,774 --> 01:38:26,776
(song ends)

1482
01:38:33,408 --> 01:38:36,077
BOYKINS:
This arc of exploration,

1483
01:38:36,161 --> 01:38:39,581
which is anchored
in Spirit and Opportunity...

1484
01:38:41,082 --> 01:38:43,960
...now leads to the next rover.

1485
01:38:44,044 --> 01:38:45,879


1486
01:38:45,962 --> 01:38:49,966
Perseverance will be the granddaughter
of Spirit and Opportunity.

1487
01:38:51,676 --> 01:38:56,890
Her essence is built on the backbone
of those rovers in front of her.

1488
01:38:57,974 --> 01:38:59,976
SIEGFRIEDT:
Milo, you ready to launch the rocket?

1489
01:39:00,060 --> 01:39:02,062
(giggling)

1490
01:39:03,563 --> 01:39:04,981


1491
01:39:06,149 --> 01:39:11,279
I became pregnant with my second child
when Perseverance was getting built.

1492
01:39:14,407 --> 01:39:17,994
And it was almost like
the rover was in a little NICU.

1493
01:39:19,454 --> 01:39:23,541
And we were all looking over her--
our next baby.

1494
01:39:24,793 --> 01:39:28,505
DERROL NAIL: What a beautiful morning
here on the Space Coast.

1495
01:39:28,588 --> 01:39:30,757
-I'm Derrol Nail.
-And I'm Moogega Cooper.

1496
01:39:30,840 --> 01:39:32,842
In the 50 minutes leading up to launch,

1497
01:39:32,926 --> 01:39:35,261
we will show you
how this mission will reach

1498
01:39:35,345 --> 01:39:38,014
and search for ancient microscopic life
on Mars

1499
01:39:38,098 --> 01:39:40,141
and test new technologies
critical to the ultimate goal:

1500
01:39:40,225 --> 01:39:42,143
future human missions to Mars.

1501
01:39:42,227 --> 01:39:43,770
MANNING:
It's part of our tradition.

1502
01:39:45,230 --> 01:39:47,315
Is open up peanuts.

1503
01:39:51,528 --> 01:39:52,946
Do you want some?

1504
01:39:53,029 --> 01:39:55,657
(indistinct chatter)

1505
01:39:58,034 --> 01:40:01,996
TREBI-OLLENNU: Some people think
planetary exploration is very foreign,

1506
01:40:02,080 --> 01:40:07,085
but I always remind them, you know,
when your forefathers walked the planet,

1507
01:40:07,168 --> 01:40:10,839
the first thing they did is
they looked to the heavens.

1508
01:40:10,922 --> 01:40:12,465
And what did they see?

1509
01:40:12,549 --> 01:40:15,301
Constellations and stars,
wonderful things.

1510
01:40:15,385 --> 01:40:17,178
What did they do with that?

1511
01:40:17,262 --> 01:40:20,640
They use the heavens...

1512
01:40:20,724 --> 01:40:23,017
to come up with a calendar,

1513
01:40:23,101 --> 01:40:26,688
to know when to plant
and to know when to harvest.

1514
01:40:26,771 --> 01:40:28,732
MAN (over speaker):
Flight mission, copy. It's go time.

1515
01:40:28,815 --> 01:40:31,526
TREBI-OLLENNU: And they did it
from the confines of Earth.

1516
01:40:31,609 --> 01:40:36,364
So, planetary exploration
has been with us from the beginning,

1517
01:40:36,448 --> 01:40:38,533
and we're using it the same way

1518
01:40:38,616 --> 01:40:40,660
that our forefathers have done
for generations--

1519
01:40:40,744 --> 01:40:43,246
to make life better on Earth.

1520
01:40:43,329 --> 01:40:45,373
MAN:
Two, one, zero.

1521
01:40:45,457 --> 01:40:46,750
(rumbling)

1522
01:40:46,833 --> 01:40:48,668
Release and liftoff.

1523
01:40:52,213 --> 01:40:54,215


1524
01:41:19,282 --> 01:41:21,284


1525
01:41:41,805 --> 01:41:43,640
Milo, look.

1526
01:41:43,723 --> 01:41:45,809
-What is it?
-Rocket.

1527
01:41:45,892 --> 01:41:48,228
-That's right.
-(laughing)

1528
01:41:48,311 --> 01:41:50,939
And the rover.

1529
01:41:51,022 --> 01:41:52,565
SIEGFRIEDT:
The rover. That's right.

1530
01:41:52,649 --> 01:41:54,192
The rover's inside.

1531
01:41:54,275 --> 01:41:56,277


1532
01:42:10,166 --> 01:42:12,168


1533
01:42:42,198 --> 01:42:44,200


1534
01:43:14,230 --> 01:43:16,232


1535
01:43:46,262 --> 01:43:48,264


1536
01:44:18,294 --> 01:44:20,296


1537
01:44:50,326 --> 01:44:52,328
(music fades)

# [晚安机遇号.srt](https://gitee.com/yuandj/siger/issues/I640LK#note_14827882_link)

1
00:00:50,342 --> 00:00:53,471
最初是一片空白

2
00:00:56,223 --> 00:01:01,145
根本就没有这种概念

3
00:01:01,228 --> 00:01:03,856
做一个探索机器人
让它爬过另一个世界的表面

4
00:01:08,611 --> 00:01:12,656
后来 人类渐渐地开始思考

5
00:01:13,657 --> 00:01:17,244
开始行动 开始建造

6
00:01:20,331 --> 00:01:22,708
于是就诞生了那些机器

7
00:01:36,180 --> 00:01:42,144
2003年 双胞胎探索机器人
“机遇号”与“勇气号”被送上了火星

8
00:01:43,896 --> 00:01:48,025
她们原本设计的寿命只有90天

9
00:02:13,884 --> 00:02:17,763
许多人会说她们不过是普通的机器人

10
00:02:19,557 --> 00:02:22,268
但是她们一旦启动

11
00:02:22,351 --> 00:02:26,438
就超越了普通的行星探测机器人

12
00:02:38,075 --> 00:02:39,910
机遇号-避障相机
系统状态良好 气温：零下40度

13
00:02:39,994 --> 00:02:41,203
正在传入…

14
00:02:41,287 --> 00:02:44,248
唤醒曲 正在播放…

15
00:02:44,331 --> 00:02:49,086
美国宇航局：
早上好 机遇号 该起床啦！

16
00:03:20,326 --> 00:03:24,496
一旦漫游车上了火星
就有了自己的生命

17
00:03:25,247 --> 00:03:27,666
能量在她的血管中奔涌

18
00:03:28,918 --> 00:03:31,629
而且她需要爱

19
00:03:35,090 --> 00:03:37,927
所以我们会尽量保证她的安全

20
00:03:40,262 --> 00:03:43,724
但有时候 她也有自己的想法

21
00:03:44,058 --> 00:03:49,980
机遇号：我停了下来 发现障碍

22
00:03:50,981 --> 00:03:55,903
美国宇航局：一切安全 继续前进

23
00:03:56,111 --> 00:04:00,699
那只是你自己的影子

24
00:04:09,249 --> 00:04:11,627
所以说 没错 她不过是个机器人

25
00:04:12,920 --> 00:04:17,049
但是通过这个机器人
我们一起进行了一场不可思议的历险

26
00:04:18,092 --> 00:04:20,552
她也成了我们的家人

27
00:05:00,718 --> 00:05:06,682
晚安机遇号

28
00:05:28,287 --> 00:05:33,584
加州帕萨迪纳 喷气推进实验室

29
00:05:37,755 --> 00:05:41,300
美国宇航局火星计划
任务控制中心

30
00:05:41,717 --> 00:05:43,343
美国宇航局

31
00:05:43,427 --> 00:05:44,553
任务经理 珍妮佛崔瑟波

32
00:05:44,636 --> 00:05:47,056
我认为 当我们望向夜空时

33
00:05:47,973 --> 00:05:50,059
我们所有人都会想知道

34
00:05:51,977 --> 00:05:55,314
人类是否真的
孤零零地存于这宇宙之中

35
00:05:57,524 --> 00:06:02,029
这是人类一个伟大的奥秘
我们一直在努力探索这个奥秘

36
00:06:05,032 --> 00:06:06,408
在数个世纪以来

37
00:06:07,159 --> 00:06:12,831
火星一直是悬挂在空中的
一个神秘的小红点

38
00:06:12,915 --> 00:06:14,958
首席系统工程师 罗博曼宁

39
00:06:15,334 --> 00:06:18,253
它激发了无数人的想象

40
00:06:21,465 --> 00:06:24,176
在那个遥远的星球上正在发生什么？

41
00:06:28,222 --> 00:06:30,849
整个火星计划的整体目标

42
00:06:31,683 --> 00:06:35,729
一直围绕着一个问题展开
“火星上是否曾经有生命体的存在？”

43
00:06:36,355 --> 00:06:38,816
所以尤其是在火星计划的初期

44
00:06:38,899 --> 00:06:39,900
漫游车驾驶员 艾诗莉斯楚普

45
00:06:39,983 --> 00:06:41,652
我们一直在寻找水

46
00:06:42,986 --> 00:06:46,949
因为 至少在地球上 只要有水的地方…

47
00:06:49,243 --> 00:06:50,619
就有生命形式的存在

48
00:06:57,459 --> 00:07:01,713
所以我们关心的是
“火星上是否曾经有水？

49
00:07:02,339 --> 00:07:04,216
“有的话 是哪一种水？

50
00:07:04,758 --> 00:07:07,719
“那是否曾经养育了生命体？”

51
00:07:12,224 --> 00:07:14,977
在1970年代中期开展的两次维京任务

52
00:07:15,060 --> 00:07:18,147
算是当时火星探索的缩影

53
00:07:18,897 --> 00:07:21,984
美国宇航局派出了
两个轨道探测器和两个着陆器

54
00:07:22,568 --> 00:07:25,737
让我们对火星有全新的了解

55
00:07:37,166 --> 00:07:38,876
对 这张不错

56
00:07:46,592 --> 00:07:48,760
首席科学家 史蒂夫斯奎尔斯

57
00:07:48,844 --> 00:07:50,929
感觉太妙了 一堆40年前拍的图片

58
00:07:51,013 --> 00:07:53,724
居然能够引发如此强烈的回忆

59
00:07:55,309 --> 00:07:56,268
但我真的很激动 天哪

60
00:07:57,311 --> 00:07:59,521
我还记得第一次看见它的时候

61
00:08:03,859 --> 00:08:06,361
在开展维京任务时

62
00:08:06,820 --> 00:08:09,198
我是个非常注重实地考察的地质学家

63
00:08:09,281 --> 00:08:12,951
我会到现场去 进行地质学实地考察

64
00:08:14,369 --> 00:08:15,871
这是一门令人着迷的科学

65
00:08:16,914 --> 00:08:19,666
但它让我失望的是

66
00:08:20,292 --> 00:08:23,086
地球上已经没有新的地方等待发现

67
00:08:25,839 --> 00:08:29,927
不过后来 我开始研究
维京任务的轨道探测器发回来的图像

68
00:08:30,719 --> 00:08:34,264
我看着这些照片
就像是俯视着火星一样

69
00:08:36,016 --> 00:08:38,894
而且我对于眼前的星球一无所知

70
00:08:38,977 --> 00:08:41,188
但这正是它的美妙之处
没有一个人了解它

71
00:08:44,233 --> 00:08:47,027
这是看着前人从未看到过的东西

72
00:08:49,071 --> 00:08:51,907
我当下就知道 我一定要做太空探索

73
00:08:53,909 --> 00:08:57,037
那两个维京轨道探测器拍到火星的照片

74
00:08:57,120 --> 00:08:59,248
他们看着火星的照片 心想：“真奇怪

75
00:08:59,831 --> 00:09:02,876
“火星上可能存在水流动的痕迹

76
00:09:04,419 --> 00:09:08,840
“火星以前是一个绿色的世界吗？
它是否有过生物和蓝色的海洋？”

77
00:09:08,924 --> 00:09:10,092
首席系统工程师 罗博曼宁

78
00:09:12,552 --> 00:09:16,932
要是我们能去火星 就一定会去
但我们去不了

79
00:09:18,350 --> 00:09:21,311
根据我作为地质学家所受的训练

80
00:09:21,436 --> 00:09:25,482
我知道如果能把一架漫游车送上火星

81
00:09:25,565 --> 00:09:27,901
它能去火星表面不同的地方

82
00:09:27,985 --> 00:09:30,737
近距离地拍摄岩石

83
00:09:30,821 --> 00:09:34,408
我们也许就能够了解
关于火星历史的真相

84
00:09:38,704 --> 00:09:41,456
因此 从1980年代中期开始

85
00:09:41,540 --> 00:09:44,876
我一共花了十年时间
一直给美国宇航局写提案

86
00:09:44,960 --> 00:09:46,795
但那些提案全都没有中选

87
00:09:49,214 --> 00:09:51,466
我当时有个让人不开心的想法

88
00:09:51,550 --> 00:09:54,594
我也许浪费了我的职业生涯中
整整十年的时间

89
00:09:54,678 --> 00:09:56,179
没有留下任何成就

90
00:09:58,265 --> 00:10:01,560
但是后来
我们在喷气推进实验室组了一支队伍

91
00:10:01,643 --> 00:10:05,397
我们能否把史蒂夫斯奎尔斯想象的
漫游车送上火星

92
00:10:06,231 --> 00:10:10,110
并用上我们已经设计好的登陆系统？

93
00:10:10,777 --> 00:10:12,279
着陆器模型

94
00:10:12,362 --> 00:10:16,992
因此 我们写了一个提案
交给了美国宇航局

95
00:10:18,618 --> 00:10:21,330
我们终于接到了一通电话

96
00:10:22,748 --> 00:10:24,791
让我们的梦想成真了

97
00:10:24,875 --> 00:10:28,670
我非常高兴向大家宣布

98
00:10:28,754 --> 00:10:31,381
我们将回到火星上去
而且这次是强势回归

99
00:10:31,465 --> 00:10:32,466
美国宇航局总部 火星计划负责人
斯科特贺伯特先生

100
00:10:32,549 --> 00:10:35,177
我们有双胞胎 火星漫游车双胞胎

101
00:10:35,260 --> 00:10:36,345
勇气号
机遇号

102
00:10:36,428 --> 00:10:39,181
我们将她们命名为“勇气号”与“机遇号”

103
00:10:40,015 --> 00:10:43,060
撰写了十年提案的努力

104
00:10:43,143 --> 00:10:46,730
在这一刻
终于有了我一直梦寐以求的结果

105
00:10:48,106 --> 00:10:52,361
但我想 如果我在当时就知道

106
00:10:52,444 --> 00:10:56,073
从那个时候起 必须历尽千辛万苦

107
00:10:56,156 --> 00:10:58,367
才能让漫游车登陆火星

108
00:10:58,450 --> 00:11:00,744
我应该不会如此兴奋雀跃

109
00:11:06,416 --> 00:11:09,044
请大家坐下来 我们可以开始了

110
00:11:09,127 --> 00:11:10,128
火星探测漫游车
任务系统关键设计评审

111
00:11:10,212 --> 00:11:12,589
好了 我是该项目的项目工程师

112
00:11:12,672 --> 00:11:15,634
为了确保飞行与任务系统之间的

113
00:11:15,717 --> 00:11:17,844
整体工作的衔接

114
00:11:17,928 --> 00:11:20,430
我将介绍一下发射
巡航和进入、下降与着陆技术…

115
00:11:20,514 --> 00:11:23,600
我们全部的目标是

116
00:11:23,683 --> 00:11:27,562
制造两辆太阳能自主漫游车

117
00:11:28,063 --> 00:11:31,733
能在火星上工作
90个火星太阳日 即三个月

118
00:11:33,360 --> 00:11:36,947
我们很希望
其中至少有一辆能正常运作

119
00:11:38,156 --> 00:11:43,453
但我们知道
如果我们做得不对 就会错过发射日期

120
00:11:44,913 --> 00:11:47,249
火星任务的时间表

121
00:11:47,332 --> 00:11:50,335
真的就是由行星的排列情况决定的

122
00:11:50,419 --> 00:11:52,879
如果你错过了那个发射机会

123
00:11:52,963 --> 00:11:56,508
下次机会大约在26个月后才出现

124
00:11:56,633 --> 00:11:58,468
地球
火星

125
00:11:58,552 --> 00:12:03,181
没有时间设计、开发和测试两辆漫游车

126
00:12:03,265 --> 00:12:05,016
并把她们分别装上两枚火箭

127
00:12:06,476 --> 00:12:09,396
因此我们团队背负着极大的压力

128
00:12:10,021 --> 00:12:12,566
所以我们必须组成一支强大的团队

129
00:12:12,649 --> 00:12:13,483
距离发射还有2年

130
00:12:13,567 --> 00:12:15,819
日以继夜地工作以完成任务

131
00:12:19,448 --> 00:12:21,741
我从小就很迷《星际旅行》

132
00:12:22,909 --> 00:12:24,786
我想成为乔迪拉弗吉

133
00:12:24,870 --> 00:12:26,496
工程部 我是拉弗吉

134
00:12:26,580 --> 00:12:29,207
关闭所有运输工具的电源
我这就上路

135
00:12:29,291 --> 00:12:30,959
但那时的我
并不知道那份工作的性质

136
00:12:31,042 --> 00:12:34,504
我知道他们是“工程师”
但我不知道工程师是干什么的

137
00:12:34,671 --> 00:12:38,508
我只知道我想成为
一个总能把东西修好的人

138
00:12:41,219 --> 00:12:45,599
勇气号和机遇号的建造
真的是从零开始

139
00:12:47,767 --> 00:12:50,729
“好的 我们想完成一个
为期90天的任务

140
00:12:50,812 --> 00:12:53,815
“希望能找到
火星上曾经存在水的证据

141
00:12:53,899 --> 00:12:56,109
“为了达到这个目标
我们需要些什么？”

142
00:12:56,193 --> 00:12:59,154
然后 这个由不同工程师组成的团队

143
00:12:59,237 --> 00:13:02,657
必须把那辆漫游车造出来

144
00:13:06,203 --> 00:13:07,913
这是我的第一个任务

145
00:13:08,997 --> 00:13:11,500
那段经历非常令人兴奋

146
00:13:11,583 --> 00:13:14,419
其中的一些事
是以前从未有人完成过的

147
00:13:14,503 --> 00:13:16,296
我在加纳长大

148
00:13:16,379 --> 00:13:19,299
我小时候对收音机非常着迷

149
00:13:20,175 --> 00:13:24,012
我很好奇
想知道收音机里是不是有人

150
00:13:24,596 --> 00:13:27,933
有一天 我拆开了一台收音机
发现里面没有人 那让我大失所望

151
00:13:28,016 --> 00:13:28,850
机器人工程师
亚西提特雷比-奥伦努

152
00:13:28,934 --> 00:13:31,520
这是我痴迷于工程学的原因

153
00:13:33,021 --> 00:13:34,731
至于漫游车的设计

154
00:13:36,399 --> 00:13:40,779
我们经过深思熟虑之后
决定将漫游车做成与人类相似

155
00:13:44,908 --> 00:13:47,327
地质学家在野外考察时

156
00:13:47,410 --> 00:13:51,540
一般会拿起岩石
把它砸开 查看内部成分

157
00:13:52,374 --> 00:13:55,043
所以火星探测机器人
需要有一个机械臂

158
00:13:55,585 --> 00:13:58,630
集多种仪器于一体 可进行测量

159
00:13:58,713 --> 00:14:02,509
拍摄显微图像 就像瑞士军刀一样

160
00:14:07,973 --> 00:14:10,517
漫游车相机镜头的分辨率

161
00:14:10,600 --> 00:14:13,311
与人类的视力一样

162
00:14:13,395 --> 00:14:15,063
卡西尼
光学和红外绘图光谱仪

163
00:14:15,146 --> 00:14:19,025
所以它们开始看起来很像眼球

164
00:14:21,778 --> 00:14:24,614
另外 漫游车高1点57米

165
00:14:24,698 --> 00:14:27,158
那是人类的平均身高

166
00:14:30,245 --> 00:14:33,707
所以当漫游车
在行驶过程中拍摄图像时

167
00:14:33,790 --> 00:14:36,793
感觉就像是
一个人在火星表面上行走

168
00:14:39,921 --> 00:14:41,840
它不过是一堆电线组成的 对吧？

169
00:14:44,050 --> 00:14:47,971
但是最终做出来的
是一个颇为可爱的机器人

170
00:14:48,054 --> 00:14:48,930
相机操作工程师
道格埃里森

171
00:14:49,014 --> 00:14:50,307
它有着一张脸

172
00:14:52,642 --> 00:14:55,562
我们有着令人惊叹的科学仪器

173
00:14:56,730 --> 00:14:59,691
但是一旦把所有东西都装在漫游车上

174
00:14:59,774 --> 00:15:01,109
它就变得越来越大了

175
00:15:01,192 --> 00:15:03,028
距离发射还有18个月

176
00:15:03,111 --> 00:15:06,656
这对于登陆火星来说是个大问题

177
00:15:06,740 --> 00:15:09,326
我的想法是

178
00:15:09,409 --> 00:15:12,912
用六根小弹力绳连接安全气囊

179
00:15:12,996 --> 00:15:17,751
这里的困难在于
可以有很多不同的方法来连接

180
00:15:17,834 --> 00:15:22,213
我们不仅不知道哪种方法最好
而且真的只有一次机会

181
00:15:22,297 --> 00:15:26,259
我们的着陆系统
使用膨胀的巨大安全气囊

182
00:15:26,968 --> 00:15:29,888
它们会让漫游车在火星表面上弹跳

183
00:15:30,930 --> 00:15:33,099
从一开始就遇到了最大的困难

184
00:15:33,183 --> 00:15:37,646
我们开始计算
勇气号和机遇号可能的重量

185
00:15:37,729 --> 00:15:40,815
这些安全气囊能承受她们的重量吗？

186
00:15:43,151 --> 00:15:44,736
于是我们开始做测试

187
00:15:44,819 --> 00:15:46,738
-是什么…
-这太棒了

188
00:15:46,821 --> 00:15:49,282
-这不是问题
-这是块好石头

189
00:15:49,366 --> 00:15:50,867
-对
-我喜欢这块石头

190
00:15:50,950 --> 00:15:51,910
是的

191
00:15:51,993 --> 00:15:54,037
我们正在尝试安全气囊

192
00:15:54,120 --> 00:15:57,290
看看它们撞上火星上的那种岩石会如何

193
00:15:57,374 --> 00:15:59,084
我们模拟了第一次下降

194
00:16:01,086 --> 00:16:04,047
安全气囊上撕出了巨大的洞

195
00:16:04,130 --> 00:16:08,385
被岩石撕裂了
我们心想：“这可不妙

196
00:16:08,468 --> 00:16:09,719
“一点都不妙”

197
00:16:10,970 --> 00:16:12,972
降落伞的设计又是另一回事

198
00:16:13,056 --> 00:16:14,057
距离发射还有1年

199
00:16:14,140 --> 00:16:16,184
三、二、一

200
00:16:16,267 --> 00:16:19,896
我们用这种巨大的
火箭形状的有效载荷来做测试

201
00:16:19,979 --> 00:16:22,816
从直升机上把它扔下来

202
00:16:22,899 --> 00:16:26,778
第一次测试…降落伞被撕成了碎片

203
00:16:29,572 --> 00:16:30,782
第二次测试…

204
00:16:33,410 --> 00:16:34,744
又被撕碎了

205
00:16:36,413 --> 00:16:39,582
我们就意识到
我们没有一个可用的降落伞

206
00:16:39,666 --> 00:16:42,627
很不幸 刚刚被撕裂的那个降落伞

207
00:16:42,711 --> 00:16:45,839
正是我们计划带到火星上去的那个

208
00:16:45,922 --> 00:16:47,006
毫不客气地说

209
00:16:47,090 --> 00:16:49,384
你们的麻烦非常大

210
00:16:49,467 --> 00:16:51,636
这其中的哪一部分提供燃气？

211
00:16:52,178 --> 00:16:53,596
哪里会牵涉到你们？

212
00:16:53,680 --> 00:16:57,183
这些家伙来找我 带着一份问题清单

213
00:16:57,267 --> 00:16:59,436
我把所有这些问题都加了起来

214
00:16:59,519 --> 00:17:02,939
它们涵盖了
我们认为可能出错的方方面面

215
00:17:03,022 --> 00:17:04,649
我理解你们的担忧

216
00:17:04,733 --> 00:17:09,529
你的内心深处有一个担忧
“这个国家资产价值上十亿美元

217
00:17:09,612 --> 00:17:12,031
“这可能会是一场彻底的灾难”

218
00:17:13,908 --> 00:17:16,911
距离发射还有10个月

219
00:17:20,415 --> 00:17:21,583
好的 我们准备好了

220
00:17:21,666 --> 00:17:22,500
振动测试

221
00:17:22,584 --> 00:17:23,710
开始吧

222
00:17:23,793 --> 00:17:27,464
勇气号测试

223
00:17:33,219 --> 00:17:36,014
我们制造勇气号和机遇号时

224
00:17:36,097 --> 00:17:39,058
有意把她们两个做得完全相同

225
00:17:40,852 --> 00:17:45,023
一开始还算顺利
但情况很快就开始偏离了计划

226
00:17:49,235 --> 00:17:50,820
满级

227
00:17:51,780 --> 00:17:52,989
好 一切正常

228
00:17:53,072 --> 00:17:55,533
在组装和测试的整个过程中

229
00:17:55,617 --> 00:17:59,454
一直都是勇气号先进行测试
而且她都会失败

230
00:17:59,537 --> 00:18:01,998
-后退 一次
-一个套管坏了

231
00:18:02,624 --> 00:18:03,875
一个套管坏了？

232
00:18:03,958 --> 00:18:05,376
看甲板

233
00:18:06,586 --> 00:18:09,088
然后是机遇号进行测试

234
00:18:09,172 --> 00:18:10,256
机遇号测试
勇气号测试

235
00:18:10,340 --> 00:18:11,424
玩得开心吗？好的

236
00:18:11,508 --> 00:18:12,383
机遇号测试

237
00:18:12,467 --> 00:18:14,803
三、二、一

238
00:18:16,179 --> 00:18:17,138
停

239
00:18:18,389 --> 00:18:19,724
谢谢你

240
00:18:19,808 --> 00:18:23,478
在每一次测试中
机遇号都出色完成任务

241
00:18:25,563 --> 00:18:29,108
所以即使是在她们离开地球之前
勇气号已经有很多麻烦

242
00:18:29,192 --> 00:18:31,277
而机遇号一直都非常完美

243
00:18:31,528 --> 00:18:35,114
距离发射还有7个月

244
00:18:35,198 --> 00:18:39,369
漫游车经过了
如此长时间的建造与测试之后

245
00:18:40,870 --> 00:18:43,581
现在该是让机遇号进行地面测试了

246
00:18:47,585 --> 00:18:49,337
这是我们第一次

247
00:18:50,213 --> 00:18:52,757
给漫游车注入了生命

248
00:18:54,175 --> 00:18:55,260
动起来

249
00:19:02,016 --> 00:19:03,059
她的第一步

250
00:19:05,645 --> 00:19:06,813
我变得非常兴奋！

251
00:19:07,522 --> 00:19:10,233
我的内心在欢呼：“它活过来了！”

252
00:19:15,613 --> 00:19:18,616
她几乎就像是有了生命

253
00:19:21,244 --> 00:19:25,665
一个真实的、活生生的机器人
你能想象她上火星去

254
00:19:25,790 --> 00:19:28,751
在那里做一些你做梦都想做的事

255
00:19:30,128 --> 00:19:33,882
要是把这与孩子出生相提并论
似乎有轻视父母情分之嫌

256
00:19:33,965 --> 00:19:35,842
但那种感动类似于自己孩子的出生

257
00:19:39,178 --> 00:19:41,681
同时又不确定

258
00:19:41,764 --> 00:19:45,226
自己的孩子是否真的准备好了
应对这个狂野的世界

259
00:19:47,729 --> 00:19:50,273
我们想做的所有测试是否都做过了？

260
00:19:52,108 --> 00:19:53,318
绝对没有

261
00:19:56,029 --> 00:19:58,448
但是到最后 已经没有时间了

262
00:19:59,908 --> 00:20:01,326
该让她们飞翔了

263
00:20:06,456 --> 00:20:08,583
肯尼迪航天中心 发射当天早上

264
00:20:08,666 --> 00:20:11,127
我们在外面 现在是凌晨5点半

265
00:20:11,210 --> 00:20:14,005
但对于我们而言 有许多个夜晚

266
00:20:14,088 --> 00:20:17,967
我们都不眠不休地长时间工作
这是出成果的时候

267
00:20:18,760 --> 00:20:21,554
感觉很不真实
我不知道梦想是否能成真

268
00:20:21,638 --> 00:20:24,557
我现在非常紧张

269
00:20:33,983 --> 00:20:35,360
绅士牌

270
00:20:35,443 --> 00:20:36,402
幸运花生

271
00:20:38,446 --> 00:20:40,239
先发射勇气号

272
00:20:40,323 --> 00:20:41,658
2003年6月10日 勇气号发射

273
00:20:41,741 --> 00:20:43,826
机遇号则在三个星期后发射

274
00:20:43,910 --> 00:20:44,744
2003年7月7日 机遇号发射

275
00:20:44,827 --> 00:20:48,706
这里是德尔塔发射控制中心
倒计时8分40秒 开始倒数…

276
00:20:48,790 --> 00:20:52,710
勇气号发射时
我在喷气推进实验室的控制室

277
00:20:53,836 --> 00:20:56,422
我有工作要做 那让我很庆幸

278
00:20:57,507 --> 00:20:59,384
因为那样我才能保持专注

279
00:20:59,467 --> 00:21:02,011
不太容易变得情绪化

280
00:21:02,095 --> 00:21:04,889
毕竟必须专注于工作

281
00:21:04,973 --> 00:21:08,685
现在进行勇气号火星车A
航天器的最后检查

282
00:21:08,768 --> 00:21:10,561
我是在俄亥俄州的农场长大的姑娘

283
00:21:11,312 --> 00:21:13,940
从小养羊、养猪、养牛

284
00:21:14,607 --> 00:21:17,485
我父亲曾在陆军工程兵团工作

285
00:21:18,903 --> 00:21:20,863
建造第一批火箭

286
00:21:20,947 --> 00:21:23,574
他会经常讲一些令人惊奇的故事

287
00:21:25,243 --> 00:21:30,415
但是我周边的女生
没有人从事航空工程

288
00:21:32,667 --> 00:21:34,627
所以我无法想象

289
00:21:34,711 --> 00:21:38,381
我将有机会把漫游车送上火星

290
00:21:39,674 --> 00:21:41,926
-火星车2号即将发射
-收到

291
00:21:42,301 --> 00:21:45,847
倒计时10分钟时 会开始觉得心慌

292
00:21:46,806 --> 00:21:52,145
九、八、七、六、五、四

293
00:21:52,228 --> 00:21:54,731
-三、二、一
-三、二、一

294
00:21:54,814 --> 00:21:57,525
启动引擎 起飞

295
00:21:57,608 --> 00:21:59,235
机遇号-勇气号

296
00:21:59,318 --> 00:22:01,779
能听见火箭的声音…

297
00:22:04,699 --> 00:22:07,243
没有爆炸声

298
00:22:07,326 --> 00:22:10,496
没有爆炸声

299
00:22:15,460 --> 00:22:18,546
减载指令已发送 运载火箭正在响应

300
00:22:18,629 --> 00:22:22,216
运载火箭起飞后 恢复得很好

301
00:22:23,426 --> 00:22:25,928
我不知道我是否流下了眼泪 但我想…

302
00:22:26,012 --> 00:22:31,476
这枚火箭承载着我的希望与梦想

303
00:22:31,559 --> 00:22:35,688
这种情感非常…难以描述

304
00:22:37,315 --> 00:22:40,818
我感觉那枚火箭
载着我必生工作的成果

305
00:22:48,034 --> 00:22:49,952
这个孩子是我抚养的

306
00:22:50,036 --> 00:22:51,412
耶！

307
00:22:51,496 --> 00:22:53,498
就是那种感觉

308
00:22:55,208 --> 00:22:57,668
现在到了这个孩子发光发亮的时候了

309
00:23:00,088 --> 00:23:02,673
但是告别令人心碎

310
00:23:04,258 --> 00:23:07,595
我将我16年的生命
都奉献给了这些漫游车

311
00:23:09,764 --> 00:23:14,185
然后把她们放在火箭顶部
把她们射入太空

312
00:23:15,061 --> 00:23:17,271
我再也见不到她们了

313
00:23:22,318 --> 00:23:25,696
机遇号发射时 我跟家人一起在外面

314
00:23:25,780 --> 00:23:28,366
我们在发射台上观看

315
00:23:28,449 --> 00:23:31,077
就在那个发射台上
我父亲曾经完成过发射任务

316
00:23:33,579 --> 00:23:35,373
他已经去世了

317
00:23:37,083 --> 00:23:41,295
他是非常以自己的孩子为荣的父亲

318
00:23:42,755 --> 00:23:46,717
那一刻非常感人
我、我妈妈和我的家人都很感动

319
00:23:46,801 --> 00:23:52,807
因为是他启发和鼓励了我进行太空探索

320
00:24:10,658 --> 00:24:14,912
两辆漫游车
前往火星的旅程均为六个半月

321
00:24:18,457 --> 00:24:21,794
勇气号和机遇号的发射时间
只隔了三个星期

322
00:24:21,878 --> 00:24:24,463
所以从天体的角度来看
她们的距离并不远

323
00:24:25,423 --> 00:24:29,051
我们有一条通往火星的轨迹
我们要确保

324
00:24:29,135 --> 00:24:32,096
火箭正在沿着通往火星的道路前进

325
00:24:32,180 --> 00:24:35,975
地球
火星

326
00:24:36,058 --> 00:24:39,854
那个精确度相当于
在洛杉矶把一个高尔夫球打出去

327
00:24:39,937 --> 00:24:44,150
球打到白金汉宫的门把手上
那就是我们努力要做到的事

328
00:24:44,817 --> 00:24:45,943
机遇号-勇气号

329
00:24:46,027 --> 00:24:49,947
那一段叫“静止期”
六个半月的静谧时光

330
00:24:50,072 --> 00:24:51,157
没有任何事情发生

331
00:24:51,824 --> 00:24:54,702
事实并非完全如此

332
00:24:58,164 --> 00:25:03,961
我们遇上了史上最大的太阳耀斑系列

333
00:25:05,379 --> 00:25:09,842
我们看到大量太阳能量和粒子喷射出来

334
00:25:09,926 --> 00:25:11,719
冲向我们的飞行器

335
00:25:20,019 --> 00:25:22,146
在太阳耀斑爆发期间

336
00:25:22,230 --> 00:25:26,150
太阳会喷射出大量的等离子体

337
00:25:28,486 --> 00:25:32,573
等离子体是带有强电荷的电子云

338
00:25:34,492 --> 00:25:38,204
那些高能量粒子真的能杀死人类

339
00:25:38,829 --> 00:25:41,249
它们直接撞向我们的漫游车

340
00:25:42,500 --> 00:25:45,086
一直进到电脑里去

341
00:25:50,174 --> 00:25:51,759
这对航天器的损害很大

342
00:25:55,554 --> 00:25:58,391
我们安装的软件已经被损坏

343
00:26:01,185 --> 00:26:03,187
因此我们不得不重新启动两辆漫游车

344
00:26:07,316 --> 00:26:10,736
我们让漫游车进入休眠状态

345
00:26:11,654 --> 00:26:13,531
这真的很可怕

346
00:26:15,741 --> 00:26:19,745
我们需要将新版软件上传到运载火箭上

347
00:26:20,538 --> 00:26:23,541
并把任务转移给新版软件
只是在键盘上操作

348
00:26:23,624 --> 00:26:24,834
希望一切能够正常运作

349
00:26:36,887 --> 00:26:37,972
成功了

350
00:26:38,723 --> 00:26:40,099
她们重启了

351
00:26:41,684 --> 00:26:45,604
我们花了几个星期
来清理航天器上的电脑

352
00:26:47,898 --> 00:26:50,609
那时 太阳已经平静了下来

353
00:26:50,693 --> 00:26:54,280
软件已加载
我们也准备好登陆火星了

354
00:27:00,328 --> 00:27:06,167
但在当时
三分之二的火星任务都失败了

355
00:27:08,336 --> 00:27:10,254
火星就是太空飞船的墓地

356
00:27:10,755 --> 00:27:11,797
在我们开展任务的时候

357
00:27:14,300 --> 00:27:15,509
四年前

358
00:27:15,593 --> 00:27:19,472
几年前 美国宇航局启动了两次火星任务

359
00:27:19,555 --> 00:27:22,141
火星极地着陆者号和火星气候探测者号

360
00:27:24,894 --> 00:27:25,978
两个任务都失败了

361
00:27:27,855 --> 00:27:29,899
其中一个在大气中烧了

362
00:27:29,982 --> 00:27:32,193
另一个坠毁在火星表面

363
00:27:33,444 --> 00:27:36,405
火星气候探测者号的失败
是通信错误造成的

364
00:27:36,489 --> 00:27:40,659
我们把向我们提供的数据换算成英制

365
00:27:40,743 --> 00:27:42,995
我们以为那些数据是以公制为单位的

366
00:27:43,079 --> 00:27:45,831
那真是荒谬得令人尴尬

367
00:27:45,915 --> 00:27:48,584
女士们 先生们
有来自外太空的大新闻

368
00:27:48,667 --> 00:27:54,423
据科学家们宣称
现在在美国宇航局没有任何智慧生物

369
00:27:54,507 --> 00:27:55,508
是的

370
00:27:56,842 --> 00:28:00,429
所以 当时所有的目光都集中在我们身上

371
00:28:03,349 --> 00:28:08,062
我们的团队觉得勇气号和机遇号的任务

372
00:28:08,145 --> 00:28:10,773
应该要担负起救赎的使命

373
00:28:13,275 --> 00:28:16,320
作为团队的一员 我们感觉

374
00:28:17,571 --> 00:28:20,032
如果这次登陆失败

375
00:28:20,991 --> 00:28:23,160
那宇航局也许就走到了终点

376
00:28:23,244 --> 00:28:26,038
美国宇航局
加州理工学院喷气推进实验室

377
00:28:26,122 --> 00:28:29,166
大家晚上好
欢迎来到喷气推进实验室

378
00:28:29,250 --> 00:28:33,546
今晚有望成为一个
激动人心且精彩纷呈的夜晚

379
00:28:33,629 --> 00:28:36,799
这是勇气号登陆火星的现场报道

380
00:28:36,882 --> 00:28:38,592
机遇号登陆火星的现场报道

381
00:28:38,676 --> 00:28:39,510
机遇号登陆：2004年1月24日
勇气号登陆：2004年1月3日

382
00:28:39,593 --> 00:28:43,848
今天晚上 导航组说
“所有系统准备就绪”

383
00:28:48,269 --> 00:28:52,106
勇气号和机遇号将降落在火星的两侧

384
00:28:52,189 --> 00:28:53,274
登陆时间相隔三周

385
00:28:55,818 --> 00:28:57,278
大家都感到非常焦虑

386
00:28:58,112 --> 00:29:02,366
我不知道我是什么时候
开始服用降压药的

387
00:29:03,617 --> 00:29:05,953
这个晚上 在飞行甲板非常宜人

388
00:29:06,036 --> 00:29:10,624
我们目前的时速
是18217公里 这个速度

389
00:29:10,708 --> 00:29:13,878
足以在12分钟内横穿美国国境

390
00:29:14,628 --> 00:29:17,256
这一次 建议大家坐下来观看着陆

391
00:29:17,339 --> 00:29:18,799
喷气推进实验室主任
美国宇航局管理员

392
00:29:20,676 --> 00:29:22,803
进入、下降、着陆

393
00:29:23,846 --> 00:29:27,850
大约包含86个事件
一旦出现一个问题

394
00:29:29,977 --> 00:29:31,437
都可能会失去漫游车

395
00:29:33,105 --> 00:29:35,774
这是你能想到的最可怕的事情

396
00:29:35,858 --> 00:29:38,986
因为从漫游车开始传输活动信息

397
00:29:39,069 --> 00:29:41,614
到地球上接收到信息需要十分钟

398
00:29:44,575 --> 00:29:46,243
万一出现了问题 我们完全无能为力

399
00:29:47,828 --> 00:29:49,663
只能希望她们能够撑过去

400
00:29:50,289 --> 00:29:52,500
我们称之为“恐怖六分钟”

401
00:29:53,250 --> 00:29:58,339
就是从飞船进入火星大气层顶部

402
00:29:58,422 --> 00:30:03,719
到它完成所有需要做的自主活动

403
00:30:03,802 --> 00:30:06,055
以安全降落在火星表面所需的时间

404
00:30:06,138 --> 00:30:10,351
进入大气 倒数 三、二、一

405
00:30:13,103 --> 00:30:16,649
在这恐怖六分钟之内不容有失

406
00:30:16,732 --> 00:30:18,984
运载火箭现在处于火星大气层上方

407
00:30:19,068 --> 00:30:20,486
加热温度会飙升至最高

408
00:30:20,569 --> 00:30:21,403
机遇号-勇气号

409
00:30:21,487 --> 00:30:24,657
隔热罩将被加热到1600摄氏度以上

410
00:30:27,201 --> 00:30:28,202
降落伞打开

411
00:30:30,287 --> 00:30:31,455
进一步减速

412
00:30:31,539 --> 00:30:33,707
当前时速为717公里

413
00:30:33,791 --> 00:30:36,126
我们预计运载火箭此时将进入亚音速

414
00:30:38,629 --> 00:30:41,549
有一个叫隔热罩的东西 它超级热

415
00:30:41,632 --> 00:30:43,259
必须把它扔掉

416
00:30:45,678 --> 00:30:47,596
但是现在 困难的部分开始了

417
00:30:48,681 --> 00:30:52,893
着陆器必须沿着一根20米长的绳索滑下去

418
00:30:54,520 --> 00:30:58,065
现在有一个降落伞
一个后壳和一个着陆器

419
00:31:00,484 --> 00:31:01,819
机遇号-勇气号

420
00:31:01,902 --> 00:31:03,237
安全气囊充气膨胀

421
00:31:07,992 --> 00:31:10,953
在离地12米高处 后壳发射反推火箭

422
00:31:11,579 --> 00:31:14,206
将漫游车的下降速度降为零

423
00:31:14,290 --> 00:31:15,958
然后剪断最后一根绳子

424
00:31:25,259 --> 00:31:27,136
目前我们没有看到任何信号

425
00:31:27,219 --> 00:31:30,431
我们看到一个断断续续的信号
表明安全气囊正在弹跳

426
00:31:30,514 --> 00:31:34,101
但是 我们目前没有收到航天器的信号

427
00:31:34,768 --> 00:31:35,978
请待命

428
00:31:42,651 --> 00:31:44,236
勇气号消失了

429
00:31:46,530 --> 00:31:48,365
信号消失了

430
00:31:49,992 --> 00:31:51,160
彻底断了

431
00:31:53,245 --> 00:31:55,497
当时我们怀疑她可能坠毁了

432
00:32:02,588 --> 00:32:03,589
寂静无声

433
00:32:05,341 --> 00:32:07,301
每一个人都在等待信号

434
00:32:08,302 --> 00:32:10,220
每一个人都在等待一点动静

435
00:32:16,018 --> 00:32:18,979
那一刻 我心想
我们所做的一切都白费了

436
00:32:20,481 --> 00:32:22,483
这个项目也许失败了

437
00:32:30,824 --> 00:32:32,701
-你们看到了吗？
-我们看到了什么？

438
00:32:32,785 --> 00:32:34,119
你们看到了吗？

439
00:32:35,913 --> 00:32:37,331
它在那里！

440
00:32:39,875 --> 00:32:43,879
左侧频道收到非常强的信号…

441
00:32:43,962 --> 00:32:45,464
表明…

442
00:32:48,842 --> 00:32:50,886
成功登上火星了 各位

443
00:33:00,896 --> 00:33:04,108
我们大家都跳了起来 不是因为高兴

444
00:33:05,317 --> 00:33:06,819
而是因为松了一口气

445
00:33:09,780 --> 00:33:12,658
两辆漫游车都安全地
降落在了火星表面

446
00:33:23,085 --> 00:33:27,631
2004年1月4日 第1个太阳日

447
00:33:27,715 --> 00:33:31,593
勇气号漫游车日记
2004年1月4日

448
00:33:32,219 --> 00:33:33,429
第1个太阳日

449
00:33:34,012 --> 00:33:36,807
原来呀 勇气号是个戏精

450
00:33:36,890 --> 00:33:39,768
但是经过十分钟
令人焦躁不安的寂静之后

451
00:33:39,852 --> 00:33:42,938
我们有一辆漫游车
安全地降落在了火星表面

452
00:33:47,443 --> 00:33:50,112
女士们 先生们 你们有幸身处

453
00:33:50,195 --> 00:33:52,906
目前地球上最激动人心的房间里

454
00:33:54,116 --> 00:33:57,327
机遇号登陆时 我还是一名高中生

455
00:33:58,162 --> 00:33:58,996
项目科学家副手
阿比盖尔弗雷曼

456
00:33:59,079 --> 00:34:02,249
当时全世界有16名学生被选中
能在任务控制室

457
00:34:03,542 --> 00:34:07,379
跟科学团队一起
见证机遇号发送第一批图像

458
00:34:07,463 --> 00:34:10,048
我就是其中的一名学生

459
00:34:10,132 --> 00:34:13,302
所有导航相机现在都在传输图像
所有导航相机

460
00:34:19,725 --> 00:34:21,351
我们登陆火星了

461
00:34:29,860 --> 00:34:31,779
接收到第一批图片时…

462
00:34:33,197 --> 00:34:35,657
我大大地松了一口气

463
00:34:35,741 --> 00:34:38,744
我的血压水平降了下来

464
00:34:42,664 --> 00:34:44,416
我们所有人都非常激动

465
00:34:54,468 --> 00:34:56,178
我的孩子抵达目的地了

466
00:34:56,804 --> 00:34:57,888
这…

467
00:35:02,142 --> 00:35:03,560
欢迎来到火星

468
00:35:08,774 --> 00:35:12,027
机遇号漫游车日记 第1个太阳日

469
00:35:14,112 --> 00:35:16,907
来自运载火箭的信号又强又稳

470
00:35:18,075 --> 00:35:20,494
机遇号登上火星了

471
00:35:47,437 --> 00:35:50,023
机遇号降落在了子午高原的

472
00:35:50,107 --> 00:35:51,567
一个小小的陨石坑里

473
00:35:53,110 --> 00:35:56,446
那简直就是无比精准的一杆进洞

474
00:35:58,490 --> 00:36:04,204
手握相机、导航相机和避障相机
全都传回非常壮观的图像

475
00:36:08,458 --> 00:36:10,794
我们眼前的这个到底是什么？

476
00:36:14,715 --> 00:36:17,009
我不会尝试做任何科学分析

477
00:36:17,092 --> 00:36:19,553
因为它跟我这辈子见过的东西完全不同

478
00:36:21,096 --> 00:36:22,431
跟我们预料的一样

479
00:36:22,514 --> 00:36:25,642
我的妈呀！抱歉 我就是…

480
00:36:30,564 --> 00:36:31,899
我的心情无以言表

481
00:36:36,069 --> 00:36:39,823
到处都是黑色的沙子

482
00:36:42,117 --> 00:36:47,205
有一些浅色的岩石在远处
从地下面突出来

483
00:36:48,665 --> 00:36:50,751
他们蹦了起来说

484
00:36:50,834 --> 00:36:53,754
“天啊 那是基岩 各位 我看见了基岩”

485
00:36:53,837 --> 00:36:58,175
当然 我不知道那是什么意思
也不知道这为何如此重要

486
00:36:58,258 --> 00:37:01,511
我想那天晚上我完全没有合眼
那太令人兴奋了

487
00:37:01,595 --> 00:37:02,763
机遇号漫游车日记

488
00:37:02,846 --> 00:37:04,890
基岩是地质学的真理之神

489
00:37:06,183 --> 00:37:09,603
它们可以告诉你在很久以前
在这个地方

490
00:37:09,686 --> 00:37:12,272
到底发生了什么

491
00:37:22,574 --> 00:37:25,452
全世界有成百上千人

492
00:37:25,535 --> 00:37:27,245
都参与了这个项目

493
00:37:28,538 --> 00:37:32,918
所有努力都必须完美无暇
方能实现这一刻

494
00:37:48,392 --> 00:37:49,935
勇气号有信号了

495
00:37:50,018 --> 00:37:52,229
机遇号安全降落了

496
00:37:52,312 --> 00:37:55,899
而在子午高原
在我们眼前的是真正的基岩

497
00:37:57,401 --> 00:37:59,820
现在该睡觉了

498
00:37:59,903 --> 00:38:04,157
美国宇航局：晚安 机遇号
美国宇航局：晚安 勇气号

499
00:38:04,241 --> 00:38:05,742
美国宇航局
加州理工学院喷气推进实验室

500
00:38:05,826 --> 00:38:10,247
大家好 今天对于火星漫步车来说
是很重要的一天

501
00:38:10,789 --> 00:38:16,670
她已经准备好按照设计目标
完成地质学机器人的任务

502
00:38:18,213 --> 00:38:22,259
按惯例 即将播放我们的早晨唤醒曲

503
00:38:32,436 --> 00:38:34,479
机遇号：早上好

504
00:38:34,563 --> 00:38:35,397
开始任务

505
00:38:36,773 --> 00:38:39,151
在人类太空飞行史上

506
00:38:39,818 --> 00:38:42,529
一直有一个传统：叫醒机组人员

507
00:38:42,612 --> 00:38:45,615
机组人员唤醒曲 他们会播放音乐

508
00:38:45,699 --> 00:38:48,702
类似于：“快起床了 伙计们 该干活了”

509
00:38:54,791 --> 00:38:58,086
我们所谓的火星太阳日

510
00:38:58,170 --> 00:39:01,339
约比地球太阳日长40分钟

511
00:39:01,423 --> 00:39:05,469
就是说每天的日程安排
都会改变约一个小时

512
00:39:06,344 --> 00:39:08,263
我们都按照火星时间来生活

513
00:39:09,264 --> 00:39:11,349
照这个时间来生活很不容易

514
00:39:11,433 --> 00:39:15,353
因为今天的日常计划会议
将会在中午开始

515
00:39:15,437 --> 00:39:19,524
两个半星期之后
我们将从午夜开始一天的工作

516
00:39:21,026 --> 00:39:23,528
所以我们都很累 有时差反应

517
00:39:24,529 --> 00:39:26,156
我们也需要被叫醒

518
00:39:37,042 --> 00:39:39,294
我们在进行这场持续90个太阳日的赛跑

519
00:39:39,377 --> 00:39:43,256
要在这段时间内
尽可能多地了解火星的情况

520
00:39:45,467 --> 00:39:49,096
我们选择了勇气号的着陆点
古谢夫陨击坑

521
00:39:49,179 --> 00:39:53,350
它看起来似乎有一条
巨大的干涸的河床流入其中

522
00:39:53,892 --> 00:39:56,144
我们去那里 希望找到

523
00:39:56,228 --> 00:40:00,440
过去存在水及可居住性的证据

524
00:40:00,524 --> 00:40:01,775
古瑟夫撞击坑

525
00:40:01,858 --> 00:40:06,154
在古谢夫陨击坑
曾经必定有过一个湖泊

526
00:40:07,447 --> 00:40:08,824
勇气号：火山岩

527
00:40:10,951 --> 00:40:12,494
勇气号：更多的火山岩

528
00:40:12,577 --> 00:40:16,540
但是勇气号找到的只是一个熔岩监狱

529
00:40:21,378 --> 00:40:24,214
勇气号：还有更多的火山岩

530
00:40:24,297 --> 00:40:28,093
这些岩石上根本没有证据表明
它们与水有任何相互作用

531
00:40:35,142 --> 00:40:37,185
勇气号

532
00:40:37,435 --> 00:40:39,229
而在火星的另一边

533
00:40:40,730 --> 00:40:44,818
机遇号的着陆点
与我们以前见过的任何地方都不一样

534
00:40:44,901 --> 00:40:46,319
机遇号

535
00:40:48,738 --> 00:40:51,116
机遇号 第8个太阳日

536
00:40:51,199 --> 00:40:55,579
我们已经获得了
漫游车前方土壤的第一批照片

537
00:40:56,204 --> 00:40:59,666
这是我们在火星上见过的
最奇怪的东西

538
00:41:02,377 --> 00:41:06,298
原来在火星表面的这个地方

539
00:41:06,381 --> 00:41:12,387
遍布无数的小圆形物体

540
00:41:18,810 --> 00:41:21,146
一旦穿过露出地面的岩石

541
00:41:21,229 --> 00:41:22,105
机遇号-显微成像器
系统状态良好 气温：零下15度

542
00:41:22,189 --> 00:41:25,442
会看到这些圆形的小东西
是嵌在岩石里的

543
00:41:25,525 --> 00:41:27,235
就像松饼里的蓝莓一样

544
00:41:30,155 --> 00:41:32,616
微型热辐射光谱仪已开启…

545
00:41:36,036 --> 00:41:39,164
原来这些小蓝莓的成分

546
00:41:39,247 --> 00:41:41,124
是一种叫做赤铁矿的矿物

547
00:41:41,208 --> 00:41:42,584
识别出赤铁矿

548
00:41:42,667 --> 00:41:45,879
这种矿物质经常在水中形成

549
00:41:52,093 --> 00:41:54,721
无论是从矿物学
还是从地球化学来看

550
00:41:55,847 --> 00:42:01,061
我们得出
火星上曾经有水的合理结论

551
00:42:01,144 --> 00:42:03,146
所需要的一切

552
00:42:03,980 --> 00:42:07,567
就在伊格尔陨击坑的坑壁里

553
00:42:09,110 --> 00:42:12,906
但这是一个强酸性的环境

554
00:42:14,741 --> 00:42:17,369
在这样的地方不可能发展出生命体

555
00:42:18,620 --> 00:42:21,581
没错 火星上曾经存在液态水

556
00:42:21,665 --> 00:42:25,085
但那不是饮用水

557
00:42:30,548 --> 00:42:32,801
它基本上就像电池酸液

558
00:42:35,553 --> 00:42:37,555
谁都不敢把脚趾泡进去

559
00:42:37,639 --> 00:42:40,558
否则脚趾会被溶化掉 一根都不留

560
00:42:46,439 --> 00:42:51,444
我们真正想找的是
流动的、酸碱度为中性的地下水

561
00:42:54,197 --> 00:42:57,325
因此 为了找可居住性的证据

562
00:42:59,619 --> 00:43:02,038
得上路 在火星上四处转转

563
00:43:02,122 --> 00:43:04,874
第57个太阳日

564
00:43:04,958 --> 00:43:10,672
但问题是 这些漫游车只有90天的寿命

565
00:43:21,474 --> 00:43:24,853
漫游车驾驶员 凡迪威尔玛

566
00:43:24,936 --> 00:43:29,065
漫游车驾驶员
负责操作火星上的漫游车

567
00:43:32,986 --> 00:43:34,487
这份工作很有趣

568
00:43:34,571 --> 00:43:37,407
但是不能用方向盘来驾驶她

569
00:43:37,490 --> 00:43:38,366
漫游车行进规划员

570
00:43:38,450 --> 00:43:41,911
因为信号从地球传到火星

571
00:43:41,995 --> 00:43:43,955
需要4到20分钟

572
00:43:44,831 --> 00:43:46,374
所以我们会发送命令

573
00:43:46,833 --> 00:43:48,418
然后去睡觉

574
00:43:50,211 --> 00:43:53,214
然后漫游车会执行当天的行进安排

575
00:43:53,298 --> 00:43:55,633
等行进完成后

576
00:43:55,717 --> 00:43:59,512
我们回来 得到相应结果
然后又重新开始规划

577
00:44:01,014 --> 00:44:02,974
我在印度长大

578
00:44:03,058 --> 00:44:05,727
在我大约七岁的时候

579
00:44:05,810 --> 00:44:09,022
有人给了我一本关于太空探索的书

580
00:44:09,105 --> 00:44:10,690
给我留下了深刻的印象

581
00:44:12,734 --> 00:44:15,695
你有没有看到
我们一开始离那块岩石有多近？

582
00:44:15,779 --> 00:44:18,656
在执行火星任务期间
我怀着一对双胞胎

583
00:44:19,282 --> 00:44:23,286
因此 对我来说
那是与双胞胎漫游车连接的另一种方式

584
00:44:24,120 --> 00:44:29,501
这两个生命关系如此紧密、如此相似

585
00:44:29,584 --> 00:44:33,171
然后他们将会过着完全独立的生活

586
00:44:35,840 --> 00:44:38,093
两辆漫游车各有个性

587
00:44:38,176 --> 00:44:42,305
我很难选出我更偏爱哪一个

588
00:44:42,931 --> 00:44:44,057
实在无法选择

589
00:44:44,140 --> 00:44:46,643
她们可是双胞胎呀

590
00:44:54,943 --> 00:44:58,613
勇气号在古谢夫陨击坑
那一侧冷得多

591
00:44:59,531 --> 00:45:03,159
机遇号在赤道
那算是火星上的度假胜地

592
00:45:04,702 --> 00:45:08,206
因此 勇气号面临着更艰巨的任务

593
00:45:12,210 --> 00:45:16,965
勇气号找到了一块岩石
我们给它起的外号是“阿迪朗达克”

594
00:45:17,048 --> 00:45:21,594
勇气号-导航相机
系统状态良好 气温：零下60度

595
00:45:21,678 --> 00:45:22,929
她碰了那块石头

596
00:45:28,726 --> 00:45:30,437
而且她不打电话回家

597
00:45:30,520 --> 00:45:31,396
勇气号（火星车A）多普勒显示图
地球接收时间

598
00:45:31,479 --> 00:45:35,108
工作站 43 火星车-2A 现在连接上了

599
00:45:35,191 --> 00:45:38,194
是的 先生
显示器上没有显示任何东西

600
00:45:38,278 --> 00:45:40,905
你没看到任何信号？

601
00:45:44,868 --> 00:45:46,411
没有信号

602
00:45:46,494 --> 00:45:47,537
收到

603
00:45:51,958 --> 00:45:54,169
我是勇气号的任务经理之一

604
00:45:54,752 --> 00:45:58,173
所以 我连着好几天都没回家

605
00:46:00,717 --> 00:46:04,304
我们在任务支持区指挥勇气号

606
00:46:04,387 --> 00:46:08,766
试图从她那里获得信息
同时都觉得有点闷闷不乐

607
00:46:08,892 --> 00:46:11,769
我们显然在试图重新设定…

608
00:46:12,353 --> 00:46:15,982
马克阿德勒正在挑选当天的唤醒曲

609
00:46:16,065 --> 00:46:17,150
任务经理 马克阿德勒

610
00:46:17,942 --> 00:46:21,154
我心想：“天哪
我们必须播放唤醒曲吗？”

611
00:46:21,237 --> 00:46:23,156
我太担心勇气号了

612
00:46:24,240 --> 00:46:27,869
那个时候 我觉得
唤醒曲已经没有乐趣可言了

613
00:46:30,538 --> 00:46:32,332
所有工作站请注意
这是任务控制中心

614
00:46:32,415 --> 00:46:35,793
我认为今天不适宜打破传统

615
00:46:35,877 --> 00:46:37,253
所以像往常一样播放唤醒曲吧

616
00:47:41,859 --> 00:47:43,820
我想：“这首歌再适合不过了”

617
00:47:45,071 --> 00:47:46,239
阿巴乐队的《SOS》

618
00:47:52,870 --> 00:47:56,666
我们收到了“哔”的一声
但是勇气号的状态非常糟糕

619
00:47:58,251 --> 00:48:02,463
她的内置闪存不知何故损坏了

620
00:48:02,547 --> 00:48:05,425
所以 她在过去的两个月里一直醒着

621
00:48:05,508 --> 00:48:08,177
一遍又一遍地崩溃和重启

622
00:48:08,761 --> 00:48:11,139
她彻夜未眠
就像个十几岁的孩子一样

623
00:48:11,222 --> 00:48:13,933
玩电子游戏玩得停不下来

624
00:48:14,017 --> 00:48:16,477
她一直不停地重启

625
00:48:16,561 --> 00:48:18,938
直到电量几乎耗尽

626
00:48:21,399 --> 00:48:25,278
所以我们就想尝试把她关闭

627
00:48:26,571 --> 00:48:29,616
但我们给她下温和的关机命令

628
00:48:29,699 --> 00:48:31,284
她就是关不了机

629
00:48:32,368 --> 00:48:35,580
我们就开始有点慌乱了 因为现在

630
00:48:35,663 --> 00:48:38,416
我们不得不给勇气号
发一条强硬的关机命令

631
00:48:38,499 --> 00:48:41,044
不管发生什么情况 这条指令

632
00:48:41,127 --> 00:48:42,712
都会关闭漫游车

633
00:48:42,795 --> 00:48:44,339
一 四 二 十进位 阿尔法

634
00:48:44,422 --> 00:48:47,091
这会把她关闭24小时 可恶

635
00:48:54,098 --> 00:48:57,560
我们正准备向全世界宣布
我们失去了勇气号

636
00:48:57,644 --> 00:48:58,519
DC05 DC

637
00:48:58,603 --> 00:49:00,730
但在那时 突然间…

638
00:49:00,813 --> 00:49:03,274
继续吧 电信组 我们确认数据正在传输

639
00:49:03,358 --> 00:49:05,735
机遇号（火星车B）多普勒显示图

640
00:49:05,818 --> 00:49:08,738
经过几个严重失眠的夜晚之后

641
00:49:08,821 --> 00:49:11,157
漫游车现在睡得很安详

642
00:49:13,368 --> 00:49:14,952
勇气号回来了

643
00:49:15,870 --> 00:49:17,955
就像一台运转顺畅的机器

644
00:49:20,750 --> 00:49:22,669
第78个太阳日

645
00:49:22,752 --> 00:49:24,170
这只是个估算

646
00:49:24,253 --> 00:49:27,674
限制漫游车寿命的

647
00:49:27,757 --> 00:49:30,426
是它们太阳能电池板上堆积的灰尘

648
00:49:30,510 --> 00:49:33,763
可以把90个太阳日
视为保修期到期的时间

649
00:49:33,846 --> 00:49:36,599
这是任务的原定时长

650
00:49:36,683 --> 00:49:38,810
我们预期能够持续90个太阳日

651
00:49:38,893 --> 00:49:41,979
至于能够延长多久
就取决于火星的情况

652
00:49:44,941 --> 00:49:47,944
我们担心在火星上
工作了90个太阳日之后

653
00:49:48,778 --> 00:49:53,199
勇气号和机遇号的能量会不足

654
00:49:53,282 --> 00:49:55,993
能量不足则会导致漫游车关闭

655
00:50:24,355 --> 00:50:27,734
然后我们看到破坏力十足的尘暴
我们担心

656
00:50:27,817 --> 00:50:30,778
它们对勇气号和机遇号
会造成怎样的损毁

657
00:50:36,617 --> 00:50:40,663
几周前我们拍了一张照片
它变得很红 积满了灰尘

658
00:50:40,747 --> 00:50:43,458
几乎看不到太阳能电池板

659
00:50:44,250 --> 00:50:46,419
但是发生尘暴之后的那天早上

660
00:50:46,502 --> 00:50:48,963
就像有人来清洁过一样

661
00:50:50,423 --> 00:50:54,093
太阳能电池板就像着陆那天一样干净

662
00:51:00,892 --> 00:51:04,937
原来这些尘暴是这些漫游车最好的朋友

663
00:51:09,650 --> 00:51:12,320
它们实际上是我们的生命支持机器

664
00:51:12,945 --> 00:51:14,947
它们来得正是时候

665
00:51:15,573 --> 00:51:19,994
向我们输送了一点氧气
然后我们就恢复了能量

666
00:51:22,622 --> 00:51:23,539
敬我们一杯

667
00:51:23,623 --> 00:51:26,125
第91个太阳日

668
00:51:28,336 --> 00:51:31,631
我们已经完成了主要任务 90个太阳日

669
00:51:31,714 --> 00:51:32,548
勇气号团队
庆祝第91个太阳日

670
00:51:35,176 --> 00:51:39,680
我们开始想这些漫游车
也许能一直工作下去

671
00:51:39,764 --> 00:51:42,558
因为火星上的尘暴真的帮助了我们

672
00:51:42,642 --> 00:51:46,646
那我们就继续吧 继续上路
全速前进 去看看火星吧

673
00:51:46,729 --> 00:51:51,984
机遇号-勇气号

674
00:51:52,068 --> 00:51:55,488
两辆漫游车都撑过了90个太阳日
我们玩得很开心

675
00:51:57,323 --> 00:51:59,158
我们在做漫游车拉力赛

676
00:51:59,242 --> 00:52:01,452
两辆漫游车在相互竞争

677
00:52:01,536 --> 00:52:04,288
看看谁可以在限定时间内
走过更远的距离

678
00:52:05,122 --> 00:52:08,584
勇气号撑过了99个太阳日 朝着山丘进发

679
00:52:08,668 --> 00:52:10,211
勇气号漫游车日记

680
00:52:10,294 --> 00:52:12,880
勇气号遇到的情况让我们失望

681
00:52:12,964 --> 00:52:16,342
比如她的着陆点与我们想象的地方不同

682
00:52:16,968 --> 00:52:21,180
但是勇气号看了看
远处有一些山丘

683
00:52:21,264 --> 00:52:23,140
它们被命名为“哥伦比亚丘陵”

684
00:52:23,224 --> 00:52:26,310
如果有能证明存在可饮用水的证据

685
00:52:26,394 --> 00:52:28,437
也许能在那些丘陵里找到

686
00:52:30,273 --> 00:52:32,441
而在火星的另一边

687
00:52:33,192 --> 00:52:37,405
我们的幸运漫游车机遇号
所开展的冒险完全不同

688
00:52:38,781 --> 00:52:41,033
机遇漫游车日记

689
00:52:41,117 --> 00:52:45,121
我们真正需要的是
更多、掩埋得更深的基岩

690
00:52:45,663 --> 00:52:50,376
最符合这个条件的
是东部的一个大火山口 叫“坚忍环形山”

691
00:52:51,377 --> 00:52:54,005
火山口的美妙之处在于

692
00:52:54,088 --> 00:52:56,549
它呈现了按时间顺序发生的事件

693
00:52:56,632 --> 00:52:58,759
底部有古老的岩石

694
00:52:58,843 --> 00:53:02,763
越往上堆积的是越年轻的岩石

695
00:53:05,099 --> 00:53:07,310
下面的信息非常有科学价值

696
00:53:08,185 --> 00:53:13,441
但我们从未打算
让漫游车开下如此陡峭的斜坡

697
00:53:17,862 --> 00:53:20,656
像坚忍环形山这样的地方

698
00:53:20,740 --> 00:53:23,034
很容易让来自另一个星球的机器人损坏

699
00:53:23,117 --> 00:53:27,121
我的计划会是
让漫游车开到能去到的最远处

700
00:53:27,455 --> 00:53:29,582
科学家和工程师之间有摩擦

701
00:53:29,665 --> 00:53:32,335
原因在于科学家想做疯狂的事

702
00:53:32,418 --> 00:53:36,464
“我想把漫游车开上这道35度的斜坡
因为那块石头太有意思了”

703
00:53:36,547 --> 00:53:40,217
工程师们则会说
“不行 那不安全 你不能那样做

704
00:53:40,301 --> 00:53:43,054
“你想做的事太疯狂了”

705
00:53:43,137 --> 00:53:47,183
坦率地说 如果漫游车
不能安全地爬上这些岩石

706
00:53:47,266 --> 00:53:49,352
我们就不进这个火山口去

707
00:53:52,855 --> 00:53:58,319
所以我们做了一个大试验台
用一个原尺寸的漫游车模型进行试验

708
00:54:00,237 --> 00:54:01,572
这里有点滑

709
00:54:02,365 --> 00:54:05,534
我们试图模拟几何定律和土壤情况…

710
00:54:07,703 --> 00:54:10,247
好的 第一次上试验台

711
00:54:10,331 --> 00:54:12,375
直接开上去

712
00:54:12,458 --> 00:54:14,126
可它却直接滑了下来

713
00:54:17,088 --> 00:54:19,465
所以我们是一寸寸地往下前进的

714
00:54:19,548 --> 00:54:22,218
应该用公制
我们是一厘米一厘米地滑下去的…

715
00:54:24,929 --> 00:54:27,264
非常小心翼翼地做行进计划

716
00:54:27,348 --> 00:54:30,559
以防止机遇号陷入太大的困难当中

717
00:54:42,947 --> 00:54:46,492
我们第二天早上回来看图像…

718
00:54:47,159 --> 00:54:50,413
能听到大家在房间的不同角落
倒抽了一口气

719
00:54:51,122 --> 00:54:54,291
火山口这一侧的表面

720
00:54:54,375 --> 00:54:57,128
并不像我们希望的那样不会打滑

721
00:54:57,211 --> 00:55:00,172
她显然已经开始沿着山坡
朝着一块巨石…

722
00:55:01,215 --> 00:55:04,176
往下滑去了

723
00:55:04,260 --> 00:55:09,223
机遇号-避障相机
系统状态良好 气温：零下30度

724
00:55:12,518 --> 00:55:16,355
但是 我们给漫游车内设了自主性

725
00:55:18,107 --> 00:55:21,444
我们允许漫游车独立思考

726
00:55:22,278 --> 00:55:25,239
因为漫游车比我们更了解

727
00:55:25,322 --> 00:55:28,117
火星的情况

728
00:55:30,119 --> 00:55:32,955
所以 机遇号下火山口时…

729
00:55:35,249 --> 00:55:37,918
她注意到她往下滑的距离太远了

730
00:55:38,627 --> 00:55:44,592
于是停了下来
离她的太阳能电池板尖端仅几厘米

731
00:55:46,093 --> 00:55:49,096
差这么一点 就会撞到那块大石头…

732
00:55:49,764 --> 00:55:53,267
那就可能会终结机遇号的任务

733
00:55:55,394 --> 00:55:59,273
我们都快被吓出心脏病了
但她的自主性救了我们

734
00:56:00,649 --> 00:56:03,235
我们为我们幸运的漫游车
感到非常自豪

735
00:56:09,575 --> 00:56:10,785
为了跟踪机遇号的冒险

736
00:56:11,535 --> 00:56:15,372
我们打印了一张超大的图片

737
00:56:15,498 --> 00:56:19,001
是从火星轨道上拍摄的
从北到南的一条带

738
00:56:19,627 --> 00:56:22,296
上面有伊格尔陨击坑
即我们降落的地方

739
00:56:22,379 --> 00:56:23,422
坚忍环形山
伊格尔陨击坑

740
00:56:23,506 --> 00:56:25,758
坚忍环形山 就是机遇号所在的地方

741
00:56:25,841 --> 00:56:27,927
我们把它铺在桌子上

742
00:56:29,970 --> 00:56:31,597
在图片的尽头

743
00:56:31,680 --> 00:56:34,433
南面几公里处有一个大火山口

744
00:56:34,517 --> 00:56:36,310
我们把它命名为“维多利亚陨击坑”

745
00:56:40,106 --> 00:56:42,399
维多利亚陨击坑

746
00:56:42,483 --> 00:56:44,110
我知道这听起来很荒谬

747
00:56:44,193 --> 00:56:46,904
他们说：“任务时间本应该是三个月

748
00:56:46,987 --> 00:56:48,739
“有一个陨击坑

749
00:56:48,823 --> 00:56:51,117
“也许需要两年才能抵达那里…”

750
00:56:51,992 --> 00:56:53,244
但我们还是那么做了

751
00:56:53,994 --> 00:56:55,788
首先说最重要的事

752
00:57:10,928 --> 00:57:14,306
第445个太阳日

753
00:57:14,682 --> 00:57:18,227
任务开展1年半

754
00:57:18,310 --> 00:57:22,565
维多利亚陨击坑就在几公里外

755
00:57:30,656 --> 00:57:34,451
但这条路挺好走的
路上没有丘陵或山脉

756
00:57:34,535 --> 00:57:35,536
机遇号-导航相机
系统状态良好 气温：零下15度

757
00:57:35,619 --> 00:57:37,830
只有这些尘埃涟漪

758
00:57:40,583 --> 00:57:43,377
所以我们启用了
我们所说的“盲驾”模式

759
00:57:48,674 --> 00:57:50,384
我们向机遇号发指令

760
00:57:50,467 --> 00:57:53,470
“我们遮住眼睛 相信我 继续前进”

761
00:57:58,893 --> 00:58:00,477
在盲驾模式中

762
00:58:00,561 --> 00:58:04,356
以车轮转数来计算进度

763
00:58:09,528 --> 00:58:12,364
在机遇号行进过程中 车轮转了

764
00:58:14,950 --> 00:58:16,577
但令我们震惊的是…

765
00:58:19,830 --> 00:58:21,832
机遇号根本没有移动

766
00:58:23,751 --> 00:58:24,710
问题：轮子被埋了

767
00:58:24,793 --> 00:58:27,046
它刚起步就被困住了

768
00:58:27,129 --> 00:58:30,633
到了这里之后 它就没有前进多少

769
00:58:30,716 --> 00:58:33,260
但是漫游车认为自己在执行计划

770
00:58:33,344 --> 00:58:35,512
以为自己已经到了下面这里

771
00:58:35,596 --> 00:58:40,893
机遇号在那一整天里都在原地转轮子

772
00:58:40,976 --> 00:58:43,979
给自己挖了一个越来越深的坑

773
00:58:49,735 --> 00:58:51,487
美国宇航局：停止行进

774
00:58:51,779 --> 00:58:55,115
机遇号：好的 已停止

775
00:58:56,116 --> 00:58:57,576
没有任何一本书

776
00:58:57,660 --> 00:59:00,871
教我们如何从沙丘中救出火星漫游车

777
00:59:00,955 --> 00:59:03,707
所以我们在喷气推进实验室
建造了沙丘的模型

778
00:59:03,791 --> 00:59:05,125
往里面塞了一辆漫游车

779
00:59:07,086 --> 00:59:09,004
从工程的角度来看

780
00:59:09,672 --> 00:59:12,883
这很令人兴奋 因为我们喜欢挑战

781
00:59:14,426 --> 00:59:16,136
这几乎就像流沙

782
00:59:16,887 --> 00:59:21,267
我们花了六周时间
研究如何让机遇号从里面走出来

783
00:59:22,893 --> 00:59:26,689
但那道斜坡完全没有摩擦力

784
00:59:27,690 --> 00:59:30,150
这就像是在蛋糕面粉里开车一样

785
00:59:32,486 --> 00:59:37,032
工程师们认为唯一的机会
就是让它朝反方向开 并全力加速

786
00:59:40,661 --> 00:59:43,163
美国宇航局：反向 最大速度

787
00:59:43,372 --> 00:59:47,376
机遇号：好的 车轮正在转动

788
00:59:56,385 --> 00:59:59,054
但在火星上的情况却更糟糕了

789
00:59:59,138 --> 01:00:01,640
看起来机遇号越陷越深了

790
01:00:02,099 --> 01:00:03,517
这可能是致命的

791
01:00:05,769 --> 01:00:07,771
第483个太阳日

792
01:00:08,397 --> 01:00:10,316
动能大幅下降

793
01:00:10,983 --> 01:00:14,611
目前我们是在挣扎求存

794
01:00:38,052 --> 01:00:40,387
机遇号：前进了2米

795
01:00:40,471 --> 01:00:41,513
我出来了！

796
01:00:41,597 --> 01:00:43,057
第484个太阳日

797
01:00:44,350 --> 01:00:47,186
现在又可以做长期行进计划了

798
01:00:55,152 --> 01:00:56,528
我们说：“好吧

799
01:00:56,612 --> 01:00:59,948
“从现在开始
我们的行进要更加保守一点”

800
01:01:00,032 --> 01:01:02,368
所以我们小心翼翼地向南行驶

801
01:01:02,451 --> 01:01:05,537
最终到达了维多利亚陨击坑

802
01:01:06,455 --> 01:01:09,875
任务开展2年半

803
01:01:13,087 --> 01:01:15,672
在聚会上 我们有一群人

804
01:01:15,756 --> 01:01:17,633
围坐在桌子旁 喝着鸡尾酒

805
01:01:19,134 --> 01:01:22,679
有人想到了一个主意 请大家打个赌

806
01:01:22,763 --> 01:01:26,392
我们拿了一张鸡尾酒餐巾纸
所有人都写下了名字

807
01:01:26,475 --> 01:01:28,685
每个人都押20美元赌注

808
01:01:28,769 --> 01:01:29,603
星际建设协会
火星车 2003年

809
01:01:29,686 --> 01:01:30,771
我们说：“好

810
01:01:30,854 --> 01:01:34,900
“到明年有几辆漫游车在运作？
零辆、一辆还是两辆？”

811
01:01:34,983 --> 01:01:36,110
庆祝你们的开始
勇气号-机遇号

812
01:01:36,902 --> 01:01:38,946
我们保留了那张鸡尾酒餐巾纸

813
01:01:39,696 --> 01:01:43,325
每年都押一次注

814
01:01:46,286 --> 01:01:50,874
每一年 这个项目的首席科学家
史蒂夫斯奎尔斯

815
01:01:51,625 --> 01:01:55,045
都押两辆漫游车会在隔年寿终正寝

816
01:01:55,129 --> 01:01:59,591
我的想法是我总有一天会押对

817
01:01:59,675 --> 01:02:04,096
到那时候 我打赌赢钱的欢乐
就能让我忘记悲伤

818
01:02:05,264 --> 01:02:07,349
我押的注恰恰相反

819
01:02:07,433 --> 01:02:09,685
我押两辆漫游车在隔年还能运作

820
01:02:11,311 --> 01:02:15,315
所以实际上
这么多年来 我押得都很准

821
01:02:24,908 --> 01:02:28,745
在这个时候
勇气号这辆勤劳的蓝领漫游车

822
01:02:28,829 --> 01:02:30,998
正在探索哥伦比亚丘陵

823
01:02:33,417 --> 01:02:36,378
但她一直遇到机械问题

824
01:02:40,591 --> 01:02:43,802
后来她的右前轮坏了

825
01:02:52,936 --> 01:02:56,398
有人说：“这很像那种

826
01:02:56,482 --> 01:02:59,943
“轮子被卡住的购物车
往后拉比往前推更容易”

827
01:03:01,403 --> 01:03:03,864
我们灵光一闪
“往后拉 对！我们就倒退”

828
01:03:08,994 --> 01:03:12,956
所以勇气号慢慢地
倒退着穿越了哥伦比亚丘陵

829
01:03:13,040 --> 01:03:16,293
一路上都拖着这个破轮子

830
01:03:17,211 --> 01:03:21,006
这太悲惨了 因为冬天快到了

831
01:03:26,970 --> 01:03:31,975
在火星上的冬天
持续时间是地球的两倍

832
01:03:33,268 --> 01:03:35,062
气温极低

833
01:03:35,812 --> 01:03:40,526
气温太低了
漫游车必须将大部分能量用于保温

834
01:03:40,609 --> 01:03:43,820
让所有硬件保持在一定温度以上

835
01:03:43,904 --> 01:03:45,864
否则它就会发生故障

836
01:03:47,115 --> 01:03:50,536
在勇气号的工作点
我们迫切地需要找到一个办法

837
01:03:50,619 --> 01:03:53,121
让太阳能电池板朝太阳倾斜

838
01:03:54,414 --> 01:03:58,710
但唯一的办法就是让整辆车倾斜

839
01:04:10,305 --> 01:04:12,808
勇气号必须倒退着

840
01:04:12,891 --> 01:04:16,687
爬上这片崎岖多岩的地形

841
01:04:19,273 --> 01:04:22,276
才能撑过整个冬天

842
01:04:30,617 --> 01:04:34,705
除了不同的季节以外 还要应对尘暴

843
01:04:35,497 --> 01:04:36,582
第1220个太阳日

844
01:04:36,665 --> 01:04:39,960
有时候整个星球都会刮起尘暴

845
01:04:40,043 --> 01:04:41,753
任务开展3年半

846
01:04:41,837 --> 01:04:44,464
这一次对机遇号造成了最大的破坏

847
01:04:47,134 --> 01:04:49,928
第1226个太阳日

848
01:04:50,971 --> 01:04:53,557
机遇号一直在苦于保命

849
01:04:55,058 --> 01:04:58,895
火星开始发起猛烈攻击
以前所未有过的强大尘暴

850
01:04:58,979 --> 01:05:01,815
席卷机遇号的工作点

851
01:05:08,614 --> 01:05:12,618
所以我们不得不
调整漫游车上的决策过程

852
01:05:12,701 --> 01:05:15,579
当动能太低时…

853
01:05:17,956 --> 01:05:21,918
机遇号能够自动关闭 保存电量

854
01:05:22,002 --> 01:05:24,963
机遇号：关闭所有系统

855
01:05:30,844 --> 01:05:32,429
没有人想大声说出来

856
01:05:32,512 --> 01:05:35,599
但是我们在内心里认为
这次任务随时可能结束

857
01:05:37,017 --> 01:05:40,395
我们可以看到尘暴
可以从轨道上追踪它

858
01:05:42,856 --> 01:05:46,234
过了好几个星期
尘暴才开始平静了下来

859
01:05:47,944 --> 01:05:52,532
数据下传 原本预测地球接收时间

860
01:05:52,616 --> 01:05:56,203
为20点40分 但我们任务有点紧

861
01:05:57,037 --> 01:06:01,291
今天的早晨唤醒曲收到了很多建议

862
01:06:01,375 --> 01:06:04,961
我们在接收数据的同时
会继续播放这些歌曲

863
01:06:18,266 --> 01:06:22,646
然后我们就只能等待消息
看漫游车能否撑下来

864
01:07:15,907 --> 01:07:20,620
我想没有人料想到
漫游车能在这些灾难中幸存下来

865
01:07:26,334 --> 01:07:31,006
你会觉得
“现在火星已经奈何不了我们了”

866
01:07:31,089 --> 01:07:34,426
我们经历了一切 克服了一切
我们基本上是无敌的

867
01:07:36,845 --> 01:07:38,513
但是任务没有完成

868
01:07:39,973 --> 01:07:42,392
我们仍然希望能找到一个地方

869
01:07:42,476 --> 01:07:44,186
曾经发展出了生命体

870
01:07:44,770 --> 01:07:48,774
曾经存在过酸碱度呈中性的水
人类可以饮用的水

871
01:07:58,825 --> 01:08:00,619
-斯奎尔斯
-很高兴来到贵节目

872
01:08:00,702 --> 01:08:02,537
-非常感谢你上我们的节目
-不客气

873
01:08:02,621 --> 01:08:07,292
这是目前正在火星上的
一辆漫游车的模型

874
01:08:07,375 --> 01:08:09,878
-没错
-这是勇气号还是机遇号？

875
01:08:09,961 --> 01:08:12,172
她们基本上是同卵双胞胎
外表一模一样

876
01:08:12,255 --> 01:08:14,174
那你也分不清你的两个孩子？

877
01:08:14,257 --> 01:08:15,717
你是个不称职的父亲
你是这个意思吧

878
01:08:18,428 --> 01:08:21,014
勇气号与机遇号的使命

879
01:08:21,723 --> 01:08:26,102
在民众心目中非常重要

880
01:08:27,187 --> 01:08:28,063
火星任务
CNN今日新闻 迈尔斯奥布莱恩

881
01:08:28,146 --> 01:08:30,315
今天火星上的交通再次拥挤起来

882
01:08:30,398 --> 01:08:32,651
漫游车勇气号和机遇号还在埋头苦干

883
01:08:32,734 --> 01:08:34,820
到目前为止 她们已经走了14.8公里

884
01:08:34,903 --> 01:08:35,737
登陆火星大获成功

885
01:08:35,821 --> 01:08:37,572
并拍了156000多张图片

886
01:08:37,656 --> 01:08:38,865
揭开漫游车的面纱
开启火星探索新阶段

887
01:08:38,949 --> 01:08:41,701
美国宇航局在太空科学领域成就非凡

888
01:08:41,785 --> 01:08:42,744
科学家开始研究火星图像
登陆激发了生命迹象的搜寻

889
01:08:42,828 --> 01:08:47,332
对一个八岁小孩
解释伽马射线光谱学很难

890
01:08:49,626 --> 01:08:51,419
但是 所有人

891
01:08:52,170 --> 01:08:55,173
都或多或少能理解
机器人地质学家的工作

892
01:08:56,842 --> 01:08:59,261
现在 探索和冒险

893
01:08:59,344 --> 01:09:03,348
可以成为人类非常重要的共享经验

894
01:09:04,266 --> 01:09:05,183
他在做什么？

895
01:09:06,476 --> 01:09:08,979
漫游车成为一种文化现象

896
01:09:09,729 --> 01:09:10,605
喜力

897
01:09:10,689 --> 01:09:14,276
代表着对世界的好奇、兴趣与探索

898
01:09:14,359 --> 01:09:17,195
祝火星车顺利 不管你们现在在哪里

899
01:09:17,279 --> 01:09:21,867
这些漫游车持续的时间越长
未来发现的希望就越大

900
01:09:21,950 --> 01:09:23,535
手肘 有手腕

901
01:09:23,618 --> 01:09:27,914
世界各地的人们
对这些漫游车就有着越来越深的感情

902
01:09:29,583 --> 01:09:32,711
但我认为 在勇气号停顿之前

903
01:09:32,794 --> 01:09:37,549
我们任何人都没有完全意识到
我们对公众的影响

904
01:09:37,632 --> 01:09:41,428
任务经理

905
01:09:42,012 --> 01:09:45,140
我的另一个自我
勇气号 遇到了一个问题

906
01:09:45,473 --> 01:09:48,310
第1900个地球日

907
01:09:48,810 --> 01:09:50,645
她的一个轮子已经坏了

908
01:09:50,729 --> 01:09:52,230
任务开展5年

909
01:09:52,314 --> 01:09:54,691
她已经有点陷入土壤中了

910
01:09:56,192 --> 01:09:58,778
然后 另一个轮子也坏了

911
01:09:58,862 --> 01:10:00,947
而且当时已临近冬天

912
01:10:04,034 --> 01:10:08,830
但我想着 根据我对勇气号的了解
她肯定能找到办法的

913
01:10:12,125 --> 01:10:13,668
这堆岩石…

914
01:10:14,377 --> 01:10:18,006
也许漫游车就靠它们撑着

915
01:10:19,007 --> 01:10:22,761
不管是在火星还是在试验台上
漫游车行进时都会陷入土中

916
01:10:24,846 --> 01:10:29,768
这第一张幻灯片
只是概述了勇气号的动能需求

917
01:10:30,310 --> 01:10:33,063
红色的数字表示

918
01:10:33,146 --> 01:10:36,942
持续时间过长的话
这些能源不足以让漫游车撑下去

919
01:10:38,985 --> 01:10:41,404
所以现在这真的
是一场与时间的赛跑

920
01:10:41,488 --> 01:10:46,117
我们的进展非常缓慢
但我们必须努力战胜冬天

921
01:10:46,993 --> 01:10:51,539
我们开始收到公众的来信和电话

922
01:10:52,916 --> 01:10:54,918
让人真的有一种感觉

923
01:10:55,001 --> 01:10:58,380
那就是我们必须尽一切努力
拯救勇气号

924
01:10:58,922 --> 01:11:02,133
解救勇气号

925
01:11:02,217 --> 01:11:05,220
公众称这场运动为“解救勇气号”

926
01:11:06,930 --> 01:11:08,598
这让我们看到了

927
01:11:08,682 --> 01:11:14,396
人类能够与机器人建立联系与感情

928
01:11:15,814 --> 01:11:18,316
第2196个太阳日

929
01:11:19,192 --> 01:11:22,028
勇气号已经为她的冬眠做好了准备

930
01:11:23,071 --> 01:11:24,656
她盖好了被子

931
01:11:25,281 --> 01:11:28,076
现在我们都凝神屏气
等待着信号出现…

932
01:11:28,576 --> 01:11:30,203
或者信号不会出现了

933
01:11:32,831 --> 01:11:37,419
漫游车开始进入低温状态后
就再也不能交流了

934
01:11:41,840 --> 01:11:44,134
她要么第二天会醒过来

935
01:11:44,718 --> 01:11:46,511
要么长眠不醒

936
01:11:51,474 --> 01:11:53,143
太阳重新升起时

937
01:11:54,811 --> 01:11:57,605
我们用心凝听 试图听到

938
01:11:58,690 --> 01:12:02,694
一句耳语、一丝语调…任何声响

939
01:12:05,196 --> 01:12:06,197
但是我们什么都听不到

940
01:12:11,244 --> 01:12:14,122
那种感觉在许多方面而言

941
01:12:14,205 --> 01:12:17,459
确实像是
亲眼看着一个朋友离世一样

942
01:12:19,335 --> 01:12:23,757
我知道人们觉得这很奇怪
因为我听起来像是在谈论一个人

943
01:12:23,840 --> 01:12:25,800
虽然她不是一个人

944
01:12:25,884 --> 01:12:28,678
但是她依然是我们所有人
生活中一个重要的部分

945
01:12:31,723 --> 01:12:34,392
我们的漫游车勇气号
坚毅而有冒险精神

946
01:12:34,517 --> 01:12:37,312
她的环境对她提出了更高的要求

947
01:12:38,063 --> 01:12:41,024
也许是因为我是勇气号的任务经理

948
01:12:41,107 --> 01:12:43,610
我希望她像我一样 但是…

949
01:12:43,693 --> 01:12:47,238
我感觉…我与勇气号心灵相通

950
01:12:52,577 --> 01:12:55,622
也许她只是辛勤工作了这么久之后

951
01:12:55,705 --> 01:12:57,582
也有点累了

952
01:13:02,545 --> 01:13:07,717
勇气号：任务结束
2004年1月3日-2011年5月25日

953
01:13:14,057 --> 01:13:15,934
躺下更好…

954
01:13:16,518 --> 01:13:22,524
无论如何 如果今天
机遇号能行进一百米…

955
01:13:22,607 --> 01:13:24,317
只需投影大家看到的那条线…

956
01:13:24,400 --> 01:13:28,947
到了这个时候
最初参与设计的人当中只有少数几个

957
01:13:29,030 --> 01:13:30,657
还留在团队里

958
01:13:31,616 --> 01:13:36,204
所以 现在由新一代的工程师
负责机遇号的运作

959
01:13:40,917 --> 01:13:44,921
我从来没有想过
自己能参与机遇号的工作

960
01:13:47,632 --> 01:13:50,093
我上八年级的时候

961
01:13:50,176 --> 01:13:54,347
看到了关于勇气号和机遇号登陆的新闻

962
01:13:56,099 --> 01:13:59,102
我不过是来自德克萨斯州
一个偏僻小镇的女生

963
01:13:59,185 --> 01:14:00,019
飞控主任
贝卡索斯兰西格弗里德

964
01:14:00,979 --> 01:14:03,523
但我知道这就是我想做的

965
01:14:03,606 --> 01:14:06,192
我想贡献一份力量
在其他星球上寻找生命

966
01:14:07,402 --> 01:14:09,320
青史留名！

967
01:14:09,404 --> 01:14:11,239
在我大概17岁的时候

968
01:14:11,322 --> 01:14:14,742
有一场命名比赛 为勇气号和机遇号命名

969
01:14:15,743 --> 01:14:17,412
行星保护工程师
穆格加库珀

970
01:14:17,495 --> 01:14:20,874
我最终提交的两个名字是
罗慕路斯与雷穆斯

971
01:14:22,250 --> 01:14:25,628
他们的父亲是战神玛尔斯

972
01:14:27,005 --> 01:14:29,132
我不知道自己当时是怎么想的

973
01:14:29,674 --> 01:14:34,429
不过我那时候已经深深地迷上了

974
01:14:34,512 --> 01:14:36,097
火星和太空探索

975
01:14:38,308 --> 01:14:42,520
并最终引领我
加入了美国宇航局的火星计划

976
01:14:43,980 --> 01:14:46,107
我刚开始在喷气推进实验室工作时

977
01:14:47,150 --> 01:14:49,819
漫游车机遇号已经比较老旧了

978
01:14:49,903 --> 01:14:53,823
在她漫长的工作生涯中
已经完成了一千次任务

979
01:14:54,949 --> 01:14:58,953
但我正是因为她
而踏足航空航天工程的

980
01:14:59,495 --> 01:15:03,208
我知道我想从机遇号开始我的职业生涯

981
01:15:03,291 --> 01:15:06,377
所有工作站 这里是电信开发实验室

982
01:15:06,461 --> 01:15:09,589
我们约五分钟后开始进行简报

983
01:15:20,225 --> 01:15:22,852
现在勇气号已损毁

984
01:15:23,603 --> 01:15:26,606
我们开始思考
接下来该如何操作机遇号

985
01:15:28,066 --> 01:15:30,860
是让它四处随意闲逛
直到轮子掉下来吗？

986
01:15:31,861 --> 01:15:35,406
还是踩油门 全速前进

987
01:15:35,490 --> 01:15:38,409
尝试到达下一个大火山口？

988
01:15:38,493 --> 01:15:40,912
维多利亚陨击坑

989
01:15:40,995 --> 01:15:46,209
许多公里之外
有一个大的陨击坑 叫“因代沃”

990
01:15:46,960 --> 01:15:49,087
那里应该会有机遇号

991
01:15:49,170 --> 01:15:52,131
到目前为止见到的、最古老的岩石

992
01:15:52,215 --> 01:15:53,800
因代沃陨击坑

993
01:15:53,883 --> 01:15:55,885
但是要到那里得走好几年

994
01:15:56,719 --> 01:15:58,471
而且机遇号也许到不了那里

995
01:15:58,554 --> 01:16:02,392
但是它是下一个有价值的目的地
所以我们不妨试试

996
01:16:03,309 --> 01:16:07,772
第1784个太阳日 任务已经开展5年了

997
01:16:07,855 --> 01:16:09,274
任务开展5年

998
01:16:09,357 --> 01:16:12,568
机遇号一直在向着因代沃陨击坑跋涉

999
01:16:12,652 --> 01:16:16,239
尽可能频繁及长时间地行进

1000
01:16:17,740 --> 01:16:20,952
这个星期 她中了逆银河彩票

1001
01:16:21,035 --> 01:16:24,205
被闪电击中了 差不多吧

1002
01:16:24,289 --> 01:16:28,876
她被宇宙射线击中 停滞了几天

1003
01:16:29,585 --> 01:16:33,589
但她没事 又开始赶路了

1004
01:16:34,507 --> 01:16:39,053
第2042个太阳日 任务已经开展六年了

1005
01:16:39,137 --> 01:16:40,013
任务开展6年

1006
01:16:40,096 --> 01:16:43,308
机遇号仿佛化身为了陨石猎人

1007
01:16:44,100 --> 01:16:48,771
到目前为止 她在前往
因代沃的路上发现了三颗陨石

1008
01:16:48,855 --> 01:16:50,898
机遇号-避障相机
系统状态良好 气温：零下20度

1009
01:16:51,733 --> 01:16:54,319
第2213个太阳日

1010
01:16:55,403 --> 01:17:00,033
这是机遇号在火星上的第四个冬天
也是迄今为止最寒冷的一个

1011
01:17:00,908 --> 01:17:04,912
为了节省能源
机遇号沉睡更长的时间

1012
01:17:05,038 --> 01:17:07,248
让她的电子部件保持温暖

1013
01:17:08,082 --> 01:17:09,334
因代沃陨击坑

1014
01:17:09,417 --> 01:17:11,919
我们正在全力冲刺

1015
01:17:12,003 --> 01:17:15,757
有些日子能有长足进展
有些日子却寸步难移

1016
01:17:15,840 --> 01:17:17,216
但我们一直在前进

1017
01:17:18,176 --> 01:17:19,844
任务已经开展7年了

1018
01:17:19,927 --> 01:17:21,054
任务开展7年

1019
01:17:21,137 --> 01:17:23,765
机遇号离因代沃陨击坑

1020
01:17:23,848 --> 01:17:25,516
仅仅2公里远

1021
01:17:26,309 --> 01:17:29,228
她将抵达勇气号着陆点

1022
01:17:29,312 --> 01:17:32,815
这个命名是为了纪念她沉默的姐妹

1023
01:17:36,944 --> 01:17:37,820
终止

1024
01:17:37,904 --> 01:17:42,909
欢迎来到喷气推进实验室的
第一场火星马拉松比赛

1025
01:17:45,620 --> 01:17:50,625
在一个半星期前 机遇号漫游车
在火星上走过的距离

1026
01:17:50,708 --> 01:17:52,377
达到了马拉松的长度

1027
01:17:56,089 --> 01:17:58,132
恭喜大家！

1028
01:17:58,883 --> 01:18:04,430
到这时候
我们大大超过了我们的保修期

1029
01:18:04,514 --> 01:18:07,517
超过了延长保修期

1030
01:18:07,600 --> 01:18:11,145
也超过了更长的保修期

1031
01:18:16,692 --> 01:18:20,071
机遇号已经开始出现了年久失修的疲态

1032
01:18:22,031 --> 01:18:25,410
在电缆之间的缝隙中堆积的灰尘

1033
01:18:25,493 --> 01:18:28,413
就像是她灰白的头发

1034
01:18:33,126 --> 01:18:35,378
机遇号的一个肩关节

1035
01:18:35,461 --> 01:18:38,256
开始出现关节炎

1036
01:18:47,932 --> 01:18:51,144
我们最终意识到
如果我们继续让它移动

1037
01:18:51,227 --> 01:18:54,355
它会在我们不希望的地方发生故障

1038
01:18:58,860 --> 01:19:02,363
所以在任务剩下的时间里
我们只是让漫游车

1039
01:19:02,447 --> 01:19:04,157
把手臂放在前面

1040
01:19:07,410 --> 01:19:09,495
关节炎慢慢发作

1041
01:19:10,163 --> 01:19:14,333
机遇号的右前轮也开始出现了问题

1042
01:19:18,921 --> 01:19:22,758
驾驶它时 必须考虑它偏转的情况

1043
01:19:22,842 --> 01:19:25,219
以及如何纠正这个问题

1044
01:19:30,850 --> 01:19:33,269
机遇号的损耗越来越严重

1045
01:19:33,352 --> 01:19:36,939
她也在渐渐地失去记忆

1046
01:19:44,614 --> 01:19:46,407
她会进入休眠状态

1047
01:19:49,243 --> 01:19:52,914
在她醒来之前 基本上会忘记

1048
01:19:52,997 --> 01:19:56,167
所有科学信息 以及她之前做过的事

1049
01:19:59,670 --> 01:20:04,050
大约在机遇号开始失去记忆的同时

1050
01:20:06,260 --> 01:20:09,472
我奶奶也被诊断出患有老年痴呆症

1051
01:20:10,389 --> 01:20:15,686
看着我自己的奶奶
变得不再是她自己…

1052
01:20:17,146 --> 01:20:18,356
一辆…

1053
01:20:18,439 --> 01:20:19,357
自行车

1054
01:20:19,440 --> 01:20:24,237
这让我极为心痛

1055
01:20:28,074 --> 01:20:32,286
所以 当机遇号也开始渐渐失去记忆时

1056
01:20:34,205 --> 01:20:38,209
我们必须找到一个办法
在她有健忘症的新情况下

1057
01:20:38,292 --> 01:20:40,503
继续运作

1058
01:20:46,217 --> 01:20:51,138
我们成功地让她继续运作
那就是强迫她保持清醒

1059
01:20:51,222 --> 01:20:52,348
美国宇航局：今天没有午休

1060
01:20:52,431 --> 01:20:53,558
保持清醒发送数据

1061
01:20:53,641 --> 01:20:57,019
让她在入睡并忘记她所做的一切之前

1062
01:20:57,103 --> 01:20:58,896
给我们地球人发送数据

1063
01:20:58,980 --> 01:21:01,941
机遇号：正在传送数据

1064
01:21:06,070 --> 01:21:11,033
但我认为机遇号帮助了我更好地

1065
01:21:11,117 --> 01:21:13,578
接受我奶奶的情况

1066
01:21:15,955 --> 01:21:19,959
更好地理解生活中的那一部分

1067
01:21:25,673 --> 01:21:28,134
但她仍然是个完美的孩子

1068
01:21:29,176 --> 01:21:35,141
她一直在竭尽全力 努力完成使命

1069
01:21:35,266 --> 01:21:39,270
在火星上寻找可以支持生命的中性水

1070
01:21:46,235 --> 01:21:48,779
行走了几年后

1071
01:21:48,863 --> 01:21:53,284
我们终于开始远远地看到了
因代沃陨击坑的边缘

1072
01:21:59,040 --> 01:22:02,043
尽管这个陨击坑的宽度
超过16公里

1073
01:22:03,586 --> 01:22:07,256
直到我们来到了陨击坑的边缘
才突然间看到…

1074
01:22:17,475 --> 01:22:20,936
机遇号抵达因代沃陨击坑的边缘时
一切都变了

1075
01:22:23,439 --> 01:22:26,651
这几乎感觉像是开始一个全新的任务

1076
01:22:28,736 --> 01:22:31,197
等待我们去探索的是一个全新的环境

1077
01:22:31,906 --> 01:22:35,409
是在几千万或几亿年前形成的

1078
01:22:38,913 --> 01:22:41,040
所以我很喜欢这部分任务

1079
01:22:41,582 --> 01:22:42,583
任务开展9年

1080
01:22:42,667 --> 01:22:46,796
第3300个太阳日 任务开展9年了

1081
01:22:47,463 --> 01:22:50,299
机遇号正在热火朝天地

1082
01:22:50,383 --> 01:22:53,052
完成对“埃斯佩兰斯”岩石的分析

1083
01:22:53,135 --> 01:22:57,098
这可能是古代宜居环境的线索

1084
01:23:05,231 --> 01:23:06,982
光谱仪已启动…

1085
01:23:09,777 --> 01:23:14,699
这是一种粘土 其性质
已被中性水极大地改变了

1086
01:23:14,782 --> 01:23:15,616
识别出粘土矿物

1087
01:23:16,283 --> 01:23:19,620
这是机遇号遇到的

1088
01:23:19,704 --> 01:23:21,914
最有利于生物的条件

1089
01:23:25,334 --> 01:23:27,461
这是一个巨大的发现

1090
01:23:27,545 --> 01:23:28,963
机遇号-全景相机
系统状态良好 气温：零下27度

1091
01:23:29,046 --> 01:23:30,506
水

1092
01:23:30,589 --> 01:23:35,094
曾经存在于火星表面的可饮用的中性水

1093
01:23:42,643 --> 01:23:44,353
那里不仅有水

1094
01:23:44,437 --> 01:23:47,898
而且它可能曾经滋养了古代微生物

1095
01:23:48,649 --> 01:23:50,359
这绝对是革命性的发现

1096
01:23:52,820 --> 01:23:56,407
这向我们展示了远古时代的火星

1097
01:23:57,158 --> 01:24:02,663
更适合生命的起源

1098
01:24:04,749 --> 01:24:06,542
这是我们梦寐以求的发现

1099
01:24:07,334 --> 01:24:09,920
这就是我们去火星的原因

1100
01:24:10,838 --> 01:24:16,010
机遇号发现在火星上曾经有水

1101
01:24:16,093 --> 01:24:17,887
跟地球非常相似

1102
01:24:19,180 --> 01:24:20,264
那里曾经有海洋

1103
01:24:20,347 --> 01:24:22,933
水在火星早期的历史中发挥了重要作用

1104
01:24:23,017 --> 01:24:24,727
是水彻底改变了火星

1105
01:24:25,728 --> 01:24:30,733
任务开展10年

1106
01:24:30,816 --> 01:24:32,109
任务开展11年

1107
01:24:32,193 --> 01:24:35,446
机遇号花了数年时间探索因代沃陨击坑

1108
01:24:35,529 --> 01:24:36,530
任务开展12年

1109
01:24:36,614 --> 01:24:40,326
发现了令人难以置信的与水相关的线索

1110
01:24:40,409 --> 01:24:42,369
任务开展13年

1111
01:24:42,453 --> 01:24:47,416
也就是说 我们可能找到一个
火星上曾经真的存在生命的时期

1112
01:24:52,213 --> 01:24:56,175
很多人问我
我为什么认为探索火星很重要

1113
01:24:57,635 --> 01:25:00,846
我认为勇气号与机遇号对历史的部分影响

1114
01:25:00,930 --> 01:25:02,932
将会向这些问题

1115
01:25:03,015 --> 01:25:05,559
提供一些答案

1116
01:25:07,394 --> 01:25:08,604
火星上曾经存在水

1117
01:25:09,772 --> 01:25:11,440
那些水后来遇到了什么事？

1118
01:25:11,524 --> 01:25:13,692
我们能否获取这些信息

1119
01:25:13,776 --> 01:25:16,737
并理解那些事如何能在地球上发生？

1120
01:25:18,531 --> 01:25:20,533
我们能否了解我们在其中的作用？

1121
01:25:21,408 --> 01:25:25,704
我们是否正在做一些事情
让地球加速地

1122
01:25:26,831 --> 01:25:29,250
朝那个局面发展？

1123
01:25:30,584 --> 01:25:33,295
因为一旦到了那个地步
就再也无法恢复了

1124
01:25:54,358 --> 01:25:55,442
任务开展14年

1125
01:25:55,526 --> 01:25:57,778
我们的任务已经开始14年了

1126
01:25:57,862 --> 01:26:01,156
第5000个太阳日只此一次

1127
01:26:01,240 --> 01:26:02,908
这是极为重要的里程碑

1128
01:26:04,535 --> 01:26:08,163
我们的漫游车正在老化
她不仅健忘 还有关节炎

1129
01:26:08,247 --> 01:26:11,125
相机还在工作 我们能做什么？

1130
01:26:12,751 --> 01:26:16,088
我在第5000个太阳日
到来的前几天开玩笑说

1131
01:26:16,547 --> 01:26:18,173
“我们得弄张自拍”

1132
01:26:20,843 --> 01:26:23,637
我们一直通过机遇号的眼睛看火星

1133
01:26:25,431 --> 01:26:27,892
但我们从来没有看到过机遇号的全部

1134
01:26:29,184 --> 01:26:31,729
从2003年她离开地球以来就没见过

1135
01:26:31,812 --> 01:26:34,565
我们接下来要进行数据挖掘…

1136
01:26:34,648 --> 01:26:37,192
第5000个太阳日的规划开始了

1137
01:26:37,276 --> 01:26:40,195
我们科学部的主管说

1138
01:26:40,279 --> 01:26:42,740
“工程团队有个要求

1139
01:26:42,823 --> 01:26:44,658
“他们想拍张机遇号的自拍”

1140
01:26:44,742 --> 01:26:46,869
当时全场静得能听见针掉下来的声音

1141
01:26:46,952 --> 01:26:49,288
整个科学队的成员的第一反应是

1142
01:26:49,705 --> 01:26:50,789
“你说什么？”

1143
01:26:51,957 --> 01:26:55,544
这一纯粹为了
满足虚荣心的机器人的举动

1144
01:26:56,295 --> 01:27:00,007
可能会耗尽机械臂的剩余寿命

1145
01:27:02,051 --> 01:27:04,762
我们试图说服科学团队接受这个想法

1146
01:27:07,932 --> 01:27:11,977
但这很难办到 因为她的肩膀骨折了

1147
01:27:13,312 --> 01:27:18,359
我们必须想办法
在不移动漫游车肩膀的情况下

1148
01:27:18,901 --> 01:27:20,694
从不同的角度拍到她

1149
01:27:22,780 --> 01:27:25,532
这不是很理想
但我们最多只能做到这样了

1150
01:27:26,533 --> 01:27:30,454
我认为这就像是科学团队

1151
01:27:30,537 --> 01:27:33,666
向工程团队致谢 “这回算我们的

1152
01:27:33,749 --> 01:27:36,418
“花点时间拍张自拍吧
这是你们应得的

1153
01:27:36,502 --> 01:27:40,005
“我们来看看你们做的这个
为我们效力的机器人吧”

1154
01:27:51,892 --> 01:27:55,938
工程师们要从17个不同的角度拍照

1155
01:27:58,107 --> 01:28:00,192
那些角度是根据

1156
01:28:00,275 --> 01:28:03,278
他们认为显微镜
将会显示的小预览图来设定的

1157
01:28:04,530 --> 01:28:07,116
机遇号的电脑又慢又旧…

1158
01:28:10,953 --> 01:28:15,416
大约需要一分钟才能拍一张照片

1159
01:28:20,838 --> 01:28:23,132
我们一直在刷新 还没有图像

1160
01:28:23,215 --> 01:28:25,426
刷新 暂时还没有图像 再刷新

1161
01:28:25,509 --> 01:28:27,344
砰！所有的缩略图一下子都出来了

1162
01:28:27,428 --> 01:28:30,097
小小的64像素的缩略图

1163
01:28:32,474 --> 01:28:35,477
但是图像有点模糊 而且上下颠倒

1164
01:28:37,563 --> 01:28:40,357
但是我们浏览了这些图像

1165
01:28:41,775 --> 01:28:43,944
组合成了一张机遇号的照片

1166
01:28:45,487 --> 01:28:49,116
对 图像有点小 是黑白照而且模糊

1167
01:28:49,199 --> 01:28:53,996
但是在那时候
那是14多年来 我们头一回

1168
01:28:55,039 --> 01:28:56,498
看到我们的漫游车

1169
01:29:05,215 --> 01:29:11,180
第5000个太阳日

1170
01:29:12,973 --> 01:29:15,517
查尔斯埃拉奇任务控制中心

1171
01:29:17,227 --> 01:29:19,813
每个参与机遇号项目的人

1172
01:29:19,897 --> 01:29:24,109
都会收到关于当天
火星天气数据的电子邮件

1173
01:29:25,778 --> 01:29:30,032
有一天 我查看邮件
机遇号的工作点当天的天气

1174
01:29:30,115 --> 01:29:32,117
尘土飞扬且多云

1175
01:29:34,953 --> 01:29:40,167
这是在第5106个太阳日拍的

1176
01:29:40,250 --> 01:29:43,504
你可以看到
圆圆的太阳很大、很明亮

1177
01:29:44,213 --> 01:29:48,550
但是仅在三个太阳日之后
太阳完全消失了

1178
01:29:50,385 --> 01:29:51,386
是的

1179
01:29:52,971 --> 01:29:54,640
这真的很吓人

1180
01:29:57,935 --> 01:30:01,396
机遇号即将遭遇一场尘暴

1181
01:30:02,898 --> 01:30:05,818
她在火星上遇到过别的尘暴
都幸存了下来

1182
01:30:05,901 --> 01:30:07,528
她顽强地撑了过来

1183
01:30:08,487 --> 01:30:12,950
但是我想尘暴持续几天后
大家才开始意识到

1184
01:30:13,033 --> 01:30:16,328
这跟我们经历过的别的尘暴不同

1185
01:30:18,205 --> 01:30:20,958
第5111个太阳日

1186
01:30:21,041 --> 01:30:22,543
第5111个太阳日

1187
01:30:22,626 --> 01:30:26,839
机遇号遇到的这场尘暴
变得强劲了很多

1188
01:30:31,635 --> 01:30:36,431
我们宣布了航天器紧急状况
预期会出现低动力故障

1189
01:30:42,229 --> 01:30:44,398
我的电池电量低

1190
01:30:44,481 --> 01:30:46,441
周围越来越黑了

1191
01:30:46,525 --> 01:30:50,779
机遇号-全景相机
系统状态良好 气温：零下28度

1192
01:30:56,994 --> 01:30:59,204
然后她就关闭了

1193
01:31:01,999 --> 01:31:06,086
但我们都说：“我们知道该怎么做
我们有应对尘暴的小手册

1194
01:31:06,170 --> 01:31:08,630
“我们会尽我们所能

1195
01:31:08,714 --> 01:31:11,091
“跟机遇号重新建立联系”

1196
01:31:11,175 --> 01:31:12,634
机遇号最棒

1197
01:31:12,718 --> 01:31:15,512
在这个时候
几乎已经没有播唤醒曲的习惯了

1198
01:31:16,180 --> 01:31:20,893
但当时我们重启了这个传统
希望也许唤醒曲能对她有所帮助

1199
01:31:23,145 --> 01:31:26,940
每次我们想唤醒漫游车时
都会播唤醒曲

1200
01:31:41,205 --> 01:31:43,790
第5176个太阳日

1201
01:31:44,541 --> 01:31:48,754
我们跟机遇号失去联系
已经超过60个太阳日了

1202
01:31:49,796 --> 01:31:52,382
但也许还需要几周尘暴才会平息

1203
01:31:53,842 --> 01:31:57,471
数据控制员

1204
01:31:59,556 --> 01:32:01,892
戈德斯通14号
风速每小时2.47公里

1205
01:32:01,975 --> 01:32:04,519
第5210个太阳日

1206
01:32:04,603 --> 01:32:07,898
跟机遇号失去联系近100个太阳日后

1207
01:32:07,981 --> 01:32:12,361
团队正满怀期待地等待机遇号的消息

1208
01:32:24,581 --> 01:32:26,792
第5292个太阳日

1209
01:32:26,875 --> 01:32:27,876
第5292个太阳日

1210
01:32:27,960 --> 01:32:32,297
自最后一次与机遇号联系后
已经过了六个多月

1211
01:32:32,381 --> 01:32:35,008
尘暴终于消停了

1212
01:32:36,051 --> 01:32:39,179
所以我心怀希望 我想她会醒过来

1213
01:32:39,263 --> 01:32:40,847
然后说 “我们活下来了”

1214
01:32:46,186 --> 01:32:47,604
但这个愿望并没有发生

1215
01:32:50,023 --> 01:32:54,945
她有内设的自主操作命令
让她在特定时间醒过来

1216
01:32:55,028 --> 01:32:57,406
我们知道警报响起的时间

1217
01:32:57,489 --> 01:33:00,158
那样就可以尝试跟她联系

1218
01:33:00,242 --> 01:33:03,954
我们每天在那个时间
都会不断地尝试

1219
01:33:04,037 --> 01:33:04,955
机遇号漫游车日记

1220
01:33:05,038 --> 01:33:09,126
时间不多了 而且秋天快到了

1221
01:33:09,209 --> 01:33:12,212
我们开始更积极地向她发送指令

1222
01:33:14,006 --> 01:33:18,593
我们每天都在听信号
希望机遇号会跟我们说话

1223
01:33:27,811 --> 01:33:31,523
宇航局宣布 我们将最后一次

1224
01:33:31,606 --> 01:33:32,941
太空飞行操作大楼
230

1225
01:33:33,025 --> 01:33:36,486
尝试跟机遇号联系并唤醒她

1226
01:33:36,570 --> 01:33:38,697
2019年2月12日
第5352个太阳日

1227
01:33:44,703 --> 01:33:48,790
我们都开始盯着“暗室”的地板看

1228
01:33:48,874 --> 01:33:51,501
15年来 我们给两辆漫游车的

1229
01:33:51,585 --> 01:33:54,379
所有指令 都是从那里发出的

1230
01:34:01,887 --> 01:34:05,682
我们都在想：“快醒过来吧
我们会让一切都变得更好的

1231
01:34:08,018 --> 01:34:10,228
“我们继续探索”

1232
01:34:28,580 --> 01:34:32,959
时间一分一秒地过去了
到了那个时候 我们都知道…

1233
01:34:36,213 --> 01:34:40,592
我回想起了登陆火星之夜

1234
01:34:40,675 --> 01:34:42,177
非常栩栩如生

1235
01:34:42,928 --> 01:34:47,224
当时我是个16岁的孩子
就站在同一个房间里

1236
01:34:47,307 --> 01:34:50,394
在那里找到了我的人生目标

1237
01:35:01,988 --> 01:35:04,991
但旅程结束了…

1238
01:35:07,369 --> 01:35:09,663
所有的回忆一下子涌上了心头

1239
01:35:15,919 --> 01:35:17,421
操作团队说

1240
01:35:17,504 --> 01:35:20,006
“我们想把挑选最后一首

1241
01:35:20,090 --> 01:35:22,509
“漫游车唤醒曲的机会交给你们”

1242
01:35:24,177 --> 01:35:26,596
我从来没有挑过一首漫游车唤醒曲

1243
01:35:26,680 --> 01:35:29,808
在那一刻 我真的很想
挑一首应景的歌

1244
01:35:30,934 --> 01:35:34,479
最后我选的那首歌

1245
01:35:34,563 --> 01:35:38,483
描述了一段关系的结束

1246
01:35:39,234 --> 01:35:40,444
它表达了…

1247
01:35:45,490 --> 01:35:49,870
表达了我们对曾经拥有的
那段感情 心怀感激

1248
01:35:55,208 --> 01:35:57,210
火星车项目结束

1249
01:36:13,602 --> 01:36:15,187
我们对这些漫游车

1250
01:36:15,270 --> 01:36:19,483
有很深的感情
这是不用说的 对吧？

1251
01:36:19,566 --> 01:36:22,819
我们要慎重地用“爱”这个字眼

1252
01:36:22,903 --> 01:36:25,405
但是我们真的爱这些漫游车

1253
01:36:27,365 --> 01:36:29,493
作为她们的父亲 我感到很骄傲

1254
01:36:30,619 --> 01:36:32,621
我们重写了历史

1255
01:36:33,705 --> 01:36:35,957
但作为一个人 我真的感到很难过

1256
01:36:36,041 --> 01:36:37,834
因为她是我的朋友

1257
01:36:44,382 --> 01:36:48,386
这整个项目有一种爱贯穿其中

1258
01:36:50,138 --> 01:36:51,556
我们爱漫游车

1259
01:36:52,682 --> 01:36:56,019
也爱与我们一起建造漫游车的人

1260
01:36:56,102 --> 01:36:58,813
我们爱这么多年来
带着深切的爱意

1261
01:36:58,897 --> 01:37:02,567
跟我们一起操作及照料漫游车的人

1262
01:37:04,361 --> 01:37:06,029
对我们每一个人而言

1263
01:37:06,112 --> 01:37:10,242
那是此生难得的荣幸

1264
01:37:11,451 --> 01:37:14,371
这种冒险这一生只有这一回

1265
01:37:21,586 --> 01:37:26,800
第5352个太阳日 任务开展已经15年了

1266
01:37:28,009 --> 01:37:29,594
从第一天开始

1267
01:37:29,678 --> 01:37:33,598
她一杆进洞 开进伊格尔陨击坑时

1268
01:37:33,682 --> 01:37:38,270
机遇号就被亲切地
称为“幸运的漫游车”

1269
01:37:39,187 --> 01:37:44,818
现在 在接收13744个命令文件

1270
01:37:44,901 --> 01:37:48,655
并超出原定的90个太阳日退休计划

1271
01:37:48,738 --> 01:37:52,909
坚持工作了5262个太阳日之后

1272
01:37:52,993 --> 01:37:57,747
机遇号令人难以置信的旅程画上了句号

1273
01:37:58,498 --> 01:38:02,127
晚安 机遇号 非常棒

1274
01:38:31,364 --> 01:38:33,283
火星计划倒计时

1275
01:38:33,366 --> 01:38:36,119
这条探索的弧线

1276
01:38:36,202 --> 01:38:39,581
以勇气号和机遇号为锚点

1277
01:38:40,999 --> 01:38:43,960
现在它通向了下一辆漫游车

1278
01:38:45,879 --> 01:38:50,091
毅力号是勇气号和机遇号的孙女

1279
01:38:51,676 --> 01:38:56,931
她的精髓建立在
她的前辈漫游车成功的基础之上

1280
01:38:57,891 --> 01:39:00,727
-米洛 你准备好发射火箭了吗？
-好了

1281
01:39:05,982 --> 01:39:11,404
造毅力号的时候
我怀着我的第二个孩子

1282
01:39:14,324 --> 01:39:17,994
就好像漫游车
躺在新生儿重症监护室里

1283
01:39:19,162 --> 01:39:23,541
我们都在看着她
她是我们的下一个孩子

1284
01:39:24,793 --> 01:39:28,505
这是在太空海岸的一个美好的早晨

1285
01:39:28,588 --> 01:39:30,632
-我是达里尔尼尔
-我是穆格加库珀！

1286
01:39:30,715 --> 01:39:31,549
2020年7月 毅力号发射

1287
01:39:31,633 --> 01:39:34,803
在发射前的50分钟里
我们将向大家展示这次任务

1288
01:39:34,886 --> 01:39:38,014
将如何抵达火星
在火星上寻找古代微生物

1289
01:39:38,098 --> 01:39:42,018
并为未来的人类火星任务测试新技术

1290
01:39:42,102 --> 01:39:47,023
我们的传统是开一包花生

1291
01:39:51,945 --> 01:39:52,946
要吃一点吗？

1292
01:39:58,076 --> 01:40:01,996
有些人认为行星探索非常陌生

1293
01:40:02,080 --> 01:40:07,127
但我总是提醒他们
当他们的祖先在地球上行走时

1294
01:40:07,794 --> 01:40:11,131
他们做的第一件事就是仰望天空

1295
01:40:11,214 --> 01:40:12,465
他们看到了什么？

1296
01:40:12,549 --> 01:40:15,260
星座和星星 这些奇妙的东西

1297
01:40:15,343 --> 01:40:17,178
他们观星的结果是什么？

1298
01:40:17,262 --> 01:40:22,058
他们根据对天空的观察
创造了一个日历

1299
01:40:23,226 --> 01:40:26,229
来知道什么时候该播种或收割

1300
01:40:26,813 --> 01:40:28,898
飞控小组 任务中心收到了 发射时间到

1301
01:40:29,023 --> 01:40:31,443
这是他们身处地球而做到的成就

1302
01:40:31,526 --> 01:40:36,406
所以行星探索
从一开始就伴随着我们

1303
01:40:36,489 --> 01:40:40,660
我们探索行星的目的
跟我们世代祖先的目的是一样的

1304
01:40:40,744 --> 01:40:42,537
为了让地球上的生活变得更美好

1305
01:40:42,620 --> 01:40:45,123
二、一、零

1306
01:40:47,208 --> 01:40:48,585
起飞

1307
01:41:41,304 --> 01:41:44,766
米洛 你看！那是什么？

1308
01:41:44,849 --> 01:41:47,227
-火箭
-太对了！

1309
01:41:48,394 --> 01:41:50,188
还有一辆漫游车！

1310
01:41:51,022 --> 01:41:54,025
漫游车 对 漫游车在里面！

1311
01:44:49,617 --> 01:44:51,619
字幕翻译： 李小秀

1312
01:44:51,703 --> 01:44:53,705
创意监督：肖雪

![输入图片说明](https://foruda.gitee.com/images/1669864364800379882/52b2d910_5631341.png "屏幕截图")

# [机遇号为何比其它火星车、月球车走得更长更久？](https://zhuanlan.zhihu.com/p/57055686)

[三体引力波](https://www.zhihu.com/people/cheng-yi-zhi-44)​ 大连檀橙文化有限公司 法定代表人

34 人赞同了该文章

![输入图片说明](https://foruda.gitee.com/images/1669864376399010290/94094764_5631341.png "屏幕截图")

机遇号，永别了，带走了遗憾，留下了传奇。

[机遇号…… 向传奇告别](https://mp.weixin.qq.com/s?__biz=MzI4MDQ0MzMxNA==&mid=2247502831&idx=1&sn=1fe70e95e5c33ebd03cb83b58b39cdb6)

![输入图片说明](https://foruda.gitee.com/images/1669864405791877516/9ea479e2_5631341.png "屏幕截图")

- 微信号：三体引力波 the-3body
- 功能介绍：从三体到引力波，从科幻到科学的科普品牌号

机遇号，曾创造过诸多纪录，其中两项堪称太空传奇：超期服役最长、行驶距离最远的探测车。

![输入图片说明](https://foruda.gitee.com/images/1669864442608902056/8851c26a_5631341.png "屏幕截图")

- 机遇号是人类史上在其它星球行驶时间最长的探测车——超期服役近15年（14年294天）。

原计划任务期90个火星日（92.5个地球日），实际运行5352个火星日（5498个地球日，相当于15个地球年或者8个火星年），是当初设计寿命的59倍。

![输入图片说明](https://foruda.gitee.com/images/1669864465287963648/9f150b41_5631341.png "屏幕截图")

- 机遇号是人类史上在其它星球行驶距离最远的，无论探测车还是宇航员——45.16公里。

比1973年苏联发射的月球车2号行驶39公里还要长6公里；比阿波罗登月时代行驶最远的阿波罗17号月球车（35.89公里）足足长了近10公里；机遇号是同款火星车勇气号行驶里程（7.73公里）的5.3倍；是中国玉兔号月球车行驶里程的451倍。

- 关键问题来了，为啥机遇号能够跑赢其它探测车？

简明扼要来说，

1. 坚固耐用的结构设计；
2. 创造性的解决方案；
3. 顶好顶好的运气，

三种因素造就了它的传奇。

![输入图片说明](https://foruda.gitee.com/images/1669864528538078277/db5c54dc_5631341.png "屏幕截图")

应该说，2003年发射的机遇号，集合了当时最高水准的航天设计工艺、工程学、机械学、材料学、计算机自动化、控制学、行星科学……多元学科与工艺。很多技术工艺与解决方案至今仍在沿用，甚至会用到未来。

![输入图片说明](https://foruda.gitee.com/images/1669864536763871679/c71d0c5c_5631341.png "屏幕截图")

### 骨骼强劲，经得住100度温差

要想在严酷的外星世界活得久，骨骼强劲是必须的。为此工程师们设计机遇号时，采用了一种名叫aluminum and titanium mobility systems铝钛合金动态系统，从而应对每天温差高达100°C的剧烈波动。

![输入图片说明](https://foruda.gitee.com/images/1669864648799692826/833f967a_5631341.png "屏幕截图")

### 自带暖宝，怀揣核心

光靠强劲骨骼还是不够的，要想抵御极寒（夜间火星气温降到-105°C），不会被冻死（冻死机），机遇号必须自带暖宝——8个放射性同位素加热器，让最稳定的钚氧化物——二氧化钚平稳持续地释放热能。

![输入图片说明](https://foruda.gitee.com/images/1669864663450130240/bbd1db38_5631341.png "屏幕截图")

### 像向日葵那样，转动太阳能翅膀

机遇号也自带了一套太阳能电池板，不过特别之处在于它是翼型的，你不妨理解为会转动的太阳能翅膀。

要知道，火星距离太阳是地日距离的两倍，充电效果有限。所以如何才能更好地吸收太阳光，显得非常关键。像向日葵那样，能够跟着太阳光弹性转动太阳能翅膀，这就是机遇号太阳能电池板的关键所在。

如果阳光充裕的话，机遇号还会来一把日光浴，彻底享受，彻底充满电。

这样的话，每个火星日最多能产生900瓦小时，这对于一辆高尔夫球车大小的火星车来说，虽说称不上能量丰沛，也算是够用。

![输入图片说明](https://foruda.gitee.com/images/1669864677669575963/a72f50a7_5631341.png "屏幕截图")

### 秒速五厘米，慢得稳当

当然，这也是机遇号为什么最高行驶速度只有5厘米/秒的主要原因。

秒速五厘米？

文艺点说就是樱花飘落的速度（新海诚同名动画片），差不多也是录音机磁带走带的速度。感觉很慢是吧。

要知道，这可是最高速度，其实机遇号的平均速度还不到最高秒速的1/5，仅仅0.89厘米/秒，不过这已是最快的火星车了。

当然，慢有慢得好处，慢——走得稳当可靠，走成最小风险。

15年走过了45公里，走出了一趟马拉松。

![输入图片说明](https://foruda.gitee.com/images/1669864695044584672/9e7cf653_5631341.png "屏幕截图")

### 怀揣太阳系最好的电池

光有灵活的太阳能翅膀、聪明的阳光转向系统还是不够的，机遇号还怀揣着「 **太阳系中最好的充电电池** 」（the finest batteries in the solar system——机遇号项目团队负责人Callas卡拉斯评价语），主电池能经受超过5000次充放电循环周期。

要知道，特斯拉的最新单体电池充放电周期1200次；iPhone手机电池的充放电循环周期，也就500次左右。

![输入图片说明](https://foruda.gitee.com/images/1669864725603866426/6a319950_5631341.png "屏幕截图")

### 明智之举，装死态

在火星上赶路，不可避免地会遇到冬季、沙尘暴，太阳能充电效率就会锐减。对此，机遇号项目团队最明智地解决方案，就是让火星车乖乖冬眠，进入装死态。

待到太阳光充裕时，重新启动所有设备，以便保证机遇号不被冻坏，更不会被冻死。

![输入图片说明](https://foruda.gitee.com/images/1669864725464132718/1911ceb8_5631341.png "屏幕截图")

### 六轮驱动，很难翻车

细心人可能会发现：无论是火星车还是月球车，都是六轮驱动，因为这种设计在崎岖不平的外星世界表现更好，着力更均衡、车身更平衡、行驶更平稳。

机遇号不仅同样六轮驱动，还装备了一种rocker-bogie「摇摆转向」悬架，即使是车身任意方向倾斜45度角也不会翻车。

与此同时，机遇号还装上了一套差速器，如果左侧轮子向上翘，那么右侧轮子就会向下压，这样尽可能将车身重量分摊到六个轮子上。

![输入图片说明](https://foruda.gitee.com/images/1669865006581554593/42bed6de_5631341.gif "wovzl-9ltoj.gif")

### 前后驱动，前后逢源

更厉害的是，机遇号火星车不仅会六轮向前驱动，还能向后驱动，前后逢源。而这种设计正好拯救了机遇号和它的孪生兄弟。

2005年，机遇号被困在一个名叫Purgatory Dune「炼狱沙丘」里，经过几周不懈努力，动用了一系列组合解决方案，包括向后驱动方法，最终使得机遇号摆脱困境。

2006年，孪生兄弟勇气号遭受右前轮经常失灵的打击，NASA项目团队及时改弦更张，多数时候让勇气号「倒车行使」，向后驱动，也就是拖着右前轮一路前行，这使得它又多走了三年路，直到2009年不幸陷入沙地里，无法动弹为止。

![输入图片说明](https://foruda.gitee.com/images/1669865034517268658/c45bab58_5631341.png "屏幕截图")

- 最关键的问题来了，既然机遇号能够拥有如此装备，为啥不能应用到其它探测车身上？

先来看看一组统计数据，迄今为止人类派出去的太空探测车，成功登陆并且有效行驶的，无论火星车还是月球车，总共只有11辆： **火星车4辆、月球车7辆** 。

其它都属于各种着陆探测器，如洞察号、嫦娥四号等等。

- 火星车4辆——

![输入图片说明](https://foruda.gitee.com/images/1669865062298450361/15fe52b4_5631341.png "屏幕截图")

### 火星探路者Sojourner（1997年）

设计期限90个火星日（92个地球日），实际运行83个火星日，行驶100多米，最高速度1米/分钟（1.6厘米/秒）。

![输入图片说明](https://foruda.gitee.com/images/1669865122390182199/6daf8cb0_5631341.png "屏幕截图")

### 勇气号Spirit（2004～2010年）

设计期限90个火星日，实际运行2208个火星日（2269个地球日），实际行驶时间1892个火星日（1944个地球日），行驶距离7.7公里，最高速度5厘米/秒。

### 机遇号Opportunity（2004～2018）

设计期限90个火星日，实际运行5352个火星日（5498个地球日），实际行驶时间1892个火星日（1944个地球日），行驶距离42.16公里，最高速度5厘米/秒。

![输入图片说明](https://foruda.gitee.com/images/1669865144549369590/776dcf77_5631341.png "屏幕截图")

### 好奇号Curiosity（2012至今，目前唯一运行的火星车）

设计期限2年，实际运行截至目前（2019年2月18日）2323个火星日（2386个地球日），行驶距离20.4公里，最高时速90米（2.5厘米/秒，相当于机遇号的一半），平均时速30米（0.83厘米/秒，比机遇号略低）。

- 月球车7辆——

![输入图片说明](https://foruda.gitee.com/images/1669865161175014790/d3bd35ad_5631341.png "屏幕截图")

> ▲ 苏联月球车1号（1970～1971）

设计期限3个月，实际运行321天，行驶10.54公里。

![输入图片说明](https://foruda.gitee.com/images/1669865178575877582/6bafbb37_5631341.png "屏幕截图")

> ▲ 苏联月球车2号（1973）

实际运行约4个月，行驶39公里（俄罗斯曾估算42公里，后与NASA共同确认这一结果），内部温度过热导致失联。

![输入图片说明](https://foruda.gitee.com/images/1669865188515509111/79f9d3c0_5631341.png "屏幕截图")

### 阿波罗15号月球车（1971年）

首次使用载人月球车（四轮全电驱动，3辆月球车共计花费4000万美元，作为登月宇航员的代步工具，用后即弃，至今仍留在月球），15号月球车实际行驶3小时02分，总计27.8公里，最高时速12公里。

![输入图片说明](https://foruda.gitee.com/images/1669865219686024976/8c07fb4f_5631341.png "屏幕截图")

### 阿波罗16号月球车（1972年）

实际行驶3小时26分，总计27.1公里。

![输入图片说明](https://foruda.gitee.com/images/1669865255021687790/5abebd28_5631341.png "屏幕截图")

### 阿波罗17号月球车（1972年）

实际行驶4小时26分，总计35.74公里，最高时速18公里，创下探测车最高行驶纪录。

![输入图片说明](https://foruda.gitee.com/images/1669865262162241296/15904cf7_5631341.png "屏幕截图")

### 玉兔号月球车（2013～2014）

设计期限3个月，实际运行973天，实际行驶42天，行驶距离114.8米（设计行驶距离10公里）。最终因电线问题（非机械故障）导致太阳能电池板无法移动到正确位置，低温冻死。

![输入图片说明](https://foruda.gitee.com/images/1669865272934240156/b1f3e3b2_5631341.png "屏幕截图")

### 玉兔2号月球车（2019至今）

设计期限3个月，实际运行自2019年1月3日至今，1月29日第一次从冬眠状态苏醒，2月28日将第二次苏醒，恢复运行，目前为止已行驶距离120米，超过前辈玉兔号。

别忘了，玉兔2号可是目前唯一运行的月球车。

![输入图片说明](https://foruda.gitee.com/images/1669865284470265078/d51a7c44_5631341.png "屏幕截图")

其实，我们从中已经看出一些端倪，机遇号不可能应用到他国探测车上。

迄今为止成功部署火星车、月球车的，目前只有美苏中三国。

20世纪70年代部署的都是太空竞赛的产物，不可能技术共享。即便到了现在，太空探索最尖端科技依然彼此设防——只有打探别国的份儿，没有拿出来共享的可能。所以，也就谈不上机遇号技术装备与他国分享的天真浪漫了。

![输入图片说明](https://foruda.gitee.com/images/1669865292937778177/5dd9122f_5631341.png "屏幕截图")

再看看NASA自己研发的4款火星车：

第一款火星探路者Sojourner属于上世纪90年代产物，也是打前阵的首款火星车，不管大小、性能、期望值，的的确确就是个探路者角色。

后来居上的孪生兄弟勇气号、机遇号，可以说是沿着火星探路者趟出来的路，才能一路前行、一走就是数年。

代表着21世纪第二个十年科技水平的好奇号Curiosity，同样是站在孪生兄弟肩膀之上，纵览红色星球、深度探索火星的。

事实上，每十年一次技术迭代，在NASA火星车身上体现得很明显。

![输入图片说明](https://foruda.gitee.com/images/1669865298842251556/0ffb52e9_5631341.png "屏幕截图")

- 为啥机遇号、勇气号设计期是90天，结果都活了几年？

其实，从最初的火星探路者一直到孪生兄弟勇气号、机遇号，设计任务期都是90个火星日（92个地球日）。

NASA工程设计师之所以给出这个期限，是根据火星灰尘覆盖太阳能电池板所需时间计算出来的。

并不是根据火星车核心部件、设备、机械的使用寿命决定的，更不是事先设定好它们的使用寿命就是90天。

![输入图片说明](https://foruda.gitee.com/images/1669865315728597747/aa13b6cd_5631341.png "屏幕截图")

幸运的是，机遇号、勇气号都大大超过了这一期限，机遇号是设计寿命的59倍，勇气号是设计寿命的25倍。

这要归功于火星风既能扬尘，也能拂尘，幸运的孪生兄弟时常遇到干净的火星风，尘土一扫而空。

![输入图片说明](https://foruda.gitee.com/images/1669865324513436498/808b1a4c_5631341.png "屏幕截图")

- 为啥同款火星车勇气号没能跑得更远、活得更久？

说到这儿，不能不提另一个因素：运气。

尽管孪生兄弟俩勇气号、机遇号生得一样，能耐一样，但探索路径不同，遭遇也必然不同，也就决定了命运不可能相同。

尤其是在孤寂无援的火星世界，偶然因素必然也会被放大。

前面说过，先行登陆的勇气号遭遇过两次打击：前轮失灵、深陷沙地，最终被困死沙场。2011年3月22日，NASA最后一次联系勇气号。

![输入图片说明](https://foruda.gitee.com/images/1669865334894830695/1dc2554b_5631341.png "屏幕截图")

相比而言，机遇号则要幸运得多。

一次陷入沙地，结果几周后竟然奇迹般走出绝地；一次机械臂出现故障，并不影响一路前行；

遭遇一次「失忆症」，数据存储器失灵导致系统重置，结果顽强活下来；

遭遇两次沙尘暴，2007年第一次，后来幸运遇到神奇的「迷你火星旋风」，将身上的沙尘一扫而空，太阳能翅膀照常使用。

![输入图片说明](https://foruda.gitee.com/images/1669865342839451654/5e9e1ecf_5631341.png "屏幕截图")

直到2018年遭遇第二次，也是最后一次。从2018年6月10日一直到2019年1月12日，NASA项目团队总计发出过800多个指令，竭尽全力地唤醒，只因幸运之神并非一直都在眷顾着机遇号，没有了神奇的火星旋风，没有了奇迹再现，死神接管了一切。

![输入图片说明](https://foruda.gitee.com/images/1669865351875450638/6caf9e4a_5631341.png "屏幕截图")

不过，机遇号一生无憾。

将要发射的新一代火星车Mars2020，也有机遇号的优秀DNA，未来人类踏足的火星世界，也会铭记这个先驱者的传奇……

![输入图片说明](https://foruda.gitee.com/images/1669865362120782664/61d86974_5631341.png "屏幕截图")

发布于 2019-02-18 20:37

# [再见机遇号：失联8个月 一千多次呼叫后被放弃](https://www.dogstar.net/post/420/)

2019/02/16 dogstar 2 幻灯模式

![输入图片说明](https://foruda.gitee.com/images/1669865886407266362/a80095ef_5631341.png "屏幕截图")

> 机遇号火星车，它于2004年与勇气号一道在火星分别成功着陆

北京时间2月14日凌晨，美国宇航局(NASA)宣布，机遇号火星车探测任务正式结束。

### 失联8个月联络1000多次

机遇号设计寿命仅三个月，但持续工作了近15年之久，为科学家寻找火星曾经适合生命存在的证据。然而八个月前，火星掀起了一场猛烈的全球性沙尘暴，机遇号从此失联。美国宇航局喷气推进动力实验室(JPL)的控制人员与机遇号尝试联系了1000多次，都杳无音讯。

美国当地时间2月12日，控制人员发送了最终的恢复指令，以及最后一首唤醒音乐Billie Holiday的《I’ll Be Seeing You》。太空依然沉寂，黯然泪下的控制人员没有收到任何回复。2018年6月10日，机遇号与地球的最后一次通信也成了“诀别”。

北京时间2月14日，在喷气推进动力实验室(JPL)举行的葬礼上，美国宇航局科学任务负责人托马斯·佐伯辰（Thomas Zurbuchen）宣布，我们心爱的机遇号已经离我们而去。

葬礼现场，数百名现任和前任任务团队成员都来为机遇号送行，他们负责过机遇号以及它的孪生兄弟勇气号的运行。面对他们，项目经理约翰·卡拉斯(John Callas)说道：“机遇号虽然是一台机器，但与它说‘再见’，令人不舍和难过，可我们不得不这样做，已经是时候了。”

![输入图片说明](https://foruda.gitee.com/images/1669865928509883170/ba6ba4c5_5631341.png "屏幕截图")

> 2004年7月26日，当机遇号像耐力陨坑进发时，火星车的影子恰好出现在其前方的避险相机面前。

![输入图片说明](https://foruda.gitee.com/images/1669865960196667851/e0c0b5d3_5631341.png "屏幕截图")

> 2016年3月31日（4332火星日），当机遇号在一处山坡上歇脚时，这辆火星车的导航相机拍摄到下方山谷中扫过的尘卷风。

### 15年行驶45公里创纪录

2004年，勇气号和机遇号火星车先后成功在着陆火星。两辆火星车完全相同，只有高尔夫球车大小，移动速度非常缓慢。按设计，他们将在火星行驶1公里，任务持续90个火星日，1火星日比地球日长39分钟。

2010年3月22日，工作6年之久的勇气号发回最后一次信号，之后便再无音讯。2011年5月24日，美国宇航局宣布终止唤醒勇气号的尝试。

相比之下，机遇号的表现更为出色，工作时长比勇气号还要多8年，由此创下的寿命和行驶里程纪录数十年内都难以被打破。截止去年6月失联前，机遇号已经在火星行驶45公里，工作时间比迄今其他任何着陆器都要长。

在科学家眼中，机遇号就如同一名机器地质学家。它配有一只机械臂，其末端携带有分析岩石和土壤的摄像机和设备。它最大的成果就是与勇气号一道，发现了远古火星表面曾有水流流动，并且能够维持微生物生存的证据。

项目科学家马修·戈隆贝克(Matthew Golombek)表示，这些火星车旨在帮助我们回答近乎神学的问题：生命是在条件恰到好处的地方形成的，还是我们真的非常幸运吗？

美国宇航局行星科学代理主任洛瑞·格拉兹(Lori Glaze)则表示，勇气号和机遇号开拓了探索其他行星表面的新途径。她解释说，火星车让我们可以直接到达想要研究的岩石附近，然后用显微成像仪近距离观察，甚至通过敲击、摇动、采样分析等手段，了解这些岩石的化学成分。

![输入图片说明](https://foruda.gitee.com/images/1669865986589400999/bca90a44_5631341.png "屏幕截图")

> 2004年1月31日，“机遇号”成功的驶下着陆器，它的六只轮子真正踏上了火星的地面。照片中我们可以间看见空空的着陆器，以及“机遇号”的轮子在火星土地上碾过的痕迹。该照片由“机遇号”尾部的危险识别相机（hazard-identification camera ）拍摄。

### 一个时代的结束

因沙尘暴失联时，机遇号正在考察毅力峡谷(Perseverance Valley)。此次的沙尘暴堪称几十年来火星最强烈的一次，火星的天空因此阴暗数月，导致机遇号的太阳能电池板无法接收到足够的阳光进行充电。

当沙尘暴消散，天空重新放晴后，机遇号没有了动静，其内部时钟可能出现故障，以至于火星车无法知道该何时休眠或苏醒接收指令。这也是为何控制人员发送了1000多次恢复指令也没有结果的原因。

鉴于每月维持项目的成本高达50万美元，美国宇航局最终决定，没有必要再继续尝试联络机遇号了。

项目经理约翰·卡拉斯透露，当最后一次尝试联络机遇号的指令发送结束时，控制室的许多人流下了眼泪，同时现场也响起了一阵掌声。·在场人员甚至都没有去等待是否会有信号从太空传回，因为他们知道，已经没有希望了。

这是一个悲伤的时刻，也是一个时代的结束，机遇号和勇气号都已经走了。

![输入图片说明](https://foruda.gitee.com/images/1669866012102743441/532c51a5_5631341.png "屏幕截图")

> “机遇号”拍下的火星泥土的显微照片

### 人类火星探索不止

目前，成功登陆火星的八个航天器全部来自美国宇航局，机遇号是其中第五个，而它们当中现在仍有两台仍在火星工作。

核动力的好奇号火星车，自2012年着陆后一直在火星表面四处探索；2018年底着陆的洞察号探测器，刚刚于本周在火星表面放置了一个热感应自锤式探测器，它可以像钻孔一样钻入火星内部。

而在2020年，来自美国、中国和欧洲的三个探测器也将着陆火星。

美国宇航局局长吉姆·布里登斯廷(Jim Bridenstine)表示，探索火星最重要的目标就是寻找过去甚至现在的火星上，存在微生物生命的证据，并为有朝一日，可能是2030年代，人类宇航员登上火星寻找合适的着陆地点。

他说：“从一个任务切换到另一个任务的确令人伤感，但这正是实现探索火星大目标的一部分”。

### 机遇号小成就 

- 2005年3月20日机遇号创造火星车单日最长行驶纪录，当天它行驶了220米； 

- 发回21.7万张照片，包括15张360°彩色全景图片；

- 打磨了52块岩石的表面，分析露出的矿物成分；还用刷子清理了72个额外目标，供光谱仪和显微成像仪探测； 

- 在其着陆点发现赤铁矿，一种在有水环境中形成的矿物；

- 在耐力陨坑发现远古水活动的有力证据，这些水类似地球上的池塘或湖泊。

(dogstar)

### 延伸阅读

- [视频：从发射到着陆！360秒回顾机遇号近15年火星任务](http://www.dogstar.net/post/422/)
- [主题图集：机遇号火星照片回顾](http://www.dogstar.net/photo/ablum33/)
- [机遇号火星失联两个多月还能醒来吗？这六件事了解一下](http://www.dogstar.net/post/395/)

---

【笔记】[晚安机遇号](https://gitee.com/yuandj/siger/issues/I640LK)

![输入图片说明](https://foruda.gitee.com/images/1669834843820011293/5b739a25_5631341.jpeg "D80C0532@9D3F3754.3787876300000000.jpg")

- [机遇号的设计寿命只有3个月，她是如何创造纪录在火星上孤独的运行15年的](https://www.bilibili.com/video/BV16D4y1U7U6) @[乔壹REC](https://space.bilibili.com/442357714)

  > 机遇号的设计寿命只有3个月，她是如何创造纪录在火星上孤独的运行15年的，传奇火星车机遇号和勇气号的火星探索解密

- I’ll Be Seeing You

  > 机遇号设计寿命仅三个月，但持续工作了近15年之久，为科学家寻找火星曾经适合生命存在的证据。 然而八个月前，火星掀起了一场猛烈的全球性沙尘暴，机遇号从此失联。 美国宇航局喷气推进动力实验室 (JPL)的控制人员与机遇号尝试联系了1000多次，都杳无音讯。 美国当地时间2月12日，控制人员发送了最终的恢复指令，以及最后一首唤醒音乐Billie Holiday的《I’ll Be Seeing You》。

    - 再见机遇号：失联8个月 一千多次呼叫后被放弃_最新文章__天狼 …  
      www.dogstar.net/post/420/