##  What is Breadth-First Search (BFS) ？

# 基本概念     
    BFS 是（Breath First Search）的缩写，中文就是广度优先搜索，也有叫 宽度优先搜索的，

以前我们上课的老师还叫做 “白法师”， 正好和 DFS（Depth First Search） “大法师”对应上。 

# 具体思路
    BFS在搜索中就是广度入手，先搜索能够到达的范围，然后再一层一层的向外扩散。直到最终找到目标。


# 实现方法
    BFS 的实现都是通过队列 queue 来实现的。因为队列属于是先进先出，所以先把最先能达到一层的入队，
然后再按照入队的顺序先到达的一层再去搜索它能达到的下一层。 举个例子我用BFS解数独里面的程序就是这样，
先把填好第一个空格的所有合法方案都入队，然后再依次读取后看看填第一个空格后能有多少填第二个空格的方案，
然后依次填第三个空格，第四个空格．．．．直到全部填满就完成了。
```
 q.push(node);    　　//　入队最初始的起点
    while( !q.empty()) {  // bfs
    	node = q.front();   
        q.pop();			
        int num = -1;
		for(int i=0;i<81;i++) { // find next
			if(node.a[i]==0) {
				num = i;
				break;
			} 	
		}
		if(num == -1) {	 　//　如果没有空格了则表示全部填完了。
			print(node.a);   
			flag = true; // win 
			break;	
        }
        for(int i = 1; i <= 9; i++) {  　//　每个空格都可以填以填　１－９
            node.a[num] = i;
            if (check(node.a)) {  // if  true　　如果这个填法是合理的
            	q.push(node);     　//　就把这个填法也压入队。
			}	
        }
	}
```

＃　容易和DFS混淆的地方
    记得我当时学这个的时候总是容易把　DFS和BFS　搞混，因为这两种搜索方式有点像，
后来我就去问曾经做过程序员的爸爸，这两种搜索区别到底是什么。爸爸想了一会，说你记
不记得以前我们一起看的电影《流浪地球》，驱动地球的发动机有一些熄火，每台发动机都
需要火石来驱动，救援行动总共派出了很多人运送大量火石进行饱和式救援。这种饱和式救
援就有点类似BFS，每一条道路都派一支小分队去，这样虽然有的分队会受到阻拦，但最后总
有一支队伍能点燃发动机完成任务的。哦，我似乎明白了一些，那DFS又是怎样的呢？我接着
问道。DFS就有点类似只派一支超级队伍，碰到走不通的就回退到分叉口重新换一条路。
    听到这里，我终于彻底理解了DFS和BFS的区别，再也不会混淆了。

＃　BFS的用途
    DFS和BFS作为两种基本的搜索算法可以用在非常多的地方，由于我更喜欢流浪地球的那种
饱和式救援，所以更加偏爱BFS这种方式。除了解数独以外，我后来还陆陆续续写了很多BFS的
程序，比如解2阶魔方，算简单24点，杯子倒水，韩信分油等等....后面有机会会继续和大家分
享，BFS 真的特别好用。
