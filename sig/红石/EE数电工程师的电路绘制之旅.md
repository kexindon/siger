红石数电评论：
- [【Minecraft】基于随机计算的纯红石卷积神经网络——概览](%E3%80%90Minecraft%E3%80%91%E5%9F%BA%E4%BA%8E%E9%9A%8F%E6%9C%BA%E8%AE%A1%E7%AE%97%E7%9A%84%E7%BA%AF%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E6%A6%82%E8%A7%88.md)
- [【Minecraft】红石神经网络教程第一节——随机数生成、运算及计数.md](红石神经网络教程第一节——随机数生成、运算及计数.md) 【[视频](https://www.bilibili.com/video/BV1qW4y117Xv)】
- [【论文3】VLSI Implementation of Deep Neural Network Using Integral Stochastic Computing（中译）](VLSI%20Implementation%20of%20DNN%20Using%20Integral%20SC.md)

SIGer 学习笔记：
- [数电基础：组合逻辑电路之乘法器的设计.md](%E6%95%B0%E7%94%B5%E5%9F%BA%E7%A1%80%EF%BC%9A%E7%BB%84%E5%90%88%E9%80%BB%E8%BE%91%E7%94%B5%E8%B7%AF%E4%B9%8B%E4%B9%98%E6%B3%95%E5%99%A8%E7%9A%84%E8%AE%BE%E8%AE%A1.md) 
- [ReLU网络结构的设计.md](ReLU%E7%BD%91%E7%BB%9C%E7%BB%93%E6%9E%84%E7%9A%84%E8%AE%BE%E8%AE%A1.md)
- [ReLU神经网络de手写输入？.md](ReLU%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9Cde%E6%89%8B%E5%86%99%E8%BE%93%E5%85%A5%EF%BC%9F.md)
- [Pytorch实现手写数字识别.md](Pytorch%E5%AE%9E%E7%8E%B0%E6%89%8B%E5%86%99%E6%95%B0%E5%AD%97%E8%AF%86%E5%88%AB.md)【[基于全连接神经网络](https://www.biaodianfu.com/imdb-sentiment-analysis-full-connection.html)】

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0729/085245_bfa28c10_5631341.jpeg" title="数电3副本.jpg"></p>

> 翻过自己心中最难的那座山的时间。这是《[我们距离RISCV红石CPU还有多远.md](我们距离RISCV红石CPU还有多远.md)》完成时写下的话。又缩短了一周啦，还有两个点：权重数据的存储，真的手搓一把红石电路。月初发文给小陈老师，可不能成了红石砖家再开始动手啊，现在跃跃欲试了，就像小朋友刚学了两招儿，就想找人练练一样。在几百万追粉中的留言：如果现实世界真有红石，距离手搓RISC-V应该不远啦。Why NOT? 开始本周学习，还是要感谢小陈老师的鼓励，直接上手画起来。

# [学习笔记](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2)提纲：

1. [卷积神经网络设计 15x15→7x7](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11847025_link) 

   - sig/红石/[数电基础：组合逻辑电路之乘法器的设计.md](%E6%95%B0%E7%94%B5%E5%9F%BA%E7%A1%80%EF%BC%9A%E7%BB%84%E5%90%88%E9%80%BB%E8%BE%91%E7%94%B5%E8%B7%AF%E4%B9%8B%E4%B9%98%E6%B3%95%E5%99%A8%E7%9A%84%E8%AE%BE%E8%AE%A1.md) 

     | [并行](%E6%95%B0%E7%94%B5%E5%9F%BA%E7%A1%80%EF%BC%9A%E7%BB%84%E5%90%88%E9%80%BB%E8%BE%91%E7%94%B5%E8%B7%AF%E4%B9%8B%E4%B9%98%E6%B3%95%E5%99%A8%E7%9A%84%E8%AE%BE%E8%AE%A1.md#%E4%B8%80%E5%B9%B6%E8%A1%8C%E4%B9%98%E6%B3%95%E5%99%A8) | [移位相加](%E6%95%B0%E7%94%B5%E5%9F%BA%E7%A1%80%EF%BC%9A%E7%BB%84%E5%90%88%E9%80%BB%E8%BE%91%E7%94%B5%E8%B7%AF%E4%B9%8B%E4%B9%98%E6%B3%95%E5%99%A8%E7%9A%84%E8%AE%BE%E8%AE%A1.md#%E4%BA%8C%E7%A7%BB%E4%BD%8D%E7%9B%B8%E5%8A%A0%E4%B9%98%E6%B3%95%E5%99%A8) | [查找表乘法器](%E6%95%B0%E7%94%B5%E5%9F%BA%E7%A1%80%EF%BC%9A%E7%BB%84%E5%90%88%E9%80%BB%E8%BE%91%E7%94%B5%E8%B7%AF%E4%B9%8B%E4%B9%98%E6%B3%95%E5%99%A8%E7%9A%84%E8%AE%BE%E8%AE%A1.md#%E4%B8%89%E6%9F%A5%E6%89%BE%E8%A1%A8%E4%B9%98%E6%B3%95%E5%99%A8) | [加法树](%E6%95%B0%E7%94%B5%E5%9F%BA%E7%A1%80%EF%BC%9A%E7%BB%84%E5%90%88%E9%80%BB%E8%BE%91%E7%94%B5%E8%B7%AF%E4%B9%8B%E4%B9%98%E6%B3%95%E5%99%A8%E7%9A%84%E8%AE%BE%E8%AE%A1.md#%E5%9B%9B%E5%8A%A0%E6%B3%95%E6%A0%91%E4%B9%98%E6%B3%95%E5%99%A8) | [乘法器比较](%E6%95%B0%E7%94%B5%E5%9F%BA%E7%A1%80%EF%BC%9A%E7%BB%84%E5%90%88%E9%80%BB%E8%BE%91%E7%94%B5%E8%B7%AF%E4%B9%8B%E4%B9%98%E6%B3%95%E5%99%A8%E7%9A%84%E8%AE%BE%E8%AE%A1.md#%E4%BA%94%E5%9B%9B%E7%A7%8D%E4%B9%98%E6%B3%95%E5%99%A8%E6%AF%94%E8%BE%83) | [乘累加器](%E6%95%B0%E7%94%B5%E5%9F%BA%E7%A1%80%EF%BC%9A%E7%BB%84%E5%90%88%E9%80%BB%E8%BE%91%E7%94%B5%E8%B7%AF%E4%B9%8B%E4%B9%98%E6%B3%95%E5%99%A8%E7%9A%84%E8%AE%BE%E8%AE%A1.md#%E5%85%AD%E4%B9%98%E7%B4%AF%E5%8A%A0%E5%99%A8) |

2. [我们将网络的结构设定成 15×15→7×7→30→30→10](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11849410_link)

   > [【Minecraft】基于随机计算的纯红石卷积神经网络——概览](%E3%80%90Minecraft%E3%80%91%E5%9F%BA%E4%BA%8E%E9%9A%8F%E6%9C%BA%E8%AE%A1%E7%AE%97%E7%9A%84%E7%BA%AF%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E6%A6%82%E8%A7%88.md)

   ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/022232_beb1b1b4_5631341.png "在这里输入图片标题")

   [15×15→7×7](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11849513_link)

   - sig/红石/[ReLU网络结构的设计.md](ReLU%E7%BD%91%E7%BB%9C%E7%BB%93%E6%9E%84%E7%9A%84%E8%AE%BE%E8%AE%A1.md)

     - ReLU网络神经元个数如何确定？
     - [确定神经网络层数以及神经元个数](https://blog.csdn.net/sinat_38079265/article/details/121519632)

   [7×7→30](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11860776_link)

   [30->30](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11889116_link)

   - sig/红石/[ReLU神经网络de手写输入？.md](ReLU%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9Cde%E6%89%8B%E5%86%99%E8%BE%93%E5%85%A5%EF%BC%9F.md)

     - [手撸神经网络（识别手写数字）](https://blog.csdn.net/m0_47595520/article/details/109177836)
     - [激活函数ReLU的理解与总结](https://zhuanlan.zhihu.com/p/376520223)

3. [权重如何生成: 5Hz单比特随机数生成器](%E3%80%90Minecraft%E3%80%91%E5%9F%BA%E4%BA%8E%E9%9A%8F%E6%9C%BA%E8%AE%A1%E7%AE%97%E7%9A%84%E7%BA%AF%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E6%A6%82%E8%A7%88.md) 【[笔记](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11889168_link)】
   - [sig/红石/红石神经网络教程第一节——随机数生成、运算及计数.md](红石神经网络教程第一节——随机数生成、运算及计数.md) 【[视频](https://www.bilibili.com/video/BV1qW4y117Xv)】

4. [神经元电路](%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E5%8E%9F%E7%90%86.md#%E7%A5%9E%E7%BB%8F%E5%85%83%E7%94%B5%E8%B7%AF) [最重要的一步是激活函数。](%E3%80%90Minecraft%E3%80%91%E5%9F%BA%E4%BA%8E%E9%9A%8F%E6%9C%BA%E8%AE%A1%E7%AE%97%E7%9A%84%E7%BA%AF%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E6%A6%82%E8%A7%88.md) 【[笔记](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11889171_link)】

5. [最后一层的输出（以及层间缓存）：5hz模电计数器](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11906869_link)

   [30→10](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11932307_link)

6. sig/红石/[Pytorch实现手写数字识别.md](Pytorch%E5%AE%9E%E7%8E%B0%E6%89%8B%E5%86%99%E6%95%B0%E5%AD%97%E8%AF%86%E5%88%AB.md)【[基于全连接神经网络](https://www.biaodianfu.com/imdb-sentiment-analysis-full-connection.html)】

7. sig/红石/[VLSI Implementation of DNN Using Integral SC.md](VLSI%20Implementation%20of%20DNN%20Using%20Integral%20SC.md)

## 1、[卷积神经网络设计 15x15→7x7](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11847025_link)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0717/031347_f9beca4c_5631341.png "在这里输入图片标题")

卷积公式：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0723/190341_29be0b7c_5631341.png "屏幕截图.png")

手写输入 = 

<img height="99px" src="https://images.gitee.com/uploads/images/2022/0717/030527_e9b76d4e_5631341.png">  + <img height="99px" src="https://images.gitee.com/uploads/images/2022/0717/030754_267ed1f8_5631341.png"> 

= 卷积输出

![输入图片说明](https://images.gitee.com/uploads/images/2022/0724/020809_f653e62e_5631341.png "电路绘制副本.png")

- [BING 算法公式逻辑电路框图](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11993846_link)

  <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0723/183612_4f96fbe2_5631341.png"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0723/183629_cbf66618_5631341.png"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0723/183747_e701ef31_5631341.png"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0723/183844_1d506383_5631341.png"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0723/184033_fabe365d_5631341.png"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0723/184626_d379b67d_5631341.png"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0724/010028_27fd1049_5631341.png"><img height="99px" src="https://images.gitee.com/uploads/images/2022/0724/012126_78b6bb5d_5631341.png"></p>

## 2、[我们将网络的结构设定成 15×15→7×7→30→30→10](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11849410_link)

### [15×15→7×7](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11849513_link)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0724/102925_9499da0c_5631341.jpeg "15x15-7x7up副本.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0724/103643_ef6f768d_5631341.jpeg "15x15-7x7v副本.jpg")

### [7×7→30](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11860776_link)

7×7→30 第一步，引出卷积输出：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0725/110350_b0d4fa88_5631341.png "屏幕截图.png")

卷积3x3输入的输出是两条红石线路，并作为 30的全连接层第一层的输入，需要进一步调研！

![输入图片说明](https://images.gitee.com/uploads/images/2022/0725/111207_5974551a_5631341.png "屏幕截图.png")

这是卷积3x3本体，以及两条输出红石线，于前面的完整输出线的对接。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0725/112042_47e4c935_5631341.png "屏幕截图.png")

这是 7x7 转接 30 的重要接口接口，是一个16x3层的硬连接，下方是 30 全连接第一层的输入，后面会介绍全连接电路，但是没有明确介绍输入的结构是什么？上方就是7x7的引线会垂直向下，与此接口链接。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0725/234247_775352f3_5631341.png "屏幕截图.png")

7x7 输出引线到这里，每3条为一组的输入，对接 30的输入。   
如果把30的输入分成上下两组，分别是15个输入，需要15x3=45个输入，本图展示的是灰色7x7输出  
30输入的坐标，上到下abc，本视角为右的话（前图面向输入屏），应该是 ，16a,16b,16c 对接  
卷积输出的灰色：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0725/235206_b58c45d9_5631341.png "屏幕截图.png")

顺着这个图，确定卷积输出的灰色的输入屏坐标。分别是1，3，5行，如果用a,c,e表示的话。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/000323_bcb11804_5631341.png "屏幕截图.png")

7x7 输出，第1层 a层，左到右对接的是 30输入的第1层 a层。最右边的6组，坐标是：  
1a-11a, 2a-12a, 3a-13a, 4a-14a, 5a-15a, 6a-16a。7a 另外连接！

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/001244_7438b1d8_5631341.png "屏幕截图.png")

7x7 输出，第2层 b层全部7个输出，左到右对接的是 30输入的中间7组，坐标是：  
1b-4a, 2b-5a, 3b-6a, 4b-7a, 5b-8a, 6b-9a, 7b-10a. 30输a层就剩下最后3组。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/001927_21bd9c68_5631341.png "屏幕截图.png")

7x7 输出的第3层，c层最右面的3个输出，对应了 30输入第一层最左边的三个输入：  
5c-1a, 6c-2a, 7c-3a.

1g-1c, 2g-2c, 3g-3c, 4g-4c, 5g-5c, 6g-6c, 7g-7c,  (最底层7个输出，对最底层最左7个输入)  
1f-8c, 2f-9c, 3f-10c, 4f-11c, 5f-12c, 6f-13c, 7f-14c, （倒数第二层第六层 7个输出，对紧跟着的7个输入）  
1e-15c, 2e-16c （倒数第三层第5层 左面的2个输出，对最底层最右面的两个输入。）

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/005925_6ace119f_5631341.png "屏幕截图.png")

30输入的16个左到右依次是：e5(3e,4e,5e,6e,7e),d7(1d,2d,3d,4d,5d,6d,7d),c4(1c,2c,3c,4c)  
e5(3e-1b, 4e-2b, 5e-3b, 6e-4b, 7e-5b,)  
d7(1d-6b, 2d-7b, 3d-8b, 4d-9b, 5d-10b, 6d-11b, 7d-12b),  
c4(1c-13b, 2c-14b, 3c-15b, 4c-16b)

[`https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11860776_link`](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11860776_link)

5c-1a, 6c-2a, 7c-3a.  
1b-4a, 2b-5a, 3b-6a, 4b-7a, 5b-8a, 6b-9a, 7b-10a.  
1a-11a, 2a-12a, 3a-13a, 4a-14a, 5a-15a, 6a-16a.   
7a 悬空！

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/011615_34fb483c_5631341.png "屏幕截图.png")

conv-ReLU layer    
→, ↓ conv_(0-48)

![](https://images.gitee.com/uploads/images/2022/0726/012039_a44ac650_5631341.png "屏幕截图.png")

敲空了7a 输出没有使用。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/013425_dee61a10_5631341.png "屏幕截图.png")

48个输入被并行分派的30的输入，  
（可能需要30全连接层的输入端通过选址的方式，实现输入选择，需要下节求证）。  
卷积输入的49个输出剪掉最后一个，成48个输出，分三行作为30输入，

30输入从工程上分上下两层，每层15个神经元，堆叠方向与 卷积输出垂直。

7a: 悬空！

| 5c | 6c | 7c | 1b | 2b | 3b | 4b | 5b | 6b | 7b | 1a | 2a | 3a | 4a | 5a | 6a |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
| 1a | 2a | 3a | 4a | 5a | 6a | 7a | 8a | 9a | 10a | 11a | 12a | 13a | 14a | 15a | 16a |

| 3e | 4e | 5e | 6e | 7e | 1d | 2d | 3d | 4d | 5d | 6d | 7d | 1c | 2c | 3c | 4c |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
| 1b | 2b | 3b | 4b | 5b | 6b | 7b | 8b | 9b | 10b | 11b | 12b | 13b | 14b | 15b | 16b |

| 1g | 2g | 3g | 4g | 5g | 6g | 7g | 1f | 2f | 3f | 4f | 5f | 6f | 7f | 1e | 2e |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
| 1c | 2c | 3c | 4c | 5c | 6c | 7c | 8c | 9c | 10c | 11c | 12c | 13c | 14c | 15c | 16c |

此部分，如何进行的输入端数据合并，见[全连接层输入](%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E5%8E%9F%E7%90%86.md#%E5%85%A8%E8%BF%9E%E6%8E%A5%E5%B1%82)： 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0717/193602_91302003_5631341.png "在这里输入图片标题")

y = f(0.5x1-0.7x2+0.3x3) = f(0.5b-0.7a+0.3c) (需要求证下)

另外还可以参照[源码求证](%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E5%8E%9F%E7%90%86.md#%E4%BB%A3%E7%A0%81-codes)！  
https://gitee.com/RV4Kids/StochasticNet

随机计算模拟器 / SC simulator (C++)
- bitstring.h
- neuron.h
- CNN.cpp

![输入图片说明](https://images.gitee.com/uploads/images/2022/0728/111040_e9e8f6ac_5631341.png "屏幕截图.png")

再论文3中，以上是传统随机函数的伪码，与基于随机计算的串流的整数计算方案的伪码对对照（右侧为整数计算）换来了平效的同时，提高了计算的速度，尽管随机串计算本身需要时长代价。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0728/111826_a4cb097a_5631341.png "屏幕截图.png")

神经网络设计 7x7->30->30->10 共三次计算。其中可见层输入，以及隐藏层输出都使用3->1的加权处理。本节是可见输入的电路部分。使用右侧的神经元框图表示系统逻辑。

其中7x7->30的输入对照表见贴：

[![输入图片说明](https://images.gitee.com/uploads/images/2022/0728/112151_72f2dc81_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11889199_link)

具体的红石电路描述的神经元，则包含了这个3->1的输入变化。后面再介绍。

### [30->30](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11889116_link)

我们将网络的结构设定成15×15→7×7→30→30→10。  
网络结构是如何设置的？

阅读之后的参考文献了解原理后，结合红石电路，

7x7->30 甩掉了最后一个数，三个一组作为一个输入（具体如何合并，需要细节，如[上节末尾的表格](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11889199_link)）    
30->30 则直接输出，以下图为证：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/025136_404c9dec_5631341.jpeg "全连1到全连2，30-30.jpg")

分上下两层，15x2，输出对输入。上图左侧为输入手写板，右侧为原理卷积层的神经元输出和输入。

- 此处，就是红石电路的并行输入的实现方案，真实电路不需要模仿！

## 3、[权重如何生成: 5Hz单比特随机数生成器](%E3%80%90Minecraft%E3%80%91%E5%9F%BA%E4%BA%8E%E9%9A%8F%E6%9C%BA%E8%AE%A1%E7%AE%97%E7%9A%84%E7%BA%AF%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E6%A6%82%E8%A7%88.md) 【[笔记](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11889168_link)】

考虑到在Minecraft中做反向传播是不现实的，权重都是使用pytorch训练好然后通过代码编辑到nbt文件里，接着通过WorldEdit（小木斧）导入Minecraft。因为权重也是随机串，所以只要编辑随机数生成装置即可。具体来说，就是调整投掷器内不同物品的数量。

随机数生成的原理是投掷器投掷物品是随机的。借此可以搭一个随机数生成器。不可堆叠的钻石剑输出信号强度2，而可堆叠的猪排输出1,。通过调整可堆叠物品和不可堆叠物品的比例，可以产生分数概率的随机数。简单优化后，我们可以得到一个2tick输出一次的随机数生成器。它将生成网络权重。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/022250_dbb3e889_5631341.png "在这里输入图片标题")

> 5Hz单比特随机数生成器

【Minecraft】[红石神经网络教程第一节——随机数生成、运算及计数](https://www.bilibili.com/video/BV1qW4y117Xv) 详细介绍了，如何生成随机串，并展示了随机串的概率。

这就给逆向出权重带来了麻烦，开源的只是红石的数据文件，原始的权重文本，并没有放出。
（这又是一个难点！或许，我们需要自己训练一下这个网络。）

全连接层需要矩阵运算，即加法和乘法。乘法如前文所说，只要一个同或就可以了，而两个串是无法相加得到新串的。毕竟-1`~`1的串没法表示-2~2的范围。为此，我们使用模电拓展随机01串，将01串变成整数串。由此实现加法。

- [sig/红石/红石神经网络教程第一节——随机数生成、运算及计数.md](红石神经网络教程第一节——随机数生成、运算及计数.md) 【[视频](https://www.bilibili.com/video/BV1qW4y117Xv)】

此处的神经元随机串计算原理，不需要复刻。

 **未解决的问题是：权重保存在哪里？怎么保存的？** 

## 4、[神经元电路](%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E5%8E%9F%E7%90%86.md#%E7%A5%9E%E7%BB%8F%E5%85%83%E7%94%B5%E8%B7%AF) [最重要的一步是激活函数。](%E3%80%90Minecraft%E3%80%91%E5%9F%BA%E4%BA%8E%E9%9A%8F%E6%9C%BA%E8%AE%A1%E7%AE%97%E7%9A%84%E7%BA%AF%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E6%A6%82%E8%A7%88.md) 【[笔记](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11889171_link)】

神经网络的数学本质是将输入数据非线性地映射到高维度特征空间然后线性分类。而激活函数则是提供非线性的来源。主流的激活函数有ReLU，tanh，sigmoid等。我们在卷积层使用ReLU，全连接层使用tanh。如前文所述，使用随机计算可以简单的实现tanh。这一算法详见论文[3]。简而言之，我们可以只使用一个全加器完成计算。

我们在卷积层使用ReLU，
> 前节已经充分讨论，有了合适的输出 X1,X2,X3，之后tanh，7x7变成30

全连接层使用tanh。如前文所述，使用随机计算可以简单的实现tanh。这一算法详见论文[3]。
> 核心是如何用随机计算来模拟 Tanh 或者逼近 Tanh ，这中间用到的随机数的输出概率的双极表示（数）

下面进行延展阅读。

[神经元电路](https://gitee.com/flame-ai/siger/blob/master/sig/%E7%BA%A2%E7%9F%B3/%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E5%8E%9F%E7%90%86.md#%E7%A5%9E%E7%BB%8F%E5%85%83%E7%94%B5%E8%B7%AF)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0717/194402_4538d41b_5631341.png "在这里输入图片标题")

- 加权求和 / Weighted summation
- 激活函数 / Activation function
- ↓ 输入 / input xi ↓
- → 输出 / output y

其中：  
权重被储存在投掷器里（调整物品配比生成不同的频率的随机串）  
The weights are stored in the droppers (adjust items' ratio to get different random strings)

如何保存，也就是投掷器中物品的配比，影响随机串的输出概率  
见：【Minecraft】[红石神经网络教程第一节——随机数生成、运算及计数](https://www.bilibili.com/video/BV1qW4y117Xv)，的 [学习笔记](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11889222_link)

- 除了反向获得权重数据？
- 权重数据是如何加入计算也是问题？

上图中，加权求和指的是：

[![输入图片说明](https://images.gitee.com/uploads/images/2022/0717/193602_91302003_5631341.png "在这里输入图片标题")](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11889203_link)

在接下来的神经元电路中，此加权求TanH 也是用随机串模拟的，如下：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0717/195734_9b037308_5631341.gif "在这里输入图片标题")

而其原理是论文3揭示的。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0717/195929_5a894f6a_5631341.png "在这里输入图片标题")

推测是，权重和这3个输入合并，汇集成4个随机数串，最后用极值表示，趋于 Tanh 的真值结果。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0717/203334_e3dff98c_5631341.gif "在这里输入图片标题")

相应的电路实现稍后会补上：《VLSI Implementation of Deep Neural Network Using Integral Stochastic Computing.pdf》

> 比如这里有3个“1”和1个“0”，那么累加器S加3减1得2，此时S≥0, 输出1  
> Here, the accumulator S should add 3 and minus 1 and become 2, S≥0, output 1  
> 类似描述，可以和论文中的电路实现进行一一对应，在绘制电路时使用。

接下来是类似的计算。可以证明，输出的串表示 tanh 的结果（双极表示）  
Then, similar calculations. It can be proved that the output string is the tanh result (bipolar)

[电路实现](https://gitee.com/flame-ai/siger/blob/master/sig/%E7%BA%A2%E7%9F%B3/%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E5%8E%9F%E7%90%86.md#%E7%94%B5%E8%B7%AF%E5%AE%9E%E7%8E%B0)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0717/211704_00f78360_5631341.png "在这里输入图片标题")

该电路的构造未明：

电路实现：首先通过模电计算加法，而后转为数电信号  
Realization by circuits: first, analog summation, then converted to digital signals

累加器则改装了 @euap!xoxOxxoO 提供的 2tick 流水线加法器，使之不会溢出  
The accumulator is made by preventing the 2tick pipeline adder (by @euap!xoxOxxoO) from overflow

阅读了论文3的相关章节，就是随机串的逐位累加，通过SC计算的电路实现，对tanh的逼近。相关章节描述的是最少 1024 的串长度。因此需要等待一段时间，才能得到统计结果的近似值。

在神经元电路的前段输入，对x1,x2,x3的加权求和，也是使用的随机串模拟的。这样就有了 x1+x2+x3+x4取tanh的形式。

随机串的概率，保存在投掷器中，应该可以在存档中可以查看到。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0728/112842_761adc93_5631341.png "屏幕截图.png") 

这是随机整数计算的逐位与计算的电路实现。替换掉神经元电路的核心部分，就可以啦。根据神经元的数量，平铺克隆即可。

由于红石电路的累加器的实现方式，没有相关教程，需要电路设计师重新等价设计。待到红石电路累加器的教程出来后，再行比对。

## 5、[最后一层的输出（以及层间缓存）：5hz模电计数器](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11906869_link)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0717/212434_ab83af5a_5631341.png "在这里输入图片标题")

最后一层的输出（以及层间缓存）使用这样一个模电计数器  
The output of the last layer (as well as inter-layer buffers) uses such an analog counter

这个计数器可以统计 5hz 串中“1”的数量，容量是1024  
This counter can count the "1"s in a 5Hz string, with maximum capacity of 1024.

![输入图片说明](https://images.gitee.com/uploads/images/2022/0717/213051_38e8a452_5631341.png "在这里输入图片标题")

如何计算5hz串中 1 的数量，需要研究这个计数器的实现，否则，就要自己设计啦？

使用对随机串的1的数量统计的方法，作为输出层的设计。通过训练，发现各个传输层之间的联系权重。然后倒入到神经网络中，最终实现，数字的分类识别。

- 这个神经网络输出30->10是怎么实现的，需要考察电路连接。

### [30→10](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5IKI2#note_11932307_link)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0727/111408_23a26ea3_5631341.png "屏幕截图.png")

本图清晰地展示了三层连接的递减关系：  
7x7->30 tanh(x1+x2+x3)  
30->30 tanh(s1)weight  
30->10 tanh(x1+x2+x3)

上图是第2层（蓝色30）输出（红色）到第3层（黄色10）  
黄色10 会接主贴的 5hz串计数。

训练权重，还没有找到! 电路连线已经清晰啦。可以画顶层设计啦。

10个输出的随机串，按照5赫兹统计1的个数。未能理解。电路部分也无从下手。暂且防止。直接输出系统框图。

## 6、sig/红石/[Pytorch实现手写数字识别.md](Pytorch%E5%AE%9E%E7%8E%B0%E6%89%8B%E5%86%99%E6%95%B0%E5%AD%97%E8%AF%86%E5%88%AB.md)【[基于全连接神经网络](https://www.biaodianfu.com/imdb-sentiment-analysis-full-connection.html)】

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/153838_c401812c_5631341.png)

## 7、sig/红石/[VLSI Implementation of DNN Using Integral SC.md](VLSI%20Implementation%20of%20DNN%20Using%20Integral%20SC.md)

<p><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/071057_c4d2186c_5631341.png"><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/071021_ba9f9e7e_5631341.png"></p>

Fig. 2. Stochastic multiplications using (a) AND gate in unipolar format and (b) XNOR gate in bipolar format
> 图 2. 使用 (a)单极格式的与门和 (b)双极格式的异或门进行随机乘法

<p><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/071448_e23581d8_5631341.png"><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/071412_d8ee9257_5631341.png"></p>

Fig. 3. Stochastic additions using (a) MUX and (b) OR gate
> 图 3. 使用 (a) MUX 和 (b) OR门的随机加法

<p><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/072542_d8f34bd2_5631341.png"><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/072614_8bce6b2b_5631341.png"></p>

Fig. 5. (a) Stochastic representations of 0.75 and (b) Integer stochastic representation of 1.5
> 图 5. (a) 0.75 的随机表示和 (b) 1.5 的整数随机表示

![输入图片说明](https://images.gitee.com/uploads/images/2022/0728/112842_761adc93_5631341.png "屏幕截图.png") 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0728/111040_e9e8f6ac_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0728/111826_a4cb097a_5631341.png "屏幕截图.png")

# 红石电路运行时序

![输入图片说明](https://images.gitee.com/uploads/images/2022/0729/102200_29e6ff88_5631341.png "屏幕截图.png")

- FC: fully connect
- UH: upper half
- LH: lower half

1. FC1 UH running 运算全连接第一层上半
2. FC1 LH running 运算全连接第一层下半
3. FC2 UH running 运算全连接第二层上半
4. FC2 LH running 运算全连接第二层下半
5. FC3 running 运算全连接第三层
6. Finished 运行结束

> 写到这里，和第一次进入红石电路，两眼一抹黑已经完全不同了。我刚刚检查了第一层全连接层的下层，活塞门一开一合地运算着... 也许可以交作业啦！ :pray: