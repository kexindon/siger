- [首台可编程忆阻器电脑（转译自网络）](https://zhuanlan.zhihu.com/p/93382384)
- [忆阻器技术引燃模拟革命](https://developer.aliyun.com/article/195960)
- [忆阻器（二）——忆阻模拟器](https://blog.csdn.net/qq_42556934/article/details/113922656)
- [宁波材料所实现全光控忆阻器](https://www.cas.cn/syky/202102/t20210207_4777358.shtml)
- [Adv Funct Materials - 2020 - Hu - All‐Optically Controlled Memristor for Optoelectronic Neuromorphic Computing](https://doi.org/10.1002/adfm.202005582)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0802/132020_88eb392d_5631341.jpeg" title="202AFM-Memristor副本.jpg"></p>

> 当我输入 “ADVANCED FUNCTIONAL MATERIALS” 的时候，搜索引擎的图片结果是满屏的科技感十足的封面，我震撼了，以发表在国外期刊为目标的学习是有道理的。SIGER 期刊封面源有了，各种生物技术，纳米科技，围观世界的封面素材。就好比国家地理就是大片一样。SIGer 努力中，内容素质，封面质量，都有标准拉。

# [首台可编程忆阻器电脑（转译自网络）](https://zhuanlan.zhihu.com/p/93382384)

原作者：Samuel K. Moore

英文原文出处： 国际电气和电子工程协会网站

为寻求低功耗人工智能和神经元计算，很多科学家，初创企业及芯片设计公司都在致力于研究如何把运算从中央处理器的运算中心转移到记忆体内进行运算。忆阻器件和其它存储芯片本身的特性使它们具备担任此项技术的极佳角色。然而，迄今为止绝大多数的存储器内计算都在独立的加速芯片中实现，或者设计是为解决一个特殊类型的人工智能问题，抑或需要存储器外的处理器来运行。密歇根大学的工程师们声称首台完全基于亿阻器件的AI可编程计算机已能够独立运行。

“存储确实是瓶颈，”密歇根大学卢伟教授说道。机器学习模型变得越来越大而我们在芯片上没有足够的存储空间去存下这些内容。“把数据从运算芯片存储到DRAM需要花费100倍于计算的时间和功耗。即便你在运算芯片中有足够的存储空间，在运算核心和其存储区域间传输这些数据也会花费过多的时间和功耗，他说，”但你可以在存储体内进行运算。“

他的实验室一直致力于忆阻器件（或称作RRAM)，即把数据存放成电阻的研究已超过十年。并且展示了其有效执行AI计算（例如乘法和累加运算）在深度学习的核心的潜力和机制。忆阻器阵列可以有效地完成这些任务，因为它们成为模拟计算而非数字计算。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/115026_40b77ab3_5631341.png "屏幕截图.png")

这款新芯片将5,832个忆阻器阵列与开源RISC处理器结合在一起。 486个专门设计的数模转换器，162个模数转换器和两个混合信号接口充当忆阻器模拟计算与主处理器之间的转换器。

“所有功能都在单颗芯片上实现，” 也是电气和电子工程师协会研究员的卢说道。 “要兑现诺言，您不能仅仅建立各个分支部分。”

在最高频率下，该芯片消耗的功率刚刚超过300毫瓦，同时进行每秒1880亿次操作/瓦（GOPS / W）。与英伟达的最新研究型AI加速器芯片（每秒9.09万亿次操作/瓦（TOPS / W））相比，还有些距离，尽管没有考虑功耗成本和从DRAM传输数据的延迟。但卢指出，他的芯片CMOS部分是使用已有20年历史的180纳米半导体工艺制造的。如果将其转移到更新的制程中，例如产生于2008年的40纳米技术，就可以将功耗降至42毫瓦，并将性能提升至1.37 TOPS / W，这还没有考虑英伟达芯片需将数据和DRAM传输需要的额外时间和功耗。而英伟达的芯片采用2014年首次亮相的更先进的16纳米半导体工艺制造。

卢的团队对该芯片进行了三项测试，以证明其可编程性和处理各种机器学习任务的能力。最简单的一种称为感知器，用于对信息进行分类。为此，忆阻器计算机必须识别希腊字母，即使它们的图像分辨率很差。

第二个也是更困难的任务是稀疏编码问题。在稀疏编码中，您正在尝试构建最有效的人工神经元网络，以完成任务。这意味着，随着网络学习其任务，使神经元相互竞争以争夺网络中的位置。失败者被忽视，只有绝对必要的连接留下而使其更像大脑的高效神经网络。 卢于2017年在较小的阵列上展示了基于忆阻器的稀疏编码。

最后的任务是一个双层神经网络，该网络能够进行所谓的无监督学习。该芯片没有进行一系列标签图像的学习，而是分析乳腺X线摄影测试评分。神经网络首先确定得分组合的重要特征是什么，然后以94.6％的准确度将恶性肿瘤与良性肿瘤区分开。

卢预估该芯片的下一个版本将在明年完成，它将具有更快，更高效的CMOS和多个忆阻器阵列。他说：“我们将使用多个阵列来说明您可以将它们绑在一起以形成更大的网络。”

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/115036_1af840c9_5631341.png "屏幕截图.png")

编辑于 2019-11-24 11:47

# [忆阻器技术引燃模拟革命](https://developer.aliyun.com/article/195960)

2017-08-01 1202

### 简介：

我们是否已经准备好为企业数据中心引进基于忆阻器的人工智能基础设施了呢?

我们做事情总是追求更快更智能，这是人的本性。在数据中心里，我们通过机器学习算法将巨大和快速的数据流分层，从而建立独特的业务竞争优势(或更大的社会效益)。

虽然有着令人瞩目的处理能力、性能和容量，今天的数字计算和存储仍然无法与我们的大脑相比，已远超数字架构的6、7或者8个数量级。如果我们想在生物规模和速度方面进行计算，我们必须利用能够远远超越这些严格数字的新形式硬件。

许多以机器学习为基础的应用程序以调查数据的内在模式和行为为基础，然后通过这些情报归类哪些是我们所知道的，并预测接下来会发生的事情，同时识别异常。这类似于我们自己的神经元和突触，从传入的信号中进行学习，将学到的东西保存下来，然后“转发”出去用于做出更明智的决定(或采取行动)。在过去的30年里，AI从业人员已经建立了实用的神经网络和其他类型的机器学习算法用于各种应用程序，但是如今被有限的数字规模(成指数增长的互联网络规模只是其中一个方面)和速度所束缚。

今天的数字计算基础设施以切换数字位为基础，与摩尔定律同步还需克服一些重大障碍。即使有一些量级上的改善，也脱离不了传统数字设计范式，功耗、规模和速度等方面存在固有的限制。不管我们是否将人工智能进化成人形机器人，或者将更大规模的机器学习投入到更大型的数据集来实现更有针对性的广告预算，然而，传统计算基础设施根本就没有足够的原始动力可以达到生物的规模和密度。

最终，动力才是真正的缺陷。显而易见，组件之间的“信息传递”或信号(数据)来回传播是一项重要消耗。在数字设计的基础级别，即使是最小的数据处理任务，CPU之间也会产生大量的IO及其他操作。即使我们增加密度，制作更小的芯片或在CPU附件添加闪存，数字架构之间的比特移动仍将花费很大的精力和时间。在我们的大脑中，内存、存储和加工都是紧密融合的。

与数字系统不同的是，早上我们不需要花百万瓦特的电力起床，因为我们的大脑以低功率的模拟架构运行。而模拟电路，如果就眼下的问题进行自定义构建，可以直接以光的速度解决难题，而不需要大量的指令周期。根据多次的数值输出，它可以计算到任意精度。进一步而言，如果这种电路实现持久性存储，对于远程设备上的存储数字位就不会出现任何令人吃惊的IO等待。

### 向忆阻器问好

当然硅设备基本上是模拟的，但我们已经在上面建立了复杂的数字逻辑门和位存储。但如果我们可以“回到未来”利用今天的硅片密度为模拟计算电路来设计硅设备呢?新的突破利用了新兴的忆阻器设备的模拟属性。

忆阻器是一种可以通过输入的电信号改变其内部电阻的装置，持久性抵制可以测量和用于非易失性内存。忆阻器是一个类似DRAM的快速硅设备——至少比基于NAND的NVRAM快10倍，所以它可以作为主存储器。比如惠普一直在研究一种新的忆阻器技术作为持续的数字存储器，但目前尚未完全推向市场。如果实现的话，我们可能将迎来全新一代整合了存储和内存的数字计算架构。

现在我们看到的是，创业公司Knowm使用了开创性的新形式计算，利用忆阻器技术不仅在快速内存中存储数据，而且从内部预测危急的计算功能，否则需要将存储数据卸载到CPU，再经过加工并写回。Knowm称其利用了小型忆阻器电路的模拟特性——一个具有自适应学习能力的“突触”。它能够直接对传输进来的信号模式进行学习，同时持久保存。

从理论上讲，基于这个基本功能单元，几乎任何类型的机器学习算法可以获得极大的加速。虽然Knowm的发展还处于早期阶段，但已经提供了一个完整的技术堆栈——离散工作的突触芯片、可伸缩的模拟器、低级别的APIs 和更高级的机器学习库，再加上一个服务体系，该体系可以帮助大量的突触分层，直接映射到现有的CMOS设计。

对于AI和Terminator爱好者，Tanjea Group的团队认为破坏的机会远远大于机器学习加速。一个被Knowm称为Neural Processing Unit的新型硬件能够智能地利用模拟硬件功能实现极速、低功耗、高密度和存储融合计算，这是整个计算行业一个真正的重大变化与转折点。不管是谁首先利用这种类型的计算解决方案，都将可能导致大规模的转变，不仅仅是机器学习，还有所有计算的实现方法。



作者：Mike Matchett 翻译：杨旭

来源：51CTO

# [忆阻器（二）——忆阻模拟器](https://blog.csdn.net/qq_42556934/article/details/113922656)

icfg66 于 2021-02-21 19:59:32 发布

除了真实的器件，我们也可以用运放和乘法器来搭忆阻模拟器。这节将介绍数学模型、matlab仿真及multisim仿真。

## 一、数学模型

忆阻分为磁控和荷控两种，这里介绍三次非线性磁控忆阻模型：

$$\begin{cases}
I=W(\varphi)V \\
W(\varphi)=a+3b \varphi ^2\\
\frac{d \varphi }{dt}=V
\end{cases}$$

这里的 $W$ 为忆导。

假设电压为正弦波动 $V(t)=V_m\sin(\omega t)$，积分可得磁通量：

$$\begin{aligned}
\varphi(t)&=\int_{-\infty}^t V(\tau)d\tau=\varphi_0+\int_0^t V_m\sin(\omega \tau)d\tau\\
&=\varphi_0+\frac{V_m}{\omega}[1-\cos(\omega t)]
\end{aligned}$$

带入电流表达式可以得到：

$$\begin{aligned}
I(t)&=\{a+\frac{3b}{\omega^2}[\varphi_0\omega+V_m-V_m\cos(\omega t)]^2\}V_m\sin(\omega t)\\
&=c_1\sin(\omega t)+c_2\sin(2\omega t)+c_3\sin(3\omega t)
\end{aligned}$$

其中 $c_1=(a+3b\varphi_0^2+\frac{6b}{\omega}V_m+\frac{15b}{4\omega^2}V_m^2)V_m$, $c_2=-\frac{3b}{\omega^2}(\varphi_0 \omega+V_m)V_m^2$, $c_3=\frac{3b}{4\omega^2}V_m^3$.

## 二、matlab仿真
假设系统参数 $a=0.6667mS,b=194.4945 S/Wb^2$ ，可以得到不同条件下电流 $I(t)$ 随电压 $V(t)$ 的变化关系：(代码见附录)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/121418_0de3ae17_5631341.png "屏幕截图.png")

## 三、基于运放和乘法器的忆阻模拟器
### 1、原理推导
忆阻模拟器的原理图如下：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/121431_f049da97_5631341.png "屏幕截图.png")

包含3个运放、2个乘法器、1个电容和5个电阻。第一级放大器 $U_1$ 用于避免负载效应；第二级放大器 $U_2$ 与电阻 $R_0$ 和电容 $C_0$ 构成积分器：

$$v_a(t)=v_0(t)=-\frac{1}{R_0C_0}\int_{-infty}^t v(\tau)d \tau =- \frac{1}{R_0C_0}\varphi(t)$$

乘法器 $M_2$ 的输出电压：

$$v_b(t)=g_1g_2[v_a(t)]^2v(t)$$

$g_1,g_2$ 分别表示 $M_1,M_2$ 的增益。

第三级放大器 $U_3$ 与 $R_1,R_2,R_3$ 构成电流反转电路，当 $R_2=R_3$ 时，可以得到：

$$i_1(t)=-\frac{v(t)-v_b(t)}{R_1}=\{-\frac{1}{R_1}+\frac{g_1g_2[v_a(t)]^2}{R_1}\}v(t)$$

最后得到输入端电流：

$$i(t)=\{(\frac{1}{R_{in}}-\frac{1}{R_1})+\frac{g_1g_2[v_a(t)]^2}{R_1}\}v(t)$$

等效忆导的表达式为：

$$W(\varphi)=\frac{1}{R_{in}}-\frac{1}{R_1}+\frac{g_1g_2}{R_1(R_0 C_0)^2}\varphi^2$$

与数学模型对比可以得：

$$\begin{cases}
a= \frac{1}{R_{in}}-\frac{1}{R_1}\\
b=\frac{g_1g_2}{3R_1(R_0 C_0)^2}
\end{cases}$$

### 2、仿真

设定放大器的工作电压为 $±15V$，磁控忆阻的等效参数 $a=0.6667mS,b=194.4945 S/Wb^2$ ，进而得到电路参数：

| 电路参数 | 物理含义 | 参数值 |
|---|---|---|
| $C_0$ | 电容 | 47nF |
| $R_0$ | 电阻 | $8.2k\Omega$ |
| $R_1$ | 电阻 | $750\Omega$ |
| $R_2$ | 电阻 | $1.5k\Omega |
| $R_3,R_4$ | 电阻 | $2k\Omega,2k\Omega$ |
| $g_1$ | 尺度因子($M_1$) | 0.1 |
| $g_2$ | 尺度因子($M_2$) | 1.3 |

multisim搭好电路图：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/115427_22515695_5631341.png "屏幕截图.png")

改变参数，可以得到不同的电流——电压变化图：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/115412_4a068b3e_5631341.png "屏幕截图.png")

这里选取最后一个例子：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/115358_bb8fa00d_5631341.gif "affca12cee604f0f277d1c28b76bf783.gif")

附录

```
% 1.basic parameter
clear;clc;
a = 0.6667 * 10^(-3);
b = 194.4945 ;
% vm = 1,2,3
vm = 1;
% fai0 = 0,-0.5,0.5 m Wb
fai0 = -0* 10^(-3);
% f = 300,600,2000Hz
w = 2*pi*300;
% 2.time
t = linspace(0,2*pi/w,1001);
delta = 2*pi/w/1000;
% 3.it,x,M,V
vt = vm*sin(w*t);
%vt = 2*vm*sin(w*t)+1.5*vm*cos(2*w*t);
N = size(t);N = N(2);
fai =vt;
for k=2:N
    fai(k)=vt(k)+fai(k-1);
end
fai = fai0 + delta *fai;
W = a+ 3*b*fai.^2;
I = W .*vt;
plot(vt,I);

```

[原文链接](http://www.icfgblog.com/index.php/InMemoryComputing/134.html)

 **参考文献** 

[1]包伯成,徐权,包涵 著. 忆阻电路与多稳定性[M]. 北京：科学出版社, 2018

# [宁波材料所实现全光控忆阻器](https://www.cas.cn/syky/202102/t20210207_4777358.shtml)

2021-02-07 来源： 宁波材料技术与工程研究所【字体：大 中 小】语音播报

　　类脑计算直接在硬件上模拟人脑功能，有望实现速度更快、能耗更低、硬件消耗更少的新一代人工智能。忆阻器结构简单，易超高密度集成，是实现类脑计算较为理想的元器件。然而，目前报道的忆阻器，工作机制涉及的离子迁移会改变器件微结构，并需要较高电压或电流来调节电导变化，产生的大量焦耳热进一步加速微结构变化，导致器件稳定性能恶化，难以得到实际应用。

　　近年来，中国科学院宁波材料技术与工程研究所先进纳米材料与器件实验室研究员诸葛飞围绕忆阻器的稳定性问题开展系列研究，包括超低电压忆阻器（Advanced Materials, 2017, 29: 1606927；Applied Physics Letters, 2020, 116: 221602）、纯电子型忆阻器（Applied Physics Letters, 2016, 108: 013504；Applied Physics Letters, 2016, 109: 143505）等，并撰写忆阻器及类脑器件领域综述论文（Advanced Materials Technologies，2019, 4: 1800544；Physica Status Solidi–Rapid Research Letters, 2019, 13: 1900082）。

　　为了从根本上解决忆阻器稳定性问题，诸葛飞和博士研究生胡令祥基于较成熟的氧化物半导体材料研发出全光控忆阻器。仅通过改变入射光信号的波长，就可实现器件电导态的可逆调控，并具有非易失性。电导全光调控可能源于光诱导氧化物界面势垒宽度的可逆变化。在此基础上，通过设计光信号的组合方式，实现了类人脑的脉冲时间依赖可塑性学习。

　　全光控忆阻器工作机制不涉及微结构变化，并且所需光信号的功率密度非常低（～20 μW/cm2或更低），从而为克服忆阻器的稳定性难题提供了全新途径。此外，全光控忆阻器能实现感、存、算一体，可用于构建新一代人工视觉系统。

　　近日，相关研究成果以开放获取的形式发表在Advanced Functional Materials上，并被选为当期封面论文。该成果申请了发明专利。研究工作得到中科院脑科学与智能技术卓越创新中心、国家自然科学基金面上项目、中科院战略性先导科技专项、浙江省自然科学基金重大项目等的资助。

　　[论文链接](https://doi.org/10.1002/adfm.202005582)  https://doi.org/10.1002/adfm.202005582

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/124204_bf6dbd28_5631341.png "屏幕截图.png")

图1.（a）全光控忆阻器工作模式示意图；（b）全光信号调控下的电导可逆转变；（c）类人脑学习规则模拟

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/124212_eb1bb3b9_5631341.png "屏幕截图.png")

图2.封面论文

### [Adv Funct Materials - 2020 - Hu - All‐Optically Controlled Memristor for Optoelectronic Neuromorphic Computing](https://doi.org/10.1002/adfm.202005582)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/131744_15f77b05_5631341.jpeg "Adv Funct Materials - 2020 - Hu - All‐Optically Controlled Memristor for Optoelectronic Neuromorphic Computing-1.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/131756_c663538b_5631341.jpeg "Adv Funct Materials - 2020 - Hu - All‐Optically Controlled Memristor for Optoelectronic Neuromorphic Computing-2.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/131809_08bcef28_5631341.jpeg "Adv Funct Materials - 2020 - Hu - All‐Optically Controlled Memristor for Optoelectronic Neuromorphic Computing-3.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/131822_10f2312a_5631341.jpeg "Adv Funct Materials - 2020 - Hu - All‐Optically Controlled Memristor for Optoelectronic Neuromorphic Computing-4.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/131834_9dcf0996_5631341.jpeg "Adv Funct Materials - 2020 - Hu - All‐Optically Controlled Memristor for Optoelectronic Neuromorphic Computing-5.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/131847_80588064_5631341.jpeg "Adv Funct Materials - 2020 - Hu - All‐Optically Controlled Memristor for Optoelectronic Neuromorphic Computing-6.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/131906_27199a6c_5631341.jpeg "Adv Funct Materials - 2020 - Hu - All‐Optically Controlled Memristor for Optoelectronic Neuromorphic Computing-7.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/131919_2e4a9660_5631341.jpeg "Adv Funct Materials - 2020 - Hu - All‐Optically Controlled Memristor for Optoelectronic Neuromorphic Computing-8.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/131932_df57fc1b_5631341.jpeg "Adv Funct Materials - 2020 - Hu - All‐Optically Controlled Memristor for Optoelectronic Neuromorphic Computing-9.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/131944_50aa5f75_5631341.jpeg "Adv Funct Materials - 2020 - Hu - All‐Optically Controlled Memristor for Optoelectronic Neuromorphic Computing-10.jpg")