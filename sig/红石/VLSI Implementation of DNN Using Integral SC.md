【Minecraft】[基于随机计算的纯红石卷积神经网络——概览](https://gitee.com/flame-ai/siger/blob/master/sig/%E7%BA%A2%E7%9F%B3/%E3%80%90Minecraft%E3%80%91%E5%9F%BA%E4%BA%8E%E9%9A%8F%E6%9C%BA%E8%AE%A1%E7%AE%97%E7%9A%84%E7%BA%AF%E7%BA%A2%E7%9F%B3%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E2%80%94%E2%80%94%E6%A6%82%E8%A7%88.md) 

[3] A. Ardakani, F. Leduc-Primeau, N. Onizawa, T. Hanyu and W. J. Gross, "VLSI Implementation of Deep Neural Network Using Integral Stochastic Computing," in IEEE Transactions on Very Large Scale Integration (VLSI) Systems, vol. 25, no. 10, pp. 2688-2699, Oct. 2017, doi: 10.1109/TVLSI.2017.2654298.

https://www.researchgate.net/publication/282403337_VLSI_Implementation_of_Deep_Neural_Network_Using_Integral_Stochastic_Computing

请参阅本出版物的讨论、统计数据和作者简介： 
https ://www.researchgate.net/publication/282403337

# 使用积分随机的深度神经网络的 VLSI 实现计算

IEEE Transactions on Very Large Scale Integration (VLSI) Systems 中的文章 · 2015 年 9 月

### 5位作者，
包括：

- 阿拉什·阿尔达卡尼
  - 麦吉尔大学
  - 28种出版物 424次引用

- 鬼泽直也
  - 东北大学
  - 100 种出版物 994次引用

- 弗朗索瓦·勒杜克‑普里莫
  - 蒙特利尔理工学院
  - 53种出版物 456次引用

### 作者也从事这些相关项目：
- Turbo 解码器的 VLSI 实现查看项目
- Brainware LSI (BLSI) 项目查看项目
  > Goal: We are designing specialized VLSIs for brainwave (brain-inspired) computers.

# 深度神经网络的 VLSI 实现使用积分随机计算

- Arash Ardakani、François Leduc‑Primeau、Naoya Onizawa ，IEEE 成员,
- Takahiro Hanyu， IEEE 高级会员
- Warren J. Gross， IEEE 高级会员

### 摘要 

深度神经网络（DNN）的硬件实现最近受到了极大的关注：许多应用实际上需要适合硬件实现的高速运算。然而，通常需要大量的元件和复杂的互连，导致大面积的占用和大量的功耗。尽管现有的随机算法需要导致长延迟的长流，但随机计算已经显示出低功耗、面积高效的硬件实现的有希望的结果。在本文中，我们提出了一种整数形式的随机计算并介绍了一些基本电路。然后，我们提出了一种基于积分随机计算的 DNN 的有效实现。所提出的架构使用整数随机流和改进的基于有限状态机的tanh函数来执行计算，与传统的随机计算相比，甚至可以减少延迟。所提出的架构已在 Virtex7 FPGA 上实现，与文献中报道的最佳架构相比，面积和延迟平均减少了 44.96% 和 62.36%。我们还以 65 nm CMOS 技术合成电路，并表明当允许发生时序违规时，它们可以在某些计算中容忍高达 20% 的故障率，从而节省功耗。所提出架构的容错特性使其适用于固有不可靠的先进工艺技术，例如忆阻器技术。

### 索引词 

深度神经网络、机器学习、硬件实现、积分随机计算、模式识别、超大规模集成 (VLSI)。

## 一、引言

最近，受生物启发的人工神经网络（例如受限玻尔兹曼机（RBM））的实现因其在解决复杂函数方面的高性能而引起了极大的兴趣。各种应用程序都可以从中受益，尤其是机器学习算法。它们可以分为两个阶段，称为学习和推理阶段 [1]。学习引擎找到合适的配置将学习输入数据映射到所需的输出，而推理引擎使用提取的配置来计算新数据的输出。

<p><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/033115_733646b5_5631341.png"><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/070630_6094df88_5631341.png"></p>

Fig. 1. A N-layer DBN where W and N denote the weights of each layer and number of layers respectively.
> 图 1. N 层 DBN，其中W和N分别表示每层的权重和层数。


深度神经网络，尤其是深度信念网络 (DBN)，已经在各种计算机视觉和识别任务 [2]‑[7] 上展示了最先进的结果。 DBN 可以通过将 RBM 相互堆叠以构建深度网络来形成，如图 1 [3] 所示。 DBN 中使用的 RBM 使用基于梯度的对比发散 (GCD) 算法进行预训练，然后使用梯度下降和反向传播算法对结果进行分类和微调 [3]、[4]。在过去的几年里，通用处理器已经主要用于DBN的训练和推理引擎的软件实现。然而，大功率消耗和高资源利用率是这种方法的主要缺点。训练过程在配备图形处理单元 (GPU) 的云服务器上执行一次，提取的权重存储在内存中以供推理引擎使用。这主要是由于许多应用程序需要实时计算，然而，集中的方法，即来回发送数据到云服务器是耗时的。请注意，训练和推理计算都是在集中式方法的云服务器上执行的。因此，与基于软件的实现对应物相比，推理引擎的硬件实现是一种很有前途的方法。

DBN 由多层 RBM 和最后的分类层构成。主计算内核由数百个向量矩阵乘法和每一层中的非线性函数组成。由于在硬件中实现乘法的成本很高，因此这种网络的现有并行或半并行 VLSI 实现会受到高硅面积和功耗的影响。非线性函数也使用查找表 (LUT) 实现，需要非常大的内存。此外，该网络的硬件实现导致硅面积大：这是由层之间的连接引起的，导致严重的路由拥塞。因此，一个有效的 VLSI 实现DBN 仍然是一个悬而未决的问题。

最近，随机计算 (SC) 在各种应用程序的超低成本和容错硬件实现方面显示出可喜的结果 [8]‑[10]。例如，使用单极 SC，乘法和加法分别使用与门和多路复用器来实现 [11]、[12]。

然而，基于多路复用器的加法器引入了一个缩放因子，可能导致精度损失[13]，导致深度神经网络的 SC 失败，这需要许多加法。如果输入值很小，或门可以提供一个很好的加法近似值 [12]。然而，与其定点硬件实现相比，使用或门在 DBN 中执行加法会导致巨大的错误分类错误。因此，仍然缺少保持 DBN 性能的有效随机实现。

本文引入积分随机计算来解决传统比例加法器的精度损失问题。在这种计算形式中，利用树加法器在随机域中执行加法，而不会造成任何精度损失。还值得一提的是，与传统的二进制随机计算相比，所提出的技术具有更低的延迟。然后提出了一种新颖的基于有限状态机 (FSM) 的 tanh函数作为 DBN 中使用的非线性函数。最后，提出了一种基于上述技术的DBN 的有效随机实现，具有可接受的误分类误差，与最先进的随机架构相比，平均面积减少了 44.96%。

纳米级存储器电阻器（忆阻器）器件是一种非易失性数字存储器，与CMOS 相比，它消耗的能量要少得多，并且可以缩放到 10 nm 以下的尺寸 [14]。忆阻器设备的一个具有挑战性的问题是存在显着的随机变化，这使得该技术不适合确定性计算。处理忆阻器的不确定性的一种有前途的方法是设计容错的 SC 系统 [14]。

在本文中，我们表明，当允许发生时序违规时，所提出的架构可以容忍高达 20% 的故障率，使其适用于忆阻器设备。

本文的其余部分安排如下。第二部分对 SC 及其计算元素进行了回顾。第三节介绍了该领域中提出的积分随机计算和操作。第四节描述了DBN 的整体随机实现。第五节提供了所提出架构的实现结果。在本节中，研究了当电路受到时序违规影响时随机实现的性能。请注意，接受偶尔的时序违规可以降低电源电压，从而提高系统的能效。在第六节中，我们总结了论文并讨论了未来的研究。

## 二、随机计算及其计算元素

<p><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/071057_c4d2186c_5631341.png"><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/071021_ba9f9e7e_5631341.png"></p>

Fig. 2. Stochastic multiplications using (a) AND gate in unipolar format and (b) XNOR gate in bipolar format
> 图 2. 使用 (a)单极格式的与门和 (b)双极格式的异或门进行随机乘法


在随机计算中，数字表示为随机位的序列。序列的信息内容不依赖于每个比特的特定值，而是根据他们的统计数据。让我们用X ∈ {0, 1}表示随机序列中的一点。为了表示一个实数x ∈ [0, 1]，我们简单地生成这样的序列：

E[X] = x, (1)

其中E[X]表示随机变量 X 的期望值。这被称为单极格式。双极格式是另一种常用的格式，其中x ∈ [−1, 1] 由设置表示：

E[X] = (x + 1)/2。 (2)

请注意，任何实数都可以通过按比例缩小以适合适当的间隔以这两种格式之一表示。在本文中，我们使用大写字母表示随机流的元素，而小写字母表示与该流相关的实际值。

### A. SC 中的乘法

两个随机流的乘法分别使用单极和双极编码格式的AND和XNOR门执行，如图 2(a) 和 2(b) 所示。在单极格式中， A和B的两个输入随机流的乘法计算如下：

Y = AND (A, B) = A·B,  (3)

如果输入序列是独立的，我们有：

y = E[Y ] = a × b。 (4)

双极格式的乘法可以执行为：

Y = XNOR(A, B) = OR(A · B,(1 ‑ A) · (1 ‑ B)),  (5)

E[Y ] = E[A · B] + E[(1 ‑ A) · (1 ‑ B)]。 (6)

如果输入流是独立的，

E[Y ] = E[A] × E[B] + E[1 ‑ A] × E[1 ‑ B]。 (7)

通过简化上述方程，我们有：

y = 2E[Y ] ‑ 1 = (2E[A] ‑ 1) × (2E[B] ‑ 1)。 (8)

<p><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/071448_e23581d8_5631341.png"><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/071412_d8ee9257_5631341.png"></p>

Fig. 3. Stochastic additions using (a) MUX and (b) OR gate
> 图 3. 使用 (a) MUX 和 (b) OR门的随机加法

### B. SC 中的加法

SC 中的加法通常通过使用缩放加法器或OR门 [11]、[12] 来执行。缩放加法器使用多路复用器 (MUX) 来执行加法。 MUX Y的输出由下式给出

Y = A · S + B · (1 ‑ S)。 (9)

As a result, the output of MUX would be (E[A] + E[B])/2 when the select signal S is a stochastic stream with probability of 0.5, as illustrated in Fig. 3(a).这个 2 输入缩放加法器通过将其缩小 2 倍来确保其输出在每种编码格式的合法范围内。因此，可以通过使用多个 2 输入 MUX 的树来执行 L 输入加法。通常， L 输入缩放加法器的结果按比例缩小L倍，这会降低流的精度。为了达到所需的精度，必须使用更长的比特流，从而导致更大的延迟。

OR门也可以用作近似加法器，如图3（b）所示。具有输入A、 B的OR门的输出Y可以表示为

Y = A + B ‑ A · B。 (10)

仅当E[AB] ≪ 1 且E[A] + E[B] < 1 时，或门才能用作加法器。因此，应首先按比例缩小输入以确保满足上述条件。这种类型的加法器仍然需要长比特流来克服由缩放因子引起的精度损失。

为了克服这种可能导致结果不准确的信息丢失，[13] 中提出了累积并行计数器（APC）。 APC 将N个并行位作为输入，并在系统的每个时钟周期将它们添加到计数器中。因此，该加法器保留了所有信息，并由于其总和的小方差而导致较低的延迟。还值得一提的是，这个加法器将随机流转换为二进制形式 [13]。因此，此加法器仅限于执行加法以获得最终结果或需要二进制格式的中间结果的情况。

### C. SC 中基于 FSM 的函数

双曲正切和指数函数是许多应用程序所需的计算。这些功能是通过使用 FSM [15] 在随机域中实现。

<p><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/072515_c0dd0212_5631341.png"><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/072444_66a26680_5631341.png"></p>

Fig. 4. State transition diagram of the FSM implementing (a) tanh and (b) exponentiation functions
> 图 4. FSM 实现 (a) tanh 和 (b) 取幂函数的状态转换图

<p><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/072542_d8f34bd2_5631341.png"><img width="49%" src="https://images.gitee.com/uploads/images/2022/0727/072614_8bce6b2b_5631341.png"></p>

Fig. 5. (a) Stochastic representations of 0.75 and (b) Integer stochastic representation of 1.5
> 图 5. (a) 0.75 的随机表示和 (b) 1.5 的整数随机表示

图 4(a) 和 4(b) 显示了 FSM 实现 tanh 和取幂函数的状态转换图，其中n表示FSM 中的状态数， G表示取幂函数的线性增益， Y表示随机输出序列（另见等式（11）和（12））。让我们将随机域中 tanh 和 exp 的近似函数定义为 Stanh 和Sexp。值得一提的是，Stanh 函数的输入和输出都是双极格式，而 Sexp 函数的输入和输出分别是双极和单极格式。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0727/072408_c34eae94_5631341.png "屏幕截图.png")

...