# [B站大佬用我的世界搞出卷积神经网络，LeCun转发！爆肝6个月，播放破百万](https://mp.weixin.qq.com/s/AArfI_Nb0ckU4hbUsrs6wQ)

关注前沿科技 量子位 2022-07-05 14:44 发表于北京

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154106_c3049290_5631341.png "屏幕截图.png")

- 微信号：量子位 QbitAI
- 功能介绍：追踪人工智能新趋势，关注科技行业新突破

丰色 发自 凹非寺
> 量子位 | 公众号 QbitAI

 **非计算机专业、神经网络小白，** 爆肝六个月——

在《我的世界》里搭建一个卷积神经网络，这是什么神操作？

![输入图片说明](640%20(34).gif)

最近，来自B站up主 @辰占鳌头 的作品着实火了一把。

他和朋友们合作完成的这个号称“世界首个纯红石神经网络”，神经元、卷积层、全连接层、激活函数、乘法器、输入、输出……样样俱全、蔚为壮观，而且可以真的 **实现手写数字识别，准确率还达到了80%** 。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154345_79531848_5631341.gif "640 (35).gif")

这波，妥妥就是如网友所说：

> 简直是实力与耐心做出来的超凡成果。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154401_2f98c673_5631341.png "屏幕截图.png")

目前，这个视频的播放量已经上了一百万，在外网，它甚至还引起了 **LeCun** 的注意：不仅转发了他们的作品，还给出了“Very meta”的评价。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154429_96183fb4_5631341.png "屏幕截图.png")

### 耗时6个月，用红石搭建卷积神经网络

红石是我的世界里一种可以传递信号的矿石资源，可以用来制作红石电路，进而完成小到如自动门、光开关、频闪电源的简单机械，大到电梯、自动农场、盾构机、小游戏平台甚至计算机的复杂工具。

本次作者们搭建的这个神经网络的架构，基于LeCun于1998年提出的经典卷积结构LeNet-5，它就是用来实现手写数字识别的。

相比传统的全精度计算（ **乘法器和加法器** ），作者经过一番思考和估算，决定采用随机计算的方式来实现这个神经网络，这样可以让设计和布局都简单一些。

毕竟对于用随机计算实现乘法来说，只需一个与门就可以表示单极，一个同或就能表示双极。

由于在我的世界中进行反向传播不太可能，网络的权重都是先在Pytorch中训练好，然后直接搬进去。

为了生成由随机串组成的权重，作者利用“投掷器投掷物品是随机的”这一原理造了一个随机数生成器。

总的来说，他们采用的是一个压缩的LeNet-5，先使用一个带权重的窗口（ **卷积核** ）逐次扫描图像并提取笔画特征，然后将这些笔画特征馈入到深度神经网络（ **全连接层** ）进行分类识别。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154506_ed75ec76_5631341.png "屏幕截图.png")

具体来说：

首先由输入设备：一个单脉冲式压力板手写板和15×15坐标屏，产生坐标信号，并在屏幕上绘制出笔迹。

![输入图片说明](640%20(36).gif)

然后手写数字进入卷积层，累加卷积核被遮盖的部分，并将结果输出到下一层。

其中：

（1）在卷积层，作者没有使用随机计算，而是使用我的世界中的模拟信号进行加法运算；

（2）为了保证输入数据可以非线性地映射到高维度特征空间然后进行线性分类，输出经过了激活函数ReLU；

（3）由于卷积无法随意移动，所以采用直接堆叠的方式，再通过硬连线连接到手写板输入上。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154558_971d3d6b_5631341.png "屏幕截图.png")

随后，是全连接层。每层由若干神经网络构成，每个神经元都连接多个输入，并产生一个输出。神经元将每个输入加权累加，然后带入一个激活函数输出。

全连接层使用的是随机计算，

激活函数用的是非线性的tanh。

实际的神经元电路如下：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154608_55afc679_5631341.png "屏幕截图.png")

最后一层的输出使用一个模电计数器，用来统计5Hz串中“1”的数量，容量则为1024。

最终，输出部分，计数器的高4位被连接到计数板上，然后电路选取最大的值并在面板上显示结果。



![输入图片说明](640%20(37).gif)

结构总览：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154654_36961f1d_5631341.png "屏幕截图.png")

网络架构总览：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154701_89f98f53_5631341.png "屏幕截图.png")

作者介绍，该神经网络在MNIST数据集上约实现了 **80%** 的准确率，作为对比， **同权重的全精度网络的准确率为88%** 。

另外，它的单次理论识别时间约为5分钟，但没想到Minecraft的运算能力实在有限——在实际测试中，可能要40分钟以上。

由此作者得出，Minecraft随机计算神经网络在时间开销上未必优于全精度网络。不过全精度网络目前还没有人做出来。

### “工作量和难度都很大”

在这个作品的评论区，都是一水儿的称赞有加与膜拜（ **连大V籽岷都冒泡了** ）——

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154746_7eac7a79_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154752_b844d5b3_5631341.png "屏幕截图.png")

看完大佬的巨作，有网友甚至开始怀疑自己和up主们玩的不是一个游戏。（手动狗头）

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154803_d2728a12_5631341.png "屏幕截图.png")

还有人指出，别看最终实现的功能只是相当于机器学习中的“hello world”，但用游戏中提供的红石元件复现出来，可以说是“工作量和难度都很大”。

因为“这要求作者对算法的底层实现或者硬件执行原理（ **类似cuda编程** ）有深刻的理解，还能使用游戏机制来优化执行过程以及完成并行计算”。

虽然最终识别速度比较慢，但“在这里探讨效率的意义不大”。

言外之意，它本身就很有价值，“我们不能拿cpu的两个线程经过无数层模拟器嵌套后的识别效率去跟显卡识别效率比较”。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154831_42caf714_5631341.png "屏幕截图.png")

最后，还有人感叹：好了，现在红石神经网络有了，硬盘、CPU、显示器也早就有了，红石超级电脑是不是也不远了？

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154838_5ca7cf82_5631341.png "屏幕截图.png")

“说不定到时我们就能在MC里玩MC了～”

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/154845_1b99e5fb_5631341.png "屏幕截图.png")

### 关于作者

这个红石卷积神经网络一共有5位作者，up主@辰占鳌头是主要贡献者，负责电路的总体设计、搭建和调试。

他和另外一位作者@学杂不善（GitHub@leamoon）都是 **香港科技大学** 的学生，现在分别攻读 **理论物理学博士和电子工程学博士** 。

其他3位合作者中，有一位高中刚毕业（@NKID00），另外两位（@enadixxoOxoxO和@爱红石的小章鱼）没有透露自己的身份。

@辰占鳌头 在2014年的时候就开始接触《我的世界》这款游戏了，曾做过一个编码汉字的显示器和一个8位的CPU。



![输入图片说明](640%20(38).gif)



问及为什么一个非计算机专业的学生想要搭建一个神经网络时，他告诉我们，其实自己参加过信息竞赛，由于之前接触过随机计算（ **stochastic computing** ），最初是想展示一下随机计算在特定任务下的优越性所以开始了这个项目。

最终为了搭建完整的神经网络，他看了50+页英文文献，写了1000多行代码。

要说最难/耗时最久的部分，他回答：是 **全连接层调试** ，因为运行速度很慢而且使用随机计算很难找到问题所在。

对于未来，up主表示，他正在考虑搭建一台 **支持RISC-V指令集的红石CPU** 。

查了一下，目前似乎还没人做出来，值得期待～

- **B站视频：**  https://www.bilibili.com/video/BV1yv4y1u7ZX
- **原理介绍：**  https://www.bilibili.com/video/BV1wF411F7PU

— 完 —