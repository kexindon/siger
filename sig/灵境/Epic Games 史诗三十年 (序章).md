[虚幻宇宙创造者——Epic Games 史诗三十年 (序章)](https://zhuanlan.zhihu.com/p/450175393)
- [1991 年前：Epic 第一款编辑器，不是 Unreal](https://zhuanlan.zhihu.com/p/450175671)
- [1991-1997：Epic 1.0 时期，Doom 逼出虚幻引擎](https://zhuanlan.zhihu.com/p/452939468)
- [1998-2011：Epic 2.0 与 3.0 时期：祭出战争机器，Epic 加入主机战争](https://zhuanlan.zhihu.com/p/455291207)
- [2012-2018：Epic 4.0 时期：堡垒之夜，意料之外的必然](https://zhuanlan.zhihu.com/p/458357432)
- [2019 至今：(终篇)：打造元宇宙](https://zhuanlan.zhihu.com/p/464363863)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0708/031815_4b64d2ab_5631341.jpeg" title="EPICGAMES.jpg"></p>

# [虚幻宇宙创造者——Epic Games 史诗三十年 (序章)](https://zhuanlan.zhihu.com/p/450175393)

[Marc](https://www.zhihu.com/people/yanzhenzhong)
​- 腾讯 商业分析

2021 年是 Epic Games 公司成立 30 周年，在科技产业，这足以称得上是一家“老”公司。但这家公司为更广泛的科技圈以及大众所认知，好像也就只在最近几年。在过去一段时间里，元宇宙 (metaverse) 概念兴起，Epic 凭借虚幻引擎 (Unreal Engine, UE) 和社交游戏堡垒之夜 (Fortnite: Battle Royale)，从一家游戏开发商的定位，跃升到了离元宇宙最近的少数几家公司之一。

玩家群体对 Epic Games 当然不陌生，不少 3A 级的游戏都通过虚幻引擎开发。对于海外玩家来说，Epic 旗下的堡垒之夜最高月活跃用户在 2020 年超过了 8000 万，是海外最受欢迎的竞技类游戏。在过去几年，美国的中学和大学举办电竞，最多人参与的项目必然是堡垒之夜。对于中国玩家来说，备受关注的黑神话：悟空就是使用 UE5 开发，并且得到了 Epic 官方的技术支持。Epic Games 也在不同场合秀出 UE5 的“肌肉”，包括在 PS5 主机上的实机展示，最近也有黑客帝国：觉醒的 demo 供玩家体验。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/020202_2048785e_5631341.png "屏幕截图.png")

> 黑客帝国：觉醒 demo https://www.bilibili.com/video/BV1rY411p7Tg


在相当一段时间以来，Epic 是我最感兴趣的公司之一。我在美国读书的时候，学校离 Epic 总部只有 20 分钟车程。Epic 所在的北卡州 Cary 市是一个富人小镇，也是一个 foodie town。虽然我也偶尔去 Cary 饕餮一番，但却没有机会走近 Epic 一睹真容。但因为离得不远，也结识了几个在 Epic 工作的朋友。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/020217_9f5e4328_5631341.png "屏幕截图.png")

> Epic Games 总部办公楼，位于 Cary 市中心的新总部地址仍在建造中


Epic 神秘吗？好像也不是那么神秘。创始人 Tim Sweeney 在社交网络上活跃，过去二十多年间也偶有对 Epic 核心人员的访谈。但进一步挖掘会发现，完整描述 Epic 这家公司的文章几乎没有，很多是零散的片段和侧写。原因我猜是这样的：

- Epic 太复杂，作为游戏厂商和技术公司一体两面，不太好进行定义
- Epic 太“老”：老本身不是问题，但由于经历了几个截然不同的发展阶段，Epic 不像是一家游戏厂商般“简单”
- 缺少 defining franchise：尽管屡出佳作，但和使命召唤之于动视，以及任天堂和暴雪多部经典，明显逊色一些
- 缺少故事性：和 John Carmack 以及任天堂一系相比，创始人的传奇性欠奉，公司发展过程也没有育碧和维旺迪斗智斗勇的商战画面
- 而对国内来说，Epic 和中国的关联较弱，堡垒之夜至今没有版号，其他作品在国内影响力弱，即便是现在都在谈论的虚幻引擎，只要找回三四年前的 UE vs. Unity 线上讨论，劝退 UE 的论述仍占主流


重新回顾历史，这家公司取得的成就令人敬佩，足以、也值得为此留下笔迹。恰逢 Epic 成立三十周年的契机，也是元宇宙成为热门话题的今天，我希望能够根据自己对 Epic 的研究和理解，展现这家公司三十年的发展历程、重大转变和背后的哲学与思考。

作为一家已经成立三十年的公司，Epic 有不少故事可以被分享，怎么组织内容是一个小难题。幸好，Tim Sweeney 曾经以 1.0、2.0、3.0 和 4.0 来标记 Epic 经历过的几次公司转型。比如他曾提到，Epic 在 2012 年的转型是 Epic 4.0 的开始，以成立 20 年来首次对外进行战略融资 (腾讯)，作为公司向 F2P (free-to-play) 游戏发展的标记。以此为准绳，我也把 Epic 的发展相应地分成几个阶段，每篇文章介绍其中一个时期。

- 1990 年以前：讲述 Tim Sweeney 的成长经历以及这些经历是怎样影响了日后的 Epic Games
- 1991-1997 / Epic 1.0 时期：团队组建和起步期的发展，开发虚幻引擎
- 1998-2005 / Epic 2.0 时期：虚幻，主机战争的机会
- 2006-2011 / Epic 3.0 时期：商业模式的争论与转型阵痛
- 2012-2018 / Epic 4.0 时期：转型 F2P 和移动端机会的挖掘
- 2018 年后：Epic 所看到的的 Metaverse 和相关布局


第一到第五部分，我会编年体的方式讲述 Epic 各个阶段的发展历程，展开关键事件和背后的思考。第六部分会结合我对 Epic 的产品、技术和投资并购情况，尝试反推 Epic 元宇宙的逻辑和布局打法。

一旦了解 Epic 的发展史和 Tim Sweeney 其人，就不难理解 Epic 数次主动转型的原因和 Tim 的技术信仰，也能够对 Tim 建立开放平台、鼓励 UGC 创作的态度有更准确的判断。

参考资料方面，我主要参照了互联网上能搜索到的 Epic 相关人士的访谈和公司事件记载，也在一些书籍中有零星的摘录，无奈材料比较零散且有材料之间存在相互矛盾 (好在都不是节骨眼的事情)，我会根据多份材料相互印证以及我个人的认知来进行判断。如有疏漏也请不吝指正。


 _Epic 创始团队之一，战争机器创意总监 Cliff "CliffyB" Bleszinski 在 2022 年 7 月会出版他的回忆录《Control Freak: My Epic Adventure Making Video Games》，到时候会有 Epic Games 前二十年更详实的材料，非常期待_ 

发布于 2021-12-27 07:34

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/020114_e5c1d0ba_5631341.png "屏幕截图.png")

---

【[笔记](https://gitee.com/yuandj/siger/issues/I4UEVV)】EPICGAMES - 元宇宙史诗 SIGGRAPH 

- [如何构建开放“元宇宙”，你想知道的一切 | SIGGRAPH 2021 研讨会](https://gitee.com/yuandj/siger/issues/I4UEVV#note_11168183_link)
- [Epic Games 史诗三十年 (终篇)：打造元宇宙](https://gitee.com/yuandj/siger/issues/I4UEVV#note_11486319_link)

<p><img width="26%" src="https://images.gitee.com/uploads/images/2022/0708/022656_ffad8c20_5631341.jpeg"> <img width="26%" src="https://images.gitee.com/uploads/images/2022/0708/022906_d171945a_5631341.jpeg"> <img width="29.2%" src="https://images.gitee.com/uploads/images/2022/0708/023804_0e2f12f6_5631341.jpeg"> <img width="14.6%" src="https://images.gitee.com/uploads/images/2022/0708/023817_7abfa185_5631341.jpeg"></p>
<p><img width="29.6%" src="https://images.gitee.com/uploads/images/2022/0708/024500_93642da3_5631341.jpeg"> <img width="35%" src="https://images.gitee.com/uploads/images/2022/0708/024526_5976518b_5631341.jpeg"> <img width="32.9%" src="https://images.gitee.com/uploads/images/2022/0708/024539_2625f57d_5631341.jpeg"></p>