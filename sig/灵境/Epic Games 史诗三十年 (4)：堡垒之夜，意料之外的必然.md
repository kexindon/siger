![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/003342_289821b0_5631341.png "屏幕截图.png")

# [Epic Games 史诗三十年 (4)：堡垒之夜，意料之外的必然](https://zhuanlan.zhihu.com/p/458357432)

[Marc](https://www.zhihu.com/people/yanzhenzhong)
​- 腾讯 商业分析
​
Epic Games 史诗三十年系列共五篇，本篇是第四篇：

- 1991 年前：Tim Sweeney 成长经历以及 Epic 的创立
- 1991-1997：Epic 1.0 时期，开发虚幻与虚幻引擎
- 1998-2011：Epic 2.0 与 3.0 时期，Epic 的 3A 游戏战争机器
-  **2012-2018：Epic 4.0 时期：转型 F2P 和堡垒之夜** 
- 2019 至今：Epic 所看到的 Metaverse 和相关布局

### 3A 困局


随着战争机器的成功，Epic 奠定了在主机 3A 游戏中的地位，也获得了超过一亿美元的收入。在 Epic 和微软的合作上，作为发行商 (publisher) 的微软主要负责市场推广，承担了大部分成本，研发商 (developer) Epic 一侧的成本只有 1200 万美元，一来一回，战争机器项目的利润率达到了惊人的 88%。


在当时，主机的商业模式看似很美好。研发商专心进行游戏开发，发行商负责市场宣发和渠道。这时候的发行商和研发商还是合作和分工的关系。但随着行业发展，作为链条中体量最大的发行商愈加强势，侵入到行业上游。这时候发行商不仅能给研发商的项目提出建议，甚至逐渐获得了项目创意决策权、IP 所有权，也出现了发行商带着 IP 去找研发商进行开发的情况，研发商更类似一个落地实现的代工方角色。而发行商获得资金-开发项目-回笼资金，体量做大后的发行商通过上市进一步打开资金来源，长此往复。


另一边厢的研发商，尽管游戏属于软件开发，但单机游戏还是依靠人力堆砌，多开一个产品项目就要多一个团队，而主机 3A 游戏由于有对内容量和品质的高要求，也很难通过天才创意做出来低成本爆款。总结来说就是，主机 3A 无法产生规模效应，边际收益处于递减状态。


更让开发商难受的是，随着技术不断迭代，对游戏品质的要求也更加高，同等体量的游戏项目，团队人员规模线性增加。2006 年推出的战争机器，开发团队规模在 110 人左右 (Epic 约 80 人，合同工约 30 人)。而到了 2008 年的战争机器 2，开发人员已经达到了 200+，而且 Xbox 也投入了过百人进行本地化和测试等工作，总人数超过 300。


但问题是，战争机器续作销量并没有在首作基础上增加，维持在 500 万份量级。但项目成本却因为团队规模不断扩大而大幅增加，导致单项目利润率下降。战争机器上的利润率从首作的 88%，急剧下降到 30-40%。Epic 意识到了问题所在——主机 3A 生态中，研发商天花板非常有限。


进入新世纪的美国，宽带网络逐渐普及。有前瞻性的游戏厂商开始布局联机和在线游戏。2003 年，Valve 通过推出反恐精英 1.6 版本 (CS 1.6) 和游戏平台 Steam，并且要求玩家必须要登录 Steam 才可以启动 CS 1.6。Valve 捆绑拳头产品强推平台，在当年产生了巨大争议。但历史证明了 Gabe Newell (Valve 创始人) 的超前眼光——2008 年，美国家庭宽带网络接入率突破 50%，Steam 用户数量也突破了 1300 万。2009 年，英雄联盟 (League of Legends) 推出，并走上了其成为全球第一电竞游戏之路。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/003521_24e7b04c_5631341.png "屏幕截图.png")

> 2009 年的英雄联盟

另外一方面，3G 网络和移动端应用形态也带来了移动互联网生态，启动了一个未来全球规模将达到千亿美元级别的手游市场。


Tim 观察到了网络设施进步，以及网络游戏平台和手游崛起带来的变化：

- 研发商可以通过网络分发内容，降低发行商和实体渠道商的影响，甚至实现自研自发
- Steam Early Access 等预购功能能够帮助研发商提前回收资金，缓解了资金压力
- 在线/联机竞技游戏能够通过一次发行，不断优化迭代和为玩家提供服务来产生长期价值 (不像 3A 单机游戏每一作的团队规模都需要扩大，扩大营收需要赌新 IP)
- DotA 和英雄联盟等游戏验证了免费网游 (F2P) 可以通过皮肤和饰品等方式，在不影响公平竞技的情况下实现商业化，提升了玩家平均生命周期
- 移动端潜在用户数以十亿计在，移动游戏也将会是比 PC 游戏和主机大数倍的市场


看到巨大市场机会的 Epic 希望能在战争机器上探索新方向，并尝试和微软协商，但收效甚微。反而在过程中发生的一系列事件，加剧了 Epic 对于主机 3A 游戏商业模式限制的感受。


首先是发行商和开发商之间不对等关系。战争机器的外传作品战争机器：审判 (Gears of War: Judgement) 推出之后，玩家社区对其多人模式的一些关卡提出了反馈意见，Epic 在内部分析之后认为需要重做这些关卡。Tim 认为，这和当年 Epic 推出虚幻后发现的问题，由推出虚幻竞技场来修复和完善，是同样性质。但微软驳回了这个提案，因为这不在发行商的产品规划以内。后来 Epic 提出在战争机器 3 立项阶段提出将为游戏推出一个纯多人联机游戏版本，也被微软拒绝。


另外是主机游戏宣发销售模式的局限性。主机大作销售讲究节点，比如在美国节日季 (holiday season，一般是感恩节到新年这一段公众假期比较集中的时间) 就是游戏销售旺季，也需要广告和渠道铺设配合。微软希望旗下两大枪战射击游戏光环和战争机器能够交替在 2006 年到 2010 年推出，也不会影响彼此销量。但后来光环出现变动，一个大项目被取消，人员被释放去开发光环 3 资料片 ODST 并安排在 2009 年推出，而原定在 2009 年发售的光环：致远星 (Halo: Reach) 则推迟到 2010 年，和战争机器 3 撞车。Epic 认为战争机器对微软来说重要性肯定不如作为“亲儿子”的光环，无法与之竞争微软的推广资源，而被迫把战争机器推迟到 2011 年推出。


最后，严格的开发周期也是束缚。由于 Epic 需要每两年推出一部战争机器，以匹配微软的排期。项目组有一些好创意因为严格限定的开发周期而来不及落地。因此每次只能实现部分项目组的想法。但这些好的创意也并不一定能在下一代作品中实现——由于技术的进步对品质的要求进一步提升，同一个 IP 的续作并非在前作基础上迭代，而是作为新项目几乎从零开始。前作更多是经验积累，做不到工程大量复用。


战争机器 3 推出后，反响依然热烈——首周销量超过 300 万份，成为 Xbox360 平台上首周销量最佳的游戏。一边厢，战争机器开发团队尽情庆祝；但另一边厢，Tim 正在为 Epic 的未来忧虑。这时候游戏行业马上迎来下一世代的主机平台，对游戏品质的要求会进一步提高，但也这将进一步压缩 Epic 的利润率。


Tim 决定做出改变。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/003618_3aad835c_5631341.png "屏幕截图.png")

> 战争机器 3


### 破釜沉舟


实际上，Epic 的转变从移动端开始。看到 iPhone 的成功和潜力后，Epic 就开始着手开发能够适配移动端的虚幻引擎。适用于 iOS 的 UE3 在 2008 年年初大致完成。Epic 进一步收购了位于美国犹他州的精品工作室 Chair Entertainment，让 Chair 为 Epic 开发一款使用 UE3 的手游，作为虚幻引擎进入手游市场最好的演示。


Chair 不负众望，12 人的团队在 Epic 支持下，只花了五个月就完成了无尽之剑 (Infinity Blade) 的开发。无尽之剑在 2010 年 9 月的苹果发布会上以 Project Sword 为名首次对外展示，也是移动端上首次有游戏展现高质量 3D 画面。无尽之剑在 2010 年 12 月 9 日正式推出，到 2011 年底的约一年时间内，创造了 2300 万美元流水。Epic 也在借此推出了 UE 移动端技术 demo Citadel 和 SDK，吸引移动端开发者群体。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/003634_6c232ef3_5631341.png "屏幕截图.png")

> Epic 在苹果发布会上展示 Project Sword


在游戏开发方面，尽管 Tim 已经敏锐地捕捉了市场的转变，但 Epic 从 3A 到 F2P 的转型之路并不好走。其中最难改变的事情之一，是人的惯性和现实营收数字。


从公司经营角度考虑，战争机器系列仍能够持续为 Epic 带来可观的收入，战争机器 IP 也是公司的重要资产，不会轻易雪藏。因此在战争机器 3 开发完成后，CliffyB 和 Rod Fergusson 也开始了下一代战争机器的开发。


这时候，Epic 内部有一个名为堡垒之夜 (Fortnite) 的项目正在开发。堡垒之夜始于 2011 年 8 月 Epic 内部 game jam (类似于黑客马拉松，在限定时间内发挥创意制作出游戏原型)。在战争机器 3 开发完成后，Epic 内部决定举办一个 game jam 来给员工挥洒创意，也是一种放松手段。当时团队受到我的世界 (Minecraft) 和泰拉瑞亚 (Terraria) 中的建造元素中获得启发，结合 Epic 擅长的射击，打算打造一款建造 + 射击的塔防生存游戏。堡垒之夜在 2011 年 11 月正式立项开发，并且在 12 月公布，定位 F2P。


但当时 Epic 的项目评审仍然以 CliffyB 和 Rod Fergusson 等战争机器主创为主。团队一时间难以转换思维方式，还是以主机 3A 游戏标准来给项目提出意见。加上引擎代际更换等因素，堡垒之夜的开发进展缓慢。


Tim 也一直关注着公司各个游戏项目的进展。随着战争机器 4 项目规模逐渐铺开，会占据公司大量资源，这样 F2P 转型就会更加缓慢。因此，Tim 希望能够引入外部投资者来帮助 Epic。


Tim 除了对市场的嗅觉灵敏，也注重建立盟友关系来实现目的。在公司 2.0 时期开发虚幻，Epic 找到了 GT Interactive (即雅达利) 作为发行合作伙伴。而在 Epic 3.0 时期进入主机 3A 市场，Epic 选择了和微软联手。而对于设想中进军 F2P 的 Epic 4.0，Tim 也希望能够有战略合作伙伴提供帮助，这时候他想到了腾讯。


这时候的腾讯已经具备了丰富的 F2P 经验，并在 2008 年在中国国内发行地下城与勇士 (Dungeon & Fighter) 和穿越火线 (Cross Fire) 取得成功。2009 年，腾讯投资英雄联盟的开发商拳头游戏 (Riot Games)，获得 22.34% 股份，并在 2011 年实现控股。Epic 的转型一方面需要筹集大量资金，另一方面也希望能够获得 F2P 的经验，于是开始和腾讯接触。2012 年六月，双方发出公告，腾讯以 3.3 亿美元收购 Epic 48.4% 流通股票，获得 Epic 40% 股份。


腾讯对 Epic 的投资非常重要。一方面，腾讯一向不太干涉投资公司的方向和运营，无论是拳头、Supercell 还是 Epic 都是如此，给了 Tim 足够的空间；另一方面，腾讯提供的 3.3 亿美元资金，为 Epic 大转型提高了容错率。而事后来看，腾讯凭借丰富的联网游戏经验，也为 Epic 提供了宝贵建议。


获得腾讯注资的 Epic 减少了后顾之忧。Tim 在战争机器 4 开发六个月后将项目喊停，转而全力开发 F2P 游戏，也招聘了不少具备 MMORPG (多人在线角色扮演游戏) 经验的人员加入。


但和渐进式演变相比，Tim 推动的这种 180 度转变在 Epic 内部也并不能完全取得认同。很多人对公司方向产生质疑而出走，这其中不乏公司里功勋卓著的人员。


首先是战争机器系列的执行制作人 Rod Fergusson 在 2012 年 8 月离开了 Epic。他仍然希望专注于主机 3A 游戏开发，因此在短暂加入 Irrational Games 后很快回到微软 Black Tusk 工作室继续负责战争机器系列。(注：微软在 2014 年一月和 Epic 达成协议，收购了战争机器所有的资产和权利)


CliffyB 也感到心灰意冷，并开始不来公司。过了不久后的一天，Tim 开车到 CliffyB 在海边的房子，和 CliffyB 有了一次推心置腹的谈话。两人在海滩上边散步边聊，Tim 希望 CliffyB 能够继续在公司工作，但 Epic 坚定转型 F2P，CliffyB 想做的游戏并不是公司的方向。面对将自己视作老大哥、并肩作战 20 年的创业伙伴 CliffyB，Tim 认为这次谈话也让他更懂得 CliffyB 想要的是什么。他最后让 CliffyB 做出选择：要么投身到 F2P，要么只能离开。CliffyB 在第二天提交了辞呈。不久后，CliffyB 宣布退休，声明需要离开游戏行业好好休息一段时间。


公司总裁 Mike Capps 也不认同 Tim 选择的方向，在 2012 年 12 月宣布退休并转为顾问兼董事，并在次年三月宣布已经辞去在 Epic 的一切职务。至此，帮助 Epic 转向主机 3A 并打造战争机器的核心团队全部离开。


Tim 并没有想到，Epic 3.0 向 4.0 的转变以失血为开始。但他仍然坚定不移，他说：

> 我其实并没有预料到会有这么大的影响 (指人才流失)。我以为新战略是正确的 (大家也会认同)。在人才保留方面，我其实可以做得好……Epic 的商业模式需要作出重大改变。已经有很多最优质的游戏采用了持续运营的模式，而非依靠零售式的大规模发售。未来 Epic 在行业里的角色应该是推动持续运营游戏的发展。因此，我们从一个垂直的专注于 Xbox 的主机游戏研发上，转型为多平台、自研自发，扩张的同时保持独立。


2014 年 1 月，Epic 和微软达成协议，将战争机器全部资产和权利出售。至此，Epic 将他们在 3.0 时期的最大标志和资产换成了资金，准备放手一搏。



### 偶然中的必然


除了堡垒之夜，Epic 内部还押注了两个项目。一个是虚幻争霸 (Paragon)，另一个是虚幻竞技场的重启 (下文称为新 UT)。


2012 年，Epic 从动视 Infinity Ward 工作室招揽来了 John Wasilczyk 作为主策划，和担任创意总监和制作人的 Steve Superville 一起从零开始负责一个新项目。除了必须是多人游戏以外，Epic 并未对这个新项目进行限制，给予了充分的创作空间。经过近一年碰撞，他们决定在 MOBA (即英雄联盟和 DotA 的品类) 基础上加入 3D 动作元素，成为一款新游戏，名为虚幻争霸。


Epic 对虚幻争霸寄予了厚望，希望将项目成为 Epic 4.0 运作模式的“样板”。一方面，项目团队持续不断地听取玩家意见，根据玩家反馈来对游戏进行迭代。另外，虚幻争霸采用纯线上发布模式，没有实体版本，也采用了英雄和皮肤作为主要的商业化方式。而作为 Epic 在移动端 F2P 游戏的首次尝试 (无尽之剑是付费买断制游戏)，工程师们也开始为虚幻争霸开发跨平台联机功能。得益于始于 UE3 的移动端兼容，使用 UE4 开发的虚幻争霸在不久后实现了 PC、主机和手机端的账号互通和联机对战。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/004226_9a3d0b89_5631341.png "屏幕截图.png")

> 虚幻争霸


新 UT 则走向了一个极端。与其说是一个独立的游戏项目，新 UT 更像是 Epic 测试社区共创和 UGC 平台商业模式的实验容器。新 UT 完全免费，而非 F2P——除了免费游玩以外，游戏中并不包含付费内容和微交易。该项目研发采用众包模式，Epic 通过论坛和直播方式来公布常规更新。项目源代码在 GitHub 上公开。Epic 希望在新 UT 上建立一个市场 (marketplace)，创作者可以销售 mod 和各种内容，而平台则在其中分成。新 UT 继承了 ZZT 和虚幻的理念，也标志着从主机游戏市场抽身的 Epic，重新扛起了赋能 UGC 大旗。


引擎业务方面，新一代虚幻引擎 (UE4) 在 2014 年正式推出 (之前有一些厂商获得了先行版本)，并公布了新的付费模式——19 美元/月订阅制，对于已发布的产品，Epic 则是收取流水 5% 的分成制度。2015 年 3 月，Epic 宣布完全免费使用，对于季度流水在 3000 美元以上的产品则收取 5% 的分成或者另外协商。此外，UE4 也优化了编辑器性能，推出“蓝图” (Blueprint) 工具，无需代码就可以实现基础的游戏设计，帮助开发者快速测试玩法，降低了游戏开发的技术门槛。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/004234_566a4534_5631341.png "屏幕截图.png")

> UE4 的蓝图功能


这些举动吸引了大量中小开发者，UE 开发生态开始蓬勃发展——一些大型游戏研发商的人员选择独立出来，组建小团队并使用 UE4 进行游戏开发，而不必担心成本和工具能力问题。一些小团队使用 UE4 开发的优秀游戏逐渐出现，比如火箭联盟 (Rocket League)、方舟：生存进化 (ARK: Survival Evolved)，以及现象级游戏——绝地求生 (PlayerUnknown's Battlegrounds)。


大逃杀类游戏是受日本电影《大逃杀》(Battle Royale) 所启发，有着和电影中类似的竞争机制——多名玩家在同一张地图之中，随着生存区域 (设定一般是外围毒气逼近或者是电网等) 逐渐缩小，玩家之间需要战斗至最后一人为最终胜利者。绝地求生并非大逃杀游戏的开创者，但被认为是其定义者 (genre defining title)，在机制和品质等方面找到了平衡，也是该品类首个全球级别的爆款。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/010227_4ec09c7f_5631341.jpeg "v2-af7efb53299a8a586fcdbd6ccdb8d0f1_720w.jpg")

> 绝地求生


尽管 UE4 帮助了诸多外部开发者研发游戏，但 Epic 的 F2P 爆款仍然难产。虚幻争霸尽管品质很高，但在玩法上始终难以找到平衡，在单局时长、地图设计和核心机制上不断挣扎。新 UT 更多是硬核玩家社区的自娱自乐，无法达到足够规模和形成生态。而堡垒之夜作为 Epic 4.0 时代最早开始研发的项目，有点像是夹生饭——核心玩法是 PvE (玩家对抗电脑/环境) 的沙盒生存游戏，玩家通过合作探索，收集资源来建造防御工事来抵抗怪物入侵，玩家之间的竞技对抗性较弱。商业模式方面也是玩家先行付费游玩，后续付费内容主要是不影响游戏平衡的装饰和皮肤。


在看到绝地求生的火爆之后，新 UT 开发团队的 Eric Williamson 和 Zack Estep 跃跃欲试。他们认为，最适合改造成大逃杀玩法的是同为第三人称、带有射击元素、有着大地图的堡垒之夜。由于 Epic 所有的项目都使用 UE 开发，编辑器也都通用，他们立刻可以上手。Epic 也允许他们调用堡垒之夜的模型等资产。堡垒之夜的大逃杀模式在 2017 年 7 月开始开发。团队拟定的目标很简单：尽量使用已有的素材和资产，先不考虑开发复杂新功能，开发越快越好。


一些来自新 UT 团队的成员也陆续参与到大逃生模式的开发之中。堡垒之夜大逃杀模式耗时两个月后在 2017 年 9 月完成。相比起 PvE 模式，大逃杀模式在武器、陷阱、地形方面都进行了简化。研发团队希望游戏单局时长不超过 25 分钟。大逃杀模式保留了 PvE 模式中的建造功能——最开始时，团队并不清楚玩家会怎样使用这个功能，只是希望能和绝地求生做出明显差异化。随着玩家增多，团队发现高手可以通过建造获得地形优势和防御加成，或者更容易到达某些区域。建造机制能够有效区分不同水平的玩家，提升玩家的操作上限和学习曲线。团队也因此不断改进建造模块，让玩家能够更便捷地进行建造，这也成为游戏的一大特色。堡垒之夜：大逃杀 (Fortnite: Battle Royale) 正式诞生。


在 Epic 最初的计划里，大逃杀模式将包含需要付费购买的堡垒之夜内，成为玩法模式之一。在计划推出日期的两周前，Epic 决定将大逃杀模式独立于本体之外免费推出。没有购买堡垒之夜：守护家园的玩家也可以免费下载游玩。


堡垒之夜：大逃杀在 2017 年 9 月底正式推出，并且迅速获得玩家的追捧。玩家人数在两周内超过了 1000 万。2018 年 6 月，Epic 宣布上线九个月的堡垒之夜：大逃杀玩家人数超过了 1.25 亿。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/004259_c736fc06_5631341.png "屏幕截图.png")

> 堡垒之夜：大逃杀


堡垒之夜：大逃杀甫一推出就同时支持 PC、主机和手机端，并且支持跨平台联机，进一步撬动了庞大的玩家群体。这时候市面上的其他游戏都无法做到，绝地求生甚至需要高配电脑才可以流畅运行。实现跨平台互通和联机，一方面是 Epic 在虚幻争霸开发时的技术积累，另外也是 Epic 在背后推动 Xbox 和 PlayStation 开放的结果。


由于采用了美漫风格 (创作团队一开始以皮克斯为基准)，堡垒之夜相比其他枪战射击游戏少了暴力元素，吸引了大批青少年玩家，成为一种社交场景。Epic 也根据玩家的喜好，加强了和游戏直播主播的合作，以及和潮流元素联动，包括主题皮肤等。逐渐意识到堡垒之夜并不止是一个游戏，而是一种文化现象的 Epic 也趁热打铁，利用堡垒之夜的实时 3D 互动、人物角色 (avatar)、大空间场景等特性，举办虚拟活动。比如，堡垒之夜举办了历史上首次大规模的虚拟演唱会——DJ 棉花糖 (Marshmallow) 的演唱会——吸引了超过一千万玩家实时在线观看。堡垒之夜内的虚拟活动也逐渐从演唱会，发展出来电影观影会等多种形式。

[![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/005047_0ab912b9_5631341.png "2022-07-08 00-50-09 的屏幕截图.png")](https://video.zhihu.com/video/1466375218386694144)

> 棉花糖在堡垒之夜举办的演唱会


堡垒之夜在 2018 年全年为 Epic 创造了 54 亿美元的营业收入，成为当年全球最赚钱的游戏。堡垒之夜横空出世，也是预示着 Epic 的 4.0 转型终获成功。


回过头来看，堡垒之夜的成功存在非常多的偶然——绝地求生的启发、新 UT 团队自发接手、撬动了移动端的群体、跨平台放大了社交效应……但各种偶然，恰好来自于 Epic 和 Tim 的理念、前瞻和长期坚持：

- 虚幻引擎的持续迭代，让工具强大到能够让 Brenden Greene 带领小团队就能做出绝地求生，反过来启发了堡垒之夜大逃杀
- 虚幻引擎的工具通用性，让新 UT 团队能够无缝切入，使用堡垒之夜本体的代码和资产，迅速开发出大逃杀模式
- Tim 早早意识到移动端的潜力，在 UE3 开始提升引擎在移动端的能力，是堡垒之夜能够打通主机、PC 端和移动端的重要前提
- Tim 的跨平台愿景，推动 Xbox 和 PlayStation 开放跨端联机，才进一步放大了社交效应，后来居上成为最主流的大逃杀游戏
- 引入腾讯投资，让 Epic 能够有充足的资源投入到各个游戏项目和虚幻引擎上，是上述所有事情发生的前提条件


历史无法重来，但经过梳理后其实可以断言：由于过去的种种铺垫和积累，让 Epic 已经具备了成功的条件，在 F2P 上取得成功只是时间问题。就像是 ZZT、虚幻、虚幻引擎和战争机器，Epic 在 1.0 到 3.0 时期所成就的。


而这次，堡垒之夜开花结果。


下一篇——展望 Epic：打造元宇宙 (metaverse)


参考资料：

1. Epic's latest: Paragon, Fortnite and Unreal Tournament, Polygon

2. How Bungie, bright colors, and four-player co-op shaped Gears of War 3, Polygon

3. Gears of War 4 would have cost Epic $100m - Sweeney, http://GamesIndustry.biz

4. The fixer: Why Rod Fergusson returned to Gears of War, Polygon

5. The making of Infinity Blade, Edge Online

6. Fortnite announces early access release, hands-on the unfinished game, Polygon

7. An Epic cross-play journey: Paragon, Fortnite, and what comes next, Epic Games

8. Epic Games, Acquired

9. The four lives of Epic Games, Polygon

编辑于 2022-02-06 20:19
