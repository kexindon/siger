<p><img width="706px" src="https://foruda.gitee.com/images/1659841572685260053/218砂之盒vr沉浸展副本.jpeg" title="218砂之盒VR沉浸展副本.jpg"></p>

- 艺术家们的狂欢带我一个！
- 一直想去阿那亚，但总是各种意外截胡。真的好想去（PS:这次还有我最爱的VR），来一次说走就走的机会！
- 期盼已久，迫不及待~
- 海滩和虚拟现实，想不到比这更美妙的搭配了。
- 出京的理由有啦！
- 阿那亚的 everything 都很喜欢！
- 有几个真不错啊！
- 都想去！特别喜欢沉浸式体验！尤其这种科技与艺术的结合，视觉上，感觉上，想象中……绝对惊艳！
- 关注 mana 很久了，阿那亚每隔一段时间就想去一次，没想到这次合作了！
- 一直想去秦皇岛，有了想去看的展，有了必须去的里有啦，一起啊！
- MANA 阿那亚，那个能有不值得冲的理由！去就完了
- 先说结论：PICO 有魄力，VR有未来！很多人把 VR 等同于游戏，这是小看了VR的价值。其实游戏只是前菜，将所有人与所有空间连接起来才是大餐，以身临其境的方式获取无尽的体验和内容才是VR或者说元宇宙的最大应用场景，建议了解一下，未来已来，只是尚未均匀分布。
- 我想抓住夏天的尾巴！
- 去买车票了，来吧！

> 这些就是看到活动的直观感受，我和他们一样，走起，转起！灵境频道的首刊就是VR影展，看到金狮的LOGO，似曾相识的画风，相信这会是一次难忘的旅程！:pray:

# [阿那亚一场艺术与科技的狂欢！国内规模最大VR沉浸艺术季开启](https://mp.weixin.qq.com/s/FJcXjSOvTymILsEF6g9KyA)

新媒体艺术站 2022-08-03 19:22 发表于上海

![](https://foruda.gitee.com/images/1659836862258381710/屏幕截图.png "屏幕截图.png")

-  **微信号** : 新媒体艺术站 mediaart
-  **功能介绍** : MANA | 全球新媒体艺术平台 www.manamana.net【新媒体艺术，具有“与时俱进”的特性，其利用计算机、网络、高科技材料、影像、生物、化学、表演等领域的新科技成果进行创新创作，已深入到当代艺术的各个领域。】

![输入图片说明](https://foruda.gitee.com/images/1659836927744043242/640-(1).gif "640 (1).gif")

![输入图片说明](https://foruda.gitee.com/images/1659837025001987962/640-(2).gif "640 (2).gif")

![输入图片说明](https://foruda.gitee.com/images/1659837038266809316/屏幕截图.png "屏幕截图.png")

### 2022 砂之盒沉浸艺术季

（2022年7月28日 – 2022年8月21日）

是目前国内规模最大的VR影像展

开始于2019年

集结了全球VR电影节的优秀作品



今年在 **阿那亚** 1公里海岸线沿途分设多个展区

包括32部 **全球VR/AR最新佳作** 

- 入围威尼斯电影节的《掌声》

- 入围戛纳电影节的VR作品《夜的尽头》

- 入围翠贝卡电影节作品《未尽之作》

……

(文末 **福利赠票** ，送你去现场)

更有 **首次** 面向公众的 **水下VR体验** 

VR影院、AR剧场、大空间多人互动、装置化内容等

多种形式的沉浸式内容，

为观众带来不同以往的度假式沉浸艺术狂欢。

![输入图片说明](https://foruda.gitee.com/images/1659837124695281869/屏幕截图.png "屏幕截图.png")

<p align="right">阿那亚礼堂 图片来源：阿那亚官方公众号</p>

![输入图片说明](https://foruda.gitee.com/images/1659837151925075664/屏幕截图.png "屏幕截图.png")

<p align="right">阿那亚园区展区分布</p>


由 **砂之盒沉浸展** 发起， **阿那亚** 联合主办， **Pico**  作为特约合作伙伴加盟的2022 砂之盒沉浸艺术季（以下简称：SIF 2022）于2022年7月28日-2022年8月21日在秦皇岛市北戴河新区阿那亚社区正式举办。届时，SIF 2022将携手全球知名VR/AR品牌Pico，为广大游客与观众带来长达25天的超长精彩活动。

![输入图片说明](https://foruda.gitee.com/images/1659837212490116418/屏幕截图.png "屏幕截图.png")

本次沉浸艺术季部分展映作品介绍

---

图片

## 水下VR体验 

### 《飞出太空 Spaced Out》

![输入图片说明](https://foruda.gitee.com/images/1659837362553539777/640-(2).jpeg "640 (2).jpg")

- 10 分钟 / 英语 / 水下VR体验
- 10 mins / English / Underwater VR experience
- 主创艺术家：Pierre Friquet（“Pyaré”）
- Lead artist：Pierre Friquet（“Pyaré”）

作品邀请观众漂浮在水面上，用通气管呼吸，造成一种沉浸在月球表面的错觉。作品通过画面将观众从地球运送到月球，从水运送到太空，让自己的感官离开身体，到达月球，并到达月球中空的中心。

![输入图片说明](https://foruda.gitee.com/images/1659837390113541656/屏幕截图.png "屏幕截图.png")

[图片上传失败(image-iefzzE4T5I16YRBDWauj)]


##  VR剧场 

### 《掌声 Clap》

![输入图片说明](https://foruda.gitee.com/images/1659837590917911910/640-(3)-(2).gif "640 (3) (2).gif")
![输入图片说明](https://foruda.gitee.com/images/1659837602750834659/640-(4)-(2).gif "640 (4) (2).gif")
![输入图片说明](https://foruda.gitee.com/images/1659837614814119210/640-(5)-(1).gif "640 (5) (1).gif")

- 13 分钟 / 无对白 / 交互体验
- 13 mins / No dialogue / VR Interactive Experience
- 导演：Keisuke Itoh
- 制片：Katsutoshi Machiba、Hiroko Fujioka

论前作《羽毛》还是新作《掌声》，导演Keisuke Itoh的主角总是羞涩而内向。然而，没有任何言语，仅仅通过肢体互动，便能带给人最单纯可爱的小美好。

- MANA APP 点击观看《掌声》作品预告片

  ![输入图片说明](https://foruda.gitee.com/images/1659837692772819589/屏幕截图.png "屏幕截图.png")

![输入图片说明](https://foruda.gitee.com/images/1659837704375642571/屏幕截图.png "屏幕截图.png")

在一个下雨的日子里，'线条人'踩着罐子跳起了踢踏舞。人们聚集在周围，大声为他鼓掌。然而，这些掌声却让他感到害怕。只有观众发自内心的掌声才能带给线条人真正的鼓舞。从此，一种美丽的声音开始奏响……

### 《扩散的阴影: 追逐星星的影子》

Augmented Shadow: Chasing Stars in Shadows

![输入图片说明](https://foruda.gitee.com/images/1659837725923788785/屏幕截图.png "屏幕截图.png")
![输入图片说明](https://foruda.gitee.com/images/1659837730394644602/屏幕截图.png "屏幕截图.png")
![输入图片说明](https://foruda.gitee.com/images/1659837817608482240/640-(6).gif "640 (6).gif")
![输入图片说明](https://foruda.gitee.com/images/1659837829310549284/640-(7).gif "640 (7).gif")

- 15 分钟 / 无对白 / 沉浸式投影
- 15 mins / No dialogue / Immersive Projection
- 主创艺术家：Joon Moon
- Lead artist: Joon Moon

即使是二维的影子，也可以有三维的表达。通过光影互动技术和音乐，作品讲述了六个影子和一群小鱼的故事。体验者可以控制移动灯笼，通过改变投射在墙上和地上的影子与其他体验者进行互动。

- MANA APP 点击观看《扩散的阴影: 追逐星星的影子》作品预告片

  ![输入图片说明](https://foruda.gitee.com/images/1659837853754205347/屏幕截图.png "屏幕截图.png")

![输入图片说明](https://foruda.gitee.com/images/1659837889834798594/屏幕截图.png "屏幕截图.png")

### 《黑色记忆 Black Ice》

![输入图片说明](https://foruda.gitee.com/images/1659837905633458437/屏幕截图.png "屏幕截图.png")

- 19 分钟 / 英语 / 交互体验
- 19 mins / English / VR Interactive Experience
- 导演：Arif Khan

在赛博朋克的未来，一位年轻女子试图使用记忆编辑器去删除自己的那些负面回忆。然而，她越是改变记忆，就越是发现自己身陷其中。《黑色记忆》是一部交互叙事体验，探讨了记忆的力量：它如此危险，却也是构成我们自身的重要组成部分......无论好坏。

### 《未尽之作 ：第 5 集》

MISSING PICTURES: EPISODES 5

![输入图片说明](https://foruda.gitee.com/images/1659837937026923502/屏幕截图.png "屏幕截图.png")

- 14 分钟 / 英语 / 交互体验
- 14 mins / English / VR Interactive Experience
- 导演：Clément Deneux, Joseph Beauregard
- 制片：Atlas V、Arte France、BBC、PTS Taiwan、Serendipity Films、Wild Fang Films、Giioii

每拍一部电影，就有成百上千的画面被埋没。《未尽之作》给了导演们一个机会，来讲述那些无法面世的故事。作品每集邀请一位电影人讲述他们电影制作中的关键时刻，探讨这部电影对他们的重要性，以及这部电影无法面世的原因。《未尽之作》第五集由河濑直美讲述。

- MANA APP 点击观看《未尽之作 ：3-5 集》预告片

  ![输入图片说明](https://foruda.gitee.com/images/1659838005144328219/屏幕截图.png "屏幕截图.png")

## 2部大空间装置&互动投影 

### 《极限凌波 All Kinds of Limbo》

![输入图片说明](https://foruda.gitee.com/images/1659838024996370772/屏幕截图.png "屏幕截图.png")

- 12 分钟 / 英语 / 交互体验
- 12 mins / English / VR Interactive Experience
- 导演：Toby Coffey
- 制片：Dimension Studios

《极限凌波》是一场独特的音乐表演，由英国国家剧院创作，将观众带入一段音乐之旅。作品的灵感来自于加勒比文化对英国音乐界的影响，主演努比亚演绎了雷鬼、灰泥、古典和卡利普索等音乐风格，讲述了自己在这些音乐流派中的生活感悟。

![输入图片说明](https://foruda.gitee.com/images/1659838046759714070/屏幕截图.png "屏幕截图.png")

![输入图片说明](https://foruda.gitee.com/images/1659838055906546981/屏幕截图.png "屏幕截图.png")

……

## 2022砂之盒沉浸艺术季完整获奖名单 
 
![输入图片说明](https://foruda.gitee.com/images/1659838072438581148/屏幕截图.png "屏幕截图.png")

<p align="right">2022砂之盒沉浸艺术季颁奖现场</p>

### 砂之盒评委会大奖
（Sandbox Jury Award）

![输入图片说明](https://foruda.gitee.com/images/1659838115264621478/屏幕截图.png "屏幕截图.png")

 **《迷宫LAVRYNTHOS》** 
- 20 分钟 / 英语 / 交互体验
- 导演：Amir Admoni, Fabito Rychter
- 制片：Delirium XR

这是一个集观赏性和娱乐性为一体的作品。从故事完整度、制作精美度以及交互体验等多维度的完成都非常好地呈现给体验者。作品中的人物造型精美，叙事方式创新，在互动环节引入得自然流畅，也由此增加了与观众的粘合度和好感，是符合当下时代审美的好作品。

### 砂之盒最佳体验奖
（Best Immersive Experience）

![输入图片说明](https://foruda.gitee.com/images/1659838194973672410/屏幕截图.png "屏幕截图.png")

 **《双世阴谋Lustration》** 
- 30 分钟 / 英语 / 交互体验
- 导演：Ryan Griffen

此作品运用便捷、多维度的交互创新手段让体验者在双故事线索的情节中感受到得心应手，也由此让故事的沉浸感和粘合度增高。另外，美术风格鲜明，视觉清新让人记忆深刻。

### 砂之盒最佳叙事奖
（Best Immersive Story）& Pico×SIF VR新锐作品奖

![输入图片说明](https://foruda.gitee.com/images/1659838233051267571/屏幕截图.png "屏幕截图.png")

 **《夜的尽头End of Night》** 
- 49 分钟 / 英语 / 交互体验
- 导演：David Adler
- 制片：Makropol (Mikkel Skov, Mads Damsbo), Novelab

此作品非常娴熟地运用技术手段的优势，将一个宏大的叙事题材表述得严谨、完整且合理流畅。真实影像的捕捉与场景的特效完美贴合故事情节的跌宕起伏。丰富的层次，更增添了故事的情感渲染。

### 砂之盒最佳沉浸艺术奖
（Best Immersive Art）

![输入图片说明](https://foruda.gitee.com/images/1659838326555227614/屏幕截图.png "屏幕截图.png")

 **《黑色记忆Black Ice》** 
- 19 分钟 / 英语 / 交互体验
- 导演：Arif Khan

此作品视觉风格鲜明，用色大胆，取材新颖。它巧妙的互动设计将叙事和交互完美地结合，具有很强的情节带入。整个作品带来的沉浸体验有扑面而来的新世代气息。

### 最佳技术创新奖
（Best Technical Innovation）

![输入图片说明](https://foruda.gitee.com/images/1659838352367480405/屏幕截图.png "屏幕截图.png")

 **《极限凌波 All kind of Limbo》** 

- 12 分钟 / 英语 / 交互体验
- 导演：Toby Coffey
- 制片：Dimension Studios

此作品运用复杂的技术将视觉、音乐、场景等多维度沉浸式体验融合得不留痕迹，是一部精美的作品。作为VR的创作，作品极限凌波具有很好的落地体验，在未来发展可能上有很大的想象空间。

### 砂之盒最佳华人作品奖
（Best Chinese Work）

![输入图片说明](https://foruda.gitee.com/images/1659838496145712943/未标题-7.jpeg "未标题-7.jpg")

 **《诊断Diagnosia》** 
- 30 分钟 / 英语 / 交互体验
- 导演：Mengtai Zhang
- 制片：Mengtai Zhang, Lemon Guo, Huang Yue

此作品的可贵之处在于突破了VR技术通常所营造的身临其境外，通过故事实现由景至情的共鸣，现实场景游戏式交互让体验者有主人公感同身受的带入感。

---

## 活动详情

![输入图片说明](https://foruda.gitee.com/images/1659838521293500145/屏幕截图.png "屏幕截图.png")

### 时间

2022年7月28日-2022年8月21日

### 每日场次

- 10:00-12:30
- 13:00-15:30
- 16:00-18:30
- 19:00-21:30

### 主办单位

砂之盒沉浸展

### 联合主办

阿那亚

### 特约合作伙伴

Pico

### 展映地点

秦皇岛市北戴河新区阿那亚社区·A剧场内

### 票价

198元

### 购票方式 

扫描下方二维码

![输入图片说明](https://foruda.gitee.com/images/1659838654129663072/屏幕截图.png "屏幕截图.png")


### 关于砂之盒沉浸展

![输入图片说明](https://foruda.gitee.com/images/1659838671320590419/屏幕截图.png "屏幕截图.png")

砂之盒沉浸展（SIF）是沙核科技（砂之盒沉浸展组委会）发起的，全球规模最大，级别最高的年度沉浸娱乐盛会。内容涵盖:虚拟现实、增强现实、沉浸式戏剧、数字艺术及线下主题娱乐等领域。SIF通过峰会、展映、工作坊、创投会等形式全面呈现最尖端的沉浸娱乐体验内容。

### 关于Pico

![输入图片说明](https://foruda.gitee.com/images/1659838702051039063/屏幕截图.png "屏幕截图.png")

Pico成立于2015年3月，是全球知名的具备独立创新和研发创造能力的VR/AR品牌。Pico旗下VR产品销量长期居于国内和国际市场前列。作为Pico旗下旗舰级6DoF产品，Pico Neo3不仅在互动娱乐、运动休闲、交互观影等消费级场景获得了消费者的一致好评，在教育、医疗、培训等商用场景亦有广泛应用，给全球企业级用户提供了多元化的VR一体机解决方案，备受各界认可。

### 关于「大千」

![输入图片说明](https://foruda.gitee.com/images/1659838718775607375/屏幕截图.png "屏幕截图.png")

「大千」是Sandman Studios基于UE引擎构建的集虚拟观影、虚拟演出、虚拟展览、虚拟世界探索等于一体的新一代沉浸式虚拟娱乐平台，将为虚拟娱乐内容提供一个更加专业化、场景化、社交化的虚拟内容聚合分发平台。

<p align="center">
在此地<br>
在沙滩上<br>
在浪潮中<br>
在阳光和风里<br>
皮肤被沙砾粘黏<br>
思想被现实裹挟<br>
<br>
在此时<br>
沉浸想象<br>
身体在现场<br>
亦在脑海中<br>
思绪延伸、腾跃、恣意探知<br>
感知离现<br>
进入虚拟的外延<br>
<br>
离场，也是入场<br>
突破感知边界<br>
探寻鲜活表达<br>
拥抱别样的自我<br>
去感受，去体验<br>
去拓展自己的生命经验<br>
<br>
沐着夏日的阳光和海水<br>
让身体与精神释放<br>
完整离现，进入未知<br>

![输入图片说明](https://foruda.gitee.com/images/1659838830357263487/屏幕截图.png "屏幕截图.png")

---

回顾“青岛2019砂之盒沉浸影像周”▽

![输入图片说明](https://foruda.gitee.com/images/1659838890853768484/640-(8).gif "640 (8).gif")

福利 赠送 **2022 砂之盒沉浸艺术季** 

展览门票（展期内任意一天晚间场 19:00）

下方评论区留言，获得点赞数最多的
- 第1位将获得门票3张
- 第2位将获得门票2张
- 第3位将获得门票1张
- 第4位将获得门票1张
- 第5位将获得门票1张

2022年8月10日（下周三）  
中午12点整为最终截止时间  
(最终解释权归MANA所有）

 **项目推广及商务合作** 
- 添加微信：artmana

## 近期文章推荐

- [00后团队，元宇宙打造上市仪式](https://mp.weixin.qq.com/s?__biz=MzI0MDQxNTI2OQ==&mid=2247756735&idx=1&sn=a8f3f611bed966a565671b0f964b7815)
- [7月盘点全球新媒体艺](https://mp.weixin.qq.com/s?__biz=MzI0MDQxNTI2OQ==&mid=2247756511&idx=1&sn=12e3cdf4efb920080de6ab99ad4d43e8)


MANA | 全球新媒体艺术平台，MANA是全球新媒体艺术行业媒体与资源对接平台，致力于建立艺术、设计与科技协同创新的生态系统。MANA持续聚焦追踪行业热点，旨在提升优秀原创、艺术科技跨界的影响力，为相关领域人士提供交流互励平台，为优秀创作者赋能，为企业及品牌整合对接优质的行业资源，推动行业发展。／ 官 方 网 站 ／[www.manamana.net](https://www.manamana.net/#!zh)

![输入图片说明](https://foruda.gitee.com/images/1659839276073556358/640-(9).gif "640 (9).gif")