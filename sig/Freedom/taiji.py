# author: Eli
# date: 2022/7/8
# draw a taiji graph.

import turtle as t
t.speed(1)
t.begin_fill()
t.circle(100,180)
t.circle(200,180)
t.circle(100,-180)
t.end_fill()
t.circle(100,180)
t.circle(200,180)
t.goto(0,100-25)
t.fillcolor("white")
t.begin_fill()
t.circle(-25)
t.end_fill()
t.up()
t.goto(0,-75)
t.fillcolor("black")
t.begin_fill()
t.circle(25)
t.end_fill()
t.hideturtle()