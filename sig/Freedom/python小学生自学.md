- [Thonny-适合初学者的 Python IDE](https://blog.csdn.net/huidaoli/article/details/121531205)
- [linux 安装 tkinter](#linux-安装-tkinter) 
- [如何用 Python 画太极图？](https://zhuanlan.zhihu.com/p/361819251)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0709/044247_59bcbbf4_5631341.jpeg"></p>

同学们一定被封面上帅气的拳手吸引到了吧，没错今天的主角就是 Eli。第一次见到他是通过屏幕，妈妈发来了一段PY视频，结尾处一阵旋风拳打出屏幕，深深地吸引了我。这也是本期封面背景的直接原因（注：网图，不是Eli本尊）。今天我很高兴，从 Eli 哪里学来了 Th 果然清爽，一键安装后，直接上手。我连夜完成本期专题，就是想回赠一份礼物给他，我补上了 Linux 安装，学开源怎么能不上Linux (专业人士都用它，嘿嘿)。刚发了一段儿验证视频给妈妈，就收到了 Eli 的语音，赶紧补上 [他山之石](#他山之石) 画法不同，异曲同工。我相信他会喜欢上 SIGer 的。

> 剧透一个频道花絮：Eli 妈妈的昵称是 “自游”，当我们讨论 火种队的筹备工作时，我想到了这个名字，“自由” 畅游在知识的海洋里，快乐成长的 Eli 就和他的拳风一样 呈现在了我的面前。英文以 Freedom 为名，是我认识一位 丹麦的跆拳道教练，他的名字就是 Freedom 分毫不差，我当时看到签名还以为我看错了，丹麦 - 乐高的故乡，北欧风，玩中学 的鼻祖。就用 Freedom 为名，期待聚集一票快乐小伙伴，快乐玩耍起来吧。

# 《[python小学生自学](https://gitee.com/eliwang0722/siger/issues/I5G8O5#note_11511102_link)》学习小组的介绍

Eli 是一名柔道体育特长生，机缘巧合下，得知了 SIGer 志愿者项目。作为一名快8岁的二年级的小学生，我想分享在自学 python 的成长过程中的每个项目和经验所得，在此形成连续的输出，让更多的同龄孩子对 python 多一些了解和兴趣，建立信心，甚至教会他们如何自学 python；并且可以在 Gitee 平台上结交更多的志同道合的小伙伴，让更多的中国青少年，爱上编程，在生活中运用编程。

### 太极图的 python 项目： 

 **用Thonny 实现的太极图的运行效果** 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/031859_99b82bb0_5631341.png "屏幕截图.png")

话不多说，先看视频介绍：https://www.bilibili.com/video/BV15Y4y1J7Tn

### python的编写软件

作为python的编写软件有很多，比如，IDLE，Pycharm，PyDev，还有一些编程机构，都有自己的推荐安装的 python 编辑器软件。

我在这里比较熟练使用的是（初学者推荐首选的也是）： **Thonny** 

推荐理由，见此文章。
https://blog.csdn.net/huidaoli/article/details/121531205

### Thonny 的安装：

想一起自学学习python的同学，先安装python的编辑器软件。

 **Thonny的安装教程：** 

打开网址：https://thonny.org/  
下拉到最下面，找到 [windows](https://github.com/thonny/thonny/releases/download/v3.3.13/thonny-3.3.13.exe) 和 [mac os](https://github.com/thonny/thonny/releases/download/v3.3.13/thonny-3.3.13.pkg) 的安装下载链接，点击下载  
按照指示，完成 Thonny 的安装

下面的我发表的每一期Issue，就是一个太极图的编程思路，感兴趣的同学，也可以按照你的思路提交自己的太极图python代码。


```
import turtle as t
t.speed(1)
t.begin_fill()
t.circle(100,180)
t.circle(200,180)
t.circle(100,-180)
t.end_fill()
t.circle(100,180)
t.circle(200,180)
t.goto(0,100-25)
t.fillcolor("white")
t.begin_fill()
t.circle(-25)
t.end_fill()
t.up()
t.goto(0,-75)
t.fillcolor("black")
t.begin_fill()
t.circle(25)
t.end_fill()
t.hideturtle()
```


 **每一次的python编程的代码，我都会尝试不一样的太极图的画法，1-2-3 的迭代完善** 

# [Thonny-适合初学者的 Python IDE](https://blog.csdn.net/huidaoli/article/details/121531205)

[huidaoli](https://huidaoli.blog.csdn.net/) 于 2021-11-25 10:30:27 发布

> 今天来说说Python IDE,如果你是一位Python初学者,那最适合Thonny它了，如果不是初学者，请选择PyDev和Pycharm.

## 一、下载安装说明

适用于 [Windows](https://github.com/thonny/thonny/wiki/Windows)  
适用于 [Mac](https://github.com/thonny/thonny/wiki/MacOSX)  
适用于 [Linux](https://github.com/thonny/thonny/wiki/Linux)  
[pip 安装](https://github.com/thonny/thonny/wiki/SeparateInstall)  
[常问问题](https://github.com/thonny/thonny/wiki/FAQ)  
最新的稳定版本链接在本页顶部的下载框中。旧版本和预发布可以在 https://github.com/thonny/thonny/releases 找到

## 二、初学者为什么适合选择它：

看下它的界面就知道，简直是清爽的不能再清爽了。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/032646_7e4e0440_5631341.png "屏幕截图.png")

## 三、那它有那些特征：

### 1.易于上手

Thonny 内置了 Python 3.7，因此只需要一个简单的下载安装过程，您就可以开始学习编程了。（如有必要，您也可以使用单独的 Python 安装。）初始用户界面去除了所有可能分散初学者注意力的功能。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/032706_e56b4a2e_5631341.png "屏幕截图.png")

是不是很有个性的IDE.

### 2.没有麻烦的变量

完成 hello-worlds 后，选择View → Variables并查看您的程序和 shell 命令如何影响 Python 变量。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/032727_8fd60443_5631341.png "屏幕截图.png")

### 3.简单的调试器

调试程序时只需按 Ctrl+F5 而不是 F5，您就可以逐步运行您的程序，不需要断点。按 F6 为一大步，F7 为一小步。步骤遵循程序结构，而不仅仅是代码行。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/032743_0b240061_5631341.png "屏幕截图.png")

### 4.逐步完成表达式评估

如果您使用小步骤，那么您甚至可以看到 Python 如何评估您的表达式。您可以将这个浅蓝色框视为一张纸，其中 Python 将子表达式逐个替换为其值。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/032757_050a17b5_5631341.png "屏幕截图.png")

### 5.函数调用的忠实表示

单步执行函数调用会打开一个新窗口，其中包含单独的局部变量表和代码指针。充分理解函数调用的工作原理对于理解递归尤其重要。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/032809_45f1c606_5631341.png "屏幕截图.png")

### 6.突出显示语法错误

未闭合的引号和括号是初学者最常见的语法错误。Thonny 的编辑器使这些很容易被发现。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/032823_718f9dcd_5631341.png "屏幕截图.png")

### 7.解释范围

突出显示变量出现提醒您，相同的名称并不总是意味着相同的变量，并有助于发现错别字。局部变量在视觉上与全局变量不同。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/032837_e2f46cbe_5631341.png "屏幕截图.png")

### 8.解释参考的模式

变量最初根据简化模型（名称→值）呈现，但您可以切换到更现实的模型（名称→地址/id→值）。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/032853_25391b80_5631341.png "屏幕截图.png")

### 9.代码补全

学生可以在代码完成的帮助下探索 API。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/032905_50b6a7a4_5631341.png "屏幕截图.png")

### 10.初学者友好的系统Sell

选择工具 → 打开系统Sell以安装额外的软件包或学习在命令行上处理 Python。PATH 以及与其他 Python 解释器的冲突由 Thonny 处理。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/032919_4fba9fa5_5631341.png "屏幕截图.png")

### 11.简单干净的 pip GUI

选择工具 → 管理包，以便更轻松地安装第三方包。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/032933_4ebe731e_5631341.png "屏幕截图.png")

## 四、插件安装

Thonny [具有用于扩展的简单基础结构](https://github.com/thonny/thonny/wiki/Plugins)。

### 这些是一些已知的 Thonny 插件：

- [thonny-black-format](https://pypi.org/project/thonny-black-format/) 添加了一个用[黑色](https://black.readthedocs.io/en/stable/)格式化当前文件的命令
- [thonny-ev3dev](https://pypi.org/project/thonny-ev3dev/) 允许将代码上传到 EV3（以及更多）
- [thonny-error-explainer](https://pypi.org/project/thonny-error-explainer/) 使用新的错误检查器扩展了 Assistant
- [thonny-lahendus](https://pypi.org/project/thonny-lahendus/) 允许从 [lahendus.ut.ee](https://lahendus.ut.ee/) 加载练习并提交解决方案以进行自动评估。
- [thonny-edison](https://pypi.org/project/thonny-edison/) 允许将Python 代码上传到 [Edison 教育机器人](https://meetedison.com/)
- [thonny-dracula](https://pypi.org/project/thonny-dracula/) 添加了 Dracula 语法主题。
- [thonny-onedark](https://pypi.org/project/thonny-onedark/) 添加了 One Dark 语法主题。
- [thonny-crosshair](https://pypi.org/project/thonny-crosshair/) 添加了调用 [CrossHair](https://github.com/pschanely/CrossHair) 分析器的命令。
- [thonny-icontract-hypothesis](https://pypi.org/project/thonny-icontract-hypothesis/) 添加了调用[icontract-hypothesis](https://github.com/mristin/icontract-hypothesis) 分析器的命令。

如果你要扩展功能，可以安装上面的插件。  
————————————————  
版权声明：本文为CSDN博主「huidaoli」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。  
原文链接：https://blog.csdn.net/huidaoli/article/details/121531205

# linux 安装 tkinter 

```
#python2安装tkinter
sudo apt-get install python-tk
#python3安装tkinter
sudo apt-get install python3-tk
 
sudo apt install tk-dev
```

通常没有安装过，运行PY程序时会报错：

```
>>> import turtle as t
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/usr/lib/python3.9/turtle.py", line 107, in <module>
    import tkinter as TK
ModuleNotFoundError: No module named 'tkinter'
```

[`ModuleNotFoundError: No module named ‘_tkinter‘` 解决方案](https://gitee.com/eliwang0722/siger/issues/I5G9XU)：
> 转载自：https://blog.csdn.net/weixin_58503231/article/details/122355493

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/095451_452b18a8_5631341.gif "乐秀视频剪辑第152部_20220709095011233 (1).gif")

> 使用 mobaxterm 远程访问 ubuntu 开发板，运行实况！

# 他山之石


```
from turtle import *

def yin(radius, color1, color2):
    width(3)
    color("black", color1)
    begin_fill()
    circle(radius/2., 180)
    circle(radius, 180)
    left(180)
    circle(-radius/2., 180)
    end_fill()
    left(90)
    up()
    forward(radius*0.35)
    right(90)
    down()
    color(color1, color2)
    begin_fill()
    circle(radius*0.15)
    end_fill()
    left(90)
    up()
    backward(radius*0.35)
    down()
    left(90)

def main():
    reset()
    yin(200, "black", "white")
    yin(200, "white", "black")
    ht()
    return "Done!"

if __name__ == '__main__':
    main()
    mainloop()
```

- 需要 `nano taiji.py` 事先保存到文件里，
- 再运行 `python3 taiji.py`

### 【转载】[如何用Python画太极图？](https://zhuanlan.zhihu.com/p/361819251)

Hello，各位小伙伴们，本次为大家介绍一个Python中的绘图模块——turtle。turtle（海龟）是Python内置的一个标准模块，它提供了绘制线、圆以及其他形状的函数，使用该模块可以创建图形窗口，在图形窗口中通过简单重复动作直观地绘制界面与图形。

turtle模块的逻辑非常简单，利用该模块内置的函数，用户可以像使用笔在纸上绘图一样在turtle画布上绘制图形。turtle的使用主要分为创建窗口、设置画笔和移动画笔三个方面，例如创建800*600像素的窗口，turtle.setup(800*600)。

画笔（pen）的设置包括画笔属性（如尺寸、颜色）和画笔状态的设置。turtle模块中定义了设置画笔属性和状态的函数，width()函数用于设置画笔尺寸，它的参数width设置画笔绘制出的线条的宽度；speed()函数的参数speed用于设置画笔移动的速度，其取值范围为[0,10]内的整数，数字越大，速度越快；color()函数的参数color用于设置画笔的颜色。

正如在纸上绘制一样，turtle中的画笔分为提起（UP）和放下（DOWN）两种状态。只有画笔为放下状态时，移动画笔，画布上才会留下痕迹。turtle中的画笔默认为放下状态，使用penup()函数可以提起画笔，使用pendown()函数可以放下画笔。

在我们绘制图形的时候，还需要画笔在画布上移动。turtle模块中画笔控制函数主要分为移动控制、角度控制和图形绘制3种。

用于移动控制函数forward()和backward()，他们有相同的参数distance，用于指定画笔移动的距离，单位为像素；用于角度控制函数right()和left()，他们有共同的参数degree，用于指定画笔向右与向左的角度。

除此之外，我们还需要用于图形绘制的circle()函数，使用该函数可绘制以当前坐标为圆心，以指定像素值为半径的圆或弧，函数circle()的参数radius用于设置半径，extent用于设置弧的角度。

下面我们就使用turtle模块绘制一个太极图，代码如上。

运行程序，效果如下图所示。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/093847_1ec9b6c5_5631341.png "屏幕截图.png")

> 绘图步骤：
  - 画黑鱼边框（黑色）
  - 填充黑色
  - 画黑鱼白眼（圆边黑色）
  - 填充白色
  - 画白鱼边框（黑色）
  - 填充白色
  - 画白鱼黑眼（圆边黑色）
  - 填充黑色

好了，今天给大家介绍Python内置模块Turtle，如果大家有其他想法可以随时给出建议，今天先讲解到这里，希望这篇文章能给大家带来帮助。