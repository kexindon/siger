# https://f1tenth.org/about.html

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/034924_6e1c71c3_5631341.jpeg "upenn-ppl.jpg")

F1TENTH is an international community of researchers, engineers, and autonomous systems enthusiasts. It was originally founded at the University of Pennsylvania in 2016 but has since spread to many other institutions worldwide. Our mission is to foster interest, excitement, and critical thinking about the increasingly ubiquitous field of autonomous systems. We fulfill this mission with four pillars:

1.  **Build**  - We designed and maintain the F1TENTH Autonomous Vehicle System, a powerful and versatile open-source platform for autonomous systems research and education.

2.  **Learn**  - We create courses that teach the foundations of autonomy but also emphasize the analytical skills to recognize and reason about situations with moral content in the design of autonomous.

3.  **Race**  - We bring our international community together by holding a number of autonomous race car competitions each year where teams from all around the world gather to compete.

4.  **Research**  - Our platform is powerful and versatile enough to be used for a variety of research that includes and is not limited to autonomous racing, reinforcement learning, robotics, communication systems, and much more.


## Meet the Team

### DEVELOPERS

- [Billy Zheng](https://www.linkedin.com/in/hongruizheng/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/183318_7ab529f8_5631341.png "屏幕截图.png")

- [Johannes Betz](https://www.linkedin.com/in/johannes-betz-254049107/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/183329_12354b6c_5631341.png "屏幕截图.png")

#### Website Map

 **[Build](https://f1tenth.org/build.html)**  contains all the information to get started with an F1TENTH vehicle.

 **[Learn](https://f1tenth.org/learn.html)**  teaches the fundamentals of an autonomous vehicle system.

 **[Race](https://f1tenth.org/race.html)**  has details regarding upcoming as well as past races.

 **[Research](https://f1tenth.org/research.html)**  gives a glimpse of the different research areas that use F1TENTH.

 **Crew**  comprises of the core group of people who maintain all of the content on this website. Scroll down to see what F1TENTH alumni are up to now.

 **Partners**  is a list of the institutions that participate in F1TENTH.

> For more information on how to join the F1TENTH community, [email us](mailto:contact@f1tenth.org).

### ORGANIZERS

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/183701_9ef754bd_5631341.png "屏幕截图.png")

### ALUMNI

- [Xinlong Zheng](https://www.linkedin.com/in/xinlong-zheng-47092316a/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/183909_4b05ae6f_5631341.png "屏幕截图.png")

- [Xiaozhou Zhang](https://www.linkedin.com/in/xiaozhou-zhang-91760b13a/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/183944_c083b92b_5631341.png "屏幕截图.png")

- [Lejun Jiang](https://www.linkedin.cn/incareer/in/lejun-jiang-a60274174/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/184048_634928cf_5631341.png "屏幕截图.png")

- [Junfan Pan](https://www.linkedin.com/in/junfan-pan/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/184119_54d50e9b_5631341.png "屏幕截图.png")

- Zhihao Ruan

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/184258_890f3731_5631341.png "屏幕截图.png")

- [Dayong Tong](https://www.linkedin.cn/incareer/in/dayong-tong/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/184331_08400b1f_5631341.png "屏幕截图.png")

- [Kuk Jang](https://www.linkedin.com/in/kuk-jang-9a971014/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/184434_f42c1513_5631341.png "屏幕截图.png")

  - PhD Candidate, University of Pennsylvania

- [Kaiying Guo](https://www.linkedin.com/in/48kaiying/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/184459_cd5e7433_5631341.png "屏幕截图.png")

  - Software Enginner Intern, Facebook

- [Susan Xie](https://www.linkedin.com/in/susan-xie/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/184912_458f1987_5631341.png "屏幕截图.png")

  - Tools Intern, Hi-Rez Studios

- [Diana Hoang](https://www.linkedin.com/in/dnhoang/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/184852_f5564d4c_5631341.png "屏幕截图.png")

  - Recruiting Support Staff, Oracle

- [Yuwei Wang](https://www.linkedin.com/in/yuwei-wang-956a68152/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/184843_f9f09648_5631341.png "屏幕截图.png")

  - Applied Scientist, Amazon

- [Baihong Zeng](https://www.linkedin.com/in/baihong-zeng-a5a3a512b/)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/184830_1c6cdd88_5631341.png "屏幕截图.png")

  - Vehicle Enginner, Nissan

- [Zirui Zang](https://f1tenth.org/crew/zang.jpeg)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/184817_918a5e68_5631341.png "屏幕截图.png")

  - Imaging Science, PerkinElmer


### PARTNERS

<p>
<a href="https://www.asu.edu/" target="_blank"><img src="https://f1tenth.org/partners/asu.png" height="150px"></a>
<a href="https://www.autoware.org/" target="_blank"><img src="https://f1tenth.org/partners/autoware.jpg" height="150px"></a>
<a href="https://www.binghamton.edu/" target="_blank"><img src="https://f1tenth.org/partners/binghamton.png" width="250px"></a>
<a href="https://www.cmu.edu/" target="_blank"><img src="https://f1tenth.org/partners/cmu.jpg" width="250px"></a>
<a href="https://www.cuny.edu/" target="_blank"><img src="https://f1tenth.org/partners/cuny.png" height="125px"></a>
<a href="http://www.clemson.edu/" target="_blank"><img src="https://f1tenth.org/partners/clemson.png" width="225px; padding-left: 30px"></a>
<a href="https://www.columbia.edu/" target="_blank"><img src="https://f1tenth.org/partners/columbia.png" width="225px"></a>
<a href="https://www.cvut.cz/en" target="_blank"><img src="https://f1tenth.org/partners/czech.png" width="225px"></a>
<a href="https://duke.edu/" target="_blank"><img src="https://f1tenth.org/partners/duke.png" height="125px"></a>
<a href="https://ethz.ch/en.html" target="_blank"><img src="https://f1tenth.org/partners/ethzurich.png" width="250px; padding-right: 10px"></a>
<a href="https://www2.gmu.edu/" target="_blank"><img src="https://f1tenth.org/partners/georgemason.png" width="200px"></a>
<a href="https://www.gatech.edu/" target="_blank"><img src="https://f1tenth.org/partners/GAtech.png" width="170px"></a>
<a href="http://www.gzu.edu.cn/en/" target="_blank"><img src="https://f1tenth.org/partners/guizhoulogo.png" width="170px"></a>
<a href="https://www.hh.se/english.html" target="_blank"><img src="https://f1tenth.org/partners/halmstad.jpg" width="200px"></a>
<a href="https://www.polyu.edu.hk/en/" target="_blank"><img src="https://f1tenth.org/partners/hongkongpoly.png" width="300px"></a>
<a href="http://www.iitb.ac.in/" target="_blank"><img src="https://f1tenth.org/partners/iit.png" width="250px"></a>
<a href="https://www.ip-paris.fr/en/home-en/" target="_blank"><img src="https://f1tenth.org/partners/polytechparis.jpg" width="250px"></a>
<a href="http://www.kaist.edu/" target="_blank"><img src="https://f1tenth.org/partners/kaist.png" width="200px"></a>
<a href="https://www.k-state.edu/" target="_blank"><img src="https://f1tenth.org/partners/kansas.png" width="200px"></a>
<a href="https://www.kit.edu/english/index.php" target="_blank"><img src="https://f1tenth.org/partners/kit.png" width="200px"></a>
<a href="https://www.kth.se/en" target="_blank"><img src="https://f1tenth.org/partners/kth.png" height="200px"></a>
<a href="https://en.knu.ac.kr/" target="_blank"><img src="https://f1tenth.org/partners/kyungpook.png" height="150px"></a>
<a href="https://www1.lehigh.edu/" target="_blank"><img src="https://f1tenth.org/partners/lehigh.png" height="65px"></a>
<a href="https://www.lunduniversity.lu.se/" target="_blank"><img src="https://f1tenth.org/partners/lund.png" height="200px"></a>
<a href="http://www.mit.edu/" target="_blank"><img src="https://f1tenth.org/partners/mit.png" width="225px"></a>
<a href="http://en.nagoya-u.ac.jp/" target="_blank"><img src="https://f1tenth.org/partners/nagoya.png" height="125px"></a>
<a href="https://www.nyu.edu/" target="_blank"><img src="https://f1tenth.org/partners/nyu.png" width="230px"></a>
<a href="https://www.northwestern.edu/" target="_blank"><img src="https://f1tenth.org/partners/northwestern.png" height="125px"></a>
<a href="https://oregonstate.edu/" target="_blank"><img src="https://f1tenth.org/partners/oregon.png" width="250px"></a>
<a href="https://www.osaka-u.ac.jp/" target="_blank"><img src="https://f1tenth.org/partners/osaka.png" width="250px"></a>
<a href="http://pan.pl/" target="_blank"><img src="https://f1tenth.org/partners/pan.png" width="250px"></a>
<a href="https://www.purdue.edu/" target="_blank"><img src="https://f1tenth.org/partners/purdue.png" width="250px"></a>
<a href="https://www.rutgers.edu/" target="_blank"><img src="https://f1tenth.org/partners/rutgers.png" width="250px"></a>
<a href="https://www.sjsu.edu/" target="_blank"><img src="https://f1tenth.org/partners/sjsu.png" width="250px"></a>
<a href="https://en.snu.ac.kr/" target="_blank"><img src="https://f1tenth.org/partners/seoulnationaluni.png" width="200px"></a>
<a href="https://en.seoultech.ac.kr/" target="_blank"><img src="https://f1tenth.org/partners/seoultech.png" width="200px"></a>
<a href="https://www.stonybrook.edu/" target="_blank"><img src="https://f1tenth.org/partners/stonybrook.png" width="280px"></a>
<a href="https://www.tum.de/en/" target="_blank"><img src="https://f1tenth.org/partners/munich.png" width="280px"></a>
<a href="https://tec.mx/es" target="_blank"><img src="https://f1tenth.org/partners/monterrey.jpg" width="280px"></a>
<a href="https://www.temple.edu/" target="_blank"><img src="https://f1tenth.org/partners/temple.png" height="80px"></a>
<a href="https://www.tu-dortmund.de/en/" target="_blank"><img src="https://f1tenth.org/partners/tu.png" height="100px"></a>
<a href="https://www.uantwerpen.be/en/" target="_blank"><img src="https://f1tenth.org/partners/antwerp.png" width="250px"></a>
<a href="https://www.berkeley.edu/" target="_blank"><img src="https://f1tenth.org/partners/ucb.png" width="220px; padding-left: 20px"></a>
<a href="https://uci.edu/" target="_blank"><img src="https://f1tenth.org/partners/uci.png" width="185px"></a>
<a href="http://www.ucla.edu/" target="_blank"><img src="https://f1tenth.org/partners/ucla.png" width="250px"></a>
<a href="https://www.ucr.edu/" target="_blank"><img src="https://f1tenth.org/partners/ucr.png" width="250px"></a>
<a href="https://ucsd.edu/" target="_blank"><img src="https://f1tenth.org/partners/ucdsd.png" width="220px"></a>
<a href="https://www.ucf.edu/" target="_blank"><img src="https://f1tenth.org/partners/ucf.png" width="250px"></a>
<a href="https://www.ucr.ac.cr/" target="_blank"><img src="https://f1tenth.org/partners/costarica.png" width="250px"></a>
<a href="https://www.colorado.edu/" target="_blank"><img src="https://f1tenth.org/partners/colorado.png" width="250px"></a>
<a href="https://uconn.edu/" target="_blank"><img src="https://f1tenth.org/partners/uconn.png" height="70px"></a>
<a href="https://uiowa.edu/" target="_blank"><img src="https://f1tenth.org/partners/iowa.png" width="200px"></a>
<a href="https://www.umd.edu/" target="_blank"><img src="https://f1tenth.org/partners/maryland.png" width="230px"></a>
<a href="http://www.international.unimore.it/" target="_blank"><img src="https://f1tenth.org/partners/unimore.jpg" width="200px"></a>
<a href="https://www.unl.edu/" target="_blank"><img src="https://f1tenth.org/partners/nebraska.png" width="225px"></a>
<a href="https://www.unm.edu/" target="_blank"><img src="https://f1tenth.org/partners/unm.png" width="250px; padding-left: 60px"></a>
<a href="https://www.unc.edu/" target="_blank"><img src="https://f1tenth.org/partners/northcarolina.png" width="250px; padding-right: 60px"></a>
<a href="https://www.upenn.edu/" target="_blank"><img src="https://f1tenth.org/partners/upenn.png" width="250px; padding-right: 80px"></a>
<a href="https://www.usc.edu/" target="_blank"><img src="https://f1tenth.org/partners/usc.png" width="250px; padding-right: 80px"></a>
<a href="https://www.utexas.edu/" target="_blank"><img src="https://f1tenth.org/partners/unitexas.png" width="250px; padding-left: 40px"></a>
<a href="https://www.utoronto.ca/" target="_blank"><img src="https://f1tenth.org/partners/utoronto.png" width="220px; padding-left: 30px"></a>
<a href="https://www.virginia.edu/" target="_blank"><img src="https://f1tenth.org/partners/virginia.png" width="220px; padding-left: 30px"></a>
<a href="https://www.vanderbilt.edu/" target="_blank"><img src="https://f1tenth.org/partners/vanderbilt.png" width="225px"></a>
<a href="https://www.tuwien.at/en/" target="_blank"><img src="https://f1tenth.org/partners/vienna.png" height="110px"></a>
</p>