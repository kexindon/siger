- [试用期到怎么办？软件逆向第一课——OllyDbg使用](https://gitee.com/g5pik/siger/issues/I503C1#note_13479158_link)

<p><img width="706px" src="https://foruda.gitee.com/images/1665041108783636980/9573b3d4_5631341.jpeg" title="314OllyDbg.jpg"></p>

终于开篇了，辛苦了我们的站长 @g5pik 这个国庆假期，这是密集地补课来了。选这篇是有小私心的，也是对站长偏爱的原因，再看了后面的安全书单，更加坚定以此为旗啦。菌子的频道，转为你开，SIGer 少年 就此立刊啦。

> 话说逆向的小应用注册机，早就伴随者软件的诞生如影随形了。与机器性能和摩尔定律无关，这是软件攻防的一对矛与盾。运行在虚拟机上的加密系统早就已经是标配，而探秘虚拟机的工具，就是逆向软件调试工具啦，给予软件模拟的调试器，给予硬件的系统调试软件，层出不穷，永远逃离不了软件攻防的矛与盾。我们可以化身正义的白帽黑客，也可以躲在暗夜里窃取私密，这无关软件的正义与否，这是科技发展必须承受之重。背景中昏暗的灯光，影影绰绰地代码墙，烟雾缭绕地光影下，键速如飞的程序员，满足了我全部的想象和追忆。只是屏幕上的名字不再是 SoftIce，换成了 OllyDbg，而榜单上 Top 9 的逆向工程软件 IDA pro 依然是榜首，没有之二。致敬曾经的青春。国庆快乐！2022.

# [试用期到怎么办？软件逆向第一课——OllyDbg使用](https://gitee.com/g5pik/siger/issues/I503C1#note_13479158_link)

### 0x01 写在前面

写这篇文章的目的是解释如何通过使用OllyDbg工具在查看源代码的情况下破解一个可执行文件。尽管有很多工具可以实现同样的目标，但OllyDbg背后的魅力在于它操作简单，可以免费使用。在本文中，我们面对的是一个完全未知的应用程序。简单地说，我们没有真正的源代码。我们只有可执行版本（即二进制版本）。对于初学者来说，这将是一项较为繁琐的逆向工程任务。

### 0x02 必备工具

阅读本文需要有一定的计算机常识与汇编语言知识，若要复现本文操作，需要计算机上配置下面的工具：

- OllyDbg
- CFF explorer

### 0x03 二进制文件修改

在不提供源代码的情况下，仍然可以对相应的软件二进制文件进行修改，以消除供应商施加的各种限制。软件中内置的一种熟悉的限制类型是复制保护，这通常是由软件供应商强制实施的，以测试软件复制保护的稳健性。在复制保护中，用户通常需要在使用前注册该产品。供应商对试用版本软件规定了一个时间限制，以避免许可证的滥用，并允许产品在用户注册之前只能以降低功能的模式运行。

### 0x04 可执行文件

下面的例子显示了一种绕过或移除复制保护的方法，以便在不延长试用期或事实上不购买完整版本的情况下使用该产品。复制保护机制通常涉及一个过程，在这个过程中，软件检查它是否应该运行，如果应该运行，哪些功能应该被允许。

试用版或测试版软件中常见的一种复制保护允许程序只运行到某一日期。正如下面这个试用软件的应用程序已经过期，用户无法进一步使用该软件：当我们试图执行它时，它显示了一个错误信息，如下图。

![输入图片说明](https://foruda.gitee.com/images/1664867808506749943/306fbbcc_9961555.png)

现在我们对这个软件一无所知，甚至不知道这个软件是用哪种编程语言或在哪个平台上开发的，所以第一个任务是确定其来源。我们可以使用CFF Explorer，它显示了一些重要的信息：这个软件是用VC++语言开发的，如下图。

![输入图片说明](https://foruda.gitee.com/images/1664867817088423536/b4702f13_9961555.png)

我们可以很容易地得出结论，这是一个本地可执行文件，它不是在CLR下执行的。我们不能使用ILDASM或Reflector来分析其操作代码。这一次，我们必须选择一些不同的方法来破解这个本地可执行文件。

### 0x05 使用OllyDbg进行反汇编

当我们试图加载SoftwareExpiration.exe文件时，它将拒绝运行，因为当前日期已经超过了授权试用期过期的日期。尽管试用期已过，我们怎样才能使用这个软件？下面说明了在取消复制保护限制的情况下的步骤。

- 加载过期的程序，以便了解幕后发生的事情。
- 用OllyDbg调试这个程序。
- 向后追踪代码以确定代码路径。
- 修改二进制文件，强迫所有的代码路径都成功，并且不再打到试用过期的代码路径。
- 测试这些修改。

这样的任务也可以由一个强大的工具IDA Pro来完成，但它是商业性的，不能免费使用。OllyDbg没有IDA Pro那么强大，但在某些情况下是很有用的。首先从它的官方网站下载OllyDbg，并在你的机器上正确配置它。它的界面看起来像这样。

![输入图片说明](https://foruda.gitee.com/images/1664867827978076253/2c123b33_9961555.png)

现在在OllyDbg IDE中从File à open菜单中打开SoftwareExpiration.exe程序，它将反编译该二进制文件。不要害怕怪异的汇编代码，因为所有的修改都在本地汇编代码中进行。

这里的红框显示了程序的入口点指令，被称为00401204。CPU主线程窗口以汇编指令的形式显示软件代码，以从上到下的方式执行。这就是为什么，正如我们前面所说，在对本地可执行文件进行逆向工程时，汇编编程知识是必要的。

![输入图片说明](https://foruda.gitee.com/images/1664867836050144003/51e058c6_9961555.png)

不幸的是，我们没有实际的源代码，那么我们怎样才能检查汇编代码呢？在这里，错误信息 "对不起，这个试用软件已经过期 "可能会帮助我们解决这个问题，因为，在这个错误信息的帮助下，我们可以确定导致它的实际代码路径。

当错误对话框仍在显示时，按F9或从Debug菜单开始调试。现在你可以找到时间限制代码。接下来，按F12以暂停代码的执行，这样我们就可以找到导致显示错误信息的代码。

好了。现在按Alt+K查看调用堆栈。在这里，你可以很容易地发现，试错文本是MessageBoxA的一个参数，如下所示。

![输入图片说明](https://foruda.gitee.com/images/1664867846480610731/d7959ce4_9961555.png)

选择靠近调用堆栈底部的USER32.MessageBoxA，右击，并选择 "显示调用"。

![输入图片说明](https://foruda.gitee.com/images/1664867851841817052/2d37bc04_9961555.png)

这显示了对MessageBoxA的汇编调用被选中的起点。注意到一些代码行旁边的大于号（>），这表明另一行代码会跳到该位置。在调用MessageBoxA的正前方（红色的右窗格），四个参数被推入堆栈。这里的PUSH 10指令包含了大于号，它被另一行代码所引用。

![输入图片说明](https://foruda.gitee.com/images/1664867859977491169/0b1f4a79_9961555.png)

选择位于004011C0地址的PUSH 10指令，引用所选行的代码将显示在CPU窗口顶部窗格下面的文本区域，如下图所示。

![输入图片说明](https://foruda.gitee.com/images/1664867867081310136/b91dbf9a_9961555.png)

选择上图中的文本区代码，右击打开快捷菜单。如图所示，它可以让你轻松地浏览到指的是所选择的一行代码的代码。

![输入图片说明](https://foruda.gitee.com/images/1664867873035620214/e7b88446_9961555.png)

我们现在已经确定了负责产生错误信息的实际代码行。现在是时候对二进制代码做一些修改了。前面图中的上下文菜单显示，00401055和00401063都包含JA（跳到上面）到消息框用的PUSH 10。

首先从上下文菜单中选择转到JA 00401055。你现在应该是在代码的位置0x00401055。你的最终目的是防止程序打到错误代码的路径上。这可以通过将JA指令改为NOP（No Operation, 无操作）来实现，它实际上什么也不做。在CPU窗口内右击0x00401055指令，选择 "二进制"，然后点击填充NOP，如下图。

![输入图片说明](https://foruda.gitee.com/images/1664867883293708816/f574fe1d_9961555.png)

这个操作把0x00401055的所有对应指令都填上了NOP。

![输入图片说明](https://foruda.gitee.com/images/1664867888576842613/2d247166_9961555.png)

按连字符(~)回到PUSH 10，对指令0x00401063重复前面的过程，如下所示。

![输入图片说明](https://foruda.gitee.com/images/1664867898600385709/308011e0_9961555.png)

现在，在CPU窗口中点击右键，点击复制到可执行文件，然后点击所有修改，保存这些修改。然后在下一个对话框中点击复制所有按钮，如下图所示。

![输入图片说明](https://foruda.gitee.com/images/1664867925969697393/28e03588_9961555.png)

![输入图片说明](https://foruda.gitee.com/images/1664867930201415093/6115dc34_9961555.png)

在点击 "复制所有 "按钮后，将出现一个名为 "SoftwareExpiration.exe "的新窗口。在这个窗口中点击右键，选择保存文件。

![输入图片说明](https://foruda.gitee.com/images/1664867936955514801/cb77e03f_9961555.png)

最后，用一个新的名字保存修改或修补的二进制文件。现在加载修改后的程序；你可以看到，这里没有显示试用期已过的错误信息。至此，我们成功地解除了试用期的限制。

![输入图片说明](https://foruda.gitee.com/images/1664867943814672486/b5fb9e1d_9961555.png)

### 0x06 写在最后

本文展示了一种方法，即使用OllyDbg挑战复制保护措施，并确定如何使你的软件更安全地防止未经授权的消费。通过尝试击败你的应用程序的复制保护，我们可以了解到很多关于保护机制的强大程度。通过在产品公开发售前进行这种测试，我们可以在产品发布前修改代码，使规避复制保护更加困难。

---

<p><img width="66%" src="https://foruda.gitee.com/images/1665042438883619216/a75a85f1_5631341.png"> 　　　 <img width="9.9%" src="https://foruda.gitee.com/images/1665042552870748735/8d96ad34_5631341.png"></p>

> Reverse engineering, also called back engineering, is the process by which a man-made object is deconstructed to reveal its designs, architecture, or to extract knowledge from the object; ------ from wikipedia 软件代码逆向主要指对软件的结构，流程，算法，代码等进行逆向拆解和分析。 应用领域 主要应用于软件维护，软件破解，漏洞挖掘，恶意代码分析。 CTF 竞赛中的逆向
  - 软件逆向工程简介 - CTF Wiki ctf-wiki.org/reverse/introduction/ （[笔记](https://gitee.com/g5pik/siger/issues/I503C1#note_13487489_link)）

<p><img width="88%" src="https://foruda.gitee.com/images/1665037669604369705/d975cac3_5631341.png"></p>

- [9 Best Reverse Engineering Tools for 2021 [Updated]](https://www.apriorit.com/dev-blog/366-software-reverse-engineering-tools) （[笔记](https://gitee.com/g5pik/siger/issues/I503C1#note_13487515_link)）

  1. IDA Pro, Hex Rays
  2. CFF Explorer
  3. API Monitor
  4. WinHex
  5. Hiew
  6. Fiddler
  7. Scylla
  8. Relocation Section Editor
  9. PEiD

<p><img width="21%" src="https://foruda.gitee.com/images/1665038069451637896/bef26d33_5631341.jpeg"> <img width="21%" src="https://foruda.gitee.com/images/1665039153740627458/425e3445_5631341.jpeg"> <img width="10%" src="https://foruda.gitee.com/images/1665039462637138920/54327d51_5631341.jpeg"> <img width="13.9%" src="https://foruda.gitee.com/images/1665039491594710242/98add6f8_5631341.png"> <img width="19%" src="https://foruda.gitee.com/images/1665039705955313148/465abf2d_5631341.jpeg"></p>

[How to Market a Business from Scratch - Reverse Engineering](https://localchildcaremarketing.com/reverse-engineering/)  
localchildcaremarketing.com|2508 × 1672 jpeg|Image may be subject to copyright.

[Reverse engineering service for gears and transmissions. | Hindle Gears](https://www.hindlegears.com/expertise/reverse-engineering/)  
hindlegears.com|3543 × 2213 jpeg|Image may be subject to copyright.

[Reverse engineering | NDE Clarke Pitchline | NDE Clarke Pitchline](https://www.ndepower.com/services/engineering-services/reverse-engineering)  
ndepower.com|5900 × 3500 jpeg|Image may be subject to copyright.

[PREVIEW: Introduction to Software Reverse Engineering Techniques ...](https://eforensicsmag.com/download/preview-software-reverse-engineering/)  
eforensicsmag.com|794 × 1123 jpeg|Image may be subject to copyright.

[Reverse Engineering In Software - Metova](https://metova.com/reverse-engineering-in-software/)  
MetovaMetova|1920 × 1440 jpeg|Image may be subject to copyright.
