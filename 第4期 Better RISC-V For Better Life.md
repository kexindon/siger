[![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/204150_0b815631_5631341.jpeg "4th RISC-V 800.jpg")](https://images.gitee.com/uploads/images/2021/0324/194443_80de4424_5631341.jpeg)

# Better RISC-V For Better Life

> @[孔令军](专访/【孔令军】RVWeekly%20SIGer+%20青少年增刊.md) @[PicoRio](RISC-V/PicoRio.md) @[浪潮RISC-V小组](RISC-V/riscv.md) @[RV与芯片评论](RISC-V/rvnews.md) @[CRVA](RISC-V/crvs2020.md) @[RV4Kids](RISC-V/RV4Kids.md) 依照本期 SIGer 专刊的 《[开源芯片 RISC-V](https://gitee.com/yuandj/siger/issues/I3B6OG) 》的主题，一篇一篇 md 入库。[JOHN L. HENNESSY DAVID A. PATTERSON](RISC-V/JOHN-L-HENNESSY-DAVID-A-PATTERSON.md) [COMPUTER ARCHITECTURE](RISC-V/a-new-golden-age-for-computer-architecture.md) 这些关键字映入眼帘，没有比这个时刻更加激动人心的啦，一个崭新的[黄金时代](RISC-V/a-new-golden-age-for-computer-architecture.md)到来啦。

每期专刊封笔，我都要如此感慨一通，这是一份感谢，从[小孔老师](专访/【孔令军】RVWeekly%20SIGer+%20青少年增刊.md)造访我的空间，只4天时间，这一幕壮丽情景就展现在了我的面前。这都是因为他的辛苦耕耘 —— [RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) ，我和他在平行空间做着同样的事，如今交汇于一点，那就是 [RV4Kids](RISC-V/RV4Kids.md) 青少年科普。这都是 RVWeekly 总34期的功劳，雷打不动 RISC-V与芯片评论编辑部 - RISC-V和芯片动态周报 每周六准时发布的功劳。

初次回访 “[浪潮RISC-V研究小组](https://gitee.com/inspur-risc-v/)” 给人一个企业内部项目的感觉，打开 [RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) 的姊妹篇 [RVNews](RISC-V/rvnews.md) 一个详细的 Wiki 类项目展现在我面前。详尽周到，一个团队的成果，确只有小孔一人在维护。就好像我这个老师体恤学生们的课业负担，但当了大部分的 SIGer 编校约访的工作。为了与社区共享，小孔老师从他擅长的语雀平台搬迁了一份到 Gitee 。我提交了两个 PR 后，就这样和小孔老师成了朋友。

在我发出 [SIGer](RISC-V/SIGer-RVWeekly-plus.md) 合刊 RISC-V 主题，邀请[小孔老师](专访/【孔令军】RVWeekly%20SIGer+%20青少年增刊.md)担任 [RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) 增刊的[共同编审](RISC-V/SIGer-RVWeekly-plus.md)后，他欣然答应啦。原本课抽空回复，周末当晚他就整整回答了我 11 个大问题，彻底给我科普了一遍 RISC-V，我也先同学们一步踏入了这个[黄金时代](RISC-V/a-new-golden-age-for-computer-architecture.md)的大门。吮吸知识是让人无法自拔的，仅仅4天，这期 RISC-V 专题就成稿啦。而且诞生了一个全新的科普品牌 [RV4Kids](RISC-V/RV4Kids.md)。甚幸 :pray:

以排比句两个 Better 命题是效仿时代杂志对 AlphaGo 缔造者的个人专访 “[Better Chess For Better Life](RISC-V/BetterChess4BetterLife.md)” 。以相同的视角借用少年天才的成长，比照 [RV4Kids](RISC-V/RV4Kids.md) 的愿景，以期能够举全社区的力量来支持青少年科普。而封面的[两位 “大佬” ](RISC-V/JOHN-L-HENNESSY-DAVID-A-PATTERSON.md)的那份率真笑容，和 [RV4Kids](RISC-V/RV4Kids.md) 的[两位小同学](RISC-V/risc-v-microarchitecture-for-kids-steve-hoover-redwood-eda.md)的笑容又是无比相像啊，就和少年哈萨比斯无异，秉承着对科学的好奇心，执着攀登，抵达顶峰后再立新功，永无止境的劲头。这就是我向[小孔老师](专访/【孔令军】RVWeekly%20SIGer+%20青少年增刊.md)展示的愿景。 

## 本期提要：
（秉承两刊网摘之风格）

- [SIGer-RVWeekly-plus.md](RISC-V/SIGer-RVWeekly-plus.md) SIGer 编审邀请信
- [【孔令军】RVWeekly SIGer+ 青少年增刊.md](专访/【孔令军】RVWeekly%20SIGer+%20青少年增刊.md) @孔令军
- [PicoRio.md](RISC-V/PicoRio.md) @PicoRio
- [riscv.md](RISC-V/riscv.md) @浪潮RISC-V小组
- [rvnews.md](RISC-V/rvnews.md) @RV与芯片评论
- [crvs2020.md](RISC-V/crvs2020.md) @CRVA
- [RV4Kids.md](RISC-V/RV4Kids.md) @RV4Kids
  - [BetterChess4BetterLife.md](RISC-V/BetterChess4BetterLife.md) AlphaGo DeepMind 的缔造者 少年天才 “哈萨比斯” 的传奇故事。
  - [risc-v-microarchitecture-for-kids-steve-hoover-redwood-eda.md](RISC-V/risc-v-microarchitecture-for-kids-steve-hoover-redwood-eda.md)
  - [13-year-old-nicholas-sharkey-creates-a-risc-v-core.md](RISC-V/13-year-old-nicholas-sharkey-creates-a-risc-v-core.md)
  - [riscv-MYTH-Workshop.md](RISC-V/riscv-MYTH-Workshop.md) RISC-V 入门工作坊
  - [risc-v-learn-online.md](RISC-V/risc-v-learn-online.md)
- [JOHN-L-HENNESSY-DAVID-A-PATTERSON.md](RISC-V/JOHN-L-HENNESSY-DAVID-A-PATTERSON.md) 共同荣获 2017年 ACM AM图灵奖
  - [A new golden age for computer architecture](https://cacm.acm.org/magazines/2019/2/234352-a-new-golden-age-for-computer-architecture/fulltext) 《计算机体系结构的新黄金时代》（[中文译文](RISC-V/计算机体系结构的新黄金时代.md)）

## [RV4Kids](RISC-V/RV4Kids.md)：

我极力地推荐 [RV4Kids](RISC-V/RV4Kids.md) 这个崭新的教育品牌，这是我和 [RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) 主编[孔令军](专访/【孔令军】RVWeekly%20SIGer+%20青少年增刊.md)先生共同推荐的一个科普项目，我们邀请 RISC-V 的全体同仁和社区都展开双臂拥抱青少年，您只需要在您的实践项目（开源项目）中 @[RV4Kids](https://gitee.com/RV4Kids) 字样，尽您所能，在您开展的 RISC-V 相关的活动，课程，工作坊，等等可以吮吸 RISC-V 知识的场合，为青少年提供便利，允许他们参与，或安静地来，悄悄地走，或积极提问，求知若渴。谁能知道，不远的将来，他们中的某个人会成为您的同事，甚至战友。我要借用小孔老师的话 “ **身负强国使命，与一众小伙伴各种厮杀** ” 呢？

> 请读者通过阅读本刊，找到本刊关键字，它隐藏在封面原图的照片中，  
> 如果您能找到 @袁德俊 私信索取礼物，或者询问线索也可。  
> 袁老师的礼物均是社区小朋友亲手自制，礼轻情意重，希望您能喜欢。

### 更多期刊预报

- [RISCV-CHINA WEEKLY](https://gitee.com/RV4Kids/RVWeekly/issues/I3D4UK) ：作为 RVWeekly 的日常工作，每周六刊发的内容源之一 riscv.org 的一周新闻速览 将成为同学们参与开源社区的很好方式
- [openEuler sig-RISCV](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ) ：作为 SIGer 编委会 回报 openEuler 社区的专题，预留 #10 号期刊，以期再一轮开源科普的快速增长。

> 诚意邀请志愿者（同学）加入 SIGer 的共建共享，两期预告中，RISCV-CHINA 周刊将完全有学生主导担任 SIGer 编审的依然是 @[lj-k](https://gitee.com/lj-k) 孔老师，sig-RISCV 的编审则邀请 @[zhoupeng01](https://gitee.com/zhoupeng01) 周老师支持。自本期开始，[SIGer 大使](RISC-V/SIGer-RVWeekly-plus.md)计划正式发布。