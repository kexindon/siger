- [MagPi 2023](https://magpi.raspberrypi.com/issues/124/pdf) : [RASPBERRY PI BUYERS GUIDE](https://gitee.com/shiliupi/shiliupi99/issues/I64HGS)
- [Made in Kenya 肯尼亚制造](https://www.raspberrypi.com/news/made-in-kenya)
- [CRT TV + VCR Trinitron Retro Media Player](https://www.raspberrypi.com/news/crt-tv-vcr-trinitron-retro-media-player-the-magpi-124/)

<p><img width="706px" src="https://foruda.gitee.com/images/1670151748811265482/e52fc99b_5631341.jpeg" title="488Magpi2023.jpg"></p>

> Made in China 的自豪感随着中国的产业升级，开始转变了，越南，东南亚的建厂，如今的非洲建厂，这个世界正在悄然发生着变化。[Made in Kenya](https://www.raspberrypi.com/news/made-in-kenya) 这是一个重要的信号，也是一个里程碑，Mark 一下，也是为树莓派点赞 :+1: 没有比 Free DownLoad 之前的友情提示更 Nice 的啦！一如既往的支持。

[![输入图片说明](https://foruda.gitee.com/images/1670142794881920339/64f6d768_5631341.png "屏幕截图")](https://magpi.raspberrypi.com/)

# [Before you download…](https://magpi.raspberrypi.com/issues/124/pdf)

We are able to offer these resources for free thanks to our supporters around the world. You can support our work by making a contribution of any size.

- Monthly
- One-Time

GBP £ 

- £3
- £10
- £20

EUR €

- €3
- €10
- €20

USD $

- $3
- $10
- $20

$6.9

Email address*:  

> [ ] Send future issues of The MagPi directly to my inbox, along with occasional news and offers from Raspberry Pi Press.

- Contribute

- [No thanks, take me to the free PDF](https://magpi.raspberrypi.com/issues/124/pdf/download)

By contributing, you agree to Raspberry Pi Ltd’s [Terms of Service](https://magpi.raspberrypi.com/terms-and-conditions-of-contributions) and [Privacy Policy](https://www.raspberrypi.com/privacy/).

### Thank you!

Your contribution means we can make these resources available to everyone, everywhere, regardless of ability to pay.

Your download of  **The MagPi issue 124**  should begin automatically.

If your download doesn't start, [click here to get your PDF](https://magpi.raspberrypi.com/issues/124/pdf).

### Subscribe to the newsletter

Get every issue delivered directly to your inbox and keep up to date with the latest news, offers, events, and more.

Email address:    **Sign up** 

### [The MagPi — Issue 124](https://magpi.raspberrypi.com/issues/124/)？

![输入图片说明](https://foruda.gitee.com/images/1670125290090568229/602a981e_5631341.png "屏幕截图")

[Download free PDF](https://magpi.raspberrypi.com/issues/124/pdf)

Inside The MagPi magazine issue #124

-  **Raspberry Pi Buyers Guide 2023:**  Your ultimate guide to hardware, accessories and project kits.

-  **Create your own maker space:**  build a haven in your home for making

-  **Hack a CRT TV & VCR:**  give a classic television combo modern smarts with Raspberry Pi

-  **Make a game in MicroPython:**  program a tiny survival game for Pico

-  **10 amazing holiday projects:**  last-minute ideas for the festive season Win! A MicroPi Starter Kit: we love this robotics and computer kit

[View PDF of contents](https://magpi.raspberrypi.com/downloads/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBc1llIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--7bf9e5134404fda96f79b45490e5f5721845f0ff/005-006_MagPi124_CONTENTS_sample.pdf)

[![输入图片说明](https://foruda.gitee.com/images/1670142355542842844/14c05369_5631341.png "036-037_MagPi124_COVER_FEATURE_Buyers_guide_sample.jpg")](https://magpi.raspberrypi.com/storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBc2dlIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--f2fe852a28addcc9f852c0c5b09dfed324e71b23/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJYW5CbkJqb0dSVlE2RW5KbGMybDZaVjkwYjE5bWFYUmJCMmtDQ0FjdyIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--387412fca9fd2649345ddbd40efa3410e3c65ed8/036-037_MagPi124_COVER_FEATURE_Buyers_guide_sample.jpg) [![输入图片说明](https://foruda.gitee.com/images/1670142361942546077/9b482091_5631341.png "066-071_MagPi124_SECONDARY_FEATURE_sample.jpg")](https://magpi.raspberrypi.com/storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBc2tlIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--ef76b3f4f39aa324cb77b8d556c3ff4b3346c851/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJYW5CbkJqb0dSVlE2RW5KbGMybDZaVjkwYjE5bWFYUmJCMmtDQ0FjdyIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--387412fca9fd2649345ddbd40efa3410e3c65ed8/066-071_MagPi124_SECONDARY_FEATURE_sample.jpg)   
[![输入图片说明](https://foruda.gitee.com/images/1670142368853394146/373ae97e_5631341.png "008-009_MagPi124_SHOWCASE_CRT_TV_sample.jpg")](https://magpi.raspberrypi.com/storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBc29lIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--0708b173734d6e014842f938ca6252078bea9a19/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJYW5CbkJqb0dSVlE2RW5KbGMybDZaVjkwYjE5bWFYUmJCMmtDQ0FjdyIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--387412fca9fd2649345ddbd40efa3410e3c65ed8/008-009_MagPi124_SHOWCASE_CRT_TV_sample.jpg) [![输入图片说明](https://foruda.gitee.com/images/1670142374388659343/93be7552_5631341.png "054-059_MagPi124_TUTORIAL_Arc Reactor_sample.jpg")](https://magpi.raspberrypi.com/storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBc3NlIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--69100209655738bc6136ffda14e3d4e25815fb48/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJYW5CbkJqb0dSVlE2RW5KbGMybDZaVjkwYjE5bWFYUmJCMmtDQ0FjdyIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--387412fca9fd2649345ddbd40efa3410e3c65ed8/054-059_MagPi124_TUTORIAL_Arc%20Reactor_sample.jpg)

- [036-037_MagPi124_COVER_FEATURE_Buyers_guide_sample.jpg](https://foruda.gitee.com/images/1670144122758693220/653508dd_5631341.jpeg)
- [066-071_MagPi124_SECONDARY_FEATURE_sample.jpg](https://foruda.gitee.com/images/1670144145479447990/e8240b4f_5631341.jpeg)
- [008-009_MagPi124_SHOWCASE_CRT_TV_sample.jpg](https://foruda.gitee.com/images/1670144165339451160/da8337dc_5631341.jpeg)
- [054-059_MagPi124_TUTORIAL_Arc Reactor_sample.jpg](https://foruda.gitee.com/images/1670144189353057447/2eb47289_5631341.jpeg)

# [Made in Kenya](https://www.raspberrypi.com/news/made-in-kenya)

![输入图片说明](https://foruda.gitee.com/images/1670144436718867508/9c43bf67_5631341.png "屏幕截图")

21st Nov 2022 | [Eben Upton](https://www.raspberrypi.com/news/author/eben) | 13 comments

We’re delighted to say that, as of this week, we’re manufacturing Raspberry Pi Pico for the African market in Kenya, at Gearbox Europlacer in Nairobi.

![输入图片说明](https://foruda.gitee.com/images/1670125354809412126/cb66e9f9_5631341.png "kenya4.png")

Those of you who’ve been with us since the start of the Raspberry Pi story will recall that we began building Raspberry Pis in China in 2012. We immediately started work on reshoring, and built our first UK‑manufactured Raspberry Pi at the [Sony UK Technology Centre](https://www.sonypencoed.co.uk/) in Pencoed, South Wales, in the summer of that year.

### Reshoring

Over the following decade, we progressively relocated our production to the UK, to the point where today almost all Raspberry Pi products are manufactured four hours down the road from Pi Towers in Pencoed: from there we export them to customers all over the world. Notable exceptions are Pico W, which hails from Sony Inazawa in Japan; and the PoE+ HAT, which is built by our friends at Dongguan Taijie in China.

![输入图片说明](https://foruda.gitee.com/images/1670144532043116330/b02042da_5631341.png "kenya3.png")

Building our products close to home allows us to iterate quickly, applying lessons learned in production to make them simpler, and cheaper, to produce. And having a factory in the UK, which is one of our largest markets, simplifies logistics and lets us respond quickly to changes in demand.

### Offshoring

So it’s more than a little surprising to be writing an offshoring announcement. Starting this week, we’ll be building Raspberry Pi Pico products for the African market at [Gearbox Europlacer](https://www.gearbox-europlacer.com/) in Nairobi, part of the [Gearbox](https://www.gearbox.co.ke/) ecosystem. Roger is in Kenya now, helping the team there bring up the manufacturing process, and we’re expecting him to return with a freshly baked batch of “Made in Kenya” Picos by the end of the week.

![输入图片说明](https://foruda.gitee.com/images/1670144668573589285/778f18c2_5631341.jpeg "FhssADhXEAEH_eX.jpeg")

> Image from @[LatiffCherono](https://twitter.com/LatiffCherono)

### Gearbox Europlacer

Mike and I had the opportunity to visit Gearbox last month, to meet with its founder Dr Kamau Gachigi, and to tour the Europlacer facility with Latiff Cherono, its manager. What we found there was a state-of-the-art surface-mount assembly line, which in principle could be used to build almost any Raspberry Pi product. We’re starting with [Pico](https://www.raspberrypi.com/products/raspberry-pi-pico/), our smallest, simplest, lowest-cost product, but in due course hope to add others, including Pico W and Zero 2 W.

![输入图片说明](https://foruda.gitee.com/images/1670144875007285030/c728bc9d_5631341.png "kenya1.png")

Why are we doing this? As with the reshoring of production to Wales, it’s a happy coincidence of self-interest and a desire to support electronic manufacturing in a country we care deeply about. By bringing parts rather than finished goods into Kenya, we pay lower import duties, and benefit from other government incentives; and by stockpiling components locally, we will be able to react more quickly to the rapidly increasing demand for our products in east Africa.

### Approved Reseller partners in Africa

Our relationship with Gearbox Europlacer has grown out of the work we’ve been doing to build out our network of Approved Reseller partners in Africa. Since Ken Okolo joined us in May last year to run our Africa team, we’ve added eleven new resellers, and we’re on track to have at least one reseller in each country on the continent by 2025.

The scale of the opportunity in Africa is vast, and after seven days in Ghana and Kenya I’m definitely a convert. During our trip, we met the Ghanaian Minister of Education, Dr Yaw Osei Adutwum (at 6.30am!), and visited [STL Semiconductor](https://stlsemiconductor.com/), a silicon wafer processing facility in Nyeri, Kenya. Everywhere we went, the energy and enthusiasm we encountered, not to mention the welcome we received, reminded us of the earliest days of Raspberry Pi in the UK.

Within a few years, I believe the African Raspberry Pi ecosystem will be the equal of what we’ve built together in our more mature markets.

### 13 comments

- Odhiambo WASHINGTON | 21st November 2022, 11:50 am

  > This is great news. I hope this grows to a full-scale production of the RPIs product range, and to have them sold locally as well. We’ve waited for so long to have the govt type approval for Pi Zero 2W. Maybe this development will now jump over that obstacle.

  > Definitely a great move by the Raspberry Pi Foundation toward bringing affordable computing to Africa.

- Magaji Iliya | 21st November 2022, 6:03 pm

  > I’m really interested in this but I don’t know how it operate

    - Odhiambo | 22nd November 2022, 3:52 pm

      > You can find HOWTOs for so many projects you can do with the Pico.

- Kiprotich Chemweno | 21st November 2022, 6:26 pm

  > Wonderful news to hear, looking forward to electronics manufacturing taking shape in Kenya.

- Robert Aldeton | 22nd November 2022, 1:43 am

  > This will be phenomenal for the local economy. May you start a whole new ecosystem of players coming to the region. There is so much potential with great people there.

- Ens | 22nd November 2022, 6:50 am

  > Good day! Great news!  
  > But more than a year has passed since the disappearance of RPis from stores, when will it return?

- Damien | 22nd November 2022, 8:11 am

  > Wow, great news as Pi’s are just so useful. I work on a mine site in Western Australia, there is a simple process that is managed by a 3B or similar. Smiled & laughed when I saw it in its case.

- celso PINHEIRO | 22nd November 2022, 1:12 pm

  > That’s the action that has a multiplier for the region greater than 10 NGOs…

- Will Smith | 22nd November 2022, 5:14 pm

  > And here I am in North America waiting to get a 0-W-2 for under $150 bucks…

- Brian | 23rd November 2022, 11:07 am

  > Best news in a while for me.  
  > I hope this gains traction. I also hope that the price is very competitive to allow mass adoption.

  > Working with ICT authority, I will share my optimism for this project with him, hopefully get it involved in the next phase of digital literacy program

- Esther | 23rd November 2022, 12:07 pm

  > This is great news, I use Rpi 4 for my school project and it was a hustle to find one leave alone the peripherals like pi camera.  
  > This will be so great for Kenya and Africa in general.  
  > I have to keep checking the career page and hope I join the team as one of the on-site workers from Kenya.

- myles okumu | 24th November 2022, 9:01 am

  > This is amazing

- Samuel Chege | 3rd December 2022, 9:27 am

  > Good work and strategy. Kenya is ready!

# 肯尼亚制造

- 2022年11月21日 
- 埃本厄普顿
- 13个评论

我们很高兴地宣布，从本周开始，我们将在内罗毕的Gearbox Europlacer为非洲市场在肯尼亚生产树莓派。

> Pico Made in Kenya Africa

从树莓派的故事开始就跟随我们的人应该记得，我们从2012年开始在中国生产树莓派。我们立即开始回岸工作，并在那年夏天在南威尔士彭科德的索尼英国技术中心建造了我们在英国制造的第一个树莓派。

### 重新支撑

在接下来的10年里，我们逐渐将生产转移到英国，以致于今天几乎所有树莓派产品都是在距离Pencoed的Pi塔4小时车程的地方生产的:从那里我们将产品出口到世界各地的客户。值得注意的例外是来自日本Sony Inazawa的Pico W;还有PoE+ HAT，这是我们在中国东莞泰捷的朋友制造的。

> Pico Made in Kenya Africa

在离家近的地方制作我们的产品可以让我们快速迭代，应用在生产过程中吸取的经验教训，使产品生产更简单、更便宜。英国是我们最大的市场之一，在英国设厂可以简化物流，让我们对需求的变化迅速做出反应。

### 离岸外包

所以写一份离岸公告实在是太让人惊讶了。从本周开始，我们将在内罗毕的Gearbox Europlacer为非洲市场开发树莓派Pico产品，这是Gearbox生态系统的一部分。罗杰现在在肯尼亚，帮助那里的团队提高生产工艺，我们希望他能在本周末带着一批新鲜出炉的"肯尼亚制造"皮卡回来。

> Pico Made in Kenya Africa  
> 从图片 @LatiffCherono

### 变速箱Europlacer

迈克和我上月有机会参观了Gearbox，会见了其创始人卡马乌·加奇吉博士，并与该公司经理拉提夫·切诺(Latiff Cherono)一起参观了Europlacer的工厂。我们在那里发现了一条最先进的表面安装装配线，原则上可以用来制造几乎所有树莓派产品。我们从Pico开始，这是我们最小、最简单、成本最低的产品，但希望在适当的时候添加其他产品，包括Pico W和Zero 2 W。

> Pico Made in Kenya Africa

我们为什么要这么做?就像将生产迁回威尔士一样，这是一个自身利益和希望在我们非常关心的国家支持电子制造业的愉快巧合。通过将零件而不是成品进口到肯尼亚，我们支付了较低的进口关税，并受益于政府的其他激励措施;通过在当地储存零部件，我们将能够更快地应对东非地区对我们产品快速增长的需求。

### 经批准的非洲经销商合作伙伴

我们与Gearbox Europlacer的关系源自于我们一直在非洲建立的认可经销商合作伙伴网络。自从Ken Okolo去年5月加入我们，管理我们的非洲团队以来，我们已经增加了11个新的经销商，我们正朝着2025年在非洲大陆的每个国家至少拥有一个经销商的目标前进。

非洲的机会是巨大的，在加纳和肯尼亚呆了七天之后，我绝对是一个皈依者。在行程中，我们会见了加纳教育部长Yaw Osei Adutwum博士(上午6点半!)，并参观了位于肯尼亚尼耶里的硅晶圆加工工厂STL Semiconductor。无论我们走到哪里，我们所遇到的能量和热情，更不用说我们所受到的欢迎，都让我们想起了树莓派最早在英国的日子。

我相信在几年内，非洲树莓派的生态系统将与我们在更成熟的市场上共同建立的生态系统不相上下。

# [CRT TV + VCR Trinitron Retro Media Player](https://www.raspberrypi.com/news/crt-tv-vcr-trinitron-retro-media-player-the-magpi-124/) | The MagPi #124

![输入图片说明](https://foruda.gitee.com/images/1670148136435013880/39457039_5631341.png "屏幕截图")

24th Nov 2022 | [Lucy Hattersley](https://www.raspberrypi.com/news/author/lucy-hattersley) | 8 comments

 **_When Mairon Wolniewicz watches movies or plays games from the 1970s, 1980s, and 1990s, he ensures he gets the full retro experience, as David Crookes explains in the latest issue of The MagPi._** 

![输入图片说明](https://foruda.gitee.com/images/1670125421103033170/54978653_5631341.png "Screenshot-2022-11-23-at-17.54.10.png")

> The original plan was to place the Raspberry Pi computer within a stripped-out VCR space, but this method allows it to be easily unclipped and plugged into an HDMI monitor for debugging

Where is no doubt that a crisp, modern, flat display has been a blessing for our eyes, not to mention the space in our homes, schools, and offices. But if you’re a retro enthusiast and you have old video games or movies from the last century, then you may want to go the whole hog and grab yourself a cathode-ray tube (CRT) television instead.

That’s exactly what [Mairon Wolniewicz](https://github.com/MaironW) has done, creating a media server based around a Raspberry Pi computer that outputs to an old Sony Trinitron CRT he picked up following months of searching. As it happens, these TVs were created in the Sony UK Technology Centre factory in Pencoed, south Wales, where Raspberry Pi is produced today, but Mairon was simply after a quality television set.

![输入图片说明](https://foruda.gitee.com/images/1670148205132235919/02ea924a_5631341.png "屏幕截图")

> To keep Raspberry Pi in place and easily accessible, Mairon has made good use of the grill on the back of the TV

“I saw these were the best ever CRTs and one appeared out of nowhere with a built-in VCR,” he says. “The seller didn’t know if the VCR was working, but I wasn’t bothered because my idea was to remove the mechanical parts that occupied the VCR space and use it to store the cables, power supply, and Raspberry Pi.”

### Picture perfect

In that sense, he had a bold plan. “Since the television only had composite and RF video inputs, I considered connecting the internal wirings of the A/V plug – the ones on the front of the TV – into a Raspberry Pi RCA cable. It would involve cutting out the RCA connectors and soldering the wires directly to the TV,” he adds. But then curiosity got the better of him and he bought a copy of Shrek on VHS. To his surprise, it worked!

<p><img width="36.69%" src="https://foruda.gitee.com/images/1670148246858751918/d20299e2_5631341.png"> <img width="38%" src="https://foruda.gitee.com/images/1670148253140627199/4370e467_5631341.png"></p>

- It’s not quite as polished as Kodi, but that’s deliberate. It looks like Mairon loves 1980s horror flicks!
- Mairon’s Shrek physical VHS tape playing perfectly on the TV – the reason why he didn’t open up his TV (warning: don’t forget that working inside old CRT televisions is dangerous)

He then resolved to revise his project, deciding, as a lover of retro, that he simply couldn’t scrap the VCR element of the TV. Instead, he opted to place the [Raspberry Pi 4](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/) computer into a case that he clipped on to the grid at the back of the TV. He admits it’s not the most elegant of solutions (“the composite inputs are on the front of the TV,” he laments), but his retro media player could still make use of the CRT display.

So what has motivated him to do this? “Well, I’ve been spending a lot of time learning how to paint by watching videos by Bob Ross [an American painter and TV host who died in 1995], and I thought it would be cool to just let videos like these play in a loop on a small screen in my room,” he explains. “Soon, I was also looking to add other functions. My Raspberry Pi came with SNES controllers, so playing games was inevitable.”

![输入图片说明](https://foruda.gitee.com/images/1670148298537137149/e1c30ba0_5631341.png "屏幕截图")

> Mairon had been a long time user of RetroPie and EmulationStation, so decided to stick with it rather than create his own emulator/game selector

### Play it again

This is when the project took off. The setup was straightforward (videos and games are stored on a USB stick and video output is via a P3/RCA connector), but Mairon’s retro media system has a custom GUI resembling a VCR interface – complete with white text on a blue background.

“I thought it would be cool to have a GUI where I’d choose what I wanted to do with my media player, and I thought the VCR look would match nicely,” he says. This was accomplished using Qt QML, a declarative language for designing user interfaces. “C++ was used for file reading and to launch Linux commands,” he adds.

![输入图片说明](https://foruda.gitee.com/images/1670148326118544807/529c5198_5631341.png "屏幕截图")

> This retro media system is fundamentally designed to be plug-and-play, with the software used to launch the correct applications

Most of Mairon’s time has been spent tweaking Raspberry Pi configuration files. “It will run fine with composite output, on the desired resolution with legible font and without overscan,” he says. Indeed, the system is configured to output video to 480i, and it’s set up to correctly run music and games.

It’s navigated using the up/down arrow and ENTER keys on a keyboard, with the BACKSPACE key for returning to a previous menu. “I’d like the GUI to support gamepads, and I’d also like to integrate a personal assistant,” he says. “Sure, it’s not even close to retro, but wouldn’t it be cool to request your TV to play a season of an old show when you lie down on your bed?”

<p><img width="36.69%" src="https://foruda.gitee.com/images/1670148343495587957/b6124667_5631341.png"> <img width="26.16%" src="https://foruda.gitee.com/images/1670148349514257143/53426846_5631341.png"> <img width="35%" src="https://foruda.gitee.com/images/1670148356019808626/ab66aff7_5631341.png"></p>

### The MagPi #124 out NOW!

You can grab the brand-new issue right now from Tesco, Sainsbury’s, Asda, WHSmith, and other newsagents, including the [Raspberry Pi Store](https://www.raspberrypi.com/raspberry-pi-store/) in Cambridge. You can also get it via our app on [Android](https://play.google.com/store/apps/details?id=com.raspberry.magpi) or [iOS](https://itunes.apple.com/us/app/the-magpi-magazine/id972033560?ls=1&mt=8). And there’s a [free PDF](https://magpi.raspberrypi.com/issues/124) you can download too.

![输入图片说明](https://foruda.gitee.com/images/1670148384173593931/6bad585d_5631341.png "屏幕截图")

You can also [subscribe](https://magpi.cc/subscribe) to the print version of The MagPi. Not only do we deliver it globally, but people who sign up to the twelve-month print subscription get a FREE Raspberry Pi Zero Pico W!

---

![输入图片说明](https://foruda.gitee.com/images/1670150556384651393/349aef08_5631341.jpeg "MagPi124-1.jpg")

![输入图片说明](https://foruda.gitee.com/images/1670150569079803757/1417f2ea_5631341.jpeg "MagPi124-78.jpg")