- [CompuServe](http://www.trs-80.org/compuserve/) : [Special Interest Groups](../专访/【熊伟】没有SIG的开源项目不是好社区？.md#special-interest-groups)
- [The 80 Micro Young Programmer's Contest](http://www.trs-80.org/young-programmers-contest/)
- [SKY Delta Airlines inflight magazine, June 1984](http://www.teitown.com/Delta_Airlines.htm)
- [Radio Shack TRS-80 Model III Microcomputer](https://www.nightfallcrew.com/21/02/2014/radio-shack-trs-80-model-iii-microcomputer/)

<p><img src="https://foruda.gitee.com/images/1665680706607194585/c224b63f_5631341.jpeg" title="331TRS-80.jpg"></p>

> 全天下的小朋友都一样，80年代的妈妈和现在鸡娃的爹妈也无二样，区别的是孩子们手里的电脑早就摩尔定律啦。而开源和孩子们的大脑并没有摩尔定律，因为它们同属于精神层面的范畴，不能简单地复刻，至少现在还不能用脑机训练一个鸡娃 :D

原文转载是因为，不需要过多的解释，看这简介的界面和文字，信息量足够了。全民开源素养计划，不只缘起80年代，还将从精神到物质全面重温那个 “崭新的时代”。有心的同学，一定能发现其中的蛛丝马迹，且听我慢慢拉开《全面开源素养计划》的大幕吧。

<p class="title"><a href="http://www.trs-80.org/">Matthew Reed's TRS-80.org</a></p>

- <a href="http://www.trs-80.org/categories/computers/">Computers</a>
- <a href="http://www.trs-80.org/categories/arcade-games/">Arcade Games</a>
- <a href="http://www.trs-80.org/categories/interviews/">Interviews</a>
- <a href="http://www.trs-80.org/categories/hardware/">Hardware</a>
- <a href="http://www.trs-80.org/categories/books/">Books</a>
- <a href="http://www.trs-80.org/categories/software/">Software</a>
- <a href="http://www.trs-80.org/categories/magazines/">Magazines</a>

# [The 80 Micro Young Programmer's Contest](http://www.trs-80.org/young-programmers-contest/)

> written by Matthew Reed

![输入图片说明](https://foruda.gitee.com/images/1665673011844842189/3c785043_5631341.png "80 Micro February 1984")

> Cover of the February 1984 issue of <a href="http://www.trs-80.org/80-microcomputing/"><em>80 Micro</em></a> containing the results of the second Young Programmer’s Contest

For three years starting in 1982, the TRS-80 magazine <a href="http://www.trs-80.org/80-microcomputing/"><em>80 Micro</em></a> held a contest to encourage young people to program. It was known as the “80 Micro Young Programmer’s Contest.” Anyone under the age of 18 could submit a program for a chance at winning prizes. <em>80 Micro</em> published the winning contest entries in the February issue each year.<p></p>
<p>The entries, which numbered over 200 the first two years, were judged by the <em>80 Micro</em> editorial staff based on five categories: programming elegance, documentation, originality, error-trapping, and usefulness. Most of the winning entries were written for the TRS-80 Model I and III, but there were also several for the Color Computer, and even one each for the Model II, Model 4, and Model 100.</p>
<p>Prizes were awarded in three age categories:</p>
<ul>
<li>10 years and under</li>
<li>11 years through 13 years</li>
<li>14 years through 18 years</li>
</ul>
<p>There were ten money prizes for winning entries:</p>
<ul>
<li>One Grand Prize of $300 plus publication</li>
<li>Three First Prizes of $200 plus publication</li>
<li>Three Second Prizes of $100 plus publication</li>
<li>Three Third Prizes of $50 plus publication</li>
</ul>
<p>In addition, there were Honorable Mentions worth a free subscription to <em>80 Micro</em> plus possible publication.</p>
<p>Those prize money amounts didn’t include the usual payment for publication. For <em>80 Micro</em> at the time, this was around $50 per printed page.</p>

## The 1982-1983 Young Programmer’s Awards

<p>The results of the first Young Programmer’s Contest were published in the February 1983 issue of <em>80 Micro</em>. Of the eleven winning entries (there was one tie), three were for the Color Computer and the rest were for the Model I or III.</p>
<p>All the winning entries were published in the February 1983 issue of <em>80 Micro</em>.</p>

### Grand Prize

<ul>
<li>Quest for the Key of Nightshade by David Schmidt</li>
</ul>

### 14 years to 18 years

<ul>
<li>First Prize: Project Deep Dive by Michael John Lake</li>
<li>Second Prize: TRS-Turtle by Larry Brackney</li>
<li>Third Prize: Boxing Game by Lloyd Kupchanko</li>
</ul>

### 11 years to 13 years

<ul>
<li>First Prize: Music Composer by Carl Huben (for the Color Computer)</li>
<li>Second Prize: Lair of Kraken by Beth Norman (for the Color Computer)</li>
<li>Third Prize: a tie between Foreign Flag Quiz by Jennifer Neidenbach and CASS-80 by Scott Steele</li>
</ul>

### 10 years and under

<ul>
<li>First Prize: Super-Draw by Terry Myerson</li>
<li>Second Prize: Byte-Cycles by Nathan Miller (for the Color Computer)</li>
<li>Third Prize: Math Countdown by Adam Wells</li>
</ul>
<p>There were no Honorable Mentions listed.</p>

## The 1983-1984 Young Programmer’s Awards

<p>For the second year of the contest, the prizes and rules remained the same but the age categories were adjusted by one year:</p>
<ul>
<li>11 years and under</li>
<li>12 years through 14 years</li>
<li>15 years through 18 years</li>
</ul>
<p><em>80 Micro</em> had dropped coverage of the Color Computer between the start of the contest and the publication of the results. Only three winning entries were published in the February 1984 issue of <em>80 Micro</em>: the Grand Prize winner and the two non-Color Computer First Prize winners.<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup></p>

### Grand Prize

<ul>
<li>Play-Byte by Stephen Roth</li>
</ul>

### 15 years through 18 years

<ul>
<li>First Prize: Get Lost! by Steve Francis (for the Color Computer)</li>
<li>Second Prize: Electronic Inkwell by Joseph Goldberg</li>
<li>Third Prize: Dungeon of Death by Mike Erickson</li>
</ul>

### 12 years through 14 years

<ul>
<li>First Prize: SINSTEP by Brian Craft</li>
<li>Second Prize: Pilot+ by Nathaniel Koch (for the Model II)</li>
<li>Third Prize: Haunted Mansion by Scott Bradley (for the Color Computer)</li>
</ul>

### 11 years and under

<ul>
<li>First Prize: Adventure Sampler by Mark Kennedy</li>
<li>Second Prize: Bar graph by Eric Bailey (for the Model 100)</li>
<li>Third Prize: Flight simulator game by Tyler Kim</li>
</ul>
<p>There were four Honorable Mentions:</p>
<ul>
<li>Fourth Dimension by Lloyd Kupchanko</li>
<li>Key Art by Raul Acevedo</li>
<li>A “matchmaking entry” by Kim Skidmore</li>
<li>A “Mad Libs game” by Frank Conley</li>
</ul>

## The 1984-1985 Young Programmer’s Awards

<p>For the third (and final) contest, the rules were tightened so that all entries had to be written for the TRS-80 Model III, 4, or 4P only. This left out the Color Computer (not surprisingly) but also the Model I.</p>
<p>All First Prize winners were printed in the February 1985 issue of <em>80 Micro</em>. The Grand Prize winner was printed in the March 1985 issue. Some of the other winners were available on the <em>80 Micro</em> BBS.</p>

### Grand Prize

<ul>
<li>Graph by Michael Leibow</li>
</ul>

### 15 years to 18 years

<ul>
<li>First Prize: Fantastic Realms by Michael Lewicki and James Karls</li>
<li>Second Prize: Adventure Generator by Herman Calabria</li>
<li>Third Prize: Transport by Christopher Healey</li>
</ul>

### 12 years to 14 years

<ul>
<li>First Prize: Graphix by Jeff Reifman</li>
<li>Second Prize: Operator by Steven Whysong</li>
<li>Third Prize: An emulation of a Turing Machine by Ken Buckley</li>
</ul>

### 11 years and under

<ul>
<li>First Prize: Number Eaters by Jeffrey Zare</li>
<li>Second Prize: Drawarama by Stephen Lardieri</li>
<li>Third Prize: Conversion by Eric Mullenbruch (for the Model 4)</li>
</ul>
<p>There were four Honorable Mentions:</p>
<ul>
<li>3-D Maze by Lee Periolat</li>
<li>Gladiator by Jeff Reifman</li>
<li>Forms Management program by Mariam Tariq</li>
<li>Concentration by Stacy Lamb</li>
</ul>
<section class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1" role="doc-endnote">
<p>The contest rules stated that all winning entries would be published, which only happened the first year. But were all the winners still paid for publication? My guess is that they were. I know of several people who <em>80 Micro</em> paid for accepted but never published articles, which was (and is) very unusual for the publishing industry.&nbsp;<a href="#fnref:1" class="footnote-backref" role="doc-backlink">↩︎</a></p>
</li>
</ol>
</section>

## Related Posts:

<img width="120" height="120" src="https://foruda.gitee.com/images/1665673650173264066/61770a81_5631341.png" class="alignnone wp-post-image" alt=""> <img width="120" height="120" src="https://foruda.gitee.com/images/1665673672383796307/9ef03151_5631341.png" class="alignnone wp-post-image" alt=""> <img width="120" height="120" src="https://foruda.gitee.com/images/1665673692317347714/cdac1249_5631341.png" class="alignnone wp-post-image" alt=""> <img width="120" height="120" src="https://foruda.gitee.com/images/1665673718880891414/2194e2ef_5631341.png" class="alignnone wp-post-image" alt="">

- <a href="http://www.trs-80.org/trs-80-computing/">TRS-80 Computing</a>
- <a href="http://www.trs-80.org/trs80-strings/">TRS-80 Strings</a>
- <a href="http://www.trs-80.org/the-misosys-quarterly/">The MISOSYS Quarterly</a>
- <a href="http://www.trs-80.org/creative-computing/">Creative Computing</a>

<div class="categories">Categories: 
    <a class="category" href="http://www.trs-80.org/categories/magazines">Magazines</a></div>

## Comments

 **Adam Wells says:** 

<p>Wow, someone who remembers this! That was me, Adam Wells, who got third place in the 10-and-under group in 1983 for Math Countdown. I did win $50 plus my program was published in 80 Micro. I got a couple of letters from people who had tried to type it in and had had problems with it.</p>
<p>My local newspaper found out about this and ran a short human-interest story on me and my family, with a photo of me. Later, that newspaper story was, strangely, picked up by the tabloid Weekly World News, which then ran a story about me and how weird it was that a 10-year-old boy would rather play on his computer than throw a baseball (I was ahead of my time). I would have never known about this, except that my aunt happened to buy a copy of it that week.</p>
<p>Thanks for the memories! I should load up a TRS‑80 emulator and try to run my old program Math Countdown. As I recall, it wasn’t super exciting. There was a picture of a rocket on the screen, and you had to answer simple math problems correctly in order to get the countdown to advance. When you got to zero, you were rewarded with an animation of the rocket taking off.</p>

## Most Recent Posts

| img | title |
|---|---|
| <a href="http://www.trs-80.org/microsoft-fortran/"><img width="85" height="55" src="https://foruda.gitee.com/images/1665674133622162077/c4608bba_5631341.png"></a> | Microsoft FORTRAN |
| <a href="http://www.trs-80.org/muscle-micros/"><img width="85" height="55" src="https://foruda.gitee.com/images/1665674303571679975/f55cd134_5631341.png"> | The Muscle Micros |
| <a href="http://www.trs-80.org/arex/"><img width="85" height="55" src="https://foruda.gitee.com/images/1665674349656764317/93f90679_5631341.png"></a> | Arex |
| <a href="http://www.trs-80.org/trs80-disk/"><img width="85" height="55" src="https://foruda.gitee.com/images/1665674431893549627/31b216b0_5631341.png"></a> | TRS-80 Disk and Other Mysteries |
| <a href="http://www.trs-80.org/miner-2049er/"><img width="85" height="55" src="https://foruda.gitee.com/images/1665674454285790497/c9f709cc_5631341.png"></a> | Miner 2049er |

## Popular Posts

| img | title |
|---|---|
| <a href="http://www.trs-80.org/orchestra-90/"><img width="85" height="55" src="https://foruda.gitee.com/images/1665674947720694291/2f1047f7_5631341.png"></a> | Orchestra-90 |
| <a href="http://www.trs-80.org/trsdos-commented/"><img width="85" height="55" src="https://foruda.gitee.com/images/1665674971582926113/ad911451_5631341.png"></a> | TRSDOS Commented |
| <a href="http://www.trs-80.org/donkey-kong/"><img width="85" height="55" src="https://foruda.gitee.com/images/1665674992928651502/a9d9914f_5631341.png"></a> | Donkey Kong |
| <a href="http://www.trs-80.org/80-microcomputing/"><img width="85" height="55" src="https://foruda.gitee.com/images/1665675015311359928/585a801c_5631341.png"></a> | 80 Microcomputing |
| <a href="http://www.trs-80.org/robot-attack/"><img width="85" height="55" src="https://foruda.gitee.com/images/1665675035119261951/d2b67be1_5631341.png"></a> | Robot Attack |

# SKY Delta Airlines inflight magazine, June 1984

[![输入图片说明](https://foruda.gitee.com/images/1665675374982268759/f6989c34_5631341.png "屏幕截图")](http://www.teitown.com/Delta_Airlines.htm)

 _SKY Delta Airlines inflight magazine, June 1984_ 

Gee….Whiz kids!

A lack of visual contact with clients is called a bonus by many young computer users, because they are judged on merit rather than age.

Visual contact is called a bonus by many young computer users, because they are judged on merit rather than age.  Currently, Tei Gordon is involved in a massive publicity campaign, hoping to raise his current list of more than 50 companies up to around 200.  Like the other young entrepreneurs, he started using a computer to play games and progressed to other thins.  He decided this business was a good idea, “because I didn’t really have time to deliver papers with all my swimming and extra activities.”

Seventeen-year-old Anne Mei Chang of Lincroft, New Jersey, first used computers in the eighth grade.  During the next two years, she learned Basic, and assembled and took courses in Pascal at Bell Labs, through her Explorer’s post.  Her knowledge led to a job as an assistant instructor at the National Computer Camp.  By the end of the session, she was teaching the beginner’s group, while the instructor worked with the advanced students.

“I would think it (working on computers) is for people who enjoy math, because it takes logic.  It’s also for people willing to work hard because it takes a long time to learn,” said Chang.  That “willing to work hard” phrase appears to be the common thread among all of these computer whiz kids.

The push to bring out young programming talent has generated several contests.  More than 200 entries were received in the most recent  **Young Programmers contest run by 80 Micro** , the system-specific magazine for Tandy/Radio Shack  **TRS-80 microcomputer**  users.  Prizes were give for winning programs in each age category (11 and under, 12-14 and 15-18) and the top winners were paid normal freelance rates for publication of their programs in the magazine’s February issue.  Disks with the winning games from last year’s Verbatim Computer EdGame Challenge can be purchased for $3.50.  Winners from this year’s contest, which stresses the design of imaginative, instructional games, will also see their programs recorded.

The fascination with these young people who are intuitively at home with computers even spawned a weekly TV show called Whiz Kids.  The CBS program featured four teenage hackers who fight crime with the assistance of their computers, led by one teen’s talking machine, “Ralf.”

CAPTION: Tei Gordon might be 14, but companies like General Electric , J.C. Penney and GM use his weather data to save dollars.  He obtains the raw data via modem from the National Oceanic and Atmospheric Administration (NOAA) in Washington, D.C.

# [Radio Shack TRS-80 Model III Microcomputer](https://www.nightfallcrew.com/21/02/2014/radio-shack-trs-80-model-iii-microcomputer/)

![输入图片说明](https://foruda.gitee.com/images/1665676287894066316/5580c36d_5631341.png "屏幕截图") 

![输入图片说明](https://foruda.gitee.com/images/1665676293038158686/aca5782d_5631341.png "屏幕截图") ![输入图片说明](https://foruda.gitee.com/images/1665676299185336709/3724a51f_5631341.png "屏幕截图")  ![输入图片说明](https://foruda.gitee.com/images/1665676306845269621/0e828ba0_5631341.png "屏幕截图") ![输入图片说明](https://foruda.gitee.com/images/1665676312520743739/4e225213_5631341.png "屏幕截图") <img height="90px" src="https://foruda.gitee.com/images/1665676228517254472/4460e3d1_5631341.jpeg" title="IMG_0821.jpg">

TRS-80 (“Tandy/Radio Shack, Z-80 microprocessor”) was a brand associated with several desktop microcomputer lines sold by Tandy Corporation through their Radio Shack stores.

The original “TRS-80 Micro Computer System” launched in 1977 (later known as the Model I) was one of the earliest mass-produced personal computers. The first units, ordered unseen, were delivered in November 1977, and rolled out to the stores the third week of December. The line won popularity with hobbyists, home users, and small-businesses. Tandy Corporation’s leading position in what Byte Magazine called the “1977 Trinity” (Apple, Commodore and Tandy) had much to do with Tandy’s retailing the computer through more than 3,000 of its Radio Shack storefronts. Notable features of the original TRS-80 included its full-stroke QWERTY keyboard, small size, its floating point BASIC programming language, an included monitor, and a starting price of US$600 (equivalent to US$2,230 in 2011). The pre-release price was US$500 and a US$50 deposit was required, with a money back guarantee at time of delivery.

In July 1980 Tandy released the Model III. The improvements of the Model III over the Model I included built-in lower case, a better keyboard, 1500-baud cassette interface, and a faster (2.03 MHz) Z-80 processor.

With the introduction of the Model III, Model I production was discontinued as it did not comply with new FCC regulations as of January 1, 1981 regarding electromagnetic interference. The Model I radiated so much interference that while playing games an AM radio placed next to the computer could be used to provide sounds.

The Model III could run about 80% of Model I software, but used an incompatible disk format. Customers and developers complained of bugs in its BASIC and the TRSDOS operating system. The computer also came with the option of integrated disk drives.

### Download:

- [TRS-80 Model III Games Compilation in DMK/JV3/IMD Format (1243)](https://www.nightfallcrew.com/wp-content/plugins/download-monitor/download.php?id=1405)
- [TRS-80 Model III LDos v5.31 in IMD Format (1210)](https://www.nightfallcrew.com/wp-content/plugins/download-monitor/download.php?id=1406)

### Video of some games with sounds:

- http://www.youtube.com/embed/34G30wZ28pA
- http://www.youtube.com/embed/uJFnYnHb4O4

### Categories
<ul>
	<li class="cat-item cat-item-5"><a href="https://www.nightfallcrew.com/category/gallery/" title="Gallery">Gallery</a>
<ul class="children">
	<li class="cat-item cat-item-69"><a href="https://www.nightfallcrew.com/category/gallery/happyfaces_and_much_more/" title="Happy Faces and much more">Happy Faces and much more</a>
</li>
	<li class="cat-item cat-item-68"><a href="https://www.nightfallcrew.com/category/gallery/nightfallhq/" title="Nightfall HQ">Nightfall HQ</a>
</li>
	<li class="cat-item cat-item-70"><a href="https://www.nightfallcrew.com/category/gallery/partymovies/" title="Party Movies.">Party</a>
</li>
	<li class="cat-item cat-item-71"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/" title="Retro Computers and other stuff...">Retro Computers and other stuff…</a>
	<ul class="children">
	<li class="cat-item cat-item-640"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/8bcraft/" title="View all posts filed under 8bcraft">8bcraft</a>
</li>
	<li class="cat-item cat-item-629"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/acetronic/" title="View all posts filed under Acetronic">Acetronic</a>
</li>
	<li class="cat-item cat-item-187"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/acorn_family/" title="Acorn Computers">Acorn family</a>
</li>
	<li class="cat-item cat-item-490"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/act/" title="View all posts filed under ACT">ACT</a>
</li>
	<li class="cat-item cat-item-561"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/advance-business-technology-inc-abt/" title="View all posts filed under Advance Business Technology Inc (ABT)">Advance Business Technology Inc (ABT)</a>
</li>
	<li class="cat-item cat-item-555"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/advanced-memory-systems-ltd/" title="View all posts filed under Advanced Memory Systems Ltd">Advanced Memory Systems Ltd</a>
</li>
	<li class="cat-item cat-item-168"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/agf/" title="AGF">AGF</a>
</li>
	<li class="cat-item cat-item-606"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/al-alamiah-co/" title="View all posts filed under Al Alamiah co.">Al Alamiah co.</a>
</li>
	<li class="cat-item cat-item-485"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/alberici/" title="View all posts filed under Alberici">Alberici</a>
</li>
	<li class="cat-item cat-item-435"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/alix-pc-engines/" title="View all posts filed under Alix PC Engines">Alix PC Engines</a>
</li>
	<li class="cat-item cat-item-599"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/amstrad-cartridges/" title="View all posts filed under Amstrad Cartridges">Amstrad Cartridges</a>
</li>
	<li class="cat-item cat-item-137"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/amstrad/" title="Amstrad Computers">Amstrad family</a>
</li>
	<li class="cat-item cat-item-349"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/aoyue/" title="Aoyue things">Aoyue</a>
</li>
	<li class="cat-item cat-item-192"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/apple/" title="View all posts filed under Apple">Apple</a>
</li>
	<li class="cat-item cat-item-430"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/apple-clone/" title="View all posts filed under Apple clone">Apple clone</a>
</li>
	<li class="cat-item cat-item-524"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/applied-technology/" title="View all posts filed under Applied Technology">Applied Technology</a>
</li>
	<li class="cat-item cat-item-491"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/apricot/" title="View all posts filed under Apricot">Apricot</a>
</li>
	<li class="cat-item cat-item-522"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/arcade-pcb-games-jamma/" title="View all posts filed under Arcade PCB Games (Jamma)">Arcade PCB Games (Jamma)</a>
</li>
	<li class="cat-item cat-item-163"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/arcadesupergun/" title="Arcade Supergun">Arcade Supergun</a>
</li>
	<li class="cat-item cat-item-591"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/asem/" title="View all posts filed under Asem">Asem</a>
</li>
	<li class="cat-item cat-item-424"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/atari-3rd-party-hardware/" title="View all posts filed under Atari 3rd party Hardware">Atari 3rd party Hardware</a>
</li>
	<li class="cat-item cat-item-483"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/atari-clone/" title="View all posts filed under Atari clone">Atari clone</a>
</li>
	<li class="cat-item cat-item-139"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/atari/" title="Atari Consoles/Computers">Atari family</a>
</li>
	<li class="cat-item cat-item-219"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/atarimax/" title="AtariMax Things">AtariMax</a>
</li>
	<li class="cat-item cat-item-537"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/autovox/" title="Autovox">Autovox</a>
</li>
	<li class="cat-item cat-item-631"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/bmb-compuscience-ltd/" title="View all posts filed under BMB Compuscience Ltd">BMB Compuscience Ltd</a>
</li>
	<li class="cat-item cat-item-447"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/bondwell/" title="View all posts filed under Bondwell">Bondwell</a>
</li>
	<li class="cat-item cat-item-359"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/books/" title="Various Books">Books</a>
</li>
	<li class="cat-item cat-item-472"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/bung_enterprises_limited/" title="Bung Enterprises Limited">Bung Enterprises Limited</a>
</li>
	<li class="cat-item cat-item-173"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/cabletronic/" title="Cabletronic">Cabletronic</a>
</li>
	<li class="cat-item cat-item-568"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/cambridge-computer/" title="View all posts filed under Cambridge Computer">Cambridge Computer</a>
</li>
	<li class="cat-item cat-item-138"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/cbscolecovision/" title="CBS Coleco Vision Consoles">CBS Coleco Vision</a>
</li>
	<li class="cat-item cat-item-523"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/chalk-board-inc/" title="View all posts filed under Chalk Board Inc">Chalk Board Inc</a>
</li>
	<li class="cat-item cat-item-454"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/cheetah/" title="View all posts filed under Cheetah">Cheetah</a>
</li>
	<li class="cat-item cat-item-471"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/ccl/" title="China Coach Limited">China Coach Limited</a>
</li>
	<li class="cat-item cat-item-166"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/chinon/" title="Chinon">Chinon</a>
</li>
	<li class="cat-item cat-item-570"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/chinook-technology/" title="View all posts filed under Chinook Technology">Chinook Technology</a>
</li>
	<li class="cat-item cat-item-181"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/clementoni/" title="View all posts filed under Clementoni">Clementoni</a>
</li>
	<li class="cat-item cat-item-497"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/cms/" title="CMS Computer (IBM-PC Compatible)">CMS</a>
</li>
	<li class="cat-item cat-item-574"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/co-r-el-computers/" title="View all posts filed under Co.r.el Computers">Co.r.el Computers</a>
</li>
	<li class="cat-item cat-item-159"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore128family/" title="Commodore 128 family">Commodore 128 family</a>
</li>
	<li class="cat-item cat-item-161"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore16cartridges/" title="Commodore 16 Cartridges">Commodore 16 Cartridges</a>
</li>
	<li class="cat-item cat-item-157"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodoretedfamily/" title="Commodore TED family (C16/C116/Plus4)">Commodore 264 family</a>
</li>
	<li class="cat-item cat-item-146"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore64family/" title="Commodore 64 family">Commodore 64 family</a>
</li>
	<li class="cat-item cat-item-153"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore64128cartridges/" title="Commodore 64/128 Cartridges">Commodore 64/128 Cartridges</a>
</li>
	<li class="cat-item cat-item-143"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodoreamigafamily/" title="Commodore Amiga family">Commodore Amiga family</a>
</li>
	<li class="cat-item cat-item-144"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodoreamigazorrocard/" title="Commodore Amiga Zorro Card">Commodore Amiga Zorro Card</a>
</li>
	<li class="cat-item cat-item-618"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore-bags/" title="View all posts filed under Commodore Bags">Commodore Bags</a>
</li>
	<li class="cat-item cat-item-158"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodorecalculator/" title="Commodore Calculator">Commodore Calculator</a>
</li>
	<li class="cat-item cat-item-151"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodorecmpetfamily/" title="Commodore PET family">Commodore CBM/PET family</a>
</li>
	<li class="cat-item cat-item-540"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore-chessmate/" title="View all posts filed under Commodore CHESSmate">Commodore CHESSmate</a>
</li>
	<li class="cat-item cat-item-577"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore-consoles/" title="View all posts filed under Commodore Consoles">Commodore Consoles</a>
</li>
	<li class="cat-item cat-item-150"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodoredatassette/" title="Commodore Datasette ">Commodore Datassette</a>
</li>
	<li class="cat-item cat-item-148"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodorediskdrives/" title="Commodore Disk Drives">Commodore Disk Drives</a>
</li>
	<li class="cat-item cat-item-656"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore-drean/" title="View all posts filed under Commodore Drean">Commodore Drean</a>
</li>
	<li class="cat-item cat-item-507"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore-expansion-box/" title="View all posts filed under Commodore Expansion Box">Commodore Expansion Box</a>
</li>
	<li class="cat-item cat-item-658"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore-games-system-c64-gs/" title="View all posts filed under Commodore Games System (C64 GS)">Commodore Games System (C64 GS)</a>
</li>
	<li class="cat-item cat-item-164"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodorejoypaddlesmouse/" title="Commodore Joystick/Paddles">Commodore Joy/Paddles/Mouse</a>
</li>
	<li class="cat-item cat-item-657"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore-max-machine-vc10-ultimax/" title="Commodore MAX Machine (VC-10 / Ultimax)">Commodore MAX Machine</a>
</li>
	<li class="cat-item cat-item-201"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore_modem/" title="Commodore Modem">Commodore Modem</a>
</li>
	<li class="cat-item cat-item-191"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore-monitor/" title="View all posts filed under Commodore Monitors">Commodore Monitors</a>
</li>
	<li class="cat-item cat-item-594"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore-mos/" title="View all posts filed under Commodore MOS">Commodore MOS</a>
</li>
	<li class="cat-item cat-item-516"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore-peripherals/" title="View all posts filed under Commodore Peripherals">Commodore Peripherals</a>
</li>
	<li class="cat-item cat-item-672"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore-plotter/" title="View all posts filed under Commodore Plotter">Commodore Plotter</a>
</li>
	<li class="cat-item cat-item-538"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore-pre-production-prototype/" title="View all posts filed under Commodore pre-Production Prototype">Commodore pre-Production Prototype</a>
</li>
	<li class="cat-item cat-item-412"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodore_printers/" title="Commodore Printers">Commodore Printers</a>
</li>
	<li class="cat-item cat-item-160"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodorevic20cartridges/" title="Commodore VIC20 Cartridges">Commodore VIC20 Cartridges</a>
</li>
	<li class="cat-item cat-item-152"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/commodorevic20family/" title="Commodore VIC20 family">Commodore VIC20 family</a>
</li>
	<li class="cat-item cat-item-494"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/compaq/" title="View all posts filed under Compaq">Compaq</a>
</li>
	<li class="cat-item cat-item-661"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/compudata/" title="View all posts filed under CompuData">CompuData</a>
</li>
	<li class="cat-item cat-item-429"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/computertechnik/" title="View all posts filed under ComputerTechnik">ComputerTechnik</a>
</li>
	<li class="cat-item cat-item-534"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/computhink-computhink/" title="View all posts filed under CompuThink (Compu/Think)">CompuThink (Compu/Think)</a>
</li>
	<li class="cat-item cat-item-185"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/conic/" title="Conic Consoles">Conic</a>
</li>
	<li class="cat-item cat-item-584"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/creativision/" title="View all posts filed under Creativision">Creativision</a>
</li>
	<li class="cat-item cat-item-510"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/crezar/" title="View all posts filed under Crezar">Crezar</a>
</li>
	<li class="cat-item cat-item-420"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/cumana/" title="Cumana">Cumana</a>
</li>
	<li class="cat-item cat-item-581"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/daewoo/" title="View all posts filed under Daewoo">Daewoo</a>
</li>
	<li class="cat-item cat-item-196"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/datel/" title="Datel">Datel</a>
</li>
	<li class="cat-item cat-item-583"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/dick-smith/" title="View all posts filed under Dick Smith">Dick Smith</a>
</li>
	<li class="cat-item cat-item-588"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/dktronics/" title="View all posts filed under Dk'tronics">Dk'tronics</a>
</li>
	<li class="cat-item cat-item-503"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/dragon-data-ltd/" title="View all posts filed under Dragon Data Ltd">Dragon Data Ltd</a>
</li>
	<li class="cat-item cat-item-186"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/duette/" title="Duette Consoles">Duette</a>
</li>
	<li class="cat-item cat-item-170"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/elaboratebytes/" title="Elaborate Bytes">Elaborate Bytes</a>
</li>
	<li class="cat-item cat-item-508"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/elektronite/" title="View all posts filed under Elektronite">Elektronite</a>
</li>
	<li class="cat-item cat-item-513"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/enterprise-family-retro_computers_and_other_stuff/" title="View all posts filed under Enterprise family">Enterprise family</a>
</li>
	<li class="cat-item cat-item-452"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/epson/" title="Epson things">Epson</a>
</li>
	<li class="cat-item cat-item-477"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/everdrive-krikzz-cartridges/" title="View all posts filed under Everdrive (krikzz) cartridges">Everdrive (krikzz) cartridges</a>
</li>
	<li class="cat-item cat-item-639"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/exelvision/" title="View all posts filed under Exelvision">Exelvision</a>
</li>
	<li class="cat-item cat-item-617"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/exidy/" title="View all posts filed under Exidy">Exidy</a>
</li>
	<li class="cat-item cat-item-546"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/fenner/" title="View all posts filed under Fenner">Fenner</a>
</li>
	<li class="cat-item cat-item-645"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/fidelity/" title="View all posts filed under Fidelity">Fidelity</a>
</li>
	<li class="cat-item cat-item-550"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/fisher-price/" title="View all posts filed under Fisher Price">Fisher Price</a>
</li>
	<li class="cat-item cat-item-587"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/frael/" title="View all posts filed under Frael">Frael</a>
</li>
	<li class="cat-item cat-item-351"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/free_spirit_software_inc/" title="Free Spirit Software Inc">Free Spirit Software Inc</a>
</li>
	<li class="cat-item cat-item-470"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/frontfareast/" title="Front FarEast">Front FarEast</a>
</li>
	<li class="cat-item cat-item-458"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/fujifilm/" title="View all posts filed under FujiFilm">FujiFilm</a>
</li>
	<li class="cat-item cat-item-571"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/fujitsu/" title="View all posts filed under Fujitsu">Fujitsu</a>
</li>
	<li class="cat-item cat-item-172"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/gelosofamily/" title="Geloso family">Geloso family</a>
</li>
	<li class="cat-item cat-item-602"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/gig/" title="View all posts filed under GiG Electronics">GiG Electronics</a>
</li>
	<li class="cat-item cat-item-551"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/golden-image/" title="View all posts filed under Golden Image">Golden Image</a>
</li>
	<li class="cat-item cat-item-567"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/gotek/" title="View all posts filed under Gotek">Gotek</a>
</li>
	<li class="cat-item cat-item-553"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/great-valley-products-gvp/" title="View all posts filed under Great Valley Products (GVP)">Great Valley Products (GVP)</a>
</li>
	<li class="cat-item cat-item-211"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/grundig/" title="Grundig">Grundig</a>
</li>
	<li class="cat-item cat-item-654"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/grundy/" title="View all posts filed under Grundy">Grundy</a>
</li>
	<li class="cat-item cat-item-669"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/handic/" title="View all posts filed under Handic">Handic</a>
</li>
	<li class="cat-item cat-item-473"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/hardital/" title="Hardital">Hardital</a>
</li>
	<li class="cat-item cat-item-514"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/hewlett-packard/" title="View all posts filed under Hewlett-Packard">Hewlett-Packard</a>
</li>
	<li class="cat-item cat-item-556"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/highscore/" title="High Score">High Score</a>
</li>
	<li class="cat-item cat-item-174"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/hkcomputer/" title="HK-Computer">HK-Computer</a>
</li>
	<li class="cat-item cat-item-615"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/home-arcade/" title="View all posts filed under Home Arcade">Home Arcade</a>
</li>
	<li class="cat-item cat-item-566"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/hotline/" title="Hot Line">Hot Line</a>
</li>
	<li class="cat-item cat-item-622"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/hyperkin/" title="View all posts filed under Hyperkin">Hyperkin</a>
</li>
	<li class="cat-item cat-item-156"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/ibm/" title="IBM">IBM</a>
</li>
	<li class="cat-item cat-item-598"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/ice-felix/" title="View all posts filed under ICE Felix">ICE Felix</a>
</li>
	<li class="cat-item cat-item-529"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/indesit/" title="View all posts filed under Indesit">Indesit</a>
</li>
	<li class="cat-item cat-item-171"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/individualcomputers/" title="Individual Computers">Individual Computers</a>
</li>
	<li class="cat-item cat-item-620"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/inno-hit/" title="View all posts filed under Inno-Hit">Inno-Hit</a>
</li>
	<li class="cat-item cat-item-626"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/interton/" title="View all posts filed under Interton">Interton</a>
</li>
	<li class="cat-item cat-item-545"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/iomega/" title="View all posts filed under Iomega">Iomega</a>
</li>
	<li class="cat-item cat-item-212"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/irradio/" title="Irradio">Irradio</a>
</li>
	<li class="cat-item cat-item-179"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/jakks-tv-games/" title="View all posts filed under Jakks Tv Games">Jakks Tv Games</a>
</li>
	<li class="cat-item cat-item-398"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/jasonranheim/" title="Jason Ranheim Hardware">Jason Ranheim</a>
</li>
	<li class="cat-item cat-item-557"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/jen/" title="View all posts filed under Jen">Jen</a>
</li>
	<li class="cat-item cat-item-401"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/kaypro-corporation/" title="Kaypro Corporation">Kaypro Corporation</a>
</li>
	<li class="cat-item cat-item-495"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/koala/" title="View all posts filed under Koala">Koala</a>
</li>
	<li class="cat-item cat-item-521"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/kupke/" title="View all posts filed under Kupke">Kupke</a>
</li>
	<li class="cat-item cat-item-616"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/laser/" title="View all posts filed under Laser">Laser</a>
</li>
	<li class="cat-item cat-item-585"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/lear-siegler-incorporated/" title="View all posts filed under Lear Siegler Incorporated">Lear Siegler Incorporated</a>
</li>
	<li class="cat-item cat-item-666"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/leedex/" title="View all posts filed under Leedex">Leedex</a>
</li>
	<li class="cat-item cat-item-358"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/lego/" title="Lego">Lego</a>
</li>
	<li class="cat-item cat-item-559"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/lemon/" title="View all posts filed under Lemon">Lemon</a>
</li>
	<li class="cat-item cat-item-214"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/lexibook/" title="Lexibook">Lexibook</a>
</li>
	<li class="cat-item cat-item-525"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/livermore-data-systems/" title="View all posts filed under Livermore Data Systems">Livermore Data Systems</a>
</li>
	<li class="cat-item cat-item-167"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/logicahardware/" title="Logica Hardware">Logica Hardware</a>
</li>
	<li class="cat-item cat-item-169"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/lpcflashburner/" title="LPC Flash Burner">LPC Flash Burner</a>
</li>
	<li class="cat-item cat-item-600"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/luxor/" title="View all posts filed under Luxor">Luxor</a>
</li>
	<li class="cat-item cat-item-200"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/magazine/" title="Computer/Console Magazine">Magazine</a>
</li>
	<li class="cat-item cat-item-576"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/manosoft-retro_computers_and_other_stuff/" title="View all posts filed under Manosoft">Manosoft</a>
</li>
	<li class="cat-item cat-item-663"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/matra/" title="View all posts filed under Matra">Matra</a>
</li>
	<li class="cat-item cat-item-140"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/mattel/" title="Mattel Electronics Consoles/Computers">Mattel Electronics</a>
</li>
	<li class="cat-item cat-item-520"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/mcumall-electronics-inc/" title="View all posts filed under MCUmall Electronics inc">MCUmall Electronics inc</a>
</li>
	<li class="cat-item cat-item-189"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/md2/" title="View all posts filed under MD2">MD2</a>
</li>
	<li class="cat-item cat-item-528"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/media-tel-systems/" title="Media-Tel Systems">Media-Tel Systems</a>
</li>
	<li class="cat-item cat-item-539"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/memotech/" title="View all posts filed under Memotech">Memotech</a>
</li>
	<li class="cat-item cat-item-542"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/mentor/" title="View all posts filed under Mentor">Mentor</a>
</li>
	<li class="cat-item cat-item-575"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/micro-peripherals-ltd/" title="View all posts filed under Micro Peripherals Ltd">Micro Peripherals Ltd</a>
</li>
	<li class="cat-item cat-item-532"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/microdigital-eletronica-ltda/" title="Microdigital Eletrônica Ltda Family">Microdigital Eletrônica Ltda</a>
</li>
	<li class="cat-item cat-item-530"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/microsoft/" title="View all posts filed under Microsoft">Microsoft</a>
</li>
	<li class="cat-item cat-item-595"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/microtech/" title="View all posts filed under MicroTech">MicroTech</a>
</li>
	<li class="cat-item cat-item-527"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/microworld-pty-ltd/" title="View all posts filed under MicroWorld PTY LTD">MicroWorld PTY LTD</a>
</li>
	<li class="cat-item cat-item-207"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/milton_bradley_company/" title="Milton Bradley Company">Milton Bradley Company</a>
</li>
	<li class="cat-item cat-item-564"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/mirror/" title="View all posts filed under Mirror">Mirror</a>
</li>
	<li class="cat-item cat-item-562"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/mivar/" title="View all posts filed under Mivar">Mivar</a>
</li>
	<li class="cat-item cat-item-354"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/mos_technology/" title="MOS Technology, Inc., also known as CSG (Commodore Semiconductor Group)">MOS Technology</a>
</li>
	<li class="cat-item cat-item-155"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/motorola/" title="Motorola">Motorola</a>
</li>
	<li class="cat-item cat-item-202"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/msx_family/" title="Msx Family">Msx Family</a>
</li>
	<li class="cat-item cat-item-586"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/multitech/" title="View all posts filed under Multitech">Multitech</a>
</li>
	<li class="cat-item cat-item-544"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/mupi-italy/" title="View all posts filed under Mupi Italy">Mupi Italy</a>
</li>
	<li class="cat-item cat-item-499"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/nec/" title="View all posts filed under Nec">Nec</a>
</li>
	<li class="cat-item cat-item-573"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/neckerman/" title="View all posts filed under Neckerman">Neckerman</a>
</li>
	<li class="cat-item cat-item-459"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/nintendo-retro_computers_and_other_stuff/" title="View all posts filed under Nintendo">Nintendo</a>
</li>
	<li class="cat-item cat-item-417"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/nintendo-clone/" title="View all posts filed under Nintendo Clone">Nintendo Clone</a>
</li>
	<li class="cat-item cat-item-400"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/non-linear-systems-inc/" title="Non-Linear Systems Inc">Non-Linear Systems Inc</a>
</li>
	<li class="cat-item cat-item-194"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/nonamebulk/" title="View all posts filed under NoName/Bulk">NoName/Bulk</a>
</li>
	<li class="cat-item cat-item-195"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/nuova-elettronica/" title="Nuova Elettronica">Nuova Elettronica</a>
</li>
	<li class="cat-item cat-item-149"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/oceanic/" title="Oceanic Disk Drives">Oceanic Disk Drives</a>
</li>
	<li class="cat-item cat-item-461"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/olidata/" title="Olidata Family">Olidata</a>
</li>
	<li class="cat-item cat-item-415"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/olivetti_family/" title="Olivetti family">Olivetti family</a>
</li>
	<li class="cat-item cat-item-592"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/opus-supplies-ltd/" title="View all posts filed under Opus Supplies Ltd">Opus Supplies Ltd</a>
</li>
	<li class="cat-item cat-item-428"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/osborne-computer-corporation/" title="Osborne Computer Corporation">Osborne Computer Corporation</a>
</li>
	<li class="cat-item cat-item-619"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/otron/" title="View all posts filed under Otron">Otron</a>
</li>
	<li class="cat-item cat-item-572"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/palladium/" title="View all posts filed under Palladium">Palladium</a>
</li>
	<li class="cat-item cat-item-431"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/pc-cards-various/" title="View all posts filed under PC Cards (various)">PC Cards (various)</a>
</li>
	<li class="cat-item cat-item-543"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/peak/" title="View all posts filed under Peak">Peak</a>
</li>
	<li class="cat-item cat-item-580"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/perfect/" title="View all posts filed under Perfect">Perfect</a>
</li>
	<li class="cat-item cat-item-548"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/peters-plus-ltd/" title="View all posts filed under Peters Plus Ltd.">Peters Plus Ltd.</a>
</li>
	<li class="cat-item cat-item-145"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/phase5/" title="Phase5 Amiga Hardware">Phase5</a>
</li>
	<li class="cat-item cat-item-180"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/philips/" title="View all posts filed under Philips">Philips</a>
</li>
	<li class="cat-item cat-item-533"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/pico/" title="View all posts filed under Pico">Pico</a>
</li>
	<li class="cat-item cat-item-635"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/pioneer/" title="View all posts filed under Pioneer">Pioneer</a>
</li>
	<li class="cat-item cat-item-147"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/plugplaytvgames/" title="Plug &amp; Play TV Games">Plug &amp; Play TV Games</a>
</li>
	<li class="cat-item cat-item-184"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/polistil/" title="Polistil Consoles">Polistil</a>
</li>
	<li class="cat-item cat-item-142"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/pongtvgames/" title="Pong TV Games">Pong TV Games</a>
</li>
	<li class="cat-item cat-item-421"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/power_computing/" title="Power Computing">Power Computing</a>
</li>
	<li class="cat-item cat-item-469"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/ppi/" title="PPi (Personal Peripherals Inc.)">PPi (Personal Peripherals Inc.)</a>
</li>
	<li class="cat-item cat-item-578"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/prism/" title="View all posts filed under Prism">Prism</a>
</li>
	<li class="cat-item cat-item-216"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/protek/" title="Protek">Protek</a>
</li>
	<li class="cat-item cat-item-552"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/psion/" title="View all posts filed under Psion">Psion</a>
</li>
	<li class="cat-item cat-item-569"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/rd-automation/" title="View all posts filed under R&amp;D Automation">R&amp;D Automation</a>
</li>
	<li class="cat-item cat-item-136"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/radioshack/" title="Radio Shack Computers">Radio Shack</a>
</li>
	<li class="cat-item cat-item-476"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/radiola/" title="Radiola">Radiola</a>
</li>
	<li class="cat-item cat-item-500"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/radofin/" title="View all posts filed under Radofin">Radofin</a>
</li>
	<li class="cat-item cat-item-660"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/retro-games-ltd/" title="View all posts filed under Retro Games LTD">Retro Games LTD</a>
</li>
	<li class="cat-item cat-item-409"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/retro_innovations/" title="Retro Innovations">Retro Innovations</a>
</li>
	<li class="cat-item cat-item-509"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/riteman/" title="View all posts filed under Riteman">Riteman</a>
</li>
	<li class="cat-item cat-item-593"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/romantic-robot-uk-ltd/" title="View all posts filed under Romantic Robot UK Ltd">Romantic Robot UK Ltd</a>
</li>
	<li class="cat-item cat-item-468"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/rushware/" title="Rushware">Rushware</a>
</li>
	<li class="cat-item cat-item-541"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sandy_pcp_uk/" title="Sandy PCP (UK)">Sandy PCP (UK)</a>
</li>
	<li class="cat-item cat-item-198"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/schneider/" title="schneider">Schneider</a>
</li>
	<li class="cat-item cat-item-165"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sd_mmc_interfaces/" title="SD/MMC 2 IEC bus">SD/MMC Interfaces</a>
</li>
	<li class="cat-item cat-item-486"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sd2snes-ikari-cartridge/" title="View all posts filed under SD2Snes (Ikari) cartridge">SD2Snes (Ikari) cartridge</a>
</li>
	<li class="cat-item cat-item-474"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sega-cartridges/" title="View all posts filed under Sega Cartridges">Sega Cartridges</a>
</li>
	<li class="cat-item cat-item-162"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/segafamily/" title="Sega family">Sega family</a>
</li>
	<li class="cat-item cat-item-558"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/selcom/" title="View all posts filed under Selcom">Selcom</a>
</li>
	<li class="cat-item cat-item-515"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/seleco/" title="View all posts filed under Seleco">Seleco</a>
</li>
	<li class="cat-item cat-item-466"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sharp/" title="Sharp Family">Sharp</a>
</li>
	<li class="cat-item cat-item-641"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/siel/" title="View all posts filed under Siel">Siel</a>
</li>
	<li class="cat-item cat-item-326"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/siemens/" title="Siemens">Siemens</a>
</li>
	<li class="cat-item cat-item-621"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/silcom-re-el/" title="View all posts filed under Silcom (RE-EL)">Silcom (RE-EL)</a>
</li>
	<li class="cat-item cat-item-154"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sinclaircartridges/" title="Sinclair Cartridges">Sinclair Cartridges</a>
</li>
	<li class="cat-item cat-item-135"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sinclairfamily/" title="Sinclair Computers family">Sinclair family</a>
</li>
	<li class="cat-item cat-item-549"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sinclair-software/" title="View all posts filed under Sinclair Software">Sinclair Software</a>
</li>
	<li class="cat-item cat-item-649"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sinclair-zx-81-clones/" title="View all posts filed under Sinclair ZX 81 Clones">Sinclair ZX 81 Clones</a>
</li>
	<li class="cat-item cat-item-625"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sinclair-zx-interfaces/" title="View all posts filed under Sinclair ZX Interfaces">Sinclair ZX Interfaces</a>
</li>
	<li class="cat-item cat-item-604"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sinclair-zx-spectrum-clones/" title="View all posts filed under Sinclair ZX Spectrum Clones">Sinclair ZX Spectrum Clones</a>
</li>
	<li class="cat-item cat-item-665"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sinclair-zx-spetrum-next/" title="View all posts filed under Sinclair ZX Spetrum Next">Sinclair ZX Spetrum Next</a>
</li>
	<li class="cat-item cat-item-408"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sip_telecom/" title="Sip (Telecom) gadgets">Sip (Telecom)</a>
</li>
	<li class="cat-item cat-item-141"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/snkneogeo/" title="SNK NeoGeo Consoles">SNK NeoGeo</a>
</li>
	<li class="cat-item cat-item-653"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/sony/" title="View all posts filed under Sony">Sony</a>
</li>
	<li class="cat-item cat-item-504"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/spectravideo/" title="View all posts filed under Spectravideo">Spectravideo</a>
</li>
	<li class="cat-item cat-item-565"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/spem/" title="View all posts filed under Spem">Spem</a>
</li>
	<li class="cat-item cat-item-563"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/staff/" title="Staff">Staff</a>
</li>
	<li class="cat-item cat-item-209"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/stonechips_electronics/" title="Stonechips Electronics">Stonechips Electronics</a>
</li>
	<li class="cat-item cat-item-213"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/supersound/" title="Supersound">Supersound</a>
</li>
	<li class="cat-item cat-item-193"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/syquest-technology/" title="View all posts filed under SyQuest Technology">SyQuest Technology</a>
</li>
	<li class="cat-item cat-item-460"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/tandy/" title="View all posts filed under Tandy">Tandy</a>
</li>
	<li class="cat-item cat-item-501"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/tatung/" title="View all posts filed under Tatung">Tatung</a>
</li>
	<li class="cat-item cat-item-188"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/teac/" title="Teac Stuff">Teac</a>
</li>
	<li class="cat-item cat-item-496"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/techno-source/" title="View all posts filed under Techno Source">Techno Source</a>
</li>
	<li class="cat-item cat-item-177"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/texas-instruments/" title="View all posts filed under Texas Instruments">Texas Instruments</a>
</li>
	<li class="cat-item cat-item-671"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/tfw8b/" title="View all posts filed under TFW8b">TFW8b</a>
</li>
	<li class="cat-item cat-item-535"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/the-toy-lobster-company/" title="View all posts filed under The Toy Lobster Company">The Toy Lobster Company</a>
</li>
	<li class="cat-item cat-item-482"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/thomson/" title="View all posts filed under Thomson">Thomson</a>
</li>
	<li class="cat-item cat-item-502"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/timex-sinclair/" title="View all posts filed under Timex Sinclair">Timex Sinclair</a>
</li>
	<li class="cat-item cat-item-427"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/tomsad/" title="Tomsad">Tomsad</a>
</li>
	<li class="cat-item cat-item-183"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/tomy/" title="View all posts filed under Tomy">Tomy</a>
</li>
	<li class="cat-item cat-item-648"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/tonel-pc/" title="View all posts filed under Tonel PC">Tonel PC</a>
</li>
	<li class="cat-item cat-item-364"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/toshiba/" title="Toshiba">Toshiba</a>
</li>
	<li class="cat-item cat-item-673"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/total-telecommunications/" title="View all posts filed under Total Telecommunications">Total Telecommunications</a>
</li>
	<li class="cat-item cat-item-536"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/tulip-computers/" title="View all posts filed under Tulip Computers">Tulip Computers</a>
</li>
	<li class="cat-item cat-item-603"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/tulip-computers-compudata/" title="View all posts filed under Tulip Computers (CompuData)">Tulip Computers (CompuData)</a>
</li>
	<li class="cat-item cat-item-451"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/usrobotics/" title="USRobotics">USRobotics</a>
</li>
	<li class="cat-item cat-item-418"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/veb-mikroelektronik-muhlhausen/" title="View all posts filed under VEB Mikroelektronik Muhlhausen">VEB Mikroelektronik Muhlhausen</a>
</li>
	<li class="cat-item cat-item-175"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/villagetronic/" title="Village Tronic">Village Tronic</a>
</li>
	<li class="cat-item cat-item-208"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/vtech/" title="VTech">VTech</a>
</li>
	<li class="cat-item cat-item-547"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/weller/" title="View all posts filed under Weller">Weller</a>
</li>
	<li class="cat-item cat-item-632"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/woltron/" title="View all posts filed under Woltron">Woltron</a>
</li>
	<li class="cat-item cat-item-582"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/yamaha/" title="View all posts filed under Yamaha">Yamaha</a>
</li>
	<li class="cat-item cat-item-560"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/zanussi/" title="View all posts filed under Zanussi">Zanussi</a>
</li>
	<li class="cat-item cat-item-203"><a href="https://www.nightfallcrew.com/category/gallery/retro_computers_and_other_stuff/zenith/" title="View all posts filed under Zenith">Zenith</a>
</li>
	</ul>
</li>
</ul>
</li>
	<li class="cat-item cat-item-1"><a href="https://www.nightfallcrew.com/category/nightfalloldskool/" title="Nightfall Old Skool">Nightfall Old Skool</a>
<ul class="children">
	<li class="cat-item cat-item-59"><a href="https://www.nightfallcrew.com/category/nightfalloldskool/amiga-oldskool/" title="Amiga.">Amiga</a>
</li>
	<li class="cat-item cat-item-64"><a href="https://www.nightfallcrew.com/category/nightfalloldskool/asciiartbbs/" title="Ascii Art / BBs">Ascii Art / BBs</a>
</li>
	<li class="cat-item cat-item-60"><a href="https://www.nightfallcrew.com/category/nightfalloldskool/commodore64/" title="Commodore 64">Commodore 64</a>
</li>
	<li class="cat-item cat-item-95"><a href="https://www.nightfallcrew.com/category/nightfalloldskool/dtv-c64/" title="View all posts filed under Dtv (C64)">Dtv (C64)</a>
</li>
	<li class="cat-item cat-item-63"><a href="https://www.nightfallcrew.com/category/nightfalloldskool/gameboy/" title="Gameboy">Gameboy (gb/gbc)</a>
</li>
	<li class="cat-item cat-item-67"><a href="https://www.nightfallcrew.com/category/nightfalloldskool/logoandgraphic/" title="Logo and Graphic">Logo and Graphic</a>
</li>
	<li class="cat-item cat-item-215"><a href="https://www.nightfallcrew.com/category/nightfalloldskool/mac_osx/" title="Mac (OSX)">Mac (OSX)</a>
</li>
	<li class="cat-item cat-item-62"><a href="https://www.nightfallcrew.com/category/nightfalloldskool/megadrive-oldskool/" title="Megadrive">Megadrive (genesis)</a>
</li>
	<li class="cat-item cat-item-65"><a href="https://www.nightfallcrew.com/category/nightfalloldskool/nintendo64/" title="Nintendo 64">Nintendo 64 (n64)</a>
</li>
	<li class="cat-item cat-item-66"><a href="https://www.nightfallcrew.com/category/nightfalloldskool/snkneogeo-oldskool/" title="SNK NeoGeo">SNK NeoGeo</a>
</li>
	<li class="cat-item cat-item-61"><a href="https://www.nightfallcrew.com/category/nightfalloldskool/snes-oldskool/" title="Super Nintendo (snes)">Super Nintendo (snes)</a>
</li>
</ul>
</li>
	<li class="cat-item cat-item-56"><a href="https://www.nightfallcrew.com/category/today/" title="Today News">Today</a>
<ul class="children">
	<li class="cat-item cat-item-81"><a href="https://www.nightfallcrew.com/category/today/favoritelinks/" title="Favorite Links">Favorite Links</a>
</li>
	<li class="cat-item cat-item-72"><a href="https://www.nightfallcrew.com/category/today/news_rumors/" title="News &amp; Rumors from the Net.">News &amp; Rumors</a>
	<ul class="children">
	<li class="cat-item cat-item-133"><a href="https://www.nightfallcrew.com/category/today/news_rumors/amigapower/" title="Amiga Power">Amiga Power</a>
</li>
	<li class="cat-item cat-item-217"><a href="https://www.nightfallcrew.com/category/today/news_rumors/amstrad_cpc/" title="Amstrad CPC">Amstrad CPC</a>
</li>
	<li class="cat-item cat-item-394"><a href="https://www.nightfallcrew.com/category/today/news_rumors/apple-news_rumors/" title="View all posts filed under Apple">Apple</a>
</li>
	<li class="cat-item cat-item-218"><a href="https://www.nightfallcrew.com/category/today/news_rumors/atari-news_rumors-today/" title="Atari">Atari</a>
</li>
	<li class="cat-item cat-item-134"><a href="https://www.nightfallcrew.com/category/today/news_rumors/c128_128d_128dcr/" title="C128/128D/128DCR">C128/128D/128DCR</a>
</li>
	<li class="cat-item cat-item-130"><a href="https://www.nightfallcrew.com/category/today/news_rumors/c16116264plus4/" title="View all posts filed under C16/116/264/Plus4">C16/116/264/Plus4</a>
</li>
	<li class="cat-item cat-item-100"><a href="https://www.nightfallcrew.com/category/today/news_rumors/c64_sx64_new_and_rumors/" title="Commodore 64">C64/SX64</a>
</li>
	<li class="cat-item cat-item-519"><a href="https://www.nightfallcrew.com/category/today/news_rumors/c65c64dx/" title="View all posts filed under C65/C64DX">C65/C64DX</a>
</li>
	<li class="cat-item cat-item-176"><a href="https://www.nightfallcrew.com/category/today/news_rumors/cbmpet/" title="View all posts filed under CBM/PET">CBM/PET</a>
</li>
	<li class="cat-item cat-item-554"><a href="https://www.nightfallcrew.com/category/today/news_rumors/donations-news_rumors/" title="Donations">Donations</a>
</li>
	<li class="cat-item cat-item-101"><a href="https://www.nightfallcrew.com/category/today/news_rumors/dtv/" title="Dtv (c64)">DTV</a>
</li>
	<li class="cat-item cat-item-182"><a href="https://www.nightfallcrew.com/category/today/news_rumors/events/" title="View all posts filed under Event(s)">Event(s)</a>
</li>
	<li class="cat-item cat-item-633"><a href="https://www.nightfallcrew.com/category/today/news_rumors/firmware-news_rumors/" title="View all posts filed under Firmware">Firmware</a>
</li>
	<li class="cat-item cat-item-197"><a href="https://www.nightfallcrew.com/category/today/news_rumors/hardware/" title="View all posts filed under Hardware">Hardware</a>
</li>
	<li class="cat-item cat-item-205"><a href="https://www.nightfallcrew.com/category/today/news_rumors/magazine-news_rumors-today/" title="Magazine">Magazine</a>
</li>
	<li class="cat-item cat-item-132"><a href="https://www.nightfallcrew.com/category/today/news_rumors/osx_linux_win_tools/" title="OSx/Linux/Win Tools">OSx/Linux/Win Tools</a>
</li>
	<li class="cat-item cat-item-128"><a href="https://www.nightfallcrew.com/category/today/news_rumors/rubbish-dump/" title="Rubbish Dump">Rubbish Dump</a>
</li>
	<li class="cat-item cat-item-199"><a href="https://www.nightfallcrew.com/category/today/news_rumors/sinclair/" title="Sinclair Power">Sinclair Power</a>
</li>
	<li class="cat-item cat-item-129"><a href="https://www.nightfallcrew.com/category/today/news_rumors/vic20/" title="Commodore VIC20/VC20">VIC20/VC20</a>
</li>
	</ul>
</li>
	<li class="cat-item cat-item-53"><a href="https://www.nightfallcrew.com/category/today/projects_repairs/" title="Electronic Projects">Projects / Repairs</a>
	<ul class="children">
	<li class="cat-item cat-item-58"><a href="https://www.nightfallcrew.com/category/today/projects_repairs/c64mod/" title="C64 Modding.">C64 Modding</a>
</li>
	<li class="cat-item cat-item-57"><a href="https://www.nightfallcrew.com/category/today/projects_repairs/dtvmod/" title="DTV Modding.">DTV Modding</a>
</li>
	<li class="cat-item cat-item-674"><a href="https://www.nightfallcrew.com/category/today/projects_repairs/sid-player-projects_repairs/" title="View all posts filed under Sid Player">Sid Player</a>
</li>
	<li class="cat-item cat-item-114"><a href="https://www.nightfallcrew.com/category/today/projects_repairs/various/" title="Various">Various</a>
</li>
	</ul>
</li>
</ul>
</li>
		</ul>
