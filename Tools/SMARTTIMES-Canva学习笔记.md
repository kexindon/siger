<p><img width="706px" src="https://foruda.gitee.com/images/1670408098677366001/d0384d93_5631341.jpeg" title="490canva.jpg"></p>

# SMARTTIMES-Canva学习笔记

## [SMARTTIMES](../sig/生命/SMARTtimes01.md)

[SMARTTIMES](../sig/生命/SMARTtimes01.md)是我们SIGER产出的内容以刊报形式设计的一期内容。SIGER目前还是一个线上开源平台，不过我们可以将我们的内容以刊报形式转载，让更多的人看见我们SIGER的身影，增加我们的影响力。

## [Canva学习笔记](https://www.bilibili.com/video/BV1Sr4y1h77x/?p=7)

<p><img align="left" src="https://foruda.gitee.com/images/1670404531901172298/d6ad4cd9_5631341.png">
Canva是一家位于澳大利亚的软件组织，提供的一款名为Canva的软件。Canva软件套件是SaaS软件。它是一款演示软件，大多数WPS等等软件的会员和种种限制都无法能满足我们对于版式的设计。Canva的一些竞争软件产品包括EasyWorship, Faithlife Proclaim和PixTeller Editor。不过Canva优势的一点是它的运作不需要VPN的支持并且有很好的网页版方便同学们打开并编辑。此外，Canva还有合作编辑的功能，而这个功能也使得我们未来的合作编辑更加方便。<br>
<br>
同学们可以点击超链接查看Canva的入门教学哦。<p>

### 编辑与设计的技巧
#### 01 如何注册Canva

1. 登录http://Canva.com，并在Canva页面中注册会员。注意：有的朋友登录时，可能会跳转到Canva中文版本（http://Canva.cn）， 这时你如果你想要使用英文版你可以将谷歌浏览器的默认语言调为英文，并且爬墙改为全局模式，再重新进入Canva就可以了。
<br>

<img src="https://pic1.zhimg.com/80/v2-85cd81ec61eae5cfc5d6ac565a8fbfb8_1440w.jpg">

2. 点击“Sign up with Google”，使用你自己的谷歌账号注册一个Canva新账号。（如果已有Canva账号了，就可跳过这一步）同样，如果使用的是中文版，点击注册，用自己的微信账号登陆即可
<br>

<img src="https://pic4.zhimg.com/80/v2-47f04326094189145bb4fae4eb3ea973_1440w.jpg">
3. Canva Pro会员可以让你使用更多的便捷修图功能，且拥有30天的免费试用期，如果你觉得充值麻烦，便可以利用这30天的试用期将你想要制作的图片全部制作完成。
<br>

<img src="https://pic1.zhimg.com/80/v2-9e86c7947ae64d6cfe6869789dcc80d8_1440w.jpg">

#### 02 如何使用Canva调节图片的尺寸大小

1. 点击右上角的“Create a design”,选择下方的“Custom size”来确定你想要的图片尺寸。

<br>

<img src="https://pic3.zhimg.com/80/v2-08a8bd2b0b8d85d6c0ddf485a7a15a26_1440w.webp">

2. 将图片放入设定好的图层中，然后拖动图片的边缘使之铺满整个图层。
<br>

<img src="https://pic1.zhimg.com/80/v2-0a3073d653ebc0ee732dce7748881af4_1440w.webp">

3. 点击右上角的“Download”，以JPG格式下载制作好的图片即可。

<br>

<img src="https://pic3.zhimg.com/80/v2-ce909a990c01204049ca12d1a8f09192_1440w.webp">

#### 03 如何在Canva里搜图

1. 制作并进入一个你想要的尺寸图层。
<img src="https://pic2.zhimg.com/80/v2-dad86925b31411649bca42179707d049_1440w.webp">
2. 如果你想要搜索一些想要的图片的话，那就点击“Photos", 然后在上方的搜索栏输入你想要的图片名称。
<br>

<img src="https://pic2.zhimg.com/80/v2-86850b49eab34d1e4eec20e4d4224c9d_1440w.webp">

3. 如果你想要为你的图片增添一些相关的小插图或者元素的话，就点击“Elements”，然后在上方的搜索栏输入你想要的元素名称。

<br>

<img src="https://pic2.zhimg.com/80/v2-2294ab613a427685e5b3c1a0937b0e21_1440w.webp">
4. 如果你想要创建某个模板的话，就点击“Templates"，然后在上方的搜索栏输入你想要的模板的名称即可。

<br>

<img src="https://pic3.zhimg.com/80/v2-464111c1dbecd74f0e5613ba294bc0f2_1440w.webp">

#### 04 如何抠图

1. 上传图片，选择编辑图片，点击抠图
 
2. Canva会自动扣图，倘若不满意就可以自己再修
 
3. 注意⚠️，一定要点击应用图片否则就会保存没修改的图片


## 花絮-发现Canva
<img src="https://blog.galaxyweblinks.com/wp-content/uploads/2014/10/canva-logo.png">
可画作为一个澳大利亚品牌是不少国外老师们推荐的APP。我是一名国际生，而我们的历史老师总是要求我们在线上做海报和写论文，这也是为什么在他要求我们做海报的时候，大部分同学会熟练的掏出电脑。在上课后不久的一段时间，老师给我们推荐了Canva。

随着后续线上海报和工作的增加，我开始越来越频繁的使用Canva，这也是为什么在一开始在做第一期SMARTTIMES时，我第一个想到的辅助工具就是Canva。

在探索的过程中，同学们会发现Canva拥有非常多的内容且有各式各样的模版。我倾向于使用Canva的其中一个原因就是因为它的模版的繁多。不论是PPT的模版亦或是海报的模版都应用尽有且不需要会员。

另外，Canva也非常方便下载和转PDF的工作。它有适应打印的PDF，标准版PDF，PNG形式图片，JPG形式图片，MP4视频和GIF动图的选项且均不用付费。对于学生党来说非常友好！

同学们尽情探索享受创作的旅程吧！

--- 

【笔记】[SMARTTIMES-Canva学习笔记](https://gitee.com/cylina/siger/issues/I64QKD)

> 对于SMARTTIMES刊报的canva学习笔记，可以供以后同学们参考的辅助工具

  - SMARTTIMES-Canva学习笔记 ([正文文稿](https://gitee.com/cylina/siger/issues/I64QKD#note_14922891_link))

    > [足够了，剩下的只是时间，静待同学们成长！有了这篇攻略 SIGer 终于形成了 编委养成的闭环，SMARTtimes 一定能越飞越高，越飞越远，知道冲出大气层，奔赴月宫，乃至更远的星辰大海！PR 到主仓 Tools 吧。](https://gitee.com/cylina/siger/issues/I64QKD#note_14942119_link) :pray: @[Cylina](https://gitee.com/cylina)

      @[袁德俊](https://gitee.com/yuandj) 的[开箱体验](https://gitee.com/cylina/siger/issues/I64QKD#note_14942989_link)

      > @Cylina 助攻一篇开篇体验，欢迎同学们都来学习使用可画！你可以补一篇是怎么发现这个宝藏的？当做花絮，为文章添彩。 同样的国际志愿者日，老师为天文社，增添了一篇 “大火星”天文志愿讲解的专题，介绍了北京天文馆讲解员志愿服务做科普的事迹，平凡的岗位，做出了色彩，SIGer SMART 也一定可以汲取营养，茁壮成长为一颗科普新星，未来可期，大有可为。

  - [花絮-发现Canva](https://gitee.com/cylina/siger/issues/I64QKD#note_14982201_link)

  - [Canva pics](https://gitee.com/cylina/siger/issues/I64QKD#note_14985068_link) | [封面](https://gitee.com/cylina/siger/issues/I64QKD#note_14987822_link)

    <p><img height="169px" src="https://foruda.gitee.com/images/1670405060925433481/834b455a_5631341.png" title="Canva.png"><img height="169px" src="https://foruda.gitee.com/images/1670405091191181398/462d6230_5631341.png" title="canva-KIT-PNG.png"><img height="169px" src="https://foruda.gitee.com/images/1670405213424141110/499a8312_5631341.png" title="Canva-logo-sbo.png"><img height="169px" src="https://foruda.gitee.com/images/1670405129084568123/5bc0856a_5631341.jpeg" title="Web-Canva-Tutorial.jpg"><img height="169px" src="https://foruda.gitee.com/images/1670406813202258907/c5fcccb8_5631341.png" title="tl_看图王.web.png"></p>

    - [Canva - Marketing Online para escritores](https://marketingonlineparaescritores.com/curso/curso-canva/canva/)  
marketingonlineparaescritores.com|1410 × 2250 png|Image may be subject to copyright.
    - [Checkout - Canva Kit](https://my.digitall.id/product/canva-kit/)  
my.digitall.id|2000 × 1750 png|Image may be subject to copyright.
    - [Canva valuation soars to $8.7 billion - 2GB](https://www.2gb.com/canva-valuation-soars-to-8-7-billion/)  
2GB2GB|1920 × 1080 png|Image may be subject to copyright.
    - [Canva Tutorial – PCClassesOnline](http://www.pcclassesonline.com/canva-tutorial/)  
pcclassesonline.compcclassesonline.com|1920 × 1080 jpeg|Image may be subject to copyright.
    - [可画canva logo素材 - Canva可画](https://www.canva.cn/icons/MAEDibog7iE/)  
canva.cn|550 × 220 png|图像可能受版权保护。

## [开箱体验](https://gitee.com/cylina/siger/issues/I64QKD#note_14942989_link) @[袁德俊](https://gitee.com/yuandj)

1. http://Canva.com 注册

   我选择了 手机或者邮箱注册，相当丝滑，我非常反感必须用手机号绑定的（习惯），用常用的工作邮箱注册了 同名的 yuandj，轻松收到注册码，完成注册，可以用丝滑形容。

2. SIGer 是科普志愿者组织，当然是慈善机构

   ![输入图片说明](https://foruda.gitee.com/images/1670316777210534870/3efefe93_5631341.png "屏幕截图")

   尽管我是教师，但我并不认为需要培训同学们高超的艺术能力，因为它只是编委的技能之一，随着工作的深入，这回是自我表达的工具，就好像书写钢笔字，令人赏心悦目。

   我是学生，这可能是为了鼓励用户，在专业版的收费政策制定的，同学们尽可选择。

3. 我可以当即邀请 @cylina 为了写攻略，我跳过了。

   ![输入图片说明](https://foruda.gitee.com/images/1670316864629254571/f7f1418c_5631341.png "屏幕截图")

   @cylina 可以邀请我加入 SMARTtimes 的编辑团队，它就是试炼场，同学们成长练就 SIGer 核心技能的地方。

4. 恭喜你，开启了一段神奇之旅，你想设计什么？

   ![输入图片说明](https://foruda.gitee.com/images/1670317128920304196/f880b2ed_5631341.jpeg "你想设计什么？.jpg")

   想不想生日会的开场，众星捧月，我仿佛已经看到了我修成成十八般武艺，制作出精美的 SIGer 期刊的时刻了。

5. 我喜欢 “可画” 这个名字，也喜欢这么美好的开始。未来可期。我邀请同学们一起加入 [SMARTtimes](../sig/生命/SMARTtimes01.md) 的共同编纂中来 :+1: 