# SIGER们与MD格式
 **大纲：** 
- 认识markdown
- 文字
- 图片
- 表格等等辅助工具
<br>

![输入图片说明](https://ts1.cn.mm.bing.net/th/id/R-C.2488e757d6c9a5f20b58d493f67915c1?rik=pMlp2Ym7pPl0zQ&pid=ImgRaw&r=0)

###  **Markdown语法的用途** 

- Markdown是一种可以使用普通文本编辑器编写的标记语言，通过简单的标记语法，它可以使普通文本内容具有一定的格式。
- 语法简洁明了、学习容易，而且功能比纯文本更强，因此有很多人用它写博客
而我们SIGer也可以在MD格式中如鱼得水的使用它

##  **Markdown语法笔记** 
### **文字** 
#### 1.设计标题
在想要设置为标题的文字前面加#来表示 一个#是一级标题，二个#是二级标题，以此类推至六级标题。 
<br>

注意⚠️：千万记得###后面有空格，不然可能会做成跳转页面到其他issue的情况
比如：

```
# 这是一级标题
## 这是二级标题
### 这是三级标题
#### 这是四级标题
##### 这是五级标题
###### 这是六级标题

```
效果如下：
<br>
# 这是一级标题
## 这是二级标题
### 这是三级标题
#### 这是四级标题
##### 这是五级标题
###### 这是六级标题

另一方面，下面也给大家一些可以用的小标题工具（复制即可）：
█  <br>   ●  <br>   ◎ <br>

#### **2.字体** 

- 加粗：要加粗的文字左右分别用两个*号包起来
- 斜体：要倾斜的文字左右分别用一个*号包起来
- 斜体加粗：要倾斜和加粗的文字左右分别用三个*号包起来 OR  用下划线包起来（如果下划线打不出来，实际上可以用上方工具框中的斜体的I，点击就可以啦
- 删除线：要加删除线的文字左右分别用两个~~号包起来 

比如：
**这是加粗的文字**
*这是倾斜的文字*`    OR       _这是倾斜的文字_ 
***这是斜体加粗的文字***
~~这是加删除线的文字~~

#### **3.引用** 

在引用的文字前加>即可。引用也可以嵌套，如加两个>>三个>>> n个，这种符号需切换英文输入法

示例：
>这是引用的内容
>>这是引用的内容
>>>>>>>>>>这是引用的内容

#### **4.文字分割线** 
三个或者三个以上的 - 或者 * 都可以。 

比如：

---
----
***
*****
如果以前学过网址编程的或许会知道`<hr>`实际上也可以使用

#### **5.文字居左/中/右** 
- 左右对齐代码：`<p align="left/right">左右对齐</p>`
例如：<p align="right">右对齐</p>
- 居中对齐：`<center>居中</center>`
例如：<center>居中</center>

#### 6. HTML辅助元素-角标
      - 上角标
      代码：`文字<sup>角标文字</sup>`
      注意⚠️：“/”是close的意思，千万别忘加
      例如：史努比<sup>TM</sup>
      - 下角标
      代码：`文字<sub>角标文字</sub>`
      例如：H<sub>2</sup>O

### **图片** 
大家在上方工具栏中见到最常见的图片插入方式就是这样的：
`![地图](https://images.gitee.com/uploads/images/2019/0927/213315_f242e566_1831467.png "map00.png")`

#### **- 图片和文字并排** 
不过，如果我们想在md格式中将图片和文字并排怎么办呢？
这里我就直接引用我写的代码啦：

```
<img align="right" src="https://www.pixelstalk.net/wp-content/uploads/images1/Character-Anime-Angel-Wings-Photo.jpg">

曾经我们或许会幻想人长出翅膀后是一种 _天使的模样_ 。
事实上，如果只有一双翅膀长在身上根本 _飞不起来_ 。就像我们所提到的，飞行需要强大的肌肉提供动力，而人的背部却没有这样的肌肉。肌肉依靠收缩提供力量。人背部的肌肉都是用于让肩部和手臂向后运动的，而飞行需要向两个方向运动翅膀，其中向前（下）运动尤为重要。这需要身体前后两侧肌肉的配合才能做到。

```
而它的效果就是这样啦：
<img align="right" src="https://tse1-mm.cn.bing.net/th/id/OIP-C.1Y0CKjfDSJL9pmzUriD8AwAAAA?pid=ImgDet&rs=1">

曾经我们或许会幻想人长出翅膀后是一种 _天使的模样_ 。
事实上，如果只有一双翅膀长在身上根本 _飞不起来_ 。就像我们所提到的，飞行需要强大的肌肉提供动力，而人的背部却没有这样的肌肉。
肌肉依靠收缩提供力量。人背部的肌肉都是用于让肩部和手臂向后运动的，而飞行需要向两个方向运动翅膀，其中向前（下）运动尤为重要。这需要身体前后两侧肌肉的配合才能做到。

实际上，它中间align里面可以写出center/right/left其中任何一种：
`<img align="right/center/left" src="网址">`
如果写center，说明图片会呈现在中间，
如果写left，说明图片会呈现在文字左边，
如果写right，说明图片会呈现在文字右边，

#### **- 设置图片大小** 
另一方面，如果图片太大直接占了这个页面使得align的代码失效比如：
<img align="right" src="https://www.pixelstalk.net/wp-content/uploads/images1/Character-Anime-Angel-Wings-Photo.jpg">
Hello World!

那么就可以在后面加上height和width的属性值：

```
<img align="right" src="https://www.pixelstalk.net/wp-content/uploads/images1/Character-Anime-Angel-Wings-Photo.jpg" height="300" width="200">
Hello World!
```
效果如下：
<img align="right" src="https://www.pixelstalk.net/wp-content/uploads/images1/Character-Anime-Angel-Wings-Photo.jpg" height="100" width="200">
Hello World!

####  **图片与图片并排** 
代码：

```
<div align="center">
   <img src="图片网址" width="300" height="300"><img src="图片网址" width="300" height="300">
</div>
```

注意⚠️：有些图片太大并排不了的时候直接加height和width的属性值就可以
注意⚠️：两张图片代码要连着

效果如下：
<div align="center">
   <img src="https://pic1.zhimg.com/80/v2-93fed14331b430fcd062069cf09b9198_1440w.webp" width="300" height="300"><img src="https://pic2.zhimg.com/80/v2-acc96410532c12e954e48a9a824768b1_1440w.webp" width="300" height="300">
</div>

####  **加图片边框** 
      代码：`<kbd>`
      使用方法：加在图片前面
      <kbd><img align="right" src="https://www.pixelstalk.net/wp-content/uploads/images1/Character-Anime-Angel-Wings-Photo.jpg" height="200" width="300">

###  **辅助工具** 
#### 1. 表格
如果不依赖代码，表格的形成可以轻松依赖issues中的工具栏。
具体操作如下：
   1. 点击从左往右数第9个类似表格的图标
   2. 接着会弹出插入表格，选择合适大小
   3. 插入文字
   4. 勾选格式化表格并确认
效果如下：

| 插入文字 | 插入文字 |
|------|------|
| 插入文字 | 插入文字 |


如果我们将它代码形式展现出来，就不难发现：

```
| 插入文字 | 插入文字 |
|------|------|
| 插入文字 | 插入文字 |

```
制作表格使用 | 来分隔不同的单元格，使用 - 来分隔表头和其他行。
我们可以设置表格的对齐方式：
1. -: 设置内容和标题栏居右对齐
2. :- 设置内容和标题栏居左对齐
3. :-: 设置内容和标题栏居中对齐
例如：

```
| 左对齐 | 居中对齐 | 右对齐 |
|:-----|:-----:|-----:|
| 单元格 | 单元格 | 单元格 |
| 单元格 | 单元格 | 单元格 |
```
#### 2. 超链接
超链接可以帮助我们的页面跳转更多的页面，代码如下：
代码：[超链接名](超链接地址 "超链接title")
title可加可不加
例如：
`[Cylina的gitee](https://gitee.com/cylina)`
效果如下：
[Cylina的gitee](https://gitee.com/cylina)

#### 3. 列表
   - 列表
   以及这种点状型和数字型的也十分实用：
   - 这里是列表文本
   - 这里是列表文本
   1. 这里是列表文本
   2. 这里是列表文本
   <br>
    列表嵌套 - 上一级和下一级之间敲三个空格即可

#### 4. 换行
如果发现打字时换行了，但是体现的md格式没有换行说明你需要<br>.
 _错例：_ 
你想体现的：
```
Hello
World
```
实际输出的：
Hello World

 **因此你可以这样：** 

```
Hello
<br>
World
```
效果如下：
<br>
Hello
<br>
World
<br>
注意⚠️：大家应该会很常用到br不过大多数时候可以回去检查的时候再加。

#### 5. 添加代码
如果想要在内容里添加代码并使它不运行就可以：
单行代码：代码之间分别用一个反引号包起来
如下：`代码内容`
代码块：代码之间分别用三个反引号包起来，且两边的反引号单独占一行
（```）
代码。。。
代码。。。
代码。。。
（```）
> 注：为了防止转译，前后三个反引号处加了小括号，实际是没有的。这里只是用来演示，实际中去掉两边小括号即可。