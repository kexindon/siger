[![输入图片说明](https://images.gitee.com/uploads/images/2021/1221/223312_7222bbe1_5631341.jpeg "nanhu - 10years 999.jpg")](https://images.gitee.com/uploads/images/2021/1221/223245_d9eefa9e_5631341.jpeg)

【RISC-V Summit】[香山：开源高性能RISC-V处理器](https://gitee.com/RV4Kids/XiangShan/issues/I4M83F) 【[笔记](https://gitee.com/RV4Kids/XiangShan/issues/I4M83F#note_7819024_link)】

2021年RISC-V Summit在北京时间12月7日凌晨1点开幕，峰会的第一个报告来自RISC-V国际基金会理事、中科院计算所副所长、研究员包云岗，他向国际RISC-V社区介绍了香山开源高性能处理器。

这是香山第一次在国际RISC-V社区正式亮相。报告主要内容包括：
- ①香山开源项目基本情况；
- ②基于RTL仿真的性能评估方法；
- ③香山的微结构设计与改进情况。

> 未来，香山将始终坚持开源，我们期待有更多伙伴加入香山的开发队伍！

 **其他：** 

- "[敏捷与开源硬件（Agile and Open-Source Hardware）](https://gitee.com/RV4Kids/XiangShan/issues/I4M83F#note_7820835_link)" 【[海报](https://images.gitee.com/uploads/images/2021/1213/145834_06aa008e_5631341.jpeg)】CCF 收费报告
  > @包云岗 2020年，我和新加坡国立大学的Trevor Carlson 教授共同组织了一期主题为“敏捷与开源硬件（Agile and Open-Source Hardware）”的IEEE Micro专刊，一共录取了来自全世界各个大学13篇论文，但中国只有一篇，来自北京大学梁云教授团队。今年国内对开源芯片和敏捷设计领域的关注度明显升温，为此梁云老师和我一起组织了一期专题培训，欢迎大家报名

- [致敬传奇：中国并行处理四十年，他们从无人区探索走到计算的黄金时代](https://gitee.com/RV4Kids/XiangShan/issues/I4M83F#note_7820975_link) | GAIR 2021
  > 文中的一些故事都是第一次听到，向前辈们致敬！ 陈彩娴 AI科技评论 2021-12-09 22:51 
  > 时代造英雄，或英雄造时代，是历史记录中的一个经典「耦合」问题。如同「先有鸡还是先有蛋」的讨论般，奇人与奇世的关系往往难解难分。但当我们着眼于现代科技史的变迁，这个吸引思辨学家数千年的哲学问题又往往能够得到一个如公式定理般的答案——人才先于技术。

- [包老师朋友圈](https://gitee.com/RV4Kids/XiangShan/issues/I4M83F#note_7821420_link)
  > @包云岗 很荣幸关于“香山”开源高性能开源RISC-V处理器的报告被安排在2021年度RISC-V Summit的开场第一个。时间是加州早上9点，也就是北京时间凌晨1点。虽然有些晚，但这是香山第一次在国际RISC-V社区正式亮相，再晚也值得。
  > @yuandj 可以出一期 SIGER 香山特刊啦。 :pray: 这篇做标题和卷首 :pray: 没有比南湖红船的照片更醒目的封面啦。6月的雁栖湖校区的秋叶，原来是为红船做铺垫的啊:pray:
    - [红船原图](https://gitee.com/RV4Kids/XiangShan/issues/I4M83F#note_7821539_link) from PDF
    - [本期封面](https://gitee.com/RV4Kids/XiangShan/issues/I4M83F#note_7825374_link)，[1稿](https://images.gitee.com/uploads/images/2021/1213/170814_73943768_5631341.jpeg)获包老师肯定
    - [包云岗：总要有人去做一些不同的事](https://mp.weixin.qq.com/s?__biz=MzU1NTUyMjg3NQ==&mid=2247486074&idx=1&sn=6a319a6903f51d2146e479077c9477b0) 【[笔记](https://gitee.com/RV4Kids/XiangShan/issues/I4M83F#note_7963967_link)】
      [SIGer](https://gitee.com/flame-ai/siger) 转载到：《[包云岗：总要有人去做一些不同的事](baoyungang%2010%20years%20diffrent.md)》


 **较早前收录：** 

- [标签化RISC-V火苗原型系统 入选 “十三五”科技创新成就展](https://gitee.com/RV4Kids/XiangShan/issues/I4FI0Q) [全文](https://gitee.com/RV4Kids/XiangShan/issues/I4FI0Q#note_7153002_link)
- 香山双周报：[20211018期](https://gitee.com/RV4Kids/XiangShan/issues/I4FI0T) [20211004期](https://gitee.com/RV4Kids/XiangShan/issues/I4FI0S)
- [4年21份资料10万字：记录RISC-V在中国的一条轨迹](https://mp.weixin.qq.com/s?__biz=Mzg5MTY4MjgyNg==&mid=2247485118&idx=1&sn=ff6886c86e18e5d349fe2c0295485acf)【笔记】
  1. 2017年12月：《[关于RISC-V成为印度国家指令集的一些看法](https://mp.weixin.qq.com/s?__biz=MjM5MTY5ODE4OQ==&mid=2651449195&idx=1&sn=cc7f7e1166fb499dd2ed0feaae15cf3e&scene=21#wechat_redirect)》
  2. 2018年10月：《RISC-V 手册》中文版
  3. 2019年1月：《芯片敏捷开发实践：标签化RISC-V》
  4. 2019年2月：[《开放指令集与开源芯片 发展报告 》白皮书](https://mp.weixin.qq.com/s?__biz=MjM5MTY5ODE4OQ==&mid=2651458539&idx=1&sn=2dc8319a0071f0901551976d01d39b55&scene=21#wechat_redirect)
  5. 2019年2月：《降低芯片设计创新门槛——从互联网成功经验看开源芯片生态发展》
  6. 2019年5月：《[开源项目风险分析与对策建议](https://mp.weixin.qq.com/s?__biz=MzU2Mjc1NDQ1MQ==&mid=2247483782&idx=1&sn=4064ea8ae6c7cbfa7dd1b8ada3f30e56&scene=21#wechat_redirect)》
  7. 2019年6月：[《开源项目风险分析与对策建议》报告解读](https://mp.weixin.qq.com/s?__biz=MzU2Mjc1NDQ1MQ==&mid=2247483805&idx=1&sn=14a24e25e63d4d174e33dc514a6c6e0d&scene=21#wechat_redirect)
  8. 2019年7月：《驳“发展开源芯片弊大于利”论》
  9. 2019年8月：《远景研讨会纪要：面向下一代计算的开源芯片与敏捷开发》
  10. 2019年10月：《处理器芯片开源设计与敏捷开发方法思考与实践》
  11. 2019年12月：《[一些关于RISC-V的质疑与解读](https://mp.weixin.qq.com/s?__biz=MzU2Mjc1NDQ1MQ==&mid=2247483959&idx=1&sn=9dcdde9db70395b3991a93f01b4e45a7&scene=21#wechat_redirect)》
  12. 2020年3月：《[关于RISC-V和开源处理器的一些解读](https://mp.weixin.qq.com/s?__biz=MzU2Mjc1NDQ1MQ==&mid=2247484077&idx=1&sn=bc7f11c058430fc9b1e12760d55183aa&scene=21#wechat_redirect)》
  13. 2020年7月：《[“一生一芯”计划幕后的故事](https://mp.weixin.qq.com/s?__biz=Mzg5MTY4MjgyNg==&mid=2247485118&idx=1&sn=ff6886c86e18e5d349fe2c0295485acf)》
  14. 2020年9月：[《RISC-V Catalyst for Change（推动改变的RISC-V催化剂）》中文版](https://mp.weixin.qq.com/s?__biz=MzU2Mjc1NDQ1MQ==&mid=2247484362&idx=1&sn=cca1efcd0164b7a0ded3e2a6b5a3367b&scene=21#wechat_redirect)
  15. 2020年11月：《[破解“中国开源拿来主义”的几点分析](https://mp.weixin.qq.com/s?__biz=Mzg5MTY4MjgyNg==&mid=2247485118&idx=1&sn=ff6886c86e18e5d349fe2c0295485acf&chksm=cfc8efdbf8bf66cd446e92e35c11cf3b0d52a209839330e6080764bde4a9c2bcf8b3c610540c&mpshare=1&scene=1&srcid=10271yURx0SgiiImrBGU6Esa&sharer_sharetime=1635272921694&sharer_shareid=b96734bb797d99630e66735e62f56980&exportkey=AegMfUbiEbv8E5xAf9uwhMA%3D&pass_ticket=zwOyh9wHXngJbqqMJAPpvZAWt3MqI%2FZQJOktou662BeJeT4qhhDiOwYKfB%2FlaFPU&wx_header=0)》
  16. 2020年12月：《[从技术的角度来看，RISC-V 能对芯片发展、科技自主起到哪些作用？](https://mp.weixin.qq.com/s?__biz=Mzg5MTY4MjgyNg==&mid=2247485118&idx=1&sn=ff6886c86e18e5d349fe2c0295485acf&chksm=cfc8efdbf8bf66cd446e92e35c11cf3b0d52a209839330e6080764bde4a9c2bcf8b3c610540c&mpshare=1&scene=1&srcid=10271yURx0SgiiImrBGU6Esa&sharer_sharetime=1635272921694&sharer_shareid=b96734bb797d99630e66735e62f56980&exportkey=AegMfUbiEbv8E5xAf9uwhMA%3D&pass_ticket=zwOyh9wHXngJbqqMJAPpvZAWt3MqI%2FZQJOktou662BeJeT4qhhDiOwYKfB%2FlaFPU&wx_header=0)》
  17. 2021年3月：《[Linux初期在中国的发展](https://mp.weixin.qq.com/s?__biz=Mzg5MTY4MjgyNg==&mid=2247485118&idx=1&sn=ff6886c86e18e5d349fe2c0295485acf&chksm=cfc8efdbf8bf66cd446e92e35c11cf3b0d52a209839330e6080764bde4a9c2bcf8b3c610540c&mpshare=1&scene=1&srcid=10271yURx0SgiiImrBGU6Esa&sharer_sharetime=1635272921694&sharer_shareid=b96734bb797d99630e66735e62f56980&exportkey=AegMfUbiEbv8E5xAf9uwhMA%3D&pass_ticket=zwOyh9wHXngJbqqMJAPpvZAWt3MqI%2FZQJOktou662BeJeT4qhhDiOwYKfB%2FlaFPU&wx_header=0)》
  18. 2021年4月：《[论开源精神](https://mp.weixin.qq.com/s?__biz=MjM5MTY5ODE4OQ==&mid=2651487387&idx=1&sn=11df596738ae2d1f018faa72d482fb4e&scene=21#wechat_redirect)》
  19. 2021年5月：《[科技发展如何处理好开放与自主的关系](https://mp.weixin.qq.com/s?__biz=MzAwMjA5NDUwOA==&mid=2653217871&idx=1&sn=3596f24acaddc0db6d104b0a0e1ca897&scene=21#wechat_redirect)》
  20. 2021年6月：《[如何应对AIoT对芯片的碎片化需求？](https://mp.weixin.qq.com/s?__biz=Mzg5MTY4MjgyNg==&mid=2247485118&idx=1&sn=ff6886c86e18e5d349fe2c0295485acf&chksm=cfc8efdbf8bf66cd446e92e35c11cf3b0d52a209839330e6080764bde4a9c2bcf8b3c610540c&mpshare=1&scene=1&srcid=10271yURx0SgiiImrBGU6Esa&sharer_sharetime=1635272921694&sharer_shareid=b96734bb797d99630e66735e62f56980&exportkey=AegMfUbiEbv8E5xAf9uwhMA%3D&pass_ticket=zwOyh9wHXngJbqqMJAPpvZAWt3MqI%2FZQJOktou662BeJeT4qhhDiOwYKfB%2FlaFPU&wx_header=0)》
  21. 2021年6月：《[香山：开源高性能RISC-V处理器](https://mp.weixin.qq.com/s?__biz=MzU2Mjc1NDQ1MQ==&mid=2247484795&idx=1&sn=c734659839ad2651bf49635dd6ec240d&scene=21#wechat_redirect)》

 **[NutShell](https://gitee.com/RV4Kids/NutShell/issues/) 中关于香山的笔记：** 

- [“香山开源处理器” 微信 专栏](https://gitee.com/RV4Kids/NutShell/issues/I4CCTR)
  - 【会议解读】[HOT CHIPS 33 Session 1](https://mp.weixin.qq.com/s?__biz=Mzg5MTY4MjgyNg==&mid=2247484961&idx=1&sn=efa30aeed01205ee18dbdb404de07cd0) 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I4CCTR#note_6873161_link)】
    - [Intel Alder Lake CPU Architectures](https://gitee.com/RV4Kids/NutShell/issues/I4CCTR#note_6873171_link) slide [1](https://images.gitee.com/uploads/images/2021/0928/235645_a6387697_5631341.png)/[2](https://images.gitee.com/uploads/images/2021/0928/235652_fd6a762f_5631341.png)/[3](https://images.gitee.com/uploads/images/2021/0928/235710_f611c541_5631341.png)/[4](https://images.gitee.com/uploads/images/2021/0928/235729_44608783_5631341.png)/[5](https://images.gitee.com/uploads/images/2021/0928/235752_8265afb8_5631341.png)
      - 题目：Intel Alder Lake CPU Architectures
      - 作者：Efraim Rotem, Intel
      - 分享人：陈国凯，中科院计算所研一学生
    - [AMD Next Generation "Zen3" Core](https://gitee.com/RV4Kids/NutShell/issues/I4CCTR#note_6873198_link) slide [1](https://images.gitee.com/uploads/images/2021/0928/235912_4977d978_5631341.png)/[2](https://images.gitee.com/uploads/images/2021/0928/235920_ed889050_5631341.png)/[3](https://images.gitee.com/uploads/images/2021/0928/235939_c139c34c_5631341.png)/[4](https://images.gitee.com/uploads/images/2021/0929/000004_a802921b_5631341.png)/[5](https://images.gitee.com/uploads/images/2021/0929/000018_dff3a526_5631341.png)/[6](https://images.gitee.com/uploads/images/2021/0929/000037_30510d66_5631341.png)/[7](https://images.gitee.com/uploads/images/2021/0929/000105_773a33a1_5631341.png)/[8](https://images.gitee.com/uploads/images/2021/0929/000318_247ca5d1_5631341.png)/[9](https://images.gitee.com/uploads/images/2021/0929/000338_6474a9fd_5631341.png)/[10](https://images.gitee.com/uploads/images/2021/0929/000408_17bc711d_5631341.png)/[11](https://images.gitee.com/uploads/images/2021/0929/000506_25932492_5631341.png)/[12](https://images.gitee.com/uploads/images/2021/0929/000521_59a8e05a_5631341.png)
      - 题目：AMD Next Generation "Zen3" Core
      - 作者：Mark Evers, AMD
      - 分享人：蔺嘉炜，中科院计算所研二学生

---
 (自本期开始：[RV4kids](https://gitee.com/RV4kids) 将迁移到 SIGer 专刊 成为 [RISCV](https://gitee.com/flame-ai/siger/blob/master/RISC-V) 专题目录下的子刊承载报道 RV 学习 RV 的职责。  
[RV4kids](https://gitee.com/RV4kids) 组织以[收录相关仓](https://gitee.com/organizations/RV4Kids/projects)为主要职能。项目重心围绕 [石榴核](https://gitee.com/shiliupi/shiliupi99/tree/master/MagSi) 拓展！)