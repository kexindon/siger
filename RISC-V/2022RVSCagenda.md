[![输入图片说明](https://foruda.gitee.com/images/1660253152616333305/屏幕截图.png "屏幕截图.png")](https://riscv-summit-china.com/cn/index.html)

### 8/24 周三·上午·主会场
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>09:00</td><td><b><a title="Calista - RISC-V Summit China intro -RVSC2022" href="https://www.bilibili.com/video/BV1KN4y1c7M9" target="_blank">RISC-V国际基金会致辞</a></b></td><td><b>Calista</b><br>RISC-V国际基金会</td></tr>
<tr><td>09:05</td><td><b><a title="倪光南院士致辞-RVSC2022" href="https://www.bilibili.com/video/BV1eB4y1z7qA" target="_blank">倪光南院士致辞</a></b></td><td><b>倪光南</b><br>中科院</td></tr>
<tr><td>09:10</td><td><b><a title="指导单位（上海市经信委）致辞 - RISC-V中国峰会2022" href="https://www.bilibili.com/video/BV1yg411r78a" target="_blank">指导单位</a>及主办方致辞（待定）</b></td><td><b></b><a title="主办单位 中国科学院软件研究所 赵琛所长 致辞" href="https://www.bilibili.com/video/BV1pT411w7sa" target="_blank">中科院软件所</a><br><a title="主办单位（上海科技大学）上海科技大学党委书记、副校长 李儒新院长" href="https://www.bilibili.com/video/BV15t4y1E7rb" target="_blank">上海科技大学</a></td></tr>
<tr><td>09:20</td><td><b><a href="https://www.bilibili.com/video/BV1Je4y1Z786" target="_blank">The Road Ahead</a></b></td><td><b>Mark Himelstein</b><br>RISC-V 国际基金会</td></tr>
<tr><td>09:40</td><td><b><a href="https://www.bilibili.com/video/BV1LT411F7Z6" target="_blank">持续推进RISC-V技术与生态的发展</a></b></td><td><b>孟建熠</b><br>平头哥半导体有限公司</td></tr>
<tr><td>10:10</td><td><b><a href="https://www.bilibili.com/video/BV1md4y1d7bb" target="_blank">本土RISC-V处理器IP赋能产业“芯”应用</a></b></td><td><b>彭剑英</b><br>芯来科技</td></tr>
<tr><td>10:40</td><td><b><a href="https://www.bilibili.com/video/BV1ma411G7ve" target="_blank">香山开源高性能RISC-V处理器敏捷设计实践</a></b></td><td><b>包云岗</b><br>中科院计算所</td></tr>
<tr><td>11:00</td><td><b><a href="https://www.bilibili.com/video/BV1VP411L7QT" target="_blank">跟随RISC-V 弯道超车—RISC-V的应用优势</a></b></td><td><b>苏泓萌</b><br>Andes</td></tr>
<tr><td>11:20</td><td><b><a href="https://www.bilibili.com/video/BV1qd4y1A7my" target="_blank">基于RISC-V青稞微处理器全栈MCU+技术分享</a></b></td><td><b>杨勇</b><br>沁恒微电子</td></tr>
<tr><td>11:40</td><td><b>新产品发布环节</b></td><td><b>1/3 - <a href="https://www.bilibili.com/video/BV1MW4y1t7Ur" target="_blank">平头哥半导体</a></b><br><b>2/3 - <a href="https://www.bilibili.com/video/BV1RG411V7Eo" target="_blank">芯来科技</a></b><br><b>3/3 - <a href="https://www.bilibili.com/video/BV1uW4y1t7vq" target="_blank">瞬曜EDA</a></b><br></td></tr>
</tbody>
</table>
<br><br><br>
</div>


### 8/24 周三·下午·A分会场
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>13:30</td><td><b>Session A3</b></td><td><b></b><br></td></tr>
<tr><td>13:30</td><td><b>Intel Investment to Help Deliver a Thriving RISC-V Ecosystem</b></td><td><b>Gary Martz and Jiangang Duan</b><br>Intel</td></tr>
<tr><td>14:00</td><td><b>RISC-V音视频计算芯片的应用与探索</b></td><td><b>黄少锐</b><br>全志科技</td></tr>
<tr><td>14:30</td><td><b>Native Development for RISC-V</b></td><td><b>梁宇宁</b><br>鉴释科技</td></tr>
<tr><td>14:50</td><td><b>全球首款量产的高性能RISC-V多媒体处理器</b></td><td><b>赵晶</b><br>赛昉科技</td></tr>
<tr><td>15:10</td><td><b>全球首款高性能RISC-V开源SBC—VisionFive 2全球首发</b></td><td><b>许理</b><br>赛昉科技</td></tr>
<tr><td>15:30</td><td><b></b></td><td><b></b><br></td></tr>
<tr><td>15:30</td><td><b>Session A4</b></td><td><b></b><br></td></tr>
<tr><td>15:30</td><td><b>RISC-V processor embedded eco-system practice and thinking</b></td><td><b>何小庆</b><br>ESBF</td></tr>
<tr><td>16:00</td><td><b>助力RISC-V生态，OneOS赋能万物互联</b></td><td><b>张宏伟</b><br>中移物联网</td></tr>
<tr><td>16:30</td><td><b>中科昊芯基于RISC-V的DSP在伺服变频行业的应用</b></td><td><b>李玉尧</b><br>中科昊芯</td></tr>
<tr><td>16:40</td><td><b>Introducing a New Level of RISC-V CPU Performance</b></td><td><b>Itai Yarom</b><br>MIPS</td></tr>
<tr><td>17:00</td><td><b>符合RISC-V 应用要求的内存子系统配置优化</b></td><td><b>Bing Yu</b><br>Andes</td></tr>

</tbody>
</table>
<br><br><br>
</div>


### 8/24 周三·下午·B分会场
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>13:30</td><td><b>Session B3</b></td><td><b></b><br></td></tr>
<tr><td>13:30</td><td><b>主频800MHz! 先楫超高性能 RISC-V 通用MCU 在工业领域高端应用</b></td><td><b>徐琦</b><br>上海先楫半导体</td></tr>
<tr><td>13:50</td><td><b>“承影”：基于RISC-V向量扩展的开源GPGPU</b></td><td><b>杨轲翔</b><br>清华大学集成电路学院（学生）</td></tr>
<tr><td>14:20</td><td><b>芯来RISC-V CPU IP SIMD指令实现介绍</b></td><td><b>胡进</b><br>芯来科技</td></tr>
<tr><td>14:30</td><td><b>CUDA on RISC-V GPGPU</b></td><td><b>夏品正</b><br>赛昉科技</td></tr>
<tr><td>14:40</td><td><b>RISC-V特权态MPU设计及其应用</b></td><td><b>杜东、杨璧丞</b><br>上海交通大学并行与分布式系统实验室</td></tr>
<tr><td>15:10</td><td><b>Session B4</b></td><td><b></b><br></td></tr>
<tr><td>15:10</td><td><b>Embracing Innovation: How the RISC-V Software Ecosystem rethinks Fragmentation as Differentiation</b></td><td><b>Philipp Tomsich</b><br>VRULL GmbH</td></tr>
<tr><td>15:40</td><td><b>QEMU &amp; LINUX support for RV32 compat mode</b></td><td><b>郭任、刘志伟</b><br>阿里巴巴</td></tr>
<tr><td>16:10</td><td><b>Extending RISC-V ISA for Tightly Integrated Layer-Wise Quantized Mixed-Precision Inference at the Edge</b></td><td><b>Xinfei Guo, Vaibhav Verma, Xiaotian Zhao and Mircea Stan</b><br>上海交大</td></tr>
<tr><td>16:40</td><td><b>操作IOPMP时的原子性问题</b></td><td><b>辜善群</b><br>Andes</td></tr>
<tr><td>17:10</td><td><b>IOMMU for Xuantie-based Platforms</b></td><td><b>Siqi Zhao</b><br>阿里巴巴平头哥</td></tr>

</tbody>
</table>
<br><br><br>
</div>


### 8/25 周四·上午·A分会场
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>09:00</td><td><b>Session A5</b></td><td><b></b><br></td></tr>
<tr><td>09:00</td><td><b>香山处理器前端取指架构的演进</b></td><td><b>勾凌睿</b><br>中科院计算所</td></tr>
<tr><td>09:15</td><td><b>南湖架构后端流水线的设计与改进</b></td><td><b>张紫飞</b><br>中科院计算所</td></tr>
<tr><td>09:25</td><td><b>南湖架构 MMU 单元的设计与改进</b></td><td><b>张紫飞</b><br>中科院计算所</td></tr>
<tr><td>09:35</td><td><b>南湖架构访存子系统的设计与实现</b></td><td><b>王华强</b><br>中科院计算所</td></tr>
<tr><td>09:45</td><td><b>南湖架构 HuanCun Non-blocking Cache 设计与实现</b></td><td><b>王凯帆</b><br>中科院计算所</td></tr>
<tr><td>09:55</td><td><b>南湖架构 Fudian FPU 设计与实现</b></td><td><b>李乾若</b><br>中科院计算所</td></tr>
<tr><td>10:05</td><td><b>基于香山实现的标签化体系结构</b></td><td><b>蔡洛姗</b><br>中科院计算所</td></tr>
<tr><td>10:20</td><td><b>香山性能瓶颈分析</b></td><td><b>高泽宇&amp;唐浩晋</b><br>中科院计算所</td></tr>
<tr><td>10:30</td><td><b>香山处理器的 DFT 设计</b></td><td><b>何志恒</b><br>鹏城实验室</td></tr>
<tr><td>10:40</td><td><b>香山 tutorial</b></td><td><b>陈国凯、李昕、陈熙、张梓悦等</b><br>中科院计算所</td></tr>

</tbody>
</table>
<br><br><br>
</div>


### 8/25 周四·上午·B分会场
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>09:00</td><td><b>Session B5</b></td><td><b></b><br></td></tr>
<tr><td>09:00</td><td><b>Implementing RISC-V linker relaxation in lld</b></td><td><b>Fangrui Song</b><br>Google</td></tr>
<tr><td>09:30</td><td><b>Scalable Matrix - The missing part of RISC-V Scalable Vector</b></td><td><b>伍华林</b><br>兆松科技</td></tr>
<tr><td>10:00</td><td><b>Challenges with multi-level loops RISC-V Vector Auto Vectorizations in LLVM</b></td><td><b>晏明</b><br>兆松科技</td></tr>
<tr><td>10:30</td><td><b>针对RISC-V架构的代码密度优化--编译、汇编、链接协同优化</b></td><td><b>王锋</b><br>卡姆派乐</td></tr>
<tr><td>11:00</td><td><b>Session B6</b></td><td><b></b><br></td></tr>
<tr><td>11:00</td><td><b>SIM-V: Ultra-Fast RISC-V Full System Simulation for Early Software Development and Test</b></td><td><b>Lukas Jünger and Jan Weinstock</b><br>MachineWare GmbH</td></tr>
<tr><td>11:20</td><td><b>GEM5中的RISC-V V拓展支持</b></td><td><b>刘阳、胡轩</b><br>中科院软件所PLCT实验室</td></tr>
<tr><td>11:50</td><td><b>Open-source CPU profiling tools for open-source ISA cores RISC-V</b></td><td><b>Ley Foon Tan and Dizhao Wei</b><br>赛昉科技</td></tr>

</tbody>
</table>
<br><br><br>
</div>



### 8/25 周四·下午·A分会场
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>13:30</td><td><b>Session A7</b></td><td><b></b><br></td></tr>
<tr><td>13:30</td><td><b>OpenRigil：开源 RISC-V 密码学硬件密钥</b></td><td><b>Hongren Zheng and Jiuyang Liu</b><br>PLCT实验室</td></tr>
<tr><td>14:00</td><td><b>可信执行环境访问控制形式化验证</b></td><td><b>Fanlang Zeng, Xinliang Miao and Rui Chang</b><br>浙江大学</td></tr>
<tr><td>14:30</td><td><b>基于RISC-V的用户态虚拟机监控器DuVisor</b></td><td><b>糜泽羽</b><br>上海交大IPADS</td></tr>
<tr><td>15:00</td><td><b>玄铁处理器TEE解决方案</b></td><td><b>毛礼杰</b><br>阿里巴巴平头哥半导体</td></tr>
<tr><td>15:20</td><td><b>DASICS: Dynamic Address Space Isolation by Code Segment</b></td><td><b>卢天越、陈明宇</b><br>中科院计算所</td></tr>
<tr><td>15:50</td><td><b>Session A8</b></td><td><b></b><br></td></tr>
<tr><td>15:50</td><td><b>Semidynamic's RVV1.0 Out-of-order Vector Unit</b></td><td><b>Roger Espasa</b><br>SemiDynamics</td></tr>
<tr><td>16:10</td><td><b>Feature evolution of RISC-V processor IP</b></td><td><b>Alexander Kozlov</b><br>CloudBEAR</td></tr>
<tr><td>16:30</td><td><b>Customizing RISC-V Cores to accelerate Neural Networks</b></td><td><b>Jon Tayler</b><br>codasip</td></tr>
<tr><td>16:50</td><td><b>RISC-V Processors for Automotive and other Safety Critical Applications</b></td><td><b>Tommy Lin</b><br>SiFive</td></tr>
<tr><td>17:10</td><td><b>IMG RTXM-2200：Imagination第一个可授权的RISC-V Catapult CPU</b></td><td><b>Naresh Menon</b><br>Imagination Technologies 产品管理总监</td></tr>

</tbody>
</table>
<br><br><br>
</div>


### 8/25 周四·下午·B分会场
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>13:30</td><td><b>Session B7</b></td><td><b></b><br></td></tr>
<tr><td>13:30</td><td><b>基于RISC-V 的Linux 系统 - 发行版与软件生态</b></td><td><b>傅炜</b><br>Redhat</td></tr>
<tr><td>14:00</td><td><b>openEuler在RISC-V架构上的适配进展</b></td><td><b>席静</b><br>中科院软件所</td></tr>
<tr><td>14:20</td><td><b>BPF on Gentoo RISC-V</b></td><td><b>jakov smolic</b><br>Gentoo社区</td></tr>
<tr><td>14:50</td><td><b>聚元PolyOS: 面向RISC-V的AIoT嵌入式操作系统</b></td><td><b>Jiageng Yu</b><br>中科院软件所</td></tr>
<tr><td>15:20</td><td><b>AOSP 支持 RISC-V 移植工作进展报告</b></td><td><b>汪辰</b><br>中科南京软件技术研究院</td></tr>
<tr><td>15:50</td><td><b>在 Zephyr LTS 版本支持RISC-V 多核 SMP 及 AMP; </b></td><td><b>林宽钊</b><br>Andes</td></tr>
<tr><td>16:00</td><td><b>Session B8</b></td><td><b></b><br></td></tr>
<tr><td>16:00</td><td><b>RVCL：RISC-V计算软件栈进展</b></td><td><b>张先轶</b><br>澎峰科技</td></tr>
<tr><td>16:30</td><td><b>V8 RV32GC 移植项目进展介绍</b></td><td><b>邱吉</b><br>中科院软件所</td></tr>
<tr><td>16:40</td><td><b>OpenJDK RISC-V Port 将正式发布</b></td><td><b>朱艳红</b><br>华为技术有限公司</td></tr>
<tr><td>16:50</td><td><b>Java on RISC-V (OpenJDK Porting Work Update)</b></td><td><b>Xiaolin Zheng, Wei Kuai and Sanhong Li</b><br>阿里巴巴</td></tr>
<tr><td>17:00</td><td><b>OpenJDK的RV32G支持</b></td><td><b>史宁宁</b><br>中科院软件所PLCT实验室</td></tr>
<tr><td>17:10</td><td><b>深嵌入式场景下的RISC-V软件开发框架</b></td><td><b>方华启</b><br>芯来科技</td></tr>

</tbody>
</table>
<br><br><br>
</div>

### 8/26 周五·上午·A分会场
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>09:00</td><td><b>Session A9</b></td><td><b></b><br></td></tr>
<tr><td>09:00</td><td><b>RVBoards生态建设进展</b></td><td><b>张先轶</b><br>澎峰科技</td></tr>
<tr><td>09:10</td><td><b>OpenPPL：RISC-V高效推理“芯”体验</b></td><td><b>焦明俊</b><br>商汤科技</td></tr>
<tr><td>09:40</td><td><b>TVM AutoScheduler在Andes向量平台的加速效果; </b></td><td><b>张元铭、颜仲华、吴奕纬</b><br>Andes</td></tr>
<tr><td>10:00</td><td><b>在 Andes RISC-V 平台上运行 Out-of-Box MLPerf-Tiny 基准的效能分析</b></td><td><b>魏全佑; 李恒宽</b><br>Andes</td></tr>
<tr><td>10:20</td><td><b>Automatic translation of ARM Neon Intrinsics to RISC-V vector instructions</b></td><td><b>Arka Maity</b><br>赛昉科技</td></tr>
<tr><td>10:50</td><td><b>High performance GEMM on RISC-V</b></td><td><b>丘庆和吴志刚</b><br>赛昉科技</td></tr>
<tr><td>11:00</td><td><b>开源生态助力玄铁CPU开发</b></td><td><b>赵彬广</b><br>阿里巴巴集团 平头哥</td></tr>
<tr><td>11:20</td><td><b>基于RISC-V的边缘AI软硬件协同系统</b></td><td><b>李枫</b><br>独立开发者</td></tr>

</tbody>
</table>
<br><br><br>
</div>


### 8/26 周五·上午·B分会场
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>09:00</td><td><b>Implementing RISC-V processors with ISA extensions and functional safety features using ASIP Designer</b></td><td><b>Rick Wang and Patrick Verbist</b><br>synopsis</td></tr>
<tr><td>09:20</td><td><b>TL-Test: Cache Coherence Verification Framework for XiangShan</b></td><td><b>王凯帆</b><br>中科院计算所</td></tr>
<tr><td>09:40</td><td><b>香山分支预测器内外快速性能评估工具</b></td><td><b>陈国凯</b><br>中科院计算所</td></tr>
<tr><td>10:00</td><td><b>Haawking IDE 2.0：Live Watch 赋能电机系统实时调试</b></td><td><b>陈骅</b><br>北京中科昊芯科技有限公司</td></tr>
<tr><td>10:10</td><td><b>嵌入式AI在RISC-V上的实践（基于RT-Thread操作系统）</b></td><td><b>叶昌</b><br>RT-Thread</td></tr>
<tr><td>10:40</td><td><b>DiffTest：基于香山处理器的敏捷验证实践</b></td><td><b>徐易难</b><br>中科院计算所</td></tr>
<tr><td>11:10</td><td><b>芯片敏捷设计与验证之路</b></td><td><b>赖晓铮</b><br>华南理工大学计算机学院</td></tr>

</tbody>
</table>
<br><br><br>
</div>




( 注意8月26日下午只有一个分会场，分会场A是没有的 )

### 8/26 周五·下午·B分会场
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>13:30</td><td><b>Session A11</b></td><td><b></b><br></td></tr>
<tr><td>13:30</td><td><b>Riscv领域知识产权风险与知识产权保护</b></td><td><b>张俊霞</b><br>中国信通院</td></tr>
<tr><td>14:00</td><td><b>借力RISC-V开放生态，探索产学研合作模式</b></td><td><b>胡灿</b><br>芯来科技</td></tr>
<tr><td>14:20</td><td><b>基于RISC-V的全系统教学实验的设计与实现</b></td><td><b>陈璐</b><br>南京大学</td></tr>
<tr><td>14:40</td><td><b>基于RISC-V的软硬件贯通实验教学体系设计与人才培养</b></td><td><b>常瑞</b><br>浙江大学</td></tr>
<tr><td>15:10</td><td><b>面向系统能力培养的基于RISC-V架构跨课程系统实验</b></td><td><b>袁春风、吴海军、王炜、蒋炎岩、余子濠</b><br>南京大学</td></tr>
<tr><td>15:40</td><td><b>ChocoPy LLVM: An Undergraduate RISC-V Compiler Course Project</b></td><td><b>Yiwei Yang</b><br>上海科技大学</td></tr>
<tr><td>16:00</td><td><b>RISC-V 调试和跟踪技术更新以及国内的发展</b></td><td><b>行明安</b><br>劳特巴赫中国技术经理</td></tr>
<tr><td>16:20</td><td><b>From Development to Production: Comprehensive one-stop tools solution for RISC-V</b></td><td><b>Ryan Sheng</b><br>SEGGER</td></tr>

</tbody>
</table>

[![输入图片说明](https://foruda.gitee.com/images/1660253152616333305/屏幕截图.png "屏幕截图.png")](https://riscv-summit-china.com/)

### Main Aug 24 Wednesday · Morning · Main Track
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>09:00</td><td><b>Greeting from RVI</b></td><td><b>Calista Redmond</b><br>RISC-V International</td></tr>
<tr><td>09:05</td><td><b>Greeting from NI Guangnan</b></td><td><b>NI Guangnan</b><br>CAS</td></tr>
<tr><td>09:10</td><td><b>Greeting from Hosts</b></td><td><b>ISCAS</b><br>ShanghaiTech University</td></tr>
<tr><td>09:20</td><td><b>The Road Ahead</b></td><td><b>Mark Himelstein</b><br>RISC-V International</td></tr>
<tr><td>09:40</td><td><b>Keynote from Alibaba T-Head</b></td><td><b>Jianyi Meng</b><br>VP, T-Head Semiconductor Co., Ltd</td></tr>
<tr><td>10:10</td><td><b>NUCLEI™ CPU IP Accelerating RISC-V Ecosystem Development in China</b></td><td><b>Jianying Peng</b><br>Nuclei System</td></tr>
<tr><td>10:40</td><td><b>XiangShan: practice of open source high-performance RISC-V processor agile design</b></td><td><b>Yungang Bao</b><br>ICT, CAS</td></tr>
<tr><td>11:00</td><td><b>Overtaking with RISC-V – RISC-V Benefits for Applications</b></td><td><b>Charlie Su</b><br>Andes</td></tr>
<tr><td>11:20</td><td><b>Full-stack MCU+ technology sharing based on RISC-V QingKe microprocessor</b></td><td><b>Patrick Yang</b><br>Nanjing Qingheng Microelectronics Co., Ltd.</td></tr>
<tr><td>11:40</td><td><b>New Product Release</b></td><td><b>Not public yet</b><br></td></tr>

</tbody>
</table>
<br><br><br>
</div>


### A3&4 Aug 24 Wednesday · Afternoon · Track A
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>13:30</td><td><b>Session A3</b></td><td><b></b><br></td></tr>
<tr><td>13:30</td><td><b>Intel Investment to Help Deliver a Thriving RISC-V Ecosystem</b></td><td><b>Gary Martz and Jiangang Duan</b><br>Intel</td></tr>
<tr><td>14:00</td><td><b>RISC-V SOCs: Powering Embedded Video/Audio Computing</b></td><td><b>Shaorui Huang</b><br>Allwinner</td></tr>
<tr><td>14:30</td><td><b>Native Development for RISC-V</b></td><td><b>Yuning Liang</b><br>Xcalibyte</td></tr>
<tr><td>14:50</td><td><b>World's first mass-produced high-performance RISC-V multimedia processor</b></td><td><b>Jing Zhao</b><br>StarFive Technology</td></tr>
<tr><td>15:10</td><td><b>World's first high-performance RISC-V open source SBC——VisionFive 2 world premiere</b></td><td><b>Li Xu</b><br>StarFive Technology</td></tr>
<tr><td>15:30</td><td><b></b></td><td><b></b><br></td></tr>
<tr><td>15:30</td><td><b>Session A4</b></td><td><b></b><br></td></tr>
<tr><td>15:30</td><td><b>RISC-V processor embedded eco-system practice and thinking</b></td><td><b>Xiaoqing He</b><br>ESBF Community</td></tr>
<tr><td>16:00</td><td><b>OneOS embraces the RISC-V ecosystem and empowers the Internet of Things Industry</b></td><td><b>Hongwei Zhang</b><br>ChinaMobile</td></tr>
<tr><td>16:30</td><td><b>Haawking RISC-V Based DSP  -- Success Story in Servo Drive  and VFD</b></td><td><b>Yuyao Li</b><br>Beijing Haawking Technology Co.,Ltd</td></tr>
<tr><td>16:40</td><td><b>Introducing a New Level of RISC-V CPU Performance</b></td><td><b>Itai Yarom</b><br>MIPS</td></tr>
<tr><td>17:00</td><td><b>Memory subsystems to match the requirements of various embedded applications </b></td><td><b>Bing Yu</b><br>Andes</td></tr>

</tbody>
</table>
<br><br><br>
</div>


### B3&4 Aug 24 Wednesday · Afternoon · Track B
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>13:30</td><td><b>Session B3</b></td><td><b></b><br></td></tr>
<tr><td>13:30</td><td><b>Up to 800Mhz! HPMicro Ultra-high-performance RISC-V MCU applied in high-end industrial areas</b></td><td><b>Qi Xu</b><br>HPMicro Semiconductor Co.,Ltd.</td></tr>
<tr><td>13:50</td><td><b>Ventus: An Open Source Hardware Implementation of GPGPU Based on RISC-V Vector Extension </b></td><td><b>Kexiang Yang</b><br>School of Integrated Circuits, Tsinghua University</td></tr>
<tr><td>14:20</td><td><b>Nuclei RISC-V Core SIMD  Implementation and Related Software Optimization</b></td><td><b>Jin Hu</b><br>Nuclei System</td></tr>
<tr><td>14:30</td><td><b>CUDA on RISC-V GPGPU</b></td><td><b>Pinzheng Xia</b><br>StarFive Technology</td></tr>
<tr><td>14:40</td><td><b>The Design and Application of RISC-V S-mode Memory Protection Unit</b></td><td><b>Dong Du, Bicheng Yang</b><br>School of Software, Shanghai Jiao Tong University (SJTU)</td></tr>
<tr><td>15:10</td><td><b>Session B4</b></td><td><b></b><br></td></tr>
<tr><td>15:10</td><td><b>Embracing Innovation: How the RISC-V Software Ecosystem rethinks Fragmentation as Differentiation</b></td><td><b>Philipp Tomsich</b><br>VRULL GmbH</td></tr>
<tr><td>15:40</td><td><b>QEMU &amp; LINUX support for RV32 compat mode</b></td><td><b>Guo Ren, LIU Zhiwei</b><br>Alibaba T-Head</td></tr>
<tr><td>16:10</td><td><b>Extending RISC-V ISA for Tightly Integrated Layer-Wise Quantized Mixed-Precision Inference at the Edge</b></td><td><b>Xinfei Guo, Vaibhav Verma, Xiaotian Zhao and Mircea Stan</b><br>Shanghai Jiao Tong University</td></tr>
<tr><td>16:40</td><td><b>The Atomicity Issues on Programming an IOPMP</b></td><td><b>Paul Shan-Chyun Ku</b><br>Andes</td></tr>
<tr><td>17:10</td><td><b>IOMMU for Xuantie-based Platforms</b></td><td><b>Siqi Zhao</b><br>T-Head, Alibaba Group</td></tr>

</tbody>
</table>
<br><br><br>
</div>

### A5 Aug 25 Thursday · Morning · Track A
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>09:00</td><td><b>Session A5</b></td><td><b></b><br></td></tr>
<tr><td>09:00</td><td><b>The Evolution of Frontend Architecture of XiangShan Processor</b></td><td><b>Lingrui Gou</b><br>ICT, CAS</td></tr>
<tr><td>09:15</td><td><b>The Design and Improvement of Backend Pipeline in Nanhu Micro-Architecture</b></td><td><b>Zifei Zhang</b><br>ICT, CAS</td></tr>
<tr><td>09:25</td><td><b>The Design and Improvement of Memory Management Unit in Nanhu Micro-Architecture</b></td><td><b>Zifei Zhang</b><br>ICT, CAS</td></tr>
<tr><td>09:35</td><td><b>The Design and Implementation of Memory Access Sub-System in Nanhu Micro-Architecture</b></td><td><b>Huaqiang Wang</b><br>ICT, CAS</td></tr>
<tr><td>09:45</td><td><b>HuanCun: A Non-blocking Cache Design in Nanhu Micro-Architecture</b></td><td><b>Kaifan Wang</b><br>ICT, CAS</td></tr>
<tr><td>09:55</td><td><b>The Design and Implementation of Fudian FPU in Nanhu Micro-Architecture</b></td><td><b>Qianruo Li</b><br>ICT, CAS</td></tr>
<tr><td>10:05</td><td><b>The Design and Implementation of Labeled von Neumann Architecture on XiangShan </b></td><td><b>Luoshan Cai</b><br>ICT, CAS</td></tr>
<tr><td>10:20</td><td><b>Performance Bottleneck Analysis of XiangShan Processor</b></td><td><b>Zeyu Gao &amp; Haojin Tang</b><br>ICT, CAS</td></tr>
<tr><td>10:30</td><td><b>DFT Design of XiangShan Processor</b></td><td><b>Zhiheng He</b><br>ICT, CAS</td></tr>
<tr><td>10:40</td><td><b>Xiangshan tutorial</b></td><td><b>Guokai Chen</b><br>ICT, CAS</td></tr>

</tbody>
</table>
<br><br><br>
</div>


### B5&6 Aug 25 Thursday · Morning · Track B
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>09:00</td><td><b>Session B5</b></td><td><b></b><br></td></tr>
<tr><td>09:00</td><td><b>Implementing RISC-V linker relaxation in lld</b></td><td><b>Fangrui Song</b><br>Google</td></tr>
<tr><td>09:30</td><td><b>Scalable Matrix - The missing part of RISC-V Scalable Vector</b></td><td><b>Hualin Wu</b><br>Terapines' Technology</td></tr>
<tr><td>10:00</td><td><b>Challenges with multi-level loops RISC-V Vector Auto Vectorizations in LLVM</b></td><td><b>Ming Yan</b><br>Terapines' Technology</td></tr>
<tr><td>10:30</td><td><b>CAL: a Joint Method of Compiler, Assembler, and Linker on Code-size Optimiztion for RISC-V</b></td><td><b>Feng Wang</b><br>Compiler Dev</td></tr>
<tr><td>11:00</td><td><b>Session B6</b></td><td><b></b><br></td></tr>
<tr><td>11:00</td><td><b>SIM-V: Ultra-Fast RISC-V Full System Simulation for Early Software Development and Test</b></td><td><b>Lukas Jünger and Jan Weinstock</b><br>MachineWare GmbH</td></tr>
<tr><td>11:20</td><td><b>RISC-V V Extension Support in gem5</b></td><td><b>Yang Liu, Xuan Hu</b><br>PLCT Lab, ISCAS</td></tr>
<tr><td>11:50</td><td><b>Open-source CPU profiling tools for open-source ISA cores RISC-V</b></td><td><b>Ley Foon Tan and Dizhao Wei</b><br>StarFive Technology</td></tr>

</tbody>
</table>
<br><br><br>
</div>



### A7&8 Aug 25 Thursday · Afternoon · Track A
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>13:30</td><td><b>Session A7</b></td><td><b></b><br></td></tr>
<tr><td>13:30</td><td><b>OpenRigil: Open Source RISC-V Cryptographic Hardware Token</b></td><td><b>Hongren Zheng and Jiuyang Liu</b><br>PLCT Lab, ISCAS</td></tr>
<tr><td>14:00</td><td><b>Formal Verification of Access Control in Trusted Execution Environment</b></td><td><b>Fanlang Zeng, Xinliang Miao and Rui Chang</b><br>Zhejiang University</td></tr>
<tr><td>14:30</td><td><b>DuVisor: a User-level Hypervisor on RISC-V</b></td><td><b>Zeyu Mi</b><br>IPADS, SJTU</td></tr>
<tr><td>15:00</td><td><b>RISC-V based TEE Solution for XuanTie CPU</b></td><td><b>Lijie Mao</b><br>Alibaba T-Head</td></tr>
<tr><td>15:20</td><td><b>DASICS: Dynamic Address Space Isolation by Code Segment</b></td><td><b>Tianyue Lu and Mingyu Chen</b><br>ICT, CAS</td></tr>
<tr><td>15:50</td><td><b>Session A8</b></td><td><b></b><br></td></tr>
<tr><td>15:50</td><td><b>Semidynamic's RVV1.0 Out-of-order Vector Unit</b></td><td><b>Roger Espasa</b><br>SemiDynamics</td></tr>
<tr><td>16:10</td><td><b>Feature evolution of RISC-V processor IP</b></td><td><b>Alexander Kozlov</b><br>CloudBEAR</td></tr>
<tr><td>16:30</td><td><b>Customizing RISC-V Cores to accelerate Neural Networks</b></td><td><b>Jon Tayler</b><br>CodeSip</td></tr>
<tr><td>16:50</td><td><b>RISC-V Processors for Automotive and other Safety Critical Applications</b></td><td><b>Tommy Lin</b><br>SiFive</td></tr>
<tr><td>17:10</td><td><b>IMG RTXM-2200: Imagination’s first Licensable RISC-V Catapult CPU</b></td><td><b>Naresh Menon</b><br>Imagination Technologies</td></tr>

</tbody>
</table>
<br><br><br>
</div>


### B7&8 Aug 25 Thursday · Afternoon · Track B
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>13:30</td><td><b>Session B7</b></td><td><b></b><br></td></tr>
<tr><td>13:30</td><td><b>Linux on RISC-V - Distro and ecosystem</b></td><td><b>Wei Fu</b><br>Redhat</td></tr>
<tr><td>14:00</td><td><b>openEuler on RISC-V - past, present, and future</b></td><td><b>Jing Xi</b><br>ISCAS</td></tr>
<tr><td>14:20</td><td><b>BPF on Gentoo RISC-V</b></td><td><b>jakov smolic</b><br>Gentoo Community</td></tr>
<tr><td>14:50</td><td><b>PolyOS: RISC-V based AIoT Operating System</b></td><td><b>Jiageng Yu</b><br>ISCAS</td></tr>
<tr><td>15:20</td><td><b>AOSP for RISC-V Work Progress Report</b></td><td><b>Chen Wang</b><br>NIST</td></tr>
<tr><td>15:50</td><td><b>Supporting RISC-V SMP and AMP in the Zephyr LTS Release</b></td><td><b>Kuan-Jau Lin</b><br>Andes</td></tr>
<tr><td>16:00</td><td><b>Session B8</b></td><td><b></b><br></td></tr>
<tr><td>16:00</td><td><b>RVCL：RISC-V Compute Library</b></td><td><b>Xianyi Zhang</b><br>PerfXLab （Beijing）Technology co.，ltd.</td></tr>
<tr><td>16:30</td><td><b>Enable V8 JavaScript Engine on RV32GC</b></td><td><b>Ji Qiu</b><br>ISCAS</td></tr>
<tr><td>16:40</td><td><b>OpenJDK RISC-V Port to be released</b></td><td><b>Yanhong Zhu</b><br>Huawei Technologies</td></tr>
<tr><td>16:50</td><td><b>Java on RISC-V (OpenJDK Porting Work Update)</b></td><td><b>Xiaolin Zheng, Wei Kuai and Sanhong Li</b><br>Alibaba</td></tr>
<tr><td>17:00</td><td><b>OpenJDK on RV32G</b></td><td><b>Ningning Shi</b><br>PLCT Lab, ISCAS</td></tr>
<tr><td>17:10</td><td><b>RISC-V Software Development Framework for Deeply Embedded Scenario</b></td><td><b>Huaqi Fang</b><br>Nuclei System</td></tr>

</tbody>
</table>
<br><br><br>
</div>

### A9 Aug 26 Friday · Morning · Track A
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>09:00</td><td><b>Session A9</b></td><td><b></b><br></td></tr>
<tr><td>09:00</td><td><b>RVBoards Ecosystem Update</b></td><td><b>Xianyi Zhang</b><br>PerfXLab （Beijing）Technology co.，ltd.</td></tr>
<tr><td>09:10</td><td><b>OpenPPL：High performance inference framework on the RISC-V platform</b></td><td><b>Mingjun Jiao</b><br>Sense Time</td></tr>
<tr><td>09:40</td><td><b>TVM AutoScheduler on Andes Bare-Metal Platform with Vector Extension</b></td><td><b>Yuan-Ming Chang ; Chung-Hua Yen;  I-Wei Wu</b><br>Andes</td></tr>
<tr><td>10:00</td><td><b>The Out-of-Box MLPerf-Tiny-benchmark on Andes RISC-V Platform</b></td><td><b>魏全佑; 李恒宽</b><br>Andes</td></tr>
<tr><td>10:20</td><td><b>Automatic translation of ARM Neon Intrinsics to RISC-V vector instructions</b></td><td><b>Arka Maity</b><br>StarFive Technology</td></tr>
<tr><td>10:50</td><td><b>High performance GEMM on RISC-V</b></td><td><b>Calvin Qiu and Zhigang Wu</b><br>StarFive Technology</td></tr>
<tr><td>11:00</td><td><b>OPENSOURCE ACCELLERATE XUANTIE CPU DEVELOPMENT</b></td><td><b>Binguang Zhao</b><br>Alibaba Group T-HEAD</td></tr>
<tr><td>11:20</td><td><b>RISC-V based HW-SW System for Edge AI</b></td><td><b>Feng Li</b><br>independent Developer</td></tr>

</tbody>
</table>
<br><br><br>
</div>


### B Aug 26 Friday · Morning · Track B
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>09:00</td><td><b>Implementing RISC-V processors with ISA extensions and functional safety features using ASIP Designer</b></td><td><b>Rick Wang and Patrick Verbist</b><br>synopsis</td></tr>
<tr><td>09:20</td><td><b>TL-Test: Cache Coherence Verification Framework for XiangShan</b></td><td><b>Kaifan Wang</b><br>ICT, CAS</td></tr>
<tr><td>09:40</td><td><b>Branch Predictor Related Performance Evaluation Tools of XiangShan</b></td><td><b>Guokai Chen</b><br>ICT, CAS</td></tr>
<tr><td>10:00</td><td><b>Haawking IDE 2.0：Live Watch Enabling Motor Control System Real-time Debugging</b></td><td><b>Hua Chen</b><br>Beijing Haawking Technology Co.,Ltd</td></tr>
<tr><td>10:10</td><td><b>Exploration and Practice of Edge AI on the RISC-V architecture (Based on the RT-Thread real time operating system)</b></td><td><b>Chang Ye</b><br>RT-Thread</td></tr>
<tr><td>10:40</td><td><b>DiffTest: Practice of Agile Processor Verification on XiangShan</b></td><td><b>Yinan Xu</b><br>ICT, CAS</td></tr>
<tr><td>11:10</td><td><b>The Road to Agile Design and Verification of Chips</b></td><td><b>Xiaozheng Lai</b><br>School of Computer Science, South China University of Technology</td></tr>

</tbody>
</table>
<br><br><br>
</div>




( Attention. Aug 26 Afternoon Only one Track ，Track A have not ）

### A11 Aug 26 Friday · Afternoon · Track B
<hr>
<table style="margin:auto;width:100%">
<colgroup>
<col width="10%">
<col width="60%">
<col width="30%">
</colgroup>
<tbody>

<tr><td>13:30</td><td><b>Session A11</b></td><td><b></b><br></td></tr>
<tr><td>13:30</td><td><b>Intellectual property risk and intellectual property protection in the field of RISC-V</b></td><td><b>Junxia Zhang</b><br>CAICT</td></tr>
<tr><td>14:00</td><td><b>Leverage RISC-V open ecosystem，Explore industry-university-research cooperation</b></td><td><b>Can Hu</b><br>Nuclei System</td></tr>
<tr><td>14:20</td><td><b>Design and implementation of full-system teaching experiment based on RISC-V</b></td><td><b>Lu Chen</b><br>Nanjing University</td></tr>
<tr><td>14:40</td><td><b>Designing the experiments of software-hardware integration and talent cultivation based on RISC-V arichtecture</b></td><td><b>Rui Chang</b><br>Zhejiang University</td></tr>
<tr><td>15:10</td><td><b>Cross-curricular system experiment based on RISC-V architecture for system ability training</b></td><td><b>Chunfeng Yuan, et, al</b><br>Nanjing University</td></tr>
<tr><td>15:40</td><td><b>ChocoPy LLVM: An Undergraduate RISC-V Compiler Course Project</b></td><td><b>Yiwei Yang</b><br>ShanghaiTech University</td></tr>
<tr><td>16:00</td><td><b>RISC-V debugging and tracing technology updates and domestic developments in China</b></td><td><b>Frank Xing</b><br>Lauterbach China</td></tr>
<tr><td>16:20</td><td><b>From Development to Production: Comprehensive one-stop tools solution for RISC-V</b></td><td><b>Ryan Sheng</b><br>SEGGER</td></tr>

</tbody>
</table>
