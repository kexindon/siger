![输入图片说明](https://foruda.gitee.com/images/1661972911570892647/b64bba8a_5631341.jpeg "在这里输入图片标题")

# “BBC 电脑，阿姨的 Micro” 

这是我帮小牧订正的译文，也许只有我才能更深地理解这句话的意义。它正式我一直寻觅的北向文化的直接表现。1980年，读者们没有几个能比它更 “老” 的吧，这句话出现在一本科普著作中，呈现给读者的是蜚声全球的树莓派的创始人（全文流露出对前辈的致敬，将自己重现PC时代英国的辉煌的成绩，归功于前辈的全民计算机素养的努力），而小牧和我的其他学生正在码更翻译，尽管只能在假期时稍微加快些步伐，但是以开源的方式，去翻译一家开源硬件闻名的树莓派基金会出版的科技史的读物，更加有意义啦。在 SIGer 编辑部有一个工作流程，凡事要先立项，《80年代的英国家用电脑》的翻译，是 SIGer 刚成立三个月，就以 STEM 为题确立的项目啦。而 SIGer 编辑部都是为了科普开源，让 学生们通过，开源协作的方式编写一本科普杂志。和 SIGer 最早膜拜的《开源指北》一样，写作分享是再好不过的形式了。SIGer 创刊号为第零期，则借用了指北，感谢了一下引着我和学生进入开源社区的 OE 社区，曰《欧拉指北》，说来惭愧，当时的我，竟然花了半个月才 PR 成功，还是导师的帮助，而最期待的就是得到帮助，我反复使用了 “拥抱” 来表达对社区的期待，然鹅，凡事只能靠自己，SIGer 编辑部已经拥有了超过 100 名编委啦。

说好的不吐槽的，再给《开源观止》的三个选题中，还是 “夹杂” 了一个最能有机会吐槽的内容《小学生一套代码搞定一切 FLUTTER 框架，搭建 SIGer 学习型社区》，只为了教会学生如何PR，分享自己的学习笔记。所有孩子初上 Gitee 的蒙圈肯定不能成为今天的主题，“用工程的方式，以应用的角度，开始学习” 比较适合科普开源的范畴，能够让同学们通过体会 PR 建立一个先验经验，才好进一步去解释什么是开源，开源能做什么？为什么要开源？这些基本问题。墙内种花，墙外香，已经 261期的 SIGer 依然要耐心地为新编委培训怎么 PR，SIGer 却真正地成长为了 科普自媒体。已经生长出了十多个频道。这不，8月的下半个月，我这个主编 “开小差” 去了 RVSC 2022（第2届 RISCV 中国峰会）现场采访，只能放任孩子们不管啦，能收到 小牧的译文 “作业” 是我最欣慰的事情了。

# RISCV 2022 中国峰会，青少年专题：对话·未来·教育

借着 FULL INSIDE STORY 的话头儿，我今天就是来讲故事的。连 “新闻” 都能让我煽情一把讲成故事。

> 本次 SIGer 出品的 RVSC 2022 青少年科普专题的 主题是 对话·未来·教育，饱含着对未来的愿景，期待 RISC-V 的能成为一份来自未来的礼物，2021 RVSC 的集锦收录在 B站 RISCV国际基金会 中，“年轻人的第一个ISA” 就是这个意思。

> 石榴派是 2021 RVSC 我带去的礼物之一，另一个礼物 RV4kids 已经成长为 RV少年 科普自媒体 专注报道青少年视角的RISC-V产业的动态和学习资源。两者有机结合，石榴派是 RISCV 的青少年习作，也是实践成果的展现，RV少年 —— SIGer 学习小组 是学习笔记的呈现和分享，相辅相成。

> 之所以用补课，是2021 RVSC 同为参展“单位”，间隙也造访了友商的展台，只是当时对 RISCV 固有的印象，跑不了石榴派的 DESKTOP 应用，整个 RISCV 产业还没有太多高性能开发板能够满足所有“需求”，整体在爬升阶段。今天看到了 这则激动人心的消息 《 Ubuntu IOT 适配成功 RISCV 开发板》，我振奋啦。石榴派除了南向发展，推出了 FPGA 开发板，跑 RISCV 软核，用 ISA 写CPU，同时北向应用对接科普第一线，一直都是借鉴 树莓派社区的经验，作为 LINUX 发行版，也是石榴派一直在做的事情，汇集了一众时刻开源科普的应用，对接主流的发行版都做过适配。包括 DEBIAN, RASPBAIN, UBUNTU。其中 UBUNTU IOT 在 RASPBIAN 4B+ 的适配成功，大大简化了 石榴派应用构建的效率。也是 2022年 一年我的标准工作环境，此时此刻编写的 SIGER 文章，就是 UBUNTU IOT。桌面，引用了 Canonical 去年适配树莓派时使用的 “用心做好DESKTOP” 的标语，尽管它是突出 UBUNTU IOT 的 LINUX 内核。但这不能掩盖，开发板本身的优秀，才能支撑起 UBUNTU IOT的顺利运行。

  - 我想从现在起，我可以用上国人自研的 RISC-V 开发板，验证应用，搭建学习环境，撰写 RISCV 科普专栏啦。

> 我作为科普志愿者，除了专长的人工智能科普，在社区教授小朋友用AI技术复原的非遗民族棋小游戏，去年参加 RVSC 2021的视频，在同学们中也早就传遍啦。只是手里的 RISCV FPGA 开发板，只能小小地运行几个示范应用，现在非遗民族棋项目已经和国际社区合作，将民族棋中国史项目，升级为国际民族棋史，世界各地的游戏样本已经 1314 个，使用通用的人工智能引擎予以示教，UBUNTU IOT 是最顺畅的，不只通用的简单UI的版本，复杂精美的UI界面的示教程序同样可以畅快运行。如果再给小朋友们演示世界民族棋小游戏的时候，一块RISCV开发板在手，就全都有啦。非常值得期待。

  - 熟悉我的朋友都见过，我头像的照片，一个小姑娘，举着她画的彩色的五角星，它也是一个非遗小游戏，叫 LuckyStar 是首个被国际古棋盘游戏史项目收录的中国游戏，我带同学们来开源社区，接受社区的 “拥抱” 也有它的功劳。我想带上一个完整地可以自由运行 LUCKYSTAR 的 RISCV 开发板，给她展示石榴派最新的科普成果，1314 个世界民族棋。她一定会开心的。她是我的学生，社区小志愿者，绘制这幅精美的幸运星，已经征服了全世界，其中一幅就陈列在 树莓派基金会的图书馆里。

> 我们都相信愿的力量，RVSC 2022再次地欣欣向荣，就说明，整个 RISCV 中国社区的活力，如果再更加聚集在一起，合力攻坚，我们共同的未来，一定是美景无限的。这份抒情，不只给 友商 点赞，而是给 整个 RVSC 2022的全体嘉宾的。本期专访，相对偷懒，没有深度访问，相信，在接下来石榴派的适配工作中一定会有更多交流，再行分享。附上 2021 RVSC 的回顾，作为弥补，充实本期专题，作为 第一个正式的 RVSC 2022 对话未来教育的专题，奉献给各位老师。

页底的10张海报，就是 SIGer 为了预热 RVSC 2022 峰会，采用相同的策略，同演讲人同参展单位，对比两年的主题，重温回放2021，进行预热的手段，开启了本次峰会的采访任务。因为有去年的经验，完整的录像回顾，我有些许偷懒，而且双会场方式，一定无法全部聆听，尽管主题会有分类，但作为媒体的我就只好捉襟见肘了。总体感觉，线上不如线下，会务工作繁琐不讨好，除了几大赞助商的视频会后发布在了官方B站，其他演讲基本上都无缘重温啦，或许技术更新，真的没有想象的那么快吧。按计划，我讲目标锁定在了 一生一芯 技术论坛，和北京分会场。

<p><a href="luckySTARfive.md" target="_blank"><img width="19%" src="https://foruda.gitee.com/images/1660808612456264987/241starfive副本.jpeg" title="241.luckySTARfive.md"></a> <a href="2022RVSC收视指南.md" target="_blank"><img width="19%" src="https://foruda.gitee.com/images/1660689936543886738/236grass副本.jpeg" title="236.收视指南"></a> <a href="RVfpga.md" target="_blank"><img width="19%" src="https://foruda.gitee.com/images/1660116715577888459/227rvfpga副本.jpeg" title="227.RVfpga"></a> <a href="2021RVSCeduforum.md" target="_blank"><img width="19%" src="https://foruda.gitee.com/images/1660116821628023571/226教育生态2021副本.jpeg" title="226.教育生态2021"></a> <a href="2021RVSCvolunteer.md" target="_blank"><img width="19%" src="https://foruda.gitee.com/images/1660116913448107145/225科技志愿者2021副本.jpeg" title="225.科技志愿者2021"></a> <a href="蜂鸟未来.md" target="_blank"><img width="19%" src="https://foruda.gitee.com/images/1660081541161294100/224蜂鸟副本.jpeg" title="224.蜂鸟未来"></a> <a href="RVOS.md" target="_blank"><img width="19%" src="https://foruda.gitee.com/images/1660117036115934026/223rvos副本.jpeg" title="223.RVOS"></a> <img width="19%" src="https://foruda.gitee.com/images/1660083409570451493/222tarsier副本.jpeg" title="222.OEsigRISCVtarsier"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0802/070935_8958a570_5631341.jpeg" title="数独液晶小黑板"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0727/024539_81514f74_5631341.jpeg" title="2022 2nd RISC-V 中国峰会 rvsc2022 8.24-8.26"></p>

![输入图片说明](https://foruda.gitee.com/images/1662307730831429276/b828bcde_5631341.jpeg "webwxgetmsgimg.jpg")

# SIGer 少年芯，从北向南的开源文化之旅

峰会主论坛较一生一芯技术论坛，提前结束的。北京分会场发布了政产学研用五位一体的联盟，虽然耀眼，但不如一生一芯更吸引青少年科普媒体的 SIGer，我也是第一次以 SIGer 主编的身份，现场采访 一生一芯，补上之前只能线上与导师们见面的遗憾。为了感谢 包云岗 老师的厚爱，我特地准备了 “厚礼” 相送，《SIGer 少年芯》青少年 RISCV 开源实践笔记，这是我们共同的愿望，早日见证 “中学生” 在一生一芯项目中，开发出自己的芯片。我们还有一个默契，把这些孩子比喻成 “红小鬼”。象征着，不畏艰难万险，随着长征抵达延安的红军小战士。尽管现在还没有实现，但方向是正确的，我们只需要坚定向前。

从北向南的开源文化之旅，完全是今天新加的主题，用 少年芯 代表整个计算机体系结构的最南向，SIGer 已经做到了，就 RVSC 2022 的教育主题之一：全栈学习我们又进了一步！是胜任的。《SIGer 少年芯》从这个意义讲，有了更大的外延，文化的外延，不只代表技术栈的全栈学习，而是讲 RISC-V 作为开源实践的载体，从文化科普的角度，去传播和实践开源科普，才是今日发布这个主题的目的。北向开源文化做指引，指导青少年的开源项目实践，包括 RISCV，少年芯则代表了心上萌芽，锐意进取的青少年。

以下是我论坛现场奉上《[SIGer 少年芯](https://gitee.com/flame-ai/siger/blob/master/RISC-V/SIGer%E5%B0%91%E5%B9%B4%E8%8A%AF.pdf)》的即兴分享：

（主持人：SIGer青少年开源文化期刊主编，袁德俊老师，在中学生社区积极推动一生一芯，请他分享一些感想，欢迎袁老师。）大家好，我再补充一下我的身份，你看啊我现在换上了一生一芯的T恤，大家看穿白色T恤的同学呢，大家一定知道他们的身份是志愿者。我还有个身份是，科普志愿者，所以今天（给大家）带来的SIGer是，科普自媒体。

我今天来呢，主要是带着感谢来的，为什么呢？我和SIGer杂志的读者都是包云岗老师的粉丝，所以今天是带礼物来的，送上（我手中的）纸质版的期刊。那么，首先，今天时间比较短，有很多故事（就省略啦），我和包老师之间。[他一直都很支持我们的（青少年科普）活动](https://gitee.com/flame-ai/siger/tree/master/sig/ysyx)，所以，我们有一个默契（期待有一天中学生在“一生一芯”计划中设计出人生的第一颗CPU芯片！2021.9.9），就是刚才发言的这些同学（高中生代表分享一生一芯的学习心得），可以再扩大一些，我们对他们有一个昵称，是什么呢？叫 “红小鬼”。我今天带来这本杂志呢，也有个昵称，叫做：“红小鬼学习笔记”。

里面，我选了三个（红小鬼）代表。一个是初中生，刚初一的学生（我在学校里带一些社团），她的感言是什么呢？就是，通过 RISC-V 的学习，来探索科技。第二个呢？是高中生的学校的AI社团，他通过一生一芯计划（包括 RISCV）的学习，他认为，现在已经到了一个软硬齐飞的时代。第3个代表呢，是大学学习安全的学生，也我们，大家看我们的SIGer期刊（左下角）sig/ysyx，也就是我们也有一个 sig 组，专门是一生一芯的 sig 学习小组，我们还有一个全栈的学习环境，叫 “石榴派 Shiliu Pi” ，我是去年参加过（首届RISCV中国峰会）演讲嘉宾，大家可以搜这个 “石榴派”，可以搜到我的演讲。我当时也是穿的会务的志愿者服装（白色的T恤）。这位一生一芯 sig 学习小组 的组长，是学信息安全的，他呢有一个特殊的爱好是什么呢？叫高山速降（骑山地车的），所以我们 SIGer 本身就是一个很有意思的这个话题。

也就是说，这次我们除了带来了三篇日记，红小鬼还有一个重要的特点是什么呢？就是要有实战经验。所以，这两个（大家看背景的大屏幕的两期期刊的封面）其实是我们带来的两个 SIG 组。一个是这个数独，它是一个独特的数独规则，当时我们初中的那个学生看到这个以后，她说：“哎呀，我熟悉数独，所以我可以来听一下。”，右边这个（海报）实际上，大家看是一个声学的RISCV解决方案，实际上，行业内是有解决方案的，那么我得到一个新的消息是什么呢？国内的，有一所医学院已经开始进行叫 “工医” 结合的（教学实践），就是有 90个学生，（今年）四月份刚批准的新专业，他们也会对这方面感兴趣。所以，我带来的 SIG 组，就是第2个礼物。

那么第三个礼物是什么呢？就是我自己。大家参加第四期（学习）的时候（B站发布的一生一芯第四期动员会），都会点个赞，我是置顶的那个帖子的“石榴派”，就是我，我应该是 “课代表”（帮同学整理学习笔记的）。这次来就是，真正地见到了我们的 “校长” （包云岗老师）很激动。所以，我这次来，把我自己作为礼物，送给包云岗老师。我想通过我们 SIGer 期刊，能够带来更多的 SIG 组，（带来）更多的兴趣。刚才我带来的（红小鬼），高中的那个AI社团的同学呢，他实际上是喜欢玩魔方，他说，可不可以为自己的魔方装一个自主设计的芯片。然后，刚才那个，说到我们一生一芯 SIG 组的班长，他有一篇文章写的什么呢？叫做《[君子不器](https://gitee.com/flame-ai/siger/blob/master/sig/ysyx/%E3%80%8A%E5%90%9B%E5%AD%90%E4%B8%8D%E5%99%A8%E3%80%8B%E2%80%9C%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF%E2%80%9D%20%E7%A7%91%E6%99%AE%E5%AD%A6%E4%B9%A0%E5%B0%8F%E7%BB%84%E7%BB%84%E9%95%BF%E5%AE%A3%E8%A8%80.md)》，也就我们不要拘泥于，只设计芯片，我们要把 RISC-V，把一生一芯，作为一个科普的入口。他的主题分享是什么呢？不要拘泥于某一个方向，把自己禁锢住，也就是说，成为一个全面的学习（者），不只是要全栈，还甚至于要 “栈溢出”。所以，我想我通过我们这个 SIGer 杂志呢，一定可以做到这个目标。

我毛遂自荐，肯定要有一些案例，我实际上是来自于 openEular 社区的第75个 sig 组。我们的 sig 组是做通用人工智能，民族棋非遗保护的。所以，我对 SIG 组的（建设）经验非常丰富，也就是我在 2021，我们 SIGer 杂志的创刊日期是 2021.1.1。也就是说三天前（2020.12.27）我是受邀参加国内的开源社区的，也是代表科普过来的。所以，我的这个 SIG 的运营经验一定可以胜任这个 “课代表” 的职责，甚至说，我们（SIGer）能培养出更多的课代表。每个 SIG组 都是（有）一个课代表。

（也是OE社区的开源操作系统的会议，陈海波老师主持的，也是这次会议，我来到相同的地点，中科院计算所，首次听到了南向，北向，SIG组这些热词，也是带着疑惑，中学生能投身到开源社区吗？才有了第75个SIG组，才有了树莓派的关注，RISCV的关注，都是源自社区，还记得RISCV的第一篇有分量的学习笔记的标题《[南征北战](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md)》，并在一年后，成功如愿和南向导师包云岗老师回合，真正实现了从北向南的全栈贯通！）

包云岗老师能不能满足我的这个愿望？如果能，来接受一下我们的礼物，好吗？大家给我们（SIGer）一些鼓励啊，谢谢！（主持人：感谢，袁老师的分享！）（然后就有了开头的那张留影纪念。:pray: 要不是疫情，真该带着学生来，孩子们至今还珍藏着，我一年多前，从开源社区带回去的中科院计算所的笔记本，上面就印着包云岗老师的签名，我想这就是可爱的红小鬼的样子啦，他们需要的只是前辈的榜样力量！一份精神的鼓舞啦！）

# [专题策划：造芯人才的核心技能，计算机、数学、物理](https://gitee.com/flame-ai/siger/blob/master/sig/xiaoyuan/%E9%80%A0%E8%8A%AF%E4%BA%BA%E6%89%8D%E7%9A%84%E6%A0%B8%E5%BF%83%E6%8A%80%E8%83%BD.md)

> 故事是这样发生的，作为 SIGer 少年芯的主编，行使记者职责，到访一个会议现场，一定是要极早制定采访线路，熟悉环境后，取得了会场志愿者的帮助后，我选择了一个便于起身行动的座位，稍微耽搁了一下，不偏不中的黄金位被一位 “同行” 抢先了。这里用 “同行” 是我们的共同身份 “孩子爸”。他是来为孩子拍照的，这么一个造芯新势力的发布会，能够出席，一定是孩子的荣光时刻，见我和会场志愿者的融洽，我回答了 “同行” 的提问，“全程有录像和回放的。”

由于隐私问题，不能刊载访谈全文，通过我的回答和提问，能够看到我国造芯人才培养的曙光。

（如果把 “造芯” 上升到国家战略，拥有系统观，具备全栈能力的未来领军人才的培养，才是所有努力的汇聚点，因为，我们的未来，只能由他们自己创造，他们才是未来的栋梁，才是我们未来美好生活的保证。大胆地，交给他们吧。SIGer 就是这么一群有为青年的合集！:pray: ）

## SIGer 提问学生

1. 本次 “造芯计划” 是通过那个渠道了解到的？
1. 在学校的时间和在家的时间是如何分配的，从加入 “造芯计划” 的学习开始。
1. 设定自己的学习目标，除了未来拥有自己的 “芯”，是否和自己的高考或学业有目标关联？有哪些规划
1. 学校也有科技社团，是否有其他社团经验？可以分享初中甚至小学的。“造芯计划” 的社团有哪些区别？
1. 社团老师，在 “造芯计划” 学习的过程中有哪些帮助？还是只跟 “造芯计划” 社区在线课程的助教老师有关。
1. 自己和同学都有立志处理器设计行业吗？对自己的未来规划和目标是怎样的？可以和3的高考一起回答。
1. SIGER是青少年科普，兴趣学习社区。能分享下自己的兴趣爱好吗？可以着重社团外的？当然可以强调社团经历。

## 我给家长分享 SIGer 和 我们的试验项目

学生接受采访前，除了了解一些学生的成长背景外，特地征询了家长对开源社区的了解程度，并借此机会对 SIGer 做了简要的介绍和宣传。

### 开源社区是 “理工男” 的热土。

其实我当时问您那个有关这开源社区的事情，（问题5）其实我跟您一样，我原来是学软件的，然后做系统编程，然后这个07还是08年，那时候开始，然后，在这个科技媒体，做了一段时间这个频道主编，然后，也算是媒体出身，然后从那之后，其实基本上就脱离技术岗了，但是当时的。其实每一个技术人，就像你说的理工男，心中都是有一种理想的，所以开源社区真的是天生的，这个理工男的热土。

做开源的人，就是他们，包括你看，今天 “造型计划” 的这些老师，包括整个课程，架构，非常细致，整个社区，都是典型的理工男思维，就很不容易去，就很 “不善于” 和人交流，但要跟机器交流是非常方便，其实我也有过，那么一段时间。后来做了媒体之后，包括后来，有机会做老师，现在带科技社团，然后，就越发的 “外向”，然后，就是。但是这个就是理工男的那种心里的那种理想，没有放弃。

### SIGer 燃起了我心中的教育理想

实际上从教育这个角度来说，我因为正好跟孩子接触更多了，包括我自己也有，所以说也是思考的特别多，然后也和很多家长在一块儿，然后，我之前介绍自己的身份，我利用了更多的业余时间，跟社区，做志愿者，做科普志愿者，然后也是接触了很多的学生，老师，包括家长，所以对教育有一个自己的心中的理想。

所以说，其实开源社区很早以前也知道，就像你说的就是主要是使用。然后，那个真正的开源有影响力，大家从这个web端 Linux的这个服务端的应用。这个用户端，其实很少人。都说 Linux 是技术人员的工具。所以我是从那个 OpenEuler，就是华为的那个 欧拉操作系统之后。

满打满算，其实 整个 SIGer 是我带学生开始做这个开源社区的一个成果。最开始实际上就是为了（科普开源），我等于说一年多前，我对开源也是蒙圈的，就是甚至连那个，你刚才说的一句 PR 贡献这件事儿，想都不用考虑。就是拿来主义，就是把外国的一些开源社区的成果拿来用。能用的好，就在国内，已经是开源界的一把了，就别说这个贡献这件事儿。现在一些大的公司，包括这个比如说阿里，华为，包括腾讯，他们就也开始在做这个Linux内核的贡献什么的，甚至于把它做一个，等于说行业的这种指标，来标榜，然后这成了一种就风尚，就变成最开始是叫软件定义硬件，然后现在叫开源定义软件。

我这一年多，就是说重燃心中的理想，所以说，我认为开源实际上是一个理想主义者的一个圈子，就包括你一看 “造芯计划” 的老师，他们在分享的时候，真的就是没有那些乱七八糟的事儿，所以我觉得他对咱们的胃口，这是我的一个体会。（然后当然我现在当老师，就是说的比较多一些，有点儿话痨了。）

### 开源文化的推广（科普）就是理想，对孩子来说，开源文化更有意义

就是我在提的问题当中，有特别重要的一点，就是说实际上。我在做 SIGer 的时候，实际上是希望让学生通过协作，共同来编辑一本杂志的方式,来体验这个PR和这个贡献和分享的这个过程，但没想到就是这种过程反而促成了 SIGer 的媒体化，现在青少年科普当中，还是有一席之地的，然后下个月我们会跟... Gitee，他们的源头社区叫 OSChina，开源中国，他们有一个线下杂志，我们会开专栏。他们都是讲的叫什么，开源治理，那个真是理想化的一些内容，但其实从目前来说，就是我潜意识中觉得这个方向是对的，就是对于孩子来说，其实开源文化是更有意义的，这也是我这个副刊的副刊题。

### 我们都是开源文化的 “践行者”

那我在给您提问题的时候，就是可能因为您的跟开源社区交集的比较少，所以说没有应我的那个问题。实际上我当时想问的是什么，就是现在 **喜欢学习** ， **学习的状态** ...

现在（造芯计划）的学习完全是社区化的设计，根本就不是，甚至于包括大学的课程都不是这样儿的，它完全就是一个社区化的，可以说，99%的这个时间都是在自己去搜索。实际上在社区里，为什么一上来就让你回答《提问的艺术》，实际上社区里最要命的就是这个问题，它是分时的，大家都是，比如尤其这个美国和中国时区，你不可能就像微信一样，没事儿就拉个微信群去问，这真的不是这样的，甚至你在那个想提个问题，恨不得一个月之后能有人回答，国内的开源社区老是吐槽人家，说是这个上游社区，就是外国的，回的特别慢，其实你真是要理解这个问题。

他这个提问实际上就是在这个社区当中，真的是很难的，有可能你提完问题，隔了好段时间就自己解决了。所以 “造芯计划” 的整个设计也是我特别认同的一个点，它不是把它做成一个 cpu 的科普，而是一个开源文化的科普。实际上这群人，他们本身就是一个开源文化的践行者，那种理想主义精神，实际上是非常打动我的。

### 开源社区的学习方式，高效的自主学习，及兴趣驱动。

所以我在提的问题当中，就是讲的是说，其实现在孩子们他的这种学习环境，我认为，随着这个开放，包括随着，比如说校园的壁垒的打破，包括一些，当然体制可能有点儿远，包括现在，您让孩子选择走国际体系本身，都是一个，我认为是突破。所以从这个学习上来说，其实真的是，就像您刚才提的就是孩子小的时候，学的那个 Arduino 什么之类，其实就是开源。开源本身就是一个国外的文化体系，它就更接轨。

所以说现在从学习的角度来说，就是自主学习，其实从某种意义上讲，它就是一个，开源社区学习的一个方式。

那这时候就是一个选择学什么的问题，就像您讲的，您最自豪的事儿，就是孩子他的自主学习能力和这个想学这件事儿，就已经做到了。其实很多家长现在还在焦虑，说孩子天天这个从后面拿鞭子赶着，前面引着他，他都不学习，在这个层面上，您已经是很成功了，所以刚才您前面的分享，其实已经有很多可以给孩子们借鉴的。所以我现在 SIGer 的这个实践当中，就是主要讲的，就是说，通过孩子自主的兴趣，然后通过比如说社区的一些，比如前人，或者说这个老师给一些（引导和支持）。

### SIGer 的试验项目是如何产生的？

今天我分享的两个项目，实际上就都是学生自己提的，比如像数独，包括那个听障，实际上都是有一个提点，那个数独，是我提了，那个听障，实际上是3月3号有一个国际爱耳日，也是学生做的一个跟 “造芯” 主题没有关系的，一个分享。后来我们就等于说，因为它是叫学习型社区，实际上有很多学生，包括我自己也参与，实际上我跟孩子们也学到很多东西。然后就发现了，这个 “造芯” 的一个应用案例，所以才有的。所以，也就是说，现在一定他要有一个动因在里面，再加上您最自豪的这个孩子的这个学习能力，所以这样一结合，孩子就一点儿问题都没有，所以这是我，我本来想问出来的，我就自己自圆其说去解释一下，这个事情。要不咱们一定是有共同语言的，所以也是也算是这个。

### SIGer 的分时语音采访方式

就是跟您多说一些话。现在也是周末，因为带孩子也比较忙，可以不用回，回头就是等孩子有空，语音回一下。我觉得其实这种分享还是蛮重要的，有时候用手，就像您说的，因为跟机器打交道，有时候码字的时候，他可能会过滤掉一些，情感上的东西，所以我很愿意用这种语音的方式去交流，这样话就相当于我们面对面一样，分时的方式。很高兴认识您，我觉得孩子非常棒，然后很期待你们的分享，等我这个正式的采访提纲出来之后，我会把小样发给您看一下，然后没问题，我会再PR到主仓里。

---

# [《开源观止》第四期](https://oscimg.oschina.net/public_shard/opensource-guanzhi-20220915.pdf)

- OSCHINA www.oschina.net 征集合作（OPEN SOURCE GUAN ZHI)
- [《开源观止》第四期](https://oscimg.oschina.net/public_shard/opensource-guanzhi-20220915.pdf) contents 目录
- 《[SIGer 少年芯](https://gitee.com/flame-ai/siger/blob/master/RISC-V/SIGer%20%E5%B0%91%E5%B9%B4%E8%8A%AF%EF%BC%8C%E4%BB%8E%E5%8C%97%E5%90%91%E5%8D%97%E7%9A%84%E5%BC%80%E6%BA%90%E6%96%87%E5%8C%96%E4%B9%8B%E6%97%85.md)》SIGer P59 - P62 

<p><img width="706px" src="https://foruda.gitee.com/images/1663241206183918576/3be539aa_5631341.jpeg" title="281开源观止4.jpg"></p>

OSCHINA www.oschina.net
### 征集合作（OPEN SOURCE GUAN ZHI)

《开源观止》是 OSCHINA 于 2022年6月推出的一本关于开源的精选集，旨在集中化地呈现一些关于 FLOSS 的信息与观点。我们有一个美好的愿景：开源，观止矣。而这需要更多开源人、开发者参与进来共建。

因此，OSCHINA 编辑部特向大家征稿，征稿类型如下：

（1）开源实务经验：合规、安全、运营等与开源有关的实践过程、方法经验，参考总第1期文章《企业开源运营之道：目标、用户、指标》；

（2）开源创业历程：体现创业者的奋斗历程、成长经过，阐述创业项目的运营情况、未来前景，分享创业感悟、创业经验，参考总第1期文章《 SphereEx 张亮：创业是为了让开源更好》；

（3）开源观点论述，要求观点明确、新颖，论述严谨，参考总第1期文章《开源纯粹主义：每一颗螺丝钉都必须是自由的！》；

（4）热点事件解读：针对当前开源领域的热点事件进行解读，挖掘事件背后原因，引发读者思考；

（5）开源历史故事：与开源有关的历史故事，以史鉴今，参考总第1期《专题|古典主义 Debian》栏目；

（6） 开源报告、调查数据；

（7）开源趣味小故事、冷知识，参考散落各处的《开源识多D》；

（8）……

如欲投稿、合作，请联系：

肖滢 183 7099 8278（微信同号）

![输入图片说明](https://foruda.gitee.com/images/1663242974327435369/7e052206_5631341.jpeg "opensource-guanzhi-20220915-2.jpg")

### contents 目录

- 01 封面

- 02 征稿

- 03 卷首语

- 05 聚焦

  - 开源的尽头是商业？

- 13 专题

  - 开源，交易成本与商业化
  - 史上最全！全球22种开源商业收入模式
  - T-IO: 被神指引的开源商业之路

- 25 创业小辑

  - 35 岁 ”危机“
  - 老板由 ”技术男“ 到 ”销售员“ 的转变

- 27 溯源

  - 一个技术人 ”误入歧途“，做了个向量数据库新物种
  - ”工具人“ 赵生宇：清北本硕，为开源从阿里辞职去同济读博

- 38 星推荐

  - Thief | Apache Kvrocks | ShareX | Astro

- 41 众说

  - 如何从 0 开始搭建开发者如何从零开始
  - 参与 Apache 顶级开源项目？社区运营策略

- 53 新鲜事

- 59 SIGer

- 63 Gitee

![输入图片说明](https://foruda.gitee.com/images/1663243347770010425/8e3914b3_5631341.jpeg "opensource-guanzhi-20220915-4.jpg")

### [SIGer 全文](https://gitee.com/flame-ai/siger/blob/master/RISC-V/SIGer%20%E5%B0%91%E5%B9%B4%E8%8A%AF%EF%BC%8C%E4%BB%8E%E5%8C%97%E5%90%91%E5%8D%97%E7%9A%84%E5%BC%80%E6%BA%90%E6%96%87%E5%8C%96%E4%B9%8B%E6%97%85.md) 

页面顺序：1230 (p60,p61,p62,p59)，阅读顺序：0123（59，60，61，62）

![输入图片说明](https://foruda.gitee.com/images/1663243426989929039/393a3e20_5631341.jpeg "开源观止99-12.jpg")

（左：p60，右：p61）

![输入图片说明](https://foruda.gitee.com/images/1663243450203818107/d450fc52_5631341.jpeg "开源观止99-03.jpg")

（左：p62，右：p59）

