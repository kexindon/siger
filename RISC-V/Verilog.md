- [天璇 Merak](https://space.bilibili.com/1563198865)[Verilog入門教學]与背景知识
- [YADAN](docs.yadanboard.com) [教程 阶段二](https://verimake.com/d/144)：在 YADAN Board 上入门 Verilog [1](https://verimake.com/d/80-yadan-board-verilog-1),[2](https://verimake.com/d/115-yadan-board-verilog-2-pwm),[3](https://verimake.com/d/146-yadan-board-verilog-3-ip-core-adc),[4](https://verimake.com/d/149-yadan-board-verilog-4-uart),5

<p><img width="706px" src="https://foruda.gitee.com/images/1668041146200851081/be4ed6f3_5631341.jpeg" title="412天璇副本.jpg"></p>

> 大大的眼睛，嗲嗲的声线，绝对是 Verilog 讲师的清流，这也是鸭蛋主播圈粉的秘籍啦。尽管本篇收录了两则虚拟主播的笔记，但这不是本期专题的核心，真正的核心是去年就收藏的 Verilog 入门教程，终于配上了合适的开发板啦 —— 就是鸭蛋。这是我完成实验课之后，第一个跃跃欲试的挑战目标。结果是喜人的，有了鸭蛋和TD的工程经验，回头再重温 天璇妹妹的课程，是真的弄明白了。这里抛砖引玉，请同学们沿着这个实验路径，将天璇Verilog入门教程的实验课做个移植吧，改改LED灯的符号，DIY一些开关，应该不难。

# [天璇 Merak](https://space.bilibili.com/1563198865)[Verilog入門教學]与背景知识

![输入图片说明](https://images.gitee.com/uploads/images/2021/0412/035603_6805a229_5631341.png "屏幕截图.png")

- [Verilog入門教學] 背景知識#1 進位制與進位轉換
  https://www.bilibili.com/video/BV1Cy4y1n7gr
- [Verilog入門教學] 背景知識#2 位元、二補數與加減法
  https://www.bilibili.com/video/BV1Br4y1P79S
- [Verilog入門教學] 背景知識#3 布林代數與邏輯閘
  https://www.bilibili.com/video/BV1m5411J7V7

![输入图片说明](https://images.gitee.com/uploads/images/2021/0412/035447_98eeeddb_5631341.png "屏幕截图.png")

- [Verilog入門教學] 本篇#0 硬體描述語言簡介
  https://www.bilibili.com/video/BV1My4y1H76C
- [Verilog入門教學] 本篇#1 verilog基礎語法
  https://www.bilibili.com/video/BV1Sh411C7EQ
- [Verilog入門教學] 本篇#2 電路驗證工具-QuartusII與FPGA
  https://www.bilibili.com/video/BV16Z4y1P7Ln
- [Verilog入門教學] 本篇#3 模組調用、匯流排與八對一多工器
  https://www.bilibili.com/video/BV1r64y1D7y6

![QuartusII](https://images.gitee.com/uploads/images/2021/0412/040151_ac2b10c3_5631341.png "屏幕截图.png")

![FPGA](https://images.gitee.com/uploads/images/2021/0412/040058_5ebcdcd8_5631341.png "屏幕截图.png")

### Verilog入門教學 本篇

- 2-7（零）硬體描述語言簡介 
https://www.bilibili.com/video/BV1My4y1H76C
- 2-8（一）verilog基礎語法  
https://www.bilibili.com/video/BV1Sh411C7EQ
- 3-7（二）電路驗證工具-QuartusII與FPGA 
https://www.bilibili.com/video/BV16Z4y1P7Ln
- 3-18（三）模組調用、匯流排與八對一多工器 
https://www.bilibili.com/video/BV1r64y1D7y6
- 4-25（四）七段顯示器控制電路 
https://www.bilibili.com/video/BV1XK4y1R7kK
- 5-7（五）四位元漣波進位加法器 4-bit ripple-carry adder 
https://www.bilibili.com/video/BV1p541137BQ
- 5-17（六）四位元超前進位加法器 4-bit carry lookahead adder 
https://www.bilibili.com/video/BV1W64y1y7eJ
- 5-29（七）四位元加減法器與溢位偵測 
https://www.bilibili.com/video/BV1nq4y1j7P3
- 6-12（八）四位元乘法器 
https://www.bilibili.com/video/BV1544y1B721
- 6-26（九）解碼器 Decoder 
https://www.bilibili.com/video/BV1Fg41137WC
- 7-10（十）優先序編碼器 Priority encoder 
https://www.bilibili.com/video/BV1fq4y1s7iX
- 8-7（十一）累加器 accumulator 
https://www.bilibili.com/video/BV1BU4y1J7T5
- 9-12（十二）除頻器 frequency divider

### 其他

- 5-25 Live2D Model Test 
https://www.bilibili.com/video/BV1Sb4y1o758
- 8-12 休假公告
https://www.bilibili.com/video/BV1jL411J7jA

### 背景知识

- 2-7 07:32（一）進位制與進位轉換 
https://www.bilibili.com/video/BV1Cy4y1n7gr
- 2-7 06:36（二）位元、二補數與加減法
https://www.bilibili.com/video/BV1Br4y1P79S
- 2-10 6:32（三）布林代數與邏輯閘
https://www.bilibili.com/video/BV1m5411J7V7
- 4-12 8:58（四）卡諾圖 K-map
https://www.bilibili.com/video/BV1ci4y1A7Zs
- 7-24 7:50（五）循序邏輯電路、latch與flip-flop
https://www.bilibili.com/video/BV1154y1E7iw

### 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I3IA6B)】[Verilog入門教學]与背景知识

- [RV4Kids/Shiliu PI, Shiliu Silicon, Shiliu Si, 石榴核.md](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md)

  [收录！这将会继续完善，配合上接口，发展成为一个石榴派的特色实验课。](https://gitee.com/RV4Kids/RVWeekly/issues/I3IA6B#note_5613806_link)

- [好久没有跟新了，只能把天璇妹妹作为围棋爱好者收录了，学习者的角色在虚拟世界也是曾经一瞥！](https://gitee.com/RV4Kids/RVWeekly/issues/I3IA6B#note_7831582_link) :pray: 祝好！ :sunflower: 

- 小K直播姬 https://yunboai.com/live

  > 2022-3-29 · 小K直播姬 - 首款无穿戴3D虚拟直播产品，视频动捕黑科技，自研二次元捏人系统，免费使用，人人皆可3D虚拟直播！

    [<img width="169px" src="https://images.gitee.com/uploads/images/2022/0404/095046_c223901f_5631341.png" title="Screenshot 2022-04-04 at 09-50-13 零成本3D虚拟形象！网课直播视频都能用的黑科技软件【收藏不亏16】_哔哩哔哩_bilibili.png">](https://www.bilibili.com/video/BV1fm4y1d7VM)

### [使用 YADAN 开发板验证第一个 Verlog 作业](https://gitee.com/RV4Kids/yadan-board/issues/I5X5XL#note_14245599_link)

[Verilog入門教學][与背景知识](https://gitee.com/RV4Kids/RVWeekly/issues/I3IA6B)

- [Verilog入門教學 的视频](https://gitee.com/RV4Kids/RVWeekly/issues/I3IA6B#note_6205293_link)

  - 2-7（零）硬體描述語言簡介 
https://www.bilibili.com/video/BV1My4y1H76C
  - 2-8（一）verilog基礎語法  
https://www.bilibili.com/video/BV1Sh411C7EQ
  - 3-7（二）電路驗證工具-QuartusII與FPGA 
https://www.bilibili.com/video/BV16Z4y1P7Ln
  - 3-18（三）模組調用、匯流排與八對一多工器 
https://www.bilibili.com/video/BV1r64y1D7y6
  - 4-25（四）七段顯示器控制電路 
https://www.bilibili.com/video/BV1XK4y1R7kK
  - 5-7（五）四位元漣波進位加法器 4-bit ripple-carry adder 
https://www.bilibili.com/video/BV1p541137BQ
  - 5-17（六）四位元超前進位加法器 4-bit carry lookahead adder 
https://www.bilibili.com/video/BV1W64y1y7eJ
  - 5-29（七）四位元加減法器與溢位偵測 
https://www.bilibili.com/video/BV1nq4y1j7P3
  - 6-12（八）四位元乘法器 
https://www.bilibili.com/video/BV1544y1B721
  - 6-26（九）解碼器 Decoder 
https://www.bilibili.com/video/BV1Fg41137WC
  - 7-10（十）優先序編碼器 Priority encoder 
https://www.bilibili.com/video/BV1fq4y1s7iX
  - 8-7（十一）累加器 accumulator 
https://www.bilibili.com/video/BV1BU4y1J7T5
  - 9-12（十二）除頻器 frequency divider

[Verilog入門教學] 本篇1: verilog基礎語法
https://www.bilibili.com/video/BV1Sh411C7EQ

![输入图片说明](https://foruda.gitee.com/images/1667924647581935078/bdee1578_5631341.png "屏幕截图")


```
module HW1(a, b, c, d, s);
    input a, b, c, d;
    output s;
    wire b_, g0, g1;

    not n1(b_,b);
    xor x1(g0,a,b);
    or o1(g1,b_,c,d);
    and a1(s,g0,g1);

endmodule
```

[Verilog入門教學] 本篇2: 電路驗證工具-QuartusII與FPGA
https://www.bilibili.com/video/BV16Z4y1P7Ln

有详细的 QII 的工具使用说明，和 TD 的工作流程是一致的。结尾的FPGA实验也清晰明了，可以移植相应的教程。

![输入图片说明](https://foruda.gitee.com/images/1667924544983727232/043892fc_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1667925658300328474/7982bce0_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1667925681930474182/9f72ab64_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1667925699032994540/1b6e6679_5631341.png "屏幕截图")


```
BIT-1004 : Generate bits file basic01.bit.
>>
```

![输入图片说明](https://foruda.gitee.com/images/1667925801968141533/cdc3a8d1_5631341.png "屏幕截图")

- 结果是，两个BUTTON 单独按，LED 亮，灭。
- 一起按下，LED 灭。

说明 VERILOG 逻辑起作用了。

### 天璇妹妹大眼睛

[![输入图片说明](https://images.gitee.com/uploads/images/2022/0404/094348_5cbff3bb_5631341.png "屏幕截图.png")](https://www.bilibili.com/video/BV1Sb4y1o758)![输入图片说明](https://images.gitee.com/uploads/images/2022/0404/094528_7a75f6d1_5631341.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2022/0404/094741_c5963ea8_5631341.png "Screenshot 2022-04-04 at 09-46-25 Live2D Model Test_哔哩哔哩_bilibili.png")

原来，大眼睛，是天璇妹妹啊！

# [I5YEZB](https://gitee.com/RV4Kids/yadan-board/issues/I5YEZB): [YADAN Project | 介绍](https://gitee.com/RV4Kids/yadan-board/issues/I5X5XL#note_14238433_link)

- [第一个实机实验](https://gitee.com/RV4Kids/yadan-board/issues/I5YEZB#note_14034321_link)
- [在 YADAN Board 上入门 Verilog - (2) 用 PWM 实现呼吸灯，来学习仿真和层次化设计](https://gitee.com/RV4Kids/yadan-board/issues/I5YEZB#note_14186999_link)
- [在 YADAN Board 上入门 Verilog - (3) 用旋钮调节灯的亮度，来学习调用 IP Core 访问内建 ADC](https://gitee.com/RV4Kids/yadan-board/issues/I5YEZB#note_14189314_link)
- [在 YADAN Board 上入门 Verilog - (4) 使用 UART 串口传输数据给电脑](https://gitee.com/RV4Kids/yadan-board/issues/I5YEZB#note_14189596_link)

  - [Python - ModuleNotFoundError No module named `serial`](https://gitee.com/RV4Kids/yadan-board/issues/I5YEZB#note_14189693_link)
  - [在使用pip install opencv-python 安装 cv2的过程中出现了installing build dependencies错误。](https://gitee.com/RV4Kids/yadan-board/issues/I5YEZB#note_14190298_link)

    > 错误原理：这个指令安装的pip会出现CV2版本与python安装的版本，不匹配导致，你安装的opencv不成功。  
      解决方法：使用 python -m pip install --upgrade pip 更新pip后再使用pip install opencv-python 安装。
  - [`module UART_RX (input CLK_24MHz, input RX, output reg LED);`](https://gitee.com/RV4Kids/yadan-board/issues/I5YEZB#note_14190271_link)

- [https://gitee.com/RV4Kids/yadan-board/blob/master/串口调试助手.exe](https://gitee.com/RV4Kids/yadan-board/issues/I5YEZB#note_14190330_link)