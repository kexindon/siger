<p><img width="706px" src="https://foruda.gitee.com/images/1660116913448107145/225科技志愿者2021副本.jpeg"></p>

先来为说明下封面的4张照片（上到下，中间两张分左右）：

1. 上图是我的石榴派展台接待志愿者的到访，开幕的头两天，各种熟悉和流程工作，是不可能分心的，他们都是上海科技大学的同学，也同样地对新技术和业态保有好奇心。我高兴地为他们解答了石榴派的内容，体验了一把我带来的小游戏，和志愿者在一起，不分你我，我骄傲。（封面的大背景就是我挂在胸前的名牌，大大的V，和白色的志愿者T恤。）

2. 中左是我此行的目的啦，身为 [POSTER #1 号演讲人](https://www.bilibili.com/video/BV1qV411H7j9)，我自信开了个好头。RV4kids 获得了所有观众的好评，我代表的青少年力量附体了，我的心也更年轻了。我用少年心来形容。这不2022的Solgan顺理成章地升级成 RV少年 啦。

3. 中右是 RVSC 2021 的封面主题啦。之后 SIGer 期刊（RV少年 - SIGer 学习小组）第3期，就使用的它。

4. 下图是我21日下午抵达 RVSC 2021 会场主场上海科技大学校门的场景，看着白色T恤的志愿者，就心生好感，合影留念后，也是我向主办方领取参会T恤特地申请的志愿者白的原因。

（下面是这4张照片的原图，冠以科技志愿者之名，是一生一芯的包云岗老师，前两天刚转了CCA 60周年大会，一则科技志愿者的介绍，整个中国计算机学会，之前挂靠在中科院计算所，现在的秘书处所在，包括学会通讯的主编，都是以志愿者身份投入工作的。所以，我们都是科技志愿者。）

![输入图片说明](https://foruda.gitee.com/images/1660085466240484341/img_20210622_165443.jpeg "在这里输入图片标题")
![输入图片说明](https://foruda.gitee.com/images/1660085399848674737/img_20210622_131059.jpeg "在这里输入图片标题")
![输入图片说明](https://foruda.gitee.com/images/1660086316779731692/img_20210622_091204.jpeg "在这里输入图片标题")
![输入图片说明](https://foruda.gitee.com/images/1660085453401693439/img_20210621_140907.jpeg "在这里输入图片标题")

# [RISC-V 2021 中国峰会花絮（定稿）：让我们重温这一周的紧张、兴奋、期待](https://www.bilibili.com/video/BV1Hh411r7jR)

https://www.bilibili.com/video/BV1Hh411r7jR

<p><img width="49%" src="https://foruda.gitee.com/images/1660005283770710836/屏幕截图.png" title="[0:25]芯来.png"> <img width="49%" src="https://foruda.gitee.com/images/1660005268018907252/屏幕截图.png" title="[1:06]RV4Kids.png"></p>

作为 [RISCV国际基金会](https://space.bilibili.com/1121469705) B站置顶视频，是志愿者对自己的犒赏，我出境两次，下面分享下花絮。

<p><img width="49%" src="https://foruda.gitee.com/images/1660085413349002650/img_20210622_170931.jpeg"> <img width="49%" src="https://foruda.gitee.com/images/1660085430043843953/img_20210622_170943.jpeg"></p>

0:25 我到访芯来展台和徐来老师自拍合影的瞬间，被会务组抓拍下来了。之后得到的礼物，RVSC 2021回顾的其中一项，蜂鸟未来的专题的封面就是它了。我特别喜欢芯来的寄语，这份好感是对芯来团队独特的气质的欣赏。2022 SIGer 还做过一期 吉他的专题，就取材于徐来老师的朋友圈分享。满满地回忆杀，能坚持青年时代的爱好，相当不容易。

<p><img width="49%" src="https://foruda.gitee.com/images/1660086277441897432/img_20210622_081432.jpeg"> <img width="49%" src="https://foruda.gitee.com/images/1660086305372606436/img_20210622_081452.jpeg"></p>

这就是科技志愿者忙碌的日常，大会离不开这群可敬的同学们。为他们记录下忙碌的身影。

# [RISCV国际基金会](https://space.bilibili.com/1121469705) B站视频排行

在回顾 RVSC 2021 的过程中，行业热点是访问最多的，为同学们转发下[最多播放](https://space.bilibili.com/1121469705/video)，方便同学们回顾，也为2022导视参考。  
第1屏[香山](https://www.bilibili.com/video/BV1kU4y1N738)，[NEMU No.2](https://www.bilibili.com/video/BV1Zb4y1k7RJ)，[南大教材 No.5](https://www.bilibili.com/video/BV1Rg411j79t)，第2屏的[宇航级芯片](https://www.bilibili.com/video/BV1mw411d7sq)是热点，结尾是[教育生态](https://www.bilibili.com/video/BV1hq4y1H7HK)，[石榴派](https://www.bilibili.com/video/BV1qV411H7j9)在第3屏。都值得期待...

| # | 时长 | 标题 | 点击 | 发布 |
|---|---|---|---|---|
| 1. | 12:59 | <a href="//www.bilibili.com/video/BV1SU4y1n7we" target="_blank" title="张雨昕 - 基于RISCV指令扩展的缓冲区溢出漏洞预防机制 - 第一届 RISC-V 中国峰会">张雨昕 - 基于RISCV指令扩展的缓冲区溢出漏洞预防机制 - 第一届 RISC-V 中国峰会</a> | 4.3万 | 2021-7-20
| 2. | 19:58 | <a href="//www.bilibili.com/video/BV1Zb4y1k7RJ" target="_blank" title="【余子濠】NEMU：一个效率接近QEMU的高性能解释器 - 第一届 RISC-V 中国峰会">【余子濠】NEMU：一个效率接近QEMU的高性能解释器 - 第一届 RISC-V 中国峰会</a> | 1.1万 | 2021-7-15
| 3. | 15:23 | <a href="//www.bilibili.com/video/BV1NV411s71b" target="_blank" title="周意可 - Pipe Mimic RVWMO内存一致性模型验证工具 - 第一届 RISC-V 中国峰会">周意可 - Pipe Mimic RVWMO内存一致性模型验证工具 - 第一届 RISC-V 中国峰会</a> | 9084 | 2021-7-16
| 4. | 11:31 | <a href="//www.bilibili.com/video/BV13V411s7Jo" target="_blank" title="张紫飞 - 香山处理器MMU的设计与实现 - 第一届 RISC-V 中国峰会">张紫飞 - 香山处理器MMU的设计与实现 - 第一届 RISC-V 中国峰会</a> | 4392 | 2021-7-17
| 5. | 19:11 | <a href="//www.bilibili.com/video/BV1Rg411j79t" target="_blank" title="袁春风@南京大学 - 基于RISC-V架构的系统类课程教学现状与思考 - 第一届 RISC-V 中国峰会">袁春风@南京大学 - 基于RISC-V架构的系统类课程教学现状与思考 - 第一届 RISC-V 中国峰会</a> | 3768 | 2021-7-20
| 6. | 22:37 | <a href="//www.bilibili.com/video/BV1kU4y1N738" target="_blank" title="RISC-V Summit 2021 - 包云岗 - 香山：开源高性能RISC-V处理器">RISC-V Summit 2021 - 包云岗 - 香山：开源高性能RISC-V处理器</a> | 3245 | 2021-12-7
| 7. | 17:57 | <a href="//www.bilibili.com/video/BV19X4y1w7EB" target="_blank" title="【包云岗】香山：开源高性能RISC-V处理器 - 第一届 RISC-V 中国峰会">【包云岗】香山：开源高性能RISC-V处理器 - 第一届 RISC-V 中国峰会</a> | 2717 | 2021-7-15
| 8. | 01:23:04 | <a href="//www.bilibili.com/video/BV1uP4y1j7m9" target="_blank" title="金枪鱼之夜：从 RTL 到 SoC - 主讲人：刘玖阳（Sequencer）">金枪鱼之夜：从 RTL 到 SoC - 主讲人：刘玖阳（Sequencer）</a> | 2667 | 2021-11-5
| 9. | 23:09 | <a href="//www.bilibili.com/video/BV1iB4y1T7YK" target="_blank" title="倪光南 - 中国 RISC-V 发展 -第一届 RISC-V 中国峰会 - 2021年6月22日">倪光南 - 中国 RISC-V 发展 -第一届 RISC-V 中国峰会 - 2021年6月22日</a> | 2057 | 2021-7-11
| 10. | 10:21 | <a href="//www.bilibili.com/video/BV1mP4y147iV" target="_blank" title="基于RISC-V架构的新一代智能存储：开放，高效的存算一体化芯片 - 吴子宁、周杰- 第一届 RISC-V 中国峰会">基于RISC-V架构的新一代智能存储：开放，高效的存算一体化芯片 - 吴子宁、周杰- 第一届 RISC-V 中国峰会</a> | 1459 | 2021-7-20
| 11. | 18:13 | <a href="//www.bilibili.com/video/BV16b4y167ek" target="_blank" title="邸志雄@西南交大 - RV-SoC Design Methodology：国内首个基于商业级 RISC-V 处理器的 MOOC 课程">邸志雄@西南交大 - RV-SoC Design Methodology：国内首个基于商业级 RISC-V 处理器的 MOOC 课程</a> | 1411 | 2021-7-20
| 12. | 08:48 | <a href="//www.bilibili.com/video/BV1144y127Xt" target="_blank" title="徐金焱@浙江大学 - 搭建最小化嵌入式RISC-V计算机系统 - 第一届 RISC-V 中国峰会">徐金焱</a><a href="/zju"></a><a href="/zju">@浙江大学 </a>  - 搭建最小化嵌入式RISC-V计算机系统 - 第一届 RISC-V 中国峰会 | 1382 | 2021-7-20
| 13. | 22:58 | <a href="//www.bilibili.com/video/BV1do4y1X7yJ" target="_blank" title="【赖晓铮】PyChip--基于Python的“全栈硬件设计与验证框架 - 第一届 RISC-V 中国峰会">【赖晓铮】PyChip--基于Python的“全栈硬件设计与验证框架 - 第一届 RISC-V 中国峰会</a> | 1380 | 2021-7-16
| 14. | 16:47 | <a href="//www.bilibili.com/video/BV1444y127SJ" target="_blank" title="余红斌 - 天枢：高性能乱序RISC-V处理器架构设计 - 第一届 RISC-V 中国峰会">余红斌 - 天枢：高性能乱序RISC-V处理器架构设计 - 第一届 RISC-V 中国峰会</a> | 1182 | 2021-7-17
| 15. | 01:19 | <a href="//www.bilibili.com/video/BV13f4y1j7bz" target="_blank" title="POSTER - 基于 RISC-V 设计一门开发操作系统的入门课程 - 汪辰 - 第一届 RISC-V 中国峰会">POSTER - 基于 RISC-V 设计一门开发操作系统的入门课程 - 汪辰 - 第一届 RISC-V 中国峰会</a> | 969 | 2021-7-17
| 16. | 12:44 | <a href="//www.bilibili.com/video/BV1s44y127cW" target="_blank" title="张林隽 - 香山处理器非阻塞DCache的设计与实现 - 第一届 RISC-V 中国峰会">张林隽 - 香山处理器非阻塞DCache的设计与实现 - 第一届 RISC-V 中国峰会</a> | 960 | 2021-7-16
| 17. | 21:24 | <a href="//www.bilibili.com/video/BV1eb4y167zY" target="_blank" title="常瑞@浙江大学 - 面向信息安全专业的RISC-V软硬件贯通课程教学实践 - 第一届 RISC-V 中国峰会">常瑞</a><a href="/zju"></a><a href="/zju">@浙江大学 </a>  - 面向信息安全专业的RISC-V软硬件贯通课程教学实践 - 第一届 RISC-V 中国峰会 | 889 | 2021-7-20
| 18. | 09:18 | <a href="//www.bilibili.com/video/BV1Y64y1X7Pw" target="_blank" title="王华强 - 香山处理器访存流水线的设计与实现 - 第一届 RISC-V 中国峰会">王华强 - 香山处理器访存流水线的设计与实现 - 第一届 RISC-V 中国峰会</a> | 885 | 2021-7-16
| 19. | 01:31 | <a href="//www.bilibili.com/video/BV1sP4y147Ty" target="_blank" title="大会主席包云岗致辞 - 第一届 RISC-V 中国峰会">大会主席包云岗致辞 - 第一届 RISC-V 中国峰会</a> | 805 | 2021-7-22
| 20. | 10:54 | <a href="//www.bilibili.com/video/BV1vg411M76r" target="_blank" title="Tengine：加速RISC-V的AI部署和落地 - 黄明飞 @ OPEN AI LAB - 第一届 RISC-V 中国峰会">Tengine：加速RISC-V的AI部署和落地 - 黄明飞 @ OPEN AI LAB - 第一届 RISC-V 中国峰会</a> | 799 | 2021-7-20
| 21. | 17:24 | <a href="//www.bilibili.com/video/BV1Ug411T7UQ" target="_blank" title="勾凌睿 - 香山处理器分支预测部件的设计与实现 - 第一届 RISC-V 中国峰会">勾凌睿 - 香山处理器分支预测部件的设计与实现 - 第一届 RISC-V 中国峰会</a> | 772 | 2021-7-16
| 22. | 10:22 | <a href="//www.bilibili.com/video/BV19q4y1W7Jh" target="_blank" title="YADAN An FPGA based Arduino Board for RISC-V Education - 张志鹏 - 第一届 RISC-V 中国峰会">YADAN An FPGA based Arduino Board for RISC-V Education - 张志鹏 - 第一届 RISC-V 中国峰会</a> | 761 | 2021-7-20
| 23. | 12:04 | <a href="//www.bilibili.com/video/BV1YP4y147L8" target="_blank" title="熊谱翔@RT-Thread - RV64上的微内核操作系统rt-smart  - 第一届 RISC-V 中国峰会">熊谱翔</a><a href="/rt-thread"></a><a href="/stop19">@小师弟 </a>  - RV64上的微内核操作系统rt-smart  - 第一届 RISC-V 中国峰会 | 695 | 2021-7-19
| 24. | 11:51 | <a href="//www.bilibili.com/video/BV1eg411M7uJ" target="_blank" title="程斌@阿里巴巴 - 阿里云基础软件 C/C++ 编译器工作现状和展望 - 第一届 RISC-V 中国峰会">程斌</a><a href="/lb1984"></a><a href="/lb1984">@阿里巴巴 </a>  - 阿里云基础软件 C/C++ 编译器工作现状和展望 - 第一届 RISC-V 中国峰会 | 694 | 2021-7-20
| 25. | 19:30 | <a href="//www.bilibili.com/video/BV19h41167iP" target="_blank" title="【解壁伟】开源EDA探索与实践 - 第一届 RISC-V 中国峰会">【解壁伟】开源EDA探索与实践 - 第一届 RISC-V 中国峰会</a> | 683 | 2021-7-16
| 26. | 10:11 | <a href="//www.bilibili.com/video/BV1y54y1n74q" target="_blank" title="【余子濠】LightSSS：基于内存的轻量级仿真快照 - 第一届 RISC-V 中国峰会">【余子濠】LightSSS：基于内存的轻量级仿真快照 - 第一届 RISC-V 中国峰会</a> | 666 | 2021-7-15
| 27. | 09:56 | <a href="//www.bilibili.com/video/BV1eb4y167cE" target="_blank" title="周耀阳@计算所 - Checkpoint + Sampling：10小时内估算出RISC-V CPU的SPEC分数 - 第一届 RISC-V 中国峰会">周耀阳@计算所 - Checkpoint + Sampling：10小时内估算出RISC-V CPU的SPEC分数 - 第一届 RISC-V 中国峰会</a> | 663 | 2021-7-19
| 28. | 17:22 | <a href="//www.bilibili.com/video/BV11W4y1r7Sr" target="_blank" title="RISC-V Mentorship Mentor Interview : MLIR Convolution Vectorization">RISC-V Mentorship Mentor Interview : MLIR Convolution Vectorization</a> | 649 | 6-8
| 29. | 11:07 | <a href="//www.bilibili.com/video/BV1Q44y1275f" target="_blank" title="夏虞斌@上海交大 - 蓬莱：RISC-V平台的TEE与安全方案 - 第一届 RISC-V 中国峰会">夏虞斌@上海交大 - 蓬莱：RISC-V平台的TEE与安全方案 - 第一届 RISC-V 中国峰会</a> | 645 | 2021-7-19
| 30. | 01:39 | <a href="//www.bilibili.com/video/BV19V411W7zb" target="_blank" title="POSTER - 使用gem5进行RISC-V NPU建模和架构探索 - 欧阳鑫 - 第一届 RISC-V 中国峰会">POSTER - 使用gem5进行RISC-V NPU建模和架构探索 - 欧阳鑫 - 第一届 RISC-V 中国峰会</a> | 643 | 2021-7-17
| 31. | 15:03 | [郑云龙- 宇航级高可靠嵌入式RISC-V处理器的研究与进展 - 第一届 RISC-V 中国峰会](https://www.bilibili.com/video/BV1mw411d7sq) | 638 | 2021-7-17 |
| 32. | 19:25 | [蔺嘉炜 - 香山处理器后端流水线的设计与实现 - 第一届 RISC-V 中国峰会](https://www.bilibili.com/video/BV1Fb4y1C7rM) | 623 | 2021-7-16 |
| 33. | 21:54 | [詹荣开 - NeuralScale：基于RISC-V指令集的高性能可编程神经网络处理器架构 - 第一届 RISC-V 中国峰会](https://www.bilibili.com/video/BV1cf4y1j7Wq) | 618 | 2021-7-17 |
| 34. | 01:44 | [RISC-V 2021 中国峰会花絮（定稿）：让我们重温这一周的紧张、兴奋、期待](https://www.bilibili.com/video/BV1Hh411r7jR?spm_id_from=333.999.0.0) | 616 | 2021-7-20 |
| 56. | 55:18 | [峰会圆桌讨论（教育方向） - 第一届 RISC-V 中国峰会](https://www.bilibili.com/video/BV1hq4y1H7HK) | 307 | 2021-7-20 |
| 71. | 01:54 | [POSTER - 石榴派 - 袁德俊 - 第一届 RISC-V 中国峰会](https://www.bilibili.com/video/BV1qV411H7j9) | 238 | 2021-7-1 |
