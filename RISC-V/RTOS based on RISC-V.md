- [IC技术圈期刊 2020年 第08期](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484243&amp;idx=1&amp;sn=8d66c94fe8d2c1bddf577f28e2a36dbe&amp;scene=19#wechat_redirect)
- [关于 RISC-V 架构下 RTOS 的一些知识](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484193&amp;idx=1&amp;sn=f905a206557ff85d747bc8c6119cfa51&amp;scene=19#wechat_redirect)
- [我的毕设项目--基于jetson tx2的ros机器人设计](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484062&amp;idx=1&amp;sn=2cfa9c2aa0c679d30746e080ce593fd6&amp;scene=19#wechat_redirect)
- [Nvidia Jetson TX2 刷机并安装JetPack3.1](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484062&amp;idx=2&amp;sn=6f395ddf7272826d6d87c264f0f8feb9&amp;scene=19#wechat_redirect)

<p><img width="706px" src="https://foruda.gitee.com/images/1670672436137490217/1460185b_5631341.jpeg" title="493IC技术圈.jpg"></p>

> 这个版本降低了核心的开销，CPU 寄存器裁剪了一半，为 16 个。

原来 RISC-V 可以这样玩，说好的扩展还能缩减。当真是开了眼界，这是考古到的一篇宝藏文《[关于 RISC-V 架构下 RTOS 的一些知识](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484193&amp;idx=1&amp;sn=f905a206557ff85d747bc8c6119cfa51&amp;scene=19#wechat_redirect)》的片段，它出自一个选集《[IC技术圈 2020 第8期](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484243&amp;idx=1&amp;sn=8d66c94fe8d2c1bddf577f28e2a36dbe&amp;scene=19#wechat_redirect)》，本期主题就是它了。遵照原刊的封面主题，我挑选几张汽车电子的主题图做了替换，使用 SIGer 的大背景的风格。下面是我的考古笔记：

> 看时点 2020年8月，博主毕业了，自诩萌新的他已经骄傲的投稿到了 IC技术圈，这和 SIGer 的编委同学有着相同的体会，初次登录 SIGer 在同学和老师的注视下，第一篇自选主题的分享，成刊的时刻，还是相当激动的。现在 SIGer 的几大频道的主编也是这样成长起来的。和博主交谈，如今已经很少更新博客了，公号也定格了，这和 IC技术圈的文章源情况吻合，大佬们工作太忙，只能将有限的精力，专注在工作上，为公司撰写文档。

> ROS 机器人相当的帅，我找到了博主的公号的主题图，拉满未来感的六足火星车。事无巨细地文风，符合每一位热爱分享的程序员，希望自己的坑能为其他同学提个醒。公号和博主的内容大部分重合，我对照了博主的博客 ROS 标签的文章，公号的内容有3篇是半年后复制到公号上的。也许这就是技术博客自媒体的标准范式，IC技术圈的大佬们也很多是跨社区地重复建号，这里有：B站，知乎，CSDN，还有少不了的微信公号，自建网站的很少，GITEE PAGES 服务没两个。

> 博主的公号非常干练，加好友私聊，推荐 IC技术圈，我特地考古了 IC技术圈的其他博主，我比较喜欢看简介，一段文字，一句话，都能让我驻足浏览，这是相当宝贵的成长曲线的重要部分。也许同学们需要大咖的膜拜，更多师兄师姐的心路历程，才是我们需要了解的日常，唯有怀揣梦想，脚踏实地，才能抵达我们的目标。

这次考古，对宝藏博主有了相对全面的了解，宝藏文章做了一个普查，留待同学们后续学习。之后我会采访博主一些前传和后转，以期更加立体地介绍这位师兄给同学们。

> PS: 他就是昨天刚推荐给同学们的博主《[好书推荐——简史系列](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&mid=2247484025&idx=1&sn=73170c299eb0496793af603d083d0c3c&scene=19#wechat_redirect)》的作者啦！（[SIGer 专题推荐到读书频道](https://gitee.com/flame-ai/siger/blob/master/sig/读书/简史全集.md)）

[strongwong](https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzI0NDU0NzQ5OA==&subscene=0#wechat_redirect)

![输入图片说明](https://foruda.gitee.com/images/1670564488888282242/28433389_5631341.png "屏幕截图")

# 关于 RISC-V 架构下 RTOS 的一些知识

## IC设计

| title | author | img |
|---|---|---|
| [什么是超低功耗基准测试——ULPMark Benchmark](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484332&amp;idx=1&amp;sn=e787f8c516d69f92c74936ad87725e5e&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670592134890478871/718f2596_5631341.png">
| [先进的电源管理-ULP 相关知识](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484332&amp;idx=2&amp;sn=367e42923bd2bc69e0be3811682b3f90&amp;scene=19#wechat_redirect) |  | <img width="96px" src="https://foruda.gitee.com/images/1670592093402037697/016ef7bb_5631341.png">
| [浅谈 RISC-V 软件开发生态之 IDE](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484272&amp;idx=1&amp;sn=d4c7c612b56d43a08a44ef81fd177dab&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670592055501645336/f1521544_5631341.png">
| [简单了解一下小米 vela](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484253&amp;idx=1&amp;sn=61218f343f5ba21593ac125c1917b127&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670592026643662182/52a97b22_5631341.png">
| [IC技术圈期刊 2020年 第08期](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484243&amp;idx=1&amp;sn=8d66c94fe8d2c1bddf577f28e2a36dbe&amp;scene=19#wechat_redirect) |  | <img width="96px" src="https://foruda.gitee.com/images/1670591988079794012/1cb35b48_5631341.png">
| [wujian100 的 PWM 周期问题](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484233&amp;idx=1&amp;sn=fae4d5621c7da87c8785598f6b76111e&amp;scene=19#wechat_redirect) | Edwin | <img width="96px" src="https://foruda.gitee.com/images/1670591953606726255/a245d30d_5631341.png">
| [bittyCore](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484200&amp;idx=1&amp;sn=2f6319832ad9fb30480bbb2606005a39&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670591921162844037/b670b02a_5631341.png">
| [关于 RISC-V 架构下 RTOS 的一些知识](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484193&amp;idx=1&amp;sn=f905a206557ff85d747bc8c6119cfa51&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670591886806613768/d7d68960_5631341.png">
| [使用 vivado 综合 wujian100 生成 bitstream 文件](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484193&amp;idx=2&amp;sn=919e02f4b279190adc8264e91ae988ec&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670589288065284143/6170bec0_5631341.png">
| [Windows 下学习阿里平头哥 RISC-V 芯片开发平台 wujian100](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484193&amp;idx=3&amp;sn=cdc5663436433b6b6ce14a0bd546cf43&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670589255325947920/d476fbdf_5631341.png">
| [FPGA 数字图像处理联合仿真平台的搭建及使用举例](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484135&amp;idx=1&amp;sn=f906d3e71d4f69d8426a4483e29ba895&amp;scene=19#wechat_redirect) | Ricky | <img width="96px" src="https://foruda.gitee.com/images/1670589222242027456/b7abf158_5631341.png">
| [初窥 SDRAM](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484092&amp;idx=3&amp;sn=3acbf2b6aa0a79ea224d0d9ef0b6909c&amp;scene=19#wechat_redirect) | Ricky | <img width="96px" src="https://foruda.gitee.com/images/1670589188040902203/beafaf5e_5631341.png">
| [总线 —— SoC 内部的主干道](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484092&amp;idx=1&amp;sn=e6279c5a3a1e9aef3c7ef48b4b3fd5c0&amp;scene=19#wechat_redirect) | Ricky | <img width="96px" src="https://foruda.gitee.com/images/1670589158127381810/02e81b7c_5631341.png">
| [提升数字 IC 设计效率从 Vim 开始](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484092&amp;idx=2&amp;sn=e793e2a915c53a4f051fb7fcd0786269&amp;scene=19#wechat_redirect) | Ricky | <img width="96px" src="https://foruda.gitee.com/images/1670589129596718391/3e3322ad_5631341.png">

## [ROS](https://blog.strongwong.top/categories/%E5%AD%A6%E4%B9%A0/%E6%AF%95%E4%B8%9A%E8%AE%BE%E8%AE%A1/)

| title | author | img |
|---|---|---|
| [我的毕设项目--基于jetson tx2的ros机器人设计](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484062&amp;idx=1&amp;sn=2cfa9c2aa0c679d30746e080ce593fd6&amp;scene=19#wechat_redirect) |  | <img width="96px" src="https://foruda.gitee.com/images/1670589100712181529/dfc4a4b0_5631341.png">
| [Nvidia Jetson TX2 刷机并安装JetPack3.1](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484062&amp;idx=2&amp;sn=6f395ddf7272826d6d87c264f0f8feb9&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670589064948916517/edb5116a_5631341.png">
| [Jetson TX2 重新编译内核添加usb驱动](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484072&amp;idx=1&amp;sn=f627916fbfdd6f82eabb171c47c5bc9d&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670589027823240419/78c04b14_5631341.png">
| [ROS 机器人操作系统](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484072&amp;idx=2&amp;sn=d2ef0c2e29d9f47a8c2ce88266df5a37&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670588972686073067/58c3d4d6_5631341.png">
| [ROS 的一些基础知识](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484072&amp;idx=3&amp;sn=d9d13d36cab82657bbfb557b0444f322&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670588937635446550/ddf7ff35_5631341.png">

## 有趣

| title | author | img |
|---|---|---|
| [【ATC录音】后疫情时代，日渐繁忙的航路__南京进近](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484241&amp;idx=1&amp;sn=a6292357654a8122388250003b021f01&amp;scene=19#wechat_redirect) |  | <img width="96px" src="https://foruda.gitee.com/images/1670567029868125817/e97c3857_5631341.png">
| [拆解一个很有意思的开关](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484124&amp;idx=1&amp;sn=41ba0221d04d778cfc7333e74a68e418&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670566993482360626/e9e0fb82_5631341.png">
| [使用 Docker + GitLab 构建自用的代码管理服务](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=100000429&amp;idx=1&amp;sn=187dca7bef15751cfae996b23ae92c2e&amp;scene=19#wechat_redirect) |  | <img width="96px" src="https://foruda.gitee.com/images/1670566957579635164/21e2a95a_5631341.png">
| [年轻人的第一次跳槽面试](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=100000612&amp;idx=1&amp;sn=39cb46eb7b09d21a6887583c03ff0f6a&amp;scene=19#wechat_redirect) |  | <img width="96px" src="https://foruda.gitee.com/images/1670566907676280099/acf80e2f_5631341.png">
| [拆解某宝上热销的一款蓝牙耳机](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484111&amp;idx=1&amp;sn=56de19eb01fd7926c516e9148b0ef247&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670566874766260181/5a5bab53_5631341.png">
| [好书推荐——简史系列](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484025&amp;idx=1&amp;sn=73170c299eb0496793af603d083d0c3c&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670566834749707384/96629113_5631341.png">
| [使用树莓派 3b 和 RTL_SDR 搭建小功率无线电监测点](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=100000434&amp;idx=1&amp;sn=6010dc937d0150765e1bfaa391a52f65&amp;scene=19#wechat_redirect) |  | <img width="96px" src="https://foruda.gitee.com/images/1670566781159350200/2f5a9885_5631341.png">
| [使用 Hexo+Github Pages 搭建个人博客记录](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247484045&amp;idx=1&amp;sn=ba46a60b881e7ff512d228559ca72381&amp;scene=19#wechat_redirect) | strongwong | <img width="96px" src="https://foruda.gitee.com/images/1670566723973173541/32ce7425_5631341.png">
| [如何更好的蹭网，开启路由器中继模式](http://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&amp;mid=2247483996&amp;idx=1&amp;sn=80fb57504aba0cd2634f194668aa5c97&amp;scene=19#wechat_redirect) | stronwong | <img width="96px" src="https://foruda.gitee.com/images/1670566205321396238/1cf73fba_5631341.png">

# [关于 RISC-V 架构下 RTOS 的一些知识](https://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&mid=2247484193&idx=1&sn=f905a206557ff85d747bc8c6119cfa51&scene=21#wechat_redirect)

原创 strongwong strongwong 2020-04-09 18:47

![输入图片说明](https://foruda.gitee.com/images/1670648123189294278/93312ac8_5631341.png "屏幕截图")

- 微信号：strongwong BH6BAO
- 功能介绍：嵌入式工程师，业余无线电爱好者，业余 ICer，航模爱好者，个人日记，分享工作生活中的趣事

## 0x00

之前的 blog 有介绍了一些，wujian100 的一些知识，包括综合、测试等。最近就想在 wujian100 上看看能不能移植一下比较常见的一些  **RTOS (Real Time Operating System,实时操作系统)** 上去试试，比如 Free RTOS、RT-Thread等。结果发现这里还是有一些坑的。虽然 FreeRTOS 和 RTT 都支持 RISC-V 的芯片了，但是 wujian100 这个是 RISC-V  **"E" 基础架构** ，也就是 RV32E 就是 标准嵌入式扩展 指令集（ **这个版本降低了核心的开销，CPU 寄存器裁剪了一半，为 16 个** ）。但是 FreeRTOS 和 RTT 目前支持的版本都是 32 个寄存器的，对于任务或者说线程的上下文切换时对栈帧的操作还是有一些差异。然后呢也想对比一下 ARM 架构和 RISC-V 架构下嵌入式实时操作系统处理的一些区别，这里呢就想做一些的简单记录。

## ARM 和 RISC-V 架构的区别

由于我是先学的 ARM 也相对了解一些，所以做什么总是想拿来和 ARM 对比一下，看看能不能套在 ARM 上，这也对自己理解也有一些帮助。缺点就是会产生一些先入为主的观念。

一个最简单的 RTOS 应该至少要实现一个多任务管理的功能，所以 RTOS 也可以叫实时多任务操作系统。那么一个简单的 RTOS 的核心就是怎么处理多任务或者说多线程之间的切换，这里我们也叫做上下文切换，所以上下文切换机制的实现就非常重要，这就要牵扯到不同架构的 CPU 会有不同的处理方式。

### ARM 架构下 RTOS 的一般处理过程

这里以 Cortex-M3 为例，在 ARM 架构中有一组  **特殊功能寄存器组** ，很多时候就是专门留给 OS 使用的。其中由 **CONTROL[0:1]** 寄存器来定义 CPU 的特权等级。这里就要提到在 ARM 架构中的双堆栈机制，在 CM3 内核中支持两个堆栈，一个是 MSP（主堆栈指针）指向的主堆栈和 PSP（线程堆栈指针）指向的线程堆栈。通过配置 CONTROL 寄存器的两个位来选择特权级别和使用不同的堆栈指针（还有一个骚操作就是从异常返回时修改 LR 的 `bit1` 和 `bit2` 也可以切换模式和堆栈，我们可以在很多开源的 RTOS 中见到）。这样通过这两个寄存器的配置就可以分开对待用户程序和系统程序，避免因用户级程序的问题对系统造成危害。同时在出入异常处理时这两个堆栈指针是通过硬件自动切换的，对于现场的保存就不需要软件来处理了。而且在 Handler 或者说异常中只能使用 MSP（主堆栈指针）。

| CONTROL[0] | CONTROL[1] | 组合 | 模式 |
|---|---|---|---|
| 特权选择 | 堆栈指针选择 |  |  |
| 0 | 0 | 特权级+MSP | Handler 模式和 Kernel(OS) |
| 0 | 1 | 特权级+PSP | 线程模式 |
| 1 | 0 | 用户级+MSP | 错误用法 |
| 1 | 1 | 用户级+PSP | 线程模式 |

由于有了这样的机制，在 RTOS 中对于任务切换就带来了很多便利，通常情况下都是通过  **SVCall(即 SVC，System service Call,系统服务调用)** 和  **PendSV(Pendable request for system serivce,可挂起系统调用)** 这两个异常来完成系统特权和任务上下文的切换。当然也可以先不考虑特权模式和用户模式，那么就可以仅通过 PendSV 异常来完成任务上下文切换即可。这里可以参考一下 FreeRTOS 的处理代码：


```
// SVCHandler 进行任务切换
__asm void vPortSVCHandler( void )
{
    extern pxCurrentTCB;    // 外部参数，当前任务控制块指针

    PRESERVE8

    ldr r3, = pxCurrentTCB  // 加载 pxCurrentTCB 的地址到 r3
    ldr r1, [r3]            // 加载 pxCurrentTCB 到 r1
    ldr r0, [r1]            // 加载 pxCurrentTCB 指向的值到 r0, 即当前第一个任务的任务栈栈顶指针
    ldmia r0!, {r4-r11}     // 以 r0 为基地址，将栈里面的内容加载到 r4-r11 寄存器，同时 r0 会递增
    msr psp, r0             // 将 r0 的值，即任务栈指针更新到 psp
    isb
    mov r0, #0              // 将 r0 的值，设置为 0
    msr basepri, r0         // 将 basepri 寄存器设置为0，即所有的中断都没有被屏蔽

    //骚操作
    orr r14, #0x0d          // 当从 SVC 中断服务退出前，通过向 R14 最后4位按位或上0x0d,
                            // 使得硬件在退出时，使用进程堆栈指针 PSP 完成出栈操作并返回后进入线程模式、返回 Thumb 状态
                            //  r14 的 bit1 : 0 PSP 1 MSP；bit2: 0 特权模式  1 用户模式

    bx r14                  // 异常返回，这个时候栈中的剩下内容将会自动加载到 CPU 寄存器
                            // xPSR,PC(任务入口地址),R14,R12,R3,R2,R1,R0(任务形参) 同时 PSP 的值也将更新，即指向任务栈的栈顶
}
```


```
__asm void xPortPendSVHandler(void)
{
    extern pxCurrentTCB;        // 外部参数，当前任务控制块指针
    extern vTaskSwitchContext;  // 外部函数，当前任务切换函数

    PRESERVE8

    // 当进入 PendSVC Handler 时，上一个任务运行环境，即：
    // xPSR,PC(任务入口地址),R14,R12,R3,R2,R1,R0(任务形参),这些将自动保存到任务栈中，剩下的r4-r11需要手动保存
    // 获取任务栈指针到 r0
    mrs r0, psp
    isb

    ldr r3, =pxCurrentTCB   // 加载 pxCurrentTCB 的地址到 r3
    ldr r2, [r3]            // 加载 pxCurrentTCB 到 r2

    stmdb r0!, {r4-r11}     // 将 CPU 寄存器 r4-r11 的值存储到 r0 指向的地址
    str r0, [r2]            // 将任务栈的新的栈顶指针存储到当前任务TCB的第一个成员，即栈顶指针

    stmdb sp!,{r3,r14}      // 将 r3 和 r14 临时压入堆栈，因为即将调用函数 
                            // 调用函数时，返回地址自动保存到 r14 中，导致 r14 的值会被覆盖，所以 r14 的值需要入栈保护
                            // r3 保存的当前激活的任务 TCB 指针( pxCurrentTCB ),函数调用后会用到，因此也需要入栈保护

    mov r0, #configMAX_SYSCALL_INTERRUPT_PRIORITY    // 进入临界段
    msr basepri, r0         // 屏蔽所有中断
    dsb
    isb
    bl vTaskSwitchContext   // 调用函数 vTaskSwitchContext，寻找新的任务运行，通过使变量 pxCurrentTCB 指向新的任务来实现任务切换
    mov r0, #0              // 退出临界段
    msr basepri, r0
    ldmia sp!, {r3,r14}     // 恢复 r3, r14

    ldr r1, [r3]            
    ldr r0, [r1]            // 当前激活的任务 TCB 第一项保存了任务堆栈的栈顶指针，现在栈顶值存入了 r0
    ldmia r0!,{r4-r11}      // 出栈
    msr psp, r0
    isb
    bx r14                  // 异常发生时，R14 中保存异常返回标志，包括返回后进入线程模式还是处理器模式
                            // 使用 psp 堆栈指针还是 msp 堆栈指针，当调用 bx r14 指令后，硬件会知道要从硬件返回
                            // 然后出栈，这个时候堆栈指针 psp 硬件指向了 新任务堆栈的正确位置
                            // 当新任务的运行地址被出栈到 pc 寄存器后，新的任务也会被执行
    nop    
}
```

这两个汇编函数就完成了 ARM 架构下的任务切换机制。其实对于任务上下文切换就是任务现场的保存和恢复，这个现场就是当前的 CPU 运行状态，也就是 CPU 各个寄存器的状态的保存与恢复。这其中也包括很重要的栈帧切换。当然仅仅靠这两个函数也是不完全可靠的，还有一些临界段的处理函数来共同保证任务的安全切换。

### RISC-V 架构下的 RTOS 一般处理过程

在 RISC-V 架构中，也有不同的特权级别，目前主要定义了三种特权级别，分别是 **机器模式（Machine Mode，M-mode）、监管模式（Supervisor Mode,S-mode）和用户模式（User Mode,U-Mode）， 通过 CSRs(control and status registers,控制状态寄存器)**  的 `bit11`、`bit12`(即 MPP 位)两个位的不同编码来实现不同特权模式的切换，在不同特权模式下都有单独的 CSRs。这里需要说明的是我这个 MPP 指的是 Machine-Level CSRs 中 mstatus 寄存器（即 M-mode status register）的控制位。

| Level | MPP[12:11] | 模式 | 简写 |
|---|---|---|---|
| 0 | 0 0 | User/Application | U |
| 1 | 0 1 | Supervisor | S |
| 2 | 1 0 | Reserved（Hypervisor） | （保留） |
| 3 | 1 1 | Machine | M |

但是一个 RISC-V 处理器的实现并不要求同时支持这三种特权级，接受以下的一些实现组合，降低实现成本：

| Number of levels | Supported Modes | Intended Usage |
|---|---|---|
| 1 | M | Simple embedded systems |
| 2 | M,U | Secure embedded systems | 
| 3 | M,S,U | Systems running Unix-like operating systems |

上图中可以看出，这三种模式只有 M-mode 是必须要实现的，其它两种模式是可选的。M-mode 是 RISC-V 中  **hart（hardware thread，硬件线程）** 可以执行的最高权限模式。在 M 模式下运行的 hart 对内存，I/O 和一些对于启动和配置系统来说必要的底层功能有着完全的使用权。因此它是唯一所有标准 RISC-V 处理器都必须实现的权限模式。实际上简单的 RISC-V 微控制器仅支持 M 模式。

好了，上面说的是特权模式和 ARM 的区别，下面就是堆栈指针的区别。上文已经提到 ARM 中有 MSP 和 PSP 之分，且在 handler 中只能使用 MSP，也就意味着 OS 和线程模式使用不同的栈。并且出入异常的栈帧切换由硬件完成。

而在 RISC-V 架构处理器中，没有区分异常、中断和线程模式使用的栈帧，在进入和退出中断处理模式时没有硬件自动保存和恢复上下文（通用寄存器）的操作，因此需要软件明确地使用（汇编语言编写的）指令进行上下文的保存和恢复。并且还要区分 ecall（environment call for U/S/M-mode，不同特权模式下的环境调用异常）。

所以 RISC-V 这一块的处理要复杂一些，有大量的 RISC-V 汇编，具体的代码我就不贴了，有兴趣的可以去看一下 FreeRTOS 的源码。链接： https://github.com/FreeRTOS/FreeRTOS-Kernel/blob/master/portable/GCC/RISC-V/portASM.s

下表是 RISC-V  RV32I 基础指令集寄存器结构，但 RV32E 基础指令集只有 `x0-x15`。

| Register | ABI Name | Description | Saver |
|---|---|---|---|
| x0 | zero | Hard-wired zero | - |
| x1 | ra | Return address | Caller |
| x2 | sp | Stack pointer | Callee |
| x3 | gp | Global pointer | - |
| x4 | tp | Thread pointer | - |
| x5-7 | t0-2 | Temporaries | Caller |
| x8 | s0/fp | Saved register/Frame pointer | Callee |
| x9 | s1 | Saved register | Callee |
| x10-11 | a0-1 | Function Arguments/return values | Caller |
| x12-17 | a2-7 | Function arguments | Caller |
| x18-27 | s2-11 | Saved registers | Callee |
| x28-31 | t3-6 | Temporaries | Caller |

上表中虽然对各个寄存器有了一些描述，但在 RISC-V 指令集中并没有指定专用的堆栈指针或子程序返回地址链接寄存器等，事实上指令编码允许将任何 x 寄存器用于这些目的。但是，标准软件调用约定使用寄存器 x1 来保存呼叫的返回地址，而寄存器 x5 可用作备用链接寄存器。标准调用约定使用寄存器 x2 作为堆栈指针。硬件可能会选择加速使用 x1 或 x5 的函数调用和返回。（不知道这段 Google 翻译的描述是否准确，大家可以去阅读《riscv-spec-20191213》的 2.1 节原文参考）

## 在 wujian100 RISC-V 开源平台上实现简单的任务调度系统

在了解了上面的一些区别后，我准备尝试移植 FreeRTOS 或者 RT-Thread 到 wujian100 上试试，但是我发现它们大多是只支持了以 RV32I 为基础指令集的处理器。而 wujian100 是 E902，是 RV32E 基础指令集，在底层汇编的处理上有一些不同，可能还要做一些修改。所以我就想试着把我之前学习 FreeRTOS 时，实现的仅有任务调度功能的极简版 FreeRTOS 放上去试试，因为代码量比较少。

接下来，我就尝试在 wujian100 开源的 SDK 中移花接木，把我自己这个极简的小操作系统移植上去。在仔细翻阅了 wujian 开源的代码后发现他们这里提供了一个 AliOS 的内核，叫 rhino 内核。在他们这个内核的底层是有实现一些上下文切换的代码的，于是我就基于这个底层把我的上层接上去。当然这过程中还要修改很多东西，这里就不一一详述，直接看这段汇编代码是怎么处理的，这里我已经做了一些修改，我的两个小任务也转起来了。


```
#define MSTATUS_PRV1 0x1880

.global cpu_intrpt_save
.type cpu_intrpt_save, %function
cpu_intrpt_save:
    csrr    a0, mstatus  // 读控制状态寄存器，写入 a0，并返回到 psr 返回值中,psr 是外部定义的一个变量，恢复时会使用
    csrc    mstatus, 8  // 将控制状态寄存器清零。清零对应的标志位，该语句即为清除 MIE ，即禁止全局中断使能。就是禁用中断
    ret

.global cpu_intrpt_restore
.type cpu_intrpt_restore, %function
cpu_intrpt_restore:
    csrw    mstatus, a0   // a0 是传进来的参数，即上一次保存的控制状态寄存器的值，对于 a0 中每一个为 1 的位，把 mstatus 中对应的位进行置位
    ret

.global cpu_task_switch
.type cpu_task_switch, %function
cpu_task_switch:                // 主动任务切换调度
    la     a0, g_intrpt_level_1 // g_intrpt_level_1 是一个全局变量，用于保存当前中断嵌套的层级；这里是将其地址加载到 a0 中
    lb     a0, (a0)             //  将 a0 地址的数据加载到 a0 中
    beqz   a0, __task_switch    // beqz 是对于零时的分支指令，如果等于零，就执行 __task_switch 函数，也就是意味着当前没有中断嵌套

    la     a0, pxCurrentTCB      // 如果不等于零，即有中断嵌套，就进行下面的操作；加载 pxCurrentTCB 的地址到 a0,即获取当前任务指针
    la     a1, g_ReadyTasksLists // 加载 g_ReadyTasksLists 的地址到 a1，即获取当前最高优先级的就绪任务指针
    lw     a2, (a1)  // 加载就绪任务指针到 a2  (lw 指令读取一个字，即4个字节的数据 到 a2
    sw     a2, (a0)  // 将 a2 的低4个字节存储到 a0（即将就绪任务指针放到当前任务）

    ret

.global cpu_intrpt_switch
.type cpu_intrpt_switch, %function
cpu_intrpt_switch:   // 中断中的任务切换 操作和上面类似
    la     a0, pxCurrentTCB
    la     a1, g_ReadyTasksLists
    lw     a2, (a1)
    sw     a2, (a0)

    ret

.global cpu_first_task_start
.type cpu_first_task_start, %function
cpu_first_task_start:      // 第一次进入任务时是不用返回的
    j       __task_switch_nosave

.type __task_switch, %function
__task_switch:    // 任务切换函数 
    addi    sp, sp, -60  // 规划保存数据需要的栈帧大小
// 保存现场，将寄存器的数据保存到栈帧中
    sw      x1, 0(sp)
    sw      x3, 4(sp)
    sw      x4, 8(sp)
    sw      x5, 12(sp)
    sw      x6, 16(sp)
    sw      x7, 20(sp)
    sw      x8, 24(sp)
    sw      x9, 28(sp)
    sw      x10, 32(sp)
    sw      x11, 36(sp)
    sw      x12, 40(sp)
    sw      x13, 44(sp)
    sw      x14, 48(sp)
    sw      x15, 52(sp)

    sw      ra, 56(sp)

    la      a1, pxCurrentTCB // 将当前任务控制块指针地址，加载到 a1
    lw      a1, (a1)         // 将任务控制块指针地址加载到 a1
    sw      sp, (a1)         // 将栈指针加载到当前任务控制块指针地址

__task_switch_nosave:        // 第一次进入任务入口，接下来切换任务指针
    la      a0, g_ReadyTasksLists
    la      a1, pxCurrentTCB
    lw      a2, (a0)
    sw      a2, (a1)

    lw      sp, (a2)

    /* Run in machine mode */
    li      t0, MSTATUS_PRV1
    csrs    mstatus, t0  // 将对于 t0 对应为 1 的每一位置位，即 mpp 设置为 11，machine mode 运行；mpie 置位，用于保存发生异常时 mie 的值；即切换到 M-mode

    lw      t0, 56(sp)   // 将 56(sp) 低 4个字节的数据加载到 t0，即返回地址
    csrw    mepc, t0     // 将 t0 写入 mepc  这里需要注意的是，栈区的数据，在任务初始化的时候就要初始化好，包括第一次启动
// 加载栈帧数据
    lw      x1, 0(sp)
    lw      x3, 4(sp)
    lw      x4, 8(sp)
    lw      x5, 12(sp)
    lw      x6, 16(sp)
    lw      x7, 20(sp)
    lw      x8, 24(sp)
    lw      x9, 28(sp)
    lw      x10, 32(sp)
    lw      x11, 36(sp)
    lw      x12, 40(sp)
    lw      x13, 44(sp)
    lw      x14, 48(sp)
    lw      x15, 52(sp)

    addi    sp, sp, 60
    mret   // M-mode 特有指令，返回时将 PC 指针设置为 mepc,将 mpie 复制到 mie 恢复之前的中断设置，并将特权模式设置为 mpp 中的值；这里就可以完成特权模式的切换(M-U or U-M) 

.global Default_IRQHandler
.type   Default_IRQHandler, %function
Default_IRQHandler:    // 异常、中断处理，这里也需要保存现场，处理类似
    addi    sp, sp, -60

    sw      x1, 0(sp)
    sw      x3, 4(sp)
    sw      x4, 8(sp)
    sw      x5, 12(sp)
    sw      x6, 16(sp)
    sw      x7, 20(sp)
    sw      x8, 24(sp)
    sw      x9, 28(sp)
    sw      x10, 32(sp)
    sw      x11, 36(sp)
    sw      x12, 40(sp)
    sw      x13, 44(sp)
    sw      x14, 48(sp)
    sw      x15, 52(sp)

    csrr    t0, mepc
    sw      t0, 56(sp)

    la      a0, pxCurrentTCB
    lw      a0, (a0)
    sw      sp, (a0)

    la      sp, g_top_irqstack

    csrr    a0, mcause     // 读取异常类型
    andi    a0, a0, 0x3FF
    slli    a0, a0, 2
// 处理异常
    la      a1, g_irqvector
    add     a1, a1, a0
    lw      a2, (a1)
    jalr    a2
// 退出异常，恢复
    la      a0, pxCurrentTCB
    lw      a0, (a0)
    lw      sp, (a0)

    csrr    a0, mcause
    andi    a0, a0, 0x3FF

    /* clear pending *///清除挂起的异常
    li      a2, 0xE000E100
    add     a2, a2, a0
    lb      a3, 0(a2)
    li      a4, 1
    not     a4, a4
    and     a5, a4, a3
    sb      a5, 0(a2)

    /* Run in machine mode */
    li      t0, MSTATUS_PRV1
    csrs    mstatus, t0

    lw      t0, 56(sp)
    csrw    mepc, t0

    lw      x1, 0(sp)
    lw      x3, 4(sp)
    lw      x4, 8(sp)
    lw      x5, 12(sp)
    lw      x6, 16(sp)
    lw      x7, 20(sp)
    lw      x8, 24(sp)
    lw      x9, 28(sp)
    lw      x10, 32(sp)
    lw      x11, 36(sp)
    lw      x12, 40(sp)
    lw      x13, 44(sp)
    lw      x14, 48(sp)
    lw      x15, 52(sp)

    addi    sp, sp, 60
    mret
```

 **除了上面的汇编部分，还有几个主要函数如下，代码工程我后面整理好会上传到我的 Github 上。** 


```
void TaskSwitching_example(void)
{
    prvInitTaskLists();

    Task1_Handle = xTaskCreateStatic(  Task1_Entry,
                                       "Task1_Entry",
                                       TASK1_STACK_SIZE,
                                       NULL,
                                                         1,
                                       Task1Stack,
                                       &Task1TCB );
    // 核心就是插入函数 vListInsert, 将任务插入到就绪列表中
    vListInsert(&pxReadyTasksLists[1], &Task1TCB.xStateListNode);   

    Task2_Handle = xTaskCreateStatic(  Task2_Entry,
                                       "Task2_Entry",
                                       TASK2_STACK_SIZE,
                                       NULL,
                                                         2,
                                       Task2Stack,
                                       &Task2TCB );
    vListInsert(&pxReadyTasksLists[2], &Task2TCB.xStateListNode);    
    vTaskStartScheduler();  //去启动第一个任务
}

void vTaskSwitchContext(void)
{
    // 轮流切换两个任务，我这里任务暂时是手动切换的，没使用优先级
    if( pxCurrentTCB == &Task1TCB)
    {
        g_ReadyTasksLists[0] =& Task2TCB;
    }
    else
    {
        g_ReadyTasksLists[0] =& Task1TCB;
    }
}

void wjYIELD(void)
{
    PSRC_ALL();
    portDISABLE_INTERRUPTS();
    vTaskSwitchContext();
    cpu_task_switch();
    portENABLE_INTERRUPTS();
}

// 第一个任务函数 Task1 入口函数 ；task2 和 task1 一样
void Task1_Entry(void *p_arg)
{
    for(;;)
    {
        flag1 = 1;
            printf("flag1 = %d \n", flag1);
        delay( 100 );
//        vTaskDelay( 20 );
        flag1 = 0;
            printf("flag1 = %d \n", flag1);
        delay( 100 );
//        vTaskDelay( 20 );
            wjYIELD(); // 注意，这里是手动切换任务
    }
}

int main(void)
{
    TaskSwitching_example();
    return 0;
}
```

好了，差不多就这些了。

![图片wujian_CamelOS](https://foruda.gitee.com/images/1670648862841832456/cd526fc2_5631341.png "屏幕截图")

## 踩坑总结

通过这次研究，明白了 ARM 和 RISC-V 架构上的异同，加深了自己对两种架构的理解，相信对以后的学习也更加有帮助。还有就是 wujian100 的开源资料中并没有提供特权架构的相关文档，异常和中断向量表规划也没有具体的说明文档，目前有限的文档中只介绍了外设 IP 的说明，所以在后续的软件开发增加了很多障碍。只有去扒他们提供的 SDK 中的源代码，通过源码来了解他们的架构，还有一点就是他们提供的代码及资料和阿里体系的东西相对耦合或者说兼容。跟开源社区现有的资料和体系不能很好融合。

## 参考资料

- 《RISC-V-Reader-Chinese-v2p1》
- 《riscv-spec-20191213》
- 《riscv-privileged-20190608-1》
- 《Cortex-M3权威指南》
- 《computer organization and design》
- https://github.com/FreeRTOS/FreeRTOS-Kernel/tree/master/portable/GCC/RISC-V

![](https://foruda.gitee.com/images/1670589100712181529/dfc4a4b0_5631341.png)

# [我的毕设项目--基于jetson tx2的ros机器人设计](https://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&mid=2247484062&idx=1&sn=2cfa9c2aa0c679d30746e080ce593fd6&scene=19#wechat_redirect)

strongwong strongwong 2018-08-12 18:36

![输入图片说明](https://foruda.gitee.com/images/1670640472601354068/3e729d86_5631341.png "屏幕截图")

- 微信号：strongwong BH6BAO
- 功能介绍：嵌入式工程师，业余无线电爱好者，业余 ICer，航模爱好者，个人日记，分享工作生活中的趣事

在做毕设的时候就有这个想法，想把毕设过程中学到的和遇到坑全都一点点的记录下来。但是由于毕设时间也比较紧张，又要上班，所以一直拖到现在也没有写些什么。好在最近毕设也答辩结束了，也毕业了，稍稍轻松了一些，准备最近花一些时间来整理一些毕设的东西，写几篇文字记录一下。接下来整体介绍一下这个机器人。

简单来说，我的毕设就是做一个可以进行室内 SLAM 建图的移动机器人。首先呢，我把我的机器人分为上层和底层两个部分。上层为决策层，主要是在 Jetson TX2 开发板上，运行 ROS 机器人操作系统以及 SLAM 算法。通过激光雷达或者深度相机获取深度数据，进行机器人的同时定位与地图构建。底层为具体的控制层，主要是 STM32 通过串口通讯与上层进行通信，接收决策层的速度控制指令，以及进行电机速度控制。机器人的系统框架如下图所示。

![输入图片说明](https://foruda.gitee.com/images/1670640521171106938/f5fbe805_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1670640527573729735/0e571ead_5631341.png "屏幕截图")

> 机器人系统框架

各部分的具体介绍，将在后面我有空的时候慢慢更新，哈哈哈~~

不定期更新~~  
欢迎关注交流~~

![输入图片说明](https://foruda.gitee.com/images/1670640549898272243/5d012c2e_5631341.png "屏幕截图")

# [Nvidia Jetson TX2 刷机并安装JetPack3.1](https://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&mid=2247484062&idx=2&sn=6f395ddf7272826d6d87c264f0f8feb9&scene=19#wechat_redirect)

原创 strongwong strongwong 2018-08-12 18:36

![输入图片说明](https://foruda.gitee.com/images/1670640633536680808/6568b530_5631341.png "屏幕截图")

- 微信号：strongwong BH6BAO
- 功能介绍：嵌入式工程师，业余无线电爱好者，业余 ICer，航模爱好者，个人日记，分享工作生活中的趣事

上篇，我已经简单介绍了一下我的整个小车的物理框架和软件架构。下面我可能会分成几次推文，介绍一下搭建小车的具体过程。 本次主要介绍一下给 Nvidia Jetson TX2 开发板刷机的过程。

### 具体步骤

1. 准备工具
2. 准备工作
3. 安装
4. 结束语

### 准备工具

1. 一块Jetson TX2 开发板
2. 一台安装 Ubuntu 系统的独立主机(不建议使用虚拟机，推荐使用 Ubuntu 16.04)
3. 一台路由器
4. 两根网线，一根 micro usb 数据线

### 准备工作

1. 首先需要从 Nvidia 的官方网站上下载 Jet Pack3.1 的安装包，使用 Ubuntu host 直接下载或者下载好拷贝到 host 上也可以。(我当时最新的是 Jet Pack3.1 ，现在已经到 3.3 了，你也可以使用最新的版本) 下载网址：https://developer.nvidia.com/embedded/jetpack

2. 将 TX2 开发板和主机都通过网线连接到一台路由器上。准备好 TX2 开发板和 Ubuntu Host 之后就可以开始刷机了。

### 安装

下载好 Jet Pack3.1 安装包后，打开 Terminal 进入到安装包所在的目录，执行下面这条命令运行安装包。运行效果如下图。(如果文件没有执行权限可以使用 chmod -x file 命令来改变执行权限)

`$ ./JetPack-L4T-3.1-linux-x64.run`

运行完会弹出 JetPack L4T 3.1 Installer, 一路 Next 就好，如下图：

![输入图片说明](https://foruda.gitee.com/images/1670640726740588362/8b84d1e5_5631341.png "屏幕截图")

注意选择 Jetson TX2 开发板

![输入图片说明](https://foruda.gitee.com/images/1670640731489551141/a9b36870_5631341.png "屏幕截图")

点击 Next 之后会提示输入密码，待安装完成后，就会进入 JetPack L4T Component Manager。(这里要注意，如果网络不好可能会要等很久也出不来安装包信息，所以一定要保证网络环境好，可能有一些包还需要学上网。) 如果你的包加载好了，检查一下 CUDA Toolkit 和 OpenCV for Tegra 这两个包是否选择了，这两个一定要安装。选择好之后，点击 Next 。在弹出的弹框中勾选所有协议，等待各种包下载完成。

![输入图片说明](https://foruda.gitee.com/images/1670640738560531277/650e7e94_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1670640743486316309/e15641c1_5631341.png "屏幕截图")

下载时间可能会比较长，等待下载完成。下载完成后，选择 Host 和 TX2 的连接方式，我们选择第一项，通过同一路由器连接在同一网络。接下来的网口选择保持默认就好。

![输入图片说明](https://foruda.gitee.com/images/1670640749698915326/dccaaf09_5631341.png "屏幕截图")

接下来就是将包移动到 TX2 开发板上。文件较大，可能要等一会。执行下一步后，会出现一个提示重启 TX2 的步骤。按照提示进行操作。

第一步，将 TX2 关机， 拔下电源，使用 micro usb 数据线将 TX2 与 Host 相连。

第二步，重新插上电源，启动 TX2 ，同时按住 rec 和 rst 两个按键两秒钟， 然后松开 rst 按键，按住 rec 按键 3 秒钟。

第三步，这时在 Host 端，重新打开一个 terminal，查看 usb 端口信息(使用命令 lsusb 就可以查看)，这时应该就可以看 ID 为 0955:7C18 的叫 Nvidia Corp 的端口，就说明 TX2 已经进入 REC 模式并和 host 连接好了，这时回到有重启步骤的窗口，按回车 Enter，就开始 TX2 固件更新了。

![输入图片说明](https://foruda.gitee.com/images/1670640758055292740/34f0067f_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1670640763388536928/5607e36b_5631341.png "屏幕截图")

安装完成后 TX2 就会重新启动，然后接下来会进行 CUDA 等一些软件的安装。

至此，Nvidia TX2 的安装就基本完成了。就可以愉快的在 Jetson TX2 上进行开发啦！

### 结束语

在 TX2 上进行基本开发的环境就已经基本搭建好了，但是大型的开发可能 TX2 本身自带的 30 多个 G 内存可能是不够的，因此我们可能还需要一个容量较大的 SSD 来放系统。还有就是 TX2 开发板默认的镜像设置可能会有一些端口没有开放，为了跟好的开发，所以后面需要我们自己重新编译镜像。这些在后面我会继续介绍。

不定期更新~~  
欢迎关注交流~~

![输入图片说明](https://foruda.gitee.com/images/1670640795089924860/59375078_5631341.png "屏幕截图")

# [IC技术圈期刊 2020年 第08期](https://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&mid=2247484243&idx=1&sn=8d66c94fe8d2c1bddf577f28e2a36dbe&scene=19#wechat_redirect)

strongwong 2020-08-15 18:00

![输入图片说明](https://foruda.gitee.com/images/1670639307863023591/4552acff_5631341.png "屏幕截图")

- 微信号：strongwong BH6BAO
- 功能介绍：嵌入式工程师，业余无线电爱好者，业余 ICer，航模爱好者，个人日记，分享工作生活中的趣事

> 编者荐语：
> 
> 加入了 IC 技术圈，萌新第一次在 IC 技术圈投稿。这里都是一些圈内既懂技术又会写公众号的大佬写的各类经验方法。欢迎大家关注 IC 技术圈。

以下文章来源于 ExASIC ，作者IC技术圈的小编们

![输入图片说明](https://foruda.gitee.com/images/1670639374405399082/3113a2c4_5631341.png "屏幕截图")

### 本期文章目录

[一个小型RISC-V开源处理器核介绍！](https://mp.weixin.qq.com/s?__biz=MzI1NDI3MTA0Mg==&mid=2247485549&idx=1&sn=7385782dd1a0d3a0d74c56cc445e572a&scene=21#wechat_redirect)
- #SOC #FPGA #RISC-V
- 数字积木

[为什么说模拟工程师无法被取代，需要什么必备技能？](https://mp.weixin.qq.com/s?__biz=MzU4NTYyODU4MA==&mid=2247486063&idx=1&sn=3708ed4a958c197f5808bbfc6275e693&scene=21#wechat_redirect)
- #模拟工程师 #模拟IC
- ZYNQ

[寻找“芯”人才 | 希望大学像“灯塔”照亮自己也照亮别人](https://mp.weixin.qq.com/s?__biz=MzU4NTYyODU4MA==&mid=2247486062&idx=1&sn=472ee1503f8449857dd16358c24ade55&scene=21#wechat_redirect)
- #芯片设计 #华为 #集成电路
- ZYNQ

[P&R | 如何在实现全流程中考虑IR-Drop](https://mp.weixin.qq.com/s?__biz=MzUzODczODg2NQ==&mid=2247486703&idx=1&sn=0fa15919a919b8a7893c5134216ce65d&scene=21#wechat_redirect)
- #IR #PR
- 陌上风骑驴看IC

[低功耗 | Glitch Power 分析](https://mp.weixin.qq.com/s?__biz=MzUzODczODg2NQ==&mid=2247486718&idx=1&sn=7202a9de0ede4fa41f37aacd2910d323&scene=21#wechat_redirect)
- #低功耗 #glitch power
- 陌上风骑驴看IC

[FPGA DNA加密](https://mp.weixin.qq.com/s?__biz=MzI4NjE3MzUwMA==&mid=2652139734&idx=1&sn=e603560b0187ebfdf91ef20e038d47fc&scene=21#wechat_redirect)
- #FPGA #加密
- 瓜大三哥

[VCS与Verdi的联合仿真](https://mp.weixin.qq.com/s?__biz=MzA5NjI1NDk0NQ==&mid=2247484641&idx=1&sn=a034138ecf29cf149af76b26caab661e&scene=21#wechat_redirect)
- #VCS #Verdi
- 根究FPGA

[通信协议之uart简析](https://mp.weixin.qq.com/s?__biz=MzIzMTg4NDM3Mw==&mid=2247484328&idx=1&sn=a9dd94a69d19e7383a4f4be10a600121&scene=21#wechat_redirect)
- #协议 #uart
- 数字IC小站

[如何看懂congestion map](https://mp.weixin.qq.com/s?__biz=MzU5NzkxNzI0Mg==&mid=2247484697&idx=1&sn=4b709e41612a68e28f6a00e8b742a404&scene=21#wechat_redirect)
- #congestion #后端
- 白山头讲IC

[使用SystemVerilog简化FPGA中的接口](https://mp.weixin.qq.com/s?__biz=MzU4ODY5ODU5Ng==&mid=2247484930&idx=2&sn=e2c6630d63b540531f0dce0fd64c9e68&scene=21#wechat_redirect)
- #FPGA #SystemVerilog #简化接口
- 科学计算Tech

[关于 RISC-V 架构下 RTOS 的一些知识](https://mp.weixin.qq.com/s?__biz=MzI0NDU0NzQ5OA==&mid=2247484193&idx=1&sn=f905a206557ff85d747bc8c6119cfa51&scene=21#wechat_redirect)
- #RISC-V #ARM #RTOS
- strongwong

[AXI4协议详解（一）](https://mp.weixin.qq.com/s?__biz=MzU1OTAwODMwNg==&mid=2247483733&idx=1&sn=e6b6073a324ffb590cfa143b685cfda2&scene=21#wechat_redirect)
- #AMBA #AXI4
- TechDiary

[什么是小黄鸭调试法？](https://mp.weixin.qq.com/s?__biz=MzIyMDc2NTQ0Mw==&mid=2247486124&idx=1&sn=0b1e144e70f34e6cd71c65f3d95bbed8&scene=21#wechat_redirect)
- #ASIC #FPGA #硅农
- 硅农

[Verilog仿真事件队列](https://mp.weixin.qq.com/s?__biz=Mzg3NjAxNTU5NQ==&mid=2247484867&idx=1&sn=5d366b01f8cf09fbb9e444983327e9b1&scene=21#wechat_redirect)
- #Verilog #FPGA
- 精进攻城狮

[国产FPGA概况](https://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw==&mid=2247486939&idx=1&sn=44f55af1e977c0a114e456dcdd331fe2&scene=21#wechat_redirect)
- #FPGA #国产 #概况
- OpenFPGA

[基于FPGA的数字表示](https://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw==&mid=2247486985&idx=1&sn=308d86e2cca84e0fde8cd282f1d5f977&scene=21#wechat_redirect)
- #FPGA #数字 #小数
- OpenFPGA

[硬件设计之 Distributed Arithmetic 一例](https://mp.weixin.qq.com/s?__biz=MzAwMzQ5MTU5OQ==&mid=2649747755&idx=1&sn=d8cb6e7a85015a43ec7539eff0a7bbef&scene=21#wechat_redirect)
- #硬件算法
- icsoc

[Audio ABC | CD为何是44.1KHz采样率？](https://mp.weixin.qq.com/s?__biz=MzAwMzQ5MTU5OQ==&mid=2649747713&idx=1&sn=6d092ba7b2bfb390d45d61b682161885&scene=21#wechat_redirect)
- #音频 #采样率 #Audio
- icsoc

[【重磅干货】手把手教你动态编辑Xilinx FPGA内LUT内容](https://mp.weixin.qq.com/s?__biz=MzU1MzI4ODA5OA==&mid=2247486328&idx=1&sn=2266bad989d15ebd192309616d703183&scene=21#wechat_redirect)
- #FPGA #LUT #动态编辑
- 网络交换FPGA

[备战秋招-求职经验分享](https://mp.weixin.qq.com/s?__biz=MzA3NzUzNTk5Ng==&mid=2247485336&idx=2&sn=04957da1c18f4a32d4550b4dbf213c4f&scene=21#wechat_redirect)
- #笔试准备 #面试准备 #简历
- 摸鱼范式

[你的case真的pass了吗？](https://mp.weixin.qq.com/s?__biz=MzUyODYzMjY3NA==&mid=2247484666&idx=1&sn=855b469b4b1c97882e2711f096e3fc69&scene=21#wechat_redirect)
- #IC验证 #UVM
- 杰瑞IC验证

[人人都会用到，但是大部分人不清楚是什么的“神秘空间”](https://mp.weixin.qq.com/s?__biz=MzUyODYzMjY3NA==&mid=2247484550&idx=1&sn=d50539f25fdb95bc1102c9b50433f917&scene=21#wechat_redirect)
- #IC验证 #SV
- 杰瑞IC验证

[从零开始写RISC-V处理器](https://mp.weixin.qq.com/s?__biz=MzI1NDI3MTA0Mg==&mid=2247485595&idx=1&sn=f00c89d0528febcb4a3472f3afcc72f0&scene=21#wechat_redirect)
- #RISC-V
- 数字积木

[一种数字delayline的设计方案](https://mp.weixin.qq.com/s?__biz=MzI2MDMwOTQ3Mg==&mid=2247483845&idx=1&sn=751d585a0d6bc34af34ba76bf7558e97&scene=21#wechat_redirect)
- #delayline #dll #asic
- IC小迷弟

[判决反馈均衡器（中）](https://mp.weixin.qq.com/s?__biz=MzIxMzY1MzI4Ng==&mid=2247484976&idx=1&sn=87ead36be29c3029e4381d031c7ec9a1&scene=21#wechat_redirect)
- #dfe
- 不忘初心模拟小牛牛

[在CentOS8上编译安装开源EDA工具——Surelog](https://mp.weixin.qq.com/s?__biz=MzIyMjYxNzA4NQ==&mid=2247485512&idx=1&sn=b79c48389baae01771c85c7ec102e222&scene=21#wechat_redirect)
- #eda #开源
- ExASIC

### 往期期刊

- [IC技术圈期刊 2020年 第01期](http://mp.weixin.qq.com/s?__biz=MzIyMjYxNzA4NQ==&mid=2247485330&idx=1&sn=afceac09da46ec3dd79a6af6032898df)
- [IC技术圈期刊 2020年 第02期](http://mp.weixin.qq.com/s?__biz=MzIyMjYxNzA4NQ==&mid=2247485245&idx=1&sn=8e0a82f450cdeeeca636d4625ae4cb9c)
- [IC技术圈期刊 2020年 第03期](http://mp.weixin.qq.com/s?__biz=MzIyMjYxNzA4NQ==&mid=2247485278&idx=1&sn=58ada800abd420a41582efb32c4d1646)
- [IC技术圈期刊 2020年 第04期](http://mp.weixin.qq.com/s?__biz=MzIyMjYxNzA4NQ==&mid=2247485315&idx=1&sn=976ae5a9b7d345b216aec13a30fc39ed)
- [IC技术圈期刊 2020年 第05期](http://mp.weixin.qq.com/s?__biz=MzIyMjYxNzA4NQ==&mid=2247485336&idx=1&sn=4ef2a1bc3d5daa920620d2bb32b86b9a)
- [IC技术圈期刊 2020年 第06期 求职特刊](http://mp.weixin.qq.com/s?__biz=MzIyMjYxNzA4NQ==&mid=2247485396&idx=1&sn=c0f84353348f036f1f0e24b8bca235cc)
- [IC技术圈期刊 2020年 第07期](http://mp.weixin.qq.com/s?__biz=MzIyMjYxNzA4NQ==&mid=2247485502&idx=1&sn=2b5b1a50725a3cd47320e15ef666ba62)

### IC技术圈简介

2020年元旦，我们，十多位IC行业的公众号号主，共同建立了一个 “ IC技术圈 ” 。经过半年的发展，目前有50多位成员。我们将在这里或分享经验，或传授技巧，或聊求职就业，或谈人生规划。

### 视频介绍



### 知乎圈子

![输入图片说明](https://foruda.gitee.com/images/1670640067442273531/4ca0846f_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1670640072733905015/3d43ba43_5631341.png "屏幕截图")

长按二维码立即加入知乎圈子

![输入图片说明](https://foruda.gitee.com/images/1670640082059544696/fca5792e_5631341.png "屏幕截图")

长按下方二维码  
浏览 **IC技术圈** 官方网站

![输入图片说明](https://foruda.gitee.com/images/1670640103449057476/92ce7f51_5631341.png "屏幕截图")

http://iccircle.com

点击“阅读原文”，进入IC技术圈

[阅读原文](http://iccircle.com)

@[BH6BAO](https://gitee.com/bh6bao)

感谢你的关注！希望我的分享对你有帮助！欢迎给我留言，给我提问，知无不言！  
这是我的博客：https://blog.strongwong.top  
我的 GitHub：https://github.com/strongwong  
不定期更新，感兴趣欢迎 Follow，点赞！谢谢！

<p><img width="199px" src="https://foruda.gitee.com/images/1670593217936205439/e0eaae3f_5631341.png"></p>

<p><img width="199px" src="https://foruda.gitee.com/images/1670593228118255964/92aa45ec_5631341.png"></p>

> 2020年元旦，我们，十多位IC行业的公众号主，共同建立了一个"IC技术圈"。经过短短几个月的发展，目前有将近30位成员。我们将在这里或分享经验，或传授技巧，或聊求职就业，或谈人生规划。如果你想获得更多内容，或者想加入我们，都欢迎访问我们的网站。等着志同道合的你一起来，共同创建IC行业的第一大圈子。

- http://iccircle.com/

  IC技术圈：致力于建立知识、人的联系

  - 同名知乎圈子
  - IC技术圈期刊
  - IC知识图谱
  - IC技术圈文章plus

  [首页](http://iccircle.com/) | [成员列表](http://iccircle.com/member) | [期刊](http://iccircle.com/journal) | [开源项目](https://github.com/iccircle) | [搜索](http://iccircle.com/search) | [RSS订阅](http://iccircle.com/feed) | [视频](http://iccircle.com/video) | [EDA在线](http://iccircle.com/eda) | [IC问答](http://chipeasycloud.com/)

  - IC技术圈简介

    > 2020年元旦，我们，十多位IC行业的公众号号主，共同建立了一个 “ IC技术圈 ” 。经过三年的发展，目前有73位成员。我们将在这里或分享经验，或传授技巧，或聊求职就业，或谈人生规划。

  - IC技术圈期刊

    > 优秀IC技术文章精选，接地气、有深度。每月中旬发刊，欢迎阅读，欢迎投稿。

    [FPGA](http://iccircle.com/journal?kw=FPGA) | [前端](http://iccircle.com/journal?kw=前端) | [验证](http://iccircle.com/journal?kw=验证) | [后端](http://iccircle.com/journal?kw=后端) | [嵌入式](http://iccircle.com/journal?kw=嵌入式) | [自动化](http://iccircle.com/journal?kw=自动化) | [模拟](http://iccircle.com/journal?kw=模拟) | [求职就业](http://iccircle.com/journal?kw=求职就业) | [管理](http://iccircle.com/journal?kw=管理) | [软件](http://iccircle.com/journal?kw=软件)

  - IC技术圈成员（随机排列）

    | # | name | comments | ====qr==== |
    |---|---|---|---|
    | 1. | <a href="http://iccircle.com/journal?wxname="><a href="http://iccircle.com/journal?wxname=两猿社">两猿社</a> | 分享IC前端、互联网编程知识，算法，项目经验和一些有趣的想法。还有零基础IC和互联网入门项目连载详解，可软可硬，由你决定。 | ![](https://foruda.gitee.com/images/1670606323750945748/9562fd4a_5631341.png) |
    | 2. | <a href="http://iccircle.com/journal?wxname=Andy的ICer之路">Andy的ICer之路</a> | 分享数字IC前端设计、验证、FPGA的学习过程。一个ICer的成长之路，希望能和大家一起学习共同进步！ | ![输入图片说明](https://foruda.gitee.com/images/1670610126770791352/29957018_5631341.png "屏幕截图")
    | 3. | <a href="http://iccircle.com/journal?wxname=icsoc">icsoc</a> | i cAN sLeep oN cHAIR | ![输入图片说明](https://foruda.gitee.com/images/1670610114093577128/288d7af2_5631341.png "屏幕截图")
    | 4. | <a href="http://iccircle.com/journal?wxname=数字芯片设计笔记">数字芯片设计笔记</a> | 数字IC设计基础，适合研究生及初级工程师。 | ![输入图片说明](https://foruda.gitee.com/images/1670610098382715844/2c1d84aa_5631341.png "屏幕截图")
    | 5. | <a href="http://iccircle.com/journal?wxname=IC解惑君">IC解惑君</a> | 一起学习一起成长，一起做最好的工程师。 | ![输入图片说明](https://foruda.gitee.com/images/1670610083079544897/f09f13c4_5631341.png "屏幕截图")
    | 6. | <a href="http://iccircle.com/journal?wxname=ZYNQ">ZYNQ</a> | 5G技术应用，FPGA学习，数字IC设计，包含C语言与Python编程，分享最芯资讯及ZYNQ/PYNQ学习指南，提供了一个IC领域的技术交流和信息分享平台。 | ![输入图片说明](https://foruda.gitee.com/images/1670610065161278285/c4e5f2de_5631341.png "屏幕截图")
    | 7. | <a href="http://iccircle.com/journal?wxname=FPGA自习室">FPGA自习室</a> | FPGA自习室欢迎大家关注，旨在与大家一同学习与分享，一步一个脚印 | ![输入图片说明](https://foruda.gitee.com/images/1670610050483543228/1d8baddc_5631341.png "屏幕截图")
    | 8. | <a href="http://iccircle.com/journal?wxname=于博士Jacky">于博士Jacky</a> | 于兆杰博士，知乎ID：Jacky-树芯计划。毕业于西安交通大学微电子专业，有十年大厂数字芯片验证经验，对芯片设计验证领域有独到见解，是理论与实践兼备的老鸟。现为“树芯计划”首席讲师，开设数字验证课程，讲课风趣幽默，善于将理论实践结合。 [![输入图片说明](https://foruda.gitee.com/images/1670609982505219368/0ca530d3_5631341.png "树芯计划")](https://b23.tv/IlYhZCp) [![输入图片说明](https://foruda.gitee.com/images/1670610006996266348/b9898cbd_5631341.png "Jacky-树芯计划")](https://www.zhihu.com/people/jocky-90) | ![输入图片说明](https://foruda.gitee.com/images/1670609972129250602/3b4c75da_5631341.png "屏幕截图")
    | 9. | <a href="http://iccircle.com/journal?wxname=傅里叶的猫">傅里叶的猫</a> | 在这里我们会分享各种前沿技术、FPGA/Julia/Python/Matlab/C++等编程的技巧，并会有免费的视频教程。 | ![输入图片说明](https://foruda.gitee.com/images/1670609885884211868/60d3ffbe_5631341.png "屏幕截图")
    | 10. | <a href="http://iccircle.com/journal?wxname=数字芯片实验室">数字芯片实验室</a> | 前瞻性的眼光，和持之以恒的学习。 | ![输入图片说明](https://foruda.gitee.com/images/1670609859583118373/ee477dc2_5631341.png "屏幕截图")
    | 11. | <a href="http://iccircle.com/journal?wxname=IC小迷弟">IC小迷弟</a> | 用心做好ic这件事情 | ![输入图片说明](https://foruda.gitee.com/images/1670609844253487434/e9f1bb23_5631341.png "屏幕截图")
    | 12. | <a href="http://iccircle.com/journal?wxname=FPGA%20LAB">FPGA LAB</a> | 用心践行开源思想，主要分享FPGA开发以及逻辑前端设计相关知识以及心得，欢迎广大温柔的看客关注，你们是我带来优质内容的动力，谢谢！ [![输入图片说明](https://foruda.gitee.com/images/1670609733306224294/dbad5642_5631341.png "李锐博恩")](https://blog.csdn.net/Reborn_Lee) [![输入图片说明](https://foruda.gitee.com/images/1670609756255047866/35176b6b_5631341.png "None")](http://iccircle.com/None) [![输入图片说明](https://foruda.gitee.com/images/1670609781066944689/793e6302_5631341.png "李锐博恩")](https://www.zhihu.com/people/Reborn_Lee) [![输入图片说明](https://foruda.gitee.com/images/1670609808661037233/346040d9_5631341.png "屏幕截图")](https://www.ebaina.com/users/200000090893/articles) | ![输入图片说明](https://foruda.gitee.com/images/1670609697362304179/a11eafae_5631341.png "屏幕截图")
    | 13. | <a href="http://iccircle.com/journal?wxname=数字芯片联合实验室">数字芯片联合实验室</a> | 分享数字芯片设计经验和资源，追逐处理器的前沿技术。 | ![输入图片说明](https://foruda.gitee.com/images/1670609669994352634/6758b78e_5631341.png "屏幕截图")
    | 14. | <a href="http://iccircle.com/journal?wxname=老秦谈芯">老秦谈芯</a> | 交流ASIC技术 | ![输入图片说明](https://foruda.gitee.com/images/1670609653862512353/ea1ad405_5631341.png "屏幕截图")
    | 15. | <a href="http://iccircle.com/journal?wxname=浩杰行天下">浩杰行天下</a> | 浩杰的ic验证之路 | ![输入图片说明](https://foruda.gitee.com/images/1670609637363278373/e430500d_5631341.png "屏幕截图")
    | 16. | <a href="http://iccircle.com/journal?wxname=IC设计验证圈">IC设计验证圈</a> | 主要分享IC设计、验证小知识，后期会关注开源SOC以及低功耗设计。 | ![输入图片说明](https://foruda.gitee.com/images/1670609617642759233/b8c9b3c1_5631341.png "屏幕截图")
    | 17. | <a href="http://iccircle.com/journal?wxname=IC%20Verification%20Club">IC Verification Club</a> | 分享验证相关，好好学习，天天向上！ [![输入图片说明](https://foruda.gitee.com/images/1670609578563088853/a8c3491b_5631341.png "Holden_Liu")](https://blog.csdn.net/Holden_Liu)  | ![输入图片说明](https://foruda.gitee.com/images/1670609572717833391/7ab797eb_5631341.png "屏幕截图")
    | 18. | <a href="http://iccircle.com/journal?wxname=ReCclay">ReCclay</a> | 90后技术宅/在读研究生/CSDN认证博客专家/IC验证干货分享/求职面试题目分享 [![输入图片说明](https://foruda.gitee.com/images/1670609497621470144/7315ea11_5631341.png "ReCclay")](https://recclay.blog.csdn.net/) [![输入图片说明](https://foruda.gitee.com/images/1670609532820427094/54b10ed8_5631341.png "ReCclay")](https://www.zhihu.com/people/recclay/posts) | ![输入图片说明](https://foruda.gitee.com/images/1670609489932894778/52cc8bcf_5631341.png "屏幕截图")
    | 19. | <a href="http://iccircle.com/journal?wxname=FPGA探索者">FPGA探索者</a> | 关注FPGA无线通信物理层实现、数字信号处理、高速接口，Matlab算法仿真，HLS实现，分享求职信息。 | ![输入图片说明](https://foruda.gitee.com/images/1670609468285148230/e3fc6f78_5631341.png "屏幕截图")
    | 20. | <a href="http://iccircle.com/journal?wxname=杰瑞IC验证">杰瑞IC验证</a> | IC验证干货分享，以幽默拆解晦涩，谈笑中传口诀心法，新手进阶之路的加速器。 | ![输入图片说明](https://foruda.gitee.com/images/1670609432066330997/83d8b629_5631341.png "屏幕截图")
    | 21. | <a href="http://iccircle.com/journal?wxname=数字ICer">数字ICer</a> | 数字IC、FPGA、Linux、Python、信号处理等学习笔记以及生活杂记，快乐学习，一起进步。[![输入图片说明](https://foruda.gitee.com/images/1670609361067628041/e500ee99_5631341.png "芯片设计工程师")](https://b23.tv/Kc9Xxm) [![输入图片说明](https://foruda.gitee.com/images/1670609388518653027/60837311_5631341.png "Thomas")](https://www.zhihu.com/people/but-31-18) | ![输入图片说明](https://foruda.gitee.com/images/1670609347705577058/d05ed017_5631341.png "屏幕截图")
    | 22. | <a href="http://iccircle.com/journal?wxname=小蔡读书">小蔡读书</a> | 《小蔡读书》主要写数字功耗和半导体工艺相关内容，另外会发表一些读书心得，谢谢大家支持！ | ![输入图片说明](https://foruda.gitee.com/images/1670609318473978670/1d1161c6_5631341.png "屏幕截图")
    | 23. | <a href="http://iccircle.com/journal?wxname=精进攻城狮">精进攻城狮</a> | SoC设计 | ![输入图片说明](https://foruda.gitee.com/images/1670609300146965028/1d81f2da_5631341.png "屏幕截图")
    | 24. | <a href="http://iccircle.com/journal?wxname=陌上风骑驴看IC">陌上风骑驴看IC</a> | 闲情偶寄，谈天说地，拔草锄地 | ![输入图片说明](https://foruda.gitee.com/images/1670609275341842163/a70eddf5_5631341.png "屏幕截图")
    | 25. | <a href="http://iccircle.com/journal?wxname=亦安的数字小站">亦安的数字小站</a> | 一个什么都发的数字工程师。 [![输入图片说明](https://foruda.gitee.com/images/1670609222859700759/7c77762c_5631341.png "QAQ亦安")](http://iccircle.com/member) | ![输入图片说明](https://foruda.gitee.com/images/1670609161741237827/bdeb63d9_5631341.png "屏幕截图")
    | 26. | <a href="http://iccircle.com/journal?wxname=FPGA技术江湖">FPGA技术江湖</a> | 专注于FPGA电子硬件项目研发、技术交流学习、技术培训服务，仗剑闯天涯，志同煮酒言欢，携手共进FPGA江湖。 ![输入图片说明](https://foruda.gitee.com/images/1670609025571379690/f8c88e6f_5631341.png "FPGA技术江湖") [![输入图片说明](https://foruda.gitee.com/images/1670609045555866948/4871b6a9_5631341.png "FPGA技术江湖")](https://blog.csdn.net/qq_40310273) [![输入图片说明](https://foruda.gitee.com/images/1670609081858300288/a33de1b5_5631341.png "None")](http://iccircle.com/None) [![输入图片说明](https://foruda.gitee.com/images/1670609108052560838/43e3c7cc_5631341.png "FPGA技术江湖")](https://www.zhihu.com/people/luhui0614) | ![输入图片说明](https://foruda.gitee.com/images/1670609015309825613/63fe24eb_5631341.png "屏幕截图")
    | 27. | <a href="http://iccircle.com/journal?wxname=电子狂人">电子狂人</a> | 电子的路很漫长，于此分享技术学习路上的点点滴滴，于此与君共勉~ | ![输入图片说明](https://foruda.gitee.com/images/1670609001412620690/0df173e8_5631341.png "屏幕截图")
    | 28. | <a href="http://iccircle.com/journal?wxname=FPGA算法工程师">FPGA算法工程师</a> | 致力于FPGA、数字IC技术开发，普及现代移动通信、卫星通信技术，分享技术笔记。 | ![输入图片说明](https://foruda.gitee.com/images/1670608988601563019/3ba56c71_5631341.png "屏幕截图")
    | 29. | <a href="http://iccircle.com/journal?wxname=硅农">硅农</a> | 分享数字IC前端设计、FPGA相关知识。总结、记录自己的学习过程，一个硅农的进阶之路。 | ![输入图片说明](https://foruda.gitee.com/images/1670608978086305471/b5b81182_5631341.png "屏幕截图")
    | 30. | <a href="http://iccircle.com/journal?wxname=网络交换FPGA">网络交换FPGA</a> | 秉承“工匠”精神，专注网络与交换领域FPGA开发与芯片实现，记录、分享与交流技术上的点点滴滴，与大家共同进步成长。 | ![输入图片说明](https://foruda.gitee.com/images/1670608968396680604/4f8e2209_5631341.png "屏幕截图")
    | 31. | <a href="http://iccircle.com/journal?wxname=根究FPGA">根究FPGA</a> | 本公众号致力于FPGA案例分析与硬件讲解，欢迎大家来玩~ | ![输入图片说明](https://foruda.gitee.com/images/1670608954246800137/974a68f5_5631341.png "屏幕截图")
    | 32. | <a href="http://iccircle.com/journal?wxname=全栈芯片工程师">全栈芯片工程师</a> | 负责CIS、MCU芯片设计、验证、后端全流程实现，欢迎同行加入交流学习！ | ![输入图片说明](https://foruda.gitee.com/images/1670608945510608099/1b1a3b42_5631341.png "屏幕截图")
    | 33. | <a href="http://iccircle.com/journal?wxname=模拟数字射频IC设计资源交流">模拟数字射频IC设计资源交流</a> | 模拟数字射频IC设计资源交流QQ群 | ![输入图片说明](https://foruda.gitee.com/images/1670608925455296088/09400685_5631341.png "屏幕截图")
    | 34. | <a href="http://iccircle.com/journal?wxname=摸鱼范式">摸鱼范式</a> | 分享一些平时学习数字IC与数字验证的一些收获，包括看的书籍，自制的实验。同时也会持续带来校招社招的资讯。欢迎关注！ | ![输入图片说明](https://foruda.gitee.com/images/1670608913859005528/e305e48d_5631341.png "屏幕截图")
    | 35. | <a href="http://iccircle.com/journal?wxname=硅芯思见">硅芯思见</a> | 仅向大家分享在芯片设计、验证的学习、工作过程中遇到的各种问题，以及一些个人的想法，希望大家互相学习补充！ | ![输入图片说明](https://foruda.gitee.com/images/1670608904303020295/b34153e4_5631341.png "屏幕截图")
    | 36. | <a href="http://iccircle.com/journal?wxname=Spinal%20FPGA">Spinal FPGA</a> | 互联网行业数据中心FPGA码农，记录FPGA进阶之路 | ![输入图片说明](https://foruda.gitee.com/images/1670608896520515734/aab7df2b_5631341.png "屏幕截图")
    | 37. | <a href="http://iccircle.com/journal?wxname=strongwong">strongwong</a> | 嵌入式工程师，业余无线电爱好者，业余 ICer，主要更新个人学习经验 | ![输入图片说明](https://foruda.gitee.com/images/1670608883222844773/b3e85a83_5631341.png "屏幕截图")
    | 38. | <a href="http://iccircle.com/journal?wxname=验证芯发现">验证芯发现</a> | 关注验证语言基础、验证技术和技巧，分享验证相关经验，尝试从多领域交叉的角度发现芯片验证的方方面面。 | ![输入图片说明](https://foruda.gitee.com/images/1670608872628431091/e008510d_5631341.png "屏幕截图")
    | 39. | <a href="http://iccircle.com/journal?wxname=艾思后端实现">艾思后端实现</a> | 专注芯片后端实现流程，策略和技术，同时也是脚本语言的爱好者。 | ![输入图片说明](https://foruda.gitee.com/images/1670608863951535605/8ef256b1_5631341.png "屏幕截图")
    | 40. | <a href="http://iccircle.com/journal?wxname=TechDiary">TechDiary</a> | 【科技日记TechDiary】，为你展现一个小白的进阶之路。 | ![输入图片说明](https://foruda.gitee.com/images/1670608849854267922/deec3fa0_5631341.png "屏幕截图")
    | 41. | <a href="http://iccircle.com/journal?wxname=ICer消食片">ICer消食片</a> | 主要分享数字IC和芯片验证相关技术，以及职场经验，个人学习进阶和相关IC资讯等等，知乎b站同名。 ![输入图片说明](https://foruda.gitee.com/images/1670608678351445790/e2ca71ab_5631341.png "ICer消食片") ![输入图片说明](https://foruda.gitee.com/images/1670608722642949835/4060558b_5631341.png "ICer消食片") [![输入图片说明](https://foruda.gitee.com/images/1670608741745545506/06c13bd3_5631341.png "ICer消食片")](https://blog.csdn.net/weixin_41921488) [![输入图片说明](https://foruda.gitee.com/images/1670608772948042769/c45df765_5631341.png "ICer消食片")](https://space.bilibili.com/27899744) [![输入图片说明](https://foruda.gitee.com/images/1670608814888971520/89ee95ea_5631341.png "ICer消食片")](https://zhihu.com/people/eb66c92e89f861a67a2695afc6054ca7) | ![输入图片说明](https://foruda.gitee.com/images/1670608667966309441/93f81034_5631341.png "屏幕截图")
    | 42. | <a href="http://iccircle.com/journal?wxname=FPGA技术联盟">FPGA技术联盟</a> | 业界最专业的FPGA以及数字电路设计公众号，提供了一个FPGA领域的技术交流和信息分享平台。 | ![输入图片说明](https://foruda.gitee.com/images/1670608652793806776/627fb5bf_5631341.png "屏幕截图")
    | 43. | <a href="http://iccircle.com/journal?wxname=瓜大三哥">瓜大三哥</a> | 深耕在FPGA，专注着视频图像处理领域，卓越于神经网络。 | ![输入图片说明](https://foruda.gitee.com/images/1670608643727241242/631cee42_5631341.png "屏幕截图")
    | 44. | <a href="http://iccircle.com/journal?wxname=数字IC与硬件设计的两居室">数字IC与硬件设计的两居室</a> | 数字IC设计/FPGA/Verilog等硬件知识的分享，为数字萌新提供学习教程，为经验老手提供查询手册。不求极为深刻高端，但求特别细节全面。 | ![输入图片说明](https://foruda.gitee.com/images/1670608632480256249/2aef75b9_5631341.png "屏幕截图")
    | 45. | <a href="http://iccircle.com/journal?wxname=IC验证分享圈">IC验证分享圈</a> | 致力于分享IC验证相关的技术、经验和心得和学习总结。任何人都可以投稿分享自己在IC验证或IC领域的技术经验和心得，打造一个共同分享、共同学习、共同进步的学习交流圈。欢迎大家积极分享！ | ![输入图片说明](https://foruda.gitee.com/images/1670608621715414924/d172cbf5_5631341.png "屏幕截图")
    | 46. | <a href="http://iccircle.com/journal?wxname=数字积木">数字积木</a> | 主打数字逻辑设计和FPGA开发，包括数字信号处理，数字图像处理，CPU设计，SOC设计等等，也会发一些其他方面的技术杂文。 | ![输入图片说明](https://foruda.gitee.com/images/1670608611361167186/82ee9abe_5631341.png "屏幕截图")
    | 47. | <a href="http://iccircle.com/journal?wxname=模拟IC设计实践">模拟IC设计实践</a> | 提供模拟IC设计课程与项目资源 | ![输入图片说明](https://foruda.gitee.com/images/1670608599005406377/9ec03360_5631341.png "屏幕截图")
    | 48. | <a href="http://iccircle.com/journal?wxname=不忘出芯">不忘出芯</a> | 【不忘出芯】少说废话，多做实事。 | ![输入图片说明](https://foruda.gitee.com/images/1670608587887999137/e92c7fe8_5631341.png "屏幕截图")
    | 49. | <a href="http://iccircle.com/journal?wxname=阿辉说">阿辉说</a> | 主要记录平常的一些零零碎碎，我是从硬件设计转行IC验证，中间经历了比较痛苦的转型期，想通过记录锻炼自己的写作能力，同时作为一种投资，如果能帮到一些人更好。 | ![输入图片说明](https://foruda.gitee.com/images/1670608565766157180/2f89f395_5631341.png "屏幕截图")
    | 50. | <a href="http://iccircle.com/journal?wxname=芯片学堂">芯片学堂</a> | 专注于芯片验证、芯片设计、脚本工具、学科前沿和工程经验等专业技术分享，期待您的关注！ | ![输入图片说明](https://foruda.gitee.com/images/1670608554166747792/47e26d77_5631341.png "屏幕截图")
    | 51. | <a href="http://iccircle.com/journal?wxname=数字设计课堂">数字设计课堂</a> | 记录数字IC前端的方方面面，目前致力于基础知识和求职面试题目的分享。记录成长，共同进步！ | ![输入图片说明](https://foruda.gitee.com/images/1670608542609969131/f1d4be9b_5631341.png "屏幕截图")
    | 52. | <a href="http://iccircle.com/journal?wxname=码农的假期">码农的假期</a> | 主要是周末或者假期更新，围绕FE/ME/BE衔接部分，分享自己工作或者学习的心得。 | ![输入图片说明](https://foruda.gitee.com/images/1670608531803358922/401e701b_5631341.png "屏幕截图")
    | 53. | <a href="http://iccircle.com/journal?wxname=ExASIC">ExASIC</a> | 分享数字集成电路设计中的经验和方法。分享让工作更轻松。![输入图片说明](https://foruda.gitee.com/images/1670608400550936598/a2b5939e_5631341.png "ExASIC") [![输入图片说明](https://foruda.gitee.com/images/1670608414669029915/c078c2ec_5631341.png "ExASIC的博客")](https://blog.csdn.net/u013233381?spm=1011.2124.3001.5113) [![输入图片说明](https://foruda.gitee.com/images/1670608434537304500/0a8b5d86_5631341.png "陈锋ExASIC")](https://www.zhihu.com/people/chen-feng-37-23-76) [![输入图片说明](https://foruda.gitee.com/images/1670608503613612698/ec4a6755_5631341.png "屏幕截图")](http://exasic.com/) | ![输入图片说明](https://foruda.gitee.com/images/1670608393572257745/f2cbf62d_5631341.png "屏幕截图")
    | 54. | <a href="http://iccircle.com/journal?wxname=RTL2GDS">RTL2GDS</a> | 主要涉及数字集成电路设计中从RTL到GDS Tapeout整个过程中的知识点系统性分享。包括：IC后端设计(物理设计), 综合, STA, PV, DFT等。也会发表一些行业观察类的文章。 | ![输入图片说明](https://foruda.gitee.com/images/1670608377289941112/251ce080_5631341.png "屏幕截图")
    | 55. | <a href="http://iccircle.com/journal?wxname=OpenFPGA">OpenFPGA</a> | OpenFPGA - 分享才是“硬”道理。![输入图片说明](https://foruda.gitee.com/images/1670607292291936673/914a1092_5631341.png "碎碎思") [![输入图片说明](https://foruda.gitee.com/images/1670607314711912551/b58bd719_5631341.png "碎碎思")](https://blog.csdn.net/Pieces_thinking?spm=1000.2115.3001.5343) [![输入图片说明](https://foruda.gitee.com/images/1670607349611908125/12d9df19_5631341.png "OpenFPGA")](https://space.bilibili.com/1551161745) [![输入图片说明](https://foruda.gitee.com/images/1670607386447376378/6be8b47d_5631341.png "OpenFPGA")](https://mp.toutiao.com/profile_v4/index) [![输入图片说明](https://foruda.gitee.com/images/1670607414743130057/9eb57b7b_5631341.png "碎碎思")](https://www.zhihu.com/people/xin-li-you-jbshu) | ![输入图片说明](https://foruda.gitee.com/images/1670606894952106431/29e69651_5631341.png "屏幕截图")
    | 56. | <a href="http://iccircle.com/journal?wxname=路科验证">路科验证</a> | 专注于系统验证思想和前沿验证资讯，为IC验证从业人员提供技术食粮。 | ![输入图片说明](https://foruda.gitee.com/images/1670606882584698456/8dc572f6_5631341.png "屏幕截图")
    | 57. | <a href="http://iccircle.com/journal?wxname=IC媛">IC媛</a> | 老学姐分享数字IC求职经验 | ![输入图片说明](https://foruda.gitee.com/images/1670606871635425652/84f4a5f7_5631341.png "屏幕截图")
    | 58. | <a href="http://iccircle.com/journal?wxname=数字IC小站">数字IC小站</a> | 记录学习历程，你我共同成长。数字IC小站。 | ![输入图片说明](https://foruda.gitee.com/images/1670606861612454464/cb5bcb35_5631341.png "屏幕截图")
    | 59. | <a href="http://iccircle.com/journal?wxname=数字芯片设计工程师">数字芯片设计工程师</a> | 工作研究手稿整理及一小部分教材，包括功能验证，Fpga IP核研究，Fpga工程开发，接口等。 | ![输入图片说明](https://foruda.gitee.com/images/1670606851884380477/e6245954_5631341.png "屏幕截图")
    | 60. | <a href="http://iccircle.com/journal?wxname=疯狂的FPGA">疯狂的FPGA</a> | 跌跌撞撞，坎坎坷坷，进入FPGA的世界已经十年之久。岁月过得太匆匆，慢慢的认识到了责任，认识到了义务，也认识到了社会使命。我还是我，但我已经不再是我——不会再骄傲，人各有所长；不会再狂躁，心静自然凉；不会再轻视，人屌有资本，人嫩潜力大……走过了太多坎坷的路，错过了太多唯美的风景，对不起青春，不能对不起梦想，越来越觉得，我可能需要一个公众号（CrazyFPGA），能和大家诉说我们的一起走过的故事。 | ![输入图片说明](https://foruda.gitee.com/images/1670606798464950592/52db1142_5631341.png "屏幕截图")
    | 61. | <a href="http://iccircle.com/journal?wxname=FPGA开发之路">FPGA开发之路</a> | 本公众号旨在记录自己在FPGA开发设计领域的点滴学习和思考 | ![输入图片说明](https://foruda.gitee.com/images/1670606789297471263/49c8140f_5631341.png "屏幕截图")
    | 62. | <a href="http://iccircle.com/journal?wxname=酒酒聊IC编程">酒酒聊IC编程</a> | 酒酒成电研三在读，自学算法leetcode刷题，双修IC验证，斩获互联网公司BAT offer 以及一些IC大厂offer：zeku、展锐、华为、寒武纪、地平线的程序媛 and IC媛一枚，在公众号分享真实的求职经历及未来大厂工作经验，欢迎和酒酒一起学IT或IC呀 | ![输入图片说明](https://foruda.gitee.com/images/1670606750804882482/37d9aa48_5631341.png "屏幕截图")
    | 63. | <a href="http://iccircle.com/journal?wxname=芯灵动">芯灵动</a> | 追逐 IC前沿技术，挑战芯的问题，保持一颗执着的“芯”，迈向“芯台阶”。 | ![输入图片说明](https://foruda.gitee.com/images/1670606736281595873/9c312841_5631341.png "屏幕截图")
    | 64. | <a href="http://iccircle.com/journal?wxname=数字IC打工人">数字IC打工人</a> | 用户群体为在校学生，主要致力于分享数字IC知识，笔试真题，面试技巧等。IC笔试冲冲冲 MARS | ![输入图片说明](https://foruda.gitee.com/images/1670606725001625769/b2356bb1_5631341.png "屏幕截图")
    | 65. | <a href="http://iccircle.com/journal?wxname=数字IC剑指offer">数字IC剑指offer</a> | 专注于分享求职过程中实用的数字IC前端、后端知识点。 | ![输入图片说明](https://foruda.gitee.com/images/1670606716613258645/d285f174_5631341.png "屏幕截图")
    | 66. | <a href="http://iccircle.com/journal?wxname=歪睿老哥">歪睿老哥</a> | 一个芯片设计行业老哥；忙时研发，闲时写作；聚焦芯片行业的那些事，唯武侠与芯片不可辜负。 | ![输入图片说明](https://foruda.gitee.com/images/1670606706739747691/90ca91c9_5631341.png "屏幕截图")
    | 67. | <a href="http://iccircle.com/journal?wxname=芯片设计验证">芯片设计验证</a> | 专注芯片设计验证的前沿技术、工具技巧、职业发展的分享。 | ![输入图片说明](https://foruda.gitee.com/images/1670606697802203815/bbcae70d_5631341.png "屏幕截图")
    | 68. | <a href="http://iccircle.com/journal?wxname=白话IC">白话IC</a> | ic设计技术分享与感悟。目前主要是综合、PR、STA。 | ![输入图片说明](https://foruda.gitee.com/images/1670606688243329075/0b447ad1_5631341.png "屏幕截图")
    | 69. | <a href="http://iccircle.com/journal?wxname=FPGA开源工作室">FPGA开源工作室</a> | 知识，创新，创艺，FPGA，matlab，opencv，数字图像，数字信号，数字世界。传递有用的知识，传递创艺的作品。FPGA开源工作室欢迎大家的关注。 | ![输入图片说明](https://foruda.gitee.com/images/1670606677679269442/9df27312_5631341.png "屏幕截图")
    | 70. | <a href="http://iccircle.com/journal?wxname=FPGA and ICer">FPGA and ICer</a> | 致力于FPGA 和IC的coder，记录学习过程，分享学习的经验，道阻且长，行则将至。Vuko-wxh | ![输入图片说明](https://foruda.gitee.com/images/1670606664042931152/acf12452_5631341.png "屏幕截图")
    | 71. | <a href="http://iccircle.com/journal?wxname=芯启示">芯启示</a> | 号主模拟IC博士搬砖狗，芯片重度爱好者，喜深度思考，闲暇涉猎经融史哲，常怀敬畏，获益匪浅。于模数转换器，相信精度的高低与带宽的大小同等重要。于知识，深信理解的层次同推广的能力同等重要。于人生，坚信思维的深度和眼界的宽度同等重要。重点为IC技术分享交流，以及人生所思所感。给大家以启示，为此号存在之第一要义。 | ![输入图片说明](https://foruda.gitee.com/images/1670606654600172845/770154dd_5631341.png "屏幕截图")
    | 72. | <a href="http://iccircle.com/journal?wxname=OpenIC">OpenIC</a> | 专注于数字芯片设计，可测性设计（DFT）技术的分享，芯片相关科普，以及半导体行业时事热点的追踪。 | ![输入图片说明](https://foruda.gitee.com/images/1670606642446240822/b28b0fd8_5631341.png "屏幕截图")
    | 73. | <a href="http://iccircle.com/journal?wxname=ICSolveRecord">ICSolveRecord</a> | Wonder Why Wonder How关于IC 后端常见问题实践经历 问题探讨 记录总结 | ![](https://foruda.gitee.com/images/1670606165302774600/4b644ede_5631341.png)
